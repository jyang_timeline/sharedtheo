import glob
import os
import pickle
import shutil
from group import Group
from collections import OrderedDict


class CorrIndexSharedTheo:

    def __init__(self, cluster_dir):
        if cluster_dir[-1] == '/':
            cluster_dir = cluster_dir[:-1]

        self.cluster_dir = cluster_dir
        self.cluster_data = self._read_cluster_data()

    def _read_cluster_data(self):
        dirs = glob.glob(os.path.join(self.cluster_dir, '*'))
        data = {}

        for dir in dirs:
            corr = dir.split('corr')[1].split('_')[0]
            clusters = pickle.load(open(os.path.join(dir, 'clusters.p'), 'rb'))
            inverse_clusters = pickle.load(open(os.path.join(dir, 'inverse_clusters.p'), 'rb'))
            data[corr] = {'c': clusters, 'ic': inverse_clusters}

        return data

    def _get_default_basename(self, suffix):
        basename = os.path.basename(self.cluster_dir) + '_' + suffix
        return basename    
