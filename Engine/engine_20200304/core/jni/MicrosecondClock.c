#include <jni.h>

#include <stdio.h>
#include <time.h>

#include "com_nogle_core_util_MicrosecondClock.h"

JNIEXPORT jlong JNICALL
Java_com_nogle_core_util_MicrosecondClock_getMicrosecond(JNIEnv* env,
		jobject object) {
	struct timespec tp;
	// clock_gettime(CLOCK_MONOTONIC, &tp);
	clock_gettime(CLOCK_REALTIME, &tp);
	return tp.tv_nsec;
}

