package com.nogle.core;

import org.apache.commons.lang3.StringUtils;

public class ClientParameters {
    private static final String ACCOUNT_UNKNOWN = "*";

    private final String username;
    private final String password;

    public ClientParameters(final String username, final String password) {
        this.username = StringUtils.isEmpty(username) ? ACCOUNT_UNKNOWN : username;
        this.password = password;
    }

    public final String getUsername() {
        return username;
    }

    public final String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return username;
    }

}
