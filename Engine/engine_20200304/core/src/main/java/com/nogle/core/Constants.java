package com.nogle.core;

import com.nogle.core.config.EngineMode;

public class Constants {
    public final static String COMMA = ",";
    public final static String SLASH = "\\";
    public final static String B_SLASH = "/";
    public final static String COLON = ":";
    public final static String WILDCARD = "*";
    public final static String TAB = "\t";
    public final static String NEW_LINE = "\n";
    public final static String LBRACES = "{";
    public final static String RBRACES = "}";
    public final static String SYMBOL_TYPE_SEPARATOR = "~";
    public final static String UNDERSCORE = "_";
    public final static String FUZZY_QUERY = "%";
    public final static String QUOTE_PRODUCT_SEPARATOR = UNDERSCORE;
    public final static String QUOTE_SYMBOLS_SEPARATOR = COLON;
    public final static String COMPOUNDKEY_SEPARATOR = SLASH;
    public final static String STRATEGY_FILE_SEPARATOR = COMMA;
    public final static String IPC_PORT_SEPARATOR = "@";
    public final static String PROP_FILE_EXT = ".properties";
    public final static String EMPTY_STRING = "";
    public final static String COMMAND_SEPARATOR = " ";
    public final static char EMPTY_CHAR = '\0';
    public final static int SPLIT_LIMIT = -1;
    public final static String NEWLINE = System.getProperty("line.separator");

    public final static int DEFAULT_NULL_INT = -1;
    public final static double DEFAULT_NULL_DOUBLE = Double.NaN;
    public final static String BUY = "buy side";
    public final static String SELL = "sell side";
    public final static int LONGORDER = 1;
    public final static int SHORTORDER = -1;
    public final static char AT = '@';

    public static final int GATEWAY_DEFAULT_PORT = 5557;
    public static final int TRADER_DEFAULT_PORT = 5567;

    // ================================= Market Data Section =================================/
    public final static int BOOK_DATA = 2;
    public final static int TICK_DATA = 1;
    public final static String DEFAULT_TESTING_GATEWAY_ID = "one";

    private final static String MARKETBOOK_TOPIC_PREFIX = "mb:/";
    private final static String QUOTETICK_TOPIC_PREFIX = "qt:/";
    private final static String IPC_PREFIX = "ipc://";
    private final static String TEMP_PATH = "/tmp";
    private final static String TCP_PREFIX = "tcp://";
    private final static String DEFAULT_BROKERID = "8000";

    // ================================= Order Request Section =================================/
    public final static int INVALID_CLIENT_ORDER_ID = -1;
    public final static int CLIENT_ORDER_MULTIPLIER = 10000;
    public final static int TRADE_SERVER_SPLITORDER_INCREMENT = CLIENT_ORDER_MULTIPLIER / 2;
    public final static int DATE_ORDER_MULTIPLIER = 1000000; // 100 ENGINE * above number

    public static String composeTickBookTopic(final int type, String brokerId, final String instrumentId) {
        if (brokerId == null) {
            // use default
            brokerId = DEFAULT_BROKERID;
        }
        if (type == TICK_DATA) {
            final StringBuilder sbQuote = new StringBuilder(24);
            sbQuote.append(QUOTETICK_TOPIC_PREFIX).append(instrumentId).append(B_SLASH).append(brokerId);
            return sbQuote.toString();
        } else if (type == BOOK_DATA) {
            final StringBuilder sbBook = new StringBuilder(24);
            sbBook.append(MARKETBOOK_TOPIC_PREFIX).append(instrumentId).append(B_SLASH).append(brokerId);
            return sbBook.toString();
        } else {
            return "unknown";
        }
    }

    /**
     * For AsyncClient and AsyncServer
     *
     * @param mode
     * @param type
     * @param brokerId
     * @param host
     * @param port
     * @return
     */
    public static String composeURL(final EngineMode mode, final int type, final String brokerId, final String host, final int port) {
        final StringBuilder sb = new StringBuilder(32);
        if (mode == EngineMode.PRODUCTION) {
            // everything is tcp based, tcp://*:port? tcp://ipv4:port what's the difference?
            sb.append(TCP_PREFIX).append(host).append(COLON).append(port);

        } else {
            // ipc:///tmp/type/one/@port
            sb.append(IPC_PREFIX).append(TEMP_PATH)//
                .append(B_SLASH).append(type)//
                .append(B_SLASH).append(brokerId)//
                .append(B_SLASH).append(IPC_PORT_SEPARATOR).append(port);
        }
        return sb.toString();
    }

    public static String convertInternalSymbolFormat(final String exchange, final String symbol) {
        final StringBuilder symbolBuilder = new StringBuilder(32);
        // EXCHANGE + _ + Code + Year[2 digits] + Month[2 digits] (EX: CFFEX_IF1503)
        symbolBuilder.append(exchange).append(QUOTE_PRODUCT_SEPARATOR).append(symbol);
        return symbolBuilder.toString();
    }

    public enum HEADER_TYPE {
        PING(0, false),
        PONG(1, false),
        TICK(2, true),
        BOOK(4, true),
        ORDERREQ(8, true),
        ORDERREP(16, true),
        ORDEREXE(32, true);

        HEADER_TYPE(final int type, final boolean isData) {
            this.type = type;
            this.isData = isData;
        }

        public int getType() {
            return type;
        }

        private final int type;
        @SuppressWarnings("unused")
        private final boolean isData;
    }

}
