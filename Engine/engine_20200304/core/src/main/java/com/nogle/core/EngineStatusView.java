package com.nogle.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.utils.EngineHeartbeat;
import com.nogle.core.event.ExchangeEvent;
import com.nogle.core.strategy.CoreBuilder;
import com.nogle.strategy.types.Contract;
import com.nogle.util.ArrayUtils;
import com.timelinecapital.commons.HeartbeatWarnings;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.metrics.EngineDatapoint;
import com.timelinecapital.commons.metrics.EngineMetrics;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.commons.util.FeedStatisticsUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.handler.ContractInfoWarningHandler;

public class EngineStatusView {
    private static final Map<String, ExchangeEvent> exchangeToExgEvent = new HashMap<>();
    private static final Map<Exchange, byte[]> warningExchangeWithType = new HashMap<>();

    private static final Set<String> warningExchanges = new HashSet<>();
    private static final Set<String> warningContracts = new HashSet<>();

    private static String noFeedMsg = StringUtils.EMPTY;
    private static String noInfoMsg = StringUtils.EMPTY;
    private static String noFeedByTypeMsg = StringUtils.EMPTY;

    private final EngineHeartbeat heartbeat;
    private final EngineMetrics metrics = new EngineMetrics();
    private final EngineDatapoint datapoint = new EngineDatapoint();

    private ConnectionProbe[] probes = new ConnectionProbe[0];
    private int strategyCount;

    private EngineStatusView() {
        heartbeat = new EngineHeartbeat(EngineConfig.getEngineId());

        datapoint.setId(EngineConfig.getEngineId());
        metrics.setLabel("EngineHBT");
        metrics.setDatapoints(Arrays.asList(datapoint));

        CoreBuilder.addContractInfoHandler(new ContractInfoWarningHandler(this));
    }

    private static class StatusViewHolder {
        public static final EngineStatusView INSTANCE = new EngineStatusView();
    }

    public static EngineStatusView getInstance() {
        return StatusViewHolder.INSTANCE;
    }

    public synchronized ExchangeEvent getExchangeEvent(final Contract contract) {
        final Exchange exchange = Exchange.valueOf(contract.getExchange());
        ExchangeEvent exchangeEvent = exchangeToExgEvent.get(exchange.toString());
        if (exchangeEvent == null) {
            exchangeEvent = new ExchangeEvent(exchange);
            exchangeToExgEvent.put(exchange.toString(), exchangeEvent);
            heartbeat.setFeedCount(exchange.toString(), 0l);
            datapoint.getFeedEvents().computeIfAbsent(exchange.toString(), e -> new long[DataType.values().length]);
        }
        return exchangeEvent;
    }

    public synchronized void addConnectionProbe(final ConnectionProbe probe) {
        probes = ArrayUtils.addElement(probes, probe);
    }

    public synchronized void updateConnectionProbeStatus(final String name, final boolean isConnected) {
        for (final ConnectionProbe probe : probes) {
            if (name.contentEquals(probe.getConnectionKey())) {
                probe.setConnected(isConnected);
            }
        }
    }

    public synchronized void increaseStrategyCount() {
        strategyCount++;
    }

    public synchronized void decreaseStrategyCount() {
        strategyCount--;
    }

    void fromBase() {
        heartbeat.setModels(strategyCount);
        for (final ConnectionProbe probe : probes) {
            checkConnectionWarnings(probe);
            heartbeat.setConnectionStatus(probe.getConnectionKey(), probe.isConnected());
        }

        for (final ExchangeEvent event : exchangeToExgEvent.values()) {
            checkFeedWarnings(heartbeat.getFeedCount().get(event.getExchangeName()), event.getFeedEventCount(), event.getExchangeName());
            heartbeat.setFeedCount(event.getExchangeName(), event.getFeedEventCount());
        }
    }

    void fromHeartbeat() {
        datapoint.setModels(strategyCount);
        for (final ConnectionProbe probe : probes) {
            datapoint.setConnection(probe.getConnectionKey(), probe.isConnected());
        }

        for (final ExchangeEvent event : exchangeToExgEvent.values()) {
            final long[] snapshot = datapoint.getFeedEvents().get(event.getExchangeName());
            final long[] running = event.getMarketDataCount();

            final int hashCode = event.getFeedDiff().hashCode();

            for (int i = 0; i < snapshot.length; i++) {
                event.getFeedDiff().set(i, FeedStatisticsUtil.hasNoUpdate(snapshot[i], running[i], event.isDataTypeRegistered(i)));
            }

            if (event.getFeedDiff().isEmpty()) {
                if (warningExchangeWithType.containsKey(event.getExchange())) {
                    warningExchangeWithType.remove(event.getExchange());
                    updateQuoteWarnings();
                }
            } else {
                if (hashCode != event.getFeedDiff().hashCode()) {
                    warningExchangeWithType.put(event.getExchange(), event.getFeedDiff().toByteArray());
                    updateQuoteWarnings();
                }
            }
            System.arraycopy(running, 0, snapshot, 0, running.length);
        }
    }

    public EngineHeartbeat translateToHeartbeat() {
        fromBase();
        heartbeat.withoutContractInfo();
        return heartbeat;
    }

    public EngineHeartbeat translateToQueryResponse() {
        fromBase();
        // TODO get contract summary
        return heartbeat;
    }

    public EngineMetrics translateToMetrics() {
        fromHeartbeat();
        return metrics;
    }

    public void addWarnings(final Contract contract) {
        warningContracts.add(contract.getSymbol());
        noInfoMsg = String.join(DelimiterUtil.MESSAGE_SEPARATOR, warningContracts);
        heartbeat.setWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS, noInfoMsg);
        datapoint.setWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS, noInfoMsg);
    }

    public void removeWarnings(final Contract contract) {
        final boolean removed = warningContracts.remove(contract.getSymbol());
        if (removed) {
            noInfoMsg = String.join(DelimiterUtil.MESSAGE_SEPARATOR, warningContracts);
            heartbeat.setWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS, noInfoMsg);
            datapoint.setWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS, noInfoMsg);
        }
        if (warningContracts.isEmpty()) {
            heartbeat.removeWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS);
            datapoint.removeWarnings(HeartbeatWarnings.MISSING_PRICE_LIMITS);
        }
    }

    private void checkConnectionWarnings(final ConnectionProbe probe) {
        if (probe.isConnected()) {
            heartbeat.removeWarnings(fromProbeType(probe.getConnectionType()));
        } else {
            heartbeat.setWarnings(fromProbeType(probe.getConnectionType()), probe.getConnectionKey());
        }
    }

    private void checkFeedWarnings(final long lastNumEvents, final long currentNumEvents, final String exchange) {
        if (lastNumEvents == currentNumEvents) {
            heartbeat.setWarnings(HeartbeatWarnings.QUOTE_NO_UPDATE, toQuoteWarnings(exchange));
        } else {
            final boolean removed = warningExchanges.remove(exchange);
            if (removed) {
                if (warningExchanges.isEmpty()) {
                    heartbeat.removeWarnings(HeartbeatWarnings.QUOTE_NO_UPDATE);
                } else {
                    heartbeat.setWarnings(HeartbeatWarnings.QUOTE_NO_UPDATE, rebuildQuoteWarnings());
                }
            }
        }
    }

    private String toQuoteWarnings(final String exchange) {
        if (warningExchanges.contains(exchange)) {
            return noFeedMsg;
        }
        warningExchanges.add(exchange);
        noFeedMsg = String.join(DelimiterUtil.MESSAGE_SEPARATOR, warningExchanges);
        return noFeedMsg;
    }

    private String rebuildQuoteWarnings() {
        noFeedMsg = String.join(DelimiterUtil.MESSAGE_SEPARATOR, warningExchanges);
        return noFeedMsg;
    }

    private void updateQuoteWarnings() {
        noFeedByTypeMsg = String.join(DelimiterUtil.MESSAGE_SEPARATOR,
            warningExchangeWithType.entrySet().stream().map(e -> e.getKey() + DelimiterUtil.CONTRACT_DESCRIPTOR_DELIMETER + Arrays.toString(e.getValue()))
                .collect(Collectors.toList()));
        if (warningExchangeWithType.isEmpty()) {
            datapoint.removeWarnings(HeartbeatWarnings.QUOTE_NO_UPDATE);
        } else {
            datapoint.setWarnings(HeartbeatWarnings.QUOTE_NO_UPDATE, noFeedByTypeMsg);
        }
    }

    private HeartbeatWarnings fromProbeType(final ConnectionType connectionType) {
        if (ConnectionType.TRADE.equals(connectionType)) {
            return HeartbeatWarnings.TRADE_NOT_CONNECTED;
        }
        return HeartbeatWarnings.QUOTE_NOT_CONNECTED;
    }

}
