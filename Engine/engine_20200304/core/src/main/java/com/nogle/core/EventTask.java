package com.nogle.core;

import java.util.concurrent.ConcurrentLinkedDeque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.strategy.api.Strategy;
import com.timelinecapital.core.warmup.LoaderLock;

import net.openhft.affinity.Affinity;
import net.openhft.affinity.AffinityLock;

public abstract class EventTask implements Runnable {
    private final static Logger log = LogManager.getLogger(EventTask.class);

    protected final ConcurrentLinkedDeque<EventHandler> eventQueue = new ConcurrentLinkedDeque<>();
    protected final Class<?> clazz;
    protected final int id;
    protected final String name;
    protected boolean stop;
    protected final String taskName;

    private boolean isWarmupAllowed;
    private LoaderLock loaderLock;
    private EventHandler pendingEventHandler;
    private StrategyUpdater strategyUpdater;

    public EventTask(final Class<?> clazz, final int id, final String name) {
        stop = false;
        taskName = id + "-" + name;
        this.clazz = clazz;
        this.id = id;
        this.name = name;
    }

    public abstract void start();

    public abstract void shutdown();

    public String getTaskName() {
        return taskName;
    }

    public void setLoaderLock(final LoaderLock loaderLock, final boolean isWarmupAllowed) {
        this.loaderLock = loaderLock;
        this.isWarmupAllowed = isWarmupAllowed;
    }

    public void onTradeWarmup(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    public void onPendingInvoke(final EventHandler pendingEventHandler) {
        this.pendingEventHandler = pendingEventHandler;
    }

    public void invoke(final EventHandler eventHandler) {
        eventQueue.add(eventHandler);
    }

    public void invokeNow(final EventHandler eventHandler) {
        eventQueue.addFirst(eventHandler);
    }

    public void invokeLast(final EventHandler eventHandler) {
        eventQueue.addLast(eventHandler);
    }

    @Override
    public void run() {
        if (!clazz.isAssignableFrom(Strategy.class)) {
            log.error("[EventThread] Unable to initiate task thread for class: {}", clazz);
            return;
        }

        Thread.currentThread().setName(Strategy.class.getSimpleName() + "-" + id);
        log.warn("[EventThread] ******** CREATE new Thread : {} ********", Thread.currentThread().getName());

        log.warn("[EventThread] ******** START new Thread : {} ********", Thread.currentThread().getName());
        ThreadContext.put("ROUTINGKEY", taskName);
        try (AffinityLock lock = AffinityLock.acquireLock()) {
            log.info("[EventThread] Accuired CPU: {} ThreadId: {} ", Affinity.getCpu(), Affinity.getThreadId());
            log.info("[EventThread] CurrentLocks:\n{}", AffinityLock.dumpLocks());

            EventHandler handler = null;
            while (!stop) {
                try {
                    handler = eventQueue.poll();
                    if (handler != null) {
                        handler.handle();
                    }
                } catch (final Exception e) {
                    log.error("EventQueue with exceptions", e);
                }
            }
        }

        log.warn("[{}] has been stopped...", Thread.currentThread().getName());
    }

}
