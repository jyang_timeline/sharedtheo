package com.nogle.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.TimeoutException;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.nogle.core.config.EngineThreadPolicy;
import com.nogle.core.controller.Commander;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.core.strategy.event.StrategyEvent;
import com.nogle.core.strategy.event.StrategyEventHandler;
import com.nogle.core.strategy.event.StrategyEventTranslator;
import com.nogle.strategy.api.Strategy;
import com.nogle.util.RoundRobinWrapper;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.util.DisruptorUtil;
import com.timelinecapital.core.util.TaskNotifier;
import com.timelinecapital.core.util.TaskThreadFactory;

public class EventTaskFactory {
    private static final Logger log = LogManager.getLogger(EventTaskFactory.class);

    private static final int SLEEP_MILLIS_BETWEEN_DRAIN_ATTEMPTS = 1;
    private static final int MAX_DRAIN_ATTEMPTS_BEFORE_SHUTDOWN = 200;

    private static final RoundRobinWrapper<EventTask> roundRobinEventTask;
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private static final Map<Integer, EventTask> idToEventTask;

    private static EventTask systemTask = null;

    static {
        idToEventTask = new ConcurrentHashMap<>();
        if (EngineThreadPolicy.POOL.equals(EngineConfig.getThreadPolicy())) {
            final int threadCount = EngineConfig.getThreadPoolCount();
            for (int count = 0; count < threadCount; ++count) {
                idToEventTask.put(count, new SharedStrategyEventTask(Thread.class, count, "TaskPool"));
            }
            roundRobinEventTask = new RoundRobinWrapper<EventTask>(idToEventTask.values().toArray(new EventTask[idToEventTask.size()]));
        } else {
            roundRobinEventTask = null;
        }
    }

    public static EventTask getSystemTask() {
        if (systemTask == null) {
            systemTask = new SystemEventTask(Commander.class, EngineConfig.getEngineId(), Commander.class.getSimpleName());
        }
        return systemTask;
    }

    public static EventTask get(final Class<?> clazz, final int id, final String name) {
        if (EngineThreadPolicy.POOL.equals(EngineConfig.getThreadPolicy())) {
            return roundRobinEventTask.next();
        } else {
            EventTask strategyEventTask;
            if (EngineThreadPolicy.RING.equals(EngineConfig.getThreadPolicy())) {
                strategyEventTask = new RingEventTask(clazz, id, name);
            } else {
                strategyEventTask = new DedicatedEventTask(clazz, id, name);
            }
            idToEventTask.put(id, strategyEventTask);
            return strategyEventTask;
        }
    }

    public static void publish(final int strategyId, final EventHandler eventHandler) {
        final EventTask strategyEventTask = idToEventTask.get(strategyId);
        if (strategyEventTask != null) {
            strategyEventTask.invoke(eventHandler);
        }
    }

    public static void shutdown(final int strategyId) {
        final EventTask strategyEventTask = idToEventTask.get(strategyId);
        if (strategyEventTask != null) {
            strategyEventTask.shutdown();
            idToEventTask.remove(strategyId);
        }
    }

    private static class RingEventTask extends EventTask {
        private final Disruptor<StrategyEvent> disruptor;
        private final StrategyEventTranslator translator;
        private final StrategyEventHandler handler;

        RingEventTask(final Class<?> clazz, final int id, final String name) {
            super(clazz, id, name);
            disruptor = new Disruptor<>(
                StrategyEvent.FACTORY,
                DisruptorUtil.calculateRingBufferSize(),
                TaskThreadFactory.createThreadFactory(Strategy.class.getSimpleName(), id),
                ProducerType.MULTI,
                DisruptorUtil.createWaitStrategy());
            translator = new StrategyEventTranslator();
            handler = new StrategyEventHandler();
        }

        @SuppressWarnings("unchecked")
        @Override
        public synchronized final void start() {
            disruptor.handleEventsWith(handler);
            disruptor.handleExceptionsFor(handler).with(DisruptorUtil.getStrategyEventExceptionHandler());
            disruptor.start();
        }

        @Override
        public final void shutdown() {
            log.warn("[{}] is shutting down. Releasing resources...", taskName);

            if (disruptor == null) {
                log.info("[{}] Disruptor: disruptor for this context already shut down.", taskName);
                return;
            }
            log.warn("[{}] Disruptor: shutting down disruptor for this context.", taskName);

            for (int i = 0; hasBacklog(disruptor) && i < MAX_DRAIN_ATTEMPTS_BEFORE_SHUTDOWN; i++) {
                try {
                    Thread.sleep(SLEEP_MILLIS_BETWEEN_DRAIN_ATTEMPTS); // give up the CPU for a while
                } catch (final InterruptedException e) { // ignored
                }
            }
            try {
                // busy-spins until all events currently in the disruptor have been processed
                disruptor.halt();
                disruptor.shutdown(10, TimeUnit.SECONDS);
            } catch (final TimeoutException e) {
                disruptor.halt();
                disruptor.shutdown();
            }

            // finally, kill the processor thread
            // final boolean isShutdown = ExecutorServices.shutdown(coreExecutor, 10, TimeUnit.SECONDS, toString());
            // log.warn("[{}] Disruptor: shutting down disruptor executor: {}", taskName, isShutdown);
            // coreExecutor = null;
        }

        /**
         * Returns {@code true} if the specified disruptor still has unprocessed events.
         */
        private static boolean hasBacklog(final Disruptor<?> theDisruptor) {
            final RingBuffer<?> ringBuffer = theDisruptor.getRingBuffer();
            return !ringBuffer.hasAvailableCapacity(ringBuffer.getBufferSize());
        }

        @Override
        public final void invoke(final EventHandler eventHandler) {
            disruptor.getRingBuffer().publishEvent(translator, eventHandler);
        }

        @Override
        public final void invokeNow(final EventHandler eventHandler) {
            disruptor.getRingBuffer().publishEvent(translator, eventHandler);
        }

        @Override
        public final void invokeLast(final EventHandler eventHandler) {
            disruptor.getRingBuffer().publishEvent(translator, eventHandler);
        }

        @Override
        public final void run() {
            log.warn("[RingEvent] ******** 'run' method should not be invoked directly {} !!! ********", Thread.currentThread().getName());
        }
    }

    private static class DedicatedEventTask extends EventTask {
        DedicatedEventTask(final Class<?> clazz, final int strategyId, final String strategyName) {
            super(clazz, strategyId, strategyName);
        }

        @Override
        public final void start() {
            executor.execute(this);
        }

        @Override
        public final void shutdown() {
            log.warn("[{}] is shutting down. Releasing resources...", Thread.currentThread().getName());
            stop = true;
        }
    }

    private static class SharedStrategyEventTask extends EventTask {
        SharedStrategyEventTask(final Class<?> clazz, final int strategyId, final String strategyName) {
            super(clazz, strategyId, strategyName);
        }

        @Override
        public final void start() {
        }

        @Override
        public final void shutdown() {
            log.warn("[{}] is shutting down.", Thread.currentThread().getName());
        }
    }

    private static class SystemEventTask extends EventTask {
        private final TaskNotifier taskNotifier = new TaskNotifier();

        SystemEventTask(final Class<?> clazz, final int id, final String name) {
            super(clazz, id, name);
        }

        @Override
        public final void invoke(final EventHandler eventHandler) {
            eventQueue.add(eventHandler);
            taskNotifier.doNotify();
        }

        @Override
        public final void invokeNow(final EventHandler eventHandler) {
            eventQueue.addFirst(eventHandler);
            taskNotifier.doNotify();
        }

        @Override
        public final void invokeLast(final EventHandler eventHandler) {
            eventQueue.addLast(eventHandler);
            taskNotifier.doNotify();
        }

        @Override
        public final void start() {
            executor.execute(this);
        }

        @Override
        public final void shutdown() {
            log.warn("SystemTask thread is going down. Releasing resources...");
            stop = true;
        }

        @Override
        public final void run() {
            Thread.currentThread().setName(clazz.getSimpleName() + "-" + id);

            log.warn("[EventThread] ******** START SystemTask Thread : {} ********", Thread.currentThread().getName());
            EventHandler handler = null;
            while (!stop) {
                try {
                    while (!eventQueue.isEmpty()) {
                        handler = eventQueue.poll();
                        if (handler != null) {
                            handler.handle();
                        }
                    }
                    taskNotifier.doWait();
                } catch (final Exception e) {
                    log.error("EventQueue with exceptions", e);
                }
            }
        }
    }

}
