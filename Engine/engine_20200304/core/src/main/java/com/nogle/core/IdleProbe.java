package com.nogle.core;

public class IdleProbe implements Probe {

    @Override
    public void showQuoteToTrade(final long clOrdId, final String requestType, final long sourceTime) {
    }

    @Override
    public void showQuoteToTrade(final long clOrdId, final String requestType, final long sourceTime, final long commitTime, final long enqueueTime, final long launchTime) {
    }

    @Override
    public void showQuoteToTradeFootprint(final long quoteTimeMicros, final long tiggerMicros, final long sendingMicros, final long clOrdId, final String requestType) {
    }

}
