package com.nogle.core;

import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.util.Unbox;

import com.nogle.core.config.EngineMode;
import com.nogle.core.util.AccurateClock;
import com.nogle.core.util.MicrosecondClock;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.core.config.EngineConfig;

public class LatencyProbe implements Probe {
    private static final Logger log = LogManager.getLogger(LatencyProbe.class);

    final AccurateClock clock;

    public LatencyProbe() {
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            // running in Simulation. Use historical data as Clock
            clock = new AccurateClock() {

                @Override
                public long getPreciseTime() {
                    return TradeClock.getCurrentMicros();
                }
            };
        } else if (SystemUtils.IS_OS_WINDOWS) {
            // running in Windows platform. Use system clock
            clock = new AccurateClock() {

                @Override
                public long getPreciseTime() {
                    return System.currentTimeMillis();
                }
            };
        } else {
            // running in Linux platform. Use c/JNI clock
            clock = new AccurateClock() {
                MicrosecondClock msClock = new MicrosecondClock();

                @Override
                public long getPreciseTime() {
                    return msClock.getMicrosecond();
                }
            };
        }

    }

    @Override
    public void showQuoteToTrade(final long clOrdId, final String requestType, final long sourceTime) {
        final long completedTime = TradeClock.getCurrentMicrosOnly();
        log.debug("[TraceLatency] quoteTime {} clOrdId {} type {} TCP {}", sourceTime, clOrdId, requestType, completedTime);
    }

    @Override
    public void showQuoteToTrade(final long clOrdId, final String requestType, final long sourceTime, final long commitTime, final long enqueueTime, final long launchTime) {
        final long completedTime = TradeClock.getCurrentMicrosOnly();
        log.debug("[TraceLatency] quoteTime {} clOrdId {} type {} EVENT {} QUEUE {} -> {} -> {}", sourceTime, clOrdId, requestType,
            commitTime, enqueueTime, launchTime, completedTime);
    }

    @Override
    public void showQuoteToTradeFootprint(final long quoteTimeMicros, final long tiggerMicros, final long sendingMicros, final long clOrdId, final String requestType) {
        log.debug("[TraceLatency] quoteTime {} tiggerTime {} sendingTime {} clOrdId {} type {}", quoteTimeMicros, tiggerMicros, sendingMicros, clOrdId, requestType);
    }

}
