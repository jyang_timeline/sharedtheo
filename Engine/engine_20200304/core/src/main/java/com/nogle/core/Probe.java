package com.nogle.core;

public interface Probe {

    void showQuoteToTrade(long clOrdId, String requestType, long sourceTime);

    void showQuoteToTrade(long clOrdId, String requestType, long sourceTime, long commitTime, long enqueueTime, long launchTime);

    void showQuoteToTradeFootprint(long quoteTimeMicros, long tiggerMicros, long sendingMicros, long clOrdId, String requestType);

}
