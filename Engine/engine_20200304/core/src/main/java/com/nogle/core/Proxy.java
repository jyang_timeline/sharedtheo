package com.nogle.core;

public interface Proxy {

    boolean connect();

}
