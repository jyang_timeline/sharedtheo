package com.nogle.core;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.exception.DataParserException;
import com.nogle.core.exception.InvalidDateIntervalException;
import com.nogle.core.exception.InvalidSymbolException;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.market.SimulatedMarket;
import com.nogle.core.strategy.SimulatedStrategyLoader;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.config.SimulatedTimeFrame;
import com.nogle.core.util.PnlFormatter;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionView;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.types.SessionIdentifier;
import com.timelinecapital.core.util.TradingAccountHelper;
import com.timelinecapital.core.util.TradingHoursHelper;

public class SimulationTask implements Runnable {
    private static final Logger log = LogManager.getLogger(SimulationTask.class);

    private final Map<String, List<SessionIdentifier>> exchangeSessions = new HashMap<>();

    private final Map<String, Double> strategyToPnl = new HashMap<>();
    private final List<SimulationResult> simResults = new ArrayList<>();

    private final int strategyId;
    private final StrategyConfig strategyConfig;
    private final SimulatedMarket simMarket;
    private final SimulatedStrategyLoader simStrategyLoader;

    @SuppressWarnings("unused")
    private Map<String, Map<String, Instrument>> tradingDayToInstruments;

    public SimulationTask(final int strategyId, final StrategyConfig strategyConfig, final SimulatedMarket simMarket) {
        this.strategyId = strategyId;
        this.strategyConfig = strategyConfig;
        this.simMarket = simMarket;
        simStrategyLoader = new SimulatedStrategyLoader(simMarket, new HashMap<>());
    }

    @Override
    public void run() {
        CoreController.setStrategyLoader(simStrategyLoader);
        Thread.currentThread().setName(Strategy.class.getSimpleName() + "-" + strategyId);
        ThreadContext.put("ROUTINGKEY", strategyId + "-" + strategyConfig.getName());

        log.info("Simulation of strategy {} from {} to {}\n",
            strategyConfig.getName(),
            SimulationConfig.getStartDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT),
            SimulationConfig.getEndDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT));

        init();
        double totalPnl = 0.0d;
        try {
            // onContractQuery();

            for (final SimulatedTimeFrame timeFrame : getSimulateDates()) {
                TradeClock.setTradingDay(timeFrame.getTradingDate().getMillis());
                TradingHoursHelper.updateCoreSession(timeFrame.getTradingDate().getMillis(), exchangeSessions);
                TradingHoursHelper.updateAuctionSession(timeFrame.getTradingDate().getMillis(), exchangeSessions);
                TradingHoursHelper.updateAuctionsFreezeSession(timeFrame.getTradingDate().getMillis(), exchangeSessions);

                if (timeFrame.getTradingDate().getDayOfWeek() == DateTimeConstants.SATURDAY || timeFrame.getTradingDate().getDayOfWeek() == DateTimeConstants.SUNDAY) {
                    log.warn("Simulation of strategy {} Skip: {} weekend", strategyConfig.getName(), timeFrame.getTradingDay());
                    continue;
                }
                // final Map<String, Contract> contracts = tradingDayToContracts.get(timeFrame.getTradingDay());
                // if (contracts.isEmpty()) {
                // log.warn("Simulation of strategy {} Skip: {} NO ContractInfo", strategyConfig.getName(),
                // timeFrame.getTradingDay());
                // continue;
                // }

                final Map<String, Instrument> instruments = new HashMap<>();
                for (final Map.Entry<String, String> entry : strategyConfig.getStrategyContracts().entrySet()) {
                    final Instrument instrument = ContractFactory.getByTradingDay(entry.getValue(), TradeClock.getTradingDay());
                    instruments.put(entry.getKey(), instrument);
                }
                instruments.forEach((k, v) -> log.info("{} @{} Contract: {} ({})", strategyConfig.getName(), timeFrame.getTradingDay(), v, k));

                // for (final Entry<String, Contract> entry : contracts.entrySet()) {
                // contractDecorators.put(entry.getKey(), new ContractDecorator(entry.getValue(),
                // ContractFactory.getOrderViewType(entry.getKey())));
                // }

                try {
                    final Map<FeedType, List<Contract>> feeds = onExtraFeed(instruments);

                    /*
                     * Prepare historical data: MarketData and FeedData
                     */
                    final Map<Contract, File[]> marketDataFiles = simMarket.getPriceFeedSources(timeFrame, instruments);
                    final Map<Contract, File[]> feedDataFiles = simMarket.getAdditionalPriceFeedSources(timeFrame, feeds);

                    /*
                     * Check whether there is any duplicate file
                     */
                    final List<File> marketFiles = marketDataFiles.values().stream().flatMap(f -> Arrays.stream(f)).collect(Collectors.toList());
                    final List<File> feedFiles = feedDataFiles.values().stream().flatMap(f -> Arrays.stream(f)).collect(Collectors.toList());
                    final Set<File> dupFiles = feedFiles.stream().distinct().filter(marketFiles::contains).collect(Collectors.toSet());
                    if (!dupFiles.isEmpty()) {
                        log.warn("Skipped simulation task due to duplicated files {} ", dupFiles);
                        continue;
                    }
                    if (marketDataFiles.isEmpty() && feedFiles.isEmpty()) {
                        log.warn("Skipped simulation task due to nothing to run {} ", marketFiles);
                        continue;
                    }

                    /*
                     * To publish data and do simulation
                     */
                    simMarket.prepareQuotes(marketDataFiles, feedDataFiles);
                    simStrategyLoader.buildStrategy(TradingAccountHelper.getAccountInstance(StringUtils.EMPTY), strategyId, strategyConfig, instruments);
                    simMarket.buildExchangeSchedules(exchangeSessions);
                    simMarket.publishQuotes();

                    for (final StrategyUpdater strategyUpdater : simStrategyLoader.getStrategyUpdaters()) {
                        double pnl = 0.0d;
                        for (final Instrument instrument : instruments.values()) {
                            pnl += getContractPnL(strategyUpdater.getTradeServices().getPositionView(instrument));
                        }
                        totalPnl = getCumulativePNL(strategyUpdater.getStrategyName(), pnl);

                        simResults.add(new SimulationResult(
                            strategyConfig.getName(),
                            TradeClock.getTradingDay(),
                            SimulationStatus.Success,
                            strategyUpdater.getStrategyStatusView().getFinalReport()));

                        TradeClock.setCurrentMillis(timeFrame.getClosingTime());
                        log.info("{}\tPNL: {}", strategyConfig.getName(), PnlFormatter.format(pnl));
                    }

                } catch (final InvalidSymbolException e) {
                    simResults.add(new SimulationResult(strategyConfig.getName(), TradeClock.getTradingDay(),
                        SimulationStatus.ContractNotFound, e.getMessage()));
                    log.warn(e.getMessage());
                } catch (final DataParserException e) {
                    simResults.add(new SimulationResult(strategyConfig.getName(), TradeClock.getTradingDay(),
                        SimulationStatus.FileNotFound, e.getMessage()));
                    log.warn(e.getMessage());
                } catch (final StrategyException e) {
                    simResults.add(new SimulationResult(strategyConfig.getName(), TradeClock.getTradingDay(),
                        SimulationStatus.BuildFailed, e.getMessage()));
                    log.error(e.getMessage(), e);
                }

                simStrategyLoader.shutdownStrategy(null);
                simMarket.cleanup();
            }

            TradeClock.setCurrentMillis(System.currentTimeMillis());
            for (final SimulationResult simResult : simResults) {
                log.info("{} @{}\t{}: {}", simResult.getStrategyName(), simResult.getTradingDay(), simResult.getStatus(),
                    simResult.getResult());
            }
            log.info("Total PNL from {} to {} : {}",
                SimulationConfig.getStartDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT),
                SimulationConfig.getEndDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT),
                PnlFormatter.format(totalPnl));

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private void init() {
        strategyToPnl.clear();
        simResults.clear();
    }

    void onContractQuery() {
        tradingDayToInstruments = ContractFactory.getByTradingDays(
            strategyConfig.getStrategyContracts(),
            SimulationConfig.getStartDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT),
            SimulationConfig.getEndDate().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT));
    }

    /*
     * Build contracts that requires additional data from strategy configs
     */
    private Map<FeedType, List<Contract>> onExtraFeed(final Map<String, Instrument> contractDecorators) {
        final Map<FeedType, List<Contract>> feeds = new HashMap<>();
        for (final Entry<String, String> entry : strategyConfig.getAdditionalDataSubscriber().entrySet()) {
            final String[] codeNames = entry.getValue().split(DelimiterUtil.CONTRACT_DELIMETER);
            final List<Contract> available = contractDecorators.entrySet().stream()
                .filter(
                    e -> (Arrays.stream(codeNames).filter(d -> StringUtils.substringAfter(e.getKey(), DelimiterUtil.CONTRACT_DESCRIPTOR_DELIMETER).contentEquals(d)).count()) == 1)
                .map(e -> e.getValue())
                .collect(Collectors.toList());
            feeds.put(FeedType.valueOf(entry.getKey()), available);
        }
        return feeds;
    }

    // private void onOrderViewType(final Map<String, Contract> contractMap) {
    // contractToOrderViewType.clear();
    // for (final Entry<String, Contract> entry : contractMap.entrySet()) {
    // contractToOrderViewType.put(entry.getValue(), ContractFactory.getOrderViewType(entry.getKey()));
    // }
    // }

    private SimulatedTimeFrame[] getSimulateDates() throws InvalidDateIntervalException {
        final DateTime start = SimulationConfig.getStartDate();
        final DateTime end = SimulationConfig.getEndDate();
        if (start.isAfter(end)) {
            throw new InvalidDateIntervalException("Incorrect interval between start date and end date");
        }
        if (EngineConfig.isKeepPositionOverNight()) {
            return new SimulatedTimeFrame[] { new SimulatedTimeFrame(start, end) };
        } else {
            final int days = Days.daysBetween(start, end).getDays() + 1;
            final SimulatedTimeFrame[] dates = new SimulatedTimeFrame[days];
            for (int i = 0; i < days; i++) {
                final DateTime dt = start.withFieldAdded(DurationFieldType.days(), i);
                dates[i] = new SimulatedTimeFrame(dt, dt);
            }
            return dates;
        }
    }

    private double getCumulativePNL(final String strategyName, double pnl) {
        final Double cumulative = strategyToPnl.get(strategyName);
        if (cumulative != null) {
            pnl += cumulative;
        }
        strategyToPnl.put(strategyName, pnl);
        return pnl;
    }

    // private double getDailyPNL(final StrategyUpdater strategyUpdater) {
    // double pnl = 0.0d;
    // for (final PositionView positionView : positionViews) {
    // pnl += positionView.getClosedPnl() - positionView.getCommission();
    // }
    // return pnl;
    // }

    private double getContractPnL(final PositionView positionView) {
        return positionView.getClosedPnl() - positionView.getCommission();
    }

    private enum SimulationStatus {
        Success,
        ContractNotFound,
        FileNotFound,
        BuildFailed;
    }

    private class SimulationResult {
        String strategyName;
        String tradingDay;
        SimulationStatus status;
        String result;

        public SimulationResult(final String strategyName, final String tradingDay, final SimulationStatus status, final String result) {
            this.strategyName = strategyName;
            this.tradingDay = tradingDay;
            this.status = status;
            this.result = result;
        }

        public String getStrategyName() {
            return strategyName;
        }

        public String getTradingDay() {
            return tradingDay;
        }

        public SimulationStatus getStatus() {
            return status;
        }

        public String getResult() {
            return result;
        }

    }

}
