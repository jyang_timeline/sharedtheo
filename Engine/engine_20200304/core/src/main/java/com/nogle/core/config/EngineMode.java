package com.nogle.core.config;

import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;

public enum EngineMode {
    PRODUCTION(1) {

        @Override
        public String getExchangeTimeFormat(final String exchange) {
            return EngineConfig.getExchangeTimeFormat(exchange);
        }

        @Override
        public SbeVersion getSchemaVersion() {
            return EngineConfig.getSchemaVersion();
        }

    },
    TESTING(2) {

        @Override
        public String getExchangeTimeFormat(final String exchange) {
            return EngineConfig.getExchangeTimeFormat(exchange);
        }

        @Override
        public SbeVersion getSchemaVersion() {
            return EngineConfig.getSchemaVersion();
        }

    },
    SIMULATION(4) {

        @Override
        public String getExchangeTimeFormat(final String exchange) {
            return SimulationConfig.getExchangeTimeFormat(exchange);
        }

        @Override
        public SbeVersion getSchemaVersion() {
            return SbeVersion.fromInt(SimulationConfig.getHistoricalDataEncoderVersion());
        }

    };

    public abstract String getExchangeTimeFormat(String exchange);

    public abstract SbeVersion getSchemaVersion();

    private int intValue;

    EngineMode(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }
}
