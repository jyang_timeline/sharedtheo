package com.nogle.core.config;

public enum EngineThreadPolicy {
    DEDICATE("DEDICATE"),
    POOL("POOL"),
    RING("RING"),
    ;

    private String stringValue;

    EngineThreadPolicy(final String stringValue) {
        this.stringValue = stringValue;
    }

    static public EngineThreadPolicy fromString(final String stringValue) {
        if (stringValue.contentEquals(POOL.stringValue)) {
            return POOL;
        }
        if (stringValue.contentEquals(RING.stringValue)) {
            return RING;
        }
        return DEDICATE;
    }

    public String getString() {
        return stringValue;
    }
}
