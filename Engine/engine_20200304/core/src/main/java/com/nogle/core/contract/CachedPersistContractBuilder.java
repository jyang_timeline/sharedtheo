package com.nogle.core.contract;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.utils.SymbolQueryForm;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FillType;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.util.SecurityTypeHelper;

public class CachedPersistContractBuilder extends ContractBuilder {
    private static final Logger log = LogManager.getLogger(CachedPersistContractBuilder.class);

    private final String cacheDirectory = SimulationConfig.getPersistCacheDirectory();
    private final ContractBuilder inquiryHelper;

    private Symbol symbol;
    private FeeCalculator feeCalculator;

    private String query_key;

    public CachedPersistContractBuilder(final ContractBuilder inquiryHelper) {
        log.info("CachedPersistContractBuilder");
        this.inquiryHelper = inquiryHelper;

        feeCalculator = new FeeCalculator() {

            @Override
            public double getFee(final long quantity, final double price, final FillType fillType) {
                return 0;
            }
        };
    }

    @Override
    public Instrument getByName(final String symbolName, final String exchange, final String day) {
        log.info("getContractByName: symbol={} exchange={} day={}", symbolName, exchange, day);
        symbol = null;

        query_key = exchange + "_" + symbolName;
        if ((symbol = getByNameFromCache(day, query_key)) != null) {
            return symbol;
        }

        final Instrument instrument = inquiryHelper.getByName(symbolName, exchange, day);

        setByGroupToCache(day, query_key,
            "", // wad productID
            symbolName,
            exchange,
            instrument.getProductGroup(),
            instrument.getTickCalculator().toPrice(1),
            instrument.getTickCalculator().toValue(1, 1),
            instrument.getLimitUp(),
            instrument.getLimitDown());

        return instrument;
    }

    @Override
    public Instrument getIndices(final String codeName, final String exchange, final String day) {
        log.info("getIndices: symbol={} exchange={} day={}", codeName, exchange, day);
        symbol = null;

        query_key = exchange + "_" + codeName;
        if ((symbol = getByNameFromCache(day, query_key)) != null) {
            return symbol;
        }

        final Instrument instrument = inquiryHelper.getByName(codeName, exchange, day);

        setByGroupToCache(day, query_key,
            "",
            codeName,
            exchange,
            instrument.getProductGroup(),
            instrument.getTickCalculator().toPrice(1),
            instrument.getTickCalculator().toValue(1, 1),
            instrument.getLimitUp(),
            instrument.getLimitDown());

        return instrument;
    }

    @Override
    public Instrument getByGroup(final String productGroup, final String type, final String exchange, final String day) {
        log.info("getContractByGroup: group={} type={} exchange={} day={}", productGroup, type, exchange, day);
        symbol = null;

        query_key = exchange + "_" + productGroup + "_" + type;
        if ((symbol = getByNameFromCache(day, query_key)) != null) {
            return symbol;
        }

        final Instrument instrument = inquiryHelper.getByGroup(productGroup, type, exchange, day);

        setByGroupToCache(day, query_key,
            "",
            instrument.getSymbol(),
            exchange,
            instrument.getProductGroup(),
            instrument.getTickCalculator().toPrice(1),
            instrument.getTickCalculator().toValue(1, 1),
            instrument.getLimitUp(),
            instrument.getLimitDown());

        return instrument;
    }

    @Override
    public Instrument getByGroupRank(final String productGroup, final String rank, final String rankType, final String exchange, final String day) {
        log.info("getContractByRank: group={} rank={} rankType={} exchange={} day={}", productGroup, rank, rankType, exchange, day);
        symbol = null;

        query_key = exchange + '_' + productGroup + "," + rank + "," + rankType;
        if ((symbol = getByNameFromCache(day, query_key)) != null) {
            return symbol;
        }

        final Instrument instrument = inquiryHelper.getByGroupRank(productGroup, rank, rankType, exchange, day);

        setByGroupToCache(day, query_key,
            "",
            instrument.getSymbol(),
            exchange,
            instrument.getProductGroup(),
            instrument.getTickCalculator().toPrice(1),
            instrument.getTickCalculator().toValue(1, 1),
            instrument.getLimitUp(),
            instrument.getLimitDown());

        return instrument;
    }

    @Override
    FeeCalculator get(final Contract contract, final String day) {
        return feeCalculator;
    }

    Symbol getByNameFromCacheInternal(final String cachefile) {
        try (Scanner scanner = new Scanner(new File(cachefile))) {
            while (scanner.hasNext()) {
                final String kv = scanner.nextLine();
                final String[] fields = kv.split(",");
                log.info("cache hit: {}", query_key);
                symbol = new Symbol(
                    fields[0],
                    fields[1],
                    fields[2],
                    fields[3],
                    new FixedTickCalculator(Double.parseDouble(fields[4]), Double.parseDouble(fields[5])),
                    SecurityTypeHelper.getSecurityType(fields[1], fields[2]),
                    Double.parseDouble(fields[6]), // limitUp
                    Double.parseDouble(fields[7]) // limitDown
                );
                return symbol;
            }
        } catch (final IOException e) {
        }

        return null;
    }

    Symbol getByNameFromCache(final String day, final String query_key) {
        Symbol res;
        res = getByNameFromCacheInternal(Paths.get(cacheDirectory, day, query_key).toString());
        if (res != null) {
            return res;
        }

        log.info("Cannot read cache {}, use default value", query_key);
        return getByNameFromCacheInternal(Paths.get(cacheDirectory, "default", query_key).toString());
    }

    void setByGroupToCache(
        final String day,
        final String key,
        final String productId,
        final String symbol,
        final String exchange,
        final String productGroup,
        final double tickSize,
        final double contractUnit,
        final double limitUp,
        final double limitDown) {

        if (getByNameFromCache(day, key) != null) {
            log.info("someone already updated cache: key={}.  Skipped update.", key);
            return;
        }

        final String val = String.format("%s,%s,%s,%s,%f,%f,%f,%f\n",
            productId,
            symbol,
            exchange,
            productGroup,
            tickSize,
            contractUnit,
            limitUp,
            limitDown);

        log.info("add cache: key={}, val={}", key, val);
        final File directory = new File(Paths.get(cacheDirectory, day).toString());
        if (!directory.exists()) {
            directory.mkdir();
        }
        final String cachefile = Paths.get(cacheDirectory, day, query_key).toString();
        try {
            final File file = new File(cachefile);
            // Creates a random access file stream to read from, and optionally to write to
            final RandomAccessFile rafile = new RandomAccessFile(file, "rw");
            final FileChannel channel = rafile.getChannel();
            // Acquire an exclusive lock on this channel's file (blocks until lock can be retrieved)
            FileLock lock;
            if ((lock = channel.tryLock()) != null) {
                rafile.seek(rafile.length());
                rafile.write(val.getBytes());
                lock.release();
            }
            channel.close();
            rafile.close();
        } catch (final IOException e) {
            log.error("cannot write to cache file: {}", cachefile);
        }
    }

    @Override
    public Map<String, Map<String, Instrument>> getAllByInterval(final Collection<SymbolQueryForm> queryInfos, final String startDay, final String endDay) {
        return Collections.emptyMap();
    }

    @Override
    public void refresh(final Collection<Instrument> instruments) {
    }

    @Override
    public Endpoints getFeedEndpoints(final String exchange, final String protocol) {
        return null;
    }

    @Override
    public Endpoints getTradeEndpoints(final String account, final String protocol) {
        return null;
    }
}
