package com.nogle.core.contract;

import org.apache.commons.lang3.StringUtils;

import com.nogle.core.config.EngineMode;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.StockFeeCalculator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.TickCalculator;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.instrument.SecurityType;
import com.timelinecapital.core.util.FeeHelper;

public abstract class ContractBuilder implements ContractInquiry {

    public FeeCalculator getFeeCalculator(final Contract contract, final String day) {
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode()) &&
            (FeeHelper.isEquityTrading(contract.getExchange()) || FeeHelper.isEquityTrading(ContractInquiry.getTargetExchange()))) {
            return new StockFeeCalculator(SimulationConfig.getCommissionInfo(SimulationConfig.getTargetBroker(), Exchange.valueOf(contract.getExchange())));
        }
        return get(contract, day);
    }

    abstract FeeCalculator get(Contract contract, String day);

    public abstract Endpoints getFeedEndpoints(String exchange, String protocol);

    public abstract Endpoints getTradeEndpoints(String account, String protocol);

    public Instrument getSelfDefinedContract(final String contractString) {
        final String[] details = contractString.split("@");
        return new Symbol(StringUtils.EMPTY, details[0], details[1], StringUtils.EMPTY, new FixedTickCalculator(1, 1), SecurityType.FUTURES, Double.NaN, Double.NaN);
    }

    class Symbol implements Instrument {
        private final String productId;
        private final String symbol;
        private final String exchange;
        private final String productGroup;
        private final SecurityType instrumentType;
        private final TickCalculator tickCalculator;
        private double limitUp;
        private double limitDown;

        private final int hashCode;

        public Symbol(final String productId, final String symbol, final String exchange, final String productGroup, final TickCalculator tickCalculator,
            final SecurityType instrumentType, final double limitUp, final double limitDown) {
            this.productId = productId;
            this.symbol = symbol;
            this.exchange = exchange;
            this.productGroup = productGroup;
            this.instrumentType = instrumentType;
            this.tickCalculator = tickCalculator;
            this.limitUp = limitUp;
            this.limitDown = limitDown;
            hashCode = buildHash();
        }

        private int buildHash() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (productGroup == null ? 0 : productGroup.hashCode());
            result = prime * result + (symbol == null ? 0 : symbol.hashCode());
            result = prime * result + (exchange == null ? 0 : exchange.hashCode());
            return result;
        }

        public String getProductId() {
            return productId;
        }

        @Override
        public String getSymbol() {
            return symbol;
        }

        @Override
        public String getBroker() {
            return EngineConfig.getBroker();
        }

        @Override
        public String getExchange() {
            return exchange;
        }

        @Override
        public String getProductGroup() {
            return productGroup;
        }

        @Override
        public SecurityType getSecurityType() {
            return instrumentType;
        }

        @Override
        public TickCalculator getTickCalculator() {
            return tickCalculator;
        }

        @Override
        public double getLimitUp() {
            return limitUp;
        }

        @Override
        public double getLimitDown() {
            return limitDown;
        }

        @Override
        public void updatePriceLimit(final double limitUp, final double limitDown) {
            this.limitUp = limitUp;
            this.limitDown = limitDown;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }

            return hashCode == obj.hashCode();
        }

        @Override
        public String toString() {
            return symbol;
        }
    }

}
