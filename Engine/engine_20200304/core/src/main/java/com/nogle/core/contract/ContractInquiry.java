package com.nogle.core.contract;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.nogle.commons.utils.SymbolQueryForm;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.instrument.Instrument;

public interface ContractInquiry {

    Instrument getByName(String symbol, String exchange, String date);

    Instrument getIndices(String codeName, String exchange, String date);

    Instrument getByGroup(String productGroup, String type, String exchange, String date);

    Instrument getByGroupRank(String productGroup, String rank, String rankType, String exchange, String date);

    Map<String, Map<String, Instrument>> getAllByInterval(Collection<SymbolQueryForm> queryInfos, String startDay, String endDay);

    void refresh(Collection<Instrument> instruments);

    static String getTargetExchange() {
        switch (EngineConfig.getEngineMode()) {
            case PRODUCTION:
            case TESTING:
            case SIMULATION:
                return EngineConfig.getTradeExchange();
        }
        return StringUtils.EMPTY;
    }

}
