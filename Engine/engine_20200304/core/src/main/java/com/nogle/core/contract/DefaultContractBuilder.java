package com.nogle.core.contract;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.nogle.commons.utils.SymbolQueryForm;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FillType;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.instrument.SecurityType;

public class DefaultContractBuilder extends ContractBuilder {

    private static final String DEFAULT_PRODUCT_ID = "0";
    private static final String DEFAULT_PRODUCTGROUP_STOCK = "STOCK";
    private static final String DUMMY_FUTURES_CODE = "1912";

    private final FixedTickCalculator calculator;
    private final FeeCalculator feeCalculator;

    public DefaultContractBuilder() {
        calculator = new FixedTickCalculator(1, 1);
        feeCalculator = new FeeCalculator() {

            @Override
            public double getFee(final long quantity, final double price, final FillType fillType) {
                return 0;
            }
        };
    }

    @Override
    public Instrument getByGroup(final String productGroup, final String type, final String exchange, final String date) {
        if (exchange.contentEquals(Exchange.SSE.name()) || exchange.contentEquals(Exchange.SZSE.name())) {
            return new Symbol(DEFAULT_PRODUCT_ID, StringUtils.EMPTY, exchange, productGroup, calculator, SecurityType.STOCKS, Double.NaN, Double.NaN);
        }
        return new Symbol(DEFAULT_PRODUCT_ID, productGroup + DUMMY_FUTURES_CODE, exchange, productGroup, calculator, SecurityType.FUTURES, Double.NaN, Double.NaN);
    }

    @Override
    public Instrument getByName(final String symbol, final String exchange, final String date) {
        if (exchange.contentEquals(Exchange.SSE.name()) || exchange.contentEquals(Exchange.SZSE.name())) {
            return new Symbol(DEFAULT_PRODUCT_ID, symbol, exchange, DEFAULT_PRODUCTGROUP_STOCK, calculator, SecurityType.STOCKS, Double.NaN, Double.NaN);
        }
        return new Symbol(DEFAULT_PRODUCT_ID, symbol, exchange, StringUtils.EMPTY, calculator, SecurityType.FUTURES, Double.NaN, Double.NaN);
    }

    @Override
    public Instrument getIndices(final String codeName, final String exchange, final String date) {
        return new Symbol(DEFAULT_PRODUCT_ID, codeName, exchange, DEFAULT_PRODUCTGROUP_STOCK, calculator, SecurityType.STOCKS, Double.NaN, Double.NaN);
    }

    @Override
    public Instrument getByGroupRank(final String productGroup, final String rank, final String rankType, final String exchange, final String date) {
        return new Symbol(DEFAULT_PRODUCT_ID, productGroup + DUMMY_FUTURES_CODE, exchange, productGroup, calculator, SecurityType.FUTURES, Double.NaN, Double.NaN);
    }

    @Override
    public Map<String, Map<String, Instrument>> getAllByInterval(final Collection<SymbolQueryForm> queryInfos, final String startDay, final String endDay) {
        return Collections.emptyMap();
    }

    @Override
    public void refresh(final Collection<Instrument> contracts) {
    }

    @Override
    FeeCalculator get(final Contract contract, final String day) {
        return feeCalculator;
    }

    @Override
    public Endpoints getFeedEndpoints(final String exchange, final String protocol) {
        return null;
    }

    @Override
    public Endpoints getTradeEndpoints(final String account, final String protocol) {
        return null;
    }

}