package com.nogle.core.contract;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.timelinecapital.commons.channel.FeedType;

public class Endpoints {
    private static final String EMPTY_ENDPOINT = "null";

    private final String snapshot;
    private final String marketBook;
    private final String tick;
    private final String trade;

    private final Map<FeedType, String> availableEndpoints = new HashMap<>();

    public Endpoints(final String snapshot, final String marketBook, final String tick, final String trade) {
        this.snapshot = snapshot;
        this.marketBook = marketBook;
        this.tick = tick;
        this.trade = trade;
        if (!StringUtils.isEmpty(snapshot) && !EMPTY_ENDPOINT.contentEquals(snapshot)) {
            availableEndpoints.put(FeedType.Snapshot, snapshot);
        }
        if (!StringUtils.isEmpty(marketBook) && !EMPTY_ENDPOINT.contentEquals(marketBook)) {
            availableEndpoints.put(FeedType.MarketBook, marketBook);
        }
        if (!StringUtils.isEmpty(tick) && !EMPTY_ENDPOINT.contentEquals(tick)) {
            availableEndpoints.put(FeedType.Trade, tick);
        }
    }

    public String getSnapshot() {
        return snapshot;
    }

    public String getMarketBook() {
        return marketBook;
    }

    public String getTick() {
        return tick;
    }

    public String getTrade() {
        return trade;
    }

    public Map<FeedType, String> get() {
        return availableEndpoints;
    }

}
