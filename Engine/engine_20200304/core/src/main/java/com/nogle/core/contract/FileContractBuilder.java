package com.nogle.core.contract;

import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import com.nogle.commons.utils.SymbolQueryForm;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.commons.utils.json.JsonUtil;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.FuturesFeeCalculator;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.instrument.SecurityType;
import com.timelinecapital.core.util.SecurityTypeHelper;

public class FileContractBuilder extends ContractBuilder {
    private final Properties properties;

    public FileContractBuilder(final InputStream inputStream) {
        this.properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Instrument getByGroup(final String productGroup, final String type, final String exchange, final String date) {
        final String key = productGroup + '@' + exchange;
        final String actualSymbol = properties.getProperty(key);
        if (actualSymbol != null) {
            return getByName(actualSymbol.split("@")[0], exchange, date);
        }
        return null;
    }

    @Override
    public Instrument getByName(final String symbol, final String exchange, final String date) {
        final String key = symbol + '@' + exchange;
        final String jsonString = properties.getProperty(key);

        if (jsonString != null) {
            try {
                final HFTJsonObject jsonObj = JsonUtil.parseJsonString(jsonString);

                if (jsonObj != null) {
                    final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(
                        jsonObj.getDouble("tickSize"),
                        jsonObj.getDouble("contractSize"));
                    final Symbol symbolObj =
                        new Symbol(jsonObj.getString("productId"), symbol, exchange, jsonObj.getString("productClass"), fixedTickCalculator,
                            SecurityTypeHelper.getSecurityType(symbol, exchange), Double.NaN, Double.NaN);
                    return symbolObj;
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Instrument getIndices(final String codeName, final String exchange, final String date) {
        final String key = codeName + '@' + exchange;
        final String jsonString = properties.getProperty(key);

        if (jsonString != null) {
            try {
                final HFTJsonObject jsonObj = JsonUtil.parseJsonString(jsonString);

                if (jsonObj != null) {
                    final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(
                        jsonObj.getDouble("tickSize"),
                        jsonObj.getDouble("contractSize"));
                    final Symbol symbolObj =
                        new Symbol(jsonObj.getString("productId"), codeName, exchange, jsonObj.getString("productClass"), fixedTickCalculator,
                            SecurityType.OPTIONS, Double.NaN, Double.NaN);
                    return symbolObj;
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Instrument getByGroupRank(final String productGroup, final String rank, final String rankType, final String exchange, final String date) {
        final String key = productGroup + '&' + rank + '@' + exchange;
        final String actualSymbol = properties.getProperty(key);
        if (actualSymbol != null) {
            return getByName(actualSymbol.split("@")[0], exchange, date);
        }
        return null;
    }

    @Override
    public Map<String, Map<String, Instrument>> getAllByInterval(final Collection<SymbolQueryForm> queryInfos, final String startDay, final String endDay) {
        return Collections.emptyMap();
    }

    @Override
    public void refresh(final Collection<Instrument> instruments) {
    }

    @Override
    FeeCalculator get(final Contract contract, final String day) {
        final String key = contract.getSymbol() + '@' + contract.getExchange();
        final String jsonString = properties.getProperty(key);

        if (jsonString != null) {
            try {
                final HFTJsonObject jsonObj = JsonUtil.parseJsonString(jsonString);

                if (jsonObj != null) {
                    final FuturesFeeCalculator feeCalculator = new FuturesFeeCalculator(
                        jsonObj.getDouble("brokerOpenFloatingFee"),
                        jsonObj.getDouble("brokerCloseFloatingFee"),
                        jsonObj.getDouble("brokerCloseBeforeFloatingFee"),
                        jsonObj.getDouble("brokerOpenFixedFee"),
                        jsonObj.getDouble("brokerCloseFixedFee"),
                        jsonObj.getDouble("brokerCloseBeforeFixedFee"),
                        contract.getTickCalculator());
                    return feeCalculator;
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public Endpoints getFeedEndpoints(final String exchange, final String protocol) {
        final String key = exchange + '/' + protocol;
        final String jsonString = properties.getProperty(key);

        if (jsonString != null) {
            try {
                final HFTJsonObject jsonObj = JsonUtil.parseJsonString(jsonString);

                if (jsonObj != null) {
                    return new Endpoints(
                        jsonObj.getString("snapshot"),
                        jsonObj.getString("marketBook"),
                        jsonObj.getString("tick"),
                        jsonObj.getString("trade"));
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Endpoints getTradeEndpoints(final String account, final String protocol) {
        final String key = account + '/' + protocol;
        final String jsonString = properties.getProperty(key);

        if (jsonString != null) {
            try {
                final HFTJsonObject jsonObj = JsonUtil.parseJsonString(jsonString);

                if (jsonObj != null) {
                    return new Endpoints(
                        jsonObj.getString("snapshot"),
                        jsonObj.getString("marketBook"),
                        jsonObj.getString("tick"),
                        jsonObj.getString("trade"));
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
