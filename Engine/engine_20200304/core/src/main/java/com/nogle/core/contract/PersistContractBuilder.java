package com.nogle.core.contract;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogle.commons.command.QueryConnEndpoints;
import com.nogle.commons.command.QueryProductFee;
import com.nogle.commons.command.QuerySymbolByGroup;
import com.nogle.commons.command.QuerySymbolByInterval;
import com.nogle.commons.command.QuerySymbolByName;
import com.nogle.commons.command.QuerySymbolByRank;
import com.nogle.commons.command.QuerySymbolInformation;
import com.nogle.commons.format.TagValueDecoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.commons.utils.ProductInfo;
import com.nogle.commons.utils.SymbolInfoRefreshForm;
import com.nogle.commons.utils.SymbolQueryForm;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.FuturesFeeCalculator;
import com.nogle.core.util.NogleMessagingRequester;
import com.nogle.core.util.TradeClock;
import com.nogle.messaging.Messenger;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.types.EndpointType;
import com.timelinecapital.core.util.SecurityTypeHelper;

public class PersistContractBuilder extends ContractBuilder {
    private static final Logger log = LogManager.getLogger(PersistContractBuilder.class);

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final long requestTimeoutSeconds = 10;

    private final String serverName;
    private final NogleMessagingRequester batchSymbolRequester;
    private final NogleMessagingRequester symbolRequester;
    private final NogleMessagingRequester endpointRequester;
    private final NogleMessagingRequester feeRequester;

    private final Map<String, Map<String, Instrument>> infos = new HashMap<>();

    private Symbol symbol;
    private FeeCalculator feeCalculator;
    private Contract feeContract;
    private Endpoints endpoints;

    public PersistContractBuilder(final String serverName) {
        this.serverName = serverName;

        batchSymbolRequester = new NogleMessagingRequester("PS/Batch/Request", Messenger.getClientId() + "/Batch/Reply") {

            @Override
            public void onReply(final TagValueDecoder decoder) {
                final String temp = decoder.get(TradeEngineCommunication.msgTypeTag);
                if (temp.equals(TradeEngineCommunication.msgTypeReply)) {

                    try {
                        final String jsonString = decoder.get(TradeEngineCommunication.batchSymbolQuery);
                        final ProductInfo[] productInfos = mapper.readValue(jsonString, ProductInfo[].class);

                        for (final ProductInfo productInfo : productInfos) {
                            final String tradingDay = new DateTime(productInfo.getDay()).toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
                            final Map<String, Instrument> contractMap = infos.computeIfAbsent(tradingDay, inst -> new HashMap<>());

                            if (productInfo.getTickSize() == 0 || productInfo.getContractSize() <= 0) {
                                log.error("Invalid tick size or contract unit");
                                continue;
                            }

                            final Symbol symbol = new Symbol(
                                String.valueOf(productInfo.getProductId()),
                                productInfo.getSymbol(),
                                productInfo.getExchange(),
                                productInfo.getProductClass(),
                                new FixedTickCalculator(productInfo.getTickSize(), productInfo.getContractSize()),
                                SecurityTypeHelper.getSecurityType(productInfo.getSymbol(), productInfo.getExchange()),
                                productInfo.getLimitUpPrice(),
                                productInfo.getLimitDownPrice());
                            contractMap.put(productInfo.getLookupKey(), symbol);
                        }
                    } catch (final Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }

        };

        symbolRequester = new NogleMessagingRequester("PS/Symbol/Request", Messenger.getClientId() + "/Symbol/Reply") {

            @Override
            public void onReply(final TagValueDecoder decoder) {
                final String temp = decoder.get(TradeEngineCommunication.msgTypeTag);
                if (temp.equals(TradeEngineCommunication.msgTypeReply)) {
                    final double tickSize = Double.parseDouble(decoder.get(TradeEngineCommunication.tickSizeTag));
                    final double contractUnit = Double.parseDouble(decoder.get(TradeEngineCommunication.contractUnitTag));
                    double limitUp = Double.NaN;
                    if (decoder.get(TradeEngineCommunication.limitUpTag) != null) {
                        limitUp = Double.parseDouble(decoder.get(TradeEngineCommunication.limitUpTag));
                    }
                    double limitDown = Double.NaN;
                    if (decoder.get(TradeEngineCommunication.limitDownTag) != null) {
                        limitDown = Double.parseDouble(decoder.get(TradeEngineCommunication.limitDownTag));
                    }
                    if (tickSize != 0 && contractUnit != 0) {
                        symbol = new Symbol(
                            decoder.get(TradeEngineCommunication.productIDTag),
                            decoder.get(TradeEngineCommunication.symbolTag),
                            decoder.get(TradeEngineCommunication.exchangeTag),
                            decoder.get(TradeEngineCommunication.productGroupTag),
                            new FixedTickCalculator(tickSize, contractUnit),
                            SecurityTypeHelper.getSecurityType(decoder.get(TradeEngineCommunication.symbolTag), decoder.get(TradeEngineCommunication.exchangeTag)),
                            limitUp,
                            limitDown);
                    } else {
                        log.error("Invalid tick size or contract unit");
                    }
                }
            };
        };

        endpointRequester = new NogleMessagingRequester("PS/Endpoint/Request", Messenger.getClientId() + "/Endpoint/Reply") {

            @Override
            public void onReply(final TagValueDecoder decoder) {
                if (decoder.get(TradeEngineCommunication.msgTypeTag).equals(TradeEngineCommunication.msgTypeReply)) {
                    endpoints = new Endpoints(
                        decoder.get(TradeEngineCommunication.snapshotEndpointTag),
                        decoder.get(TradeEngineCommunication.marketBookEndpointTag),
                        decoder.get(TradeEngineCommunication.tickEndpointTag),
                        decoder.get(TradeEngineCommunication.tradeEndpointTag));
                }
            }
        };

        feeRequester = new NogleMessagingRequester("PS/Fee/Request", Messenger.getClientId() + "/Fee/Reply") {

            @Override
            public void onReply(final TagValueDecoder decoder) {
                final String temp = decoder.get(TradeEngineCommunication.msgTypeTag);
                if (temp.equals(TradeEngineCommunication.msgTypeReply)) {
                    feeCalculator = new FuturesFeeCalculator(
                        Double.parseDouble(decoder.get(TradeEngineCommunication.openFloatingFee)),
                        Double.parseDouble(decoder.get(TradeEngineCommunication.closeFloatingFee)),
                        Double.parseDouble(decoder.get(TradeEngineCommunication.closeBeforeFloatingFee)),
                        Double.parseDouble(decoder.get(TradeEngineCommunication.openFixedFee)),
                        Double.parseDouble(decoder.get(TradeEngineCommunication.closeFixedFee)),
                        Double.parseDouble(decoder.get(TradeEngineCommunication.closeBeforeFixedFee)),
                        feeContract.getTickCalculator());
                }
            };
        };
    }

    @Override
    public Instrument getByName(final String symbolName, final String exchange, final String day) {
        log.info("getContractByName: symbol={} exchange={} day={}", symbolName, exchange, day);
        symbol = null;
        symbolRequester.sendBlockingRequest(
            QuerySymbolByName.name,
            QuerySymbolByName.composeParameter(symbolName, exchange, day),
            requestTimeoutSeconds);
        return symbol;
    }

    @Override
    public Instrument getIndices(final String codeName, final String exchange, final String day) {
        log.info("getIndices: symbol={} exchange={} day={}", codeName, exchange, day);
        symbol = null;
        symbolRequester.sendBlockingRequest(
            QuerySymbolByName.name,
            QuerySymbolByName.composeParameter(codeName, exchange, day),
            requestTimeoutSeconds);
        return symbol;
    }

    @Override
    public Instrument getByGroup(final String productGroup, final String type, final String exchange, final String day) {
        log.info("getContractByGroup: group={} type={} exchange={} day={}", productGroup, type, exchange, day);
        symbol = null;
        symbolRequester.sendBlockingRequest(
            QuerySymbolByGroup.name,
            QuerySymbolByGroup.composeParameter(productGroup, type, exchange, day),
            requestTimeoutSeconds);
        return symbol;
    }

    @Override
    public Instrument getByGroupRank(final String productGroup, final String rank, final String rankType, final String exchange, final String day) {
        log.info("getContractByRank: group={} rank={} rankType={} exchange={} day={}", productGroup, rank, rankType, exchange, day);
        symbol = null;
        symbolRequester.sendBlockingRequest(
            QuerySymbolByRank.name,
            QuerySymbolByRank.composeParameter(productGroup, rank, rankType, exchange, day),
            requestTimeoutSeconds);
        return symbol;
    }

    @Override
    public Map<String, Map<String, Instrument>> getAllByInterval(final Collection<SymbolQueryForm> queryInfos, final String startDay, final String endDay) {
        log.info("getAllByInterval: symbol={} start={} end={}", queryInfos, startDay, endDay);
        infos.clear();
        try {
            batchSymbolRequester.sendBlockingRequest(
                QuerySymbolByInterval.name,
                QuerySymbolByInterval.composeParameter(queryInfos, startDay, endDay),
                requestTimeoutSeconds);
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        return Map.copyOf(infos);
    }

    @Override
    public void refresh(final Collection<Instrument> instruments) {
        log.info("refresh all contract information #{}", instruments.size());
        infos.clear();
        try {
            final List<SymbolInfoRefreshForm> forms = instruments.stream()
                .map(c -> new SymbolInfoRefreshForm(c.getSymbol(), c.getExchange(), TradeClock.getTradingDay()))
                .collect(Collectors.toList());

            batchSymbolRequester.sendBlockingRequest(
                QuerySymbolInformation.name,
                QuerySymbolInformation.composeParameter(forms),
                requestTimeoutSeconds);

            final Map<String, Instrument> map = infos.getOrDefault(TradeClock.getTradingDay(), Collections.emptyMap());

            instruments.stream().filter(c -> map.values().contains(c)).forEach(symbol -> {
                final Instrument updated = map.get(symbol.getSymbol());
                symbol.updatePriceLimit(updated.getLimitUp(), updated.getLimitDown());
            });

        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    FeeCalculator get(final Contract contract, final String day) {
        log.info("getFeeCalculator: symbol={} exchange={} broker={} day={}", contract.getSymbol(), contract.getExchange(), EngineConfig.getBroker(), day);
        feeCalculator = null;
        feeContract = contract;

        feeRequester.sendBlockingRequest(
            QueryProductFee.name,
            QueryProductFee.composeParameter(contract.getSymbol(), contract.getExchange(), EngineConfig.getBroker(), day),
            requestTimeoutSeconds);
        return feeCalculator;
    }

    @Override
    public Endpoints getFeedEndpoints(final String exchange, final String protocol) {
        log.info("[Feed] getEndpoints: exchange={} broker={} protocol={}", exchange, EngineConfig.getBroker(), protocol);
        endpoints = null;
        endpointRequester.sendBlockingRequest(
            QueryConnEndpoints.name,
            QueryConnEndpoints.composeParameter(EndpointType.FEED.toInt(), exchange, EngineConfig.getBroker(), protocol),
            requestTimeoutSeconds);
        return endpoints;
    }

    @Override
    public Endpoints getTradeEndpoints(final String account, final String protocol) {
        log.info("[Trade] getEndpoints: broker={} protocol={} account={} host={}", EngineConfig.getBroker(), protocol, account, serverName);
        endpoints = null;
        endpointRequester.sendBlockingRequest(
            QueryConnEndpoints.name,
            QueryConnEndpoints.composeParameter(EndpointType.TRADE.toInt(), EngineConfig.getBroker(), protocol, account, serverName),
            requestTimeoutSeconds);
        return endpoints;
    }

}
