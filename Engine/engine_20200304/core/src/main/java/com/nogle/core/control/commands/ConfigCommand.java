package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Config;
import com.nogle.core.exception.CommandException;
import com.nogle.strategy.api.Command;
import com.timelinecapital.core.strategy.StrategyConfig;

public class ConfigCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Set the strategy config";
    private static final String usage = Config.name + " config";
    private final StrategyConfig config;

    public ConfigCommand(final StrategyConfig config) {
        this.config = config;
    }

    @Override
    public String onCommand(final String args) {
        try {
            config.loadJSONMap(Config.parseParameter(args));
        } catch (final Exception e) {
            log.error("Failed to reload the config.", e);
            throw new CommandException("Failed to reload the config.", e);
        }
        return Config.name + " Success";
    }

    @Override
    public String getName() {
        return Config.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }
}
