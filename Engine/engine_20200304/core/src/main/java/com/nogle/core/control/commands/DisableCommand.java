package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Disable;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;

public class DisableCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Disable Strategy";

    private final StrategyUpdater strategyUpdater;

    public DisableCommand(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String onCommand(final String arg) {
        try {
            strategyUpdater.onTradingShutdown();
        } catch (final Exception e) {
            log.error("Failed to turn off order sending permission {} {}", strategyUpdater.getStrategyName(), e.getMessage());
            throw new CommandException("Failed to turn off order sending permission.", e);
        }
        return Disable.name + " Success";
    }

    @Override
    public String getName() {
        return Disable.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return Disable.name;
    }
}
