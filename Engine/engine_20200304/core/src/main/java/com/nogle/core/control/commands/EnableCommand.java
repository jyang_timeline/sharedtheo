package com.nogle.core.control.commands;

import com.nogle.commons.command.Enable;
import com.nogle.core.exception.CommandException;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;

public class EnableCommand implements Command {

    private static final String description = "Enable Strategy";

    private final StrategyUpdater strategyUpdater;
    private final StrategyView view;

    public EnableCommand(final StrategyUpdater strategyUpdater, final StrategyView view) {
        this.strategyUpdater = strategyUpdater;
        this.view = view;
    }

    @Override
    public String onCommand(final String args) {
        if (!view.getLifeCycle().equals(StrategyLifeCycle.Start) && !view.getLifeCycle().equals(StrategyLifeCycle.Stop)) {
            throw new CommandException("Strategy is not running. Wait until it starts!");
        }
        if (view.getLifeCycle().equals(StrategyLifeCycle.Shutdown)) {
            throw new CommandException("Strategy has been shutdown. Can not enable!");
        }
        liftRiskControls();
        strategyUpdater.onTradingEnable();
        return Enable.name + " success.";
    }

    private void liftRiskControls() {
        final ProtectiveManager manager = CoreController.getInstance().getIdToRiskControl().get(strategyUpdater.getStrategyId());
        if (!manager.isMonitoring()) {
            manager.launch();
        }
        for (final BurstinessCondition burstCondition : manager.getBurstConditions()) {
            burstCondition.dismiss();
        }
    }

    @Override
    public String getName() {
        return Enable.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return Enable.name;
    }
}
