package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Fill;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class FillCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Import Fill by trader. EX: IF1601@SHFE 0 2500 3";
    private static final String usage = new StringBuilder().append(Fill.name).append(" [").append(Fill.paramSymbol).append(" ").append(Fill.paramSide).append(" ")
        .append(Fill.paramPrice).append(" ").append(Fill.paramQty).append("] ").toString();

    private final StrategyUpdater strategyUpdater;

    public FillCommand(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String onCommand(final String args) {
        try {
            final HFTJsonObject jsonObj = Fill.parseParameter(args);

            strategyUpdater.importFill(
                jsonObj.getString(Fill.paramSymbol),
                // SideParser.fromChar((char) (jsonObj.getInt(Fill.paramSide))),
                Side.decode((byte) jsonObj.getInt(Fill.paramSide)),
                TimeCondition.IOC,
                jsonObj.getDouble(Fill.paramPrice),
                jsonObj.getLong(Fill.paramQty));

            return Fill.name + " success.";
        } catch (final Exception e) {
            log.error("Failed to import Fill {} {}", strategyUpdater.getStrategyName(), args);
            log.error(e.getMessage(), e);
            throw new CommandException(e.getMessage());
        }
    }

    @Override
    public String getName() {
        return Fill.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }
}
