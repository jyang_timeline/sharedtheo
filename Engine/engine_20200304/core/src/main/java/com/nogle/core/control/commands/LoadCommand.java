package com.nogle.core.control.commands;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Config;
import com.nogle.commons.command.Load;
import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.SimulationTask;
import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.CommandException;
import com.nogle.core.market.SimulatedMarket;
import com.nogle.core.strategy.classloader.ClassLoaderFactory;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.api.Command;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.util.JarNameUtil;
import com.timelinecapital.core.util.TradingAccountHelper;

public class LoadCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Load strategy";
    private static final String usage = Load.name + " strategyId className configuration(JSON format)";

    private final String targetAccount;
    private SimulationTask simulationTask;

    public LoadCommand(final String targetAccount) {
        this.targetAccount = targetAccount;
    }

    @Override
    public String onCommand(final String args) {

        HFTJsonObject jsonObj = null;

        try {
            jsonObj = Load.parseParameter(args);
        } catch (final IOException e) {
            throw new CommandException("Invalid JSON format: " + e.getMessage());
        }

        if (!jsonObj.has(Load.paramStrategyId)) {
            throw new CommandException("Lack of strategyId");
        }

        if (!jsonObj.has(Load.paramJarName)) {
            throw new CommandException("Lack of JarName");
        }

        if (!jsonObj.has(Load.paramConfigurations)) {
            throw new CommandException("Lack of Configuration");
        }

        try {
            /*
             * Need a better way to identify the input
             */
            StrategyConfig strategyConfig;
            final ClassLoader jarClassLoader = ClassLoaderFactory.getJARClassLoader(jsonObj.getString(Load.paramJarName));
            if (jsonObj.getString(Load.paramConfigurations).startsWith("{")) {
                strategyConfig = new StrategyConfig(
                    jarClassLoader,
                    JarNameUtil.getBrokerAccountName(jsonObj.getString(Load.paramJarName)),
                    Config.parseParameter(jsonObj.getString(Load.paramConfigurations)));
            } else {
                strategyConfig = new StrategyConfig(
                    jarClassLoader,
                    JarNameUtil.getBrokerAccountName(jsonObj.getString(Load.paramJarName)),
                    jsonObj.getString(Load.paramConfigurations));
            }

            if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
                log.warn("SIMULATION mode. Publish historical data for model");
                final int strategyId = jsonObj.getInt(Load.paramStrategyId);
                simulationTask = new SimulationTask(
                    strategyId,
                    strategyConfig,
                    new SimulatedMarket(strategyId, strategyConfig.getSchedules(), strategyConfig.getInteger(StrategyConfigProtocol.Key.strategyClosingOffset.toUntypedString())));
            } else {
                TradeClock.setTradingDay(new Date().getTime());

                TradingAccount account;
                if (jsonObj.has(Load.paramAccount)) {
                    account = TradingAccountHelper.getAccountInstance(jsonObj.getString(Load.paramAccount));
                    log.info("using PARAM-specified account: {}", jsonObj.getString(Load.paramAccount));
                } else if (!StringUtils.isEmpty(strategyConfig.getAccount())) {
                    account = TradingAccountHelper.getAccountInstance(strategyConfig.getAccount());
                    log.info("Using JAR-specified account: {}", strategyConfig.getAccount());
                } else {
                    account = TradingAccountHelper.getAccountInstance(targetAccount);
                    log.info("Using DEFAULT account: {}", targetAccount);
                }
                CoreController.getStrategyLoader().buildStrategy(account, jsonObj.getInt(Load.paramStrategyId), strategyConfig);
            }

            return "Success";
        } catch (final Exception e) {
            log.error("Failed to build strategy.", e);
            throw new CommandException(e.getMessage());
        }
    }

    public SimulationTask getSimulationTask() {
        return simulationTask;
    }

    @Override
    public String getName() {
        return Load.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
