package com.nogle.core.control.commands;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.PauseEngine;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;

public class PauseCommand implements Command {

    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Revoke engine trading permission";
    private static final String usage = "{\"engineId\":\"engine ID\"}";

    private final Map<Integer, StrategyUpdater> idToUpdater;
    private final int engineId;

    public PauseCommand(final Map<Integer, StrategyUpdater> idToUpdater, final int engineId) {
        this.idToUpdater = idToUpdater;
        this.engineId = engineId;
    }

    @Override
    public String onCommand(final String args) {
        try {
            final HFTJsonObject jsonObject = PauseEngine.parseParameter(args);

            if (!jsonObject.has(PauseEngine.paramEngineId)) {
                throw new CommandException("Lack of engineId");
            }

            final int engineId = jsonObject.getInt(PauseEngine.paramEngineId);
            if (this.engineId != engineId) {
                throw new CommandException("EngineId not match");
            }

            idToUpdater.forEach((id, updater) -> updater.onTradingShutdown());

        } catch (final Exception e) {
            log.error("Failed to build strategy.", e);
            throw new CommandException(e.getMessage());
        }
        return PauseEngine.name + " Success";
    }

    @Override
    public String getName() {
        return PauseEngine.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
