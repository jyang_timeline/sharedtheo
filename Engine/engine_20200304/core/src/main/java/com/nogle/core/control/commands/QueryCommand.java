package com.nogle.core.control.commands;

import java.util.Collection;

import com.nogle.commons.command.Query;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.nogle.strategy.types.Contract;

public class QueryCommand implements Command {

    private static final String description = "Query strategy informations.";
    private static final String usage = Query.name + " [" + Query.parameterCommand + "|" + Query.parameterSymbol + "]";
    private final StrategyUpdater updater;

    public QueryCommand(final StrategyUpdater updater) {
        this.updater = updater;
    }

    private String contractList(final Collection<? extends Contract> contracts) {
        final StringBuilder sb = new StringBuilder();
        for (final Contract contract : contracts) {
            sb.append(contract.getSymbol() + '@' + contract.getExchange() + ',');
        }
        return sb.toString();
    }

    private String commandList(final Collection<Command> commands) {
        final StringBuilder sb = new StringBuilder();
        final String separator = Character.toString((char) 0x02);
        for (final Command command : commands) {
            sb.append(command.getName() + separator + command.getUsage() + separator + command.getDescription() + separator);
        }
        return sb.toString();
    }

    @Override
    public String onCommand(final String arg) {
        String ret = null;
        switch (arg) {
            case Query.parameterSymbol:
                ret = contractList(updater.getContracts());
                break;
            case Query.parameterCommand:
                ret = commandList(updater.getCommands());
                break;
            default:
                ret = "Unsupport parameter: " + arg;
                break;
        }

        return ret;
    }

    @Override
    public String getName() {
        return Query.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }
}
