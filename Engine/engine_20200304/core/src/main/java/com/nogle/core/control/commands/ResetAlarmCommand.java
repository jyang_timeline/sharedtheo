package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.ResetAlarm;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyView;
import com.nogle.strategy.api.Command;

public class ResetAlarmCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Reset alarm of the strategy";
    private static final String usage = "No argument is required";

    private final StrategyView view;

    public ResetAlarmCommand(final StrategyView view) {
        this.view = view;
    }

    @Override
    public String onCommand(final String arg0) {
        try {
            view.disarm();
        } catch (final Exception e) {
            log.error("Failed to remove Alarm status of strategy: {}", e.getMessage());
            throw new CommandException(e.getMessage());
        }
        return ResetAlarm.name + " Success";
    }

    @Override
    public String getName() {
        return ResetAlarm.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
