package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.ResetOrderView;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;

public class ResetOrderViewCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Reset counter of OrderView of a strategy";
    private static final String usage = "No argument is required";

    private final StrategyUpdater strategyUpdater;

    public ResetOrderViewCommand(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String onCommand(final String args) {
        try {
            strategyUpdater.resetOrderView();
        } catch (final Exception e) {
            log.error("Failed to reset OrderView of strategy {}: {}", strategyUpdater.getStrategyName(), e.getMessage());
            throw new CommandException(e.getMessage());
        }
        return ResetOrderView.name + " Success";
    }

    @Override
    public String getName() {
        return ResetOrderView.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
