package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.SendCancel;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.api.Command;

public class SendCancelCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Send cancel request by trader. EX: IF1601@SHFE 0 I 2500 3";
    private static final String usage = "{\"id\":\"client orderId\"}";

    private final StrategyUpdater strategyUpdater;

    public SendCancelCommand(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String onCommand(final String args) {
        try {
            final HFTJsonObject jsonObj = SendCancel.parseParameter(args);
            final long clOrderId = jsonObj.getLong(SendCancel.paramId);
            if (strategyUpdater.getStrategyId() != ClientOrderIdGenerator.getStrategyId(clOrderId)) {
                throw new CommandException("Given id " + clOrderId + " does not belong to this strategy " + strategyUpdater.getStrategyId());
            }
            strategyUpdater.cancelOrder(jsonObj.getLong(SendCancel.paramId));
        } catch (final Exception e) {
            log.error("Failed to send cancel request {} {} {}", strategyUpdater.getStrategyName(), args, e.getStackTrace());
            throw new CommandException(e.getMessage());
        }
        return SendCancel.name + " success.";
    }

    @Override
    public String getName() {
        return SendCancel.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
