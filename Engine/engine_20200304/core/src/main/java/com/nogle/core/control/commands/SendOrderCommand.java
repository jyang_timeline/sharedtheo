package com.nogle.core.control.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.SendOrder;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class SendOrderCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Send order by trader. EX: IF1601@SHFE 0 I 2500 3";
    private static final String usage = new StringBuilder().append(SendOrder.name).append(" [").append(SendOrder.paramSymbol).append(" ").append(SendOrder.paramSide).append(" ")
        .append(SendOrder.paramTimeCondition).append(" ").append(SendOrder.paramPrice).append(" ").append(SendOrder.paramQty).append("] ").toString();

    private final StrategyUpdater strategyUpdater;

    public SendOrderCommand(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String onCommand(final String args) {
        try {
            final HFTJsonObject jsonObj = SendOrder.parseParameter(args);

            if (!TimeCondition.IOC.equals(TimeCondition.decode((byte) jsonObj.getInt(SendOrder.paramTimeCondition)))) {
                throw new CommandException("Only IOC order is supported now");
            }

            strategyUpdater.sendManualOrder(
                jsonObj.getString(SendOrder.paramSymbol),
                Side.decode((byte) jsonObj.getInt(SendOrder.paramSide)),
                TimeCondition.IOC,
                jsonObj.getDouble(SendOrder.paramPrice),
                jsonObj.getLong(SendOrder.paramQty));

            return SendOrder.name + " success.";
        } catch (final Exception e) {
            log.error("Failed to send manual order {} {} {}", strategyUpdater.getStrategyName(), args, e.getStackTrace());
            throw new CommandException(e.getMessage());
        }
    }

    @Override
    public String getName() {
        return SendOrder.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }
}
