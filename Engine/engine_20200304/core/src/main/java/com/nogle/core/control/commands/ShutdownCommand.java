package com.nogle.core.control.commands;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Shutdown;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.timelinecapital.core.CoreController;

public class ShutdownCommand implements Command {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "Shutdown strategy by ID";
    private static final String usage = "No argument is required";

    private final Map<Integer, StrategyUpdater> idToUpdater;

    public ShutdownCommand(final Map<Integer, StrategyUpdater> idToUpdater) {
        this.idToUpdater = idToUpdater;
    }

    @Override
    public String onCommand(final String args) {
        try {
            final int strategyId = Integer.parseInt(args);
            final StrategyUpdater strategyUpdater = idToUpdater.get(strategyId);
            CoreController.getStrategyLoader().shutdownStrategy(strategyUpdater);
        } catch (final Exception e) {
            log.error("Failed to shutdown strategy {}", e.getMessage());
            throw new CommandException(e.getMessage());
        }
        return Shutdown.name + " Success";
    }

    @Override
    public String getName() {
        return Shutdown.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
