package com.nogle.core.controller;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.core.EventTask;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.core.types.CommandPrivilege;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.nogle.strategy.api.Command;

public class CommandProcessor {
    private static final Logger log = LogManager.getLogger(CommandProcessor.class);

    private final Map<Command, CommandPrivilege> commandToPrivilege = new HashMap<>();
    private final Map<String, Command> nameToCommand = new LinkedHashMap<>();

    private final ConcurrentLinkedQueue<CommandTask> commandQueue = new ConcurrentLinkedQueue<>();
    private final EventHandler handler;
    private final TagValueEncoder encoder;

    private EventTask task = null;

    public CommandProcessor() {
        handler = new EventHandler() {
            @Override
            public final void handle() {
                commandQueue.poll().execute();
            }

            @Override
            public final String getName() {
                return CommandProcessor.class.getSimpleName();
            }
        };

        encoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
    }

    public void setEventTask(final EventTask eventTask) {
        task = eventTask;
    }

    private boolean hasHigherPrivilege(final Command command, final CommandPrivilege privilege) {
        switch (privilege) {
            case System:
                commandToPrivilege.put(command, privilege);
                return false;
            case Model:
                CommandPrivilege existingCommandPrivilege = commandToPrivilege.get(command);
                if (existingCommandPrivilege != null) {
                    return existingCommandPrivilege.isHigherOrEqual(privilege);
                }
                commandToPrivilege.put(command, privilege);
                return false;
            case Self:
                existingCommandPrivilege = commandToPrivilege.get(command);
                if (existingCommandPrivilege != null) {
                    return existingCommandPrivilege.isHigher(privilege);
                }
                commandToPrivilege.put(command, privilege);
                return false;
            default:
                throw new RuntimeException("Unknown privilege of Command");
        }
    }

    public void addCommand(final Command command, final CommandPrivilege privilege) {
        if (!hasHigherPrivilege(command, privilege)) {
            commandToPrivilege.put(command, privilege);
            nameToCommand.put(command.getName().toLowerCase(), command);
        } else {
            log.warn("{} already exists.", command);
        }
    }

    public void execute(final String requestID, final String from, final String command, final String parameter) {
        final CommandTask commandTask = new CommandTask(requestID, from, command, parameter);

        if (task != null) {
            commandQueue.add(commandTask);
            task.invoke(handler);
        } else {
            log.info("Command executing without tasking thread");
            commandTask.execute();
        }
    }

    private void sendReply(final String target, final String message) {
        try {
            Messenger.send(new BinaryMessage(new MessageHeader(target + "/Command/Reply"), message.getBytes(StandardCharsets.UTF_8)));

            log.info("< {} ", message);
        } catch (final Exception exception) {
            log.error("< {} failed", message);
        }
    }

    public void sendCommandFail(final String from, final String failMessage, final String requestID) {
        encoder.clear();
        encoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeFailed);
        encoder.append(TradeEngineCommunication.messageTag, failMessage);

        if (requestID != null) {
            encoder.append(TradeEngineCommunication.requestIDTag, requestID);
        }

        sendReply(from, encoder.compose());
    }

    public Collection<Command> getCommands() {
        return nameToCommand.values();
    }

    private class CommandTask {
        private final String requestID;
        private final String from;
        private final String command;
        private final String parameter;

        public CommandTask(final String requestID, final String from, final String command, final String parameter) {
            this.requestID = requestID;
            this.from = from;
            this.command = command;
            this.parameter = parameter;
        }

        public void execute() {
            final Command commandObject = nameToCommand.get(command.toLowerCase());

            String ret = null;

            encoder.clear();

            try {
                if (commandObject == null) {
                    ret = "Unknown command: " + command + " " + nameToCommand.toString();
                } else {
                    ret = commandObject.onCommand(parameter);
                }

                encoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeReply);
                encoder.append(TradeEngineCommunication.messageTag, ret);
                if (requestID != null) {
                    encoder.append(TradeEngineCommunication.requestIDTag, requestID);
                }

                sendReply(from, encoder.compose());
            } catch (final CommandException commandException) {
                sendCommandFail(from, commandException.getMessage(), requestID);
            }

        }
    }
}
