package com.nogle.core.controller;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Config;
import com.nogle.commons.command.Load;
import com.nogle.commons.format.TagValueDecoder;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.core.EventTask;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.config.EngineMode;
import com.nogle.core.control.commands.PauseCommand;
import com.nogle.core.control.commands.ShutdownCommand;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.types.CommandPrivilege;
import com.nogle.messaging.Message;
import com.nogle.messaging.MessageConsumer;
import com.nogle.messaging.Messenger;
import com.nogle.strategy.api.Command;
import com.timelinecapital.core.commands.system.QueryEngineStatus;
import com.timelinecapital.core.commands.system.QuerySystemLoading;
import com.timelinecapital.core.config.EngineConfig;

public class Commander {
    private static final Logger log = LogManager.getLogger(Commander.class);

    private final TagValueDecoder decoder;
    private final TagValueEncoder encoder;
    private final CommandProcessor mainInterpreter;
    private final List<String> commandWithSecurity = new ArrayList<>();

    public Commander(final int engineId, final Map<Integer, StrategyUpdater> idToUpdater) {
        decoder = new TagValueDecoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
        encoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);

        mainInterpreter = new CommandProcessor();
        mainInterpreter.addCommand(new ShutdownCommand(idToUpdater), CommandPrivilege.System);
        mainInterpreter.addCommand(new PauseCommand(idToUpdater, engineId), CommandPrivilege.System);
        mainInterpreter.addCommand(new QuerySystemLoading(idToUpdater), CommandPrivilege.System);
        mainInterpreter.addCommand(new QueryEngineStatus(idToUpdater), CommandPrivilege.System);

        commandWithSecurity.add(Load.name);
        commandWithSecurity.add(Config.name);

        final EventTask systemTask = EventTaskFactory.getSystemTask();
        if (EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode()) || EngineMode.TESTING.equals(EngineConfig.getEngineMode())) {
            new Thread(systemTask).start();
        }
        mainInterpreter.setEventTask(systemTask);

        Messenger.subscribe(Messenger.getClientId() + "/Command/Request", new MessageConsumer() {
            private String incomingMessage = null;
            private String from = null;
            private String requestID = null;
            private StrategyUpdater strategyUpdater = null;
            private CommandProcessor interpreter = null;

            @Override
            public void onMessage(final Message<?> message) {
                incomingMessage = new String((byte[]) message.getPayload(), StandardCharsets.UTF_8);

                encoder.clear();
                decoder.decode(incomingMessage);
                requestID = decoder.get(TradeEngineCommunication.requestIDTag);
                from = message.getHeader().getFromClientId();

                if (!commandWithSecurity.contains(decoder.get(TradeEngineCommunication.commandTag))) {
                    log.info("{} > {}", from, incomingMessage);
                }

                try {
                    String temp = decoder.get(TradeEngineCommunication.msgTypeTag);
                    if (temp == null) {
                        throw new CommandException("Lack of msgType tag");
                    }

                    if (!(temp.equals(TradeEngineCommunication.msgTypeRequest))) {
                        throw new CommandException("Unknown msgType: " + temp);
                    }

                    temp = decoder.get(TradeEngineCommunication.strategyIdTag);
                    if (temp == null) {
                        interpreter = mainInterpreter;
                    } else {
                        strategyUpdater = idToUpdater.get(Integer.parseInt(temp));
                        if (strategyUpdater == null) {
                            throw new CommandException("strategyId not exist: " + temp);
                        } else {
                            interpreter = strategyUpdater.getCommandProcessor();
                        }
                    }

                    interpreter.execute(requestID, from, decoder.get(TradeEngineCommunication.commandTag), decoder.get(TradeEngineCommunication.parameterTag));
                } catch (final CommandException commandException) {
                    mainInterpreter.sendCommandFail(from, commandException.getMessage(), requestID);
                }
            }

            @Override
            public void onError(final Message<?> message) {
                return;
            }
        });
    }

    public void addSystemCommand(final Command command) {
        mainInterpreter.addCommand(command, CommandPrivilege.System);
    }

}
