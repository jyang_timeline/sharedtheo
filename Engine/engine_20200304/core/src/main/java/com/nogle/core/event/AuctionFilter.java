package com.nogle.core.event;

import com.nogle.core.trade.SimulatedExchange.ExecutionEvent;
import com.timelinecapital.core.Filter;

public class AuctionFilter implements Filter<ExecutionEvent> {

    private final Result[] auctionFilters = new Result[ExecutionEvent.values().length];
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private AuctionFilter(final Result onMatch, final Result onMismatch) {
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;

        for (final ExecutionEvent event : ExecutionEvent.values()) {
            switch (event) {
                case ORDER_INSERT:
                    auctionFilters[event.getSeq()] = onMatch;
                    break;
                case ORDER_CANCEL:
                    auctionFilters[event.getSeq()] = onMatch;
                    break;
                case MARKETBOOK_MATCHING:
                    auctionFilters[event.getSeq()] = onMismatch;
                    break;
                case TRADE_MATCHING:
                    auctionFilters[event.getSeq()] = onMatch;
                    break;
            }
        }

    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result filter(final ExecutionEvent event) {
        return auctionFilters[event.getSeq()];
    }

    public static AuctionFilter createFilter(final Result match, final Result mismatch) {
        final Result onMatch = match == null ? Result.ACCEPT : match;
        final Result onMismatch = mismatch == null ? Result.ACCEPT : mismatch;
        return new AuctionFilter(onMatch, onMismatch);
    }

}
