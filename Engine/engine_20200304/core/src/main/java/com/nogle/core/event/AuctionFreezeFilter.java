package com.nogle.core.event;

import com.nogle.core.trade.SimulatedExchange.ExecutionEvent;
import com.timelinecapital.core.Filter;

public class AuctionFreezeFilter implements Filter<ExecutionEvent> {

    private final Result[] freezeFilters = new Result[ExecutionEvent.values().length];
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private AuctionFreezeFilter(final Result onMatch, final Result onMismatch) {
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;

        for (final ExecutionEvent event : ExecutionEvent.values()) {
            switch (event) {
                case ORDER_INSERT:
                    freezeFilters[event.getSeq()] = onMatch;
                    break;
                case ORDER_CANCEL:
                    freezeFilters[event.getSeq()] = onMismatch;
                    break;
                case MARKETBOOK_MATCHING:
                    freezeFilters[event.getSeq()] = onMismatch;
                    break;
                case TRADE_MATCHING:
                    freezeFilters[event.getSeq()] = onMatch;
                    break;
            }
        }

    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result filter(final ExecutionEvent event) {
        return freezeFilters[event.getSeq()];
    }

    public static AuctionFreezeFilter createFilter(final Result match, final Result mismatch) {
        final Result onMatch = match == null ? Result.ACCEPT : match;
        final Result onMismatch = mismatch == null ? Result.ACCEPT : mismatch;
        return new AuctionFreezeFilter(onMatch, onMismatch);
    }

}
