package com.nogle.core.event;

import java.util.Arrays;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.util.SecurityTypeHelper;

public final class DataTypeChangesFilter implements Filter<DataType> {

    private final Result[] available = new Result[DataType.values().length];
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private DataTypeChangesFilter(final Exchange exchange, final Result onMatch, final Result onMismatch) {
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;
        final DataType[] types = SecurityTypeHelper.getSecurityAvailableDataType(exchange);
        Arrays.fill(available, Result.DENY);
        for (final DataType type : types) {
            available[type.getSeqValue()] = onMatch;
        }
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result filter(final DataType dataType) {
        return available[dataType.getSeqValue()];
    }

    public static DataTypeChangesFilter createFilter(
        final Exchange exchange,
        final Result match,
        final Result mismatch) {
        final Result onMatch = match == null ? Result.NEUTRAL : match;
        final Result onMismatch = mismatch == null ? Result.DENY : mismatch;
        return new DataTypeChangesFilter(exchange, onMatch, onMismatch);
    }

}
