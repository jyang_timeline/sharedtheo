package com.nogle.core.event;

import com.nogle.core.trade.SimulatedExchange.ExecutionEvent;
import com.timelinecapital.core.Filter;

public class DefaultFilter implements Filter<ExecutionEvent> {

    private final Result[] generalFilters = new Result[ExecutionEvent.values().length];
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.ACCEPT;

    private DefaultFilter(final Result onMatch, final Result onMismatch) {
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;

        for (final ExecutionEvent event : ExecutionEvent.values()) {
            generalFilters[event.getSeq()] = onMatch;
        }
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result filter(final ExecutionEvent event) {
        return generalFilters[event.getSeq()];
    }

    public static DefaultFilter createFilter(final Result match, final Result mismatch) {
        final Result onMatch = match == null ? Result.ACCEPT : match;
        final Result onMismatch = mismatch == null ? Result.ACCEPT : mismatch;
        return new DefaultFilter(onMatch, onMismatch);
    }

}
