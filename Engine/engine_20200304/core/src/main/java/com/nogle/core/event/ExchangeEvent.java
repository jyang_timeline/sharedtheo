package com.nogle.core.event;

import java.util.Arrays;
import java.util.BitSet;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.Filter.Result;

public class ExchangeEvent {

    private final long[] feedEvents = new long[DataType.values().length];
    private final boolean[] mask = new boolean[DataType.values().length];

    private final Exchange exchange;
    private final String exchangeName;
    private final Filter<DataType> filter;
    private final BitSet feedDiff = new BitSet();

    private long marketdataCount = 0l;

    public ExchangeEvent(final Exchange exchange) {
        this.exchange = exchange;
        this.exchangeName = exchange.toString();

        Arrays.fill(feedEvents, 0);
        Arrays.fill(mask, false);

        filter = DataTypeChangesFilter.createFilter(exchange, Result.ACCEPT, Result.DENY);
    }

    public Exchange getExchange() {
        return exchange;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public long getFeedEventCount() {
        return marketdataCount;
    }

    public long[] getMarketDataCount() {
        return feedEvents;
    }

    public long getMarketDataCount(final DataType dataType) {
        return feedEvents[dataType.getSeqValue()];
    }

    public boolean isDataTypeRegistered(final DataType dataType) {
        return mask[dataType.getSeqValue()];
    }

    public boolean isDataTypeRegistered(final int seq) {
        return mask[seq];
    }

    public Filter<DataType> getFilter() {
        return filter;
    }

    public BitSet getFeedDiff() {
        return feedDiff;
    }

    public void onMarketData(final DataType dataType) {
        marketdataCount++;
        feedEvents[dataType.getSeqValue()]++;
    }

    public void withDataType(final DataType dataType) {
        mask[dataType.getSeqValue()] = true;
    }

    public synchronized void reset() {
        marketdataCount = 0l;
        for (int i = 0; i < feedEvents.length; i++) {
            feedEvents[i] = 0;
        }
    }

}
