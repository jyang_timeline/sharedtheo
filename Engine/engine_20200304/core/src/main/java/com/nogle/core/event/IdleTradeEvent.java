package com.nogle.core.event;

import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.ExecRequestType;

public class IdleTradeEvent implements TradeEvent {

    private static IdleTradeEvent instance = new IdleTradeEvent();

    public static IdleTradeEvent getInstance() {
        return instance;
    }

    @Override
    public final void setTradeProxyListener(final TradeProxyListener tradeProxyListener) {
    }

    @Override
    public final TradeEvent onEventContext() {
        return instance;
    }

    @Override
    public final void onFill(final long clOrderId, final long lastQty, final long remainQty, final double lastPrice, final double commission) {
    }

    @Override
    public final void onOrderAck(final long clOrdId) {
    }

    @Override
    public final void onModAck(final long clOrdId) {
    }

    @Override
    public final void onCancelAck(final long clOrdId) {
    }

    @Override
    public final void onOrderReject(final long clOrdId, final RejectReason rejectReason) {
    }

    @Override
    public final void onModReject(final long clOrdId, final RejectReason rejectReason) {
    }

    @Override
    public final void onCancelReject(final long clOrdId, final RejectReason rejectReason) {
    }

    @Override
    public final void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
    }

    @Override
    public void rewind() {
    }

    @Override
    public boolean isReady() {
        return true;
    }

}
