package com.nogle.core.event;

import java.nio.ByteBuffer;

@Deprecated
public interface QuoteEvent {

    void onBook(long millis, ByteBuffer data);

    void onTick(long millis, ByteBuffer data);

}
