package com.nogle.core.event;

import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.marketdata.type.Quote;

public final class QuoteSourceFilter implements Filter<Quote> {

    private final ContentType contentType;
    private final Exchange exchange;
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private QuoteSourceFilter(final ContentType contentType, final Exchange exchange, final Result onMatch, final Result onMismatch) {
        this.contentType = contentType;
        this.exchange = exchange;
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result filter(final Quote quote) {
        if (contentType == quote.getContentType() && exchange == quote.getExchange()) {
            return onMatch;
        }
        return onMismatch;
    }

    public static QuoteSourceFilter createFilter(
        final ContentType contentType,
        final Exchange exchange,
        final Result match,
        final Result mismatch) {
        final Result onMatch = match == null ? Result.NEUTRAL : match;
        final Result onMismatch = mismatch == null ? Result.DENY : mismatch;
        return new QuoteSourceFilter(contentType, exchange, onMatch, onMismatch);
    }

}