package com.nogle.core.event;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.core.Filter;
import com.timelinecapital.core.marketdata.type.SimulationEvent;

public final class SequenceFilter implements Filter<SimulationEvent> {
    private static final Logger log = LogManager.getLogger(SequenceFilter.class);

    private final long sequenceNumGap;
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private SequenceFilter(final long sequenceNumGap, final Result onMatch, final Result onMismatch) {
        this.sequenceNumGap = sequenceNumGap;
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result filter(final SimulationEvent event) {
        if (event.getSequenceNo() - event.getPrevious().getSequenceNo() > sequenceNumGap) {
            log.trace("Gap detected: {} {} : {} / {}", event.getKey(), event.getDataType(), event.getSequenceNo(), event.getPrevious().getSequenceNo());
            return onMismatch;
        }
        return onMatch;
    }

    public static SequenceFilter createFilter(
        final long sequenceNumGap,
        final Result match,
        final Result mismatch) {
        final Result onMatch = match == null ? Result.NEUTRAL : match;
        final Result onMismatch = mismatch == null ? Result.DENY : mismatch;
        return new SequenceFilter(sequenceNumGap, onMatch, onMismatch);
    }

}
