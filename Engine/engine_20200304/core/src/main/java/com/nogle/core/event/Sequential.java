package com.nogle.core.event;

public interface Sequential<T> {

    void stash(T event, long sourceMicros);

    void publish(long sourceMicros);

}
