package com.nogle.core.event;

import java.util.Arrays;

import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.marketdata.type.Quote;

public final class TickTypeFilter<T extends TickView & Quote> implements Filter<T> {
    // private static final Logger log = LogManager.getLogger(TickTypeFilter.class);

    private final boolean[] targetExchange = new boolean[Exchange.values().length];
    private final boolean[] targetTickType = new boolean[TickType.values().length];
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private TickTypeFilter(final TickType[] type, final Exchange[] exchange, final Result onMatch, final Result onMismatch) {
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;
        Arrays.fill(targetExchange, false);
        Arrays.fill(targetTickType, false);
        for (final Exchange e : exchange) {
            targetExchange[e.ordinal()] = true;
        }
        for (final TickType t : type) {
            targetTickType[t.ordinal()] = true;
        }
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result filter(final T event) {
        if (targetTickType[event.getType().ordinal()] && targetExchange[event.getExchange().ordinal()]) {
            // log.debug("on type/exchange matched {} {}", event.getType(), event.getExchange());
            return onMatch;
        }
        return onMismatch;
    }

    public static <T extends TickView & Quote> TickTypeFilter<T> createFilter(
        final TickType[] tickTypes,
        final Exchange[] exchanges,
        final Result match,
        final Result mismatch) {
        final Result onMatch = match == null ? Result.NEUTRAL : match;
        final Result onMismatch = mismatch == null ? Result.DENY : mismatch;
        return new TickTypeFilter<>(tickTypes, exchanges, onMatch, onMismatch);
    }

}
