package com.nogle.core.event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.Clock;
import org.apache.logging.log4j.core.util.ClockFactory;

import com.timelinecapital.core.Filter;
import com.timelinecapital.core.marketdata.type.SimulationEvent;

public final class TimeFilter implements Filter<SimulationEvent> {
    private static final Logger log = LogManager.getLogger(TimeFilter.class);

    private static final Clock CLOCK = ClockFactory.getClock();

    /**
     * Starting offset from midnight in milliseconds.
     */
    private final long start;
    /**
     * Ending offset from midnight in milliseconds.
     */
    private final long end;

    private final TimeZone timezone;

    private long midnightToday;
    private long midnightTomorrow;
    private Result onMatch = Result.ACCEPT;
    private Result onMismatch = Result.DENY;

    private TimeFilter(final long start, final long end, final TimeZone tz, final Result onMatch, final Result onMismatch) {
        this.start = start;
        this.end = end;
        this.onMatch = onMatch;
        this.onMismatch = onMismatch;
        timezone = tz;
        initMidnight(start);
    }

    @Override
    public Result getOnMatch() {
        return onMatch;
    }

    @Override
    public Result getOnMismatch() {
        return onMismatch;
    }

    /**
     * Initializes the midnight boundaries to midnight in the specified time zone.
     *
     * @param now a time in milliseconds since the epoch, used to pinpoint the current date
     */
    void initMidnight(final long now) {
        final Calendar calendar = Calendar.getInstance(timezone);
        calendar.setTimeInMillis(now);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        midnightToday = calendar.getTimeInMillis();

        calendar.add(Calendar.DATE, 1);
        midnightTomorrow = calendar.getTimeInMillis();
    }

    Result filter(final long currentTimeMillis) {
        if (currentTimeMillis >= midnightTomorrow || currentTimeMillis < midnightToday) {
            initMidnight(currentTimeMillis);
        }
        return currentTimeMillis >= midnightToday + start && currentTimeMillis <= midnightToday + end //
            ? onMatch // within window
            : onMismatch;
    }

    @Override
    public Result filter(final SimulationEvent event) {
        return filter(event.getUpdateTimeMillis());
    }

    Result filter() {
        return filter(CLOCK.currentTimeMillis());
    }

    public static TimeFilter createFilter(
        final String start,
        final String end,
        final String tz,
        final Result match,
        final Result mismatch) {

        final long s = parseTimestamp(start, 0);
        final long e = parseTimestamp(end, Long.MAX_VALUE);
        final TimeZone timezone = tz == null ? TimeZone.getDefault() : TimeZone.getTimeZone(tz);
        final Result onMatch = match == null ? Result.NEUTRAL : match;
        final Result onMismatch = mismatch == null ? Result.DENY : mismatch;
        return new TimeFilter(s, e, timezone, onMatch, onMismatch);
    }

    private static long parseTimestamp(final String timestamp, final long defaultValue) {
        if (timestamp == null) {
            return defaultValue;
        }
        final SimpleDateFormat stf = new SimpleDateFormat("HH:mm:ss");
        stf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return stf.parse(timestamp).getTime();
        } catch (final ParseException e) {
            log.warn("Error parsing TimeFilter timestamp value {}", timestamp, e);
            return defaultValue;
        }
    }

}
