package com.nogle.core.event;

import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.ExecRequestType;
import com.timelinecapital.core.event.MarketOpenAware;

public interface TradeEvent extends MarketOpenAware {

    void setTradeProxyListener(TradeProxyListener tradeProxyListener);

    default void setSentQyt(final long sentQyt) {
    }

    default void setSentPrice(final double sentPrice) {
    }

    TradeEvent onEventContext();

    void onFill(long clOrderId, long lastQty, long remainQty, double lastPrice, double commission);

    void onOrderAck(long clOrdId);

    void onCancelAck(long clOrdId);

    void onOrderReject(long clOrdId, RejectReason rejectReason);

    void onCancelReject(long clOrdId, RejectReason rejectReason);

    void onRequestFail(long clOrdId, ExecRequestType requestType);

    default void onModAck(final long clOrdId) {
        throw new RuntimeException("Does not yet support modify.");
    }

    default void onModReject(final long clOrdId, final RejectReason rejectReason) {
        throw new RuntimeException("Does not yet support modify.");
    }

}
