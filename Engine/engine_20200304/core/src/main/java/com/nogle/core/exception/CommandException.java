package com.nogle.core.exception;

@SuppressWarnings("serial")
public class CommandException extends RuntimeException {

    public CommandException(final String msg) {
        super(msg);
    }

    public CommandException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public CommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public String getMessage() {
        return super.getMessage();
    }
}

