package com.nogle.core.exception;

import com.nogle.strategy.types.Config;

public class ConditionNotFoundException extends Exception {

    private static final long serialVersionUID = 4009118801855736666L;

    public ConditionNotFoundException(final String key, final Config config) {
        super("Strategy: " + config.getString("Strategy.name") + " does not provide properties:" + key);
    }

    public ConditionNotFoundException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public ConditionNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ConditionNotFoundException(final String message) {
        super(message);
    }
}

