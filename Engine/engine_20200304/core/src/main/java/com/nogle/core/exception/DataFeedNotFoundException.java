package com.nogle.core.exception;

public class DataFeedNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -3383589757016996023L;

    public DataFeedNotFoundException(final String message) {
        super(message);
    }

    public DataFeedNotFoundException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public DataFeedNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
