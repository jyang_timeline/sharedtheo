package com.nogle.core.exception;

import com.nogle.strategy.types.Config;

public class DataParserException extends Exception {

    private static final long serialVersionUID = 4184378726579921765L;

    public DataParserException(final String key, final Config config) {
        super("Strategy: " + config.getString("Strategy.name") + " does not provide properties:" + key);
    }

    public DataParserException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public DataParserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DataParserException(final String message) {
        super(message);
    }
}
