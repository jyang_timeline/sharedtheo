package com.nogle.core.exception;

public class EventNotSupportedException extends RuntimeException {
    private static final long serialVersionUID = -3356018007852042653L;

    public EventNotSupportedException(final String message) {
        super(message);
    }

    public EventNotSupportedException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public EventNotSupportedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
