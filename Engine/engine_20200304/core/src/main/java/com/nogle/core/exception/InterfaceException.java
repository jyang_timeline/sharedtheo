package com.nogle.core.exception;

@SuppressWarnings("serial")
public class InterfaceException extends Exception {

    public InterfaceException(final String msg) {
        super(msg);
    }

    public InterfaceException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InterfaceException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
