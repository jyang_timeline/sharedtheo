package com.nogle.core.exception;

public class InvalidCodeNameException extends Exception {

    private static final long serialVersionUID = -4779446691390980649L;

    public InvalidCodeNameException(final String codeName) {
        super("CodeName: " + codeName + " fail on matching any Pattern");
    }

    public InvalidCodeNameException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidCodeNameException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
