package com.nogle.core.exception;

@SuppressWarnings("serial")
public class InvalidConfigurationException extends Exception {

    public InvalidConfigurationException(final String msg) {
        super(msg);
    }

    public InvalidConfigurationException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidConfigurationException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
