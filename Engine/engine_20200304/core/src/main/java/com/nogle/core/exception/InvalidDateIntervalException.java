package com.nogle.core.exception;

@SuppressWarnings("serial")
public class InvalidDateIntervalException extends Exception {

    public InvalidDateIntervalException(final String msg) {
        super(msg);
    }

    public InvalidDateIntervalException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidDateIntervalException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
