package com.nogle.core.exception;

public class InvalidFeedEndpointException extends RuntimeException {

    private static final long serialVersionUID = 2977058753158732172L;

    public InvalidFeedEndpointException(final String message) {
        super(message);
    }

    public InvalidFeedEndpointException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidFeedEndpointException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
