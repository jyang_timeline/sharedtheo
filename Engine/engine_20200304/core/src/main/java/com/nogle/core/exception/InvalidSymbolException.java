package com.nogle.core.exception;

public class InvalidSymbolException extends RuntimeException {
    private static final long serialVersionUID = -7498274254590201728L;

    public InvalidSymbolException(final String message) {
        super(message);
    }

    public InvalidSymbolException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidSymbolException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
