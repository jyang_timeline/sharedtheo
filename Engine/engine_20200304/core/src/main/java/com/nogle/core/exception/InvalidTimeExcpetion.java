package com.nogle.core.exception;

public class InvalidTimeExcpetion extends RuntimeException {
    private static final long serialVersionUID = -3356018007852042653L;

    public InvalidTimeExcpetion(final String message) {
        super(message);
    }

    public InvalidTimeExcpetion(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidTimeExcpetion(final String message, final Throwable cause) {
        super(message, cause);
    }

}