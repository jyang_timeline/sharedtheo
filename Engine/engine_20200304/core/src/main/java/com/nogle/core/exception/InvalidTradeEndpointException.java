package com.nogle.core.exception;

public class InvalidTradeEndpointException extends RuntimeException {

    private static final long serialVersionUID = -3184275140993191590L;

    public InvalidTradeEndpointException(final String message) {
        super(message);
    }

    public InvalidTradeEndpointException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public InvalidTradeEndpointException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
