package com.nogle.core.exception;

public class OrderViewNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 4848903444282577782L;

    public OrderViewNotFoundException(final String message) {
        super(message);
    }

    public OrderViewNotFoundException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public OrderViewNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
