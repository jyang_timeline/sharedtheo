package com.nogle.core.exception;

public class ProxyActivationException extends RuntimeException {

    private static final long serialVersionUID = 5495142140218734198L;

    public ProxyActivationException(final String message) {
        super(message);
    }

    public ProxyActivationException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public ProxyActivationException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
