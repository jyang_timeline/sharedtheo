package com.nogle.core.exception;

public class StrategyAlreadyRunningException extends Exception {

    private static final long serialVersionUID = 7802371678103841834L;

    public StrategyAlreadyRunningException(final String msg) {
        super(msg);
    }

    public StrategyAlreadyRunningException(final Throwable e) {
        super(e.getMessage(), e);
    }

    public StrategyAlreadyRunningException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
