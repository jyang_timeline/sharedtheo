package com.nogle.core.exception;

@SuppressWarnings("serial")
public class StrategyException extends Exception {

    public StrategyException(String msg) {
        super(msg);
    }

    public StrategyException(Throwable e) {
        super(e.getMessage(), e);
    }

    public StrategyException(String message, Throwable cause) {
        super(message, cause);
    }
}
