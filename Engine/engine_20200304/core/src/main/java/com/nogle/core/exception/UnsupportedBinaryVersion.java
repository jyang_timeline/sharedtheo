package com.nogle.core.exception;

public class UnsupportedBinaryVersion extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 6311803825229059666L;

    public UnsupportedBinaryVersion(final String message) {
        super(message);
    }

    public UnsupportedBinaryVersion(final Throwable e) {
        super(e.getMessage(), e);
    }

    public UnsupportedBinaryVersion(final String message, final Throwable cause) {
        super(message, cause);
    }

}
