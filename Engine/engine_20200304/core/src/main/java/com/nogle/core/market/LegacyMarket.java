package com.nogle.core.market;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.ClientParameters;
import com.nogle.core.EngineStatusView;
import com.nogle.core.config.EngineMode;
import com.nogle.core.marketdata.LegacyPriceFeedProxy;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.marketdata.subscriber.MarketDataBinarySub;
import com.nogle.core.strategy.CoreBuilder;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.LiveTradeProxy;
import com.nogle.core.trade.TradeConnection;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.scheduler.MarketOpeningScheduleListener;
import com.timelinecapital.core.scheduler.PreOpeningService;
import com.timelinecapital.core.stats.ContractSummary;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.types.StrategyEventType;
import com.timelinecapital.core.util.TradingHoursHelper;

public class LegacyMarket implements Market {
    private static final Logger log = LogManager.getLogger(LiveMarket.class);

    private static final int MARKET_OPEN_OFFSET = 1;
    private static final int MARKET_START_OFFSET = 1;

    private final List<MarketDataBinarySub> quoteConnections = new ArrayList<>();
    private final List<TradeConnection> tradeConnections = new ArrayList<>();
    private final List<StrategyView> strategyViews = Collections.synchronizedList(new ArrayList<>());

    private final Set<Instrument> contracts = new HashSet<>();

    private final Map<Instrument, QuoteView> quoteViews = new HashMap<>();
    private final Map<Integer, StrategyLifeCycle> strategyStatus = new HashMap<>();

    private final LegacyPriceFeedProxy priceFeedProxy;
    private final LiveTradeProxy tradeProxy;
    private final SbeVersion version;

    private volatile boolean isWarmupAvailable = false;
    private volatile boolean isPreMarket = true;

    public LegacyMarket(final EngineStatusView engineStatusView, final ClientParameters clientParameters) {
        priceFeedProxy = new LegacyPriceFeedProxy(quoteConnections);
        tradeProxy = new LiveTradeProxy(clientParameters, tradeConnections);
        version = EngineConfig.getSchemaVersion();
    }

    @Override
    public SbeVersion getCodecVersion() {
        return version;
    }

    public MarketDataBinarySub[] getQuoteSubscriber(final String symbol) {
        return priceFeedProxy.getQuoteSubscriber(symbol);
    }

    public QuoteView subscribeFeed(final Instrument contract, final List<FeedType> feedTypes) {
        final QuoteView quoteView = priceFeedProxy.subscribe(contract, feedTypes, version);
        quoteViews.put(contract, quoteView);
        contracts.add(contract);
        return quoteView;
    }

    public QuoteView getQuoteView(final Instrument contract) {
        return quoteViews.get(contract);
    }

    @Override
    public com.nogle.core.strategy.OrderViewContainer getOrderView(final int strategyId, final String threadTag, final Contract contract, final TradeAppendix tradeAppendix) {
        return tradeProxy.getOrderView(strategyId, threadTag, contract, tradeAppendix);
    }

    @Override
    public List<TradeConnection> getTradeConnections() {
        return tradeConnections;
    }

    @Override
    public Map<Long, Set<ChannelListener>> getStrategyToChannels() {
        return Collections.emptyMap();
    }

    @Override
    public boolean canDoWarmup() {
        if (EngineMode.TESTING.equals(EngineConfig.getEngineMode())) {
            return true;
        }
        return isWarmupAvailable;
    }

    public boolean hasTradingStrategy() {
        for (final StrategyLifeCycle lifeCycle : strategyStatus.values()) {
            if (StrategyLifeCycle.Start.equals(lifeCycle) || StrategyLifeCycle.Stop.equals(lifeCycle)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void updateStrategyStatus(final int strategyId, final StrategyLifeCycle lifeCycle) {
        strategyStatus.put(strategyId, lifeCycle);
    }

    @Override
    public void onStrategyBuild() {
        EngineStatusView.getInstance().increaseStrategyCount();
    }

    @Override
    public void onStrategyBuildFailed(final StrategyView view) {
        EngineStatusView.getInstance().decreaseStrategyCount();
        view.onStrategyException();
        strategyViews.remove(view);
        // publishState(view);
    }

    @Override
    public void onStrategyEnter(final StrategyView view) {
        strategyViews.add(view);

        boolean hasInTradingStrategy = false;
        for (final StrategyLifeCycle lifeCycle : strategyStatus.values()) {
            if (StrategyLifeCycle.Start.equals(lifeCycle) || StrategyLifeCycle.Stop.equals(lifeCycle)) {
                hasInTradingStrategy = true;
                break;
            }
        }

        if (TradingHoursHelper.isTradingHours(MARKET_OPEN_OFFSET) && EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode())) {
            log.warn("[Market] Skip warm-up flow: Market is already opened!");
            isWarmupAvailable = false;
            return;
        }
        if (hasInTradingStrategy && EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode())) {
            log.warn("[Market] Unable to execute warm-up: Strategy is in trading!");
            isWarmupAvailable = false;
            return;
        }
        log.warn("[Market] Can do warm-up before market open");
        if (EngineConfig.enableWarmup()) {
            onPreMarketWarmup();
        }
    }

    @Override
    public void onStrategyExit(final StrategyView view) {
        EngineStatusView.getInstance().decreaseStrategyCount();
        strategyViews.remove(view);
        // publishState(view);
    }

    @Override
    public void onPreOpening() {
        try {
            final PreOpeningService preOpeningService = new PreOpeningService();

            preOpeningService.setMarketOpen(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On Market open tasks");

                    onMarketOpen();
                    for (final TradeConnection tradeConnection : tradeConnections) {
                        tradeConnection.ping();
                    }
                }
            });

            preOpeningService.setPreOpeningInfoRefresh(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On contract info refresh");

                    CoreBuilder.buildContract(contracts);
                    CoreController.getInstance().getIdToEventManager().forEach((id, eventManager) -> {
                        final StrategyUpdater updater = CoreController.getInstance().getIdToUpdater().get(id);
                        if (updater != null) {
                            updater.getContracts().forEach(c -> eventManager.publish(StrategyEventType.ContractRefresh, c));
                        } else {
                            log.error("Invalid Updater on pre-opening tasks");
                        }
                    });
                }
            });

            preOpeningService.setPreOpeningPositionSync(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On position synchronization");

                    for (final TradeConnection tradeConnection : tradeConnections) {
                        tradeConnection.sync();
                    }
                }
            });

        } catch (final ParseException e) {
            log.error(e.getMessage(), e);
        }
    }

    void onPreMarketWarmup() {
        if (isPreMarket) {
            for (final TradeConnection tradeConnection : tradeConnections) {
                tradeConnection.onPreMarket();
            }
            isPreMarket = false;
            isWarmupAvailable = true;
            log.warn("[Market] Pre-Market! All connection to TradeServer has been redirected to Pre-Market service");
        }
    }

    void onMarketOpen() {
        isPreMarket = false;
        isWarmupAvailable = false;
        OverviewFactory.getContractOverviews().values().forEach(ContractSummary::reset);
        for (final TradeConnection tradeConnection : tradeConnections) {
            try {
                tradeConnection.onRegularMarket();
                tradeConnection.start();
            } catch (final IllegalThreadStateException e) {
                log.info("Trade connection thread is already running!");
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        for (final MarketDataBinarySub quoteConnection : quoteConnections) {
            try {
                quoteConnection.startSubscriber();
            } catch (final IllegalThreadStateException e) {
                log.info("Quote connection thread is already running!");
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        log.warn("[Market] Regular market! All connection to TradeServer has been redirected to Production service");
        System.gc();
    }

}