package com.nogle.core.market;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;

import com.nogle.core.ClientParameters;
import com.nogle.core.EngineStatusView;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.config.EngineMode;
import com.nogle.core.scheduler.CalendarDayNotifyService;
import com.nogle.core.scheduler.ConnectivityMonitorService;
import com.nogle.core.strategy.CoreBuilder;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.LiveTradeProxy;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.feed.Channel;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.feed.FeedProxy;
import com.timelinecapital.core.heartbeat.StateDispatcher;
import com.timelinecapital.core.heartbeat.TradeSummaryDispatcher;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.scheduler.EngineMetricsRefreshJob;
import com.timelinecapital.core.scheduler.EngineSnapshotRefreshJob;
import com.timelinecapital.core.scheduler.MarketOpeningScheduleListener;
import com.timelinecapital.core.scheduler.NotificationService;
import com.timelinecapital.core.scheduler.PositionViewRefreshJob;
import com.timelinecapital.core.scheduler.PreOpeningService;
import com.timelinecapital.core.scheduler.ScheduledEventListener;
import com.timelinecapital.core.scheduler.StrategySnapshotRefreshJob;
import com.timelinecapital.core.scheduler.TradingHourListener;
import com.timelinecapital.core.scheduler.TradingHoursNotifyService;
import com.timelinecapital.core.scheduler.ViewDeltaRefreshJob;
import com.timelinecapital.core.scheduler.WatchdogService;
import com.timelinecapital.core.stats.ContractSummary;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.types.StrategyEventType;
import com.timelinecapital.core.util.ScheduleJobUtil;
import com.timelinecapital.core.util.TradingHoursHelper;
import com.timelinecapital.core.watchdog.WatchdogEventHandler;
import com.timelinecapital.strategy.state.ExchangeSession;

public class LiveMarket implements Market {
    private static final Logger log = LogManager.getLogger(LiveMarket.class);

    private static final int MARKET_OPEN_OFFSET = 1;
    private static final int MARKET_START_OFFSET = 1;

    private final List<Channel> channels = new ArrayList<>();
    private final List<TradeConnection> tradeConnections = new ArrayList<>();
    private final List<StrategyView> strategyViews = Collections.synchronizedList(new ArrayList<>());

    private final Set<Instrument> contracts = new HashSet<>();

    private final Map<Long, Set<ChannelListener>> strategyToChannels = new HashMap<>();
    private final Map<Integer, StrategyLifeCycle> strategyStatus = new HashMap<>();

    private final FeedProxy feedProxy;
    private final LiveTradeProxy tradeProxy;
    private final SbeVersion codecVersion;

    private final StateDispatcher heartbeatDispatcher = new StateDispatcher();
    private final TradeSummaryDispatcher contractDispatcher = new TradeSummaryDispatcher(OverviewFactory.getPositionOverviews());

    private volatile boolean isWarmupAvailable = false;
    private volatile boolean isPreMarket = true;

    public LiveMarket(final ClientParameters clientParameters) {
        feedProxy = new FeedProxy(channels);
        tradeProxy = new LiveTradeProxy(clientParameters, tradeConnections);
        codecVersion = EngineConfig.getSchemaVersion();
    }

    @Override
    public SbeVersion getCodecVersion() {
        return codecVersion;
    }

    @Override
    public void subscribeFeed(final Exchange exchange, final FeedType... feedTypes) throws FeedException {
        for (final FeedType feedType : feedTypes) {
            feedProxy.subscribe(exchange, feedType);
        }
    }

    @Override
    public void subscribeContract(final long strategyId, final Instrument contract, final FeedType feedType, final ChannelListener channelListener) {
        feedProxy.subscribeContract(contract, feedType);
        feedProxy.registerListener(contract, feedType, channelListener);

        contracts.add(contract);

        strategyToChannels.computeIfAbsent(strategyId, k -> new HashSet<>());
        strategyToChannels.get(strategyId).add(channelListener);
    }

    @Override
    public void unsubscribeContract(final long strategyId, final Instrument contract, final FeedType feedType) {
        for (final ChannelListener channelListener : strategyToChannels.get(strategyId)) {
            feedProxy.deregisterListener(contract, feedType, channelListener);
        }
    }

    @Override
    public com.nogle.core.strategy.OrderViewContainer getOrderView(final int strategyId, final String threadTag, final Contract contract, final TradeAppendix tradeAppendix) {
        return tradeProxy.getOrderView(strategyId, threadTag, contract, tradeAppendix);
    }

    @Override
    public List<TradeConnection> getTradeConnections() {
        return tradeConnections;
    }

    @Override
    public Map<Long, Set<ChannelListener>> getStrategyToChannels() {
        return Collections.unmodifiableMap(strategyToChannels);
    }

    public void startNotificationService() throws ParseException {

        if (EngineConfig.isEnableDefaultView()) {
            log.info("Default engine/strategy heartbeat enabled: {}/secs {}/secs", EngineConfig.getDefaultEngineViewInterval(), EngineConfig.getDefaultStrategyViewInterval());

            final CronExpression stgyExpression = new CronExpression(String.format("0/%d * * ? * * ", EngineConfig.getDefaultStrategyViewInterval()));
            final NotificationService stgyNotify = new NotificationService("LEGACY_STGY", stgyExpression);
            stgyNotify.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    heartbeatDispatcher.publishStrategyState(strategyViews);
                }

            }, ScheduleJobUtil.JOB_KEY_HEARTBEAT_STRATEGY_SNPT, StrategySnapshotRefreshJob.class);

            final CronExpression engineExpression = new CronExpression(String.format("0/%d * * ? * * ", EngineConfig.getDefaultEngineViewInterval()));
            final NotificationService engineNotify = new NotificationService("LEGACY_ENGINE", engineExpression);
            engineNotify.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    heartbeatDispatcher.publishEngineState(EngineStatusView.getInstance());
                }

            }, ScheduleJobUtil.JOB_KEY_HEARTBEAT_ENGINE_SNPT, EngineSnapshotRefreshJob.class);

        }

        if (EngineConfig.isEnableDeltaView()) {
            log.info("Delta engine/strategy heartbeat enabled: {}/secs", EngineConfig.getDeltaViewInterval());

            final CronExpression cronExpression =
                new CronExpression(String.format("%d/%d * * ? * * ", EngineConfig.getDeltaViewInterval(), EngineConfig.getDeltaViewInterval()));

            final NotificationService notificationService = new NotificationService("DELTA", cronExpression);
            notificationService.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    // TODO
                }

            }, ScheduleJobUtil.JOB_KEY_HEARTBEAT_DELTA, ViewDeltaRefreshJob.class);

        }

        if (EngineConfig.isEnableEngineMetrics()) {
            log.info("Engine metrics enabled: {}/secs", EngineConfig.getEngineMetricsInterval());

            final CronExpression cronExpression =
                new CronExpression(String.format("%d/%d * * ? * * ", EngineConfig.getEngineMetricsInterval(), EngineConfig.getEngineMetricsInterval()));

            final NotificationService notificationService = new NotificationService("E_METRICS", cronExpression);
            notificationService.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    heartbeatDispatcher.publishEngineMetrics(EngineStatusView.getInstance());
                }

            }, ScheduleJobUtil.JOB_KEY_HEARTBEAT_METRICS, EngineMetricsRefreshJob.class);

        }

        if (EngineConfig.isEnablePositionView()) {
            log.info("Position view heartbeat enabled: {}/secs", EngineConfig.getPositionViewInterval());

            final CronExpression cronExpression =
                new CronExpression(String.format("%d/%d * * ? * * ", EngineConfig.getPositionViewInterval(), EngineConfig.getPositionViewInterval()));

            final NotificationService notificationService = new NotificationService("POSITION", cronExpression);
            notificationService.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    contractDispatcher.publish();
                }

            }, ScheduleJobUtil.JOB_KEY_HEARTBEAT_POSITION_VIEW, PositionViewRefreshJob.class);

        }

    }

    public void startConnectionMonitorService() {
        try {
            final ConnectivityMonitorService monitorService = new ConnectivityMonitorService();
            monitorService.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    for (final TradeConnection tradeConnection : tradeConnections) {
                        tradeConnection.checkConnection();
                        if (TradingHoursHelper.isTradingHours(MARKET_START_OFFSET)) {
                            tradeConnection.checkSocketThread();
                        }
                    }
                }

            }, ScheduleJobUtil.JOB_KEY_CONNECTION_CHECK);

        } catch (final ParseException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void startWatchdogService() {
        try {
            final WatchdogService watchdogService = new WatchdogService();
            watchdogService.setScheduleServiceListener(new ScheduledEventListener() {

                WatchdogEventHandler watchdogEventHandler = new WatchdogEventHandler(OverviewFactory.getContractOverviews(), strategyViews);

                @Override
                public void onScheduledEvent() {
                    EventTaskFactory.getSystemTask().invoke(watchdogEventHandler);
                }

            }, ScheduleJobUtil.JOB_KEY_WATCHDOG);

        } catch (final Exception e) {

        }
    }

    public void startScheduledTaskService() {
        try {
            final CalendarDayNotifyService tradingDayService = new CalendarDayNotifyService();
            tradingDayService.setScheduleServiceListener(new ScheduledEventListener() {

                @Override
                public void onScheduledEvent() {
                    log.warn("Clock service: on calendar day changed: {}", TradeClock.getCalendar());
                    TradeClock.onCalendarDayChange();
                }

            }, ScheduleJobUtil.JOB_KEY_TRADINGDAY_CHANGE);

        } catch (final ParseException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void startTradingHoursNotifyService() {

        final TradingHoursNotifyService service = new TradingHoursNotifyService();
        final TradingHourListener listener = new TradingHourListener() {

            @Override
            public void onTradingHourChange(final Exchange exchange, final ExchangeSession session) {
                CoreController.getInstance().getIdToUpdater().values().forEach(updater -> updater.onExchangeSessionChange(exchange, session));
            }
        };
        service.setTradingHourListener(listener);
    }

    @Override
    public boolean canDoWarmup() {
        if (EngineMode.TESTING.equals(EngineConfig.getEngineMode())) {
            return true;
        }
        return isWarmupAvailable;
    }

    public boolean hasTradingStrategy() {
        for (final StrategyLifeCycle lifeCycle : strategyStatus.values()) {
            if (StrategyLifeCycle.Start.equals(lifeCycle) || StrategyLifeCycle.Stop.equals(lifeCycle)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void updateStrategyStatus(final int strategyId, final StrategyLifeCycle lifeCycle) {
        strategyStatus.put(strategyId, lifeCycle);
    }

    public void publishState(final StrategyView view) {
        heartbeatDispatcher.publishStrategyState(view);
    }

    @Override
    public void onStrategyBuild() {
        EngineStatusView.getInstance().increaseStrategyCount();
    }

    @Override
    public void onStrategyBuildFailed(final StrategyView view) {
        EngineStatusView.getInstance().decreaseStrategyCount();
        view.onStrategyException();
        strategyViews.remove(view);
        publishState(view);
    }

    @Override
    public void onStrategyEnter(final StrategyView view) {
        strategyViews.add(view);

        boolean hasInTradingStrategy = false;
        for (final StrategyLifeCycle lifeCycle : strategyStatus.values()) {
            if (StrategyLifeCycle.Start.equals(lifeCycle) || StrategyLifeCycle.Stop.equals(lifeCycle)) {
                hasInTradingStrategy = true;
                break;
            }
        }

        if (TradingHoursHelper.isTradingHours(MARKET_OPEN_OFFSET) && EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode())) {
            log.warn("[Market] Skip warm-up flow: Market is already opened!");
            isWarmupAvailable = false;
            return;
        }
        if (hasInTradingStrategy && EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode())) {
            log.warn("[Market] Unable to execute warm-up: Strategy is in trading!");
            isWarmupAvailable = false;
            return;
        }
        log.warn("[Market] Can do warm-up before market open");
        if (EngineConfig.enableWarmup()) {
            onPreMarketWarmup();
        }
    }

    @Override
    public void onStrategyExit(final StrategyView view) {
        EngineStatusView.getInstance().decreaseStrategyCount();
        strategyViews.remove(view);
        publishState(view);
    }

    @Override
    public void onPreOpening() {
        try {
            final PreOpeningService preOpeningService = new PreOpeningService();

            preOpeningService.setMarketOpen(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On Market open tasks");

                    onMarketOpen();
                    for (final TradeConnection tradeConnection : tradeConnections) {
                        tradeConnection.ping();
                    }
                }
            });

            preOpeningService.setPreOpeningInfoRefresh(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On contract info refresh");

                    CoreBuilder.buildContract(contracts);
                    CoreController.getInstance().getIdToEventManager().forEach((id, eventManager) -> {
                        final StrategyUpdater updater = CoreController.getInstance().getIdToUpdater().get(id);
                        if (updater != null) {
                            updater.getContracts().forEach(c -> eventManager.publish(StrategyEventType.ContractRefresh, c));
                        } else {
                            log.error("Invalid Updater on pre-opening tasks");
                        }
                    });
                }
            });

            preOpeningService.setPreOpeningPositionSync(new MarketOpeningScheduleListener() {

                @Override
                public void onPreOpeningTask() {
                    log.info("On position synchronization");

                    for (final TradeConnection tradeConnection : tradeConnections) {
                        tradeConnection.sync();
                    }
                }
            });

        } catch (final ParseException e) {
            log.error(e.getMessage(), e);
        }
    }

    // private boolean isRegularMarketStarted(final int offset) {
    // boolean isRegularMarketTime = false;
    // final DateTime now = new DateTime();
    // for (final Interval[] sessionArray : tradingSessions.values()) {
    // for (int i = 0; i < sessionArray.length; i++) {
    // if (now.isAfter(sessionArray[i].getStart().minusMinutes(offset)) && now.isBefore(sessionArray[i].getEnd())) {
    // isRegularMarketTime = true;
    // break;
    // }
    // }
    // }
    // return isRegularMarketTime;
    // }

    void onPreMarketWarmup() {
        if (isPreMarket) {
            for (final TradeConnection tradeConnection : tradeConnections) {
                tradeConnection.onPreMarket();
            }
            isPreMarket = false;
            isWarmupAvailable = true;
            log.warn("[Market] Pre-Market! All connection to TradeServer has been redirected to Pre-Market service");
            // WarmupUtils.onClassLogSuppressing();
        }
    }

    void onMarketOpen() {
        isPreMarket = false;
        isWarmupAvailable = false;
        OverviewFactory.getContractOverviews().values().forEach(ContractSummary::reset);
        for (final TradeConnection tradeConnection : tradeConnections) {
            try {
                tradeConnection.onRegularMarket();
                tradeConnection.start();
            } catch (final IllegalThreadStateException e) {
                log.info("Trade connection thread is already running!");
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        // for (final MarketDataBinarySub quoteConnection : quoteConnections) {
        // try {
        // quoteConnection.startSubscriber();
        // } catch (final IllegalThreadStateException e) {
        // log.info("Quote connection thread is already running!");
        // } catch (final Exception e) {
        // log.error(e.getMessage(), e);
        // }
        // }

        log.warn("[Market] Regular market! All connection to TradeServer has been redirected to Production service");
        System.gc();
    }

}
