package com.nogle.core.market;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.TradeConnection;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.instrument.Instrument;

public interface Market {

    SbeVersion getCodecVersion();

    com.nogle.core.strategy.OrderViewContainer getOrderView(int strategyId, String threadTag, Contract contract, TradeAppendix tradeAppendix);

    List<TradeConnection> getTradeConnections();

    Map<Long, Set<ChannelListener>> getStrategyToChannels();

    default void subscribeFeed(final Exchange exchange, final FeedType... feedTypes) throws FeedException {
    }

    default void subscribeContract(final long strategyId, final Instrument instrument, final FeedType feedType, final ChannelListener channelListener) {
    }

    default void unsubscribeContract(final long strategyId, final Instrument instrument, final FeedType feedType) {
    }

    void updateStrategyStatus(final int strategyId, final StrategyLifeCycle lifeCycle);

    void onPreOpening();

    void onStrategyBuild();

    void onStrategyBuildFailed(StrategyView view);

    void onStrategyEnter(StrategyView view);

    void onStrategyExit(StrategyView view);

    boolean canDoWarmup();

    default void doWarmup() {
    }

}
