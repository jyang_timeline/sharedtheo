package com.nogle.core.market;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.Interval;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.event.TimeFilter;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.exception.DataParserException;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.marketdata.SimulatedExchangePriceFeedListener;
import com.nogle.core.marketdata.SimulatedPriceFeedProxy;
import com.nogle.core.scheduler.SimulatedTimer;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.strategy.config.SimulatedTimeFrame;
import com.nogle.core.trade.AbstractTradeProxy;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.PseudoBookExchange;
import com.nogle.core.trade.SimpleExchange;
import com.nogle.core.trade.SimulatedExchange;
import com.nogle.core.trade.SimulatedOrderSender;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.AckRecycler;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.execution.report.factory.FillRecycler;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.marketdata.source.DataProducer;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.pool.SuperPool;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.types.SessionIdentifier;
import com.timelinecapital.core.types.SimulatorMode;
import com.timelinecapital.core.util.SimulationCodecUtils;

/**
 * 1. onTick or onMarketBook is called
 *
 * 2. Ack all orders that would have arrived by the Tick/MarketBook updateTime
 *
 * 3. Queue corresponding acks with latency
 *
 * 4. Fill all aggressive orders and queue resulting fills with latency
 *
 * 5. Rest unfilled orders
 *
 * 6. Fill Resting Orders with Tick/MarketBook
 *
 * 7. Queue resulting fills with latency
 *
 * 8. Disseminate Acks and Fills that would have gone out by Tick/MarketBook updateTime and set TradeTimer's time accordingly
 *
 * 9. Set TradeTimer's time to Tick/MarketBook's updateTime
 *
 * 10. Update Strategy by calling on QuoteListener/OrderListener
 *
 * @author Hank Huang <huanghank@gmail.com>
 *
 */
public class SimulatedMarket extends AbstractTradeProxy implements Market {
    private static final Logger log = LogManager.getLogger(SimulatedMarket.class);

    private final Map<Contract, SbeVersion> contractToCodec = new HashMap<>();
    private final Map<Contract, SimulatedExchangePriceFeedListener> contractToExchFeedListener = new HashMap<>();
    private final Map<String, SimulatedExchange> simExchanges = new HashMap<>();
    private final Map<Long, TradeEvent> orderIdToTradeEvent = new HashMap<>();

    private final Queue<DelayedMessage<?>> delayedAckAndFills = new PriorityQueue<>();

    private final int strategyId;
    private final SimulatedPriceFeedProxy simulatedPriceFeedProxy;
    private final DataProducer simDataProducer;

    private final SuperPool<Ack> ackPool = new SuperPool<>(Ack.class);
    private final SuperPool<OrderFill> fillPool = new SuperPool<>(OrderFill.class);
    private final AckFactory ackFactory = new AckFactory(ackPool);
    private final FillFactory fillFactory = new FillFactory(fillPool);
    private AckRecycler ackRecycler;
    private FillRecycler fillRecycler;

    private final Map<Contract, File[]> marketDataFiles = new HashMap<>();
    private final Map<Contract, File[]> feedDataFiles = new HashMap<>();

    public SimulatedMarket(final int strategyId, final Collection<Interval> strategySchedules, final int strategyClosingOffset) {
        this.strategyId = strategyId;

        final List<Filter<SimulationEvent>> filters = new ArrayList<>();

        // Filter of event sequence
        // filters.add(SequenceFilter.createFilter(2, Result.ACCEPT, Result.NEUTRAL));

        // Filter of event timestamp
        final Iterator<Interval> itr = strategySchedules.iterator();
        for (int i = 0; i < strategySchedules.size(); i++) {
            final Interval interval = itr.next();
            filters.add(TimeFilter.createFilter(
                interval.getStart().toString(NogleTimeFormatter.ISO_TIME_FORMAT),
                interval.getEnd().plusMinutes(strategyClosingOffset).toString(NogleTimeFormatter.ISO_TIME_FORMAT),
                TimeZone.getDefault().getID(),
                null,
                null));
        }

        simulatedPriceFeedProxy = new SimulatedPriceFeedProxy(filters);
        simDataProducer = new DataProducer();

        try {
            final Constructor<AckRecycler> cAck = AckRecycler.class.getConstructor(int.class, ackPool.getClass());
            ackRecycler = cAck.newInstance(ackPool.getChainSize(), ackPool);
            final Constructor<FillRecycler> cFill = FillRecycler.class.getConstructor(int.class, fillPool.getClass());
            fillRecycler = cFill.newInstance(fillPool.getChainSize(), fillPool);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public Map<Contract, File[]> getPriceFeedSources(final SimulatedTimeFrame timeFrame, final Map<String, ? extends Contract> contracts) throws DataParserException {
        return simDataProducer.getHistoricalData(timeFrame.getTradingDate().toDate(), contracts);
    }

    public Map<Contract, File[]> getAdditionalPriceFeedSources(final SimulatedTimeFrame timeFrame, final Map<FeedType, List<Contract>> feeds) throws DataParserException {
        return simDataProducer.getFeedDataFiles(timeFrame.getTradingDate().toDate(), feeds);
    }

    public void prepareQuotes(final Map<Contract, File[]> marketDataFiles, final Map<Contract, File[]> feedDataFiles) throws IOException {
        SimulationCodecUtils.updateCodecVersionMapping(marketDataFiles, contractToCodec);
        marketDataFiles.forEach((contract, files) -> this.marketDataFiles.compute(contract, (k, v) -> ArrayUtils.addAll(v, files)));
        feedDataFiles.forEach((contract, files) -> this.feedDataFiles.compute(contract, (k, v) -> ArrayUtils.addAll(v, files)));
    }

    public void buildExchangeSchedules(final Map<String, List<SessionIdentifier>> exchangeSessions) {
        simExchanges.values().forEach(simExchange -> {
            simExchange.buildSessions(exchangeSessions.get(simExchange.getExchange()));
        });
    }

    public void publishQuotes() {
        removeDuplicateFiles();
        simulatedPriceFeedProxy.publishQuotes(marketDataFiles, feedDataFiles, contractToCodec);
    }

    private void removeDuplicateFiles() {
        marketDataFiles.forEach(((contract, files) -> {
            final Set<File> set = new LinkedHashSet<>(Arrays.asList(files));
            marketDataFiles.put(contract, set.toArray(new File[0]));
        }));

        feedDataFiles.forEach(((contract, files) -> {
            final Set<File> set = new LinkedHashSet<>(Arrays.asList(files));
            feedDataFiles.put(contract, set.toArray(new File[0]));
        }));
    }

    public SimulatedExchangePriceFeedListener getSimulatedExchangePriceFeedListener(final Contract contract) {
        return contractToExchFeedListener.computeIfAbsent(contract,
            key -> new SimulatedExchangePriceFeedListener(
                delayedAckAndFills,
                orderIdToTradeEvent,
                simExchanges.get(contract.getExchange()),
                ackRecycler,
                fillRecycler));
    }

    private SimulatedExchange getSimulatedExchange(final SimulatorMode mode, final String exchangeName) {
        switch (mode) {
            case BASIC:
                return new SimpleExchange(
                    exchangeName,
                    SimulationConfig.getSimLatencyInMicros(),
                    SimulationConfig.getSimIOCLatencyInMicros(),
                    delayedAckAndFills);
            case PSEUDO_BOOK_DOUBLE_PRICE:
            case PSEUDO_BOOK_MANTISSA_PRICE:
                return new PseudoBookExchange(
                    exchangeName,
                    SimulationConfig.getSimLatencyInMicros(),
                    SimulationConfig.getSimIOCLatencyInMicros(),
                    delayedAckAndFills);
        }
        return null;
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    protected OrderSender subscribeToSymbol(final int strategyId, final Contract contract, final TradingAccount account, final PositionImport importer) {
        // TODO should be SINGLE instance per market (Strategy)
        final SimulatedExchange simulatedExchange = simExchanges.computeIfAbsent(
            contract.getExchange(), key -> getSimulatedExchange(SimulationConfig.getSimulatorMode(), key));
        simulatedExchange.registerSymbol(contract, ContractFactory.getFeeCalculator(contract, TradeClock.getTradingDay()), ackFactory, fillFactory);

        final OrderSender orderSender = new SimulatedOrderSender(contract, simulatedExchange, orderIdToTradeEvent);
        return orderSender;
    }

    public QuoteView subscribeFeed(final Instrument instrument, final List<FeedType> feedTypes) {
        return simulatedPriceFeedProxy.subscribe(instrument, feedTypes, contractToCodec.getOrDefault(instrument, SbeVersion.PROT_BASIC));
    }

    @Override
    public SbeVersion getCodecVersion() {
        return SbeVersion.PROT_BASIC;
    }

    @Override
    public List<TradeConnection> getTradeConnections() {
        return Collections.emptyList();
    }

    @Override
    public Map<Long, Set<ChannelListener>> getStrategyToChannels() {
        return Collections.emptyMap();
    }

    public void cleanup() {
        SimulatedTimer.getInstance().resetTimeChangeListener();
        ContractFactory.releaseContractMap();

        simExchanges.clear();

        feedDataFiles.clear();
        marketDataFiles.clear();
        simulatedPriceFeedProxy.clearPriceFeed();

        delayedAckAndFills.clear();
        orderIdToTradeEvent.clear();
        contractToCodec.clear();
        contractToExchFeedListener.clear();

        resetIdGenerator(strategyId);
        OverviewFactory.reset();
    }

    @Override
    public void updateStrategyStatus(final int strategyId, final StrategyLifeCycle lifeCycle) {
    }

    @Override
    public void onPreOpening() {
    }

    @Override
    public void onStrategyBuild() {
    }

    @Override
    public void onStrategyBuildFailed(final StrategyView view) {
    }

    @Override
    public void onStrategyEnter(final StrategyView view) {
    }

    @Override
    public void onStrategyExit(final StrategyView view) {
    }

    @Override
    public boolean canDoWarmup() {
        return false;
    }

}
