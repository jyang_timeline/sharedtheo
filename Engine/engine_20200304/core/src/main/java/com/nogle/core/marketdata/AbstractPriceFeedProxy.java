package com.nogle.core.marketdata;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nogle.core.event.QuoteEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.event.DataFeedEvent;
import com.timelinecapital.core.event.MarketDataEvent;
import com.timelinecapital.core.event.SnapshotFeedEvent;
import com.timelinecapital.core.mktdata.QuoteViewFactory;
import com.timelinecapital.core.stats.FeedEvent;
import com.timelinecapital.core.stats.OverviewFactory;

public abstract class AbstractPriceFeedProxy implements PriceFeedProxy {

    protected final Map<Contract, QuoteView> symbolToQuoteView = new HashMap<>();

    // private final ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors() / 2,
    // ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);

    AbstractPriceFeedProxy() {
    }

    @Deprecated
    abstract void subscribeToSnapshotQuoteView(QuoteView quoteView);

    @Deprecated
    abstract void subscribeToQuoteView(QuoteView quoteView);

    abstract void subscribeToDataFeed(QuoteView quoteView, List<FeedType> feedTypes);

    public abstract void clearPriceFeed();

    @Override
    public final QuoteView subscribe(final Contract contract, final List<FeedType> feedTypes, final SbeVersion version) {
        final QuoteView quoteView = symbolToQuoteView.computeIfAbsent(contract, key -> QuoteViewFactory.getLegacyQuoteView(key, version));
        feedTypes.forEach(feedType -> quoteView.subscribe(feedType));
        subscribeToDataFeed(quoteView, feedTypes);
        return quoteView;
    }

    class MarketDataEventHandler implements MarketDataEvent {

        private final QuoteView quoteView;
        private final FeedEvent feedEvent;

        public MarketDataEventHandler(final QuoteView quoteView) {
            this.quoteView = quoteView;
            this.feedEvent = OverviewFactory.getFeedSBean(quoteView.getContract());
        }

        @Override
        public void onBook(final long millis, final ByteBuffer data) {
            quoteView.updateBook(millis, data);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.MarketBook)) {
                listener.onMarketBook(quoteView.getBook());
            }
            feedEvent.onMarketData(DataType.MARKETBOOK);
        }

        @Override
        public void onTick(final long millis, final ByteBuffer data) {
            quoteView.updateTick(millis, data);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.Trade)) {
                listener.onTick(quoteView.getTick());
            }
        }

        @Override
        public void onSnapshot(final long millis, final ByteBuffer data) {
            quoteView.updateMarketDataSnapshot(millis, data);
            switch (quoteView.getUpdateType()) {
                case MARKETBOOK_ONLY:
                    for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.Snapshot)) {
                        listener.onMarketBook(quoteView.getSnapshot().getMarketBook());
                    }
                    break;
                case TRADE_ONLY:
                case ALL:
                    for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.Snapshot)) {
                        listener.onTick(quoteView.getSnapshot().getTick());
                        listener.onMarketBook(quoteView.getSnapshot().getMarketBook());
                    }
                    break;
            }
            feedEvent.onMarketData(DataType.SNAPSHOT);
        }

        @Override
        public void onBestPriceOrderDetail(final long millis, final ByteBuffer marketdata) {
            quoteView.updateBestPriceOrderDetail(millis, marketdata);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.BestPriceOrderDetail)) {
                listener.onBestPriceOrderDetail(quoteView.getBestPriceOrderDetail());
            }
        }

        @Override
        public void onOrderActions(final long millis, final ByteBuffer marketdata) {
            quoteView.updateOrderActions(millis, marketdata);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.OrderActions)) {
                listener.onOrderActions(quoteView.getOrderActions());
            }
        }

        @Override
        public void onOrderQueue(final long millis, final ByteBuffer marketdata) {
            quoteView.updateOrderQueue(millis, marketdata);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.OrderQueue)) {
                listener.onOrderQueue(quoteView.getOrderQueue());
            }
        }

    }

    class DataFeedEventHandler implements DataFeedEvent {

        private final QuoteView quoteView;

        public DataFeedEventHandler(final QuoteView quoteView) {
            this.quoteView = quoteView;
        }

        @Override
        public final void onBestPriceOrderDetail(final long updateTimeMillis, final ByteBuffer marketdata) {
            quoteView.updateBestPriceOrderDetail(updateTimeMillis, marketdata);
            TradeClock.setCurrentMillis(updateTimeMillis);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.BestPriceOrderDetail)) {
                listener.onBestPriceOrderDetail(quoteView.getBestPriceOrderDetail());
            }
        }

        @Override
        public final void onOrderActions(final long updateTimeMillis, final ByteBuffer marketdata) {
            quoteView.updateOrderActions(updateTimeMillis, marketdata);
            TradeClock.setCurrentMillis(updateTimeMillis);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.OrderActions)) {
                listener.onOrderActions(quoteView.getOrderActions());
            }
        }

        @Override
        public void onOrderQueue(final long updateTimeMillis, final ByteBuffer marketdata) {
            quoteView.updateOrderQueue(updateTimeMillis, marketdata);
            TradeClock.setCurrentMillis(updateTimeMillis);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.OrderQueue)) {
                listener.onOrderQueue(quoteView.getOrderQueue());
            }
        }

    }

    public class SnapshotHandler implements SnapshotFeedEvent {

        private final DataType type = DataType.SNAPSHOT;
        private final QuoteView quoteView;
        private final FeedEvent feedEvent;

        public SnapshotHandler(final QuoteView quoteView) {
            this.quoteView = quoteView;
            this.feedEvent = OverviewFactory.getFeedSBean(quoteView.getContract());
        }

        @Override
        public final void onSnapshot(final ByteBuffer marketdata) {
            quoteView.updateMarketDataSnapshot(TradeClock.getCurrentMillis(), marketdata);
            switch (quoteView.getUpdateType()) {
                case MARKETBOOK_ONLY:
                    // quoteView.getPriceFeedListeners().forEach(
                    // listener -> {
                    // listener.onMarketBook(quoteView.getMarketDataSnapshot().getMarketbook());
                    // });
                    break;
                case TRADE_ONLY:
                case ALL:
                    // quoteView.getPriceFeedListeners().forEach(
                    // listener -> {
                    // listener.onTick(quoteView.getMarketDataSnapshot().getTrade());
                    // listener.onMarketBook(quoteView.getMarketDataSnapshot().getMarketbook());
                    // });
                    break;
            }
            feedEvent.onMarketData(type);
        }

    }

    public class QuoteEventHandler implements QuoteEvent {

        private final DataType type = DataType.MARKETBOOK;
        private final QuoteView quoteView;
        private final FeedEvent feedEvent;

        public QuoteEventHandler(final QuoteView quoteView) {
            this.quoteView = quoteView;
            this.feedEvent = OverviewFactory.getFeedSBean(quoteView.getContract());
        }

        @Override
        public final void onBook(final long updateTimeMillis, final ByteBuffer marketdata) {
            quoteView.updateBook(updateTimeMillis, marketdata);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.MarketBook)) {
                listener.onMarketBook(quoteView.getBook());
            }
            feedEvent.onMarketData(type);
        }

        @Override
        public final void onTick(final long updateTimeMillis, final ByteBuffer marketdata) {
            quoteView.updateTick(updateTimeMillis, marketdata);
            for (final PriceFeedProxyListener listener : quoteView.getPriceFeedProxyListener(FeedType.Trade)) {
                listener.onTick(quoteView.getTick());
            }
        }

    }

}
