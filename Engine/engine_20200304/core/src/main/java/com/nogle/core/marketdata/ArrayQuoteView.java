package com.nogle.core.marketdata;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import com.nogle.strategy.types.Contract;
import com.nogle.util.RoundRobinWrapper;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDSnapshotView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.marketdata.type.BestPriceOrderDetail;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.marketdata.type.OrderActions;
import com.timelinecapital.core.marketdata.type.OrderQueue;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.marketdata.type.Snapshot;
import com.timelinecapital.core.marketdata.type.Tick;
import com.timelinecapital.core.types.UpdateType;
import com.timelinecapital.core.util.ContentTypeHelper;
import com.timelinecapital.core.util.RingCapacityHelper;

public class ArrayQuoteView extends QuoteView {

    private final Map<Class<? extends SimulationEvent>, ObjectFactory<? extends SimulationEvent>> map = new HashMap<>();
    private final boolean[] availableFeeds = new boolean[FeedType.values().length];
    private final int capacity;

    private RoundRobinWrapper<MarketBook> roundRobinBooks;
    private RoundRobinWrapper<Tick> roundRobinTicks;
    private RoundRobinWrapper<Snapshot> roundRobinSnapshots;
    private RoundRobinWrapper<BDBestPriceOrderQueueView> roundRobinOrderDetails;
    private RoundRobinWrapper<BDOrderActionsView> roundRobinOrderActions;
    private RoundRobinWrapper<BDOrderQueueView> roundRobinOrderQueues;

    public ArrayQuoteView(final Contract contract, final SbeVersion version) {
        super(contract, version);
        capacity = RingCapacityHelper.getQuoteEventCapacity();
        for (int i = 0; i < availableFeeds.length; i++) {
            availableFeeds[i] = false;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    void subscribe(final FeedType feedType) {
        if (availableFeeds[feedType.getIndex()]) {
            return;
        }
        switch (feedType) {
            case OrderActions:
                map.put(BDOrderActionsView.class,
                    new ObjectFactory<>(
                        BDOrderActionsView.class,
                        capacity,
                        () -> new OrderActions(contract, ParserInstanceFactory.getActionParser(version))));
                roundRobinOrderActions = (RoundRobinWrapper<BDOrderActionsView>) map.get(BDOrderActionsView.class).getWrapper();
                break;
            case OrderQueue:
                map.put(BDOrderQueueView.class,
                    new ObjectFactory<>(
                        BDOrderQueueView.class,
                        capacity,
                        () -> new OrderQueue(contract, ParserInstanceFactory.getQueueParser(version))));
                roundRobinOrderQueues = (RoundRobinWrapper<BDOrderQueueView>) map.get(BDOrderQueueView.class).getWrapper();
                break;
            case BestPriceOrderDetail:
                map.put(BDBestPriceOrderQueueView.class,
                    new ObjectFactory<>(
                        BDBestPriceOrderQueueView.class,
                        capacity,
                        () -> new BestPriceOrderDetail(contract, ParserInstanceFactory.getBPODParser(version))));
                roundRobinOrderDetails = (RoundRobinWrapper<BDBestPriceOrderQueueView>) map.get(BDBestPriceOrderQueueView.class).getWrapper();
                break;
            case Snapshot:
                map.put(Snapshot.class,
                    new ObjectFactory<>(
                        Snapshot.class,
                        capacity,
                        () -> new Snapshot(contract, version)));
                roundRobinSnapshots = (RoundRobinWrapper<Snapshot>) map.get(Snapshot.class).getWrapper();
                break;
            case Trade:
                map.put(Tick.class,
                    new ObjectFactory<>(
                        Tick.class,
                        capacity,
                        () -> new Tick(contract, ParserInstanceFactory.getTickParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange()))));
                roundRobinTicks = (RoundRobinWrapper<Tick>) map.get(Tick.class).getWrapper();
                break;
            case MarketBook:
                map.put(MarketBook.class,
                    new ObjectFactory<>(
                        MarketBook.class,
                        capacity,
                        () -> new MarketBook(contract, ParserInstanceFactory.getBookParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange()))));
                roundRobinBooks = (RoundRobinWrapper<MarketBook>) map.get(MarketBook.class).getWrapper();
                break;
        }
        availableFeeds[feedType.getIndex()] = true;
    }

    @Override
    public final BDBookView getBook() {
        return roundRobinBooks.get();
    }

    @Override
    public final BDTickView getTick() {
        return roundRobinTicks.get();
    }

    @Override
    public final BDBestPriceOrderQueueView getBestPriceOrderDetail() {
        return roundRobinOrderDetails.get();
    }

    @Override
    public final BDOrderActionsView getOrderActions() {
        return roundRobinOrderActions.get();
    }

    @Override
    public final BDOrderQueueView getOrderQueue() {
        return roundRobinOrderQueues.get();
    }

    @Override
    public final BDSnapshotView getSnapshot() {
        return roundRobinSnapshots.get();
    }

    @Override
    public final UpdateType getUpdateType() {
        return UpdateType.ALL;
    }

    @Override
    public final void updateBook(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinBooks.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateTick(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinTicks.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateBestPriceOrderDetail(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderDetails.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateOrderActions(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderActions.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateOrderQueue(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderQueues.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateMarketDataSnapshot(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinSnapshots.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void release() {
        map.values().forEach(ins -> {
            for (int i = 0; i < ins.getData().length; i++) {
                ins.getData()[i] = null;
            }
            ins.getWrapper().clear();
        });
    }

    @Override
    <T extends SimulationEvent> boolean isOverrun(final Class<T> clazz) {
        return map.get(clazz).getWrapper().peek().isActive();
    }

    @Override
    <T extends SimulationEvent> void doResize(final Class<T> clazz) {
        final int position = (map.get(clazz).getWrapper().getPosition() + 1) % map.get(clazz).getData().length;
        map.get(clazz).doSizeIncrease(position);
    }

}
