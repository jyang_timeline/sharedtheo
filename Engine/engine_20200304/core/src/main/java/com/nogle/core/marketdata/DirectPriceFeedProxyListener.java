package com.nogle.core.marketdata;

import com.nogle.core.EventTask;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.event.MarketBookRouter;
import com.timelinecapital.core.event.TickRouter;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class DirectPriceFeedProxyListener implements PriceFeedProxyListener {

    private final EventTask task;
    private final MarketBookRouter marketBookRouter;
    private final TickRouter tickRouter;
    private final String desc;

    public DirectPriceFeedProxyListener(final EventTask task, final MarketBookRouter marketBookRouter, final TickRouter tickRouter) {
        this.task = task;
        this.marketBookRouter = marketBookRouter;
        this.tickRouter = tickRouter;
        desc = task.getTaskName() + DelimiterUtil.CLASS_DESC_SEPARATOR + marketBookRouter.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR + tickRouter.getName();
    }

    @Override
    public final void onMarketBook(final BDBookView marketBook) {
        // marketBookRouter.set(marketBook);
        // task.invoke(marketBookRouter);
    }

    @Override
    public final void onTick(final BDTickView tick) {
        // tickRouter.set(tick);
        // task.invoke(tickRouter);
    }

    @Override
    public void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onOrderActions(final BDOrderActionsView event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onOrderQueue(final BDOrderQueueView event) {
        // TODO Auto-generated method stub

    }

    @Override
    public final String toString() {
        return desc;
    }

}