package com.nogle.core.marketdata;

import java.util.Map;

import com.nogle.core.EventTask;
import com.nogle.core.strategy.event.FeedEventHandler;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.event.handler.IdleEventHandler;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.strategy.types.MarketData;

public class DisruptivePriceFeedProxyListener implements PriceFeedProxyListener {

    private final EventTask eventTask;
    private final FeedEventHandler<BDBookView> bookHandler;
    private final FeedEventHandler<BDTickView> tickHandler;
    private final FeedEventHandler<BDBestPriceOrderQueueView> bpodEventHandler;
    private final FeedEventHandler<BDOrderActionsView> oActionHandler;
    private final FeedEventHandler<BDOrderQueueView> oQueueHandler;
    private final String desc;

    @SuppressWarnings("unchecked")
    public DisruptivePriceFeedProxyListener(final EventTask eventTask, final Map<FeedType, FeedEventHandler<? extends MarketData>> feedHandlers) {
        this.eventTask = eventTask;
        bookHandler = (FeedEventHandler<BDBookView>) feedHandlers.getOrDefault(FeedType.MarketBook, IdleEventHandler.get());
        tickHandler = (FeedEventHandler<BDTickView>) feedHandlers.getOrDefault(FeedType.Trade, IdleEventHandler.get());
        bpodEventHandler = (FeedEventHandler<BDBestPriceOrderQueueView>) feedHandlers.getOrDefault(FeedType.BestPriceOrderDetail, IdleEventHandler.get());
        oActionHandler = (FeedEventHandler<BDOrderActionsView>) feedHandlers.getOrDefault(FeedType.OrderActions, IdleEventHandler.get());
        oQueueHandler = (FeedEventHandler<BDOrderQueueView>) feedHandlers.getOrDefault(FeedType.OrderQueue, IdleEventHandler.get());
        desc = eventTask.getTaskName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            bookHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            tickHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            bpodEventHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            oActionHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            oQueueHandler.getName();
    }

    @Override
    public final void onMarketBook(final BDBookView marketBook) {
        bookHandler.wrap(marketBook);
        eventTask.invoke(bookHandler);
    }

    @Override
    public final void onTick(final BDTickView tick) {
        tickHandler.wrap(tick);
        eventTask.invoke(tickHandler);
    }

    @Override
    public void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event) {
        bpodEventHandler.wrap(event);
        eventTask.invoke(bpodEventHandler);
    }

    @Override
    public void onOrderActions(final BDOrderActionsView event) {
        oActionHandler.wrap(event);
        eventTask.invoke(oActionHandler);
    }

    @Override
    public void onOrderQueue(final BDOrderQueueView event) {
        oQueueHandler.wrap(event);
        eventTask.invoke(oQueueHandler);
    }

    @Override
    public final String toString() {
        return desc;
    }

}
