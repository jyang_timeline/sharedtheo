package com.nogle.core.marketdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.EngineStatusView;
import com.nogle.core.contract.Endpoints;
import com.nogle.core.event.QuoteEvent;
import com.nogle.core.exception.ProxyActivationException;
import com.nogle.core.marketdata.subscriber.BestOrderDetailBinarySub;
import com.nogle.core.marketdata.subscriber.MarketBookBinarySub;
import com.nogle.core.marketdata.subscriber.MarketDataBinarySub;
import com.nogle.core.marketdata.subscriber.QuoteOrderDetailBinarySub;
import com.nogle.core.marketdata.subscriber.QuoteSnapshotBinarySub;
import com.nogle.core.marketdata.subscriber.TickBinarySub;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.DataFeedEvent;
import com.timelinecapital.core.event.SnapshotFeedEvent;
import com.timelinecapital.core.factory.EndpointFactory;
import com.timelinecapital.core.util.EndpointHelper;
import com.timelinecapital.core.util.TradingHoursHelper;

public class LegacyPriceFeedProxy extends AbstractPriceFeedProxy {
    private static final Logger log = LogManager.getLogger(LegacyPriceFeedProxy.class);

    private final Map<String, MarketDataBinarySub[]> symbolToSubscriber = new HashMap<>();
    private final Map<String, MarketDataBinarySub> endpointToSubscriber = new HashMap<>();
    private final List<MarketDataBinarySub> feedConnections;

    private MarketDataBinarySub[] snapshotSubscribers = new MarketDataBinarySub[0];
    private MarketDataBinarySub[] commonSubscribers = new MarketDataBinarySub[0];

    public LegacyPriceFeedProxy(final List<MarketDataBinarySub> feedConnections) {
        this.feedConnections = feedConnections;
    }

    public MarketDataBinarySub[] getQuoteSubscriber(final String symbol) {
        return symbolToSubscriber.get(symbol);
    }

    @Override
    void subscribeToSnapshotQuoteView(final QuoteView quoteView) {
        final SnapshotFeedEvent quoteEventHandler = new SnapshotHandler(quoteView);

        final Contract contract = quoteView.getContract();
        final String exchange = contract.getExchange();
        final String[] protocols = EngineConfig.getExchangeToProtocols().get(exchange);
        final Endpoints endpoints = EndpointFactory.getFeedEndpoints(exchange, protocols[0]);

        final String[] qsEndpoints = EndpointHelper.getSnapshotEndpoint(endpoints, exchange);
        for (final String qsEndpoint : qsEndpoints) {
            QuoteSnapshotBinarySub qsSubscriber = (QuoteSnapshotBinarySub) endpointToSubscriber.get(qsEndpoint);
            if (qsSubscriber == null) {
                log.info("[Connection] ******** QuoteSnapshot client endpoint: {} ********", qsEndpoint);
                try {
                    final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, EndpointHelper.getPresentName(ConnectionType.QUOTE, qsEndpoint));
                    qsSubscriber = new QuoteSnapshotBinarySub(qsEndpoint, probe, EngineConfig.getSchemaVersion());
                    feedConnections.add(qsSubscriber);
                    endpointToSubscriber.put(qsEndpoint, qsSubscriber);
                    snapshotSubscribers = ArrayUtils.add(snapshotSubscribers, qsSubscriber);
                    EngineStatusView.getInstance().addConnectionProbe(probe);
                } catch (final Exception e) {
                    log.error(e.getMessage(), e);
                    throw new ProxyActivationException("Fail to launch QuoteSnapshot subscriber");
                }
            }
            qsSubscriber.updateTradingSessions(TradingHoursHelper.getTradingHours(exchange));
            qsSubscriber.addQuoteSnapshotHandler(contract.getSymbol(), quoteEventHandler);
        }
        // quoteConnections.add(qsSubscriber);
        symbolToSubscriber.put(contract.getSymbol(), snapshotSubscribers);

        log.info("*** {} : {} has been initialized ***", exchange, contract);
    }

    @Override
    void subscribeToQuoteView(final QuoteView quoteView) {
        final QuoteEvent quoteEventHandler = new QuoteEventHandler(quoteView);

        final Contract contract = quoteView.getContract();
        final String exchange = contract.getExchange();
        final String[] protocols = EngineConfig.getExchangeToProtocols().get(exchange);
        final Endpoints endpoints = EndpointFactory.getFeedEndpoints(exchange, protocols[0]);

        final String mbEndpoint = endpoints.getMarketBook();

        MarketBookBinarySub mbSubscriber = (MarketBookBinarySub) endpointToSubscriber.get(mbEndpoint);
        if (mbSubscriber == null) {
            log.info("[Connection] ******** MarketBook client endpoint: {} ********", mbEndpoint);
            try {
                final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, EndpointHelper.getPresentName(ConnectionType.QUOTE, mbEndpoint));
                mbSubscriber = new MarketBookBinarySub(mbEndpoint, probe, EngineConfig.getSchemaVersion());
                // mbsubscriber.startSubscriber();
                feedConnections.add(mbSubscriber);
                endpointToSubscriber.put(mbEndpoint, mbSubscriber);
                commonSubscribers = ArrayUtils.add(commonSubscribers, mbSubscriber);
                EngineStatusView.getInstance().addConnectionProbe(probe);
            } catch (final UnsupportedNodeException e) {
                log.error(e.getMessage(), e);
                throw new ProxyActivationException("Fail to launch Marketbook subscriber");
            }
        }
        mbSubscriber.updateTradingSessions(TradingHoursHelper.getTradingHours(exchange));
        mbSubscriber.addHandler(contract.getSymbol(), quoteEventHandler);
        // quoteConnections.add(mbSubscriber);

        final String tkEndpoint = endpoints.getTick();

        TickBinarySub tkSubscriber = (TickBinarySub) endpointToSubscriber.get(tkEndpoint);
        if (tkSubscriber == null) {
            log.info("[Connection] ******** Tick client endpoint: {} ********", tkEndpoint);
            try {
                final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, EndpointHelper.getPresentName(ConnectionType.QUOTE, tkEndpoint));
                tkSubscriber = new TickBinarySub(tkEndpoint, probe, EngineConfig.getSchemaVersion());
                // tksubscriber.startSubscriber();
                feedConnections.add(tkSubscriber);
                endpointToSubscriber.put(tkEndpoint, tkSubscriber);
                commonSubscribers = ArrayUtils.add(commonSubscribers, mbSubscriber);
                EngineStatusView.getInstance().addConnectionProbe(probe);
            } catch (final UnsupportedNodeException e) {
                log.error(e.getMessage(), e);
                throw new ProxyActivationException("Fail to launch Tick subscriber");
            }
        }
        tkSubscriber.updateTradingSessions(TradingHoursHelper.getTradingHours(exchange));
        tkSubscriber.addHandler(contract.getSymbol(), quoteEventHandler);
        // quoteConnections.add(tkSubscriber);

        symbolToSubscriber.put(contract.getSymbol(), commonSubscribers);
        log.info("*** {} : {} has been initialized ***", exchange, contract);
    }

    @Override
    void subscribeToDataFeed(final QuoteView quoteView, final List<FeedType> feedTypes) {
        final DataFeedEvent dataFeedEventHandler = new DataFeedEventHandler(quoteView);
        final Contract contract = quoteView.getContract();

        feedTypes.forEach(dataFeedType -> {

            switch (dataFeedType) {
                case BestPriceOrderDetail: {
                    final String endpoint = EngineConfig.getOrderQueueEndpoint();
                    if (!endpoint.isEmpty()) {
                        BestOrderDetailBinarySub subscriber = (BestOrderDetailBinarySub) endpointToSubscriber.get(endpoint);
                        if (subscriber == null) {
                            log.info("[Connection] ******** OrderDetail client endpoint: {} ********", endpoint);
                            try {
                                final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, EndpointHelper.getPresentName(ConnectionType.QUOTE, endpoint));
                                subscriber = new BestOrderDetailBinarySub(endpoint, probe, EngineConfig.getSchemaVersion());
                                endpointToSubscriber.put(endpoint, subscriber);
                                EngineStatusView.getInstance().addConnectionProbe(probe);
                            } catch (final UnsupportedNodeException e) {
                                log.error(e.getMessage(), e);
                                throw new ProxyActivationException("Fail to launch OrderDetail subscriber");
                            }
                        }
                        subscriber.addDataFeedHandler(contract.getSymbol(), dataFeedEventHandler);
                        feedConnections.add(subscriber);
                    }
                    break;
                }
                case OrderActions: {
                    final String endpoint = EngineConfig.getOrderActionsEndpoint();
                    if (!endpoint.isEmpty()) {
                        QuoteOrderDetailBinarySub subscriber = (QuoteOrderDetailBinarySub) endpointToSubscriber.get(endpoint);
                        if (subscriber == null) {
                            log.info("[Connection] ******** OrderAction client endpoint: {} ********", endpoint);
                            try {
                                final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, EndpointHelper.getPresentName(ConnectionType.QUOTE, endpoint));
                                subscriber = new QuoteOrderDetailBinarySub(endpoint, probe, EngineConfig.getSchemaVersion());
                                endpointToSubscriber.put(endpoint, subscriber);
                                EngineStatusView.getInstance().addConnectionProbe(probe);
                            } catch (final UnsupportedNodeException e) {
                                log.error(e.getMessage(), e);
                                throw new ProxyActivationException("Fail to launch OrderAction subscriber");
                            }
                        }
                        subscriber.addDataFeedHandler(contract.getSymbol(), dataFeedEventHandler);
                        feedConnections.add(subscriber);
                    }
                    break;
                }
                case OrderQueue: {
                    log.error("Does not support OrderQueue event subscription!");
                    break;
                }
                default:
                    log.warn("Feed type is not supported yet: {}", dataFeedType);
                    break;
            }

        });
    }

    @Override
    public void clearPriceFeed() {
        endpointToSubscriber.clear();
    }

    @Override
    public boolean connect() {
        return true;
    }

}
