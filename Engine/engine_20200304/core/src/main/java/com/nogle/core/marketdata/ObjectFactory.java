package com.nogle.core.marketdata;

import java.lang.reflect.Array;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.nogle.util.RoundRobinWrapper;

class ObjectFactory<E> {

    private final Class<E> clazz;
    private final Supplier<E> supplier;
    private final RoundRobinWrapper<E> wrapper;
    private E[] data;

    @SuppressWarnings("unchecked")
    public ObjectFactory(final Class<E> clazz, final int capacity, final Supplier<E> supplier) {
        this.clazz = clazz;
        this.supplier = supplier;
        data = (E[]) Array.newInstance(clazz, capacity);
        Stream.generate(supplier).limit(capacity).collect(Collectors.toList()).toArray(data);
        wrapper = new RoundRobinWrapper<>(data);
    }

    public RoundRobinWrapper<E> getWrapper() {
        return wrapper;
    }

    public E[] getData() {
        return data;
    }

    @SuppressWarnings("unchecked")
    public void doSizeIncrease(final int fromPosition) {
        final E[] copied = (E[]) Array.newInstance(clazz, data.length * 2);
        Stream.generate(supplier).limit(copied.length).collect(Collectors.toList()).toArray(copied);
        for (int i = 0; i < data.length; i++) {
            if (i >= data.length - fromPosition) {
                copied[i] = data[i - (data.length - fromPosition)];
            } else {
                copied[i] = data[i + fromPosition];
            }
        }
        data = copied;
        wrapper.replaceWith(copied);
    }

}
