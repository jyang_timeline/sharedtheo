package com.nogle.core.marketdata;

import java.util.List;

import com.nogle.core.Proxy;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public interface PriceFeedProxy extends Proxy {

    QuoteView subscribe(Contract contract, List<FeedType> feedTypes, SbeVersion version);

}
