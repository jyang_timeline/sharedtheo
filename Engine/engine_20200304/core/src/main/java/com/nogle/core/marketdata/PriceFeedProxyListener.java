package com.nogle.core.marketdata;

import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public interface PriceFeedProxyListener {

    void onMarketBook(BDBookView event);

    void onTick(BDTickView event);

    void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event);

    void onOrderActions(final BDOrderActionsView event);

    void onOrderQueue(final BDOrderQueueView event);

}
