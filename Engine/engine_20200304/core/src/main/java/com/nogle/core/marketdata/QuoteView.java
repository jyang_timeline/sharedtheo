package com.nogle.core.marketdata;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDSnapshotView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.types.UpdateType;

public abstract class QuoteView {
    private static final Logger log = LogManager.getLogger(QuoteView.class);

    final Contract contract;
    final SbeVersion version;

    private final Map<Integer, PriceFeedProxyListener[]> strategyToFeedListener = new HashMap<>();
    private final PriceFeedProxyListener[][] priceListeners = new PriceFeedProxyListener[FeedType.values().length][0];

    private PriceFeedProxyListener[] excahngePriceFeedListeners = new PriceFeedProxyListener[0];

    QuoteView(final Contract contract, final SbeVersion version) {
        this.contract = contract;
        this.version = version;
    }

    public Contract getContract() {
        return contract;
    }

    public final synchronized void addSimExchangePriceFeedProxyListener(final PriceFeedProxyListener listener) {
        excahngePriceFeedListeners = ArrayUtils.add(excahngePriceFeedListeners, listener);
        log.trace("AddExchangePriceFeedProxy, New: {} {}", contract, Arrays.asList(excahngePriceFeedListeners));
    }

    public final synchronized void addPriceFeedProxyListener(final PriceFeedProxyListener listener, final int strategyId, final List<FeedType> feedTypes) {
        for (final FeedType feedType : feedTypes) {
            priceListeners[feedType.getIndex()] = ArrayUtils.add(priceListeners[feedType.getIndex()], listener);

            PriceFeedProxyListener[] listeners = strategyToFeedListener.computeIfAbsent(strategyId, id -> new PriceFeedProxyListener[0]);
            listeners = com.nogle.util.ArrayUtils.addElement(listeners, listener);
            strategyToFeedListener.put(strategyId, listeners);
            log.trace("AddPriceFeedProxy, New: {} {}", contract, Arrays.asList(priceListeners));
        }
    }

    public final synchronized void clearPriceFeedProxyListener(final int strategyId) {
        PriceFeedProxyListener[] listeners = strategyToFeedListener.remove(strategyId);
        for (final PriceFeedProxyListener listener : listeners) {
            for (final FeedType feedType : FeedType.values()) {
                priceListeners[feedType.getIndex()] = ArrayUtils.removeElement(priceListeners[feedType.getIndex()], listener);
            }
        }
        listeners = null;
        log.trace("ClearDataFeedProxy, Left: {} {}", contract, Arrays.asList(priceListeners));
    }

    public final PriceFeedProxyListener[] getExcahngePriceFeedListeners() {
        return excahngePriceFeedListeners;
    }

    public PriceFeedProxyListener[] getPriceFeedProxyListener(final FeedType feedType) {
        return priceListeners[feedType.getIndex()];
    }

    abstract void subscribe(FeedType feedType);

    public abstract BDBookView getBook();

    public abstract BDTickView getTick();

    public abstract BDSnapshotView getSnapshot();

    public abstract BDBestPriceOrderQueueView getBestPriceOrderDetail();

    public abstract BDOrderActionsView getOrderActions();

    public abstract BDOrderQueueView getOrderQueue();

    public abstract UpdateType getUpdateType();

    public abstract void updateBook(long updateTimeMillis, ByteBuffer data);

    public abstract void updateTick(long updateTimeMillis, ByteBuffer data);

    public abstract void updateMarketDataSnapshot(long updateTimeMillis, ByteBuffer data);

    public abstract void updateBestPriceOrderDetail(long updateTimeMillis, ByteBuffer data);

    public abstract void updateOrderActions(long updateTimeMillis, ByteBuffer data);

    public abstract void updateOrderQueue(long updateTimeMillis, ByteBuffer data);

    public abstract void release();

    abstract <T extends SimulationEvent> boolean isOverrun(Class<T> clazz);

    abstract <T extends SimulationEvent> void doResize(Class<T> clazz);

}
