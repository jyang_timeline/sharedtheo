package com.nogle.core.marketdata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.util.ExchangeProtocolUtil;

@Deprecated
public class QuoteViewFactory {
    private static final Logger log = LogManager.getLogger(QuoteViewFactory.class);

    public static QuoteView getQuoteView(final Contract contract, final SbeVersion codecVersion) {
        if (ExchangeProtocolUtil.isQuoteSnapshot(contract.getExchange())) {
            log.debug("[View] {} is using Snapshot: v.{}", contract.getSymbol(), codecVersion);
            return new SnapshotQuoteView(contract, codecVersion);
        }
        log.debug("[View] {} is using MDP: v.{}", contract.getSymbol(), codecVersion);
        return new ArrayQuoteView(contract, codecVersion);
    }

}
