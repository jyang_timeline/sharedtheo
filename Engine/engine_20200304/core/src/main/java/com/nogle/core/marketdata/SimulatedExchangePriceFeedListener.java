package com.nogle.core.marketdata;

import java.util.Map;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.exception.EventNotSupportedException;
import com.nogle.core.trade.SimulatedExchange;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckRecycler;
import com.timelinecapital.core.execution.report.factory.FillRecycler;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class SimulatedExchangePriceFeedListener implements PriceFeedProxyListener {
    private static final Logger log = LogManager.getLogger(SimulatedExchangePriceFeedListener.class);

    private final Queue<DelayedMessage<?>> delayedAckAndFills;
    private final Map<Long, TradeEvent> orderIdToTradeEvent;

    private final SimulatedExchange exchange;
    private final AckRecycler ackRecycler;
    private final FillRecycler fillRecycler;

    public SimulatedExchangePriceFeedListener(final Queue<DelayedMessage<?>> delayedAckAndFills, final Map<Long, TradeEvent> orderIdToTradeEvent,
        final SimulatedExchange exchange, final AckRecycler ackRecycler, final FillRecycler fillRecycler) {
        this.delayedAckAndFills = delayedAckAndFills;
        this.orderIdToTradeEvent = orderIdToTradeEvent;

        this.exchange = exchange;
        this.ackRecycler = ackRecycler;
        this.fillRecycler = fillRecycler;
    }

    @Override
    public final void onTick(final BDTickView tick) {
        final long exchangeTimeMicros = tick.getExchangeUpdateTimeMicros();

        // log.trace("[EXG] TK info: {} {} {} {}", exchangeTimeMicros, tick.getUpdateTimeMicros(), tick.getSequenceNo(),
        // tick.getContract());

        // check ACK/fill/reject here using next Quote
        do {
            exchange.processIncomingOrderQueue(exchangeTimeMicros);
            exchange.onTick(tick);
            disseminateAcksAndFills(exchangeTimeMicros);
        } while (hasIncomingRequest(exchangeTimeMicros));
    }

    @Override
    public final void onMarketBook(final BDBookView marketBook) {
        final long exchangeTimeMicros = marketBook.getExchangeUpdateTimeMicros();

        // log.trace("[EXG] MB info: {} {} {} {}", exchangeTimeMicros, marketBook.getUpdateTimeMicros(),
        // marketBook.getSequenceNo(), marketBook.getContract());

        // check ACK/fill/reject here using next Quote
        do {
            exchange.processIncomingOrderQueue(exchangeTimeMicros);
            exchange.onMarketBook(marketBook);
            disseminateAcksAndFills(exchangeTimeMicros);
        } while (hasIncomingRequest(exchangeTimeMicros));
    }

    @Override
    public void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event) {
    }

    @Override
    public void onOrderActions(final BDOrderActionsView event) {
    }

    @Override
    public void onOrderQueue(final BDOrderQueueView event) {
    }

    private boolean hasIncomingRequest(final long exchangeTimeMicros) {
        boolean hasRequest = false;
        hasRequest |= exchange.hasIncomingMessage(exchangeTimeMicros);
        return hasRequest;
    }

    private void disseminateAcksAndFills(final long exchangeTimeMicros) {
        while (!delayedAckAndFills.isEmpty() && delayedAckAndFills.peek().hasMessageArrivedBy(exchangeTimeMicros)) {
            final DelayedMessage<?> delayedMessage = delayedAckAndFills.poll();

            // TradeClock.setCurrentMillis(NogleTimeUnit.MICROSECONDS.toMillis(delayedMessage.getArrivalTimeMicros()));
            TradeClock.setCurrentMicros(delayedMessage.getArrivalTimeMicros());
            // log.info("[EXG] Update clock: {}", TradeClock.getCurrentMicros());

            switch (delayedMessage.getType()) {
                case TradeServerProtocol.Header_OrderAckChar: {
                    final long clOrdId = (Long) delayedMessage.getMessage();
                    orderIdToTradeEvent.get(clOrdId).onOrderAck(clOrdId);
                    break;
                }
                case TradeServerProtocol.Header_ModifyAckChar: {
                    final long clOrdId = (Long) delayedMessage.getMessage();
                    orderIdToTradeEvent.get(clOrdId).onModAck(clOrdId);
                    break;
                }
                case TradeServerProtocol.Header_CancelAckChar: {
                    final Ack ack = (Ack) delayedMessage.getMessage();
                    final long clOrdId = ack.getClOrderId();
                    orderIdToTradeEvent.get(clOrdId).onCancelAck(clOrdId);
                    ackRecycler.recycle(ack);
                    break;
                }
                case TradeServerProtocol.Header_CancelRejectChar: {
                    final long clOrdId = (Long) delayedMessage.getMessage();
                    orderIdToTradeEvent.get(clOrdId).onCancelReject(clOrdId, RejectReason.I);
                    break;
                }
                case TradeServerProtocol.Header_FillChar: {
                    final OrderFill fill = (OrderFill) delayedMessage.getMessage();
                    final long clOrdId = fill.getClOrderId();
                    if (!orderIdToTradeEvent.containsKey(clOrdId)) {
                        log.warn("Filled Order {} has already been removed by cancel request", clOrdId);
                    } else {
                        orderIdToTradeEvent.get(clOrdId).onFill(clOrdId, fill.getFillQty(), fill.getRemainingQty(), fill.getFillPrice(), fill.getCommission());
                        fillRecycler.recycle(fill);
                    }
                    break;
                }
                default:
                    throw new EventNotSupportedException("Unhandled response message");
            }

        }
    }

}
