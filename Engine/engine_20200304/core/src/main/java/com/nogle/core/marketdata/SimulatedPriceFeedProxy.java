package com.nogle.core.marketdata;

import java.io.File;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;

import com.nogle.core.marketdata.parser.HistoricalQuoteParserFactory;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.marketdata.type.SimulationEvent;

public class SimulatedPriceFeedProxy extends AbstractPriceFeedProxy {

    private final Queue<SimulationEvent> simulatedDataQueue = new PriorityQueue<>();
    private final Set<QuoteView> quoteViews = new HashSet<>();
    private final SimulationEventHandler eventHandler;
    private final HistoricalQuoteParserFactory parserFactory;

    public SimulatedPriceFeedProxy(final List<Filter<SimulationEvent>> filters) {
        eventHandler = new SimulationEventHandler(simulatedDataQueue, filters);
        parserFactory = new HistoricalQuoteParserFactory();
    }

    @Override
    void subscribeToSnapshotQuoteView(final QuoteView quoteView) {
        throw new RuntimeException("123");
    }

    @Override
    void subscribeToQuoteView(final QuoteView quoteView) {
        throw new RuntimeException("123");
    }

    @Override
    void subscribeToDataFeed(final QuoteView quoteView, final List<FeedType> feedTypes) {
        eventHandler.subscribe(quoteView);
        parserFactory.addFeedEventHandler(quoteView.getContract(), eventHandler);
        quoteViews.add(quoteView);
    }

    @Override
    public void clearPriceFeed() {
        simulatedDataQueue.clear();
        parserFactory.clearEventHandler();
        symbolToQuoteView.clear();
        eventHandler.unSubscribe();
        quoteViews.forEach(view -> view.release());
        quoteViews.clear();
    }

    @Override
    public boolean connect() {
        return true;
    }

    private final BinaryOperator<File[]> merge = (old, latest) -> {
        old = ArrayUtils.addAll(old, latest);
        return old;
    };

    public void publishQuotes(final Map<Contract, File[]> marketDataFiles, final Map<Contract, File[]> feedDataFiles, final Map<Contract, SbeVersion> contractToCodecVersion) {
        // final Map<Contract, File[]> files = new HashMap<>(feedDataFiles);
        // for (final Entry<Contract, File> entry : marketDataFiles.entrySet()) {
        // files.compute(entry.getKey(), (k, v) -> ArrayUtils.add(v, entry.getValue()));
        // }
        // final Map<Contract, File[]> allFiles = Stream.of(marketDataFiles, feedDataFiles)
        // .flatMap(m -> m.entrySet().stream())
        // .collect(Collectors.toMap(Entry::getKey, Entry::getValue, merge));

        final Map<Contract, List<Entry<File, SbeVersion>>> contractToSimFiles = new HashMap<>();
        Stream.of(marketDataFiles, feedDataFiles)
            .flatMap(m -> m.entrySet().stream())
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue, merge))
            .entrySet().forEach(entry -> {
                final List<Entry<File, SbeVersion>> files = new ArrayList<>();
                for (final File file : entry.getValue()) {
                    files.add(new SimpleEntry<>(file, contractToCodecVersion.get(entry.getKey())));
                    // contractToSimFiles.put(entry.getKey(), files);
                }
                contractToSimFiles.put(entry.getKey(), files);
            });

        // allFiles.forEach((contract, files) -> {
        // for (final File file : files) {
        // final Map<File, Integer> temp = new HashMap<>();
        // temp.put(file, contractToCodecVersion.get(contract));
        // contractToSimFiles.put(contract, temp);
        // }
        // });

        parserFactory.init(contractToSimFiles);
        parserFactory.startParse();
    }

}
