package com.nogle.core.marketdata;

import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class SimulatedPriceFeedProxyListener implements PriceFeedProxyListener {

    private final StrategyUpdater strategyUpdater;
    private final StrategyQuoteSnapshot quoteSnapshot;

    public SimulatedPriceFeedProxyListener(final StrategyUpdater strategyUpdater, final StrategyQuoteSnapshot quoteSnapshot) {
        this.strategyUpdater = strategyUpdater;
        this.quoteSnapshot = quoteSnapshot;
    }

    @Override
    public final void onMarketBook(final BDBookView marketBook) {
        quoteSnapshot.setMarketBook(marketBook);
        strategyUpdater.onMarketBook(marketBook);
    }

    @Override
    public final void onTick(final BDTickView tick) {
        quoteSnapshot.setTick(tick);
        strategyUpdater.onTick(tick);
    }

    @Override
    public void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event) {
        strategyUpdater.onBestPriceOrderDetail(event);
    }

    @Override
    public void onOrderActions(final BDOrderActionsView event) {
        strategyUpdater.onOrderActions(event);
    }

    @Override
    public void onOrderQueue(final BDOrderQueueView event) {
        strategyUpdater.onOrderQueues(event);
    }

}
