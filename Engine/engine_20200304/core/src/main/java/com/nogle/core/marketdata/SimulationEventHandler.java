package com.nogle.core.marketdata;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.event.Sequential;
import com.nogle.core.event.TickTypeFilter;
import com.nogle.core.exception.EventNotSupportedException;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.Filter.Result;
import com.timelinecapital.core.marketdata.SimBookExtraDataHandler;
import com.timelinecapital.core.marketdata.SimDefaultBookHandler;
import com.timelinecapital.core.marketdata.SimDefaultTradeHandler;
import com.timelinecapital.core.marketdata.SimIncrementalTradeHandler;
import com.timelinecapital.core.marketdata.SimSnapshotTradeHandler;
import com.timelinecapital.core.marketdata.SimulatedMarketBookHandler;
import com.timelinecapital.core.marketdata.SimulatedTradeHandler;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.marketdata.type.Tick;

public class SimulationEventHandler implements Sequential<SimulationEvent> {
    private static final Logger log = LogManager.getLogger(SimulationEventHandler.class);

    private final Map<String, SimulatedQuoteEventHandler> quoteToEventHandler = new HashMap<>();
    private final Queue<SimulationEvent> simulatedDataQueue;
    private final List<Filter<SimulationEvent>> filters;
    private final Filter<BDTickView> tickFilter;

    private long atomicClock = 0;

    public SimulationEventHandler(final Queue<SimulationEvent> simulatedDataQueue, final List<Filter<SimulationEvent>> filters) {
        this.simulatedDataQueue = simulatedDataQueue;
        this.filters = filters;
        tickFilter = TickTypeFilter.createFilter(
            new TickType[] { TickType.BUYER, TickType.SELLER },
            new Exchange[] { Exchange.CFFEX, Exchange.SHFE, Exchange.INE, Exchange.DCE, Exchange.CZCE },
            Result.DENY,
            Result.ACCEPT);
    }

    public void subscribe(final QuoteView quoteView) {
        quoteToEventHandler.putIfAbsent(quoteView.getContract().getSymbol(), new SimulatedQuoteEventHandler(quoteView));
    }

    public void unSubscribe() {
        quoteToEventHandler.clear();
    }

    public void onEvent(final long exchTimeMicros, final String eventKey, final long eventTime, final DataType dataType, final ByteBuffer data) {
        /* publish event that is before a given time to clients */
        publish(exchTimeMicros);

        final SimulatedQuoteEventHandler handler = quoteToEventHandler.get(eventKey);
        if (handler == null) {
            throw new RuntimeException("Event can not be handled");
        }

        /* keep event and try to do order matching */
        final long updateTime = NogleTimeUnit.MICROSECONDS.toMillis(eventTime);
        switch (dataType) {
            case MARKETBOOK:
            case GEN_MARKETBOOK:
            case REALTIME_MARKETBOOK:
                if (handler.quoteView.isOverrun(MarketBook.class)) {
                    handler.quoteView.doResize(MarketBook.class);
                }
                handler.quoteView.updateBook(updateTime, data);
                handler.updateVolumeOI();

                stash(handler.quoteView.getBook(), exchTimeMicros);
                for (final PriceFeedProxyListener exchangePriceFeedListener : handler.quoteView.getExcahngePriceFeedListeners()) {
                    final SimulatedExchangePriceFeedListener simExchangePriceFeed = (SimulatedExchangePriceFeedListener) exchangePriceFeedListener;
                    handler.exchBookHandler.onMarketBookData(simExchangePriceFeed, handler.quoteView.getBook());
                }
                break;
            case TICK:
            case GEN_TICK:
                if (handler.quoteView.isOverrun(Tick.class)) {
                    handler.quoteView.doResize(Tick.class);
                }
                handler.quoteView.updateTick(updateTime, data);

                stash(handler.quoteView.getTick(), exchTimeMicros);
                for (final PriceFeedProxyListener exchangePriceFeedListener : handler.quoteView.getExcahngePriceFeedListeners()) {
                    final SimulatedExchangePriceFeedListener simExchangePriceFeed = (SimulatedExchangePriceFeedListener) exchangePriceFeedListener;
                    handler.exchTradeHandler.onTradeData(simExchangePriceFeed, handler.quoteView.getTick());
                }
                break;
            case BESTORDERDETAIL:
                if (handler.quoteView.isOverrun(BDBestPriceOrderQueueView.class)) {
                    handler.quoteView.doResize(BDBestPriceOrderQueueView.class);
                }
                handler.quoteView.updateBestPriceOrderDetail(updateTime, data);
                stash(handler.quoteView.getBestPriceOrderDetail(), exchTimeMicros);
                // TODO trigger exchange for BestPriceOrderDetail
                break;
            case SESSION:
                break;
            case ORDERACTIONS:
                if (handler.quoteView.isOverrun(BDOrderActionsView.class)) {
                    handler.quoteView.doResize(BDOrderActionsView.class);
                }
                handler.quoteView.updateOrderActions(updateTime, data);
                stash(handler.quoteView.getOrderActions(), exchTimeMicros);
                // TODO trigger exchange for OrderActions
                break;
            case ORDERQUEUE:
                if (handler.quoteView.isOverrun(BDOrderQueueView.class)) {
                    handler.quoteView.doResize(BDOrderQueueView.class);
                }
                handler.quoteView.updateOrderQueue(updateTime, data);
                stash(handler.quoteView.getOrderQueue(), exchTimeMicros);
                // TODO trigger exchange for OrderQueue
                break;
            case REALTIME_FILL:
                if (handler.quoteView.isOverrun(Tick.class)) {
                    handler.quoteView.doResize(Tick.class);
                }
                handler.quoteView.updateTick(updateTime, data);
                stash(handler.quoteView.getTick(), exchTimeMicros);
                break;
            default:
                break;
        }
    }

    @Override
    public void stash(final SimulationEvent event, final long exchTimeMicros) {
        if (exchTimeMicros > event.getUpdateTimeMicros()) {
            log.error("ERROR - source time is greater than received one {} {} ", exchTimeMicros, event.getUpdateTimeMicros());
        }
        simulatedDataQueue.add(event);
    }

    @Override
    public void publish(final long exchTimeMicros) {
        while (!simulatedDataQueue.isEmpty() && simulatedDataQueue.peek().getUpdateTimeMicros() <= exchTimeMicros) {
            final SimulationEvent quote = simulatedDataQueue.poll();
            quote.setConsumed();

            if (quote.getUpdateTimeMicros() < atomicClock) {
                log.warn("[Time Reversal] {} -> {} {}", atomicClock, quote.getUpdateTimeMicros(), quote.getContract());
                // throw new InvalidTimeExcpetion("Time reversal @ " + quote);
            }
            atomicClock = quote.getUpdateTimeMicros();

            if (filters.stream().anyMatch(f -> f.filter(quote) == Filter.Result.DENY)) {
                continue;
            }
            final SimulatedQuoteEventHandler handler = quoteToEventHandler.get(quote.getKey());

            TradeClock.setCurrentMicros(quote.getUpdateTimeMicros());
            switch (quote.getDataType()) {
                case MARKETBOOK:
                case GEN_MARKETBOOK:
                case REALTIME_MARKETBOOK:
                    for (final PriceFeedProxyListener priceFeedProxyListener : handler.quoteView.getPriceFeedProxyListener(FeedType.MarketBook)) {
                        handler.clientBookHandler.onMarketBookData(priceFeedProxyListener, (MarketBook) quote);
                    }
                    break;
                case REALTIME_FILL:
                    for (final PriceFeedProxyListener priceFeedProxyListener : handler.quoteView.getPriceFeedProxyListener(FeedType.Trade)) {
                        handler.clientRTTHandler.onTradeData(priceFeedProxyListener, (Tick) quote);
                    }
                    break;
                case TICK:
                case GEN_TICK:
                    if (Result.DENY == tickFilter.filter((Tick) quote)) {
                        break;
                    }
                    for (final PriceFeedProxyListener priceFeedProxyListener : handler.quoteView.getPriceFeedProxyListener(FeedType.Trade)) {
                        handler.clientTradeHandler.onTradeData(priceFeedProxyListener, (Tick) quote);
                    }
                    break;
                case BESTORDERDETAIL:
                    for (final PriceFeedProxyListener listener : handler.quoteView.getPriceFeedProxyListener(FeedType.BestPriceOrderDetail)) {
                        listener.onBestPriceOrderDetail((BDBestPriceOrderQueueView) quote);
                    }
                    break;
                case ORDERACTIONS:
                    for (final PriceFeedProxyListener listener : handler.quoteView.getPriceFeedProxyListener(FeedType.OrderActions)) {
                        listener.onOrderActions((BDOrderActionsView) quote);
                    }
                    break;
                case ORDERQUEUE:
                    for (final PriceFeedProxyListener listener : handler.quoteView.getPriceFeedProxyListener(FeedType.OrderQueue)) {
                        listener.onOrderQueue((BDOrderQueueView) quote);
                    }
                    break;
                default:
                    throw new EventNotSupportedException("Unhandled quote type: " + quote.getDataType());
            }
        }
    }

    class SimulatedQuoteEventHandler {
        private final QuoteView quoteView;

        private final SimulatedTradeHandler clientTradeHandler;
        private final SimulatedMarketBookHandler clientBookHandler;
        private final SimulatedTradeHandler clientRTTHandler;

        private final SimulatedTradeHandler exchTradeHandler;
        private final SimulatedMarketBookHandler exchBookHandler;

        public SimulatedQuoteEventHandler(final QuoteView quoteView) {
            this.quoteView = quoteView;
            switch (Exchange.valueOf(quoteView.getContract().getExchange())) {
                case CFFEX:
                case SHFE:
                case INE:
                case DCE:
                case CZCE:
                    clientTradeHandler = new SimSnapshotTradeHandler();
                    break;
                case CME:
                    clientTradeHandler = new SimIncrementalTradeHandler();
                    break;
                default:
                    clientTradeHandler = new SimDefaultTradeHandler();
                    break;
            }
            clientBookHandler = new SimDefaultBookHandler();
            clientRTTHandler = new SimDefaultTradeHandler();

            exchTradeHandler = new SimDefaultTradeHandler();
            exchBookHandler = new SimBookExtraDataHandler();
        }

        private void updateVolumeOI() {
            if (quoteView.getBook().getVolume() == 0 && quoteView.getBook().getOpenInterest() == 0 && quoteView.getTick() != null) {
                quoteView.getBook().setVolume(quoteView.getTick().getVolume());
                quoteView.getBook().setOpenInterest(quoteView.getTick().getOpenInterest());
            }
        }

    }

}
