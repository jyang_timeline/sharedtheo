package com.nogle.core.marketdata;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.Contract;
import com.nogle.util.RoundRobinWrapper;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.dummy.DummyObjectFactory;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDSnapshotView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.marketdata.type.BestPriceOrderDetail;
import com.timelinecapital.core.marketdata.type.OrderActions;
import com.timelinecapital.core.marketdata.type.OrderQueue;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.marketdata.type.Snapshot;
import com.timelinecapital.core.types.UpdateType;
import com.timelinecapital.core.util.RingCapacityHelper;

public class SnapshotQuoteView extends QuoteView {

    private final int capacity;
    private final Snapshot[] quoteSnapshots;
    private final BDBestPriceOrderQueueView[] orderDetails;
    private final BDOrderActionsView[] orderActions;
    private final BDOrderQueueView[] orderQueues;

    private final RoundRobinWrapper<BDSnapshotView> roundQuoteSnapshots;
    private final RoundRobinWrapper<BDBestPriceOrderQueueView> roundRobinOrderDetails;
    private final RoundRobinWrapper<BDOrderActionsView> roundRobinOrderActions;
    private final RoundRobinWrapper<BDOrderQueueView> roundRobinOrderQueues;

    public SnapshotQuoteView(final Contract contract, final SbeVersion version) {
        super(contract, version);

        capacity = RingCapacityHelper.getQuoteEventCapacity();
        quoteSnapshots = new Snapshot[capacity];
        orderDetails = new BestPriceOrderDetail[capacity];
        orderActions = new OrderActions[capacity];
        orderQueues = new OrderQueue[capacity];

        for (int i = 0; i < capacity; i++) {
            quoteSnapshots[i] = new Snapshot(contract, version);
            orderDetails[i] = DummyObjectFactory.getBestPriceInstance(contract, version);
            orderActions[i] = DummyObjectFactory.getOrderActionsInstance(contract, version);
            orderQueues[i] = DummyObjectFactory.getOrderQueueInstance(contract, version);
        }
        roundQuoteSnapshots = new RoundRobinWrapper<>(quoteSnapshots);
        roundRobinOrderDetails = new RoundRobinWrapper<>(orderDetails);
        roundRobinOrderActions = new RoundRobinWrapper<>(orderActions);
        roundRobinOrderQueues = new RoundRobinWrapper<>(orderQueues);
    }

    @Override
    void subscribe(final FeedType feedType) {
        switch (feedType) {
            case BestPriceOrderDetail:
                for (int i = 0; i < capacity; i++) {
                    orderDetails[i] = new BestPriceOrderDetail(contract, ParserInstanceFactory.getBPODParser(version));
                }
                break;
            case OrderActions:
                for (int i = 0; i < capacity; i++) {
                    orderActions[i] = new OrderActions(contract, ParserInstanceFactory.getActionParser(version));
                }
                break;
            case OrderQueue:
                for (int i = 0; i < capacity; i++) {
                    orderQueues[i] = new OrderQueue(contract, ParserInstanceFactory.getQueueParser(version));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public final BDBookView getBook() {
        return roundQuoteSnapshots.get().getMarketBook();
    }

    @Override
    public final BDTickView getTick() {
        return roundQuoteSnapshots.get().getTick();
    }

    @Override
    public final BDBestPriceOrderQueueView getBestPriceOrderDetail() {
        return roundRobinOrderDetails.get();
    }

    @Override
    public final BDOrderActionsView getOrderActions() {
        return roundRobinOrderActions.get();
    }

    @Override
    public final BDOrderQueueView getOrderQueue() {
        return roundRobinOrderQueues.get();
    }

    @Override
    public final BDSnapshotView getSnapshot() {
        return roundQuoteSnapshots.get();
    }

    @Override
    public final UpdateType getUpdateType() {
        return roundQuoteSnapshots.get().getUpdateType();
    }

    @Override
    public final void updateBook(final long updateTimeMillis, final ByteBuffer data) {
    }

    @Override
    public final void updateTick(final long updateTimeMillis, final ByteBuffer data) {
    }

    @Override
    public final void updateBestPriceOrderDetail(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderDetails.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateOrderActions(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderActions.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateOrderQueue(final long updateTimeMillis, final ByteBuffer data) {
        roundRobinOrderQueues.next().setData(data, updateTimeMillis);
    }

    @Override
    public final void updateMarketDataSnapshot(final long updateTimeMillis, final ByteBuffer data) {
        roundQuoteSnapshots.next().setData(data, updateTimeMillis);
    }

    @Override
    public void release() {
        for (int i = 0; i < capacity; i++) {
            quoteSnapshots[i] = null;
            orderDetails[i] = null;
            orderActions[i] = null;
            orderQueues[i] = null;
        }
        roundQuoteSnapshots.clear();
        roundRobinOrderDetails.clear();
        roundRobinOrderActions.clear();
        roundRobinOrderQueues.clear();
    }

    @Override
    <T extends SimulationEvent> boolean isOverrun(final Class<T> clazz) {
        return false;
    }

    @Override
    <T extends SimulationEvent> void doResize(final Class<T> clazz) {
    }

}
