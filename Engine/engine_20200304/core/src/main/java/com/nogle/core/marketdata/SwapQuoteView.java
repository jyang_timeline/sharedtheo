package com.nogle.core.marketdata;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.Contract;
import com.nogle.util.Swapper;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.BestPriceOrderDetail;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.marketdata.type.OrderActions;
import com.timelinecapital.core.marketdata.type.OrderQueue;
import com.timelinecapital.core.marketdata.type.SimulationEvent;
import com.timelinecapital.core.marketdata.type.Snapshot;
import com.timelinecapital.core.marketdata.type.Tick;
import com.timelinecapital.core.types.UpdateType;
import com.timelinecapital.core.util.ContentTypeHelper;

public class SwapQuoteView extends QuoteView {
    private final Swapper<MarketBook> books;
    private final Swapper<Tick> ticks;
    private final Swapper<BestPriceOrderDetail> bestOrders;

    public SwapQuoteView(final Contract contract, final SbeVersion version) {
        super(contract, version);
        books = new Swapper<>(
            new MarketBook(contract, ParserInstanceFactory.getBookParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange())),
            new MarketBook(contract, ParserInstanceFactory.getBookParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange())));
        ticks = new Swapper<>(
            new Tick(contract, ParserInstanceFactory.getTickParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange())),
            new Tick(contract, ParserInstanceFactory.getTickParser(version, 0), ContentTypeHelper.fromFeedPreference(contract.getExchange())));
        bestOrders = new Swapper<>(
            new BestPriceOrderDetail(contract, ParserInstanceFactory.getBPODParser(version)),
            new BestPriceOrderDetail(contract, ParserInstanceFactory.getBPODParser(version)));
    }

    @Override
    void subscribe(final FeedType feedType) {
    }

    @Override
    public final MarketBook getBook() {
        return books.getCurrent();
    }

    @Override
    public final Tick getTick() {
        return ticks.getCurrent();
    }

    @Override
    public final BestPriceOrderDetail getBestPriceOrderDetail() {
        return bestOrders.getCurrent();
    }

    @Override
    public final OrderActions getOrderActions() {
        return null;
    }

    @Override
    public final OrderQueue getOrderQueue() {
        return null;
    }

    @Override
    public final Snapshot getSnapshot() {
        return null;
    }

    @Override
    public final UpdateType getUpdateType() {
        return UpdateType.ALL;
    }

    @Override
    public final void updateBook(final long updateTimeMillis, final ByteBuffer data) {
        books.getSpare().setData(data, updateTimeMillis);
        books.swap();
    }

    @Override
    public final void updateTick(final long updateTimeMillis, final ByteBuffer data) {
        ticks.getSpare().setData(data, updateTimeMillis);
        ticks.swap();
    }

    @Override
    public final void updateBestPriceOrderDetail(final long updateTimeMillis, final ByteBuffer data) {
        bestOrders.getSpare().setData(data, updateTimeMillis);
        bestOrders.swap();
    }

    @Override
    public final void updateOrderActions(final long updateTimeMillis, final ByteBuffer data) {
    }

    @Override
    public final void updateOrderQueue(final long updateTimeMillis, final ByteBuffer data) {
    }

    @Override
    public final void updateMarketDataSnapshot(final long updateTimeMillis, final ByteBuffer data) {
    }

    @Override
    public final void release() {
    }

    @Override
    <T extends SimulationEvent> boolean isOverrun(final Class<T> clazz) {
        return false;
    }

    @Override
    <T extends SimulationEvent> void doResize(final Class<T> clazz) {
    }

}
