package com.nogle.core.marketdata.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.marketdata.SimulationEventHandler;
import com.nogle.core.marketdata.producer.DynamicDepthMarketDataGenerator;
import com.nogle.core.marketdata.producer.EXGInfoMarketDataGenerator;
import com.nogle.core.marketdata.producer.MultiDepthMarketDataGenerator;
import com.nogle.core.marketdata.producer.QuoteDataGenerator;
import com.nogle.core.marketdata.producer.QuoteDataGenerator.EventPriority;
import com.nogle.core.scheduler.SimulatedTimer;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class CategorizedDataParser implements HistoricalQuoteParser {
    private static final Logger log = LogManager.getLogger(CategorizedDataParser.class);

    private QuoteDataGenerator[] dataGenerators;
    private long totalTime;
    private long count = 1;

    CategorizedDataParser() {
        dataGenerators = new QuoteDataGenerator[0];
    }

    @Override
    public void addFile(final String key, final File file, final SimulationEventHandler handler, final SbeVersion codecVersion) {
        try {
            QuoteDataGenerator dataGenerator = null;
            switch (codecVersion) {
                case PROT_BASIC:
                    dataGenerator = new MultiDepthMarketDataGenerator(key, codecVersion, file, handler);
                    break;
                case PROT_WITH_EXGTIME:
                    dataGenerator = new EXGInfoMarketDataGenerator(key, codecVersion, file, handler);
                    break;
                case PROT_DYNAMICLENGTH_BOOK:
                    dataGenerator = new DynamicDepthMarketDataGenerator(key, codecVersion, file, handler);
                    break;
                default:
                    log.error("No general data codec is specified !!!");
                    break;
            }
            if (dataGenerator != null) {
                dataGenerators = ArrayUtils.add(dataGenerators, dataGenerator);
                dataGenerator.onHeader();
                dataGenerator.onFormat();
            }
        } catch (final IOException e) {
            log.error("Fail to add file parser. Historical data does not exist: {}", e.getMessage());
        }
    }

    @Override
    public void clearFile() {
        for (int i = 0; i < dataGenerators.length; i++) {
            dataGenerators[i] = null;
        }
        dataGenerators = new QuoteDataGenerator[0];
    }

    @Override
    public void startParse() {
        if (dataGenerators.length == 0) {
            log.warn("No available data for parser");
            return;
        }

        final List<EventPriority<Long>> timestampList = new ArrayList<>();
        while (true) {
            timestampList.clear();
            for (final QuoteDataGenerator dataGenerator : dataGenerators) {
                if (dataGenerator.getEventPriority().getPrimary() == Long.MAX_VALUE) {
                    dataGenerators = ArrayUtils.removeElement(dataGenerators, dataGenerator);
                } else {
                    timestampList.add(dataGenerator.getEventPriority());
                }
            }

            if (timestampList.isEmpty()) {
                SimulatedTimer.getInstance().setTime(getTradingDayClosingTime());
                break;
            }
            final long start = System.nanoTime();
            final int minIndex = timestampList.indexOf(Collections.min(timestampList));
            dataGenerators[minIndex].publish();
            dataGenerators[minIndex].onNext();
            final long latency = System.nanoTime() - start;
            totalTime += latency;
            count++;
        }
        log.info("End of day. Average latency: {}us", (totalTime / count) / 1000L);
    }

    private long getTradingDayClosingTime() {
        return DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT).withHourOfDay(18).withMinuteOfHour(0).withSecondOfMinute(0).getMillis();
    }

}
