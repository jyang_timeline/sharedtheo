package com.nogle.core.marketdata.parser;

import java.io.File;

import com.nogle.core.marketdata.SimulationEventHandler;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public interface HistoricalQuoteParser {

    void addFile(String key, File file, SimulationEventHandler handler, SbeVersion codecVersion);

    void clearFile();

    void startParse();

}
