package com.nogle.core.marketdata.parser;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.nogle.core.marketdata.SimulationEventHandler;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class HistoricalQuoteParserFactory {

    private final Map<Contract, SimulationEventHandler> contractToEventHandler = new HashMap<>();
    private final HistoricalQuoteParser parser = new CategorizedDataParser();

    public HistoricalQuoteParserFactory() {
    }

    public void addFeedEventHandler(final Contract contract, final SimulationEventHandler handler) {
        contractToEventHandler.put(contract, handler);
    }

    public void clearEventHandler() {
        contractToEventHandler.clear();
    }

    public void init(final Map<Contract, File[]> sources, final SbeVersion codecVersion) {
        for (final Map.Entry<Contract, File[]> entry : sources.entrySet()) {
            for (final File file : entry.getValue()) {
                parser.addFile(entry.getKey().getSymbol(), file, contractToEventHandler.get(entry.getKey()), codecVersion);
            }
        }
    }

    public void init(final Map<Contract, List<Entry<File, SbeVersion>>> sources) {
        for (final Map.Entry<Contract, List<Entry<File, SbeVersion>>> entry : sources.entrySet()) {
            for (final Entry<File, SbeVersion> eachEntry : entry.getValue()) {
                parser.addFile(entry.getKey().getSymbol(), eachEntry.getKey(), contractToEventHandler.get(entry.getKey()), eachEntry.getValue());
            }
            // parser.addFile(entry.getKey().getSymbol(), entry.getValue().getKey(), contractToEventHandler.get(entry.getKey()),
            // entry.getValue().getValue());

            // for (final Map.Entry<File, Integer> fileEntry : entry.getValue()) {
            // parser.addFile(entry.getKey().getSymbol(), fileEntry.getKey(), contractToEventHandler.get(entry.getKey()),
            // fileEntry.getValue());
            // }
        }
    }

    public void startParse() {
        parser.startParse();
    }

}
