package com.nogle.core.marketdata.producer;

public interface BinaryDataGenerator {

    void generateMarketbook(String[] raw);

    void generateTick(String[] raw);

    default void generateBestPriceDetail(final String[] raw) {
    }

    default void generateOrderAction(final String[] raw) {
    }

    default void generateOrderQueue(final String[] raw) {
    }

}
