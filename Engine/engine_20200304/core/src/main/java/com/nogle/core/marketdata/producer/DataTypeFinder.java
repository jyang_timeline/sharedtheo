package com.nogle.core.marketdata.producer;

import com.timelinecapital.commons.type.DataType;

public interface DataTypeFinder {

    DataType getDataType();

}
