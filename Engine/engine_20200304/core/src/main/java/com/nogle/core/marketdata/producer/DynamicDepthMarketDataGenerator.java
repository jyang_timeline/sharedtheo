package com.nogle.core.marketdata.producer;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.marketdata.SimulationEventHandler;
import com.nogle.core.marketdata.producer.metadata.BestPriceDetail;
import com.nogle.core.marketdata.producer.metadata.MarketBookV3;
import com.nogle.core.marketdata.producer.metadata.OrderAction;
import com.nogle.core.marketdata.producer.metadata.OrderQueue;
import com.nogle.core.marketdata.producer.metadata.QuoteHeaderEXGInfo;
import com.nogle.core.marketdata.producer.metadata.TradeV3;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.BestPriceOrderDetailEncoder;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderActionsEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderQueueEncoder;
import com.timelinecapital.commons.marketdata.encoder.TradeEncoder;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public class DynamicDepthMarketDataGenerator extends QuoteDataGenerator {
    private static Logger log = LogManager.getLogger(DynamicDepthMarketDataGenerator.class);

    private static final String VERSION_INDECATOR = "#";

    private final SbeVersion version;
    private MarketBookEncoder mbEncoder;
    private TradeEncoder tkEncoder;
    private BestPriceOrderDetailEncoder detailEncoder;
    private OrderActionsEncoder actionEncoder;
    private OrderQueueEncoder queueEncoder;

    private String nextLine;
    private String[] raw;

    private TimeWrapper timeWrapper;

    public DynamicDepthMarketDataGenerator(final String key, final SbeVersion version, final File file, final SimulationEventHandler eventHandler) throws IOException {
        super(key, file, eventHandler);
        this.version = version;
    }

    @Override
    public void onHeader() {
        try {
            final String versionInfo = reader.readLine();
            mbEncoder = EncoderInstanceFactory.getBookEncoder(version, 0);
            tkEncoder = EncoderInstanceFactory.getTickEncoder(version, 0);
            detailEncoder = EncoderInstanceFactory.getBPODEncoder(version);
            actionEncoder = EncoderInstanceFactory.getActionEncoder(version);
            queueEncoder = EncoderInstanceFactory.getQueueEncoder(version);
            log.info("{} Binary version: {} vs {}", key, versionInfo, version);
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onFormat() {
        try {
            nextLine = reader.readLine();
            raw = nextLine.split(DelimiterUtil.MARKETDATA_DELIMETER);
            timeWrapper = TimeAdapterFactory.getTimeWrapper(
                TradeClock.getCalendarMillis(),
                raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()],
                raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);

            receiveTime = NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]);
            exchangeTime = timeWrapper.get(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));
            setEventPriority(exchangeTime, receiveTime);
            // setEventTime();

            onDataType((byte) raw[QuoteHeaderEXGInfo.Type.getIndex()].charAt(0));
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onNext() {
        try {
            if ((nextLine = reader.readLine()) != null) {
                if (nextLine.startsWith(VERSION_INDECATOR)) {
                    log.info(nextLine);
                    return;
                }
                process();
            } else {
                receiveTime = Long.MAX_VALUE;
                exchangeTime = Long.MAX_VALUE;
                setEventPriority(exchangeTime, receiveTime);
                reader.close();
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void process() throws IOException {
        raw = nextLine.split(DelimiterUtil.MARKETDATA_DELIMETER);

        receiveTime = NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]);
        exchangeTime = timeWrapper.get(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));
        setEventPriority(exchangeTime, receiveTime);

        onDataType((byte) raw[QuoteHeaderEXGInfo.Type.getIndex()].charAt(0));
    }

    private void onDataType(final byte typeData) {
        final DataType dataType = DataType.decode(typeData);
        switch (dataType) {
            case MARKETBOOK:
            case GEN_MARKETBOOK:
            case REALTIME_MARKETBOOK:
                generateMarketbook(raw);
                break;
            case TICK:
            case GEN_TICK:
            case REALTIME_FILL:
                generateTick(raw);
                break;
            case BESTORDERDETAIL:
                generateBestPriceDetail(raw);
                break;
            case SESSION:
                log.warn("Not support yet!!!");
                break;
            case SNAPSHOT:
                log.warn("Not support yet!!!");
                break;
            case ORDERACTIONS:
                generateOrderAction(raw);
                break;
            case ORDERQUEUE:
                generateOrderQueue(raw);
                break;
        }
    }

    @Override
    public void generateMarketbook(final String[] raw) {
        mbEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        mbEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        mbEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        mbEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        mbEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        mbEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
        mbEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        mbEncoder.setQuoteType(raw[MarketBookV3.QuoteType.getIndex()].getBytes()[0]);
        final int bidDepth = NumberUtils.toInt(raw[MarketBookV3.BidDepth.getIndex()]);
        mbEncoder.setBidDepth(bidDepth);
        final int askDepth = NumberUtils.toInt(raw[MarketBookV3.AskDepth.getIndex()]);
        mbEncoder.setAskDepth(askDepth);

        for (int i = 0; i < bidDepth; i++) {
            final String[] bid = raw[MarketBookV3.BookInfo.getIndex() + i].split("@");
            mbEncoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
            mbEncoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
        }
        for (int i = 0; i < askDepth; i++) {
            final String[] ask = raw[MarketBookV3.BookInfo.getIndex() + bidDepth + i].split("@");
            mbEncoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
            mbEncoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
        }

        next = new MarketData(DataType.MARKETBOOK, mbEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateTick(final String[] raw) {
        tkEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        tkEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        tkEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        tkEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        tkEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        tkEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
        tkEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        tkEncoder.setTradeType(TickConverter.valueOf(raw[TradeV3.Aggressor.getIndex()]).getValue());
        tkEncoder.setPrice(NumberUtils.toDouble(raw[TradeV3.Price.getIndex()], 0.0d));
        tkEncoder.setQuantity(NumberUtils.toLong(raw[TradeV3.Qty.getIndex()], 0l));
        tkEncoder.setOpenInterest(NumberUtils.toLong(raw[TradeV3.OpenInterest.getIndex()]));
        tkEncoder.setVolume(NumberUtils.toLong(raw[TradeV3.Volume.getIndex()]));
        tkEncoder.setTradeId(NumberUtils.toLong(raw[TradeV3.TradeID.getIndex()]));
        tkEncoder.setBuyerOrderId(NumberUtils.toLong(raw[TradeV3.BuyerOrderID.getIndex()]));
        tkEncoder.setSellerOrderId(NumberUtils.toLong(raw[TradeV3.SellerOrderID.getIndex()]));

        next = new MarketData(DataType.TICK, tkEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateBestPriceDetail(final String[] raw) {
        detailEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        detailEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        detailEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        detailEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        detailEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        detailEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
        detailEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        detailEncoder.setBestBidPrice(NumberUtils.toDouble(raw[BestPriceDetail.BidPrice.getIndex()], 0.0d));
        detailEncoder.setBestAskPrice(NumberUtils.toDouble(raw[BestPriceDetail.AskPrice.getIndex()], 0.0d));
        for (int i = 0; i < 10; i++) {
            detailEncoder.setBestBidOrderQty(i, NumberUtils.toLong(raw[BestPriceDetail.BidOrderDetail.getIndex() + i], 0l));
            detailEncoder.setBestAskOrderQty(i, NumberUtils.toLong(raw[BestPriceDetail.AskOrderDetail.getIndex() + i], 0l));
        }

        next = new MarketData(DataType.BESTORDERDETAIL, detailEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateOrderAction(final String[] raw) {
        actionEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        actionEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        actionEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        actionEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        actionEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        actionEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
        actionEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        actionEncoder.setActionType(raw[OrderAction.Action.getIndex()].getBytes()[0]);
        actionEncoder.setOrderId(NumberUtils.toLong(raw[OrderAction.ID.getIndex()]));
        actionEncoder.setOrderSide(TickConverter.valueOf(raw[OrderAction.Side.getIndex()]).getValue());
        actionEncoder.setOrderTimeCondition(TimeConditionConverter.valueOf(raw[OrderAction.Timecondition.getIndex()]).getValue());
        actionEncoder.setOrderType(raw[OrderAction.OrderType.getIndex()].getBytes()[0]);
        actionEncoder.setOrderPrice(NumberUtils.toDouble(raw[OrderAction.Price.getIndex()], 0.0d));
        actionEncoder.setOrderQty(NumberUtils.toLong(raw[OrderAction.Qty.getIndex()]));

        next = new MarketData(DataType.ORDERACTIONS, actionEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateOrderQueue(final String[] raw) {
        queueEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        queueEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        queueEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        queueEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        queueEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        queueEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
        queueEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        final byte sideInfo = TickConverter.valueOf(raw[OrderQueue.Side.getIndex()]).getValue();
        queueEncoder.setSide(sideInfo);
        queueEncoder.setPrice(sideInfo, NumberUtils.toDouble(raw[OrderQueue.Price.getIndex()], 0.0d));
        final short depth = NumberUtils.toShort(raw[OrderQueue.Count.getIndex()]);
        queueEncoder.setCount(depth);
        for (int i = 0; i < depth; i++) {
            queueEncoder.setOrder(sideInfo, i, NumberUtils.toInt(raw[OrderQueue.Info.getIndex()] + i));
        }

        next = new MarketData(DataType.ORDERQUEUE, queueEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

}
