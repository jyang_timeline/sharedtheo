package com.nogle.core.marketdata.producer;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.marketdata.SimulationEventHandler;
import com.nogle.core.marketdata.producer.metadata.BestPriceDetail;
import com.nogle.core.marketdata.producer.metadata.MarketBookEXGInfo;
import com.nogle.core.marketdata.producer.metadata.OrderAction;
import com.nogle.core.marketdata.producer.metadata.QuoteHeaderEXGInfo;
import com.nogle.core.marketdata.producer.metadata.TradeEXGInfo;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.BestPriceOrderDetailEncoder;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderActionsEncoder;
import com.timelinecapital.commons.marketdata.encoder.TradeEncoder;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public class EXGInfoMarketDataGenerator extends QuoteDataGenerator {
    private static final Logger log = LogManager.getLogger(EXGInfoMarketDataGenerator.class);

    private static final String VERSION_INDECATOR = "#";

    private final SbeVersion version;
    private MarketBookEncoder mbEncoder;
    private TradeEncoder tsEncoder;
    private BestPriceOrderDetailEncoder boEncoder;
    private OrderActionsEncoder qoEncoder;

    private String nextLine;
    private String[] raw;

    private TimeWrapper timeWrapper;

    public EXGInfoMarketDataGenerator(final String key, final SbeVersion version, final File file, final SimulationEventHandler eventHandler) throws IOException {
        super(key, file, eventHandler);
        this.version = version;
    }

    @Override
    public void onHeader() {
        try {
            final String versionInfo = reader.readLine();
            mbEncoder = EncoderInstanceFactory.getBookEncoder(version, 0);
            tsEncoder = EncoderInstanceFactory.getTickEncoder(version, 0);
            boEncoder = EncoderInstanceFactory.getBPODEncoder(version);
            qoEncoder = EncoderInstanceFactory.getActionEncoder(version);
            log.info("{} Binary version: {} vs {}", key, versionInfo, version);
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onFormat() {
        try {
            nextLine = reader.readLine();
            raw = nextLine.split(DelimiterUtil.MARKETDATA_DELIMETER);
            timeWrapper = TimeAdapterFactory.getTimeWrapper(
                TradeClock.getCalendarMillis(),
                raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()],
                raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);

            receiveTime = NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]);
            exchangeTime = timeWrapper.get(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));
            setEventPriority(exchangeTime, receiveTime);
            // setEventTime();

            onDataType((byte) raw[QuoteHeaderEXGInfo.Type.getIndex()].charAt(0));
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onNext() {
        try {
            if ((nextLine = reader.readLine()) != null) {
                if (nextLine.startsWith(VERSION_INDECATOR)) {
                    log.info(nextLine);
                    return;
                }
                process();
            } else {
                receiveTime = Long.MAX_VALUE;
                exchangeTime = Long.MAX_VALUE;
                setEventPriority(exchangeTime, receiveTime);
                reader.close();
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void process() throws IOException {
        raw = nextLine.split(DelimiterUtil.MARKETDATA_DELIMETER);

        receiveTime = NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]);
        exchangeTime = timeWrapper.get(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));
        setEventPriority(exchangeTime, receiveTime);

        onDataType((byte) raw[QuoteHeaderEXGInfo.Type.getIndex()].charAt(0));
    }

    private void onDataType(final byte typeData) {
        final DataType dataType = DataType.decode(typeData);
        switch (dataType) {
            case MARKETBOOK:
            case GEN_MARKETBOOK:
            case REALTIME_MARKETBOOK:
                generateMarketbook(raw);
                break;
            case TICK:
            case GEN_TICK:
            case REALTIME_FILL:
                generateTick(raw);
                break;
            case BESTORDERDETAIL:
                generateBestPriceDetail(raw);
                break;
            case SESSION:
                log.warn("Not support yet!!!");
                break;
            case SNAPSHOT:
                log.warn("Not support yet!!!");
                break;
            case ORDERACTIONS:
                generateOrderAction(raw);
                break;
            case ORDERQUEUE:
                log.warn("Not support yet!!!");
                break;
        }
    }

    @Override
    public void generateMarketbook(final String[] raw) {
        mbEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        mbEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        mbEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        mbEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        mbEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        mbEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        mbEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        mbEncoder.setUpdateFlag(raw[MarketBookEXGInfo.UpdateFlag.getIndex()].charAt(0));
        mbEncoder.setQuoteType(raw[MarketBookEXGInfo.QuoteType.getIndex()].getBytes()[0]);
        final int bidDepth = NumberUtils.toInt(raw[MarketBookEXGInfo.BidDepth.getIndex()]);
        mbEncoder.setBidDepth(bidDepth);
        final int askDepth = NumberUtils.toInt(raw[MarketBookEXGInfo.AskDepth.getIndex()]);
        mbEncoder.setAskDepth(askDepth);

        for (int i = 0; i < Math.max(bidDepth, askDepth); i++) {
            final String[] bid = raw[MarketBookEXGInfo.BidInfoIdx.getIndex() + i * 2].split("@");
            final String[] ask = raw[MarketBookEXGInfo.AskInfoIdx.getIndex() + i * 2].split("@");
            mbEncoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
            mbEncoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
            mbEncoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
            mbEncoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
        }

        next = new MarketData(DataType.MARKETBOOK, mbEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateTick(final String[] raw) {
        tsEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        tsEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        tsEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        tsEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        tsEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        tsEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        tsEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        tsEncoder.setTradeType(TickConverter.valueOf(raw[TradeEXGInfo.Aggressor.getIndex()]).getValue());
        tsEncoder.setPrice(NumberUtils.toDouble(raw[TradeEXGInfo.Price.getIndex()], 0.0d));
        tsEncoder.setQuantity(NumberUtils.toLong(raw[TradeEXGInfo.Qty.getIndex()], 0l));
        tsEncoder.setOpenInterest(NumberUtils.toLong(raw[TradeEXGInfo.OpenInterest.getIndex()]));
        tsEncoder.setVolume(NumberUtils.toLong(raw[TradeEXGInfo.Volume.getIndex()]));

        next = new MarketData(DataType.TICK, tsEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateBestPriceDetail(final String[] raw) {
        boEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        boEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        boEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        boEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        boEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        boEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        boEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        boEncoder.setBestBidPrice(NumberUtils.toDouble(raw[BestPriceDetail.BidPrice.getIndex()], 0.0d));
        boEncoder.setBestAskPrice(NumberUtils.toDouble(raw[BestPriceDetail.AskPrice.getIndex()], 0.0d));
        for (int i = 0; i < 10; i++) {
            boEncoder.setBestBidOrderQty(i, NumberUtils.toLong(raw[BestPriceDetail.BidOrderDetail.getIndex() + i], 0l));
            boEncoder.setBestAskOrderQty(i, NumberUtils.toLong(raw[BestPriceDetail.AskOrderDetail.getIndex() + i], 0l));
        }

        next = new MarketData(DataType.BESTORDERDETAIL, boEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    @Override
    public void generateOrderAction(final String[] raw) {
        qoEncoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        qoEncoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        qoEncoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        qoEncoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        qoEncoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        qoEncoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        qoEncoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        qoEncoder.setActionType(raw[OrderAction.Action.getIndex()].getBytes()[0]);
        qoEncoder.setOrderId(NumberUtils.toLong(raw[OrderAction.ID.getIndex()]));
        qoEncoder.setOrderSide(TickConverter.valueOf(raw[OrderAction.Side.getIndex()]).getValue());
        qoEncoder.setOrderTimeCondition(TimeConditionConverter.valueOf(raw[OrderAction.Timecondition.getIndex()]).getValue());
        qoEncoder.setOrderType(raw[OrderAction.OrderType.getIndex()].getBytes()[0]);
        qoEncoder.setOrderPrice(NumberUtils.toDouble(raw[OrderAction.Price.getIndex()], 0.0d));
        qoEncoder.setOrderQty(NumberUtils.toLong(raw[OrderAction.Qty.getIndex()]));

        next = new MarketData(DataType.ORDERACTIONS, qoEncoder.getBuffer(), NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
    }

    // public static void main(final String[] args) throws IOException {
    // try (
    // ICsvListReader listReader = new CsvListReader(new FileReader(SAMPLE_CSV_FILE_PATH), CsvPreference.STANDARD_PREFERENCE)) {
    // // First Column is header names- though we don't need it in runtime
    // @SuppressWarnings("unused")
    // final String[] headers = listReader.getHeader(true);
    //
    // List<String> fieldsInCurrentRow;
    // while ((fieldsInCurrentRow = listReader.read()) != null) {
    //
    // // if (fieldsInCurrentRow.size() == 5) {
    // // processors = getFiveColumnProcessors();
    // // } else if (fieldsInCurrentRow.size() == 4) {
    // // processors = getFourColumnProcessors();
    // // } else if (fieldsInCurrentRow.size() == 3) {
    // // processors = getThreeColumnProcessors();
    // // } else {
    // // // Create more processors
    // // }
    // final List<Object> formattedFields = listReader.executeProcessors(processors);
    // System.out.println(String.format("rowNo=%s, customerList=%s", listReader.getRowNumber(), formattedFields));
    // }
    // }
    // }

    // private static CellProcessor[] getTradeCellProcessor() {
    // final CellProcessor[] processors = new CellProcessor[] {
    // new ParseLong(),
    // new ParseLong(),
    //
    // };
    // return processors;
    // }

    // private static CellProcessor[] getFiveColumnProcessors() {
    // final String emailRegex = "[a-z0-9\\._]+@[a-z0-9\\.]+";
    // StrRegEx.registerMessage(emailRegex, "must be a valid email address");
    //
    // final CellProcessor[] processors = new CellProcessor[] {
    // new NotNull(), // Name
    // new NotNull(), // Email
    // new NotNull(), // Phone
    // new Optional(), // Country
    // new StrRegEx(emailRegex) // Email
    // };
    // return processors;
    // }

    // final CSVParser parser =
    // new CSVParserBuilder()
    // .withEscapeChar('#')
    // .withIgnoreQuotations(true)
    // .build();
    // final CSVReader csvReader =
    // new CSVReaderBuilder(new StringReader(SAMPLE_CSV_FILE_PATH))
    // .withSkipLines(1)
    // .withCSVParser(parser)
    // .build();

    // try (
    // BufferedReader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
    // CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
    // .withCommentMarker('#')
    // .withHeader("Name", "Email", "Phone", "Country")
    // .withAllowMissingColumnNames(true)
    // .withIgnoreHeaderCase()
    // .withTrim());) {
    // for (final CSVRecord csvRecord : csvParser) {
    // // Accessing values by the names assigned to each column
    // final String name = csvRecord.get("Name");
    // final String email = csvRecord.get("Email");
    // final String phone = csvRecord.get("Phone");
    // final String country = csvRecord.get("Country");
    //
    // System.out.println("Record No - " + csvRecord.getRecordNumber());
    // System.out.println("---------------");
    // System.out.println("Name : " + name);
    // System.out.println("Email : " + email);
    // System.out.println("Phone : " + phone);
    // System.out.println("Country : " + country);
    // System.out.println("---------------\n\n");
    // }
    // }
    // }

}
