package com.nogle.core.marketdata.producer;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.marketdata.SimulationEventHandler;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.BestPriceOrderDetailEncoder;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderActionsEncoder;
import com.timelinecapital.commons.marketdata.encoder.TradeEncoder;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;

public class MultiDepthMarketDataGenerator extends QuoteDataGenerator {
    private static final Logger log = LogManager.getLogger(MultiDepthMarketDataGenerator.class);

    private static final int bookBidIdx = 9;
    private static final int bookAskIdx = 10;

    private final SbeVersion version;
    private MarketBookEncoder mbEncoder;
    private TradeEncoder tsEncoder;
    private BestPriceOrderDetailEncoder boEncoder;
    private OrderActionsEncoder qoEncoder;

    private String nextLine;
    private String[] raw;
    private DataType dataType;

    public MultiDepthMarketDataGenerator(final String key, final SbeVersion version, final File file, final SimulationEventHandler eventHandler) throws IOException {
        super(key, file, eventHandler);
        this.version = version;
    }

    @Override
    public void onHeader() {
        try {
            final String versionInfo = reader.readLine();
            mbEncoder = EncoderInstanceFactory.getBookEncoder(version, 0);
            tsEncoder = EncoderInstanceFactory.getTickEncoder(version, 0);
            boEncoder = EncoderInstanceFactory.getBPODEncoder(version);
            qoEncoder = EncoderInstanceFactory.getActionEncoder(version);
            log.info("{} Binary version: {} vs {}", key, versionInfo, version);
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onFormat() {
        try {
            nextLine = reader.readLine();
            process();
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onNext() {
        try {
            if ((nextLine = reader.readLine()) != null) {
                process();
            } else {
                receiveTime = Long.MAX_VALUE;
                setEventPriority(receiveTime, receiveTime);
                reader.close();
            }
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void process() throws IOException {
        raw = nextLine.split(DelimiterUtil.MARKETDATA_DELIMETER);

        receiveTime = NumberUtils.toLong(raw[0]);
        exchangeTime = receiveTime;
        setEventPriority(receiveTime, receiveTime);

        dataType = DataType.decode(raw[2].getBytes()[0]);
        switch (dataType) {
            case MARKETBOOK:
            case GEN_MARKETBOOK:
            case REALTIME_MARKETBOOK:
                generateMarketbook(raw);
                break;
            case REALTIME_FILL:
                generateRTT(raw);
                break;
            case TICK:
            case GEN_TICK:
                generateTick(raw);
                break;
            case BESTORDERDETAIL:
                generateBestPriceDetail(raw);
                break;
            case SESSION:
                log.warn("Not support yet!!!");
                break;
            case SNAPSHOT:
                log.warn("Not support yet!!!");
                break;
            case ORDERACTIONS:
                generateOrderAction(raw);
                break;
            case ORDERQUEUE:
                log.warn("Not support yet!!!");
                break;
        }
    }

    @Override
    public void generateMarketbook(final String[] raw) {
        final long recv = NumberUtils.toLong(raw[0]);

        mbEncoder.setVersion(raw[1].getBytes()[0]);
        mbEncoder.setType(raw[2].getBytes()[0]);
        mbEncoder.setSymbol(raw[3]);
        mbEncoder.setSequenceNo(NumberUtils.toLong(raw[4]));
        mbEncoder.setMicroTime(recv);

        mbEncoder.setUpdateFlag(raw[5].charAt(0));
        mbEncoder.setQuoteType(raw[6].getBytes()[0]);
        final int bidDepth = NumberUtils.toInt(raw[7]);
        mbEncoder.setBidDepth(bidDepth);
        final int askDepth = NumberUtils.toInt(raw[8]);
        mbEncoder.setAskDepth(askDepth);

        for (int i = 0; i < Math.max(bidDepth, askDepth); i++) {
            final String[] bid = raw[bookBidIdx + i * 2].split("@");
            final String[] ask = raw[bookAskIdx + i * 2].split("@");
            mbEncoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
            mbEncoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
            mbEncoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
            mbEncoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
        }
        next = new MarketData(DataType.MARKETBOOK, mbEncoder.getBuffer(), recv);
    }

    @Override
    public void generateTick(final String[] raw) {
        final long recv = NumberUtils.toLong(raw[0]);

        tsEncoder.setVersion(raw[1].getBytes()[0]);
        tsEncoder.setType(raw[2].getBytes()[0]);
        tsEncoder.setSymbol(raw[3]);
        tsEncoder.setSequenceNo(NumberUtils.toLong(raw[4]));
        tsEncoder.setMicroTime(recv);

        tsEncoder.setTradeType(TickConverter.valueOf(raw[5]).getValue());
        tsEncoder.setPrice(NumberUtils.toDouble(raw[6], 0.0d));
        tsEncoder.setQuantity(NumberUtils.toLong(raw[7], 0l));
        tsEncoder.setOpenInterest(NumberUtils.toLong(raw[8]));
        tsEncoder.setVolume(NumberUtils.toLong(raw[9]));

        next = new MarketData(DataType.TICK, tsEncoder.getBuffer(), recv);
    }

    public void generateRTT(final String[] raw) {
        final long recv = NumberUtils.toLong(raw[0]);

        tsEncoder.setVersion(raw[1].getBytes()[0]);
        tsEncoder.setType(DataType.REALTIME_FILL.getCode());
        tsEncoder.setSymbol(raw[3]);
        tsEncoder.setSequenceNo(NumberUtils.toLong(raw[4]));
        tsEncoder.setMicroTime(recv);

        tsEncoder.setTradeType(TickConverter.valueOf(raw[5]).getValue());
        tsEncoder.setPrice(NumberUtils.toDouble(raw[6], 0.0d));
        tsEncoder.setQuantity(NumberUtils.toLong(raw[7], 0l));
        tsEncoder.setOpenInterest(NumberUtils.toLong(raw[8]));
        tsEncoder.setVolume(NumberUtils.toLong(raw[9]));

        next = new MarketData(DataType.REALTIME_FILL, tsEncoder.getBuffer(), recv);
    }

    @Override
    public void generateBestPriceDetail(final String[] raw) {
        final long recv = NumberUtils.toLong(raw[0]);

        boEncoder.setVersion(raw[1].getBytes()[0]);
        boEncoder.setType(DataType.BESTORDERDETAIL.getCode());
        boEncoder.setSymbol(raw[3]);
        boEncoder.setSequenceNo(NumberUtils.toLong(raw[4]));
        boEncoder.setMicroTime(recv);

        boEncoder.setBestBidPrice(NumberUtils.toDouble(raw[5], 0.0d));
        boEncoder.setBestAskPrice(NumberUtils.toDouble(raw[16], 0.0d));
        for (int i = 0; i < 10; i++) {
            boEncoder.setBestBidOrderQty(i, NumberUtils.toLong(raw[6 + i], 0l));
            boEncoder.setBestAskOrderQty(i, NumberUtils.toLong(raw[17 + i], 0l));
        }

        next = new MarketData(DataType.BESTORDERDETAIL, boEncoder.getBuffer(), recv);
    }

    @Override
    public void generateOrderAction(final String[] raw) {
        final long recv = NumberUtils.toLong(raw[0]);

        qoEncoder.setVersion(raw[1].getBytes()[0]);
        qoEncoder.setType(DataType.ORDERACTIONS.getCode());
        qoEncoder.setSymbol(raw[3]);
        qoEncoder.setSequenceNo(NumberUtils.toLong(raw[4]));
        qoEncoder.setMicroTime(recv);

        qoEncoder.setActionType(raw[5].getBytes()[0]);
        qoEncoder.setOrderId(NumberUtils.toLong(raw[6]));
        qoEncoder.setOrderSide(TickConverter.valueOf(raw[7]).getValue());
        qoEncoder.setOrderTimeCondition(TimeConditionConverter.valueOf(raw[8]).getValue());
        qoEncoder.setOrderType(raw[9].getBytes()[0]);
        qoEncoder.setOrderPrice(NumberUtils.toDouble(raw[10], 0.0d));
        qoEncoder.setOrderQty(NumberUtils.toLong(raw[11]));

        next = new MarketData(DataType.ORDERACTIONS, qoEncoder.getBuffer(), recv);
    }

}
