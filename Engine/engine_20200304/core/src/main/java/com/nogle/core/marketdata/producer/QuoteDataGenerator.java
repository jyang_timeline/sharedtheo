package com.nogle.core.marketdata.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.marketdata.SimulationEventHandler;
import com.nogle.core.scheduler.SimulatedTimer;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.SimulationConfig;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

public abstract class QuoteDataGenerator implements BinaryDataGenerator {

    protected final BufferedReader reader;
    protected final EventPriority<Long> eventPriority = new EventPriority<>();

    protected long receiveTime;
    protected long exchangeTime;

    protected MarketData current;
    protected MarketData next;

    private static final String SEPARATOR = " @ ";
    private static final String PATH_SMB = "smb:";

    protected final String key;
    protected final long clockDrift;
    private final SimulationEventHandler eventHandler;

    public QuoteDataGenerator(final String key, final File file, final SimulationEventHandler eventHandler) throws IOException {
        this.key = key;
        this.eventHandler = eventHandler;
        this.clockDrift = SimulationConfig.getClockDrift();

        if (file.getPath().contains(PATH_SMB)) {
            final SmbFile smbFile = new SmbFile(file.getPath().replaceAll(":/", "://").replaceAll("\\\\", "//"));
            reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(smbFile), StandardCharsets.UTF_8));
        } else {
            reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
        }
    }

    public EventPriority<Long> getEventPriority() {
        return eventPriority;
    }

    public void setEventPriority(final long primary, final long secondary) {
        eventPriority.primary = primary;
        eventPriority.secondary = secondary;
    }

    public abstract void onHeader();

    public abstract void onFormat();

    public abstract void onNext();

    public void publish() {
        SimulatedTimer.getInstance().setTime(NogleTimeUnit.MICROSECONDS.toMillis(exchangeTime));
        current = next;
        eventHandler.onEvent(exchangeTime, key, current.getRecvTime(), current.getDataType(), current.getBinaryData());
    }

    public class EventPriority<T> implements Comparable<EventPriority<T>> {
        private long primary;
        private long secondary;

        public long getPrimary() {
            return primary;
        }

        public long getSecondary() {
            return secondary;
        }

        @Override
        public int compareTo(final EventPriority<T> o) {
            final int result = Long.compare(primary, o.primary);
            if (result == 0) {
                return Long.compare(secondary, o.secondary);
            }
            return result;
        }

        @Override
        public String toString() {
            return primary + SEPARATOR + secondary;
        }
    }

    class MarketData {
        private final long exchTime;
        private final long recvTime;
        private final DataType dataType;
        private final ByteBuffer binaryData;

        MarketData(final DataType dataType, final ByteBuffer binaryData, final long exchTime, final long recvTime) {
            this.dataType = dataType;
            this.binaryData = binaryData;
            this.exchTime = exchTime;
            this.recvTime = recvTime;
        }

        MarketData(final DataType dataType, final ByteBuffer binaryData, final long recvTime) {
            this.dataType = dataType;
            this.binaryData = binaryData;
            this.exchTime = recvTime;
            this.recvTime = recvTime;
        }

        long getExchTime() {
            return exchTime;
        }

        long getRecvTime() {
            return recvTime;
        }

        DataType getDataType() {
            return dataType;
        }

        ByteBuffer getBinaryData() {
            return binaryData;
        }

    }

    enum TickConverter {
        BID('0'),
        BUY('0'),
        ASK('1'),
        SELL('1'),
        AVERAGE('2'),
        NO_AGGRESSOR('3');

        private final char value;

        private TickConverter(final char value) {
            this.value = value;
        }

        public byte getValue() {
            return (byte) value;
        }
    }

    enum TimeConditionConverter {
        GFD('D'),
        GTC('C'),
        IOC('I'),
        GFS('S');

        private final char value;

        private TimeConditionConverter(final char value) {
            this.value = value;
        }

        public byte getValue() {
            return (byte) value;
        }
    }

}
