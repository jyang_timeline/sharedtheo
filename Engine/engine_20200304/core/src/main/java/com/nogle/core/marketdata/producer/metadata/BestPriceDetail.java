package com.nogle.core.marketdata.producer.metadata;

public enum BestPriceDetail {

    BidPrice(0),
    AskPrice(11),
    BidOrderDetail(1),
    AskOrderDetail(12),
    ;

    int index;

    private BestPriceDetail(final int index) {
        this.index = index + QuoteHeaderDefault.values().length;
    }

    public int getIndex() {
        return index;
    }

}
