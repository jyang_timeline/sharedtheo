package com.nogle.core.marketdata.producer.metadata;

public enum MarketBookDefault {

    /* MarketBook */
    UpdateFlag(0),
    QuoteType(1),
    BidDepth(2),
    AskDepth(3),
    ;

    int index;

    private MarketBookDefault(final int index) {
        this.index = index + QuoteHeaderDefault.values().length;
    }

    public int getIndex() {
        return index;
    }

}
