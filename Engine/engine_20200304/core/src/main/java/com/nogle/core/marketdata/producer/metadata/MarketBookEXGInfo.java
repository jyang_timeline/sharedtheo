package com.nogle.core.marketdata.producer.metadata;

public enum MarketBookEXGInfo {

    /* Header part */
    Exchange_Timestamp(0),
    Timestamp(1),
    Version(2),
    Type(3),
    Symbol(4),
    Exchange_SeqNo(5),
    SeqNo(6),

    /* MarketBook */
    UpdateFlag(7),
    QuoteType(8),
    BidDepth(9),
    AskDepth(10),
    BidInfoIdx(11),
    AskInfoIdx(12),
    ;

    int index;

    private MarketBookEXGInfo(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
