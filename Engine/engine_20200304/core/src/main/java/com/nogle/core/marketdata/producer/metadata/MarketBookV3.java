package com.nogle.core.marketdata.producer.metadata;

public enum MarketBookV3 {

    /* Header part */
    Exchange_Timestamp(0),
    Timestamp(1),
    Version(2),
    Type(3),
    Symbol(4),
    Exchange_SeqNo(5),
    SeqNo(6),

    /* MarketBook */
    QuoteType(7),
    BidDepth(8),
    AskDepth(9),
    BookInfo(10),
    ;

    int index;

    private MarketBookV3(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
