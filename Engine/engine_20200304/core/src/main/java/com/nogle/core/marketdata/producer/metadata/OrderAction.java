package com.nogle.core.marketdata.producer.metadata;

public enum OrderAction {

    Action(0),
    ID(1),
    Side(2),
    Timecondition(3),
    OrderType(4),
    Price(5),
    Qty(6),
    ;

    int index;

    private OrderAction(final int index) {
        this.index = index + QuoteHeaderEXGInfo.values().length;
    }

    public int getIndex() {
        return index;
    }

}
