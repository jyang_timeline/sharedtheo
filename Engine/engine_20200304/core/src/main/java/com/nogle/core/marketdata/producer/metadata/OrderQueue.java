package com.nogle.core.marketdata.producer.metadata;

public enum OrderQueue {

    /* Header part */
    Exchange_Timestamp(0),
    Timestamp(1),
    Version(2),
    Type(3),
    Symbol(4),
    Exchange_SeqNo(5),
    SeqNo(6),

    /* OrderQueue */
    Side(7),
    Price(8),
    Count(9),
    Info(10),
    ;

    int index;

    private OrderQueue(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
