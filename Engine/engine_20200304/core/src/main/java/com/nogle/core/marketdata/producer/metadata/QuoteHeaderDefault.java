package com.nogle.core.marketdata.producer.metadata;

public enum QuoteHeaderDefault {

    /* Header part */
    Timestamp(0),
    Version(1),
    Type(2),
    Symbol(3),
    SeqNo(4),
    ;

    int index;

    private QuoteHeaderDefault(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
