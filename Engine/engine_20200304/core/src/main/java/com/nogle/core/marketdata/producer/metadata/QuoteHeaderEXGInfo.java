package com.nogle.core.marketdata.producer.metadata;

public enum QuoteHeaderEXGInfo {

    /* Header part */
    Exchange_Timestamp(0),
    Timestamp(1),
    Version(2),
    Type(3),
    Symbol(4),
    Exchange_SeqNo(5),
    SeqNo(6),
    ;

    int index;

    private QuoteHeaderEXGInfo(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
