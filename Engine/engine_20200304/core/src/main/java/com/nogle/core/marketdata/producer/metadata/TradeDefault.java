package com.nogle.core.marketdata.producer.metadata;

public enum TradeDefault {

    /* Trade */
    Aggressor(0),
    Price(1),
    Qty(2),
    OpenInterest(3),
    Volume(4),
    ;

    int index;

    private TradeDefault(final int index) {
        this.index = index + QuoteHeaderDefault.values().length;
    }

    public int getIndex() {
        return index;
    }

}
