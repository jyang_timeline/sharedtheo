package com.nogle.core.marketdata.producer.metadata;

public enum TradeV3 {

    /* Header part */
    Exchange_Timestamp(0),
    Timestamp(1),
    Version(2),
    Type(3),
    Symbol(4),
    Exchange_SeqNo(5),
    SeqNo(6),

    /* Trade */
    Aggressor(7),
    Price(8),
    Qty(9),
    OpenInterest(10),
    Volume(11),
    TradeID(12),
    BuyerOrderID(13),
    SellerOrderID(14),
    ;

    int index;

    private TradeV3(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
