package com.nogle.core.marketdata.subscriber;

import java.nio.ByteBuffer;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.event.QuoteEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class MarketBookBinarySub extends MarketDataBinarySub {
    private final MarketBookParser parser;
    private final HashWrapper<ByteBuffer> wrapper;

    private QuoteEvent handler = null;

    public MarketBookBinarySub(final String endpoint, final ConnectionProbe probe, final SbeVersion codecVersion) throws UnsupportedNodeException {
        super(endpoint, probe);
        parser = ParserInstanceFactory.getBookParser(codecVersion, 0);
        wrapper = getWrapper(null);
    }

    private HashWrapper<ByteBuffer> getWrapper(final ByteBuffer initData) {
        return new HashWrapper<>(initData, data -> parser.hashSymbol(data), (data1, data2) -> parser.hasSameSymbol(data1, data2));
    }

    @Override
    final void processData(final ByteBuffer mesg) {
        wrapper.setObject(mesg);
        handler = getQuoteEventHandler(wrapper);
        if (handler != null) {
            handler.onBook(TradeClock.getCurrentMillis(), mesg);
        }
    }

    @Override
    final HashWrapper<ByteBuffer> getHashWrapper(final String symbol) {
        parser.setSymbol(symbol);
        return getWrapper(parser.getBinary());
    }

}
