package com.nogle.core.marketdata.subscriber;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.Handler;
import com.nogle.commons.socket.MessageInfo;
import com.nogle.commons.socket.NodeFactory;
import com.nogle.commons.socket.SocketNode;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.config.EngineThreadPolicy;
import com.nogle.core.event.QuoteEvent;
import com.nogle.core.util.TimeUtils;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.DataFeedEvent;
import com.timelinecapital.core.event.SnapshotFeedEvent;
import com.timelinecapital.core.util.AffinityThreadFactory;
import com.timelinecapital.core.util.TaskThreadFactory;

import net.openhft.affinity.AffinityStrategies;

public abstract class MarketDataBinarySub {
    private static final Logger log = LogManager.getLogger(MarketDataBinarySub.class);

    private final Map<HashWrapper<ByteBuffer>, QuoteEvent> byteArrayToQuoteEventHandler = new HashMap<>();
    private final Map<HashWrapper<ByteBuffer>, SnapshotFeedEvent> byteArrayToSnapshotEventHandler = new HashMap<>();
    private final Map<HashWrapper<ByteBuffer>, DataFeedEvent> byteArrayToDataFeedEventHandler = new HashMap<>();

    private final MarketDataHandler dataHandler;
    private Interval[] tradingSessions = new Interval[0];
    private SocketNode socketNode = null;

    public MarketDataBinarySub(final String endpoint, final ConnectionProbe probe) throws UnsupportedNodeException {
        if (socketNode != null) {
            socketNode.stop();
        }
        final String name = "M" + StringUtils.substring(endpoint, StringUtils.ordinalIndexOf(endpoint, DelimiterUtil.IP_DELIMETER, 2) + 1).replaceAll("\\.", StringUtils.EMPTY);
        if (EngineThreadPolicy.DEDICATE.equals(EngineConfig.getSocketPolicy())) {
            socketNode = NodeFactory.newNode(new AffinityThreadFactory(name, false, AffinityStrategies.ANY), endpoint, 0);
        } else {
            socketNode = NodeFactory.newNode(TaskThreadFactory.createThreadFactory(name, 0), endpoint, 0);
        }
        dataHandler = new MarketDataHandler();
        socketNode.setHandler(dataHandler);
        socketNode.setProbe(probe);
    }

    public MarketDataHandler getDataHandler() {
        return dataHandler;
    }

    public void startSubscriber() {
        if (socketNode != null) {
            socketNode.start();
        }
    }

    public void checkConnection() {
        socketNode.onConnectionCheck();
    }

    public void checkSocketThread() {
        if (!socketNode.isNodeStarted()) {
            log.warn("MarketDataBinarySub is starting SocketNode's thread");
            socketNode.start();
        }
    }

    public void updateTradingSessions(final Interval[] newTradingSessions) {
        if (tradingSessions.length == 0 || newTradingSessions.length == 0) {
            tradingSessions = newTradingSessions;
            socketNode.setTradingSessions(tradingSessions);
            return;
        }
        for (int m = 0; m < tradingSessions.length; m++) {
            for (int k = 0; k < newTradingSessions.length; k++) {
                if (tradingSessions[m].overlaps(newTradingSessions[k])) {
                    tradingSessions[m] = TimeUtils.union(tradingSessions[m], newTradingSessions[k]);
                }
            }
        }
        socketNode.setTradingSessions(tradingSessions);
    }

    public void addHandler(final String symbol, final QuoteEvent quoteEventHandler) {
        try {
            final HashWrapper<ByteBuffer> wrapper = getHashWrapper(symbol);
            if (!byteArrayToQuoteEventHandler.containsKey(wrapper)) {
                byteArrayToQuoteEventHandler.put(wrapper, quoteEventHandler);
                socketNode.subscribe(symbol);
            }
        } catch (final Exception exception) {
            log.error("addHandler failed: ", exception);
        }
    }

    public void addQuoteSnapshotHandler(final String symbol, final SnapshotFeedEvent snapshotEventHandler) {
        try {
            final HashWrapper<ByteBuffer> wrapper = getHashWrapper(symbol);
            if (!byteArrayToSnapshotEventHandler.containsKey(wrapper)) {
                byteArrayToSnapshotEventHandler.put(wrapper, snapshotEventHandler);
                socketNode.subscribe(symbol);
            }
        } catch (final Exception exception) {
            log.error("addQuoteSnapshotHandler failed: ", exception);
        }
    }

    public void addDataFeedHandler(final String symbol, final DataFeedEvent datafeedEventHandler) {
        try {
            final HashWrapper<ByteBuffer> wrapper = getHashWrapper(symbol);
            if (!byteArrayToDataFeedEventHandler.containsKey(wrapper)) {
                byteArrayToDataFeedEventHandler.put(wrapper, datafeedEventHandler);
                socketNode.subscribe(symbol);
            }
        } catch (final Exception exception) {
            log.error("addDataFeedHandler failed: ", exception);
        }
    }

    QuoteEvent getQuoteEventHandler(final HashWrapper<ByteBuffer> byteArray) {
        return byteArrayToQuoteEventHandler.get(byteArray);
    }

    SnapshotFeedEvent getSnapshotEventHandler(final HashWrapper<ByteBuffer> byteArray) {
        return byteArrayToSnapshotEventHandler.get(byteArray);
    }

    DataFeedEvent getDataFeedEventHandler(final HashWrapper<ByteBuffer> byteArray) {
        return byteArrayToDataFeedEventHandler.get(byteArray);
    }

    abstract void processData(ByteBuffer mesg);

    abstract HashWrapper<ByteBuffer> getHashWrapper(String symbol);

    public class MarketDataHandler implements Handler {

        public MarketDataHandler() {
        }

        private boolean isTradingHours() {
            final DateTime now = new DateTime();
            for (final Interval session : tradingSessions) {
                if (now.isAfter(session.getStart().minusMinutes(5)) && now.isBefore(session.getEnd())) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public void onData(final MessageInfo info, final ByteBuffer data) {
            processData(data);
        }

        @Override
        public void onReceiveTimeout() {
            if (isTradingHours()) {
                log.warn("[QuoteIssues] Not receiving packets within trading hours");
            }
        }

    }

}
