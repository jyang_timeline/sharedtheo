package com.nogle.core.marketdata.subscriber;

import java.nio.ByteBuffer;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.util.TradeClock;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.OrderActionsParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.event.DataFeedEvent;

public class QuoteOrderDetailBinarySub extends MarketDataBinarySub {
    private final OrderActionsParser parser;
    private final HashWrapper<ByteBuffer> wrapper;

    private DataFeedEvent handler;

    public QuoteOrderDetailBinarySub(final String endpoint, final ConnectionProbe probe, final SbeVersion codecVersion) throws UnsupportedNodeException {
        super(endpoint, probe);
        parser = ParserInstanceFactory.getActionParser(codecVersion);
        wrapper = getWrapper(null);
    }

    private HashWrapper<ByteBuffer> getWrapper(final ByteBuffer initData) {
        return new HashWrapper<>(initData, data -> parser.hashSymbol(data), (data1, data2) -> parser.hasSameSymbol(data1, data2));
    }

    @Override
    final void processData(final ByteBuffer mesg) {
        wrapper.setObject(mesg);
        handler = getDataFeedEventHandler(wrapper);
        if (handler != null) {
            handler.onOrderActions(TradeClock.getCurrentMillis(), mesg);
        }
    }

    @Override
    final HashWrapper<ByteBuffer> getHashWrapper(final String symbol) {
        parser.setSymbol(symbol);
        return getWrapper(parser.getBinary());
    }

}
