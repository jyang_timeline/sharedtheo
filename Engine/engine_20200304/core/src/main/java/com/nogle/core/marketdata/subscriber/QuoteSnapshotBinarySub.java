package com.nogle.core.marketdata.subscriber;

import java.nio.ByteBuffer;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.SnapshotParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.event.SnapshotFeedEvent;

public class QuoteSnapshotBinarySub extends MarketDataBinarySub {
    private final SnapshotParser snapshotParser;
    private final HashWrapper<ByteBuffer> wrapper;

    private SnapshotFeedEvent handler = null;

    public QuoteSnapshotBinarySub(final String endpoint, final ConnectionProbe probe, final SbeVersion codecVersion) throws UnsupportedNodeException {
        super(endpoint, probe);
        snapshotParser = ParserInstanceFactory.getSnptParser(codecVersion);
        wrapper = getWrapper(null);
    }

    private HashWrapper<ByteBuffer> getWrapper(final ByteBuffer initData) {
        return new HashWrapper<>(initData, data -> snapshotParser.hashSymbol(data), (data1, data2) -> snapshotParser.hasSameSymbol(data1, data2));
    }

    @Override
    final void processData(final ByteBuffer mesg) {
        wrapper.setObject(mesg);
        handler = getSnapshotEventHandler(wrapper);
        if (handler != null) {
            handler.onSnapshot(mesg);
        }
    }

    @Override
    final HashWrapper<ByteBuffer> getHashWrapper(final String symbol) {
        snapshotParser.setSymbol(symbol);
        return getWrapper(snapshotParser.getBinary());
    }

}
