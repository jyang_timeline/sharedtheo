package com.nogle.core.marketdata.subscriber;

import java.nio.ByteBuffer;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.event.QuoteEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.TradeParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class TickBinarySub extends MarketDataBinarySub {
    private final TradeParser tickParser;
    private final HashWrapper<ByteBuffer> wrapper;

    private QuoteEvent handler = null;

    public TickBinarySub(final String endpoint, final ConnectionProbe probe, final SbeVersion codecVersion) throws UnsupportedNodeException {
        super(endpoint, probe);
        tickParser = ParserInstanceFactory.getTickParser(codecVersion, 0);
        wrapper = getWrapper(null);
    }

    private HashWrapper<ByteBuffer> getWrapper(final ByteBuffer initData) {
        return new HashWrapper<>(initData, data -> tickParser.hashSymbol(data), (data1, data2) -> tickParser.hasSameSymbol(data1, data2));
    }

    @Override
    final void processData(final ByteBuffer mesg) {
        wrapper.setObject(mesg);
        handler = getQuoteEventHandler(wrapper);
        if (handler != null) {
            handler.onTick(TradeClock.getCurrentMillis(), mesg);
        }
    }

    @Override
    final HashWrapper<ByteBuffer> getHashWrapper(final String symbol) {
        tickParser.setSymbol(symbol);
        return getWrapper(tickParser.getBinary());
    }

}
