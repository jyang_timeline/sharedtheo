package com.nogle.core.position;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.util.PnlFormatter;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.event.MarketOpenAware;

public class PositionTracker implements MarketOpenAware {
    private static final Logger log = LogManager.getLogger(PositionTracker.class);

    private final Queue<OpenPosition> fifoPositions = new LinkedList<>();
    private final Contract contract;

    private long overnightPositions;

    private long volume;
    private long cancels;
    private long position;
    private double closedPnl;
    private double totalChargedFee;
    private double totalQuantityXPrice;

    public PositionTracker(final Contract contract) {
        this.contract = contract;
    }

    public void setOvernightPositions(final long overnightPositions) {
        this.overnightPositions = overnightPositions;
    }

    public long getOvernightPositions() {
        return overnightPositions;
    }

    public void trackCancel(final long clOrdId) {
        cancels++;
    }

    public void trackFill(final Side side, final long lastQty, final double lastPrice, final double commission) {
        volume += lastQty;
        totalChargedFee += commission;
        if (side.getQuantityDelta(position) < 0) {
            processFill(side, lastQty, lastPrice);
        } else {
            openPosition(side, lastQty, lastPrice);
        }
    }

    private void processFill(final Side side, final long quantity, final double price) {
        long realizedQty = quantity;
        OpenPosition openPos;
        while (realizedQty > 0 && (openPos = fifoPositions.peek()) != null) {
            final Side openSide = openPos.side;
            final long openQty = openPos.quantity;
            final double openPrice = openPos.price;
            final long openTimestamp = openPos.timestamp;
            if (realizedQty >= openQty) {
                fifoPositions.poll();
                closePosition(openSide, openPrice, side, price, openQty, openTimestamp);
                realizedQty -= openQty;
            } else {
                openPos.reduce(realizedQty);
                closePosition(openSide, openPrice, side, price, realizedQty, openTimestamp);
                realizedQty = 0;
            }
        }
        if (realizedQty > 0) {
            openPosition(side, realizedQty, price);
        }
    }

    private void openPosition(final Side fillSide, final long fillQty, final double fillPrice) {
        final double sidedFillQty = fillSide.getQuantityDelta(fillQty);
        fifoPositions.add(new OpenPosition(fillSide, fillQty, fillPrice, TradeClock.getCurrentMillis()));
        totalQuantityXPrice += sidedFillQty * fillPrice;
        position += sidedFillQty;
    }

    private void closePosition(final Side openSide, final double openPrice, final Side closingSide, final double closingPrice, final long closingQty, final long openTimestamp) {
        final long holdingTimeInSecond = (TradeClock.getCurrentMillis() - openTimestamp) / 1000;
        final double openValue = openSide.getQuantityDelta(contract.getTickCalculator().toValue(closingQty, openPrice));
        final double closingValue = closingSide.getQuantityDelta(contract.getTickCalculator().toValue(closingQty, closingPrice));
        final double closingPnl = -closingValue - openValue;
        totalQuantityXPrice -= openSide.getQuantityDelta(closingQty) * openPrice;
        position += closingSide.getQuantityDelta(closingQty);
        closedPnl += closingPnl;
        log.info("{}:\t{} {}@{} -> {} {}@{}\t Pnl: {}\t Fee: {}\t Pos: {}\t HoldingTime: {}", contract, openSide, closingQty, openPrice,
            closingSide, closingQty, closingPrice, PnlFormatter.format(closingPnl), PnlFormatter.format(totalChargedFee), position, holdingTimeInSecond);
    }

    @Override
    public final void rewind() {
        fifoPositions.clear();
        volume = cancels = position = 0L;
        closedPnl = totalChargedFee = totalQuantityXPrice = 0.0d;
    }

    @Override
    public final boolean isReady() {
        return fifoPositions.isEmpty() && cancels == 0L;
    }

    public int getOrderCount() {
        return fifoPositions.size();
    }

    public long getVolume() {
        return volume;
    }

    public long getCancelCount() {
        return cancels;
    }

    public long getPosition() {
        return position;
    }

    public double getTotalPnl(final double marketPrice) {
        return closedPnl + getOpenPnl(marketPrice);
    }

    public double getOpenPnl(final double marketPrice) {
        if (position == 0 || Double.isNaN(marketPrice)) {
            return 0.0d;
        } else {
            final float avgPosPrice = (float) totalQuantityXPrice / position;
            if ((float) marketPrice - avgPosPrice == 0) {
                return 0.0d;
            }
            return contract.getTickCalculator().toValue(position, marketPrice - avgPosPrice);
        }
    }

    public double getClosedPnl() {
        return closedPnl;
    }

    public double getTotalChargedFee() {
        return totalChargedFee;
    }

    public double getExposure() {
        return totalQuantityXPrice;
    }

    private class OpenPosition {
        private final Side side;
        private final double price;
        private long quantity;
        private final long timestamp;

        OpenPosition(final Side side, final long quantity, final double price, final long timestamp) {
            this.side = side;
            this.quantity = quantity;
            this.price = price;
            this.timestamp = timestamp;
        }

        void reduce(final long quantity) {
            this.quantity -= quantity;
        }
    }

}
