package com.nogle.core.position.condition;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecutionReport;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.core.config.RiskManagerConfig;

public class MaxCancelsPerInterval implements BurstinessCondition {
    private static final Logger log = LogManager.getLogger(MaxCancelsPerInterval.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxCancelsPerInterval.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxCancelsPerInterval.toPresentKey();
    private static final EventType eventType = EventType.CancelAck;

    private final long intervalMillis = 60 * 1000;
    private final RiskManagerConfig riskManagerConfig;
    private final StrategyUpdater strategyUpdater;

    private final Map<Contract, CircularFifoQueue<Long>> cancelQueues = new HashMap<>();

    private boolean isOnWatch;
    private boolean isActive;
    private int maxCancelsPerInterval = 60;

    public MaxCancelsPerInterval(final RiskManagerConfig riskManagerConfig, final StrategyUpdater strategyUpdater) {
        this.riskManagerConfig = riskManagerConfig;
        this.strategyUpdater = strategyUpdater;
        init();
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null && riskManagerConfig.getMaxCancelsPerInterval() == Integer.MAX_VALUE) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        maxCancelsPerInterval = config.getInteger(conditionPropertyKey, riskManagerConfig.getMaxCancelsPerInterval());
        resize();
        log.info("Condition {} @Model {} value: {}", conditionPropertyKey, config.getString(StrategyConfigProtocol.Key.strategyName.toUntypedString()), maxCancelsPerInterval);
    }

    @Override
    public void onDefaultChanage() {
        maxCancelsPerInterval = maxCancelsPerInterval > riskManagerConfig.getMaxCancelsPerInterval() ? riskManagerConfig.getMaxCancelsPerInterval() : maxCancelsPerInterval;
        resize();
        log.info("Condition {} @Model {} with value: {}", conditionPropertyKey, strategyUpdater.getStrategyName(), maxCancelsPerInterval);
    }

    private void init() {
        for (final Contract contract : strategyUpdater.getContracts()) {
            cancelQueues.put(contract, new CircularFifoQueue<>(maxCancelsPerInterval));
        }
    }

    private void resize() {
        cancelQueues.forEach((k, v) -> {
            v.clear();
            v = new CircularFifoQueue<>(maxCancelsPerInterval);
        });
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void enableRiskControl() {
        cancelQueues.values().forEach(CircularFifoQueue::clear);
        strategyUpdater.getStrategyStatusView().disarm();
        isOnWatch = false;
        isActive = true;
    }

    @Override
    public void onWatch(final EventType eventType, final ExecutionReport executionReport) {
        final CircularFifoQueue<Long> cancelQueue = cancelQueues.get(executionReport.getContract());

        final long ackTime = TradeClock.getCurrentMillis();
        final long checkpoint = ackTime - intervalMillis;

        if (cancelQueue.isAtFullCapacity()) {
            final long leadingTime = cancelQueue.peek();

            if (leadingTime >= checkpoint && !isOnWatch && isActive) {
                isOnWatch = true;
                strategyUpdater.getStrategyStatusView().armWith(AlarmCodes.RISKCONTROL_CANCELS, AlarmInst.Request, strategyUpdater.getStrategyName());
                log.warn("Disable model {} due to {}: {} between {} and {}", strategyUpdater.getStrategyName(), MaxCancelsPerInterval.class.getSimpleName(),
                    cancelQueue.size(),
                    new DateTime(leadingTime).toString(NogleTimeFormatter.ISO_DATE_TIME_MILLIS_FORMAT),
                    new DateTime(ackTime).toString(NogleTimeFormatter.ISO_DATE_TIME_MILLIS_FORMAT));
                strategyUpdater.onTradingShutdown();
            }
        }
        cancelQueue.add(ackTime);
    }

    @Override
    public void dismiss() {
        if (isOnWatch) {
            isOnWatch = false;
            log.warn("{} of model {} has been reset", MaxCancelsPerInterval.class.getSimpleName(), strategyUpdater.getStrategyName());
            cancelQueues.values().forEach(CircularFifoQueue::clear);
        }
    }

    @Override
    public EventType getAvailableEventType() {
        return eventType;
    }

}
