package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.BaseCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.config.RiskManagerConfig;

public final class MaxCrossPercentage extends PriceRange implements BaseCondition {
    private static final Logger log = LogManager.getLogger(MaxCrossPercentage.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxCrossPercentage.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxCrossPercentage.toPresentKey();
    private static final double threshold = 100;

    private double maxCrossPercentageFromConfig = Double.NaN;
    private double maxCrossPercentage;

    public MaxCrossPercentage(final RiskManagerConfig config, final QuoteSnapshot quoteSnapshot) {
        super(config, quoteSnapshot);
    }

    @Override
    double getCrossablePrice(final Side side) {
        return side.switchSide().getTargetPrice(quoteSnapshot.getBookView(), maxCrossPercentage);
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (Double.isNaN(riskManagerConfig.getMaxCrossPercentage())) {
            log.debug("Disabled Global price check: {}", MaxCrossPercentage.class.getSimpleName());
            return false;
        }
        if (config.getDouble(conditionPropertyKey) == null) {
            log.debug("Unable to create instance of condition class: {}", MaxCrossPercentage.class.getSimpleName());
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        if (config.getDouble(conditionPropertyKey) != null) {
            maxCrossPercentageFromConfig =
                config.getDouble(conditionPropertyKey) >= threshold ? config.getDouble(conditionPropertyKey) % threshold : config.getDouble(conditionPropertyKey);
            if (maxCrossPercentageFromConfig <= 0.0d) {
                maxCrossPercentageFromConfig = riskManagerConfig.getMaxCrossPercentage();
            }
            maxCrossPercentage = maxCrossPercentageFromConfig / 100.0d;

            log.info("Model {}: {} is using self-defined value {}",
                config.getString(StrategyConfigProtocol.Key.strategyName.toUntypedString()),
                conditionPropertyKey,
                maxCrossPercentage);
        } else if (Double.isNaN(maxCrossPercentageFromConfig)) {
            maxCrossPercentageFromConfig =
                riskManagerConfig.getMaxCrossPercentage() >= threshold ? riskManagerConfig.getMaxCrossPercentage() % threshold : riskManagerConfig.getMaxCrossPercentage();
            maxCrossPercentage = maxCrossPercentageFromConfig / 100.0d;
        } else {
            log.info("Condition {} does not exist. Use previous one: {}", conditionPropertyKey, maxCrossPercentage);
        }
    }

    @Override
    public void onDefaultChanage() {
        maxCrossPercentage = maxCrossPercentage > riskManagerConfig.getMaxCrossPercentage() / 100.0d ? riskManagerConfig.getMaxCrossPercentage() / 100.0d : maxCrossPercentage;
        log.info("Condition {} with value: {}", conditionPropertyKey, maxCrossPercentage);
    }

}
