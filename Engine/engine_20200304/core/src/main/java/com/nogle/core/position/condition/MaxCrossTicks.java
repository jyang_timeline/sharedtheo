package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickCalculator;
import com.timelinecapital.core.config.RiskManagerConfig;

public final class MaxCrossTicks extends PriceRange {
    private static final Logger log = LogManager.getLogger(MaxCrossTicks.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxCrossTicks.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxCrossTicks.toPresentKey();

    private final TickCalculator tickCalculator;

    private final boolean isPerStrategyCheck;
    private int maxCrossTicks;

    public MaxCrossTicks(final RiskManagerConfig config, final QuoteSnapshot quoteSnapshot) {
        super(config, quoteSnapshot);
        // isPerStrategyCheck = EngineConfig.enableStrategyPriceCheck();
        isPerStrategyCheck = config.enableStrategyPriceCheck();
        tickCalculator = quoteSnapshot.getContract().getTickCalculator();
    }

    @Override
    double getCrossablePrice(final Side side) {
        return side.switchSide().getTargetPrice(quoteSnapshot.getBookView(), tickCalculator, maxCrossTicks);
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (!isPerStrategyCheck) {
            log.debug("Suppressing Strategy price check: {}", MaxCrossTicks.class.getSimpleName());
            return false;
        }
        if (config.getInteger(conditionPropertyKey) == null) {
            log.debug("Unable to create instance of condition class: {}", MaxCrossTicks.class.getSimpleName());
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        this.maxCrossTicks = config.getInteger(conditionPropertyKey);
    }

}
