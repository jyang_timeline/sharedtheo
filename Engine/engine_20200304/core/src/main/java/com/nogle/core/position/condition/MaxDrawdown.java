package com.nogle.core.position.condition;

import java.text.DecimalFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.ExecutionReport;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.core.config.RiskManagerConfig;

public class MaxDrawdown implements QuantityCondition, BurstinessCondition {
    private static final Logger log = LogManager.getLogger(MaxDrawdown.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxDrawdown.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxDrawdown.toPresentKey();
    private static final EventType eventType = EventType.Fill;
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");

    private final RiskManagerConfig riskManagerConfig;
    private final StrategyUpdater strategyUpdater;

    private boolean isOnWatch;
    private double maxDrawdownFromConfig = Double.MAX_VALUE;
    private double maxDrawdown;
    private double peak;

    public MaxDrawdown(final RiskManagerConfig riskManagerConfig, final StrategyUpdater strategyUpdater) {
        this.riskManagerConfig = riskManagerConfig;
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, final long quantity, final long position) {
        if (isOnWatch) {
            if (position == 0) {
                return position;
            }
            final long openPos = Math.abs(position);
            switch (side) {
                case BUY:
                    if (position > 0) {
                        log.warn("{} with EXIT mode. Only allow CLOSE position {}", side, position);
                        return 0;
                    } else if (quantity > openPos) {
                        final long revisedQty = QuantityCondition.ofBuyLotSize(openPos);
                        log.warn("{} with EXIT mode. Revise Qty {}->{}", side, quantity, revisedQty);
                        return revisedQty;
                    }
                    break;
                case SELL:
                    if (position < 0) {
                        log.warn("{} with EXIT mode. Only allow CLOSE position {}", side, position);
                        return 0;
                    } else if (quantity > openPos) {
                        final long revisedQty = QuantityCondition.ofSellLotSize(openPos);
                        log.warn("{} with EXIT mode. Revise Qty {}->{}", side, quantity, revisedQty);
                        return revisedQty;
                    }
                    break;
                default:
                    break;
            }
        }
        return quantity;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getDouble(conditionPropertyKey) == null && riskManagerConfig.getMaxDrawdown() == Double.MAX_VALUE) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        if (config.getDouble(conditionPropertyKey) != null) {
            maxDrawdownFromConfig = config.getDouble(conditionPropertyKey) < 0 ? config.getDouble(conditionPropertyKey) : -config.getDouble(conditionPropertyKey);
            maxDrawdown = maxDrawdownFromConfig;
            log.info("Model {}: {} is using self-defined value {}",
                config.getString(StrategyConfigProtocol.Key.strategyName.toUntypedString()),
                conditionPropertyKey,
                config.getDouble(conditionPropertyKey));
        } else if (Double.MAX_VALUE == maxDrawdownFromConfig) {
            maxDrawdownFromConfig = riskManagerConfig.getMaxDrawdown() < 0 ? riskManagerConfig.getMaxDrawdown() : -riskManagerConfig.getMaxDrawdown();
            maxDrawdown = maxDrawdownFromConfig;
        } else {
            log.info("Condition {} does not exist. Use previous one: {}", conditionPropertyKey, maxDrawdown);
        }
    }

    @Override
    public void onDefaultChanage() {
        maxDrawdown = maxDrawdown < riskManagerConfig.getMaxDrawdown() ? maxDrawdown : riskManagerConfig.getMaxDrawdown();
        log.info("Condition {} @Model {} with value: {}", conditionPropertyKey, strategyUpdater.getStrategyName(), maxDrawdown);
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void onWatch(final EventType eventType, final ExecutionReport executionReport) {
        /*
         * PNL snapshot might need to switch to closed PNL instead of closed+open ?
         */
        final double pnlSnapshot = strategyUpdater.getStrategyStatusView().getTotalPnl();
        peak = (pnlSnapshot > peak) ? pnlSnapshot : peak;
        if (pnlSnapshot <= (peak + maxDrawdown)) {
            if (!isOnWatch) {
                isOnWatch = true;
                strategyUpdater.getStrategyStatusView().armWith(AlarmCodes.RISKCONTROL_DRAWDOWN, AlarmInst.Strategy, strategyUpdater.getStrategyName());
                strategyUpdater.getStrategyStatusView().setExit(true);
                log.warn("{} Entering conservative mode due to drawdown protection: {}->{} : {}", strategyUpdater.getStrategyName(),
                    decimalFormat.format(peak), decimalFormat.format(pnlSnapshot), decimalFormat.format(maxDrawdown));
            }
            strategyUpdater.onTradingExit();
        }
    }

    @Override
    public void dismiss() {
        if (isOnWatch) {
            isOnWatch = false;
            strategyUpdater.getStrategyStatusView().setExit(false);
            peak = strategyUpdater.getStrategyStatusView().getRealizedPnl();
            log.warn("{} MaxDrawdown has been continuing on {} (resume point {})", strategyUpdater.getStrategyName(),
                decimalFormat.format(maxDrawdown), decimalFormat.format(peak));
        }
    }

    @Override
    public EventType getAvailableEventType() {
        return eventType;
    }

}
