package com.nogle.core.position.condition;

import java.text.DecimalFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.ExecutionReport;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.core.config.RiskManagerConfig;

public class MaxLoss implements QuantityCondition, BurstinessCondition {
    private static final Logger log = LogManager.getLogger(MaxLoss.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxLoss.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxLoss.toPresentKey();
    private static final EventType eventType = EventType.Fill;
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");

    private final RiskManagerConfig riskManagerConfig;
    private final StrategyUpdater strategyUpdater;

    private boolean isOnWatch;
    private double maxLossFromConfig = Double.MAX_VALUE;
    private double maxLoss;
    private double resumePoint;

    public MaxLoss(final RiskManagerConfig riskManagerConfig, final StrategyUpdater strategyUpdater) {
        this.riskManagerConfig = riskManagerConfig;
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, final long quantity, final long position) {
        if (isOnWatch) {
            if (position == 0) {
                return 0;
            }
            final long openPos = Math.abs(position);
            switch (side) {
                case BUY:
                    if (position > 0) {
                        log.warn("{} with EXIT mode. Only allow CLOSE position {}", side, position);
                        return 0;
                    } else if (quantity > openPos) {
                        final long revisedQty = QuantityCondition.ofBuyLotSize(openPos);
                        log.warn("{} with EXIT mode. Revise Qty {}->{}", side, quantity, revisedQty);
                        return revisedQty;
                    }
                    break;
                case SELL:
                    if (position < 0) {
                        log.warn("{} with EXIT mode. Only allow CLOSE position {}", side, position);
                        return 0;
                    } else if (quantity > openPos) {
                        final long revisedQty = QuantityCondition.ofSellLotSize(openPos);
                        log.warn("{} with EXIT mode. Revise Qty {}->{}", side, quantity, revisedQty);
                        return revisedQty;
                    }
                    break;
                default:
                    break;
            }
            // if (quantity > openPos) {
            // final long revisedQty = QuantityCondition.ofMinLotSize(openPos);
            // log.warn("{} {} MaxLoss Cover(={}). Revised Qty: {}/{}", side, quantity, position, openPos, revisedQty);
            // return revisedQty;
            // }
        }
        return QuantityCondition.ofMinLotSize(quantity);
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getDouble(conditionPropertyKey) == null && riskManagerConfig.getMaxLoss() == Double.MAX_VALUE) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        if (config.getDouble(conditionPropertyKey) != null) {
            maxLossFromConfig = config.getDouble(conditionPropertyKey) < 0 ? config.getDouble(conditionPropertyKey) : -config.getDouble(conditionPropertyKey);
            maxLoss = maxLossFromConfig;
            log.info("Model {}: {} is using self-defined value {}",
                config.getString(StrategyConfigProtocol.Key.strategyName.toUntypedString()),
                conditionPropertyKey,
                config.getDouble(conditionPropertyKey));
        } else if (Double.MAX_VALUE == maxLossFromConfig) {
            maxLossFromConfig = riskManagerConfig.getMaxLoss() < 0 ? riskManagerConfig.getMaxLoss() : -riskManagerConfig.getMaxLoss();
            maxLoss = maxLossFromConfig;
        } else {
            log.info("Condition {} does not exist. Use previous one: {}", conditionPropertyKey, maxLoss);
        }
    }

    @Override
    public void onDefaultChanage() {
        maxLoss = maxLoss < riskManagerConfig.getMaxLoss() ? maxLoss : riskManagerConfig.getMaxLoss();
        log.info("Condition {} @Model {} with value: {}", conditionPropertyKey, strategyUpdater.getStrategyName(), maxLoss);
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void onWatch(final EventType eventType, final ExecutionReport executionReport) {
        /*
         * PNL snapshot might need to switch to closed PNL instead of closed+open ?
         */
        final double pnlSnapshot = strategyUpdater.getStrategyStatusView().getTotalPnl();
        if (pnlSnapshot <= maxLoss) {
            if (!isOnWatch) {
                isOnWatch = true;
                strategyUpdater.getStrategyStatusView().armWith(AlarmCodes.RISKCONTROL_LOSS, AlarmInst.Strategy, strategyUpdater.getStrategyName());
                strategyUpdater.getStrategyStatusView().setExit(true);
                log.warn("{} Entering conservative mode due to PnL protection: {} <= {}", strategyUpdater.getStrategyName(),
                    decimalFormat.format(pnlSnapshot), decimalFormat.format(maxLoss));
            }
            strategyUpdater.onTradingExit();
        }
    }

    @Override
    public void dismiss() {
        if (isOnWatch) {
            isOnWatch = false;
            strategyUpdater.getStrategyStatusView().setExit(false);
            resumePoint = strategyUpdater.getStrategyStatusView().getRealizedPnl();
            maxLoss = resumePoint + maxLossFromConfig;
            log.warn("{} MaxLoss has been reset to {} (resume point {})", strategyUpdater.getStrategyName(),
                decimalFormat.format(maxLoss), decimalFormat.format(resumePoint));
        }
    }

    @Override
    public EventType getAvailableEventType() {
        return eventType;
    }

}
