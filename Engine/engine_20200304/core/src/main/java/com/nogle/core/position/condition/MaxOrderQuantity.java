package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class MaxOrderQuantity implements QuantityCondition {
    private static final Logger log = LogManager.getLogger(MaxOrderQuantity.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxOrderQty.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxOrderQty.toPresentKey();

    private final Contract contract;

    private Config config;
    private long maxQuantity;

    public MaxOrderQuantity(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, long quantity, final long currentPosition) {
        if (quantity > maxQuantity) {
            log.warn("{} {} {}, MaxOrderQty(={}). Revised Qty: {}.", side, quantity, contract, maxQuantity, maxQuantity);
            quantity = maxQuantity;
        }
        return quantity;
    }

    @Override
    public void reset() {
        if (config.getInteger(conditionPropertyKey) != null) {
            maxQuantity = config.getInteger(conditionPropertyKey);
        }
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }

        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        this.config = config;
        if (config.getInteger(conditionPropertyKey) != null) {
            maxQuantity = config.getInteger(conditionPropertyKey);
        } else {
            log.error("Condition: " + conditionPropertyKey + " is missing due to config change. Use previous one: " + maxQuantity);
        }
    }

}
