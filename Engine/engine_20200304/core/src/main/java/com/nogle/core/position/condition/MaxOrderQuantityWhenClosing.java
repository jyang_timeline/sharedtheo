package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.position.condition.types.BaseCondition;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Side;

public class MaxOrderQuantityWhenClosing implements QuantityCondition, BaseCondition {
    private static final Logger log = LogManager.getLogger(MaxOrderQuantityWhenClosing.class);

    private static final String presentKey = MaxOrderQuantityWhenClosing.class.getSimpleName();

    public MaxOrderQuantityWhenClosing() {
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, final long quantity, final long position) {
        switch (side) {
            case BUY:
                if (position > 0) {
                    log.warn("EXIT mode. Only allow CLOSE position {} {}", position, side);
                    return 0;
                } else if (quantity > Math.abs(position)) {
                    final long revisedQty = QuantityCondition.ofBuyLotSize(Math.abs(position));
                    log.warn("EXIT mode. Revise Qty {}->{} {}", quantity, revisedQty, side);
                    return revisedQty;
                }
                break;
            case SELL:
                if (position < 0) {
                    log.warn("EXIT mode. Only allow CLOSE position {} {}", position, side);
                    return 0;
                } else if (quantity > Math.abs(position)) {
                    final long revisedQty = QuantityCondition.ofSellLotSize(Math.abs(position));
                    log.warn("EXIT mode. Revise Qty {}->{}", quantity, revisedQty, side);
                    return revisedQty;
                }
                break;
            default:
                break;
        }
        return quantity;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
    }

    @Override
    public void onDefaultChanage() {
    }

}
