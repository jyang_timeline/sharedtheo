package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.PermissionForNewCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class MaxOrdersPerSide implements PermissionForNewCondition {
    private static final Logger log = LogManager.getLogger(MaxOrdersPerSide.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxOrderPerSide.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxOrderPerSide.toPresentKey();
    private final Contract contract;

    private long maxOrdersPerSide;

    public MaxOrdersPerSide(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean canNewOrder(final Side side, final long quantity, final double price, final long existingOrders) {
        if (existingOrders == maxOrdersPerSide) {
            log.warn("Blocking O {} {} {} MaxOrdersPerSide(={}/{})", contract, side, quantity, existingOrders, maxOrdersPerSide);
            return false;
        }
        return true;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        if (config.getInteger(conditionPropertyKey) != null) {
            maxOrdersPerSide = config.getInteger(conditionPropertyKey);
        } else {
            log.error("Condition: {} is missing due to config change. Use previous one: {} ", conditionPropertyKey, maxOrdersPerSide);
        }
    }

}
