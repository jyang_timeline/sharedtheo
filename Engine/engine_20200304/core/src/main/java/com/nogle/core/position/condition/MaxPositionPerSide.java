package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class MaxPositionPerSide implements QuantityCondition {
    private static final Logger log = LogManager.getLogger(MaxPositionPerSide.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxPositionPerSide.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxPositionPerSide.toPresentKey();

    private final Contract contract;

    private Config config;
    private long maxLongPosition;
    private long maxShortPosition;

    public MaxPositionPerSide(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, final long quantity, final long currentPosition) {
        switch (side) {
            case BUY:
                final long maxAllowedBuyQty = Math.max(maxLongPosition - currentPosition, 0);
                if (quantity > maxAllowedBuyQty) {
                    final long revisedQty = QuantityCondition.ofBuyLotSize(maxAllowedBuyQty);
                    log.warn("{} {} {} MaxPosition(={}). Revised Qty: {}/{}", side, quantity, contract, maxLongPosition, maxAllowedBuyQty, revisedQty);
                    return revisedQty;
                }
                break;
            case SELL:
                final long maxAllowedSellQty = Math.max(currentPosition - maxShortPosition, 0);
                if (quantity > maxAllowedSellQty) {
                    final long revisedQty = QuantityCondition.ofSellLotSize(maxAllowedSellQty);
                    log.warn("{} {} {} MaxPosition(={}). Revised Qty: {}/{}", side, quantity, contract, maxShortPosition, maxAllowedSellQty, revisedQty);
                    return maxAllowedSellQty;
                }
                break;
            default:
        }
        return quantity;
    }

    @Override
    public void reset() {
        if (config.getInteger(conditionPropertyKey) != null) {
            maxLongPosition = config.getInteger(conditionPropertyKey);
            maxShortPosition = -maxLongPosition;
        }
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        this.config = config;
        if (config.getInteger(conditionPropertyKey) != null) {
            maxLongPosition = config.getInteger(conditionPropertyKey);
            maxShortPosition = -maxLongPosition;
        } else {
            log.error("Condition: " + conditionPropertyKey + " is missing due to config change. Use previous one: " + maxLongPosition + " and " + maxShortPosition);
        }
    }

}
