package com.nogle.core.position.condition;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.ExecutionReport;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.core.config.RiskManagerConfig;

public class MaxRejectsPerInterval implements BurstinessCondition {
    private static final Logger log = LogManager.getLogger(MaxRejectsPerInterval.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMaxRejectsPerInterval.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMaxRejectsPerInterval.toPresentKey();
    private static final EventType eventType = EventType.Reject;

    private final long intervalMillis = 60 * 1000;
    private final RiskManagerConfig riskManagerConfig;
    private final StrategyUpdater strategyUpdater;

    private CircularFifoQueue<Long> rejectQueue;

    private boolean isOnWatch;
    private boolean isActive;
    private int maxRejectsPerInterval = 15;

    public MaxRejectsPerInterval(final RiskManagerConfig riskManagerConfig, final StrategyUpdater strategyUpdater) {
        this.riskManagerConfig = riskManagerConfig;
        this.strategyUpdater = strategyUpdater;
        init();
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null && riskManagerConfig.getMaxRejectsPerInterval() == Integer.MAX_VALUE) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        maxRejectsPerInterval = config.getInteger(conditionPropertyKey, riskManagerConfig.getMaxRejectsPerInterval());
        rejectQueue.clear();
        rejectQueue = new CircularFifoQueue<>(maxRejectsPerInterval);
        log.info("Condition {} @Model {} value: {}", conditionPropertyKey, config.getString(StrategyConfigProtocol.Key.strategyName.toUntypedString()), maxRejectsPerInterval);
    }

    @Override
    public void onDefaultChanage() {
        log.info("OnDefault changed: Condition {} @Model {} : {}", conditionPropertyKey, strategyUpdater.getStrategyName(), maxRejectsPerInterval);
        maxRejectsPerInterval = maxRejectsPerInterval > riskManagerConfig.getMaxRejectsPerInterval() ? riskManagerConfig.getMaxRejectsPerInterval() : maxRejectsPerInterval;
        rejectQueue.clear();
        rejectQueue = new CircularFifoQueue<>(maxRejectsPerInterval);
        log.info("Condition {} @Model {} with value: {}", conditionPropertyKey, strategyUpdater.getStrategyName(), maxRejectsPerInterval);
    }

    private void init() {
        if (rejectQueue == null) {
            rejectQueue = new CircularFifoQueue<>(maxRejectsPerInterval);
        }
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void enableRiskControl() {
        rejectQueue.clear();
        strategyUpdater.getStrategyStatusView().disarm();
        isOnWatch = false;
        isActive = true;
    }

    private void updateRejectQueue(final long instance) {
        rejectQueue.add(instance);
    }

    @Override
    public void onWatch(final EventType eventType, final ExecutionReport executionReport) {
        final long rejectTime = TradeClock.getCurrentMillis();
        final long checkpoint = rejectTime - intervalMillis;
        log.warn("{} {} : {} {}", rejectQueue.size(), rejectQueue.maxSize(), rejectTime, checkpoint);
        if (rejectQueue.isAtFullCapacity()) {
            final long leadingTime = rejectQueue.peek();

            if (leadingTime >= checkpoint && !isOnWatch && isActive) {
                isOnWatch = true;
                strategyUpdater.getStrategyStatusView().armWith(AlarmCodes.RISKCONTROL_REJECTS, AlarmInst.Request, strategyUpdater.getStrategyName());
                log.warn("Disable model {} due to {}: {} between {} and {}", strategyUpdater.getStrategyName(), MaxRejectsPerInterval.class.getSimpleName(),
                    rejectQueue.size(),
                    new DateTime(leadingTime).toString(NogleTimeFormatter.ISO_DATE_TIME_MILLIS_FORMAT),
                    new DateTime(rejectTime).toString(NogleTimeFormatter.ISO_DATE_TIME_MILLIS_FORMAT));
                strategyUpdater.onTradingShutdown();
            }
        }
        updateRejectQueue(rejectTime);
    }

    @Override
    public void dismiss() {
        if (isOnWatch) {
            isOnWatch = false;
            log.warn("{} of model {} has been reset", MaxRejectsPerInterval.class.getSimpleName(), strategyUpdater.getStrategyName());
            rejectQueue.clear();
        }
    }

    @Override
    public EventType getAvailableEventType() {
        return eventType;
    }

}
