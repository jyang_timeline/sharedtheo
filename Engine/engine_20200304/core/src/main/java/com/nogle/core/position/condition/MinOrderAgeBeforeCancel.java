package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.PermissionForCancelCondition;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;

public class MinOrderAgeBeforeCancel implements PermissionForCancelCondition {
    private static final Logger log = LogManager.getLogger(MinOrderAgeBeforeCancel.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionCancelDelay.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionCancelDelay.toPresentKey();

    private final Contract contract;

    private long minOrderAgeInMillis;

    public MinOrderAgeBeforeCancel(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean canCancelOrder(final long orderTimeInMillis, final long cancelCounter) {
        if (TradeClock.getCurrentMillis() - orderTimeInMillis >= minOrderAgeInMillis) {
            return true;
        } else {
            log.warn("Blocking X {}: {}", contract, MinOrderAgeBeforeCancel.class.getSimpleName());
            return false;
        }
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        minOrderAgeInMillis = config.getInteger(conditionPropertyKey);
    }

}
