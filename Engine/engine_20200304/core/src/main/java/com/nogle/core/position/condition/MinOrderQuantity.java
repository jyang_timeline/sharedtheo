package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class MinOrderQuantity implements QuantityCondition {
    private static final Logger log = LogManager.getLogger(MinOrderQuantity.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionMinOrderQty.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionMinOrderQty.toPresentKey();

    private final Contract contract;

    private Config config;
    private long minOrderQuantity;

    public MinOrderQuantity(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public long checkCondition(final Side side, final long quantity, final long position) {
        if (quantity < minOrderQuantity) {
            log.warn("Blocking O {} {} {}: MinimumQty(={})", contract, side, quantity, minOrderQuantity);
            return 0L;
        }
        return quantity;
    }

    @Override
    public void reset() {
        if (config.getInteger(conditionPropertyKey) != null) {
            minOrderQuantity = config.getInteger(conditionPropertyKey);
        }
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        this.config = config;
        minOrderQuantity = config.getInteger(conditionPropertyKey);
    }

}
