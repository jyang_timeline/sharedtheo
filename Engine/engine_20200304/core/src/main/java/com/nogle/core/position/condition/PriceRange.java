package com.nogle.core.position.condition;

import com.nogle.core.position.condition.types.PriceCondition;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.config.RiskManagerConfig;

public abstract class PriceRange implements PriceCondition {

    protected final RiskManagerConfig riskManagerConfig;
    protected final QuoteSnapshot quoteSnapshot;
    private double limitUp;
    private double limitDown;

    double crossablePrice;

    public PriceRange(final RiskManagerConfig riskManagerConfig, final QuoteSnapshot quoteSnapshot) {
        this.riskManagerConfig = riskManagerConfig;
        this.quoteSnapshot = quoteSnapshot;
        limitUp = quoteSnapshot.getContract().getLimitUp();
        limitDown = quoteSnapshot.getContract().getLimitDown();
    }

    abstract double getCrossablePrice(Side side);

    @Override
    public void onRangeUpdate() {
        limitUp = quoteSnapshot.getContract().getLimitUp();
        limitDown = quoteSnapshot.getContract().getLimitDown();
    }

    @Override
    public double checkCondition(final Side side, final double price) {
        switch (quoteSnapshot.getQuoteType()) {
            case QUOTE:
                crossablePrice = getCrossablePrice(side);
                return PriceUtils.isOutsideOf(side, price, crossablePrice) ? price : crossablePrice;
            case LIMIT_UP:
                return PriceUtils.isOutsideOf(side, price, limitUp) ? price : limitUp;
            case LIMIT_DOWN:
                return PriceUtils.isOutsideOf(side, price, limitDown) ? price : limitDown;
            case INVALID_ASK:
            case INVALID_BID:
            default:
                return 0;
        }
    }

}
