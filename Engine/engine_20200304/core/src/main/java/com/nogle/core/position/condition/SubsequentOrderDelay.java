package com.nogle.core.position.condition;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.position.condition.types.PermissionForNewCondition;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class SubsequentOrderDelay implements PermissionForNewCondition {
    private static final Logger log = LogManager.getLogger(SubsequentOrderDelay.class);

    private static final String conditionPropertyKey = StrategyConfigProtocol.Key.positionSequentOrderDelay.toUntypedString();
    private static final String presentKey = StrategyConfigProtocol.Key.positionSequentOrderDelay.toPresentKey();

    private final Contract contract;

    private long delayInMillis;
    private long lastTimeInMillis;

    public SubsequentOrderDelay(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getRenewableKey() {
        return presentKey;
    }

    @Override
    public boolean canNewOrder(final Side side, final long quantity, final double price, final long existingOrders) {
        final long currentTime = TradeClock.getCurrentMillis();
        if (currentTime >= lastTimeInMillis + delayInMillis) {
            lastTimeInMillis = currentTime;
            return true;
        } else {
            log.warn("Blocking O {} {} {}@{}: SubsequentOrderDelay(={}ms)", contract, side, quantity, price, delayInMillis);
            return false;
        }
    }

    @Override
    public boolean checkUpdate(final Config config) {
        if (config.getInteger(conditionPropertyKey) == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onConfigChange(final Config config) {
        delayInMillis = config.getInteger(conditionPropertyKey);
    }

}
