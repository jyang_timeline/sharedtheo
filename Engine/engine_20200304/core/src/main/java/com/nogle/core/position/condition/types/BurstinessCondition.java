package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.ExecutionReport;

public interface BurstinessCondition extends BaseCondition, RenewableCondition {

    enum EventType {
        CancelAck,
        Reject,
        Fill
    }

    EventType getAvailableEventType();

    void onWatch(EventType eventType, ExecutionReport executionReport);

    void dismiss();

    boolean isActive();

    default void enableRiskControl() {
    }

}
