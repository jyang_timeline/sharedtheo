package com.nogle.core.position.condition.types;

public interface PermissionForCancelCondition extends RenewableCondition {

    boolean canCancelOrder(long timestampInMillis, long cancelCounter);

}
