package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Side;

public interface PermissionForModCondition extends RenewableCondition {

    boolean canModifyOrder(long timestampInMillis, Side side, long quantity, double price, long existingOrders);

}
