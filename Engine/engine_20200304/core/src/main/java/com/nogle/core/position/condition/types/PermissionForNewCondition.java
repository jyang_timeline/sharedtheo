package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Side;

public interface PermissionForNewCondition extends RenewableCondition {

    boolean canNewOrder(Side side, long quantity, double price, long existingOrders);

}
