package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Config;

public interface PositionCondition extends RenewableCondition {

    boolean checkUpdate(Config config);

    void onConfigChange(Config config);

}
