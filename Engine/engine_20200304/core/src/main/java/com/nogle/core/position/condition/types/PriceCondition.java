package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Side;

public interface PriceCondition extends RenewableCondition {

    void onRangeUpdate();

    double checkCondition(Side side, double price);

}
