package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Side;
import com.timelinecapital.core.config.RiskManagerConfig;

public interface QuantityCondition extends RenewableCondition {

    int minLotSize = RiskManagerConfig.getInstance().getMinLotSize();

    static long ofMinLotSize(final long quantity) {
        return minLotSize * Math.floorDiv(quantity, minLotSize);
    }

    static long ofBuyLotSize(final long quantity) {
        return minLotSize * Math.floorDiv(quantity, minLotSize);
    }

    static long ofSellLotSize(final long quantity) {
        return minLotSize * Math.floorDiv(quantity, minLotSize);
    }

    long checkCondition(Side side, long quantity, long position);

}
