package com.nogle.core.position.condition.types;

import com.nogle.strategy.types.Config;

public interface RenewableCondition {

    default void reset() {
    }

    boolean checkUpdate(Config config);

    void onConfigChange(Config config);

    String getRenewableKey();

}
