package com.nogle.core.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.timelinecapital.core.scheduler.ScheduledEventListener;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class CalendarDayNotifyJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        final ScheduledEventListener listener =
            (ScheduledEventListener) dataMap.getOrDefault(ScheduleJobUtil.JOB_KEY_TRADINGDAY_CHANGE, IdleScheduleListener.getInstance());
        listener.onScheduledEvent();
    }

}
