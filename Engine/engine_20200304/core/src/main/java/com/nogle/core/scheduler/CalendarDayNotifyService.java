package com.nogle.core.scheduler;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.core.ScheduledService;
import com.timelinecapital.core.scheduler.ScheduledEventListener;

public class CalendarDayNotifyService implements ScheduledService {
    private static final Logger log = LogManager.getLogger(CalendarDayNotifyService.class);

    private static final String TRIGGER_GROUP = "ClockGroup";

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;
    private final Set<Trigger> clockTriggers = new HashSet<>();
    private JobDetail jobDetail;

    public CalendarDayNotifyService() throws ParseException {
        scheduler = JobScheduler.getInstance().getScheduler();

        final CronExpression calendarCronExpression = new CronExpression(String.format("0 0 0 ? * *"));
        clockTriggers.add(TriggerBuilder.newTrigger()
            .withIdentity("CalendarDay", TRIGGER_GROUP)
            .withSchedule(CronScheduleBuilder.cronSchedule(calendarCronExpression))
            .build());
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey) {
        try {
            jobDataMap.put(schedulerKey, listener);
            jobDetail = JobBuilder.newJob(CalendarDayNotifyJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, clockTriggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey, final Class<? extends Job> jobClass) {
        throw new RuntimeException("Method not ready");
    }

    @Override
    public void removeScheduleServiceListener() {
        log.warn("About to unschedule trading day service");
        clockTriggers.forEach(t -> {
            try {
                scheduler.unscheduleJob(t.getKey());
            } catch (final SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

}
