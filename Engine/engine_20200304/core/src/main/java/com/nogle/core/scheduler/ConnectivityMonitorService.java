package com.nogle.core.scheduler;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.ScheduledService;
import com.timelinecapital.core.scheduler.ScheduledEventListener;

public class ConnectivityMonitorService implements ScheduledService {
    private static final Logger log = LogManager.getLogger(ConnectivityMonitorService.class);

    private static final String TRIGGER_GROUP = "EngineConnManager";

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;
    private final Set<Trigger> triggers = new HashSet<>();
    private JobDetail jobDetail;

    enum CheckPoint {
        MARKET_OPEN_CHECK,
        SOCKET_THREAD_CHECK,
        CONNECTION_CHECK,
        ;
    }

    public ConnectivityMonitorService() throws ParseException {
        scheduler = JobScheduler.getInstance().getScheduler();
        int scheduleNumber = 0;
        // for (final String monitorSchedule : EngineConfig.getMonitorSchedules()) {
        // scheduleNumber++;
        // final String[] timestamp = monitorSchedule.split(":");
        // final CronExpression cronExpression =
        // new CronExpression(String.format("%d %d %d ? * *", Integer.parseInt(timestamp[2]), Integer.parseInt(timestamp[1]),
        // Integer.parseInt(timestamp[0])));
        // monitorTriggers.add(TriggerBuilder.newTrigger()
        // .withIdentity(buildTriggerName(scheduleNumber, CheckPoint.MARKET_OPEN_CHECK), TRIGGER_GROUP)
        // .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
        // .build());
        // }

        final CronExpression connCronExpression = new CronExpression(String.format("0/10 * * ? * *"));
        triggers.add(TriggerBuilder.newTrigger()
            .withIdentity(buildTriggerName(scheduleNumber++, CheckPoint.CONNECTION_CHECK), TRIGGER_GROUP)
            .withSchedule(CronScheduleBuilder.cronSchedule(connCronExpression))
            .build());
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey) {
        try {
            jobDataMap.put(schedulerKey, listener);
            jobDetail = JobBuilder.newJob(ConnectivityMonitorJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, triggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey, final Class<? extends Job> jobClass) {
        throw new RuntimeException("Method not ready");
    }

    @Override
    public void removeScheduleServiceListener() {
        log.warn("About to shutdown connectivity monitoring service");
        triggers.forEach(t -> {
            try {
                scheduler.unscheduleJob(t.getKey());
            } catch (final SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    private String buildTriggerName(final int scheduleNumber, final CheckPoint checkPoint) {
        final StringBuilder sb = new StringBuilder();
        sb.append(scheduleNumber).append(DelimiterUtil.SESSION_DESCRIPTOR_DELIMITER).append(checkPoint);
        return sb.toString();
    }

}
