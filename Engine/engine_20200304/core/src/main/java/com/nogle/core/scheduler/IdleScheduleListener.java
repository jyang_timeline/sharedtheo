package com.nogle.core.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.core.scheduler.MarketOpeningScheduleListener;
import com.timelinecapital.core.scheduler.ScheduledEventListener;

public final class IdleScheduleListener implements ScheduledEventListener, StrategyScheduleListener, MarketOpeningScheduleListener {
    private static final Logger log = LogManager.getLogger(IdleScheduleListener.class);

    private static IdleScheduleListener instance = new IdleScheduleListener();

    public static IdleScheduleListener getInstance() {
        return instance;
    }

    @Override
    public final void onStart() {
        log.error("onStart has been triggered by an IdleScheduleListener");
    }

    @Override
    public final void onStop() {
        log.error("onStop has been triggered by an IdleScheduleListener");
    }

    @Override
    public final void onShutdown() {
        log.error("onShutdown has been triggered by an IdleScheduleListener");
    }

    @Override
    public final void onPreOpeningTask() {
        log.debug("onPreOpeningTask - Task triggered with IDLE listener");
    }

    @Override
    public final void onScheduledEvent() {
    }

}
