package com.nogle.core.scheduler;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.JobScheduler;
import com.nogle.core.util.TimeUtils;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.strategy.StrategyConfig;

public class LiveStrategyScheduleService extends StrategyScheduleService {
    private static final Logger log = LogManager.getLogger(LiveStrategyScheduleService.class);

    private static final String SESSION_PREFIX = "SESSION#";
    private static final String TRIGGER_KEY = "listener";

    private final Set<Trigger> triggers = new HashSet<>();
    private final Scheduler scheduler;
    private final String strategyId;
    private JobDetail jobDetail;

    public LiveStrategyScheduleService(final int strategyId, final StrategyConfig strategyConfig) {
        super(strategyConfig);
        scheduler = JobScheduler.getInstance().getScheduler();
        this.strategyId = Integer.toString(strategyId);
    }

    @Override
    public void setScheduleListener(final StrategyScheduleListener listener) {
        super.setScheduleListener(listener);

        final JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(TRIGGER_KEY, listener);
        jobDetail = JobBuilder.newJob(StrategyScheduleJob.class).setJobData(jobDataMap).build();
        try {
            scheduler.scheduleJob(jobDetail, triggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void removeScheduleListener() {
        super.removeScheduleListener();
        try {
            clearTriggers();
            if (jobDetail != null) {
                scheduler.deleteJob(jobDetail.getKey());
            }
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void buildTradingSessions() {
        log.info("[CalendarDay] {} -> [TradingDay] {}", TradeClock.getCalendar(), tradingDay);

        clearTriggers();
        Trigger triggerOfStart = null;
        Trigger triggerOfStop = null;
        Trigger triggerOfShutdown = null;
        DateTime startTime = null;
        DateTime stopTime = null;
        DateTime shutdownTime = null;

        int sessionNumber = 0;
        for (final Interval schedule : schedules) {
            sessionNumber++;

            startTime = tradingDay.plus(schedule.getStart().getMillisOfDay());
            if (schedule.getStart().isAfter(switchTime) && !TimeUtils.isCurrentTimeFallsInSchedule(schedule.getStart(), schedule.getEnd())) {
                startTime = tradingDay.minus(NogleTimeUnit.DAYS.toMillis(1)).plus(schedule.getStart().getMillisOfDay());
            }
            stopTime = startTime.plus(schedule.toDurationMillis());
            shutdownTime = stopTime.plusMinutes(shutdownOffset);

            final CronExpression startExpr = TimeUtils.dailyAtHourMinuteAndSecond(
                startTime.getHourOfDay(),
                startTime.getMinuteOfHour(),
                startTime.getSecondOfMinute());
            triggerOfStart = TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(sessionNumber, CheckPoint.START), strategyId)
                .withSchedule(CronScheduleBuilder.cronSchedule(startExpr))
                .build();

            final CronExpression stopExpr = TimeUtils.dailyAtHourMinuteAndSecond(
                stopTime.getHourOfDay(),
                stopTime.getMinuteOfHour(),
                stopTime.getSecondOfMinute());
            triggerOfStop = TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(sessionNumber, CheckPoint.STOP), strategyId)
                .withSchedule(CronScheduleBuilder.cronSchedule(stopExpr))
                .build();

            final CronExpression shutdownExpr = TimeUtils.dailyAtHourMinuteAndSecond(
                shutdownTime.getHourOfDay(),
                shutdownTime.getMinuteOfHour(),
                shutdownTime.getSecondOfMinute());
            triggerOfShutdown = TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(sessionNumber, CheckPoint.SHUTDOWN), strategyId)
                .withSchedule(CronScheduleBuilder.cronSchedule(shutdownExpr))
                .build();

            final long currentMillis = TradeClock.getCurrentMillis();
            if (currentMillis > stopTime.getMillis()) {
                log.warn("[SkipSchedule] strategy:{} {} ~ {} current time {}", strategyId, startTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT),
                    stopTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT), TradeClock.getCalendar());
            } else if (currentMillis < stopTime.getMillis() && currentMillis >= startTime.getMillis()) {
                log.info("[ AddSchedule] strategy:{} stop@{} shutdown@{}", strategyId, stopTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT),
                    shutdownTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT));
                triggers.add(triggerOfStop);
                triggers.add(triggerOfShutdown);
                log.warn("[SkipSchedule] strategy:{} onStart immediately:{}", strategyId, TradeClock.getCalendar());
                startImmediately = true;
            } else {
                log.info("[ AddSchedule] strategy:{} start@{} stop@{} shutdown@{}", strategyId, startTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT),
                    stopTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT), shutdownTime.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT));
                triggers.add(triggerOfStart);
                triggers.add(triggerOfStop);
                triggers.add(triggerOfShutdown);
            }
        }

        log.debug("[Triggers] strategy:{} TriggerSize:{}", strategyId, triggers.size());
    }

    /*
     * Format: SESSION#3.START
     */
    private String buildTriggerName(final int sessionNumber, final CheckPoint checkPoint) {
        final StringBuilder sb = new StringBuilder();
        sb.append(SESSION_PREFIX).append(sessionNumber).append(DelimiterUtil.SESSION_DESCRIPTOR_DELIMITER).append(checkPoint);
        return sb.toString();
    }

    private synchronized void clearTriggers() {
        for (final Trigger trigger : triggers) {
            try {
                scheduler.unscheduleJob(trigger.getKey());
            } catch (final SchedulerException e) {
                log.error("Failed to clear trigger:{}", trigger.getKey(), e);
            }
        }
        triggers.clear();
    }

}
