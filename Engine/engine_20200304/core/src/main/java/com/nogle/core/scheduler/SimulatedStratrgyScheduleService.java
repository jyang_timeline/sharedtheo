package com.nogle.core.scheduler;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.core.strategy.StrategyConfig;

public class SimulatedStratrgyScheduleService extends StrategyScheduleService {

    private final ArrayBlockingQueue<Long> queue = new ArrayBlockingQueue<>(32);

    private CheckPoint currentPoint = CheckPoint.IDLE;
    private long currentBracketHead;
    private long currentBracketEnd;
    private long currentBracketShutdown;

    public SimulatedStratrgyScheduleService(final StrategyConfig strategyConfig) {
        super(strategyConfig);
        SimulatedTimer.getInstance().addTimeChangeListener(new TimeChangeListener() {

            @Override
            public void onChange(final long millis) {
                setCurrentTimeMillis(millis);
            }
        });
    }

    @Override
    public void buildTradingSessions() {
        clearCheckpoints();

        long startMillis;
        long stopMillis;
        long shutdownMillis;
        Long[] strategyCheckpoints = new Long[0];
        for (final Interval schedule : schedules) {
            if (schedule.getStart().isAfter(switchTime)) {
                if (DateTimeConstants.MONDAY == tradingDay.dayOfWeek().get()) {
                    startMillis = tradingDay.minus(NogleTimeUnit.DAYS.toMillis(3)).plus(schedule.getStart().getMillisOfDay()).getMillis();
                } else {
                    startMillis = tradingDay.minus(NogleTimeUnit.DAYS.toMillis(1)).plus(schedule.getStart().getMillisOfDay()).getMillis();
                }
            } else {
                startMillis = tradingDay.plus(schedule.getStart().getMillisOfDay()).getMillis();
            }
            stopMillis = startMillis + schedule.toDurationMillis();
            shutdownMillis = stopMillis + NogleTimeUnit.MINUTES.toMillis(shutdownOffset);

            strategyCheckpoints = ArrayUtils.add(strategyCheckpoints, startMillis);
            strategyCheckpoints = ArrayUtils.add(strategyCheckpoints, stopMillis);
            strategyCheckpoints = ArrayUtils.add(strategyCheckpoints, shutdownMillis);
        }
        Arrays.sort(strategyCheckpoints);
        queue.addAll(Arrays.asList(strategyCheckpoints));

        currentBracketHead = queue.poll();
        currentBracketEnd = queue.poll();
        currentBracketShutdown = queue.poll();
    }

    private void clearCheckpoints() {
        queue.clear();
    }

    private void setCurrentTimeMillis(final long currentMillis) {
        final CheckPoint point = getPoint(currentMillis);
        if (point != currentPoint) {
            TradeClock.setCurrentMillis(currentMillis);
            doAction(point);
            currentPoint = point;
        }
    }

    private void doAction(final CheckPoint point) {
        switch (point) {
            case SHUTDOWN:
                getScheduleListener().onShutdown();
                break;
            case START:
                getScheduleListener().onStart();
                break;
            case STOP:
                getScheduleListener().onStop();
                break;
            default:
                break;
        }
    }

    private CheckPoint getPoint(final long currentMillis) {
        if (currentMillis > currentBracketHead && currentMillis < currentBracketEnd) {
            return CheckPoint.START;
        } else if (currentMillis > currentBracketEnd && currentMillis < currentBracketShutdown) {
            return CheckPoint.STOP;
        } else if (currentMillis > currentBracketShutdown) {
            if (!queue.isEmpty()) {
                currentBracketHead = queue.poll();
                currentBracketEnd = queue.poll();
                currentBracketShutdown = queue.poll();
            }
            return CheckPoint.SHUTDOWN;
        } else {
            return CheckPoint.IDLE;
        }
    }

}
