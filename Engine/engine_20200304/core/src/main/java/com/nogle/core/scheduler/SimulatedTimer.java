package com.nogle.core.scheduler;

import org.apache.commons.lang3.ArrayUtils;

public class SimulatedTimer {

    private volatile static ThreadLocal<SimulatedTimer> THREAD_CLOCK = new ThreadLocal<SimulatedTimer>();
    private TimeChangeListener[] listeners = new TimeChangeListener[0];

    public static SimulatedTimer getInstance() {
        if (THREAD_CLOCK.get() == null) {
            try {
                THREAD_CLOCK.set(new SimulatedTimer());
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return THREAD_CLOCK.get();
    }

    public void setTime(final long currentTimeMillis) {
        for (final TimeChangeListener listener : listeners) {
            listener.onChange(currentTimeMillis);
        }
    }

    public void resetTimeChangeListener() {
        listeners = new TimeChangeListener[0];
    }

    public void addTimeChangeListener(final TimeChangeListener listener) {
        listeners = ArrayUtils.add(listeners, listener);
    }

}
