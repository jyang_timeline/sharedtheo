package com.nogle.core.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class StrategyScheduleJob implements Job {
    private static final Logger log = LogManager.getLogger(StrategyScheduleJob.class);

    private static final String JOB_KEY = "listener";

    private StrategyScheduleListener listener;
    private String checkpoint;

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        listener = (StrategyScheduleListener) context.getJobDetail().getJobDataMap().get(JOB_KEY);
        checkpoint = context.getTrigger().getKey().getName();
        if (checkpoint.contains(StrategyScheduleService.CheckPoint.START.toString())) {
            listener.onStart();
        } else if (checkpoint.contains(StrategyScheduleService.CheckPoint.STOP.toString())) {
            listener.onStop();
        } else if (checkpoint.contains(StrategyScheduleService.CheckPoint.SHUTDOWN.toString())) {
            listener.onShutdown();
        } else {
            log.error("Checkpoint unknow: {}", checkpoint);
        }
    }

}
