package com.nogle.core.scheduler;

public interface StrategyScheduleListener {

    void onStart();

    void onStop();

    void onShutdown();

}
