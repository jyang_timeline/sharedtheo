package com.nogle.core.scheduler;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.strategy.StrategyConfig;

/**
 * TimeChecker use to control strategy start & stop & shutdown. (properties tag : Strategy.startTime & Strategy.endTime) But the
 * strategy can alse control by command and WebUI. If user send command or click WebUI to change strategy state, system must be
 * control the @CheckPoint
 */
public abstract class StrategyScheduleService {

    protected final DateTime tradingDay;
    protected final DateTime switchTime;
    protected final int shutdownOffset;

    protected Interval[] schedules;
    protected StrategyScheduleListener listener;
    protected boolean startImmediately = false;

    protected enum CheckPoint {
        START,
        STOP,
        SHUTDOWN,
        IDLE
    }

    public StrategyScheduleService(final StrategyConfig strategyConfig) {
        tradingDay = DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        switchTime = EngineConfig.getTradingDaySwitchPoint();
        shutdownOffset = strategyConfig.getInteger(StrategyConfigProtocol.Key.strategyClosingOffset.toUntypedString());

        for (final Interval interval : strategyConfig.getSchedules()) {
            schedules = ArrayUtils.add(schedules, interval);
        }
    }

    public void setScheduleListener(final StrategyScheduleListener listener) {
        this.listener = listener;
    }

    public void removeScheduleListener() {
        this.listener = IdleScheduleListener.getInstance();
    }

    public StrategyScheduleListener getScheduleListener() {
        return listener;
    }

    public boolean isStartImmediately() {
        return startImmediately;
    }

    public abstract void buildTradingSessions();

}
