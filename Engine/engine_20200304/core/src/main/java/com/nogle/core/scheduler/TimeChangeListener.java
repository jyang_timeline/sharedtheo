package com.nogle.core.scheduler;

public interface TimeChangeListener {

    void onChange(long time);

}
