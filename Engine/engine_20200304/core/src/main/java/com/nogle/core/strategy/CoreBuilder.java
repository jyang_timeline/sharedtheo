package com.nogle.core.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.exception.InvalidCodeNameException;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.TradeAppendix.TradeAppendixBuilder;
import com.timelinecapital.core.event.handler.ContractInfoWarningHandler;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.stats.ExecReportEvent;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.types.PriceLevelPolicy;
import com.timelinecapital.core.util.ExchangeProtocolUtil;

public class CoreBuilder {
    private static final Logger log = LogManager.getLogger(CoreBuilder.class);

    private static final TradeAppendixBuilder builder = new TradeAppendixBuilder();

    private static ContractInfoWarningHandler contractBuiltHandler;

    public static void addContractInfoHandler(final ContractInfoWarningHandler contractBuiltHandler) {
        CoreBuilder.contractBuiltHandler = contractBuiltHandler;
    }

    public static void buildContract(final Collection<Instrument> instruments) {
        if (instruments.isEmpty()) {
            log.info("Skip batch contract refresh");
            return;
        }
        ContractFactory.tryRenew(instruments);
        instruments.stream().filter(inst -> inst.getSecurityType().hasPriceLimits()).forEach(inst -> {
            contractBuiltHandler.wrap(inst);
            contractBuiltHandler.handle();
        });
    }

    static void buildContract(final StrategyConfig strategyConfig, final Map<String, Instrument> instruments) throws InvalidCodeNameException {
        if (!instruments.isEmpty()) {
            log.trace("Skip contract lookup");
            return;
        }
        for (final Map.Entry<String, String> entry : strategyConfig.getStrategyContracts().entrySet()) {
            final Instrument instrument = ContractFactory.getByTradingDay(entry.getValue(), TradeClock.getTradingDay());
            instruments.put(entry.getKey(), instrument);
            if (instrument.getSecurityType().hasPriceLimits()) {
                contractBuiltHandler.wrap(instrument);
                contractBuiltHandler.handle();
            }
        }
        log.trace("TradingDay: {}, Contracts: {}\n", TradeClock.getTradingDay(), instruments.values());
    }

    static void buildFeedInfos(final StrategyConfig strategyConfig, final Map<String, Instrument> instruments, final Map<Instrument, List<FeedType>> contractToFeeds) {
        final Map<String, List<FeedType>> sKeyToFeeds = new HashMap<>();
        for (final Entry<String, String> feedEntry : strategyConfig.getAdditionalDataSubscriber().entrySet()) {
            final FeedType dataFeed = FeedType.valueOf(feedEntry.getKey());
            final String[] sKeys = feedEntry.getValue().split(DelimiterUtil.CONTRACT_DELIMETER);
            for (final String sKey : sKeys) {
                sKeyToFeeds.computeIfAbsent(sKey, string -> new ArrayList<>());
                sKeyToFeeds.get(sKey).add(dataFeed);
            }
        }

        for (final Entry<String, Instrument> entry : instruments.entrySet()) {
            final String representKey = StringUtils.substringAfter(entry.getKey(), DelimiterUtil.CONTRACT_DESCRIPTOR_DELIMETER);
            sKeyToFeeds.computeIfAbsent(representKey, string -> new ArrayList<>());
            sKeyToFeeds.get(representKey).addAll(ExchangeProtocolUtil.getQuoteFeedType(entry.getValue().getExchange(), entry.getValue().getSecurityType()));
            contractToFeeds.put(entry.getValue(), sKeyToFeeds.get(representKey));
        }
        log.trace("Contracts to Feed: {}", contractToFeeds);
    }

    static void buildTradeInfos(
        final TradingAccount account,
        final StrategyConfig strategyConfig,
        final Map<String, Instrument> instruments,
        final ProtectiveManager riskControlManager,
        final Map<Contract, PositionManager> contractToPositionManager,
        final Map<Contract, PriceManager> contractToPriceManager,
        final Map<Instrument, TradeAppendix> contractToTrades) {

        final PositionImport positionImport = OverviewFactory.getPositionImport(account);

        for (final Entry<String, String> entry : strategyConfig.getStrategyContracts().entrySet()) {
            final Instrument contract = instruments.get(entry.getKey());
            final PriceLevelPolicy policy = ContractFactory.getOrderViewType(entry.getValue());

            final ExecReportEvent statsCollector = OverviewFactory.getExecReportSBean(contract, account);
            positionImport.registerWith(contract.getSymbol(), statsCollector);
            positionImport.checkStaticPositions(contract.getSymbol());

            final PositionTracker positionTracker = new PositionTracker(contract);
            positionTracker.setOvernightPositions(strategyConfig.getOvernightPositions().getOrDefault(entry.getKey(), 0l));

            final TradeAppendix appendix = builder.withContract(contract)
                .withAccount(account)
                .withPositionTracker(positionTracker)
                .withRiskManager(riskControlManager)
                .withPositionManager(contractToPositionManager.get(contract))
                .withPriceManager(contractToPriceManager.get(contract))
                .withPricePolicy(policy)
                .withPositionImporter(positionImport)
                .withStatsCollector(statsCollector)
                .build();
            contractToTrades.put(contract, appendix);
        }
        log.trace("Contracts to Trade: {}", contractToTrades);
    }

}
