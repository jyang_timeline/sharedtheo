package com.nogle.core.strategy;

import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Miss;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.TradeServices;

public class IdleStrategy implements Strategy {

    private static IdleStrategy instance = new IdleStrategy();

    public static IdleStrategy getInstance() {
        return instance;
    }

    @Override
    public final void onStart(final long updateId) {
    }

    @Override
    public final void onStop(final long updateId) {
    }

    @Override
    public final void onShutdown(final long updateId) {
    }

    @Override
    public final void onConfigChange(final Config config) {
    }

    @Override
    public final void onMarketBook(final long updateId, final BookView bookView) {
    }

    @Override
    public final void onTick(final long updateId, final TickView tickView) {
    }

    @Override
    public final void onFill(final long updateId, final Fill fill) {
    }

    @Override
    public final void onMiss(final long updateId, final Miss miss) {
    }

    @Override
    public final void onCommandRollCall(final TradeServices tradeServices) {
    }

}
