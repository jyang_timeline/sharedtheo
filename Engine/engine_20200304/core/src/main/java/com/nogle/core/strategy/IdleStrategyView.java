package com.nogle.core.strategy;

import java.util.Collections;
import java.util.Map;

import com.nogle.strategy.types.Contract;

public class IdleStrategyView extends StrategyView {

    private static IdleStrategyView instance = new IdleStrategyView(1, "IdleStrategy", Collections.emptyMap(), Collections.emptyMap());

    public static IdleStrategyView getInstance() {
        return instance;
    }

    IdleStrategyView(final int strategyId, final String strategyName, final Map<Contract, StrategyQuoteSnapshot> contractToQuoteSnapshot,
        final Map<Contract, OrderViewContainer> contractToOrderViewContainer) {
        super(strategyId, strategyName, contractToQuoteSnapshot, contractToOrderViewContainer);
    }

}
