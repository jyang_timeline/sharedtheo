package com.nogle.core.strategy;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.ThreadContext;

import com.nogle.core.EngineStatusView;
import com.nogle.core.EventTask;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.market.LegacyMarket;
import com.nogle.core.market.Market;
import com.nogle.core.marketdata.DisruptivePriceFeedProxyListener;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.scheduler.LiveStrategyScheduleService;
import com.nogle.core.scheduler.StrategyScheduleListener;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.core.strategy.event.FeedEventHandler;
import com.nogle.core.strategy.event.FillHandler;
import com.nogle.core.strategy.event.MissHandler;
import com.nogle.core.strategy.event.ProtectionHandler;
import com.nogle.core.trade.LiveTradeProxyListener;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.event.handler.BestPriceOrderDetailHandler;
import com.timelinecapital.core.event.handler.MarketBookHandler;
import com.timelinecapital.core.event.handler.OrderActionsHandler;
import com.timelinecapital.core.event.handler.OrderQueueHandler;
import com.timelinecapital.core.event.handler.SnapshotHandler;
import com.timelinecapital.core.event.handler.TickHandler;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDSnapshotView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.strategy.ConfigListener;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.util.QueueFactory;
import com.timelinecapital.core.warmup.LoaderLock;
import com.timelinecapital.strategy.types.MarketData;

public class LegacyStrategyLoader extends StrategyLoader {

    private final LoaderLock loaderLock = new LoaderLock();
    private final LegacyMarket market;

    public LegacyStrategyLoader(final Market market, final Map<Integer, StrategyUpdater> idToUpdater) {
        super(market, null);
        this.market = (LegacyMarket) market;
    }

    @Override
    final StrategyScheduleService buildScheduler(final int strategyId, final StrategyConfig strategyConfig) {
        return new LiveStrategyScheduleService(strategyId, strategyConfig);
    }

    @Override
    final void backBy(final StrategyUpdater strategyUpdater, final EventTask eventTask) {
        strategyUpdater.withEventTask(eventTask);
    }

    @Override
    final void subscribeToFeed(final Map<Instrument, List<FeedType>> contractToFeeds) {
    }

    @Override
    final void subscribeToFeedListeners(final Map<Instrument, List<FeedType>> contractToFeeds, final QuoteSnapshotCache quoteSnapshotCache, final StrategyUpdater strategyUpdater,
        final EventTask eventTask) {
    }

    @Override
    final void unsubscribeFeedListeners(final StrategyUpdater strategyUpdater) {
    }

    @Override
    final void addListeners(
        final EventTask eventTask,
        final StrategyScheduleService scheduler,
        final StrategyUpdater strategyUpdater,
        final QuoteSnapshotCache quoteSnapshotCache,
        final OrderViewCache orderViewCache,
        final Map<Instrument, List<FeedType>> contractToFeeds) {

        addTradeProxyListener(eventTask, strategyUpdater, orderViewCache);
        addPriceFeedListener(eventTask, strategyUpdater, contractToFeeds);
        addConfigListener(strategyUpdater);

        // strategyUpdater.getCommandProcessor().setEventTask(eventTask);

        scheduler.setScheduleListener(new StrategyScheduleListener() {

            /*-
            * StartHandler, triggering strategy start
            */
            private final EventHandler startHandler = new EventHandler() {
                @Override
                public final void handle() {
                    ThreadContext.put("ROUTINGKEY", strategyUpdater.getStrategyId() + "-" + strategyUpdater.getStrategyName());
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Start);
                    strategyUpdater.onStart();
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-StartHandler";
                }
            };

            @Override
            public final void onStart() {
                eventTask.onPendingInvoke(startHandler);
                eventTask.invokeNow(startHandler);
            }

            /*-
            * StopHandler, triggering strategy stop
            */
            private final EventHandler stopHandler = new EventHandler() {
                @Override
                public final void handle() {
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Stop);
                    strategyUpdater.onStop();
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-StopHandler";
                }
            };

            @Override
            public final void onStop() {
                eventTask.invokeNow(stopHandler);
            }

            /*-
            * ShutdownHandler, triggering strategy shutdown
            */
            private final EventHandler shutdownHandler = new EventHandler() {
                @Override
                public final void handle() {
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Shutdown);
                    shutdownStrategy(strategyUpdater);
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-ShutdownHandler";
                }
            };

            @Override
            public final void onShutdown() {
                eventTask.invokeNow(shutdownHandler);
            }
        });

    }

    @Override
    final void startEventTask(final int strategyId, final EventTask eventTask, final StrategyUpdater strategyUpdater) throws StrategyException {
        if (eventTask == null) {
            throw new StrategyException("EventTask has not been initialized");
        }
        eventTask.setLoaderLock(loaderLock, market.canDoWarmup());
        eventTask.onTradeWarmup(strategyUpdater);
        eventTask.start();
    }

    @Override
    final void startStrategy(final StrategyScheduleService timeChecker) {
        if (timeChecker.isStartImmediately()) {
            timeChecker.getScheduleListener().onStart();
        }
    }

    @Override
    final void startFeedWarmup(final Collection<Contract> contracts) {
    }

    @Override
    public final void shutdownStrategy(final StrategyUpdater strategyUpdater) {
        strategyUpdater.onShutdown();
        EventTaskFactory.shutdown(strategyUpdater.getStrategyId());
        // strategyUpdater.getCommandProcessor().setEventTask(null);
        strategyUpdater.withEventTask(null);
        clearStrategySchedules(strategyUpdater);
        clearConfigListener(strategyUpdater);
        clearTradeProxyListener(strategyUpdater);
        clearPriceFeedListener(strategyUpdater);
        idToRiskControl.remove(strategyUpdater.getStrategyId());
        idToUpdater.remove(strategyUpdater.getStrategyId());
        EngineStatusView.getInstance().decreaseStrategyCount();
        log.warn("{} has been terminated.", strategyUpdater.getStrategyName());
    }

    @Override
    final void releaseResourceOnFail(final int strategyId) throws StrategyException {
        final StrategyUpdater strategyUpdater = idToUpdater.remove(strategyId);
        if (strategyUpdater == null) {
            throw new StrategyException("StrategyUpdater instance does not exist: " + strategyId);
        }
        EventTaskFactory.shutdown(strategyId);
        clearPriceFeedListener(strategyUpdater);
        clearTradeProxyListener(strategyUpdater);
        clearConfigListener(strategyUpdater);
        clearStrategySchedules(strategyUpdater);
        clearNotification(strategyUpdater);
        EngineStatusView.getInstance().decreaseStrategyCount();
        log.fatal("{}-{} has been terminated due to exceptions while loading", strategyId, strategyUpdater.getStrategyName());
    }

    private void clearPriceFeedListener(final StrategyUpdater strategyUpdater) {
        try {
            for (final Instrument instrument : strategyUpdater.getContracts()) {
                market.getQuoteView(instrument).clearPriceFeedProxyListener(strategyUpdater.getStrategyId());
            }
            strategyUpdater.getQuoteSnapshotCache().clearQuoteSnapshots();
        } catch (final Exception e) {
            log.error("Remove PriceFeed listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearTradeProxyListener(final StrategyUpdater strategyUpdater) {
        try {
            // final PosteventTradeListener posteventTradeProxyListener = new PosteventTradeListener(
            // strategyUpdater.getStrategyId(),
            // strategyUpdater.getStrategyName(),
            // strategyUpdater.getStrategyStatusView(),
            // (LiveMarket) market);
            // for (final OrderViewContainer orderView : strategyUpdater.getOrderViewCache().getOrderViewContainers()) {
            // orderView.setTradeProxyListener(posteventTradeProxyListener);
            // }
            strategyUpdater.getOrderViewCache().clearOrderViewCache();
        } catch (final Exception e) {
            log.error("Remove TradeProxy listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearConfigListener(final StrategyUpdater strategyUpdater) {
        try {
            strategyUpdater.getConfig().removeConfigListener(strategyUpdater.getStrategyId());
        } catch (final Exception e) {
            log.error("Remove Configuration listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearStrategySchedules(final StrategyUpdater strategyUpdater) {
        try {
            strategyUpdater.getTimeChecker().removeScheduleListener();
        } catch (final Exception e) {
            log.error("Clear Strategy schedules, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearNotification(final StrategyUpdater strategyUpdater) {
        try {
            market.onStrategyExit(strategyUpdater.getStrategyStatusView());
        } catch (final Exception e) {
            log.error("Stop Strategy heartbeat, FAIL: {} {}", e.getMessage(), e);
        }
    }

    void addPriceFeedListener(final EventTask eventTask, final StrategyUpdater strategyUpdater, final Map<Instrument, List<FeedType>> contractToFeeds) {
        final Map<FeedType, FeedEventHandler<? extends MarketData>> feedHandlers = new HashMap<>();

        contractToFeeds.entrySet().forEach(entry -> {
            final QuoteView view = market.subscribeFeed(entry.getKey(), entry.getValue());
            entry.getValue().forEach(f -> {
                switch (f) {
                    case OrderQueue: {
                        final ConcurrentLinkedQueue<BDOrderQueueView> queue = QueueFactory.getOrderQueueView(strategyUpdater.getStrategyId());
                        final OrderQueueHandler<BDOrderQueueView> handler =
                            new OrderQueueHandler<>(queue, strategyUpdater.getStrategyName(), strategyUpdater.getEventListener(f, BDOrderQueueView.class));
                        feedHandlers.put(FeedType.OrderQueue, handler);
                        break;
                    }
                    case OrderActions: {
                        final ConcurrentLinkedQueue<BDOrderActionsView> queue = QueueFactory.getOrderActionsView(strategyUpdater.getStrategyId());
                        final OrderActionsHandler<BDOrderActionsView> handler =
                            new OrderActionsHandler<>(queue, strategyUpdater.getStrategyName(), strategyUpdater.getEventListener(f, BDOrderActionsView.class));
                        feedHandlers.put(FeedType.OrderActions, handler);
                        break;
                    }
                    case BestPriceOrderDetail: {
                        final ConcurrentLinkedQueue<BDBestPriceOrderQueueView> queue = QueueFactory.getBestPriceQueueView(strategyUpdater.getStrategyId());
                        final BestPriceOrderDetailHandler<BDBestPriceOrderQueueView> handler =
                            new BestPriceOrderDetailHandler<>(queue, strategyUpdater.getStrategyName(), strategyUpdater.getEventListener(f, BDBestPriceOrderQueueView.class));
                        feedHandlers.put(FeedType.BestPriceOrderDetail, handler);
                        break;
                    }
                    case MarketBook: {
                        final ConcurrentLinkedQueue<BDBookView> queue = QueueFactory.getMarketBookView(strategyUpdater.getStrategyId());
                        final MarketBookHandler<BDBookView> handler =
                            new MarketBookHandler<>(queue, strategyUpdater.getStrategyName(), strategyUpdater.getEventListener(f, BDBookView.class));
                        feedHandlers.put(FeedType.MarketBook, handler);
                        break;
                    }
                    case Trade: {
                        final ConcurrentLinkedQueue<BDTickView> queue = QueueFactory.getTickView(strategyUpdater.getStrategyId());
                        final TickHandler<BDTickView> handler =
                            new TickHandler<>(queue, strategyUpdater.getStrategyName(), strategyUpdater.getEventListener(f, BDTickView.class));
                        feedHandlers.put(FeedType.Trade, handler);
                        break;
                    }
                    case Snapshot: {
                        final ConcurrentLinkedQueue<BDSnapshotView> queue = QueueFactory.getSnapshot(strategyUpdater.getStrategyId());
                        final SnapshotHandler<BDSnapshotView, BDBookView, BDTickView> handler =
                            new SnapshotHandler<>(queue, strategyUpdater);
                        feedHandlers.put(FeedType.Snapshot, handler);
                        break;
                    }
                }
            });

            final DisruptivePriceFeedProxyListener listener = new DisruptivePriceFeedProxyListener(eventTask, feedHandlers);
            view.addPriceFeedProxyListener(listener, strategyUpdater.getStrategyId(), entry.getValue());
        });
    }

    private void addTradeProxyListener(final EventTask eventTask, final StrategyUpdater strategyUpdater, final OrderViewCache orderViewCache) {
        final FillHandler fillHandler = new FillHandler(strategyUpdater);
        final MissHandler missHandler = new MissHandler(strategyUpdater);
        final ProtectionHandler protectionHandler = new ProtectionHandler(idToRiskControl.get(strategyUpdater.getStrategyId()));

        final LiveTradeProxyListener tradeProxyListener = new LiveTradeProxyListener(eventTask, fillHandler, missHandler, protectionHandler);
        for (final OrderViewContainer orderView : orderViewCache.getOrderViewContainers()) {
            orderView.setTradeProxyListener(tradeProxyListener);
        }
    }

    private void addConfigListener(final StrategyUpdater strategyUpdater) {
        strategyUpdater.getConfig().addConfigListener(
            strategyUpdater.getStrategyId(),
            new ConfigListener() {
                @Override
                public void checkConfig(final Config config) throws Exception {
                    // strategyUpdater.checkConfig(config);
                }

                @Override
                public void onConfigChange(final Config config) {
                    strategyUpdater.onConfigChanged(config);
                }
            });
    }

}
