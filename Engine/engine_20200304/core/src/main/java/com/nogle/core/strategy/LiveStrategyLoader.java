package com.nogle.core.strategy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.apache.logging.log4j.ThreadContext;

import com.nogle.core.EventTask;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.market.LiveMarket;
import com.nogle.core.market.Market;
import com.nogle.core.scheduler.LiveStrategyScheduleService;
import com.nogle.core.scheduler.StrategyScheduleListener;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.core.strategy.event.FillHandler;
import com.nogle.core.strategy.event.MissHandler;
import com.nogle.core.strategy.event.ProtectionHandler;
import com.nogle.core.trade.LiveTradeProxyListener;
import com.nogle.core.trade.PosteventTradeListener;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.TickView;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.handler.GenericMarketDataHandler;
import com.timelinecapital.core.event.handler.SnapshotHandler;
import com.timelinecapital.core.feed.MDChannelListener;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.mktdata.type.MDBookView;
import com.timelinecapital.core.mktdata.type.MDSnapshotView;
import com.timelinecapital.core.mktdata.type.MDTickView;
import com.timelinecapital.core.sharedtheo.LiveSharedTheoLoader;
import com.timelinecapital.core.strategy.ConfigListener;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.util.ExchangeProtocolUtil;
import com.timelinecapital.core.util.QueueFactory;
import com.timelinecapital.core.warmup.LoaderLock;
import com.timelinecapital.strategy.types.OrderQueueView;

public class LiveStrategyLoader extends StrategyLoader {

    private final LoaderLock loaderLock = new LoaderLock();
    private final LiveMarket market;

    public LiveStrategyLoader(final Market market, final Map<Integer, StrategyUpdater> idToUpdater) {
        super(market, new LiveSharedTheoLoader(market));
        this.market = (LiveMarket) market;
    }

    @Override
    final StrategyScheduleService buildScheduler(final int strategyId, final StrategyConfig strategyConfig) {
        return new LiveStrategyScheduleService(strategyId, strategyConfig);
    }

    @Override
    final void backBy(final StrategyUpdater strategyUpdater, final EventTask eventTask) {
        strategyUpdater.withEventTask(eventTask);
    }

    @Override
    void subscribeToFeed(final Map<Instrument, List<FeedType>> contractToFeeds) {
        contractToFeeds.forEach((contract, feedTypeList) -> {
            for (final FeedType feedType : feedTypeList) {
                final Exchange exchange = Exchange.valueOf(contract.getExchange());

                switch (feedType) {
                    case OrderQueue: {
                        final FeedType[] feedTypes = new FeedType[] { feedType };
                        try {
                            market.subscribeFeed(exchange, feedTypes);
                            log.info("Feed subscription: {} {}", exchange, feedTypes);
                        } catch (final FeedException e) {
                            log.error("Fail to subscribe Feed (QuoteOrderDetail) : {}", e.getMessage());
                        }
                        break;
                    }
                    case OrderActions: {
                        final FeedType[] feedTypes = new FeedType[] { feedType };
                        try {
                            market.subscribeFeed(exchange, feedTypes);
                            log.info("Feed subscription: {} {}", exchange, feedTypes);
                        } catch (final FeedException e) {
                            log.error("Fail to subscribe Feed {} : {}", feedType, e.getMessage());
                        }
                        break;
                    }
                    case BestPriceOrderDetail: {
                        final FeedType[] feedTypes = new FeedType[] { feedType };
                        try {
                            market.subscribeFeed(exchange, feedTypes);
                            log.info("Feed subscription: {} {}", exchange, feedTypes);
                        } catch (final FeedException e) {
                            log.error("Fail to subscribe Feed {} : {}", feedType, e.getMessage());
                        }
                        break;
                    }
                    case Trade:
                    case MarketBook:
                    case Snapshot: {
                        final FeedType[] feedTypes = ExchangeProtocolUtil
                            .isQuoteSnapshot(contract.getExchange()) ? new FeedType[] { FeedType.Snapshot } : new FeedType[] { FeedType.MarketBook, FeedType.Trade };
                        try {
                            market.subscribeFeed(exchange, feedTypes);
                            log.info("Feed subscription: {} {}", exchange, feedTypes);
                        } catch (final FeedException e) {
                            log.error("Fail to subscribe Feed (Snapshot/Quotes) : {}", e.getMessage());
                        }
                        break;
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    void subscribeToFeedListeners(
        final Map<Instrument, List<FeedType>> contractToFeeds,
        final QuoteSnapshotCache quoteSnapshotCache,
        final StrategyUpdater strategyUpdater,
        final EventTask eventTask) {

        final MDChannelListener channelListener = new MDChannelListener(strategyUpdater.getStrategyId(), eventTask);
        final List<FeedType> feedTypes = contractToFeeds.values().stream().flatMap(List::stream).distinct().collect(Collectors.toList());
        for (final FeedType feedType : feedTypes) {
            switch (feedType) {
                case OrderQueue: {
                    final ConcurrentLinkedQueue<OrderQueueView> queue =
                        (ConcurrentLinkedQueue<OrderQueueView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final GenericMarketDataHandler<OrderQueueView> handler = new GenericMarketDataHandler<>(
                        OrderQueueView.class,
                        queue,
                        strategyUpdater.getStrategyName(),
                        strategyUpdater.getEventListener(feedType, OrderQueueView.class));
                    channelListener.registerEventHandler(handler, DataType.ORDERQUEUE);
                    break;
                }
                case OrderActions: {
                    final ConcurrentLinkedQueue<QuoteOrderView> queue =
                        (ConcurrentLinkedQueue<QuoteOrderView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final GenericMarketDataHandler<QuoteOrderView> handler = new GenericMarketDataHandler<>(
                        QuoteOrderView.class,
                        queue,
                        strategyUpdater.getStrategyName(),
                        strategyUpdater.getEventListener(feedType, QuoteOrderView.class));
                    channelListener.registerEventHandler(handler, DataType.ORDERACTIONS);
                    break;
                }
                case BestPriceOrderDetail: {
                    final ConcurrentLinkedQueue<BestOrderView> queue =
                        (ConcurrentLinkedQueue<BestOrderView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final GenericMarketDataHandler<BestOrderView> handler = new GenericMarketDataHandler<>(
                        BestOrderView.class,
                        queue,
                        strategyUpdater.getStrategyName(),
                        strategyUpdater.getEventListener(feedType, BestOrderView.class));
                    channelListener.registerEventHandler(handler, DataType.BESTORDERDETAIL);
                    break;
                }
                case MarketBook: {
                    final ConcurrentLinkedQueue<BookView> queue =
                        (ConcurrentLinkedQueue<BookView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final GenericMarketDataHandler<BookView> handler = new GenericMarketDataHandler<>(
                        BookView.class,
                        queue,
                        strategyUpdater.getStrategyName(),
                        strategyUpdater.getEventListener(feedType, BookView.class));
                    channelListener.registerEventHandler(handler, DataType.MARKETBOOK);
                    break;
                }
                case Trade: {
                    final ConcurrentLinkedQueue<TickView> queue =
                        (ConcurrentLinkedQueue<TickView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final GenericMarketDataHandler<TickView> handler = new GenericMarketDataHandler<>(
                        TickView.class,
                        queue,
                        strategyUpdater.getStrategyName(),
                        strategyUpdater.getEventListener(feedType, TickView.class));
                    channelListener.registerEventHandler(handler, DataType.TICK);
                    break;
                }
                case Snapshot: {
                    final ConcurrentLinkedQueue<MDSnapshotView> queue =
                        (ConcurrentLinkedQueue<MDSnapshotView>) QueueFactory.getWithConcreteInstance(strategyUpdater.getStrategyId(), feedType);
                    final SnapshotHandler<MDSnapshotView, MDBookView, MDTickView> handler = new SnapshotHandler<>(queue, strategyUpdater);
                    channelListener.registerEventHandler(handler, DataType.SNAPSHOT);
                    break;
                }
            }

        }
        contractToFeeds.entrySet().stream().forEach(entry -> {
            entry.getValue().forEach(feedType -> {
                market.subscribeContract(strategyUpdater.getStrategyId(), entry.getKey(), feedType, channelListener);
            });
        });
    }

    @Override
    void unsubscribeFeedListeners(final StrategyUpdater strategyUpdater) {
        for (final Entry<Instrument, List<FeedType>> entry : strategyUpdater.getContractToFeeds().entrySet()) {
            entry.getValue().forEach(feedType -> {
                log.debug("Unsubscribe {} {} {}", strategyUpdater.getStrategyId(), entry.getKey(), feedType);
                market.unsubscribeContract(strategyUpdater.getStrategyId(), entry.getKey(), feedType);
            });
        }
    }

    @Override
    final void addListeners(
        final EventTask eventTask,
        final StrategyScheduleService scheduler,
        final StrategyUpdater strategyUpdater,
        final QuoteSnapshotCache quoteSnapshotCache,
        final OrderViewCache orderViewCache,
        final Map<Instrument, List<FeedType>> contractToFeeds) {

        addTradeProxyListener(eventTask, strategyUpdater, orderViewCache);
        addConfigListener(strategyUpdater);

        // strategyUpdater.getCommandProcessor().setEventTask(eventTask);

        scheduler.setScheduleListener(new StrategyScheduleListener() {

            /*-
             * StartHandler, triggering strategy start
             */
            private final EventHandler startHandler = new EventHandler() {
                @Override
                public final void handle() {
                    ThreadContext.put("ROUTINGKEY", strategyUpdater.getStrategyId() + "-" + strategyUpdater.getStrategyName());
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Start);
                    strategyUpdater.onStart();
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-StartHandler";
                }
            };

            @Override
            public final void onStart() {
                eventTask.onPendingInvoke(startHandler);
                eventTask.invokeNow(startHandler);
            }

            /*-
             * StopHandler, triggering strategy stop
             */
            private final EventHandler stopHandler = new EventHandler() {
                @Override
                public final void handle() {
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Stop);
                    strategyUpdater.onStop();
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-StopHandler";
                }
            };

            @Override
            public final void onStop() {
                eventTask.invokeNow(stopHandler);
            }

            /*-
             * ShutdownHandler, triggering strategy shutdown
             */
            private final EventHandler shutdownHandler = new EventHandler() {
                @Override
                public final void handle() {
                    market.updateStrategyStatus(strategyUpdater.getStrategyId(), StrategyLifeCycle.Shutdown);
                    shutdownStrategy(strategyUpdater);
                }

                @Override
                public final String getName() {
                    return strategyUpdater.getStrategyId() + "-ShutdownHandler";
                }
            };

            @Override
            public final void onShutdown() {
                eventTask.invokeNow(shutdownHandler);
            }
        });

    }

    @Override
    final void startEventTask(final int strategyId, final EventTask eventTask, final StrategyUpdater strategyUpdater) throws StrategyException {
        if (eventTask == null) {
            throw new StrategyException("EventTask has not been initialized");
        }
        market.onStrategyEnter(strategyUpdater.getStrategyStatusView());

        eventTask.setLoaderLock(loaderLock, market.canDoWarmup());
        eventTask.onTradeWarmup(strategyUpdater);
        eventTask.start();

        if (EngineConfig.enableWarmup()) {
            // TODO RING mode should not wait?
            // loaderLock.doWait(5);
            // startFeedWarmup(contracts.values());
        }
    }

    @Override
    final void startStrategy(final StrategyScheduleService timeChecker) {
        if (timeChecker.isStartImmediately()) {
            timeChecker.getScheduleListener().onStart();
        }
    }

    @Override
    final void startFeedWarmup(final Collection<Contract> contracts) {
    }

    @Override
    public final void shutdownStrategy(final StrategyUpdater strategyUpdater) {
        strategyUpdater.onShutdown();
        EventTaskFactory.shutdown(strategyUpdater.getStrategyId());

        strategyUpdater.withEventTask(null);

        clearStrategySchedules(strategyUpdater);
        clearConfigListener(strategyUpdater);
        clearTradeProxyListener(strategyUpdater);
        unsubscribeFeedListeners(strategyUpdater);

        idToStatusView.remove(strategyUpdater.getStrategyId());
        idToEventManager.remove(strategyUpdater.getStrategyId());
        idToRiskControl.remove(strategyUpdater.getStrategyId());
        idToUpdater.remove(strategyUpdater.getStrategyId());

        market.onStrategyExit(strategyUpdater.getStrategyStatusView());

        log.warn("{} has been terminated.", strategyUpdater.getStrategyName());
    }

    @Override
    final void releaseResourceOnFail(final int strategyId) throws StrategyException {
        final StrategyUpdater strategyUpdater = idToUpdater.remove(strategyId);
        if (strategyUpdater == null) {
            throw new StrategyException("StrategyUpdater instance does not exist: " + strategyId);
        }
        EventTaskFactory.shutdown(strategyId);

        unsubscribeFeedListeners(strategyUpdater);
        clearTradeProxyListener(strategyUpdater);
        clearConfigListener(strategyUpdater);
        clearStrategySchedules(strategyUpdater);

        idToStatusView.remove(strategyUpdater.getStrategyId());
        idToEventManager.remove(strategyUpdater.getStrategyId());
        idToRiskControl.remove(strategyUpdater.getStrategyId());

        market.onStrategyBuildFailed(strategyUpdater.getStrategyStatusView());

        log.fatal("{}-{} has been terminated due to exceptions while loading", strategyId, strategyUpdater.getStrategyName());
    }

    private void clearTradeProxyListener(final StrategyUpdater strategyUpdater) {
        try {
            final PosteventTradeListener posteventTradeProxyListener = new PosteventTradeListener(
                strategyUpdater.getStrategyId(),
                strategyUpdater.getStrategyName(),
                strategyUpdater.getStrategyStatusView(),
                market);
            for (final OrderViewContainer orderView : strategyUpdater.getOrderViewCache().getOrderViewContainers()) {
                orderView.setTradeProxyListener(posteventTradeProxyListener);
            }
            strategyUpdater.getOrderViewCache().clearOrderViewCache();
        } catch (final Exception e) {
            log.error("Remove TradeProxy listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearConfigListener(final StrategyUpdater strategyUpdater) {
        try {
            strategyUpdater.getConfig().removeConfigListener(strategyUpdater.getStrategyId());
        } catch (final Exception e) {
            log.error("Remove Configuration listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void clearStrategySchedules(final StrategyUpdater strategyUpdater) {
        try {
            strategyUpdater.getTimeChecker().removeScheduleListener();
        } catch (final Exception e) {
            log.error("Clear Strategy schedules, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void addTradeProxyListener(final EventTask eventTask, final StrategyUpdater strategyUpdater, final OrderViewCache orderViewCache) {
        final FillHandler fillHandler = new FillHandler(strategyUpdater);
        final MissHandler missHandler = new MissHandler(strategyUpdater);
        final ProtectionHandler protectionHandler = new ProtectionHandler(idToRiskControl.get(strategyUpdater.getStrategyId()));

        final LiveTradeProxyListener tradeProxyListener = new LiveTradeProxyListener(eventTask, fillHandler, missHandler, protectionHandler);
        for (final OrderViewContainer orderView : orderViewCache.getOrderViewContainers()) {
            orderView.setTradeProxyListener(tradeProxyListener);
        }
    }

    private void addConfigListener(final StrategyUpdater strategyUpdater) {
        strategyUpdater.getConfig().addConfigListener(
            strategyUpdater.getStrategyId(),
            new ConfigListener() {
                @Override
                public void checkConfig(final Config config) throws Exception {
                    riskManager.getPositionManager(strategyUpdater.getStrategyId()).values().forEach(manager -> manager.accept(config));
                }

                @Override
                public void onConfigChange(final Config config) {
                    riskManager.getPositionManager(strategyUpdater.getStrategyId()).values().forEach(manager -> manager.updateConfig(config));
                    strategyUpdater.onConfigChanged(config);
                }
            });
    }

}
