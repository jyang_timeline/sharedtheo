package com.nogle.core.strategy;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.nogle.core.exception.OrderViewNotFoundException;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionView;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.riskmanagement.PositionManager;

public class OrderViewCache {

    private final Map<Contract, OrderViewContainer> contractToOrderview = new HashMap<>(8, 0.75f);

    private Map<Contract, OrderViewContainer> orderviewsSnapshot;

    public OrderViewCache() {
    }

    public void addOrderView(final Contract contract, final OrderViewContainer orderView) {
        contractToOrderview.put(contract, orderView);
        orderviewsSnapshot = contractToOrderview;
    }

    void clearOrderViewCache() {
        for (final OrderViewContainer orderViewContainer : contractToOrderview.values()) {
            orderViewContainer.setOrderEntryPermission(false);
            orderViewContainer.clear();
        }
        orderviewsSnapshot = Collections.unmodifiableMap(new HashMap<>(contractToOrderview));
        contractToOrderview.clear();
    }

    public PositionView getPositionView(final Contract contract) {
        return contractToOrderview.get(contract);
    }

    public Collection<PositionView> getPositionViews() {
        return Collections.unmodifiableCollection(contractToOrderview.values());
    }

    Collection<OrderViewContainer> getOrderViewContainers() {
        return Collections.unmodifiableCollection(contractToOrderview.values());
    }

    public Map<Contract, OrderViewContainer> getContractToOrderView() {
        return Collections.unmodifiableMap(contractToOrderview);
    }

    OrderHolder getOrderHolder(final TimeCondition tif, final Side side, final Contract contract) {
        if (!contractToOrderview.containsKey(contract)) {
            throw new OrderViewNotFoundException("Strategy does not have this OrderView: " + contract.getSymbol() + " " + contractToOrderview.toString());
        }
        final OrderViewContainer orderview = contractToOrderview.get(contract);
        orderview.setTrading();
        return orderview.getOrderHolder(tif, side);
    }

    OrderHolder getOrderHolderBySymbolName(final TimeCondition tif, final Side side, final String name) {
        final Optional<Entry<Contract, OrderViewContainer>> result =
            orderviewsSnapshot.entrySet().stream().filter(entry -> entry.getKey().getSymbol().contentEquals(name)).findAny();
        if (!result.isPresent()) {
            throw new OrderViewNotFoundException("OrderView of contract: " + name + " does not exist " + contractToOrderview.toString());
        }
        result.get().getValue().setTrading();
        return result.get().getValue().getOrderHolder(tif, side);
    }

    PositionManager getPositionManagerBySymbolName(final String contractName) {
        OrderViewContainer orderViewContainer = null;
        for (final Contract contract : orderviewsSnapshot.keySet()) {
            if (contract.getSymbol().contentEquals(contractName)) {
                orderViewContainer = orderviewsSnapshot.get(contract);
                break;
            }
        }
        if (orderViewContainer == null) {
            throw new OrderViewNotFoundException("OrderView of contract:  " + contractName + " does not exist " + contractToOrderview.toString());
        }
        return orderViewContainer.getPositionManager();
    }

    void updateOrderPermission(final boolean permission) {
        for (final OrderViewContainer orderViewContainer : contractToOrderview.values()) {
            orderViewContainer.setOrderEntryPermission(permission);
        }
    }

}
