package com.nogle.core.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.PositionView;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.DummyTrade;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.stats.ExecReportEvent;
import com.timelinecapital.strategy.types.PositionType;

/**
 * Only one order view of a contract per Model. This order view contains two types of order, IOC and GFD.
 *
 * @author Mark
 *
 */
public class OrderViewContainer implements PositionView, DummyTrade, MarketOpenAware {
    private static final Logger log = LogManager.getLogger(OrderViewContainer.class);

    private final List<OrderHolder> allOrderViews = new ArrayList<>();

    private final ClientOrderIdGenerator idGenerator;
    private final Contract contract;
    private final OrderSender orderSender;
    private final PositionManager positionManager;
    private final PositionTracker positionTracker;
    private final ExecReportEvent statsCollector;

    private final OrderHolder buyDayOrderView;
    private final OrderHolder sellDayOrderView;
    private final OrderHolder buyIOCOrderView;
    private final OrderHolder sellIOCOrderView;

    private boolean isTrading = false;

    public OrderViewContainer(final ClientOrderIdGenerator idGenerator, final Contract contract, final TradeAppendix tradeAppendix, final OrderSender orderSender,
        final String threadContext) {
        this.idGenerator = idGenerator;
        this.contract = contract;
        this.orderSender = orderSender;
        this.positionManager = tradeAppendix.getPositionManager();
        this.positionTracker = tradeAppendix.getPositionTracker();
        this.statsCollector = tradeAppendix.getStatsCollector();

        buyDayOrderView = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.BUY, tradeAppendix, orderSender, threadContext, statsCollector);
        sellDayOrderView = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.SELL, tradeAppendix, orderSender, threadContext, statsCollector);
        buyIOCOrderView = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.BUY, tradeAppendix, orderSender, threadContext, statsCollector);
        sellIOCOrderView = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.SELL, tradeAppendix, orderSender, threadContext, statsCollector);

        allOrderViews.clear();
        allOrderViews.add(buyDayOrderView);
        allOrderViews.add(sellDayOrderView);
        allOrderViews.add(buyIOCOrderView);
        allOrderViews.add(sellIOCOrderView);
    }

    public List<OrderHolder> getOrderHolders() {
        return Collections.unmodifiableList(allOrderViews);
    }

    public OrderHolder getOrderHolder(final TimeCondition tif, final Side side) {
        switch (tif) {
            case GFD:
                return getDayOrderBySide(side);
            case IOC:
                return getIOCOrderBySide(side);
            default:
                return null;
        }
    }

    private OrderHolder getIOCOrderBySide(final Side side) {
        switch (side) {
            case BUY:
                return buyIOCOrderView;
            case SELL:
                return sellIOCOrderView;
            default:
                return null;
        }
    }

    private OrderHolder getDayOrderBySide(final Side side) {
        switch (side) {
            case BUY:
                return buyDayOrderView;
            case SELL:
                return sellDayOrderView;
            default:
                return null;
        }
    }

    @Override
    public final void onDummyMessage(final Side side, final TimeCondition tif, final long qty, final double price, final int positionMax, final TradeEvent tradeEventHandler)
        throws Exception {
        switch (tif) {
            case IOC:
                orderSender.sendNewIOCOrder(idGenerator.getNextId(), side, qty, price, PositionPolicy.AUTO, positionMax, tradeEventHandler,
                    TradeClock.getCurrentMillis(), TradeClock.getCurrentMicrosOnly());
                break;
            case GFD:
                final long clOrdId = idGenerator.getNextId();
                orderSender.sendNewDayOrder(clOrdId, side, qty, price, PositionPolicy.AUTO, positionMax, tradeEventHandler,
                    TradeClock.getCurrentMillis(), TradeClock.getCurrentMicrosOnly());
                orderSender.sendOrderCancel(clOrdId, side, tradeEventHandler,
                    TradeClock.getCurrentMillis(), TradeClock.getCurrentMicrosOnly());
                break;
            default:
                break;
        }
    }

    @Override
    public final void rewind() {
        idGenerator.resetId();
        positionManager.reset();
        positionTracker.rewind();
        for (final OrderHolder orderHolder : allOrderViews) {
            orderHolder.rewind();
        }
    }

    @Override
    public final boolean isReady() {
        boolean isReady = positionTracker.isReady();
        for (final OrderHolder orderHolder : allOrderViews) {
            isReady &= orderHolder.isReady();
        }
        return isReady;
    }

    public void setTrading() {
        this.isTrading = true;
    }

    public boolean isTrading() {
        return isTrading;
    }

    public void clear() {
        for (final OrderHolder orderHolder : allOrderViews) {
            orderHolder.release();
        }
        // allOrderViews.clear();
    }

    public void checkExit() {
        if (positionTracker.getOrderCount() == 0 && !hasOutstandingOrders()) {
            setOrderEntryPermission(false);
            log.warn("{} in EXIT mode. Revoke permission of sending orders by scheduler", contract);
        }
    }

    public void onExitMode() {
        if (positionTracker.getOrderCount() == 0 && !hasOutstandingOrders()) {
            setOrderEntryPermission(false);
            log.warn("{} in EXIT mode. Revoke permission of sending orders", contract);
            return;
        }
        for (final OrderHolder orderHolder : allOrderViews) {
            if ((positionTracker.getPosition() > 0 && orderHolder.getSide().equals(Side.BUY)) ||
                (positionTracker.getPosition() < 0 && orderHolder.getSide().equals(Side.SELL)) ||
                positionTracker.getPosition() == 0) {
                orderHolder.clearAllOrders();
                orderHolder.commit(0L);
            }
            orderHolder.onExitOnly(this);
        }
    }

    public void setTradeProxyListener(final TradeProxyListener tradeProxyListener) {
        for (final OrderHolder orderHolder : allOrderViews) {
            orderHolder.setTradeProxyListener(tradeProxyListener);
        }
    }

    public void setOrderEntryPermission(final boolean permission) {
        for (final OrderHolder orderHolder : allOrderViews) {
            orderHolder.setSendOrderPermission(permission);
        }
    }

    public boolean getOrderEntryPermission() {
        boolean result = false;
        for (final OrderHolder orderHolder : allOrderViews) {
            result |= orderHolder.getSendOrderPermission();
        }
        return result;
    }

    public boolean hasOutstandingOrders() {
        boolean result = false;
        for (final OrderHolder orderHolder : allOrderViews) {
            result |= (orderHolder.getOutstandingOrderCount() > 0 ? true : false);
        }
        return result;
    }

    PositionManager getPositionManager() {
        return positionManager;
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final long getStaticPositionInfo(final PositionType type) {
        return statsCollector.getStaticPositionInfo(type);
    }

    @Override
    public final long getSystemPosition() {
        return statsCollector.getPosition();
    }

    @Override
    public final long getSystemVolume() {
        return statsCollector.getVolume();
    }

    @Override
    public final long getSystemOrders() {
        return statsCollector.getOrderAcks();
    }

    @Override
    public final long getSystemCancels() {
        return statsCollector.getCancelAcks();
    }

    @Override
    public final long getOvernightPositions() {
        return positionTracker.getOvernightPositions();
    }

    @Override
    public final long getPosition() {
        return positionTracker.getPosition();
    }

    @Override
    public final long getVolume() {
        return positionTracker.getVolume();
    }

    @Override
    public final long getCancels() {
        return positionTracker.getCancelCount();
    }

    @Override
    public final double getTotalPnl(final double marketPrice) {
        return positionTracker.getTotalPnl(marketPrice);
    }

    @Override
    public final double getOpenPnl(final double marketPrice) {
        return positionTracker.getOpenPnl(marketPrice);
    }

    @Override
    public final double getClosedPnl() {
        return positionTracker.getClosedPnl();
    }

    @Override
    public final double getCommission() {
        return positionTracker.getTotalChargedFee();
    }

}
