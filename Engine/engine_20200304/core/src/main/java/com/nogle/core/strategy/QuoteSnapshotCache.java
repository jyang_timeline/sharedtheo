package com.nogle.core.strategy;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.nogle.strategy.types.Contract;

public class QuoteSnapshotCache {

    private final Map<Contract, StrategyQuoteSnapshot> contractToQuoteView = new HashMap<>(8, 0.75f);

    public QuoteSnapshotCache() {
    }

    public void addQuoteSnapshot(final Contract contract, final StrategyQuoteSnapshot quoteView) {
        contractToQuoteView.put(contract, quoteView);
    }

    public StrategyQuoteSnapshot getQuoteSnapshot(final Contract contract) {
        return contractToQuoteView.get(contract);
    }

    void clearQuoteSnapshots() {
        contractToQuoteView.clear();
    }

    Collection<StrategyQuoteSnapshot> getQuoteSnapshots() {
        return Collections.unmodifiableCollection(contractToQuoteView.values());
    }

    public Map<Contract, StrategyQuoteSnapshot> getContractToQuoteSnapshot() {
        return Collections.unmodifiableMap(contractToQuoteView);
    }

}
