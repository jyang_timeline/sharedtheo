package com.nogle.core.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.nogle.core.EventTask;
import com.nogle.core.market.Market;
import com.nogle.core.market.SimulatedMarket;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.marketdata.SimulatedPriceFeedProxyListener;
import com.nogle.core.scheduler.SimulatedStratrgyScheduleService;
import com.nogle.core.scheduler.StrategyScheduleListener;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.trade.SimulatedTradeProxyListener;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.instrument.SecurityType;
import com.timelinecapital.core.sharedtheo.SimulationSharedTheoLoader;
import com.timelinecapital.core.strategy.StrategyConfig;

public class SimulatedStrategyLoader extends StrategyLoader {

    private final List<QuoteView> quoteViews = new ArrayList<>();

    private final SimulatedMarket simMarket;

    public SimulatedStrategyLoader(final Market market, final Map<Integer, StrategyUpdater> idToUpdater) {
        super(market, new SimulationSharedTheoLoader(market));
        simMarket = (SimulatedMarket) market;
    }

    @Override
    final StrategyScheduleService buildScheduler(final int strategyId, final StrategyConfig strategyConfig) {
        return new SimulatedStratrgyScheduleService(strategyConfig);
    }

    @Override
    final void backBy(final StrategyUpdater strategyUpdater, final EventTask eventTask) {
    }

    @Override
    void subscribeToFeed(final Map<Instrument, List<FeedType>> contractToFeeds) {
    }

    @Override
    void subscribeToFeedListeners(final Map<Instrument, List<FeedType>> contractToFeeds, final QuoteSnapshotCache quoteSnapshotCache, final StrategyUpdater strategyUpdater,
        final EventTask eventTask) {
    }

    @Override
    void unsubscribeFeedListeners(final StrategyUpdater strategyUpdater) {
    }

    @Override
    final void addListeners(
        final EventTask eventTask,
        final StrategyScheduleService scheduler,
        final StrategyUpdater strategyUpdater,
        final QuoteSnapshotCache quoteSnapshotCache,
        final OrderViewCache orderViewCache,
        final Map<Instrument, List<FeedType>> contractToFeeds) {

        addPriceFeedListener(strategyUpdater, quoteSnapshotCache, contractToFeeds);
        addTradeProxyListener(strategyUpdater, orderViewCache);

        scheduler.setScheduleListener(new StrategyScheduleListener() {

            @Override
            public void onStart() {
                strategyUpdater.onSimulationMode();
                strategyUpdater.onStart();
                strategyUpdater.onTradingEnable();
            }

            @Override
            public void onStop() {
                strategyUpdater.onStop();
            }

            @Override
            public void onShutdown() {
                strategyUpdater.onShutdown();
            }

        });
    }

    @Override
    final void startEventTask(final int strategyId, final EventTask eventTask, final StrategyUpdater strategyUpdater) {
    }

    @Override
    final void startStrategy(final StrategyScheduleService timeChecker) {
    }

    @Override
    final void startFeedWarmup(final Collection<Contract> contracts) {
    }

    @Override
    public final void shutdownStrategy(final StrategyUpdater strategyUpdater) {
        idToUpdater.forEach((id, updater) -> {
            cleanrPriceFeedListener(updater);
        });
        idToUpdater.clear();
        sharedTheoLoader.clear();
    }

    @Override
    final void releaseResourceOnFail(final int strategyId) {
    }

    private void addPriceFeedListener(final StrategyUpdater strategyUpdater, final QuoteSnapshotCache quoteSnapshotCache, final Map<Instrument, List<FeedType>> contractToFeeds) {
        for (final Entry<Instrument, List<FeedType>> entry : contractToFeeds.entrySet()) {
            final QuoteView view = simMarket.subscribeFeed(entry.getKey(), entry.getValue());
            quoteViews.add(view);

            if (entry.getKey().getSecurityType().equals(SecurityType.STOCKS) || entry.getKey().getSecurityType().equals(SecurityType.FUTURES)) {
                view.addSimExchangePriceFeedProxyListener(simMarket.getSimulatedExchangePriceFeedListener(entry.getKey()));
            }
            view.addPriceFeedProxyListener(
                new SimulatedPriceFeedProxyListener(strategyUpdater, quoteSnapshotCache.getQuoteSnapshot(entry.getKey())),
                strategyUpdater.getStrategyId(),
                entry.getValue());
        }
    }

    private void cleanrPriceFeedListener(final StrategyUpdater strategyUpdater) {
        try {
            quoteViews.forEach(view -> view.clearPriceFeedProxyListener(strategyUpdater.getStrategyId()));
            strategyUpdater.getQuoteSnapshotCache().clearQuoteSnapshots();
        } catch (final Exception e) {
            log.error("Remove PriceFeed listener, FAIL: {} {}", e.getMessage(), e);
        }
    }

    private void addTradeProxyListener(final StrategyUpdater strategyUpdater, final OrderViewCache orderViewCache) {
        final TradeProxyListener tradeProxyListener = new SimulatedTradeProxyListener(strategyUpdater);
        for (final OrderViewContainer orderView : orderViewCache.getOrderViewContainers()) {
            orderView.setTradeProxyListener(tradeProxyListener);
        }
    }

}
