package com.nogle.core.strategy;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.EventTask;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.exception.StrategyAlreadyRunningException;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.market.Market;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.strategy.logging.StrategyLogWrapper;
import com.nogle.core.strategy.logging.StrategyTradeLogger;
import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.api.StrategyBuilder;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.event.handler.ContractInfoHandler;
import com.timelinecapital.core.event.handler.StaticPositionSyncHandler;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.sharedtheo.SharedTheoLoader;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.strategy.StrategyEventManager;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.types.StrategyEventType;

public abstract class StrategyLoader {
    protected static final Logger log = LogManager.getLogger(StrategyLoader.class);

    protected final Map<Integer, String> idToName;
    protected final Map<Integer, StrategyUpdater> idToUpdater;
    protected final Map<Integer, StrategyEventManager> idToEventManager;
    protected final Map<Integer, StrategyView> idToStatusView;
    protected final Map<Integer, ProtectiveManager> idToRiskControl;
    protected final SharedTheoLoader sharedTheoLoader;
    protected final RiskManager riskManager;

    private final Market market;

    public StrategyLoader(final Market market, final SharedTheoLoader sharedTheoLoader) {
        this.market = market;
        this.sharedTheoLoader = sharedTheoLoader;
        idToName = CoreController.getInstance().getIdToName();
        idToUpdater = CoreController.getInstance().getIdToUpdater();
        idToEventManager = CoreController.getInstance().getIdToEventManager();
        idToStatusView = CoreController.getInstance().getIdToStatusView();
        idToRiskControl = CoreController.getInstance().getIdToRiskControl();
        riskManager = CoreController.getInstance().getRiskManager();
    }

    abstract void backBy(StrategyUpdater strategyUpdater, EventTask eventTask);

    abstract void subscribeToFeed(Map<Instrument, List<FeedType>> contractToFeeds);

    abstract void subscribeToFeedListeners(Map<Instrument, List<FeedType>> contractToFeeds, QuoteSnapshotCache quoteSnapshotCache, StrategyUpdater strategyUpdater,
        EventTask eventTask);

    abstract void unsubscribeFeedListeners(StrategyUpdater strategyUpdater);

    abstract void addListeners(EventTask eventTask, StrategyScheduleService scheduler, StrategyUpdater strategyUpdater, QuoteSnapshotCache quoteSnapshotCache,
        OrderViewCache orderViewCache, Map<Instrument, List<FeedType>> contractToFeeds);

    abstract void startEventTask(int strategyId, EventTask eventTask, StrategyUpdater strategyUpdater) throws StrategyException;

    abstract void startStrategy(StrategyScheduleService scheduler);

    abstract void startFeedWarmup(Collection<Contract> contracts);

    public abstract void shutdownStrategy(StrategyUpdater strategyUpdater);

    abstract void releaseResourceOnFail(int strategyId) throws StrategyException;

    /*
     * Default loading model entry through LoadCommand
     */
    public void buildStrategy(final TradingAccount account, final int strategyId, final StrategyConfig strategyConfig) throws StrategyAlreadyRunningException, StrategyException {
        this.buildStrategy(account, strategyId, strategyConfig, new HashMap<>());
    }

    /*
     * For simulation task to build and run strategy in a new thread
     */
    public void buildStrategy(
        final TradingAccount account,
        final int strategyId,
        final StrategyConfig strategyConfig,
        final Map<String, Instrument> contracts) throws StrategyAlreadyRunningException, StrategyException {

        if (idToUpdater.containsKey(strategyId)) {
            throw new StrategyAlreadyRunningException("Engine has already had strategy loaded: " + strategyId);
        }

        buildSharedTheos(strategyConfig);

        market.onStrategyBuild();

        // map for feed & trade
        final Map<Instrument, List<FeedType>> contractToFeeds = new HashMap<>();
        final Map<Instrument, TradeAppendix> contractToTrades = new HashMap<>();
        final Map<Contract, PositionManager> contractToPositionManager = new HashMap<>();
        final Map<Contract, PriceManager> contractToPriceManager = new HashMap<>();

        try {
            final QuoteSnapshotCache quoteSnapshotCache = new QuoteSnapshotCache();
            final OrderViewCache orderViewCache = new OrderViewCache();

            // TaskThread
            final EventTask eventTask = EventTaskFactory.get(
                Strategy.class,
                strategyId,
                strategyConfig.getName());
            final StrategyEventManager eventManager = new StrategyEventManager(eventTask);

            // Strategy Builder
            final StrategyBuilder strategyBuilder = getStrategyBuilder(
                strategyConfig.getClassLoader(),
                strategyConfig.getString(StrategyConfigProtocol.Key.strategyFullPath.toUntypedString()));

            // Strategy View
            final StrategyView view = new StrategyView(
                strategyId,
                strategyConfig.getName(),
                quoteSnapshotCache.getContractToQuoteSnapshot(),
                orderViewCache.getContractToOrderView());

            // Strategy Logger
            final StrategyTradeLogger strategyTradeLogger = new StrategyTradeLogger(
                new StrategyLogWrapper(
                    strategyId,
                    strategyConfig.getName(),
                    strategyBuilder.getClass(),
                    view).getLogger());

            // Strategy Scheduler
            final StrategyScheduleService scheduler = buildScheduler(strategyId, strategyConfig);
            scheduler.buildTradingSessions();

            final StrategyUpdater strategyUpdater = new StrategyUpdater(
                strategyId,
                strategyConfig,
                scheduler,
                strategyBuilder,
                strategyTradeLogger,
                view,
                OverviewFactory.getOverview(account),
                quoteSnapshotCache,
                orderViewCache,
                contracts,
                contractToFeeds,
                sharedTheoLoader);
            backBy(strategyUpdater, eventTask);

            /*
             * System-wide map
             */
            idToName.put(strategyId, strategyConfig.getName());
            idToStatusView.put(strategyId, view);
            idToUpdater.put(strategyId, strategyUpdater);
            idToEventManager.put(strategyId, eventManager);

            /*
             * Core building: Contract - Feed - Quote - Trade
             */
            CoreBuilder.buildContract(strategyConfig, contracts);
            buildContractInfoControl(eventManager, strategyUpdater, contractToPriceManager);

            CoreBuilder.buildFeedInfos(strategyConfig, contracts, contractToFeeds);
            buildQuoteView(quoteSnapshotCache, contractToFeeds);

            buildRiskControl(strategyConfig, strategyUpdater);
            buildPositionControl(strategyId, strategyConfig, contracts, contractToPositionManager);
            buildPriceControl(strategyId, strategyConfig, quoteSnapshotCache, contractToPriceManager);
            CoreBuilder.buildTradeInfos(account, strategyConfig, contracts, idToRiskControl.get(strategyId), contractToPositionManager, contractToPriceManager, contractToTrades);
            buildOrderView(orderViewCache, contractToTrades, strategyId);

            /*
             * TODO: work flow re-factoring
             */
            subscribeToFeed(contractToFeeds);
            subscribeToFeedListeners(contractToFeeds, quoteSnapshotCache, strategyUpdater, eventTask);

            addListeners(eventTask, scheduler, strategyUpdater, quoteSnapshotCache, orderViewCache, contractToFeeds);

            startEventTask(strategyId, eventTask, strategyUpdater);
            startStrategy(scheduler);

            log.info("Loaded Strategy: {}", idToUpdater.keySet());

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            try {
                releaseResourceOnFail(strategyId);
            } catch (final StrategyException se) {
                log.warn("Strategy instance not found: {}", se.getMessage());
                EventTaskFactory.shutdown(strategyId);
            }
            throw new StrategyException(e);
        }
    }

    private void buildSharedTheos(final StrategyConfig strategyConfig) throws StrategyException {
        for (final String propertiesName : strategyConfig.getSharedTheos()) {
            if (sharedTheoLoader.get(propertiesName) == null) {
                sharedTheoLoader.buildSharedTheo(strategyConfig.getClassLoader(), propertiesName);
            }
        }
    }

    public StrategyUpdater[] getStrategyUpdaters() {
        return idToUpdater.values().toArray(new StrategyUpdater[idToUpdater.size()]);
    }

    private String getThreadContext(final int strategyId) {
        final StringBuilder sb = new StringBuilder();
        sb.append(strategyId).append("-").append(idToName.get(strategyId));
        return sb.toString();
    }

    abstract StrategyScheduleService buildScheduler(int strategyId, StrategyConfig strategyConfig);

    private void buildContractInfoControl(
        final StrategyEventManager eventManager,
        final StrategyUpdater strategyUpdater,
        final Map<Contract, PriceManager> contractToPriceManager) {
        eventManager.register(StrategyEventType.ContractRefresh, new ContractInfoHandler(strategyUpdater, contractToPriceManager));
        eventManager.register(StrategyEventType.PositionSync, new StaticPositionSyncHandler(strategyUpdater));
    }

    private void buildRiskControl(
        final StrategyConfig strategyConfig,
        final StrategyUpdater strategyUpdater) throws ConditionNotFoundException {

        final ProtectiveManager protectiveManager = ProtectiveManagerBuilder.buildRiskControlManager(riskManager, strategyConfig, strategyUpdater);
        idToRiskControl.put(strategyUpdater.getStrategyId(), protectiveManager);
        riskManager.addProtectiveManager(strategyUpdater.getStrategyId(), protectiveManager);
    }

    private void buildPriceControl(
        final int strategyId,
        final StrategyConfig strategyConfig,
        final QuoteSnapshotCache quoteSnapshot,
        final Map<Contract, PriceManager> contractToPriceManager) throws ConditionNotFoundException {

        for (final Entry<Contract, StrategyQuoteSnapshot> entry : quoteSnapshot.getContractToQuoteSnapshot().entrySet()) {
            final PriceManager priceManager = PriceManagerBuilder.buildPriceManager(riskManager, entry.getValue(), strategyConfig);
            contractToPriceManager.put(entry.getKey(), priceManager);
        }
        riskManager.addPriceManager(strategyId, contractToPriceManager);
    }

    private void buildPositionControl(
        final int strategyId,
        final StrategyConfig strategyConfig,
        final Map<String, Instrument> instruments,
        final Map<Contract, PositionManager> contractToPositionManager) throws ConditionNotFoundException {

        for (final Contract contract : instruments.values()) {
            final PositionManager positionManager = PositionManagerBuilder.buildPositionManager(contract, strategyConfig);
            contractToPositionManager.put(contract, positionManager);
        }
        riskManager.addPositionManager(strategyId, contractToPositionManager);
    }

    private void buildQuoteView(final QuoteSnapshotCache quoteSnapshotCache, final Map<Instrument, List<FeedType>> contractToFeeds) {
        for (final Entry<Instrument, List<FeedType>> feedEntry : contractToFeeds.entrySet()) {
            if (feedEntry.getValue().contains(FeedType.Snapshot) || feedEntry.getValue().contains(FeedType.Trade) || feedEntry.getValue().contains(FeedType.MarketBook)) {
                final StrategyQuoteSnapshot quoteSnapshot = new StrategyQuoteSnapshot(feedEntry.getKey());
                quoteSnapshotCache.addQuoteSnapshot(feedEntry.getKey(), quoteSnapshot);
            } else {
                log.warn("{} does not subscribe to QuoteView {}", feedEntry.getKey(), feedEntry.getValue());
            }
        }
    }

    private void buildOrderView(final OrderViewCache orderViewCache, final Map<Instrument, TradeAppendix> contractToTrades, final int strategyId) {
        for (final Entry<Instrument, TradeAppendix> entry : contractToTrades.entrySet()) {
            final OrderViewContainer orderView = market.getOrderView(
                strategyId,
                getThreadContext(strategyId),
                entry.getKey(),
                entry.getValue());
            orderViewCache.addOrderView(entry.getKey(), orderView);
        }
    }

    private StrategyBuilder getStrategyBuilder(final ClassLoader classLoader, final String builderClassPath) throws StrategyException {
        StrategyBuilder builder = null;
        try {
            builder = (StrategyBuilder) Class.forName(builderClassPath, true, classLoader).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
            | SecurityException e) {
            throw new StrategyException("Failed to create StrategyBuilder instance", e);
        }
        return builder;
    }

}
