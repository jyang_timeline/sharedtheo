package com.nogle.core.strategy;

import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.core.util.MarketDataObjectFactory;

public class StrategyQuoteSnapshot implements QuoteSnapshot {

    private final Contract contract;
    private BookView marketBook;
    private TickView tick;

    public StrategyQuoteSnapshot(final Contract contract) {
        this.contract = contract;
        // Dummy object to avoid NPE
        marketBook = MarketDataObjectFactory.getMarketBook();
        tick = MarketDataObjectFactory.getTick();
    }

    public void setMarketBook(final BookView marketBook) {
        this.marketBook = marketBook;
    }

    public void setTick(final TickView tick) {
        this.tick = tick;
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public long getUpdateTimeMillis() {
        return marketBook.getUpdateTimeMillis();
    }

    @Override
    public BookView getBookView() {
        return marketBook;
    }

    @Override
    public long getUpdateTimeMicros() {
        return getUpdateTimeMillis() * 1000l;
    }

    @Override
    public int getMarketBookBidDepth() {
        return marketBook.getBidDepth();
    }

    @Override
    public int getMarketBookAskDepth() {
        return marketBook.getAskDepth();
    }

    @Override
    public QuoteType getQuoteType() {
        return marketBook.getQuoteType();
    }

    @Override
    public long getBidQty(final int index) {
        return marketBook.getBidQty(index);
    }

    @Override
    public long getBidQty() {
        return marketBook.getBidQty();
    }

    @Override
    public double getBidPrice(final int index) {
        return marketBook.getBidPrice(index);
    }

    @Override
    public double getBidPrice() {
        return marketBook.getBidPrice();
    }

    @Override
    public long getAskQty(final int index) {
        return marketBook.getAskQty(index);
    }

    @Override
    public long getAskQty() {
        return marketBook.getAskQty();
    }

    @Override
    public double getAskPrice(final int index) {
        return marketBook.getAskPrice(index);
    }

    @Override
    public double getAskPrice() {
        return marketBook.getAskPrice();
    }

    @Override
    public TickType getLastType() {
        return tick.getType();
    }

    @Override
    public long getLastQty() {
        return tick.getQuantity();
    }

    @Override
    public double getLastPrice() {
        return tick.getPrice();
    }

}
