package com.nogle.core.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.EventTask;
import com.nogle.core.control.commands.ConfigCommand;
import com.nogle.core.control.commands.DisableCommand;
import com.nogle.core.control.commands.EnableCommand;
import com.nogle.core.control.commands.FillCommand;
import com.nogle.core.control.commands.QueryCommand;
import com.nogle.core.control.commands.ResetAlarmCommand;
import com.nogle.core.control.commands.ResetOrderViewCommand;
import com.nogle.core.control.commands.SendCancelCommand;
import com.nogle.core.control.commands.SendOrderCommand;
import com.nogle.core.controller.CommandProcessor;
import com.nogle.core.event.QuoteSourceFilter;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.strategy.logging.StrategyTradeLogger;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.types.CommandPrivilege;
import com.nogle.core.util.LifeCycleIdGenerator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.api.Command;
import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.api.StrategyBuilder;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Miss;
import com.nogle.strategy.types.OrderView;
import com.nogle.strategy.types.PositionView;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.TimeCondition;
import com.nogle.strategy.types.TradeLogger;
import com.nogle.strategy.types.TradeServices;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.DummyTrade;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.EventListener;
import com.timelinecapital.core.event.handler.IdleStrategyEventHandler;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.sharedtheo.SharedTheoLoader;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.util.SymbolPatternUtil;
import com.timelinecapital.core.util.TradingHoursHelper;
import com.timelinecapital.strategy.PnLStatement;
import com.timelinecapital.strategy.api.SharedTheo;
import com.timelinecapital.strategy.state.ExchangeSession;
import com.timelinecapital.strategy.state.StrategyState;
import com.timelinecapital.strategy.types.MarketData;
import com.timelinecapital.strategy.types.OrderQueueView;
import com.timelinecapital.view.TradeStatistics;

public class StrategyUpdater {
    private final Logger log = LogManager.getLogger(StrategyUpdater.class);

    private final Map<String, Instrument> contracts;
    private final Map<Instrument, List<FeedType>> contractToFeeds;
    private final Map<FeedType, EventListener<? extends MarketData>> feedToEventListener = new HashMap<>();
    private final Map<String, ExchangeSession> exchangeToSession = new HashMap<>();
    private final List<Filter<Quote>> filters = new ArrayList<>();

    private final SharedTheoLoader sharedTheoLoader;
    private volatile boolean hasAlreadyBuilt;

    private final int strategyId;
    private final String strategyName;
    private final StrategyConfig config;
    private final StrategyScheduleService timeChecker;

    private final StrategyBuilder strategyBuilder;
    private final StrategyTradeLogger strategyTradeLogger;
    private final StrategyView view;
    private final TradeStatistics statistics;

    private final QuoteSnapshotCache quoteSnapshotCache;
    private final OrderViewCache orderViewCache;

    private final LifeCycleIdGenerator lifeCycleIdGenerator;
    private final TradeServices tradeServices;
    private final CommandProcessor commandProcessor;

    private volatile Strategy currentStrategy = IdleStrategy.getInstance();
    private Strategy serviceStrategy;

    private boolean isSimulation = false;

    public StrategyUpdater(
        final int strategyId,
        final StrategyConfig config,
        final StrategyScheduleService timeChecker,
        final StrategyBuilder strategyBuilder,
        final StrategyTradeLogger strategyTradeLogger,
        final StrategyView view,
        final TradeStatistics statistics,
        final QuoteSnapshotCache quoteSnapshotCache,
        final OrderViewCache orderViewCache,
        final Map<String, Instrument> contracts,
        final Map<Instrument, List<FeedType>> contractToFeeds,
        final SharedTheoLoader sharedTheoLoader) {

        this.hasAlreadyBuilt = false;
        this.strategyId = strategyId;
        this.strategyName = config.getName();

        this.config = config;
        this.timeChecker = timeChecker;

        this.strategyBuilder = strategyBuilder;
        this.strategyTradeLogger = strategyTradeLogger;
        this.view = view;
        this.statistics = statistics;

        this.quoteSnapshotCache = quoteSnapshotCache;
        this.orderViewCache = orderViewCache;
        this.contracts = contracts;
        this.contractToFeeds = contractToFeeds;
        this.sharedTheoLoader = sharedTheoLoader;

        lifeCycleIdGenerator = new LifeCycleIdGenerator();
        tradeServices = new StrategyTradeServices();

        // TODO: Code still required further study
        for (final Entry<String, Integer> entry : EngineConfig.getExchangeToFeedFilter().entrySet()) {
            final Exchange exchange = Exchange.valueOf(entry.getKey());
            final ContentType contentType = ContentType.lookup(entry.getValue());
            filters.add(QuoteSourceFilter.createFilter(contentType, exchange, Filter.Result.DENY, Filter.Result.ACCEPT));
        }

        feedToEventListener.put(FeedType.OrderQueue, new OrderQueueListener());
        feedToEventListener.put(FeedType.OrderActions, new OrderActionsListener());
        feedToEventListener.put(FeedType.BestPriceOrderDetail, new BestPriceOrderQueueListener());
        feedToEventListener.put(FeedType.Trade, new TickListener());
        feedToEventListener.put(FeedType.MarketBook, new MarketBookListener());

        commandProcessor = new CommandProcessor();
        commandProcessor.addCommand(new EnableCommand(this, view), CommandPrivilege.Model);
        commandProcessor.addCommand(new DisableCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new QueryCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new ConfigCommand(config), CommandPrivilege.Model);
        commandProcessor.addCommand(new FillCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new SendOrderCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new SendCancelCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new ResetOrderViewCommand(this), CommandPrivilege.Model);
        commandProcessor.addCommand(new ResetAlarmCommand(view), CommandPrivilege.Model);
    }

    public void importFill(final String contractName, final Side side, final TimeCondition timeCondition, final double price, final long qty) {
        final PositionManager positionManager = orderViewCache.getPositionManagerBySymbolName(contractName);
        if (!positionManager.isValidFill(side, price, qty)) {
            log.error("Invalid Fill message: {} {} Px:{} Qty:{}", contractName, side, price, qty);
            return;
        }
        orderViewCache.getOrderHolderBySymbolName(timeCondition, side, contractName).receiveManualFill(0, qty, 0, price, 0);
    }

    public void sendManualOrder(final String contractName, final Side side, final TimeCondition timeCondition, final double price, final long qty) {
        if (SymbolPatternUtil.CODENAME_STOCK.matcher(contractName).matches()) {
            log.info("Set TimeCondition to {} due to Equity {}", TimeCondition.GFD, contractName);
            orderViewCache.getOrderHolderBySymbolName(TimeCondition.GFD, side, contractName).sendManualOrder(price, qty);
        } else {
            orderViewCache.getOrderHolderBySymbolName(timeCondition, side, contractName).sendManualOrder(price, qty);
        }
    }

    public void cancelOrder(final long clOrderId) {
        log.warn("{}\t cancel order from command {}", clOrderId);
        final List<TradeConnection> agent = CoreController.getInstance().getMarket().getTradeConnections();
        agent.get(0).forceCancel(clOrderId);
    }

    public void resetOrderView() {
        for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
            log.warn("Reset all Orderbook of contract {}", orderViewContainer.getContract());
            for (final OrderHolder orderHolder : orderViewContainer.getOrderHolders()) {
                orderHolder.reset();
            }
        }
    }

    public void withEventTask(final EventTask eventTask) {
        commandProcessor.setEventTask(eventTask);
    }

    public int getStrategyId() {
        return strategyId;
    }

    public String getStrategyName() {
        return strategyName;
    }

    QuoteSnapshotCache getQuoteSnapshotCache() {
        return quoteSnapshotCache;
    }

    OrderViewCache getOrderViewCache() {
        return orderViewCache;
    }

    public DummyTrade getDummyTrade(final Contract contract) {
        return orderViewCache.getContractToOrderView().get(contract);
    }

    public void resetDummyTrade() {
        for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
            orderViewContainer.rewind();
        }
    }

    protected StrategyScheduleService getTimeChecker() {
        return timeChecker;
    }

    protected StrategyConfig getConfig() {
        return config;
    }

    public StrategyView getStrategyStatusView() {
        return view;
    }

    protected void onConfigChanged(final Config config) {
        // orderViewCache.updatePositionManager(config);
        // orderViewCache.updateOrderExecutor(config);
        currentStrategy.onConfigChange(config);
    }

    public <T extends TickView & Quote> void onTickFilter(final T tick) {
        for (final Filter<Quote> filter : filters) {
            if (Filter.Result.DENY == filter.filter(tick)) {
                return;
            }
        }
        onTick(tick);
    }

    public void onTick(final TickView tick) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        // TODO: should be a better way of removing this map lookup
        quoteSnapshotCache.getQuoteSnapshot(tick.getContract()).setTick(tick);

        // final long micros = TradeClock.getCurrentMicros();
        // log.debug("Tick {} {} {} - {} | {} {}/{} {} {} {}", updateId,
        // tick.getSequenceNo(), tick.getContract(), micros - tick.getRecvTimeMicros(),
        // tick.getType(), tick.getQuantity(), tick.getPrice(), tick.getTradeId(), tick.getBuyerOrderId(),
        // tick.getSellerOrderId());

        view.updateLatestEventTime(tick.getUpdateTimeMicros());
        currentStrategy.onTick(updateId, tick);
        view.updateLatestQuoteTime(tick.getRecvTimeMicros());
    }

    public <M extends BookView & Quote> void onBookFilter(final M book) {
        for (final Filter<Quote> filter : filters) {
            if (Filter.Result.DENY == filter.filter(book)) {
                return;
            }
        }
        onMarketBook(book);
    }

    public void onMarketBook(final BookView marketBook) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        // TODO: should be a better way of removing this map lookup
        quoteSnapshotCache.getQuoteSnapshot(marketBook.getContract()).setMarketBook(marketBook);

        // final long micros = TradeClock.getCurrentMicros();
        // log.debug("Book {} {} {} - {} | {}# {}@{} | {}# {}@{}", updateId,
        // marketBook.getSequenceNo(), marketBook.getContract(), micros - marketBook.getRecvTimeMicros(),
        // marketBook.getBidDepth(), marketBook.getBidQty(), marketBook.getBidPrice(),
        // marketBook.getAskDepth(), marketBook.getAskQty(), marketBook.getAskPrice());

        view.updateLatestEventTime(marketBook.getUpdateTimeMicros());
        currentStrategy.onMarketBook(updateId, marketBook);
        view.updateLatestQuoteTime(marketBook.getRecvTimeMicros());
    }

    public void onBestPriceOrderDetail(final BestOrderView event) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onBestOrderDetail(updateId, event);

        // final long micros = TradeClock.getCurrentMicros();
        // log.debug("Best {} {} {} {} | {} {} {} {}", updateId,
        // event.getSequenceNo(), event.getContract(), micros - event.getRecvTimeMicros(),
        // event.getBestBidPrice(), event.getBestAskPrice(),
        // event.getBestBidOrderQty(0), event.getBestAskOrderQty(0));
    }

    public void onOrderActions(final QuoteOrderView event) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onQuoteOrderDetail(updateId, event);

        // final long micros = TradeClock.getCurrentMicros();
        // log.debug("OAct {} {} {} {} | {} {} {} | {} {} - {} {}", updateId,
        // event.getSequenceNo(), event.getContract(), micros - event.getRecvTimeMicros(),
        // event.geTimeCondition(), event.getQuoteOrderId(), event.getSide(), event.getOrderType(), event.getExecRequestType(),
        // event.getPrice(), event.getQuantity());
    }

    public void onOrderQueues(final OrderQueueView event) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onOrderQueue(updateId, event);

        // final long micros = TradeClock.getCurrentMicros();
        // log.debug("OQue {} | {} {} {} | {} {} - {} {}", updateId,
        // event.getSequenceNo(), event.getContract(), micros - event.getRecvTimeMicros(),
        // event.getSide(), event.getPrice(), event.getQueueSize(), event.getOrderQueueQty(0));
    }

    public void onFill(final Fill fill) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onFill(updateId, fill);
    }

    public void onMiss(final Miss miss) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onMiss(updateId, miss);
    }

    public void onPriceLimitUpdate(final Contract contract) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onPriceLimitUpdate(updateId, contract);
    }

    public void onPositionSync(final Contract contract) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onPositionInfoSync(updateId, orderViewCache.getPositionView(contract));
    }

    public void onExchangeSessionChange(final Exchange exchange, final ExchangeSession session) {
        final long updateId = lifeCycleIdGenerator.getNextId();
        currentStrategy.onSessionUpdate(updateId, exchange.name(), session);
    }

    public void onTradingEnable() {
        orderViewCache.updateOrderPermission(true);
        log.warn("{}\t can trade: {}", strategyName, contracts.values());
    }

    private void onTradingDisable() {
        orderViewCache.updateOrderPermission(false);
        log.warn("{}\t can NOT trade: {}", strategyName, contracts.values());
    }

    /*
     * Order entry blocked & All working orders cancelled
     */
    public void onTradingShutdown() {
        for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
            for (final OrderHolder orderHolder : orderViewContainer.getOrderHolders()) {
                if (orderHolder.getOutstandingOrderCount() > 0) {
                    log.warn("REMOVE outstanding orders [{}] [{}]", orderHolder.getContract(), orderHolder.getSide());
                    orderHolder.clearAllOrders();
                    orderHolder.commit(0L);
                }
            }
        }
        onTradingDisable();
    }

    /*
     * Order entry blocked only if there is no position
     */
    public void onTradingExit() {
        if (getPosition() == 0 && getWorkingOrders() == 0) {
            onTradingDisable();
        } else {
            log.warn("ENTER mode EXIT and SET alarm: [{}]", strategyName, contracts.values());
            // view.setAlarm(true);
            for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
                orderViewContainer.onExitMode();
            }
        }
    }

    private void doGetExchangeToSession() {
        contracts.values().forEach(instrument -> {
            final ExchangeSession session = TradingHoursHelper.getExchangeSession(instrument.getExchange(), TradeClock.getCurrentMillis());
            exchangeToSession.put(instrument.getExchange(), session);
        });
    }

    private int getWorkingOrders() {
        int count = 0;
        for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
            for (final OrderHolder orderHolder : orderViewContainer.getOrderHolders()) {
                count += orderHolder.getOutstandingOrderCount();
            }
        }
        return count;
    }

    private long getPosition() {
        long position = 0;
        for (final PositionView positionView : orderViewCache.getPositionViews()) {
            position += positionView.getPosition();
        }
        return position;
    }

    protected void onPreparing() {
        serviceStrategy = strategyBuilder.build(tradeServices);
        serviceStrategy.onCommandRollCall(tradeServices);
        hasAlreadyBuilt = true;
    }

    protected void onSimulationMode() {
        isSimulation = true;
    }

    protected void onStart() {
        if (view.getLifeCycle().equals(StrategyLifeCycle.Idle) || view.getLifeCycle().equals(StrategyLifeCycle.Shutdown)) {
            for (final OrderViewContainer orderViewContainer : orderViewCache.getOrderViewContainers()) {
                if (!orderViewContainer.isReady()) {
                    log.warn("OrderView does not rewind properly... Running rewind work flow again!!!");
                    orderViewContainer.rewind();
                }
            }
            if (!hasAlreadyBuilt) {
                log.warn("[Strategy-{}] Building strategy instance", strategyId);
                onPreparing();
            }

            doGetExchangeToSession();

            lifeCycleIdGenerator.reset();
            view.onStrategyStart();

            try {
                currentStrategy = serviceStrategy;
                currentStrategy.onStart(lifeCycleIdGenerator.getNextId());
            } catch (final Exception e) {
                log.error("[Strategy-{}] onStart with exceptions, releasing resources...", strategyId);
                log.error(e.getMessage(), e);
                CoreController.getStrategyLoader().shutdownStrategy(this);
            }
        }
    }

    protected void resume() {
        if (view.getLifeCycle().equals(StrategyLifeCycle.Stop)) {
            view.onStrategyResume();
            currentStrategy.resume(lifeCycleIdGenerator.getNextId());
        }
    }

    protected void onStop() {
        if (!view.getLifeCycle().equals(StrategyLifeCycle.Stop)) {
            view.onStrategyStop();
            currentStrategy.onStop(lifeCycleIdGenerator.getNextId());
        }
    }

    protected void onShutdown() {
        if (!view.getLifeCycle().equals(StrategyLifeCycle.Shutdown)) {
            onTradingDisable();
            try {
                currentStrategy.onShutdown(lifeCycleIdGenerator.getNextId());
            } catch (final Exception e) {
                log.error("Failed to execute shutdown of Strategy... {}", e.getMessage());
            }
            currentStrategy = IdleStrategy.getInstance();
            view.toFinalReport();
            view.toFinalSnapshot();
            view.onStrategyShutdown();
        }
    }

    public Collection<Instrument> getContracts() {
        return contracts.values();
    }

    public Map<Instrument, List<FeedType>> getContractToFeeds() {
        return contractToFeeds;
    }

    @SuppressWarnings("unchecked")
    public <T extends MarketData> EventListener<T> getEventListener(final FeedType feedType, final Class<T> type) {
        return (EventListener<T>) feedToEventListener.getOrDefault(feedType, IdleStrategyEventHandler.getInstance());
    }

    public CommandProcessor getCommandProcessor() {
        return commandProcessor;
    }

    public Collection<Command> getCommands() {
        return commandProcessor.getCommands();
    }

    public TradeServices getTradeServices() {
        return tradeServices;
    }

    private final class MarketBookListener implements EventListener<BookView> {

        @Override
        public final void onEvent(final BookView event) {
            StrategyUpdater.this.onMarketBook(event);
        }

    }

    private final class TickListener implements EventListener<TickView> {

        @Override
        public final void onEvent(final TickView event) {
            StrategyUpdater.this.onTick(event);
        }

    }

    private final class BestPriceOrderQueueListener implements EventListener<BestOrderView> {

        @Override
        public final void onEvent(final BestOrderView event) {
            StrategyUpdater.this.onBestPriceOrderDetail(event);
        }

    }

    private final class OrderActionsListener implements EventListener<QuoteOrderView> {

        @Override
        public final void onEvent(final QuoteOrderView event) {
            StrategyUpdater.this.onOrderActions(event);
        }

    }

    private final class OrderQueueListener implements EventListener<OrderQueueView> {

        @Override
        public final void onEvent(final OrderQueueView event) {
            StrategyUpdater.this.onOrderQueues(event);
        }

    }

    private final class StrategyTradeServices implements TradeServices {

        private final PnLStatement statement = StrategyUpdater.this.view;

        @Override
        public Contract getContract(final String key) {
            return contracts.get(key);
        }

        @Override
        public Config getConfig() {
            return config;
        }

        @Override
        public void addCommand(final Command command) {
            commandProcessor.addCommand(command, CommandPrivilege.Self);
        }

        @Override
        public void addMessage(final String message) {
            view.updateStrategyMessage(System.currentTimeMillis(), message);
        }

        @Override
        public void setStrategyState(final StrategyState state) {
            view.onStrategyStateChange(state);
        }

        @Override
        public SharedTheo getSharedTheo(final String theoName) {
            return sharedTheoLoader.get(theoName);
        }

        @Override
        public QuoteSnapshot getQuoteSnapshot(final Contract contract) {
            return quoteSnapshotCache.getQuoteSnapshot(contract);
        }

        @Override
        public OrderView getOrderView(final Contract contract, final Side side, final TimeCondition timeCondition) {
            return orderViewCache.getOrderHolder(timeCondition, side, contract);
        }

        @Override
        public PositionView getPositionView(final Contract contract) {
            return orderViewCache.getPositionView(contract);
        }

        @Override
        public TradeStatistics getTradeStatistics() {
            return statistics;
        }

        @Override
        public ExchangeSession getExchangeSession(final Contract contract) {
            return exchangeToSession.getOrDefault(contract.getExchange(), ExchangeSession.CORE_TRADING);
        }

        @Override
        public double getClosedPnl() {
            return statement.getClosedPnl();
        }

        @Override
        public double getOpenPnl() {
            return statement.getOpenPnl();
        }

        @Override
        public double getTotalPnl() {
            return statement.getTotalPnl();
        }

        @Override
        public TradeLogger getLogger() {
            return strategyTradeLogger;
        }

        @Override
        public boolean isSimulation() {
            return isSimulation;
        }

        @Override
        public long getLastQuoteTimeInMicros(final Contract contract) {
            return quoteSnapshotCache.getQuoteSnapshot(contract).getUpdateTimeMillis();
        }

        @Override
        public long getSystemTimeInMicros() {
            return TradeClock.getCurrentMicros();
        }

        @Override
        public String getTradingDay() {
            return TradeClock.getTradingDay();
        }

    }
}
