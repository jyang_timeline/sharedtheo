package com.nogle.core.strategy;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.commons.utils.StrategyHeartbeat;
import com.nogle.commons.utils.TradeReport;
import com.nogle.core.Constants;
import com.nogle.core.util.PnlFormatter;
import com.nogle.core.util.PriceUtils;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.commons.json.HidableModule;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.strategy.PnLStatement;
import com.timelinecapital.strategy.state.StrategyState;

public class StrategyView implements PnLStatement {
    private static final Logger log = LogManager.getLogger(StrategyView.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private final Map<Contract, TradeReport> contractToReport = new HashMap<>();

    private Map<Contract, StrategyQuoteSnapshot> contractToQuoteSnapshot;
    private Map<Contract, OrderViewContainer> contractToOrderViewContainer;
    private Collection<OrderViewContainer> orderViewContainers;

    private final int strategyId;
    private final String strategyName;

    private final StrategyHeartbeat strategyHeartbeat;
    private final TradeReport strategyReport;
    private String settlement = StringUtils.EMPTY;

    private StrategyLifeCycle lifeCycle = StrategyLifeCycle.Idle;
    private StrategyState internalState = StrategyState.NORMAL;

    private long lastQuoteArrivalTime;
    private long latestEventTime;

    private final double[] pnls = new double[3];
    private final int[] trades = new int[7];

    private final CircularFifoQueue<ModelMessage> modelMessages = new CircularFifoQueue<>(20);
    private final AlarmMessage alarmInst = new AlarmMessage();

    private String alarmMessage = StringUtils.EMPTY;
    private volatile boolean alarm = false;
    private volatile boolean exit = false;

    public StrategyView(final int strategyId, final String strategyName,
        final Map<Contract, StrategyQuoteSnapshot> contractToQuoteSnapshot, final Map<Contract, OrderViewContainer> contractToOrderViewContainer) {
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        mapper.registerModule(new HidableModule());

        this.strategyId = strategyId;
        this.strategyName = strategyName;

        this.contractToQuoteSnapshot = contractToQuoteSnapshot;
        this.contractToOrderViewContainer = contractToOrderViewContainer;
        orderViewContainers = contractToOrderViewContainer.values();

        strategyHeartbeat = new StrategyHeartbeat(EngineConfig.getEngineId(), strategyId, strategyName);
        strategyReport = new TradeReport();
    }

    public void onStrategyStart() {
        lifeCycle = StrategyLifeCycle.Start;
        // TODO should avoid using lazy initialization
        orderViewContainers.forEach(view -> {
            contractToReport.put(view.getContract(), new TradeReport());
        });
    }

    public void onStrategyResume() {
        lifeCycle = StrategyLifeCycle.Start;
    }

    public void onStrategyStop() {
        lifeCycle = StrategyLifeCycle.Stop;
    }

    public void onStrategyShutdown() {
        lifeCycle = StrategyLifeCycle.Shutdown;
    }

    public void onStrategyException() {
        lifeCycle = StrategyLifeCycle.Failed;
    }

    public StrategyLifeCycle getLifeCycle() {
        return lifeCycle;
    }

    public void onStrategyStateChange(final StrategyState internalState) {
        this.internalState = internalState;
    }

    public StrategyState getStrategyState() {
        return internalState;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void armWith(final String alarmMessage) {
        this.armWith(AlarmCodes.GENERAL, AlarmInst.Strategy, alarmMessage);
    }

    public void armWith(final AlarmCodes codes, final AlarmInst inst, final String content) {
        alarm = true;
        alarmMessage = alarmInst.withContext(codes, inst, content);
    }

    public void disarm() {
        alarm = false;
        alarmMessage = StringUtils.EMPTY;
        alarmInst.clear();
    }

    public void setExit(final boolean exit) {
        this.exit = exit;
    }

    public void updateStrategyMessage(final long currentTime, final String message) {
        modelMessages.add(new ModelMessage(currentTime, message));
    }

    public void updateLatestQuoteTime(final long lastQuoteArrivalTime) {
        this.lastQuoteArrivalTime = lastQuoteArrivalTime;
    }

    public void updateLatestEventTime(final long latestEventTime) {
        this.latestEventTime = latestEventTime;
    }

    public long getLatestEventTime() {
        return latestEventTime;
    }

    public boolean canStrategySendOrder() {
        boolean result = false;
        for (final OrderViewContainer orderViewContainer : orderViewContainers) {
            result |= orderViewContainer.getOrderEntryPermission();
        }
        return result;
    }

    public boolean hasPositions(final Contract contract) {
        // To fix NPE when strategy is not started
        if (!contractToReport.containsKey(contract)) {
            return false;
        }
        return contractToReport.get(contract).getPosition() != 0;
    }

    public Set<Contract> getContracts() {
        return contractToQuoteSnapshot.keySet();
    }

    public String getFinalReport() {
        return settlement;
    }

    @Override
    public final double getRealizedPnl() {
        double pnl = 0.0d;
        for (final OrderViewContainer orderViewContainer : orderViewContainers) {
            pnl += orderViewContainer.getClosedPnl() - orderViewContainer.getCommission();
        }
        return pnl;
    }

    @Override
    public double getTotalPnl() {
        return strategyReport.getPnl() - strategyReport.getCommission();
    }

    @Override
    public final double getPnl() {
        return strategyReport.getPnl();
    }

    @Override
    public final double getOpenPnl() {
        return strategyReport.getOpenPnl();
    }

    @Override
    public final double getClosedPnl() {
        return strategyReport.getClosedPnl();
    }

    @Override
    public final double getCommission() {
        return strategyReport.getCommission();
    }

    private void refreshStrategyReport() {
        // isFlat = true;
        strategyReport.reset();
        for (final OrderViewContainer orderViewContainer : orderViewContainers) {
            final Contract contract = orderViewContainer.getContract();
            // To fix NPE when strategy is not started
            if (!contractToReport.containsKey(contract)) {
                return;
            }
            final TradeReport contractReport = contractToReport.get(contract);
            contractReport.setHidden(!orderViewContainer.isTrading());

            updatePNLsByContract(orderViewContainer);
            contractReport.setOpenPnl(pnls[0]);
            contractReport.setClosedPnl(pnls[1]);
            contractReport.setPnl(pnls[2]);
            contractReport.setCommission(orderViewContainer.getCommission());
            contractReport.setPosition(orderViewContainer.getPosition());
            contractReport.setVolume(orderViewContainer.getVolume());

            // isFlat &= !(orderViewContainer.getPosition() != 0);

            updateTradesByContract(orderViewContainer);
            contractReport.setBidOrders(trades[0]);
            contractReport.setAskOrders(trades[1]);
            contractReport.setOrders(trades[2]);
            contractReport.setModifications(trades[3]);
            contractReport.setCancels(trades[4]);
            contractReport.setFills(trades[5]);
            contractReport.setRejects(trades[6]);

            strategyReport.summarize(contractReport);
        }
    }

    private void updatePNLsByContract(final OrderViewContainer orderViewContainer) {
        final QuoteSnapshot quoteSnapshot = contractToQuoteSnapshot.get(orderViewContainer.getContract());
        final double lastMid = PriceUtils.getMidPoint(quoteSnapshot.getQuoteType(), quoteSnapshot.getBidPrice(), quoteSnapshot.getAskPrice());
        pnls[0] = orderViewContainer.getOpenPnl(lastMid);
        pnls[1] = orderViewContainer.getClosedPnl();
        pnls[2] = pnls[0] + pnls[1];
    }

    private void updateTradesByContract(final OrderViewContainer orderViewContainer) {
        trades[0] = trades[1] = trades[2] = trades[3] = trades[4] = trades[5] = trades[6] = 0;
        for (final OrderHolder orderHolder : orderViewContainer.getOrderHolders()) {
            switch (orderHolder.getSide()) {
                case BUY:
                    trades[0] += orderHolder.getOutstandingOrderCount();
                    break;
                case SELL:
                    trades[1] += orderHolder.getOutstandingOrderCount();
                    break;
                default:
                    break;
            }
            trades[2] += orderHolder.getOrderSubmissions();
            trades[3] += orderHolder.getOrderModifications();
            trades[4] += orderHolder.getOrderCancellations();
            trades[5] += orderHolder.getOrderFills();
            trades[6] += orderHolder.getRequestRejects();
        }
    }

    public void onExitCheck() {
        if (exit) {
            for (final OrderViewContainer orderViewContainer : orderViewContainers) {
                orderViewContainer.checkExit();
            }
            exit = canStrategySendOrder();
        }
    }

    public StrategyHeartbeat onStatusRefresh() {
        refreshStrategyReport();

        strategyHeartbeat.setStatus(lifeCycle);
        strategyHeartbeat.setState(internalState.getStateCode());
        strategyHeartbeat.setEnable(canStrategySendOrder());
        strategyHeartbeat.setIdleTime((TradeClock.getCurrentMillis() - lastQuoteArrivalTime) / 1000);
        strategyHeartbeat.setVolume(strategyReport.getVolume());
        strategyHeartbeat.setPosition(strategyReport.getPosition());
        strategyHeartbeat.setBidOrders(strategyReport.getBidOrders());
        strategyHeartbeat.setAskOrders(strategyReport.getAskOrders());
        strategyHeartbeat.setOpenPnl(strategyReport.getOpenPnl());
        strategyHeartbeat.setClosedPnl(strategyReport.getClosedPnl());
        strategyHeartbeat.setCommission(strategyReport.getCommission());
        strategyHeartbeat.setOrders(strategyReport.getOrders());
        strategyHeartbeat.setModifications(strategyReport.getModifications());
        strategyHeartbeat.setCancels(strategyReport.getCancels());
        strategyHeartbeat.setFills(strategyReport.getFills());
        strategyHeartbeat.setRejects(strategyReport.getRejects());

        strategyHeartbeat.setAlarm(alarm);
        strategyHeartbeat.setAlarmMessage(alarmMessage);
        strategyHeartbeat.setModelMessage(modelMessages.toString());
        try {
            strategyHeartbeat.setDetail(mapper.writeValueAsString(contractToReport));
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return strategyHeartbeat;
    }

    void toFinalReport() {
        onStatusRefresh();

        final Map<Contract, ModelReport> contractToReport = new HashMap<>();
        for (final OrderViewContainer orderViewContainer : orderViewContainers) {
            if (orderViewContainer.getVolume() > 0 || orderViewContainer.getCancels() > 0) {
                contractToReport.put(orderViewContainer.getContract(), new ModelReport(orderViewContainer));
            }
        }
        settlement = contractToReport.toString();
        log.info("{}-{} PNL: {}/{}", strategyId, strategyName, PnlFormatter.format(getClosedPnl()), PnlFormatter.format(getTotalPnl()));
        log.info("{}-{} Report: {}", strategyId, strategyName, settlement);
    }

    void toFinalSnapshot() {
        final Map<Contract, StrategyQuoteSnapshot> immutableSnapshot = Collections.unmodifiableMap(new HashMap<>(contractToQuoteSnapshot));
        contractToQuoteSnapshot = immutableSnapshot;
        final Map<Contract, OrderViewContainer> immutableOrderViews = Collections.unmodifiableMap(new HashMap<>(contractToOrderViewContainer));
        contractToOrderViewContainer = immutableOrderViews;
        orderViewContainers = immutableOrderViews.values();
    }

    private class ModelReport {
        private final double closedPNL;
        private final double fees;
        private final double totalPNL;
        private final long volume;
        private long cancelCount;
        private final long position;

        ModelReport(final OrderViewContainer orderViewContainer) {
            closedPNL = orderViewContainer.getClosedPnl();
            fees = orderViewContainer.getCommission();
            totalPNL = closedPNL - fees;
            volume = orderViewContainer.getVolume();
            for (final OrderHolder orderView : orderViewContainer.getOrderHolders()) {
                cancelCount += orderView.getOrderCancellations();
            }
            position = orderViewContainer.getPosition();
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ClosedPNL: ").append(PnlFormatter.format(closedPNL)).append(" / ")
                .append("Fees: ").append(PnlFormatter.format(fees)).append(" / ")
                .append("TotalPNL: ").append(PnlFormatter.format(totalPNL)).append(" / ")
                .append("Volume: ").append(volume).append(" / ")
                .append("CancelCount: ").append(cancelCount).append(" / ")
                .append("Position: ").append(position);
            return sb.toString();
        }

    }

    private class ModelMessage {
        private final StringBuilder modelMessageBuilder = new StringBuilder();
        private final String timestamp;
        private final String message;

        public ModelMessage(final long timestamp, final String message) {
            this.timestamp = new DateTime(timestamp).toString(NogleTimeFormatter.ISO_DATE_TIME_MILLIS_FORMAT);
            this.message = message;
        }

        @Override
        public String toString() {
            modelMessageBuilder.setLength(0);
            modelMessageBuilder.append(Constants.NEW_LINE);
            modelMessageBuilder.append("Time: ").append(timestamp);
            modelMessageBuilder.append(Constants.TAB);
            modelMessageBuilder.append("Message: ").append(message);
            return modelMessageBuilder.toString();
        }
    }

    private class AlarmMessage {

        private final StringBuilder sb = new StringBuilder();
        private String message = StringUtils.EMPTY;
        private int hashcode = 0;
        private int sequence = 0;

        public AlarmMessage() {
        }

        void clear() {
            message = StringUtils.EMPTY;
            hashcode = 0;
        }

        String withContext(final AlarmCodes codes, final AlarmInst inst, final String key) {
            if (isSameWarningMessage(codes, inst, key)) {
                return message;
            }
            hashcode = getHashCode(codes, inst, key);
            sequence++;
            sb.setLength(0);
            sb.append(sequence).append(Constants.COMMA)
                .append(codes.getFieldDescription()).append(Constants.COMMA)
                .append(inst.name()).append(Constants.COMMA)
                .append(key);
            message = sb.toString();
            return message;
        }

        private boolean isSameWarningMessage(final AlarmCodes codes, final AlarmInst inst, final String key) {
            return hashcode == getHashCode(codes, inst, key);
        }

        private int getHashCode(final AlarmCodes codes, final AlarmInst inst, final String key) {
            int hash = 7;
            hash = 31 * hash + codes.ordinal();
            hash = 31 * hash + inst.ordinal();
            hash = 31 * hash + key.hashCode();
            return hash;
        }

    }

    // public static void main(final String[] args) throws JsonProcessingException {
    // final StrategyStatusView view = new StrategyStatusView(1, "Test", new HashMap<>(), new HashMap<>());
    //
    // final CircularFifoQueue<ModelMessage> modelMessages = new CircularFifoQueue<>(20);
    // modelMessages.add(view.new ModelMessage(System.currentTimeMillis(), "123"));
    // modelMessages.add(view.new ModelMessage(System.currentTimeMillis(), "456"));
    // System.out.println(modelMessages.toString());
    //
    // final StrategyHeartbeat strategyHeartbeat = new StrategyHeartbeat(1, 999l, "Test");
    // strategyHeartbeat.setModelMessage(modelMessages.toString());
    // System.out.println(mapper.writeValueAsString(strategyHeartbeat));
    // }

}
