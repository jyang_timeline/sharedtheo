package com.nogle.core.strategy.classloader;

import java.net.URL;
import java.net.URLClassLoader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.exception.InterfaceException;
import com.timelinecapital.core.config.EngineConfig;

public class ClassLoaderFactory {
    private static final Logger log = LogManager.getLogger(ClassLoaderFactory.class);
    // private static final HashMap<String, ClassLoader> nameToClassLoader = new HashMap<>();
    private static StrategyInterfaceExaminator examinator;

    public static ClassLoader getJARClassLoader(final String jarName) throws Exception {
        if (jarName.isEmpty()) {
            return ClassLoader.getSystemClassLoader();
        }

        if (EngineConfig.isRemoteClassLoader()) {
            final URL url = new URL(EngineConfig.getRemoteServerURL() + "/" + jarName);
            log.warn("ClassLoader from: {}", url.toString());
            return new URLClassLoader(new URL[] { url }, ClassLoader.getSystemClassLoader());
        }

        examinator = new StrategyInterfaceExaminator(jarName);
        if (!examinator.isSameInterfaceVersion()) {
            throw new InterfaceException("Conflicts: version of StrategyInterface");
        }
        // ClassLoader classLoader = nameToClassLoader.get(jarName);
        // if (classLoader == null) {
        // classLoader = new StrategyClassLoader(jarName);
        // nameToClassLoader.put(jarName, classLoader);
        // }
        final ClassLoader classLoader = new StrategyClassLoader(jarName);
        return classLoader;
        // return ClassLoader.getSystemClassLoader();
    }

}
