package com.nogle.core.strategy.classloader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.exception.StrategyException;
import com.timelinecapital.commons.util.DelimiterUtil;

public class StrategyClassLoader extends ClassLoader {
    private static final Logger log = LogManager.getLogger(StrategyClassLoader.class);

    private final Hashtable<String, Class<?>> definedClasses = new Hashtable<>();
    private final Hashtable<String, InputStream> definedResources = new Hashtable<>();
    private final String jarName;

    public StrategyClassLoader(final String jarName) {
        super(StrategyClassLoader.class.getClassLoader());
        this.jarName = jarName;
    }

    @Override
    public InputStream getResourceAsStream(final String resourceName) {
        InputStream result = definedResources.get(resourceName);
        if (result != null) {
            return result;
        }

        JarFile jarFile = null;
        try {
            jarFile = loadJarFile();
            final JarEntry entry = jarFile.getJarEntry(resourceName);
            final InputStream inputStream = jarFile.getInputStream(entry);
            final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int nextValue = inputStream.read();
            while (-1 != nextValue) {
                byteStream.write(nextValue);
                nextValue = inputStream.read();
            }
            result = new ByteArrayInputStream(byteStream.toByteArray());
            definedResources.put(resourceName, result);

            byteStream.close();
            inputStream.close();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (jarFile != null) {
                    jarFile.close();
                }
            } catch (final IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    @Override
    public Class<?> loadClass(final String className) {
        return findClass(className);
    }

    @Override
    public Class<?> findClass(final String className) {
        Class<?> result = definedClasses.get(className); // checks in cached classes
        if (result != null) {
            return result;
        }

        try {
            return findSystemClass(className);
        } catch (final Exception e) {
        }

        log.trace("{} from {}", className, jarName);

        JarFile jarFile = null;
        try {
            jarFile = loadJarFile();
            final String classPath = className.replace(DelimiterUtil.PACKAGE_DELIMETER, DelimiterUtil.PACKAGE_PATH_DELIMETER).concat(".class");
            final JarEntry entry = jarFile.getJarEntry(classPath);
            if (entry == null) {
                throw new StrategyException("Class not found from JAR: " + classPath);
            }

            final InputStream inputStream = jarFile.getInputStream(entry);
            final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int nextValue = inputStream.read();
            while (-1 != nextValue) {
                byteStream.write(nextValue);
                nextValue = inputStream.read();
            }
            final byte[] classByte = byteStream.toByteArray();
            result = defineClass(className, classByte, 0, classByte.length, null);
            definedClasses.put(className, result);

            byteStream.close();
            inputStream.close();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (jarFile != null) {
                    jarFile.close();
                }
            } catch (final IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    // void reloadClass(final Class<?> targetClass) throws ClassNotFoundException, IOException {
    // final URL[] urls = { targetClass.getProtectionDomain().getCodeSource().getLocation() };
    // final ClassLoader delegateParent = targetClass.getClassLoader().getParent();
    //
    // try (URLClassLoader cl = new URLClassLoader(urls, delegateParent)) {
    // final Class<?> reloaded = cl.loadClass(targetClass.getName());
    // System.out.printf("reloaded my class: Class@%x%n", reloaded.hashCode());
    // System.out.println("Different classes: " + (targetClass != reloaded));
    // }
    // }

    private JarFile loadJarFile() throws IOException, URISyntaxException {
        if (new File(jarName).isFile()) {
            // jarFile = new JarFile(new File(jarName));
            return new JarFile(new File(jarName));
        } else {
            final URI jarURI = new URI(this.getClass().getClassLoader().getResource(jarName).toString());
            // jarFile = new JarFile(jarURI.getPath());
            return new JarFile(jarURI.getPath());
        }
    }

}
