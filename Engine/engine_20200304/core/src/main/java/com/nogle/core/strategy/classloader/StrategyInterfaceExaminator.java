package com.nogle.core.strategy.classloader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;

public class StrategyInterfaceExaminator {
    private static final Logger log = LogManager.getLogger(StrategyInterfaceExaminator.class);

    private static final String interfaceName = "com.nogle.strategy.api.Strategy";
    private static final String versionFieldName = "version";

    private final String jarName;
    private final String engineInterfaceVersion;
    private final String interfaceVersion;
    private DisposableClassLoader classLoader;

    public StrategyInterfaceExaminator(final String jarName)
        throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, ClassNotFoundException {
        this.jarName = jarName;
        engineInterfaceVersion = this.getClass().getClassLoader().loadClass(interfaceName).getPackage().getImplementationVersion();
        interfaceVersion = this.getClass().getClassLoader().loadClass(interfaceName).getField(versionFieldName).get(versionFieldName).toString();
    }

    public boolean isSameInterfaceVersion() throws Exception {
        if (!EngineConfig.isVersionSafeMode()) {
            log.warn("Engine will not validate JAR's strategy interface version!!!");
            return true;
        }
        classLoader = new DisposableClassLoader(jarName);
        final Class<?> clazz = classLoader.getClass(interfaceName);
        final String modelInterfaceVersion = clazz.getField(versionFieldName).get(versionFieldName).toString();
        // final String modelInterfaceVersion = clazz.getPackage().getImplementationVersion();

        log.info("Version check: current ENGINE @{}, JAR: {} @{}", engineInterfaceVersion, jarName, modelInterfaceVersion);
        return engineInterfaceVersion.equals(modelInterfaceVersion) || interfaceVersion.equals(modelInterfaceVersion);
    }

    private class DisposableClassLoader extends ClassLoader {

        private final JarFile jarFile;

        public DisposableClassLoader(final String jarName) throws Exception {
            super();
            if (new File(jarName).isFile()) {
                jarFile = new JarFile(new File(jarName));
            } else {
                final URL jarURL = this.getClass().getClassLoader().getResource(jarName);
                if (jarURL == null) {
                    throw new Exception("Source file not found: " + jarName);
                }
                log.info("Checking JAR {}", jarURL.getPath());
                jarFile = new JarFile(jarURL.getPath());
            }
        }

        public Class<?> getClass(final String className) {
            Class<?> result = null;
            try {
                final JarEntry interfaceEntry = jarFile.getJarEntry(interfaceName.replace(DelimiterUtil.PACKAGE_DELIMETER, DelimiterUtil.PACKAGE_PATH_DELIMETER).concat(".class"));
                final InputStream inputStream = jarFile.getInputStream(interfaceEntry);
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int nextValue = inputStream.read();
                while (-1 != nextValue) {
                    outputStream.write(nextValue);
                    nextValue = inputStream.read();
                }
                final byte[] classByte = outputStream.toByteArray();
                result = defineClass(interfaceName, classByte, 0, classByte.length, null);

                outputStream.close();
                inputStream.close();
            } catch (final IOException e) {
                log.error(e.getMessage(), e);
            } finally {
                try {
                    jarFile.close();
                } catch (final IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            return result;
        }
    }

}
