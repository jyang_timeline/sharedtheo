package com.nogle.core.strategy.config;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;

import com.nogle.commons.utils.NogleTimeFormatter;

public class SimulatedTimeFrame {

    private final DateTime startDate;
    private final DateTime endDate;
    private final Date[] dates;

    public SimulatedTimeFrame(final DateTime startDate, final DateTime endDate) {
        this.startDate = startDate;
        this.endDate = endDate;

        final List<Date> lsDates = new ArrayList<>();
        DateTime temp = startDate;
        while (!temp.isAfter(endDate)) {
            lsDates.add(temp.toDate());
            temp = temp.withFieldAdded(DurationFieldType.days(), 1);
        }
        dates = lsDates.toArray(new Date[lsDates.size()]);
    }

    public Date[] getDates() {
        return dates;
    }

    public DateTime getTradingDate() {
        return startDate;
    }

    public String getTradingDay() {
        return startDate.toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
    }

    public long getClosingTime() {
        return startDate.withFieldAdded(DurationFieldType.hours(), 18).getMillis();
    }

    @Override
    public String toString() {
        return startDate.toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT) + " to " + endDate.toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
    }

}
