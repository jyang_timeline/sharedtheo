package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.marketdata.type.BestPriceOrderDetail;

@Deprecated
public final class BestPriceOrderDetailHandler implements EventHandler {

    private final StrategyUpdater strategyUpdater;
    private final ConcurrentLinkedQueue<BestPriceOrderDetail> queue;
    private final String handlerName;

    public BestPriceOrderDetailHandler(final ConcurrentLinkedQueue<BestPriceOrderDetail> queue, final StrategyUpdater strategyUpdater, final String symbol) {
        this.queue = queue;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + symbol + "-BestOrderDetailQueue";
    }

    public final void add(final BestPriceOrderDetail bestOrderDetail) {
        queue.add(bestOrderDetail);
    }

    @Override
    public final void handle() {
        final BestPriceOrderDetail bestOrderDetail = queue.poll();
        strategyUpdater.onBestPriceOrderDetail(bestOrderDetail);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
