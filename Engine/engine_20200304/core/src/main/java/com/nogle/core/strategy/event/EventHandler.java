package com.nogle.core.strategy.event;

public interface EventHandler {

    void handle();

    String getName();

    default double getReturnedValue() {
        return 0.0d;
    }

}
