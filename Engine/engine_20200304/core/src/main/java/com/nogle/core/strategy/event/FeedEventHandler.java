package com.nogle.core.strategy.event;

public interface FeedEventHandler<T> extends EventHandler {

    void wrap(T event);

}
