package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Fill;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.event.RenewableResource;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;

public final class FillHandler implements EventHandler, RenewableResource, MarketOpenAware {

    private final ConcurrentLinkedQueue<Fill> fillQueue = new ConcurrentLinkedQueue<>();
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public FillHandler(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-FillQueue";
    }

    public final void add(final Fill fill) {
        fillQueue.add(fill);
    }

    @Override
    public final void handle() {
        final OrderFill fill = (OrderFill) fillQueue.poll();
        strategyUpdater.onFill(fill);
        ExecReportFactory.recycleFill(fill);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public final void reset() {
        while (!fillQueue.isEmpty()) {
            ExecReportFactory.recycleFill((OrderFill) fillQueue.poll());
        }
        fillQueue.clear();
    }

    @Override
    public final void rewind() {
        while (!fillQueue.isEmpty()) {
            ExecReportFactory.recycleFill((OrderFill) fillQueue.poll());
        }
        fillQueue.clear();
    }

    @Override
    public final boolean isReady() {
        return fillQueue.isEmpty();
    }

}
