package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.marketdata.type.MarketBook;

@Deprecated
public final class MarketBookHandler implements EventHandler {

    private final ConcurrentLinkedQueue<MarketBook> marketBookQueue;
    private final StrategyQuoteSnapshot quoteSnapshot;
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public MarketBookHandler(final ConcurrentLinkedQueue<MarketBook> marketBookQueue, final StrategyQuoteSnapshot quoteSnapshot, final StrategyUpdater strategyUpdater) {
        this.marketBookQueue = marketBookQueue;
        this.quoteSnapshot = quoteSnapshot;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + quoteSnapshot.getContract().getSymbol() + "-BookQueue";
    }

    public final void add(final MarketBook book) {
        marketBookQueue.add(book);
    }

    @Override
    public final void handle() {
        final MarketBook book = marketBookQueue.poll();
        quoteSnapshot.setMarketBook(book);
        strategyUpdater.onMarketBook(book);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
