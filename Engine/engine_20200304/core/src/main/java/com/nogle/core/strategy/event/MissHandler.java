package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Miss;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.event.RenewableResource;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;

public final class MissHandler implements EventHandler, RenewableResource, MarketOpenAware {

    private final ConcurrentLinkedQueue<Miss> missQueue = new ConcurrentLinkedQueue<>();
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public MissHandler(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-MissQueue";
    }

    public final void add(final Miss miss) {
        missQueue.add(miss);
    }

    @Override
    public final void handle() {
        final OrderMiss miss = (OrderMiss) missQueue.poll();
        strategyUpdater.onMiss(miss);
        ExecReportFactory.recycleMiss(miss);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public final void reset() {
        while (!missQueue.isEmpty()) {
            ExecReportFactory.recycleMiss((OrderMiss) missQueue.poll());
        }
        missQueue.clear();
    }

    @Override
    public final void rewind() {
        while (!missQueue.isEmpty()) {
            ExecReportFactory.recycleMiss((OrderMiss) missQueue.poll());
        }
        missQueue.clear();
    }

    @Override
    public final boolean isReady() {
        return missQueue.isEmpty();
    }

}
