package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.marketdata.type.OrderActions;

@Deprecated
public final class OrderActionsHandler implements EventHandler {

    private final StrategyUpdater strategyUpdater;
    private final ConcurrentLinkedQueue<OrderActions> quoteOrderDetailQueue;
    private final String handlerName;

    public OrderActionsHandler(final ConcurrentLinkedQueue<OrderActions> quoteOrderDetailQueue, final StrategyUpdater strategyUpdater, final String symbol) {
        this.quoteOrderDetailQueue = quoteOrderDetailQueue;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + symbol + "-QuoteOrderDetailQueue";
    }

    public final void add(final OrderActions orderDetail) {
        quoteOrderDetailQueue.add(orderDetail);
    }

    @Override
    public final void handle() {
        final OrderActions quoteOrderDetail = quoteOrderDetailQueue.poll();
        strategyUpdater.onOrderActions(quoteOrderDetail);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
