package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.strategy.types.ExecutionReport;
import com.nogle.util.ArrayUtils;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.event.RenewableResource;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;

public final class ProtectionHandler implements EventHandler, RenewableResource, MarketOpenAware {

    private final ConcurrentLinkedQueue<ExecutionReport> reportQueue = new ConcurrentLinkedQueue<>();
    private final String handlerName;
    // private final Map<Contract, MarketEvent> contractStats;

    private BurstinessCondition[] pnlProtections = new BurstinessCondition[0];
    private BurstinessCondition[] cancelsProtections = new BurstinessCondition[0];
    private BurstinessCondition[] rejectsProtections = new BurstinessCondition[0];

    public ProtectionHandler(final ProtectiveManager manager) {
        handlerName = manager.getClass().getSimpleName() + "-ProtectionQueue";
        // contractStats = EngineStatusView.getSymbolToMarket();
        for (final BurstinessCondition burstCondition : manager.getBurstConditions()) {
            switch (burstCondition.getAvailableEventType()) {
                case Fill:
                    pnlProtections = ArrayUtils.addElement(pnlProtections, burstCondition);
                    break;
                case Reject:
                    rejectsProtections = ArrayUtils.addElement(rejectsProtections, burstCondition);
                    break;
                case CancelAck:
                    cancelsProtections = ArrayUtils.addElement(cancelsProtections, burstCondition);
                    break;
                default:
                    break;
            }
        }
    }

    public final void add(final ExecutionReport report) {
        reportQueue.add(report);
    }

    @Override
    public final void handle() {
        final ExecutionReport report = reportQueue.poll();
        if (report != null) {
            switch (report.getExecType()) {
                case FILL:
                    onFill(report);
                    // contractStats.get(report.getContract()).updateWorkingOrders((Fill) report);
                    break;
                case NEWORDER_ACK:
                    // contractStats.get(report.getContract()).insertWorkingOrders((Ack) report);
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case CANCEL_ACK:
                    onCancel(report);
                    // contractStats.get(report.getContract()).removeWorkingOrders(report);
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case REJECT:
                    onReject(report);
                    ExecReportFactory.recycleReject((Reject) report);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public final void reset() {
        while (!reportQueue.isEmpty()) {
            final ExecutionReport report = reportQueue.poll();
            switch (report.getExecType()) {
                case FILL:
                    ExecReportFactory.recycleFill((OrderFill) report);
                    break;
                case NEWORDER_ACK:
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case CANCEL_ACK:
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case REJECT:
                    ExecReportFactory.recycleReject((Reject) report);
                    break;
                default:
                    break;
            }
        }
        reportQueue.clear();
    }

    @Override
    public final void rewind() {
        while (!reportQueue.isEmpty()) {
            final ExecutionReport report = reportQueue.poll();
            switch (report.getExecType()) {
                case FILL:
                    ExecReportFactory.recycleFill((OrderFill) report);
                    break;
                case NEWORDER_ACK:
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case CANCEL_ACK:
                    ExecReportFactory.recycleAck((Ack) report);
                    break;
                case REJECT:
                    ExecReportFactory.recycleReject((Reject) report);
                    break;
                default:
                    break;
            }
        }
        reportQueue.clear();
    }

    @Override
    public final boolean isReady() {
        return reportQueue.isEmpty();
    }

    private void onFill(final ExecutionReport report) {
        for (final BurstinessCondition condition : pnlProtections) {
            condition.onWatch(BurstinessCondition.EventType.Fill, report);
        }
    }

    private void onCancel(final ExecutionReport report) {
        for (final BurstinessCondition condition : cancelsProtections) {
            condition.onWatch(BurstinessCondition.EventType.CancelAck, report);
        }
    }

    private void onReject(final ExecutionReport report) {
        for (final BurstinessCondition condition : rejectsProtections) {
            condition.onWatch(BurstinessCondition.EventType.Reject, report);
        }
    }

}
