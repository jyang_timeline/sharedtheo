package com.nogle.core.strategy.event;

import com.lmax.disruptor.EventFactory;

public class StrategyEvent {

    public static final Factory FACTORY = new Factory();

    /**
     * Creates the events that will be put in the RingBuffer.
     */
    private static class Factory implements EventFactory<StrategyEvent> {

        @Override
        public StrategyEvent newInstance() {
            return new StrategyEvent();
        }
    }

    private EventHandler eventHandler;

    public void setHandler(final EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void execute() {
        eventHandler.handle();
    }

    public void clear() {
        eventHandler = null;
    }

}
