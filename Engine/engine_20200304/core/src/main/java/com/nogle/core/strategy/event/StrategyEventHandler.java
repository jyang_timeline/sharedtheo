package com.nogle.core.strategy.event;

import com.lmax.disruptor.EventHandler;

public class StrategyEventHandler implements EventHandler<StrategyEvent> {

    @Override
    public final void onEvent(final StrategyEvent event, final long sequence, final boolean endOfBatch) throws Exception {
        event.execute();
        event.clear();
    }

}
