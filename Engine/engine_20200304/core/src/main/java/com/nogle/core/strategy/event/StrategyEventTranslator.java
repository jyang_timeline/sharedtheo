package com.nogle.core.strategy.event;

import com.lmax.disruptor.EventTranslatorOneArg;

public class StrategyEventTranslator implements EventTranslatorOneArg<StrategyEvent, EventHandler> {

    @Override
    public void translateTo(final StrategyEvent event, final long sequence, final EventHandler eventHandler) {
        event.setHandler(eventHandler);
    }

}
