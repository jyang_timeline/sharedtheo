package com.nogle.core.strategy.event;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.marketdata.type.Tick;

@Deprecated
public final class TickHandler implements EventHandler {

    private final ConcurrentLinkedQueue<Tick> tickQueue;
    private final StrategyQuoteSnapshot quoteSnapshot;
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public TickHandler(final ConcurrentLinkedQueue<Tick> tickQueue, final StrategyQuoteSnapshot quoteSnapshot, final StrategyUpdater strategyUpdater) {
        this.tickQueue = tickQueue;
        this.quoteSnapshot = quoteSnapshot;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + quoteSnapshot.getContract().getSymbol() + "-TickQueue";
    }

    public final void add(final Tick tick) {
        tickQueue.add(tick);
    }

    @Override
    public final void handle() {
        final Tick tick = tickQueue.poll();
        quoteSnapshot.setTick(tick);
        strategyUpdater.onTick(tick);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
