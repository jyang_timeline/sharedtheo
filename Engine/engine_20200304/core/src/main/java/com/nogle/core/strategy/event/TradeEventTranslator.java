package com.nogle.core.strategy.event;

import com.lmax.disruptor.EventTranslator;
import com.nogle.core.event.TradeEvent;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.core.execution.factory.RequestInstance;

public class TradeEventTranslator implements EventTranslator<RequestInstance> {

    private NewOrderEncoder entryEncoder;
    private CancelOrderEncoder cancelEncoder;
    private ExecRequestType requestType;
    private TimeCondition tif;
    private Side side;
    private long clOrdId;
    private long quantity;
    private double price;
    private PositionPolicy positionPolicy;
    private long positionMax;
    private TradeEvent tradeEvent;

    private long sourceEventMicros;
    private long commitEventMicros;

    @Override
    public void translateTo(final RequestInstance event, final long sequence) {
        if (ExecRequestType.OrderEntry.equals(requestType)) {
            event.setOrderEntry(entryEncoder, side, tif, clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent);
            event.setTime(sourceEventMicros, commitEventMicros);
        } else {
            event.setCancelEntry(cancelEncoder, clOrdId, tradeEvent);
            event.setTime(sourceEventMicros, commitEventMicros);
        }
        clear(); // clear the translator
    }

    public void setBasicValues(final NewOrderEncoder encoder, final ExecRequestType requestType, final TimeCondition tif, final Side side,
        final long clOrdId, final long quantity, final double price, final PositionPolicy positionPolicy, final long positionMax, final TradeEvent tradeEvent,
        final long sourceEventMicros, final long commitEventMicros) {
        this.entryEncoder = encoder;
        this.requestType = requestType;
        this.tif = tif;
        this.side = side;
        this.clOrdId = clOrdId;
        this.quantity = quantity;
        this.price = price;
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
        this.tradeEvent = tradeEvent;
        this.sourceEventMicros = sourceEventMicros;
        this.commitEventMicros = commitEventMicros;
    }

    public void setBasicValues(final CancelOrderEncoder encoder, final ExecRequestType requestType, final long clOrdId, final TradeEvent tradeEvent,
        final long sourceEventMicros, final long commitEventMicros) {
        this.cancelEncoder = encoder;
        this.requestType = requestType;
        this.clOrdId = clOrdId;
        this.tradeEvent = tradeEvent;
        this.sourceEventMicros = sourceEventMicros;
        this.commitEventMicros = commitEventMicros;
    }

    /**
     * Release references held by this object to allow objects to be garbage-collected.
     */
    private void clear() {
        setBasicValues(null, // encoder
            null, // request type
            null, // time condition
            null, // side
            0, // clOrdId
            0, // quantity
            0, // price
            null, // position policy
            0, // position max
            null, // trade event
            0, // source event time
            0 // commit event time
        );
    }

}
