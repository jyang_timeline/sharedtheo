package com.nogle.core.strategy.logging;

import java.io.Serializable;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;

import com.nogle.core.strategy.StrategyView;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.core.util.BinaryMessageTopics;

public class MessageHubAppender extends AbstractAppender {

    private final BinaryMessage binaryMessage;
    private final StrategyView view;

    protected MessageHubAppender(final int strategyId, final String name, final Filter filter, final Layout<? extends Serializable> layout, final StrategyView view) {
        super(name, filter, layout);
        this.view = view;
        binaryMessage = new BinaryMessage(new MessageHeader(BinaryMessageTopics.HEADER_LOG_STRATEGY, strategyId), null);
    }

    @Override
    public void append(final LogEvent event) {
        final byte[] data = getLayout().toByteArray(event);
        binaryMessage.setPayLoad(data);
        try {
            Messenger.send(binaryMessage);
            if (event.getLevel().isMoreSpecificThan(Level.ERROR)) {
                view.armWith(event.getMessage().getFormattedMessage());
            }
        } catch (final Exception exception) {
            throw new AppenderLoggingException(exception);
        }
    }

}
