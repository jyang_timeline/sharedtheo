package com.nogle.core.strategy.logging;

import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;

import com.nogle.core.util.AccurateClock;
import com.nogle.core.util.MicrosecondClock;
import com.nogle.core.util.TradeClock;

@Plugin(name = "PrecisionConverter", category = "Converter")
@ConverterKeys({ "clock" })
public class PrecisionPatternConverter extends LogEventPatternConverter {

    private static final String FORMAT_KEY = "clock";
    private static PrecisionPatternConverter INSTANCE;
    private static boolean isSimulation = false;

    private AccurateClock clock;

    protected PrecisionPatternConverter(final String name, final String style) {
        super(name, style);

        if (isSimulation) {
            // running in Simulation mode. Use clock from historical data
            clock = new AccurateClock() {

                @Override
                public long getPreciseTime() {
                    return TradeClock.getCurrentMicros();
                }
            };
        } else {
            if (SystemUtils.IS_OS_WINDOWS) {
                // running in Windows platform. Use system clock
                clock = new AccurateClock() {

                    @Override
                    public long getPreciseTime() {
                        return System.currentTimeMillis();
                    }
                };
            } else {
                // running in Linux platform. Use c/JNI clock
                clock = new AccurateClock() {
                    MicrosecondClock msClock = new MicrosecondClock();

                    @Override
                    public long getPreciseTime() {
                        return msClock.getMicrosecond();
                    }
                };
            }
        }

    }

    public static void onSimulationMode() {
        // running in sim mode. Use historical data
        isSimulation = true;
        // INSTANCE.clock = new AccurateClock() {
        //
        // @Override
        // public long getPreciseTime() {
        // return TradeClock.getCurrentMicros();
        // }
        // };
    }

    public static PrecisionPatternConverter newInstance(final String[] options) {
        INSTANCE = new PrecisionPatternConverter(FORMAT_KEY, FORMAT_KEY);
        return INSTANCE;
    }

    @Override
    public void format(final LogEvent event, final StringBuilder toAppendTo) {
        toAppendTo.append(clock.getPreciseTime());
    }

}
