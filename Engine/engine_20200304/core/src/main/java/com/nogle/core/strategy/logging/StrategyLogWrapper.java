package com.nogle.core.strategy.logging;

import java.nio.charset.Charset;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.routing.Route;
import org.apache.logging.log4j.core.appender.routing.Routes;
import org.apache.logging.log4j.core.appender.routing.RoutingAppender;
import org.apache.logging.log4j.core.async.AsyncLoggerConfig;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;

import com.nogle.core.config.EngineMode;
import com.nogle.core.strategy.StrategyView;
import com.nogle.strategy.api.StrategyBuilder;
import com.timelinecapital.core.config.EngineConfig;

public class StrategyLogWrapper {

    private final static String MESSAGEHUB_PATTERN = "%-5level%d{yyyy-MM-dd HH:mm:ss.SSS} %msg";
    private final static String ROLLINGFILE_APPENDER = "EngineRollingFile";
    private final static String ROUTINGFILE_APPENDER = "StrategyRoutingFile";
    private final static String SYS_PROPERTY_ENABLE_JMS = "enableJMS";
    private final static String JMS_APPENDER = "jmsQueue";

    private final String MESSAGE_HUB_ROUTING_APPENDER;
    private final String MESSAGE_HUB_APPENDER;
    private final int strategyId;
    private final String strategyName;
    private final StrategyView view;

    private Logger log = null;
    private Level messageHubLevel = Level.DEBUG;

    public StrategyLogWrapper(final int strategyId, final String strategyName, final Class<? extends StrategyBuilder> clazz, final StrategyView view) {
        this.strategyId = strategyId;
        this.strategyName = strategyName;
        this.view = view;

        MESSAGE_HUB_ROUTING_APPENDER = "MessageHubRouting@" + strategyId;
        MESSAGE_HUB_APPENDER = "MessageHubAppender@" + strategyId;
        if (EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode()) || EngineMode.TESTING.equals(EngineConfig.getEngineMode())) {
            if (EngineConfig.isLessNetworkTraffic()) {
                messageHubLevel = Level.WARN;
            }
            createLiveModeAppender(clazz);
            // createSimuModeAppender(clazz);
        } else {
            createSimuModeAppender(clazz);
        }
    }

    private void createSimuModeAppender(final Class<? extends StrategyBuilder> clazz) {
        // final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        // final LoggerConfig systemLoggerConfig = ctx.getConfiguration().getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        // if (systemLoggerConfig.getAppenders().get(ROUTINGFILE_APPENDER) != null) {
        // systemLoggerConfig.getAppenders().get(ROUTINGFILE_APPENDER).stop();
        // systemLoggerConfig.removeAppender(ROUTINGFILE_APPENDER);
        // }
        // ctx.updateLoggers();
        log = LogManager.getLogger(clazz);
    }

    private void createLiveModeAppender(final Class<? extends StrategyBuilder> clazz) {
        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final Configuration systemLoggerConfig = ctx.getConfiguration();

        final AppenderRef[] refs = new AppenderRef[] { AppenderRef.createAppenderRef(clazz.getSimpleName(), null, null) };
        final LoggerConfig modelLoggerConfig = AsyncLoggerConfig.createLogger(false, Level.DEBUG, clazz.getName(), "false", refs, null, systemLoggerConfig, null);

        final Appender rollingFileAppender = systemLoggerConfig.getAppender(ROLLINGFILE_APPENDER);
        modelLoggerConfig.addAppender(rollingFileAppender, Level.DEBUG, null);

        final Appender fileRoutingAppender = systemLoggerConfig.getAppender(ROUTINGFILE_APPENDER);
        modelLoggerConfig.addAppender(fileRoutingAppender, Level.DEBUG, null);

        if (Boolean.valueOf(System.getProperty(SYS_PROPERTY_ENABLE_JMS)) && EngineMode.PRODUCTION.equals(EngineConfig.getEngineMode())) {
            final Appender jmsAppender = systemLoggerConfig.getAppender(JMS_APPENDER);
            if (jmsAppender != null) {
                modelLoggerConfig.addAppender(jmsAppender, Level.DEBUG, null);
            } else {
                System.err.println("JMSQueue appender is missing!!!");
            }
        }

        final Appender messageHubAppender = createMesssageHubAppender(systemLoggerConfig);
        // make MessageHubAppender as an Appender in Appenders
        systemLoggerConfig.addAppender(messageHubAppender);

        final Appender messageRoutingAppender = createMessageHubRoutingAppender(systemLoggerConfig, modelLoggerConfig);
        // make MessageRoutingAppender as an Appender in Appenders
        systemLoggerConfig.addAppender(messageRoutingAppender);
        // make MessageRoutingAppender as a Ref in ROOT logger
        systemLoggerConfig.getRootLogger().addAppender(messageRoutingAppender, messageHubLevel, null);

        // create a Logger in Loggers
        final String loggerKey = new StringBuilder(clazz.getName()).append("@").append(strategyId).toString();
        systemLoggerConfig.addLogger(loggerKey, modelLoggerConfig);

        ctx.updateLoggers();
        log = LogManager.getLogger(loggerKey);
    }

    private Appender createMesssageHubAppender(final Configuration config) {
        final Layout<?> messageHubLayout = PatternLayout.newBuilder()
            .withPattern(MESSAGEHUB_PATTERN)
            .withConfiguration(config)
            .withCharset(Charset.forName("UTF-8"))
            .withAlwaysWriteExceptions(false)
            .withNoConsoleNoAnsi(false)
            .build();
        final String appenderName = MESSAGE_HUB_APPENDER;
        final Appender messageHubAppender = new MessageHubAppender(strategyId, appenderName, null, messageHubLayout, view);
        messageHubAppender.start();
        return messageHubAppender;
    }

    private Appender createMessageHubRoutingAppender(final Configuration config, final LoggerConfig loggerConfig) {
        final Route messageRoute = Route.createRoute(MESSAGE_HUB_APPENDER, strategyId + "-" + strategyName, null);
        final Route[] routeArray = new Route[1];
        routeArray[0] = messageRoute;
        final Routes routes = Routes.newBuilder()
            .withPattern("${ctx:ROUTINGKEY}")
            .withRoutes(routeArray)
            .build();
        // .createRoutes("${ctx:ROUTINGKEY}", messageRoute);

        final RoutingAppender messageRoutingAppender = RoutingAppender.newBuilder()
            .withName(MESSAGE_HUB_ROUTING_APPENDER)
            .withIgnoreExceptions(false)
            .withRoutes(routes)
            .setConfiguration(config)
            .build();
        messageRoutingAppender.start();
        loggerConfig.addAppender(messageRoutingAppender, messageHubLevel, null);
        return messageRoutingAppender;
    }

    public Logger getLogger() {
        return log;
    }

}
