package com.nogle.core.strategy.logging;

import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.TradeLogger;

public class StrategyTradeLogger implements TradeLogger {

    private final Logger log;

    public StrategyTradeLogger(final Logger log) {
        this.log = log;
    }

    @Override
    public void info(String message) {
        log.info(message);
    }

    @Override
    public void info(String message, Object... arguments) {
        log.info(message, arguments);
    }

    @Override
    public void error(String message) {
        log.error(message);
    }

    @Override
    public void error(String message, Object... arguments) {
        log.error(message, arguments);
    }

    @Override
    public void warn(String message) {
        log.warn(message);
    }

    @Override
    public void warn(String message, Object... arguments) {
        log.warn(message, arguments);
    }

    @Override
    public void debug(String message) {
        log.debug(message);

    }

    @Override
    public void debug(String message, Object... arguments) {
        log.debug(message, arguments);
    }

}
