package com.nogle.core.trade;

import java.util.HashMap;
import java.util.Map;

import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.trade.TradingAccount;

public abstract class AbstractTradeProxy implements TradeProxy {

    private final Map<Integer, ClientOrderIdGenerator> seqIds = new HashMap<>();

    public AbstractTradeProxy() {
    }

    protected abstract OrderSender subscribeToSymbol(final int strategyId, final Contract contract, TradingAccount account, PositionImport importer);

    protected void resetIdGenerator(final int strategyId) {
        final ClientOrderIdGenerator idGenerator = seqIds.get(strategyId);
        if (idGenerator != null) {
            idGenerator.resetId();
        }
    }

    @Override
    public final OrderViewContainer getOrderView(final int strategyId, final String threadTag, final Contract contract, final TradeAppendix tradeAppendix) {
        final OrderSender orderSender = subscribeToSymbol(strategyId, contract, tradeAppendix.getAccount(), tradeAppendix.getPositionImporter());
        ClientOrderIdGenerator idGenerator = seqIds.get(strategyId);
        if (idGenerator == null) {
            idGenerator = new ClientOrderIdGenerator(strategyId);
            seqIds.put(strategyId, idGenerator);
        }
        return new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, threadTag);
    }

}
