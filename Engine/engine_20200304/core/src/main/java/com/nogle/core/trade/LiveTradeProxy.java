package com.nogle.core.trade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.NodeFactory;
import com.nogle.core.ClientParameters;
import com.nogle.core.EngineStatusView;
import com.nogle.core.config.EngineThreadPolicy;
import com.nogle.core.exception.ProxyActivationException;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.sender.DelegatoryOrderSender;
import com.timelinecapital.core.factory.EndpointFactory;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.trade.ExecutorInterpretNode;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.util.AffinityThreadFactory;
import com.timelinecapital.core.util.EndpointHelper;
import com.timelinecapital.core.util.TaskThreadFactory;
import com.timelinecapital.core.util.TradingHoursHelper;

import net.openhft.affinity.AffinityStrategies;

public class LiveTradeProxy extends AbstractTradeProxy {
    private static final Logger log = LogManager.getLogger(LiveTradeProxy.class);

    private static final int socketTimeoutInSec = 3;

    private final Map<String, TradeConnection> endpointToConnection = new HashMap<>();
    private final List<TradeConnection> tradeConnections;
    private final ClientParameters clientParameters;

    private TradeConnection tradeAgent;

    public LiveTradeProxy(final ClientParameters clientParameters, final List<TradeConnection> tradeConnections) {
        this.clientParameters = clientParameters;
        this.tradeConnections = tradeConnections;
    }

    private boolean authAccount() {
        return true;
    }

    @Override
    protected OrderSender subscribeToSymbol(final int strategyId, final Contract contract, final TradingAccount account, final PositionImport importer) {
        final String protocol = EngineConfig.getTradeProtocol();
        final String exchange = contract.getExchange();

        final String tradeEndpoint = EndpointFactory.getTradeEndpoints(protocol, account.getAccountName()).getTrade();

        tradeAgent = endpointToConnection.get(tradeEndpoint);
        if (tradeAgent == null) {
            try {
                log.info("[Connection] ******** Trade client endpoint: {} ********", tradeEndpoint);
                final String name = "TCPTrade-" + tradeEndpoint.substring(tradeEndpoint.lastIndexOf(":") + 1);
                final ConnectionProbe connProbe = new ConnectionProbe(ConnectionType.TRADE, "T" + EndpointHelper.getTradePort(tradeEndpoint) + ":lo");

                final ThreadFactory threadFactory = EngineThreadPolicy.DEDICATE.equals(EngineConfig.getSocketPolicy()) ? new AffinityThreadFactory(name, false,
                    AffinityStrategies.ANY) : TaskThreadFactory.createThreadFactory(name, 0);

                tradeAgent = new ExecutorInterpretNode(
                    NodeFactory.newNode(
                        threadFactory,
                        tradeEndpoint,
                        19999 + EngineConfig.getEngineId()),
                    socketTimeoutInSec,
                    connProbe,
                    account,
                    importer);

                // Trade agent's start has been set to trading-hours dependent
                tradeConnections.add(tradeAgent);
                endpointToConnection.put(tradeEndpoint, tradeAgent);
                EngineStatusView.getInstance().addConnectionProbe(connProbe);
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
                throw new ProxyActivationException("Fail to launch Trade proxy");
            }
        }
        tradeAgent.updateTradingSessions(TradingHoursHelper.getTradingHours(exchange));

        log.info("*** {} : {} has been initialized ***", exchange, contract);
        final OrderSender orderSender = new DelegatoryOrderSender(contract, tradeAgent);
        return orderSender;
    }

    public TradeConnection getTradeConnection() {
        return tradeAgent;
    }

    @Override
    public boolean connect() {
        log.info("Connecting to trade server.");
        // connect, auth
        if (authAccount()) {
            log.info("Account {} successfully authenticated with trade server.", clientParameters.getUsername());
            return true;
        } else {
            log.error("Account {} failed to authenticate with trade server.", clientParameters.getPassword());
            return false;
        }
    }

}
