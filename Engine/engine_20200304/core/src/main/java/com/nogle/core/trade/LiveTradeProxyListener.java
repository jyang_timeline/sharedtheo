package com.nogle.core.trade;

import com.nogle.core.EventTask;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.strategy.event.FillHandler;
import com.nogle.core.strategy.event.MissHandler;
import com.nogle.core.strategy.event.ProtectionHandler;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.Reject;

public class LiveTradeProxyListener implements TradeProxyListener {

    private final EventTask task;
    private final FillHandler fillHandler;
    private final MissHandler missHandler;
    private final ProtectionHandler protectionHandler;
    private final String desc;

    public LiveTradeProxyListener(final EventTask task, final FillHandler fillHandler, final MissHandler missHandler, final ProtectionHandler protectionHandler) {
        this.task = task;
        this.fillHandler = fillHandler;
        this.missHandler = missHandler;
        this.protectionHandler = protectionHandler;
        desc = task.getTaskName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            fillHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            missHandler.getName() + DelimiterUtil.CLASS_DESC_SEPARATOR +
            protectionHandler.getName();
    }

    @Override
    public final void onFill(final OrderFill fill) {
        fillHandler.add(fill);
        task.invoke(fillHandler);

        protectionHandler.add(fill);
        EventTaskFactory.getSystemTask().invoke(protectionHandler);
    }

    @Override
    public final void onMiss(final OrderMiss miss) {
        missHandler.add(miss);
        task.invoke(missHandler);
    }

    @Override
    public final void onAck(final Ack creation) {
        protectionHandler.add(creation);
        EventTaskFactory.getSystemTask().invoke(protectionHandler);
    }

    @Override
    public final void onReject(final Reject reject) {
        protectionHandler.add(reject);
        EventTaskFactory.getSystemTask().invoke(protectionHandler);
    }

    @Override
    public final void onCancel(final Ack cancel) {
        protectionHandler.add(cancel);
        EventTaskFactory.getSystemTask().invoke(protectionHandler);
    }

    @Override
    public final void rewind() {
        fillHandler.rewind();
        missHandler.rewind();
        protectionHandler.rewind();
    }

    @Override
    public final boolean isReady() {
        return fillHandler.isReady() && missHandler.isReady() && protectionHandler.isReady();
    }

    @Override
    public final String toString() {
        return desc;
    }

}
