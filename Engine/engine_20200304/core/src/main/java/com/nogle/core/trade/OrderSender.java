package com.nogle.core.trade;

import com.nogle.core.event.TradeEvent;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;

public interface OrderSender extends SchedulableOrder {

    void sendNewDayOrder(long clOrdId, Side side, long quantity, double price, PositionPolicy positionPolicy, long positionMax, TradeEvent tradeEventHandler,
        long sourceMicros, long commitMicros) throws Exception;

    void sendNewIOCOrder(long clOrdId, Side side, long quantity, double price, PositionPolicy positionPolicy, long positionMax, TradeEvent tradeEventHandler,
        long sourceMicros, long commitMicros) throws Exception;

    void sendOrderCancel(long clOrdId, Side side, TradeEvent tradeEventHandler, long sourceMicros, long commitMicros) throws Exception;

}
