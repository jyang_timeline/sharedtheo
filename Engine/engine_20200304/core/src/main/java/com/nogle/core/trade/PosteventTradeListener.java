package com.nogle.core.trade;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.market.LiveMarket;
import com.nogle.core.strategy.StrategyView;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;

public class PosteventTradeListener implements TradeProxyListener {
    private static final Logger log = LogManager.getLogger(PosteventTradeListener.class);

    private final int strategyId;
    private final String strategyName;
    private final StrategyView view;
    private final LiveMarket market;

    public PosteventTradeListener(final int strategyId, final String strategyName, final StrategyView view, final LiveMarket market) {
        this.strategyId = strategyId;
        this.strategyName = strategyName;
        this.view = view;
        this.market = market;
    }

    @Override
    public void onFill(final OrderFill fill) {
        market.publishState(view);
        log.info("{}-{} Post FILL: {} {}@{}", strategyId, strategyName, fill.getContract(), fill.getFillQty(), fill.getFillPrice());
    }

    @Override
    public void onAck(final Ack creation) {
        market.publishState(view);
        ExecReportFactory.recycleAck(creation);
        log.info("{}-{} Post ACK: {}", strategyId, strategyName, creation.getClOrderId());
    }

    @Override
    public void onMiss(final OrderMiss miss) {
        market.publishState(view);
        log.info("{}-{} Post IOC-Miss: {} {}@{}", strategyId, strategyName, miss.getContract(), miss.getMissQty(), miss.getMissPrice());
    }

    @Override
    public void onReject(final Reject reject) {
        market.publishState(view);
        log.info("{}-{} Post REJECT: {} {}", strategyId, strategyName, reject.getClOrderId(), reject.getRejectReason());
    }

    @Override
    public void onCancel(final Ack cancel) {
        market.publishState(view);
        log.info("{}-{} Post CANCEL: {}", strategyId, strategyName, cancel.getClOrderId());
    }

    @Override
    public void rewind() {
    }

    @Override
    public boolean isReady() {
        return true;
    }

}
