package com.nogle.core.trade;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.core.event.AuctionFilter;
import com.nogle.core.event.AuctionFreezeFilter;
import com.nogle.core.exception.EventNotSupportedException;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.Filter.Result;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.trade.simulation.SimOrderEventHandler;
import com.timelinecapital.strategy.state.ExchangeSession;

public final class PseudoBookExchange extends SimulatedExchange {
    private static final Logger log = LogManager.getLogger(SimulatedExchange.class);

    private final Map<Contract, SimOrderEventHandler> contractToOrderHandler = new HashMap<>();

    public PseudoBookExchange(final String exchangeName, final long latencyInMicros, final long iocLatencyInMicros, final Queue<DelayedMessage<?>> outgoingMessages) {
        super(exchangeName, latencyInMicros, iocLatencyInMicros, outgoingMessages);
    }

    @Override
    public void registerSymbol(final Contract contract, final FeeCalculator feeCalculator, final AckFactory ackFactory, final FillFactory fillFactory) {
        contractToOrderHandler.computeIfAbsent(contract, c -> new SimOrderEventHandler(c, feeCalculator, outgoingQueue, ackFactory, fillFactory));
    }

    @Override
    public void onTick(final BDTickView tick) {
        final long exchangeTimeMicros = tick.getExchangeUpdateTimeMicros();
        final SimOrderEventHandler handler = contractToOrderHandler.get(tick.getContract());
        handler.onTick(exchangeTimeMicros, tick);
    }

    @Override
    public void onMarketBook(final BDBookView marketbook) {
        final long exchangeTimeMicros = marketbook.getExchangeUpdateTimeMicros();
        final SimOrderEventHandler handler = contractToOrderHandler.get(marketbook.getContract());
        handler.onMarketBook(exchangeTimeMicros, marketbook);
    }

    @Override
    public void processIncomingOrderQueue(final long exchangeTimeMicros) {
        while (!incomingMessages.isEmpty() && incomingMessages.peek().hasMessageArrivedBy(exchangeTimeMicros)) {
            final DelayedMessage<?> delayedMessage = incomingMessages.poll();
            switch (delayedMessage.getType()) {
                case TradeServerProtocol.Header_NewOrderChar: {
                    final SimulatedOrder simulatedOrder = (SimulatedOrder) delayedMessage.getMessage();
                    final SimOrderEventHandler handler = contractToOrderHandler.get(simulatedOrder.getContract());
                    handler.processNewOrder(delayedMessage.getArrivalTimeMicros(), simulatedOrder);
                    break;
                }
                case TradeServerProtocol.Header_CancelOrderChar: {
                    final SimulatedCancel simulatedCancel = (SimulatedCancel) delayedMessage.getMessage();
                    final SimOrderEventHandler handler = contractToOrderHandler.get(simulatedCancel.getContract());
                    handler.processOrderCancel(delayedMessage.getArrivalTimeMicros(), simulatedCancel);
                    break;
                }
                default:
                    throw new EventNotSupportedException("Unhandled request message");
            }
        }
    }

    @Override
    public void onPreOpening() {
        log.warn("OnPreOpeing");
    }

    @Override
    public void onOpening() {
        log.warn("onOpening");
        contractToOrderHandler.values().stream().forEach(handler -> handler.setSession(
            ExchangeSession.OPENING,
            AuctionFilter.createFilter(Result.ACCEPT, Result.DENY)));
    }

    @Override
    public void onOpeningFreeze() {
        log.warn("onOpeningFreeze");
        contractToOrderHandler.values().stream().forEach(handler -> handler.setSession(
            ExchangeSession.OPENING_FREEZE,
            AuctionFreezeFilter.createFilter(Result.ACCEPT, Result.DENY)));
    }

    @Override
    public void onAuctionExecution() {
        log.warn("onAuctionExecution");
        contractToOrderHandler.values().stream().forEach(handler -> handler.clearFilter());
    }

    @Override
    public void onContinuousTrading() {
        log.warn("onContinuousTrading");
        contractToOrderHandler.values().stream().forEach(handler -> handler.clearFilter());
    }

    @Override
    public void onClosing() {
        log.warn("onClosing");
        contractToOrderHandler.values().stream().forEach(handler -> handler.setSession(
            ExchangeSession.CLOSING,
            AuctionFilter.createFilter(Result.ACCEPT, Result.DENY)));
    }

    @Override
    public void onClosingFreeze() {
        log.warn("onClosingFreeze");
        contractToOrderHandler.values().stream().forEach(handler -> handler.setSession(
            ExchangeSession.CLOSING_FREEZE,
            AuctionFreezeFilter.createFilter(Result.ACCEPT, Result.DENY)));
    }

}
