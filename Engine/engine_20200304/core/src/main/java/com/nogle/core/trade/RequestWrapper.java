package com.nogle.core.trade;

import java.nio.ByteBuffer;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.TimeInForce;
import com.timelinecapital.core.execution.factory.RequestInstance;

public class RequestWrapper {

    private long clOrdId;
    private long sourceTime;
    private long commitTime;
    private long handoverTime;
    private TradeEvent event;
    private ExecRequestType requestType;

    private ByteBuffer buffer;
    // private byte[] binaryData;

    public RequestWrapper() {
    }

    private void onOrderEntry(final RequestInstance entry) {
        final NewOrderEncoder entryEncoder = entry.getEntryEncoder();
        entryEncoder.timeInForce(TimeInForce.get(entry.getTimeCondition().getCode()));
        entryEncoder.side(com.timelinecapital.commons.binary.Side.get(entry.getSide().getCode()));
        entryEncoder.clOrdId(entry.getClOrderId());
        entryEncoder.orderQty(entry.getQuantity());
        entryEncoder.price(entry.getPrice());
        entryEncoder.positionPolicy(entry.getPositionPolicy().getPolicyCode());
        entryEncoder.positionMax(entry.getPositionMax());
        buffer = entryEncoder.buffer().byteBuffer();
        // final NewOrderParser entryEncoder = entry.getEntryEncoder();
        // entryEncoder.setTimeCondition(entry.getTimeCondition().getCode());
        // entryEncoder.setSide(entry.getSide().getCode());
        // entryEncoder.setClOrdId(entry.getClOrderId());
        // entryEncoder.setQty(entry.getQuantity());
        // entryEncoder.setPrice(entry.getPrice());
        // entryEncoder.setPositionPolicy(entry.getPositionPolicy().getPolicyCode());
        // entryEncoder.setPositionmax(entry.getPositionMax());
        // binaryData = entryEncoder.getData();
    }

    private void onOrderCancel(final RequestInstance cancel) {
        final CancelOrderEncoder cancelEncoder = cancel.getCancelEncoder();
        cancelEncoder.clOrdId(cancel.getClOrderId());
        buffer = cancelEncoder.buffer().byteBuffer();
        // final CancelOrderParser cancelEncoder = cancel.getCancelEncoder();
        // cancelEncoder.setClOrdId(cancel.getClOrderId());
        // binaryData = cancelEncoder.getData();
    }

    public final void initFrom(final RequestInstance request, final TradeEvent event) {
        this.clOrdId = request.getClOrderId();
        this.requestType = request.getRequestType();
        switch (requestType) {
            case OrderEntry:
                onOrderEntry(request);
                break;
            case OrderCancel:
                onOrderCancel(request);
                break;
            case OrderModify:
                break;
        }
        this.sourceTime = request.getSourceEventTime();
        this.commitTime = request.getEventTime();
        this.event = event;
        this.handoverTime = TradeClock.getCurrentMicrosOnly();
    }

    public ByteBuffer getBinary() {
        return buffer;
    }

    // public byte[] getData() {
    // return binaryData;
    // }

    public ExecRequestType getRequestType() {
        return requestType;
    }

    public final long getClOrdId() {
        return this.clOrdId;
    }

    public final long getSourceTime() {
        return sourceTime;
    }

    public final long getCommitTime() {
        return commitTime;
    }

    public final long getHandoverTime() {
        return handoverTime;
    }

    public final TradeEvent getTradeEvent() {
        return event;
    }

}
