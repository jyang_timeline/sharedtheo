package com.nogle.core.trade;

import com.nogle.core.event.TradeEvent;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;

public interface SchedulableOrder {

    void enqueue(long clOrdId, Side side, long quantity, double price, PositionPolicy positionPolicy, long positionMax, TradeEvent tradeEventHandler);

    void dequeue(Side side, long commitMicros);

    int getNoOrders(Side side);

}
