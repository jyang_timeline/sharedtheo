package com.nogle.core.trade;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.core.exception.EventNotSupportedException;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public final class SimpleExchange extends SimulatedExchange {
    private static final Logger log = LogManager.getLogger(SimulatedExchange.class);

    private final Map<Contract, SimulatedOrderBook> symbolToOrderBook = new HashMap<>();

    public SimpleExchange(final String exchangeName, final long latencyInMicros, final long iocLatencyInMicros, final Queue<DelayedMessage<?>> outgoingMessages) {
        super(exchangeName, latencyInMicros, iocLatencyInMicros, outgoingMessages);
    }

    @Override
    public void registerSymbol(final Contract contract, final FeeCalculator feeCalculator, final AckFactory ackFactory, final FillFactory fillFactory) {
        SimulatedOrderBook simulatedOrderBook = symbolToOrderBook.get(contract);
        if (simulatedOrderBook == null) {
            simulatedOrderBook = new SimulatedOrderBook(contract, feeCalculator, latencyInMicros, outgoingMessages, ackFactory, fillFactory);
            symbolToOrderBook.put(contract, simulatedOrderBook);
        }
    }

    @Override
    public void onTick(final BDTickView tick) {
        final SimulatedOrderBook simulatedOrderBook = symbolToOrderBook.get(tick.getContract());
        if (simulatedOrderBook != null) {
            simulatedOrderBook.onTick(tick);
        }
    }

    @Override
    public void onMarketBook(final BDBookView marketbook) {
        final SimulatedOrderBook simulatedOrderBook = symbolToOrderBook.get(marketbook.getContract());
        if (simulatedOrderBook != null) {
            simulatedOrderBook.onMarketBook(marketbook);
        }
    }

    @Override
    public void processIncomingOrderQueue(final long exchangeTimeMicros) {
        while (!incomingMessages.isEmpty() && incomingMessages.peek().hasMessageArrivedBy(exchangeTimeMicros)) {
            final DelayedMessage<?> delayedMessage = incomingMessages.poll();
            switch (delayedMessage.getType()) {
                case TradeServerProtocol.Header_NewOrderChar: {
                    final SimulatedOrder simulatedOrder = (SimulatedOrder) delayedMessage.getMessage();
                    final SimulatedOrderBook simulatedOrderBook = symbolToOrderBook.get(simulatedOrder.getContract());
                    if (simulatedOrderBook != null) {
                        simulatedOrderBook.processNewOrder(delayedMessage.getArrivalTimeMicros(), simulatedOrder);
                    }
                    break;
                }
                case TradeServerProtocol.Header_CancelOrderChar: {
                    final SimulatedCancel simulatedCancel = (SimulatedCancel) delayedMessage.getMessage();
                    final SimulatedOrderBook simulatedOrderBook = symbolToOrderBook.get(simulatedCancel.getContract());
                    if (simulatedOrderBook != null) {
                        simulatedOrderBook.processOrderCancel(delayedMessage.getArrivalTimeMicros(), simulatedCancel);
                    }
                    break;
                }
                default:
                    throw new EventNotSupportedException("Unhandled request message");
            }
        }
    }

    @Override
    public void onPreOpening() {
        log.warn("OnPreOpeing");
    }

    @Override
    public void onOpening() {
        log.warn("onOpening");
    }

    @Override
    public void onOpeningFreeze() {
        log.warn("onOpeningFreeze");
    }

    @Override
    public void onAuctionExecution() {
        log.warn("onAuctionExecution");
    }

    @Override
    public void onContinuousTrading() {
        log.warn("onContinuousTrading");
    }

    @Override
    public void onClosing() {
        log.warn("onClosing");
    }

    @Override
    public void onClosingFreeze() {
        log.warn("onClosingFreeze");
    }

}
