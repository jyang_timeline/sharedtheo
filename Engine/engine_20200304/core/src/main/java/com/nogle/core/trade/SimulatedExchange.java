package com.nogle.core.trade;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.core.scheduler.SimulatedTimer;
import com.nogle.core.scheduler.TimeChangeListener;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Fill;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.scheduler.ExchangeScheduleService;
import com.timelinecapital.core.trade.simulation.ExchangeEventQueue;
import com.timelinecapital.core.types.SessionIdentifier;
import com.timelinecapital.strategy.state.ExchangeSession;

/**
 * SimulatedExchange serves as a fill simulator for a given strategy, so we must pair a unique instance with each Strategy
 *
 * @author Mark
 *
 */
public abstract class SimulatedExchange implements ExchangeScheduleService {

    final Queue<DelayedMessage<?>> incomingMessages = new PriorityQueue<>();
    final Queue<DelayedMessage<?>> outgoingMessages;

    final DelayedQueue outgoingQueue = new DelayedQueue();

    final String exchangeName;
    final long latencyInMicros;
    final long iocLatencyInMicros;

    private final ArrayBlockingQueue<SessionIdentifier> queue = new ArrayBlockingQueue<>(16);
    private long uuid = 0l;

    ExchangeSession currentSession = ExchangeSession.PRE_OPENING;

    private long currentSessionBracketEnd;
    private long nextSessionBracketHead;

    SimulatedExchange(final String exchangeName, final long latencyInMicros, final long iocLatencyInMicros, final Queue<DelayedMessage<?>> outgoingMessages) {
        this.exchangeName = exchangeName;
        this.latencyInMicros = latencyInMicros;
        this.iocLatencyInMicros = iocLatencyInMicros;
        this.outgoingMessages = outgoingMessages;

        SimulatedTimer.getInstance().addTimeChangeListener(new TimeChangeListener() {

            @Override
            public void onChange(final long millis) {
                setCurrentTimeMillis(millis);
            }
        });
    }

    public final String getExchange() {
        return exchangeName;
    }

    public abstract void registerSymbol(final Contract contract, final FeeCalculator feeCalculator, final AckFactory ackFactory, final FillFactory fillFactory);

    public abstract void processIncomingOrderQueue(long exchangeTimeMicros);

    public abstract void onTick(BDTickView tick);

    public abstract void onMarketBook(BDBookView marketbook);

    public void buildSessions(final List<SessionIdentifier> sessions) {
        queue.clear();
        queue.addAll(sessions);

        final SessionIdentifier first = queue.poll();
        currentSession = first.getSession();
        currentSessionBracketEnd = first.getInterval().getEndMillis();

        final SessionIdentifier second = queue.peek();
        nextSessionBracketHead = second.getInterval().getStartMillis();
    }

    private void setCurrentTimeMillis(final long currentMillis) {
        final ExchangeSession session = getSession(currentMillis);
        if (session != currentSession) {
            TradeClock.setCurrentMillis(currentMillis);
            doAction(session);
            currentSession = session;
        }
    }

    private void doAction(final ExchangeSession session) {
        switch (session) {
            case PRE_OPENING:
                onPreOpening();
                break;
            case OPENING:
                onOpening();
                break;
            case OPENING_FREEZE:
                onOpeningFreeze();
                break;
            case CORE_TRADING:
                onContinuousTrading();
                break;
            case CLOSING:
                onClosing();
                break;
            case CLOSING_FREEZE:
                onClosingFreeze();
                break;
            case CLOSED:
            case NO_ORDERS:
                break;
        }
    }

    private ExchangeSession getSession(final long currentMillis) {
        if (currentMillis > currentSessionBracketEnd || currentMillis > nextSessionBracketHead) {
            final SessionIdentifier next = queue.poll();
            if (next == null) {
                currentSessionBracketEnd = TradeClock.getTradingDayClosingTime();
                return ExchangeSession.CLOSED;
            }
            currentSessionBracketEnd = next.getInterval().getEndMillis();

            final SessionIdentifier next2 = queue.peek();
            nextSessionBracketHead = (next2 == null) ? currentSessionBracketEnd : next2.getInterval().getStartMillis();
            return next.getSession();
        }
        return currentSession;
    }

    // private boolean inRange(final long low, final long high, final long current) {
    // return ((current - low) * (current - high)) <= 0;
    // }

    final void onIncomingIOCOrder(final long orderSendTimeMicros, final SimulatedOrder simulatedOrder) {
        incomingMessages.add(new DelayedMessage<>(++uuid, orderSendTimeMicros + iocLatencyInMicros, TradeServerProtocol.Header_NewOrderChar, simulatedOrder));
    }

    final void onIncomingOrder(final long orderSendTimeMicros, final SimulatedOrder simulatedOrder) {
        incomingMessages.add(new DelayedMessage<>(++uuid, orderSendTimeMicros + latencyInMicros, TradeServerProtocol.Header_NewOrderChar, simulatedOrder));
    }

    final void onIncomingCancel(final long orderSendTimeMicros, final SimulatedCancel simulatedCancel) {
        incomingMessages.add(new DelayedMessage<>(++uuid, orderSendTimeMicros + latencyInMicros, TradeServerProtocol.Header_CancelOrderChar, simulatedCancel));
    }

    public final boolean hasIncomingMessage(final long exchangeTimeMicros) {
        return !incomingMessages.isEmpty() && incomingMessages.peek().hasMessageArrivedBy(exchangeTimeMicros);
    }

    public class DelayedQueue implements ExchangeEventQueue {

        private final long orderPriority = 0;
        private final long rejectPriority = 1;
        private final long fillPriority = 2;
        private final long cancelPriority = 3;

        private long uuid = 0;

        @Override
        public void queueDelayedOrderAck(final long ackMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + orderPriority, TradeServerProtocol.Header_OrderAckChar, clOrdId));
        }

        @Override
        public void queueDelayedModAck(final long ackMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + orderPriority, TradeServerProtocol.Header_ModifyAckChar, clOrdId));
        }

        @Override
        public void queueDelayedCancelAck(final long ackMicros, final Ack ack) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + cancelPriority, TradeServerProtocol.Header_CancelAckChar, ack));
        }

        @Override
        public void queueFill(final long fillMicros, final Fill fill) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, fillMicros + latencyInMicros, TradeServerProtocol.Header_FillChar, fill));
        }

        @Override
        public void queueDelayedFill(final long fillMicros, final Fill fill) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, fillMicros + latencyInMicros + fillPriority, TradeServerProtocol.Header_FillChar, fill));
        }

        @Override
        public void queueDelayedMissedAck(final long ackMicros, final Ack ack) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + cancelPriority, TradeServerProtocol.Header_CancelAckChar, ack));
        }

        @Override
        public void queueDelayedIOCFill(final long fillMicros, final Fill fill) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, fillMicros + fillPriority, TradeServerProtocol.Header_FillChar, fill));
        }

        @Override
        public void queueDelayedOrderReject(final long rejectMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, rejectMicros + latencyInMicros + rejectPriority, TradeServerProtocol.Header_OrderRejectChar, clOrdId));
        }

        @Override
        public void queueDelayedModReject(final long rejectMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, rejectMicros + latencyInMicros + rejectPriority, TradeServerProtocol.Header_ModifyRejectChar, clOrdId));
        }

        @Override
        public void queueDelayedCancelReject(final long rejectMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, rejectMicros + latencyInMicros + rejectPriority, TradeServerProtocol.Header_CancelRejectChar, clOrdId));
        }

    }

    public enum ExecutionEvent {
        ORDER_INSERT(0),
        ORDER_CANCEL(1),
        MARKETBOOK_MATCHING(2),
        TRADE_MATCHING(3),
        ;

        private int seq;

        private ExecutionEvent(final int seq) {
            this.seq = seq;
        }

        public int getSeq() {
            return seq;
        }
    }

}
