package com.nogle.core.trade;

import static com.nogle.strategy.types.Side.BUY;
import static com.nogle.strategy.types.Side.SELL;

import java.util.Queue;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.types.DelayedMessage;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Fill;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class SimulatedOrderBook {
    private static final long fillPriceBanningDelayMicros = NogleTimeUnit.SECONDS.toMicros(30);

    private final long latencyInMicros;

    private final long orderPriority = 0;
    private final long rejectPriority = 1;
    private final long fillPriority = 2;
    private final long cancelPriority = 3;

    private final Queue<DelayedMessage<?>> outgoingMessages;
    private final SimulatedOrderBookBySide buyOrderBook;
    private final SimulatedOrderBookBySide sellOrderBook;

    SimulatedOrderBook(final Contract symbol, final FeeCalculator feeCalculator, final long latencyInMicros, final Queue<DelayedMessage<?>> outgoingMessages,
        final AckFactory ackFactory, final FillFactory fillFactory) {
        this.latencyInMicros = latencyInMicros;
        this.outgoingMessages = outgoingMessages;
        final DelayedQueue delayedQueue = new DelayedQueue();
        buyOrderBook = new SimulatedOrderBookBySide(symbol, feeCalculator, BUY, fillPriceBanningDelayMicros, delayedQueue, ackFactory, fillFactory);
        sellOrderBook = new SimulatedOrderBookBySide(symbol, feeCalculator, SELL, fillPriceBanningDelayMicros, delayedQueue, ackFactory, fillFactory);
    }

    public void onTick(final BDTickView trade) {
        final long tradeTimeMicros = NogleTimeUnit.MILLISECONDS.toMicros(trade.getUpdateTimeMillis());
        switch (trade.getType()) {
            case BUYER:
                sellOrderBook.matchOnTrade(tradeTimeMicros, trade);
                break;
            case SELLER:
                buyOrderBook.matchOnTrade(tradeTimeMicros, trade);
                break;
            default:
                break;
        }
    }

    public void onMarketBook(final BDBookView marketBook) {
        final long bookTimeMicros = NogleTimeUnit.MILLISECONDS.toMicros(marketBook.getUpdateTimeMillis());
        buyOrderBook.onBook(bookTimeMicros, marketBook);
        sellOrderBook.onBook(bookTimeMicros, marketBook);
        buyOrderBook.matchOrder(bookTimeMicros);
        sellOrderBook.matchOrder(bookTimeMicros);
    }

    public void processNewOrder(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        switch (simulatedOrder.getSide()) {
            case BUY:
                buyOrderBook.onNewOrder(arrivalTimeMicros, simulatedOrder);
                break;
            case SELL:
                sellOrderBook.onNewOrder(arrivalTimeMicros, simulatedOrder);
                break;
            default:
                throw new IllegalArgumentException("SimulatedOrder had no side.");
        }
    }

    public void processOrderCancel(final long arrivalTimeMicros, final SimulatedCancel simulatedCancel) {
        switch (simulatedCancel.getSide()) {
            case BUY:
                buyOrderBook.onOrderCancel(arrivalTimeMicros, simulatedCancel);
                break;
            case SELL:
                sellOrderBook.onOrderCancel(arrivalTimeMicros, simulatedCancel);
                break;
            default:
                throw new IllegalArgumentException("SimulatedCancel had no side.");
        }
    }

    class DelayedQueue {
        private long uuid = 0;

        void queueDelayedOrderAck(final long ackMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + orderPriority, TradeServerProtocol.Header_OrderAckChar, clOrdId));
        }

        void queueDelayedModAck(final long ackMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + orderPriority, TradeServerProtocol.Header_ModifyAckChar, clOrdId));
        }

        void queueDelayedCancelAck(final long ackMicros, final Ack ack) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + latencyInMicros + cancelPriority, TradeServerProtocol.Header_CancelAckChar, ack));
        }

        void queueDelayedFill(final long fillMicros, final Fill fill) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, fillMicros + latencyInMicros + fillPriority, TradeServerProtocol.Header_FillChar, fill));
        }

        void queueDelayedMissedAck(final long ackMicros, final Ack ack) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, ackMicros + cancelPriority, TradeServerProtocol.Header_CancelAckChar, ack));
        }

        void queueDelayedIOCFill(final long fillMicros, final Fill fill) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, fillMicros + fillPriority, TradeServerProtocol.Header_FillChar, fill));
        }

        void queueDelayedModReject(final long rejectMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, rejectMicros + latencyInMicros + rejectPriority, TradeServerProtocol.Header_ModifyRejectChar, clOrdId));
        }

        void queueDelayedCancelReject(final long rejectMicros, final long clOrdId) {
            outgoingMessages.add(new DelayedMessage<>(++uuid, rejectMicros + latencyInMicros + rejectPriority, TradeServerProtocol.Header_CancelRejectChar, clOrdId));
        }
    }
}
