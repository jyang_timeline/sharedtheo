package com.nogle.core.trade;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.trade.SimulatedOrderBook.DelayedQueue;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FillType;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.util.FeeHelper;

public class SimulatedOrderBookBySide {
    private static final Logger log = LogManager.getLogger(SimulatedOrderBookBySide.class);

    private final Queue<SimulatedOrder> queuedSimOrders = new PriorityQueue<>();
    private final Map<Long, SimulatedOrder> clOrdIdToOrder = new HashMap<>();

    private final AckFactory ackFactory;
    private final FillFactory fillFactory;

    private final Contract symbol;
    private final Side side;
    private final long fillPriceBanningDelayMicros;
    private final DelayedQueue delayedQueue;
    private final FeeCalculator feeCalculator;

    private BDBookView marketBook;
    private long fillPriceResetMicros;

    private double lastPrice = Double.NaN;
    private long lastQty = 0;
    private long hittableQty;
    private long holdingPosition = 0l;

    public SimulatedOrderBookBySide(final Contract symbol, final FeeCalculator feeCalculator, final Side side, final long fillPriceBanningDelayMicros,
        final DelayedQueue delayedQueue, final AckFactory ackFactory, final FillFactory fillFactory) {
        this.symbol = symbol;
        this.feeCalculator = feeCalculator;
        this.side = side;
        this.fillPriceBanningDelayMicros = fillPriceBanningDelayMicros;
        this.delayedQueue = delayedQueue;
        this.ackFactory = ackFactory;
        this.fillFactory = fillFactory;
    }

    public void onNewOrder(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        switch (simulatedOrder.getTimeCondition()) {
            case GFS:
            case GFD:
                delayedQueue.queueDelayedOrderAck(arrivalTimeMicros, simulatedOrder.getClOrdId());
                internalAdd(arrivalTimeMicros, simulatedOrder);
                break;
            case IOC:
                internalAdd(arrivalTimeMicros, simulatedOrder);
                break;
            default:
                break;
        }
    }

    public void onOrderMod(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        if (handleOrderMod(arrivalTimeMicros, simulatedOrder)) {
            delayedQueue.queueDelayedModAck(arrivalTimeMicros, simulatedOrder.getClOrdId());
        } else {
            delayedQueue.queueDelayedModReject(arrivalTimeMicros, simulatedOrder.getClOrdId());
        }
    }

    public void onOrderCancel(final long arrivalTimeMicros, final SimulatedCancel simulatedCancel) {
        if (internalCancel(simulatedCancel.getClOrdId())) {
            final Ack ack = ackFactory.get();
            ack.setAck(simulatedCancel.getContract(), simulatedCancel.getSide(), TimeCondition.GFD, ExecType.CANCEL_ACK, simulatedCancel.getClOrdId());
            delayedQueue.queueDelayedCancelAck(arrivalTimeMicros, ack);
        } else {
            delayedQueue.queueDelayedCancelReject(arrivalTimeMicros, simulatedCancel.getClOrdId());
        }
    }

    public void onBook(final long currentTimeMicros, final BDBookView marketBook) {
        final Side otherSide = side.switchSide();
        final double newPrice = otherSide.getPrice(marketBook);
        final long newQty = otherSide.getQty(marketBook);
        if (newPrice != lastPrice
            || newQty != lastQty
            || currentTimeMicros >= fillPriceResetMicros) {
            hittableQty = otherSide.getQty(marketBook);
            // fillPriceResetMicros gets set when hittableQty is removed
            fillPriceResetMicros = Long.MAX_VALUE;
        }
        lastPrice = newPrice;
        lastQty = newQty;
        this.marketBook = marketBook;
    }

    public void matchOnTrade(final long currentTimeMicros, final BDTickView trade) {
        long fillableQty = trade.getQuantity();
        final double price = trade.getPrice();
        SimulatedOrder order;
        while ((order = queuedSimOrders.peek()) != null && fillableQty > 0) {
            final long orderRef = order.getClOrdId();
            final Contract symbol = order.getContract();
            final Side orderSide = order.getSide();
            final long orderQty = order.getQuantity();
            final double orderPrice = order.getDoublePrice();
            final TimeCondition tif = order.getTimeCondition();

            if (PriceUtils.isInsideOf(orderSide, price, orderPrice)) {
                break;
            }

            if (PriceUtils.isOutsideOf(orderSide, price, orderPrice)) {
                // final OrderFill executionReport;
                final double commission = getSimulatedCommission(orderSide, orderQty, orderPrice);
                // executionReport = new OrderFill(symbol, orderSide, tif, orderRef, orderQty, orderPrice, commission);
                final OrderFill fill = fillFactory.get();
                fill.setFill(symbol, orderSide, tif, orderRef, orderQty, orderPrice, commission);
                queuedSimOrders.poll();
                clOrdIdToOrder.remove(orderRef);
                fillableQty -= orderQty;
                delayedQueue.queueDelayedFill(currentTimeMicros, fill);
                log.debug("Fill: by matchTrade {} {}@{} ", trade.getContract(), trade.getQuantity(), trade.getPrice());
                continue;
            }

            final long priorityQty = order.getPriority();
            if (fillableQty <= priorityQty) {
                log.debug("Within price {}, not getting fill cause trade: {} pos: {}", price, fillableQty, priorityQty);
                break;
            }
            final long qtyToFill = priorityQty + orderQty;
            if (PriceUtils.isInsideOfOrEqualTo(orderSide, orderPrice, price)) {
                // final OrderFill executionReport;
                final OrderFill fill = fillFactory.get();
                if (fillableQty >= qtyToFill) {
                    final double commission = getSimulatedCommission(orderSide, orderQty, orderPrice);
                    // executionReport = new OrderFill(symbol, orderSide, tif, orderRef, orderQty, orderPrice, commission);
                    fill.setFill(symbol, orderSide, tif, orderRef, orderQty, orderPrice, commission);
                    queuedSimOrders.poll();
                    clOrdIdToOrder.remove(orderRef);
                    fillableQty -= orderQty;
                } else {
                    final long filledQty = fillableQty - priorityQty;
                    final double commission = getSimulatedCommission(orderSide, filledQty, orderPrice);
                    // executionReport = new OrderFill(symbol, orderSide, tif, orderRef, filledQty, orderPrice, commission);
                    // executionReport.setRemainingQty(orderQty - filledQty);
                    fill.setFill(symbol, orderSide, tif, orderRef, filledQty, orderPrice, commission);
                    fill.setRemainingQty(orderQty - filledQty);
                    order.fill(filledQty);
                    fillableQty -= filledQty;
                }

                delayedQueue.queueDelayedFill(currentTimeMicros, fill);
                log.debug("Fill: by matchTrade {} {}@{} ", trade.getContract(), trade.getQuantity(), trade.getPrice());
            }
        }

        if (fillableQty > 0) {
            for (final SimulatedOrder simOrder : queuedSimOrders) {
                if (simOrder.getDoublePrice() == price) {
                    if (fillableQty > simOrder.getPriority()) {
                        simOrder.updatePriority(0);
                    } else {
                        simOrder.updatePriority(simOrder.getPriority() - fillableQty);
                    }
                }
            }
        }
    }

    public void matchOrder(final long currentTimeMicros) {
        SimulatedOrder order;
        while ((order = queuedSimOrders.peek()) != null && hittableQty > 0) {
            final long clOrdId = order.getClOrdId();
            final Contract symbol = order.getContract();
            final Side orderSide = order.getSide();
            final long orderQty = order.getQuantity();
            final double orderPrice = order.getDoublePrice();
            final TimeCondition tif = order.getTimeCondition();

            final double priceToCross = side.switchSide().getPrice(marketBook);
            if (PriceUtils.isInsideOf(orderSide, priceToCross, orderPrice)) {
                final long qty = side.getQty(marketBook, orderPrice);
                if (order.getPriority() > qty && qty > 0) {
                    order.updatePriority(qty);
                }
                break;
            }

            if (PriceUtils.isInsideOfOrEqualTo(orderSide, orderPrice, priceToCross)) {
                // final OrderFill executionReport;
                final OrderFill fill = fillFactory.get();
                if (hittableQty >= orderQty) {
                    final double commission = getSimulatedCommission(orderSide, orderQty, orderPrice);
                    // executionReport = new OrderFill(symbol, orderSide, tif, clOrdId, orderQty, orderPrice, commission);
                    fill.setFill(symbol, orderSide, tif, clOrdId, orderQty, orderPrice, commission);
                    queuedSimOrders.poll();
                    clOrdIdToOrder.remove(clOrdId);
                    hittableQty -= orderQty;
                } else {
                    final double commission = getSimulatedCommission(orderSide, hittableQty, orderPrice);
                    // executionReport = new OrderFill(symbol, orderSide, tif, clOrdId, hittableQty, orderPrice, commission);
                    // executionReport.setRemainingQty(orderQty - hittableQty);
                    fill.setFill(symbol, orderSide, tif, clOrdId, hittableQty, orderPrice, commission);
                    fill.setRemainingQty(orderQty - hittableQty);
                    order.fill(hittableQty);
                    order.updatePriority(0);
                    hittableQty = 0;
                }
                fillPriceResetMicros = currentTimeMicros + fillPriceBanningDelayMicros;

                delayedQueue.queueDelayedFill(currentTimeMicros, fill);
                log.debug("Fill: by matchOrder [{}] {} {}@{}/{}@{} {}", marketBook.getSequenceNo(), marketBook.getContract(),
                    marketBook.getBidQty(), marketBook.getBidPrice(), marketBook.getAskQty(), marketBook.getAskPrice(), currentTimeMicros);
            }

        }
    }

    private boolean handleOrderMod(final long fillMicros, final SimulatedOrder simulatedOrder) {
        if (internalCancel(simulatedOrder.getClOrdId())) {
            internalAdd(fillMicros, simulatedOrder);
            return true;
        }
        return false;
    }

    private boolean internalCancel(final long clOrdId) {
        final SimulatedOrder simulatedOrder = clOrdIdToOrder.remove(clOrdId);
        if (simulatedOrder != null) {
            queuedSimOrders.remove(simulatedOrder);
            return true;
        }
        return false;
    }

    private void internalAdd(final long addMicros, final SimulatedOrder simulatedOrder) {
        final long clOrdId = simulatedOrder.getClOrdId();
        final Contract symbol = simulatedOrder.getContract();
        final Side side = simulatedOrder.getSide();
        final double restingPrice = simulatedOrder.getDoublePrice();
        final TimeCondition tif = simulatedOrder.getTimeCondition();
        final double hittablePrice = side.switchSide().getPrice(marketBook);

        if (PriceUtils.isInsideOfOrEqualTo(side, restingPrice, hittablePrice)) {
            final long restingQty = simulatedOrder.getQuantity();
            if (hittableQty > 0) {
                final double fillPrice = PriceUtils.getOutsidePrice(side, restingPrice, hittablePrice);
                if (restingQty > hittableQty) {
                    simulatedOrder.fill(hittableQty);

                    final double commission = getSimulatedCommission(side, hittableQty, fillPrice);
                    final OrderFill fill = fillFactory.get();
                    fill.setFill(symbol, side, tif, clOrdId, hittableQty, fillPrice, commission);
                    fill.setRemainingQty(restingQty - hittableQty);
                    switch (tif) {
                        case GFS:
                        case GFD:
                            delayedQueue.queueDelayedFill(addMicros, fill);
                            break;
                        case IOC:
                            delayedQueue.queueDelayedIOCFill(addMicros, fill);
                        default:
                            break;
                    }

                    hittableQty = 0;
                    log.debug("Fill: {} {} by internalAdd {} {}@{}/{}@{} {}", fillPrice, side, marketBook.getContract(),
                        marketBook.getBidQty(), marketBook.getBidPrice(), marketBook.getAskQty(), marketBook.getAskPrice(), addMicros);
                } else {
                    simulatedOrder.fill(restingQty);

                    final double commission = getSimulatedCommission(side, restingQty, fillPrice);
                    final OrderFill fill = fillFactory.get();
                    fill.setFill(symbol, side, tif, clOrdId, restingQty, fillPrice, commission);

                    switch (tif) {
                        case GFS:
                        case GFD:
                            delayedQueue.queueDelayedFill(addMicros, fill);
                            break;
                        case IOC:
                            delayedQueue.queueDelayedIOCFill(addMicros, fill);
                        default:
                            break;
                    }

                    hittableQty -= restingQty;
                    log.debug("Fill: {} {} by internalAdd {} {}@{}/{}@{} {}", fillPrice, side, marketBook.getContract(),
                        marketBook.getBidQty(), marketBook.getBidPrice(), marketBook.getAskQty(), marketBook.getAskPrice(), addMicros);
                }
            }
        }
        if (simulatedOrder.getQuantity() > 0) {
            switch (simulatedOrder.getTimeCondition()) {
                case GFS:
                case GFD:
                    simulatedOrder.setPosition(side.getQty(marketBook, restingPrice), addMicros);
                    clOrdIdToOrder.put(clOrdId, simulatedOrder);
                    queuedSimOrders.add(simulatedOrder);
                    log.debug("Queued: {} {} {} @ {}", simulatedOrder.getContract(), simulatedOrder.getQuantity(), simulatedOrder.getPrice(), simulatedOrder.getPriority());
                    break;
                case IOC:
                    // final Ack ack = new Ack(simulatedOrder.getContract(), side, tif, ExecType.NEWORDER_ACK, clOrdId);
                    final Ack ack = ackFactory.get();
                    ack.setAck(simulatedOrder.getContract(), side, tif, ExecType.NEWORDER_ACK, clOrdId);
                    delayedQueue.queueDelayedMissedAck(addMicros, ack);
                    log.debug("Missed: {} {} {} by internalAdd {} {}@{}/{}@{}", simulatedOrder.getPrice(), simulatedOrder.getQuantity(), side, marketBook.getContract(),
                        marketBook.getBidQty(), marketBook.getBidPrice(), marketBook.getAskQty(), marketBook.getAskPrice());
                    break;
                default:
                    break;
            }
        }

    }

    private double getSimulatedCommission(final Side side, final long fillQty, final double fillPrice) {
        return feeCalculator.getFee(fillQty, fillPrice, getSimulatedFillType(side, fillQty));
    }

    private FillType getSimulatedFillType(final Side side, final long fillQty) {
        if (FeeHelper.isEquityTrading(symbol.getExchange())) {
            switch (side) {
                case BUY:
                    return FillType.EQT_LONG;
                case SELL:
                    return FillType.EQT_SHORT;
                default:
                    throw new RuntimeException("Not supported");
            }
        }
        if (holdingPosition == 0 || holdingPosition < fillQty) {
            holdingPosition += fillQty;
            return FillType.FUT_OPEN;
        } else {
            holdingPosition -= fillQty;
            return FillType.FUT_CLOSE;
        }
    }

}
