package com.nogle.core.trade;

import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class SimulatedOrderSender implements OrderSender {
    private final ConcurrentLinkedQueue<SimulatedOrder> bidOrderQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<SimulatedOrder> askOrderQueue = new ConcurrentLinkedQueue<>();

    private final Map<Long, TradeEvent> orderIdToTradeEvent;
    private final SimulatedExchange simulatedExchange;
    private final Contract contract;

    private long lastSendingMicros = 0;
    private int bidOrders = 0;
    private int askOrders = 0;

    public SimulatedOrderSender(final Contract contract, final SimulatedExchange simulatedExchange, final Map<Long, TradeEvent> orderIdToTradeEvent) {
        this.simulatedExchange = simulatedExchange;
        this.orderIdToTradeEvent = orderIdToTradeEvent;
        this.contract = contract;
    }

    @Override
    public void sendNewDayOrder(final long clOrdId, final Side side, final long quantity, final double price, final PositionPolicy positionPolicy, final long positionMax,
        final TradeEvent tradeEventHandler, final long sourceMicros, final long commitMicros) {
        final SimulatedOrder simulatedOrder = new SimulatedOrder(contract, side, TimeCondition.GFD, quantity, price, clOrdId, tradeEventHandler);

        long orderSendTimeMicros = TradeClock.getCurrentMicros(); // TODO add internal latency
        if (orderSendTimeMicros <= lastSendingMicros) {
            orderSendTimeMicros = lastSendingMicros + 1;
        }
        lastSendingMicros = orderSendTimeMicros;
        simulatedExchange.onIncomingOrder(orderSendTimeMicros, simulatedOrder);

        orderIdToTradeEvent.put(clOrdId, tradeEventHandler);
    }

    @Override
    public void sendNewIOCOrder(final long clOrdId, final Side side, final long quantity, final double price, final PositionPolicy positionPolicy, final long positionMax,
        final TradeEvent tradeEventHandler, final long sourceMicros, final long commitMicros) {
        final SimulatedOrder simulatedOrder = new SimulatedOrder(contract, side, TimeCondition.IOC, quantity, price, clOrdId, tradeEventHandler);

        final long orderSendTimeMicros = TradeClock.getCurrentMicros(); // TODO add internal latency
        simulatedExchange.onIncomingIOCOrder(orderSendTimeMicros, simulatedOrder);

        orderIdToTradeEvent.put(clOrdId, tradeEventHandler);
    }

    @Override
    public void sendOrderCancel(final long clOrdId, final Side side, final TradeEvent tradeEventHandler, final long sourceMicros, final long commitMicros) {
        final SimulatedCancel simulatedCancel = new SimulatedCancel(clOrdId, contract, side);

        long orderSendTimeMicros = TradeClock.getCurrentMicros(); // TODO add internal latency
        if (orderSendTimeMicros == lastSendingMicros) {
            orderSendTimeMicros++;
        }
        lastSendingMicros = orderSendTimeMicros;
        simulatedExchange.onIncomingCancel(orderSendTimeMicros, simulatedCancel);

        orderIdToTradeEvent.put(clOrdId, tradeEventHandler);
    }

    @Override
    public void enqueue(final long clOrdId, final Side side, final long quantity, final double price, final PositionPolicy positionPolicy, final long positionMax,
        final TradeEvent tradeEventHandler) {
        final SimulatedOrder simulatedOrder = new SimulatedOrder(contract, side, TimeCondition.GFD, quantity, price, clOrdId, tradeEventHandler);
        if (side == Side.BUY) {
            bidOrderQueue.add(simulatedOrder);
            bidOrders++;
        } else {
            askOrderQueue.add(simulatedOrder);
            askOrders++;
        }
    }

    @Override
    public void dequeue(final Side side, final long eventTimeMicros) {
        SimulatedOrder simulatedOrder;
        switch (side) {
            case BUY:
                while (!bidOrderQueue.isEmpty()) {
                    simulatedOrder = bidOrderQueue.poll();
                    long orderSendTimeMicros = eventTimeMicros;
                    if (orderSendTimeMicros <= lastSendingMicros) {
                        orderSendTimeMicros = lastSendingMicros + 1;
                    }
                    lastSendingMicros = orderSendTimeMicros;

                    simulatedExchange.onIncomingOrder(orderSendTimeMicros, simulatedOrder);
                    orderIdToTradeEvent.put(simulatedOrder.getClOrdId(), simulatedOrder.getTradeEvent());
                }
                bidOrders = 0;
                break;
            case SELL:
                while (!askOrderQueue.isEmpty()) {
                    simulatedOrder = askOrderQueue.poll();
                    long orderSendTimeMicros = eventTimeMicros;
                    if (orderSendTimeMicros <= lastSendingMicros) {
                        orderSendTimeMicros = lastSendingMicros + 1;
                    }
                    lastSendingMicros = orderSendTimeMicros;

                    simulatedExchange.onIncomingOrder(orderSendTimeMicros, simulatedOrder);
                    orderIdToTradeEvent.put(simulatedOrder.getClOrdId(), simulatedOrder.getTradeEvent());
                }
                askOrders = 0;
                break;
            default:
                break;
        }
    }

    @Override
    public int getNoOrders(final Side side) {
        if (side == Side.BUY) {
            return bidOrders;
        } else {
            return askOrders;
        }
    }

}
