package com.nogle.core.trade;

import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;

public class SimulatedTradeProxyListener implements TradeProxyListener {

    private final StrategyUpdater strategyUpdater;

    public SimulatedTradeProxyListener(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public void onFill(final OrderFill fill) {
        strategyUpdater.onFill(fill);
        ExecReportFactory.recycleFill(fill);
    }

    @Override
    public void onMiss(final OrderMiss miss) {
        strategyUpdater.onMiss(miss);
        ExecReportFactory.recycleMiss(miss);
    }

    @Override
    public void onAck(final Ack creation) {
        ExecReportFactory.recycleAck(creation);
    }

    @Override
    public void onReject(final Reject reject) {
        ExecReportFactory.recycleReject(reject);
    }

    @Override
    public void onCancel(final Ack cancel) {
        ExecReportFactory.recycleAck(cancel);
    }

    @Override
    public void rewind() {
    }

    @Override
    public boolean isReady() {
        return true;
    }

}
