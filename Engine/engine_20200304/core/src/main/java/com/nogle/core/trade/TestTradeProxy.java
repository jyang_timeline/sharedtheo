package com.nogle.core.trade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.NodeFactory;
import com.nogle.core.ClientParameters;
import com.nogle.core.EngineStatusView;
import com.nogle.core.exception.ProxyActivationException;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.sender.DelegatoryOrderSender;
import com.timelinecapital.core.factory.EndpointFactory;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.trade.ExecutorInterpretNode;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.util.EndpointHelper;
import com.timelinecapital.core.util.TaskThreadFactory;
import com.timelinecapital.core.util.TradingAccountHelper;
import com.timelinecapital.core.util.TradingHoursHelper;

public class TestTradeProxy extends AbstractTradeProxy {
    private static final Logger log = LogManager.getLogger(LiveTradeProxy.class);

    private static final int socketTimeoutInSec = 3;

    private final Map<String, TradeConnection> endpointToConnection = new HashMap<>();
    private final List<TradeConnection> tradeConnections;
    private final ClientParameters clientParameters;
    private TradeConnection tradeAgent;

    public TestTradeProxy(final ClientParameters clientParameters, final List<TradeConnection> tradeConnections) {
        this.clientParameters = clientParameters;
        this.tradeConnections = tradeConnections;
        open();
    }

    private void open() {
        final String protocol = EngineConfig.getTradeProtocol();

        final TradingAccount account = TradingAccountHelper.getAccountInstance(clientParameters.getUsername());
        final String tradeEndpoint = EndpointFactory.getTradeEndpoints(protocol, clientParameters.getUsername()).getTrade();

        tradeAgent = endpointToConnection.get(tradeEndpoint);
        if (tradeAgent == null) {
            try {
                log.info("[Connection] ******** Trade client endpoint: {} ********", tradeEndpoint);
                final String name = "TCPTrade-" + tradeEndpoint.substring(tradeEndpoint.lastIndexOf(":") + 1);
                final ConnectionProbe probe = new ConnectionProbe(ConnectionType.TRADE, EndpointHelper.getPresentName(ConnectionType.TRADE, tradeEndpoint));

                tradeAgent = new ExecutorInterpretNode(
                    NodeFactory.newNode(
                        TaskThreadFactory.createThreadFactory(name, 0),
                        tradeEndpoint,
                        19999 + EngineConfig.getEngineId()),
                    socketTimeoutInSec,
                    probe,
                    account,
                    OverviewFactory.getPositionImport(account));

                tradeConnections.add(tradeAgent);
                endpointToConnection.put(tradeEndpoint, tradeAgent);
                EngineStatusView.getInstance().addConnectionProbe(probe);
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
                throw new ProxyActivationException("Fail to launch Trade proxy");
            }
        }

        for (final String exchange : EngineConfig.getExchanges()) {
            tradeAgent.updateTradingSessions(TradingHoursHelper.getTradingHours(exchange));
        }
    }

    private boolean authAccount() {
        return true;
    }

    @Override
    protected OrderSender subscribeToSymbol(final int strategyId, final Contract contract, final TradingAccount account, final PositionImport importer) {
        final String exchange = contract.getExchange();
        log.info("*** {} : {} has been initialized ***", exchange, contract);
        final OrderSender orderSender = new DelegatoryOrderSender(contract, tradeAgent);
        return orderSender;
    }

    public TradeConnection getTradeConnection() {
        return tradeAgent;
    }

    @Override
    public boolean connect() {
        log.info("Connecting to trade server.");
        // connect, auth
        if (authAccount()) {
            log.info("Account {} successfully authenticated with trade server.", clientParameters.getUsername());
            return true;
        } else {
            log.error("Account {} failed to authenticate with trade server.", clientParameters.getPassword());
            return false;
        }
    }

}
