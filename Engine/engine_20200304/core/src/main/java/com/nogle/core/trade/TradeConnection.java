package com.nogle.core.trade;

import org.joda.time.Interval;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.strategy.event.TradeEventTranslator;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.ReplaceOrderEncoder;
import com.timelinecapital.core.execution.factory.RequestInstance;

public interface TradeConnection {

    void sendNewOrder(NewOrderEncoder newOrder, long clOrdId, TradeEvent tradeEvent, long eventTimeMicros) throws Exception;

    void sendModifyOrder(ReplaceOrderEncoder modifyOrder, long clOrdId, TradeEvent tradeEvent, long eventTimeMicros) throws Exception;

    void sendOrderCancel(CancelOrderEncoder cancelOrder, long clOrdId, TradeEvent tradeEvent, long eventTimeMicros) throws Exception;

    void forceCancel(long clOrdId);

    default void sendNewOrder(final RequestInstance orderEntryObject, final long clOrdId, final TradeEvent tradeEvent) {
    }

    default void sendNewOrder(final TradeEventTranslator translator, final long clOrdId, final TradeEvent tradeEvent) {
    }

    default void sendOrderCancel(final RequestInstance orderEntryObject, final long clOrdId, final TradeEvent tradeEvent) {
    }

    default void sendOrderCancel(final TradeEventTranslator translator, final long clOrdId, final TradeEvent tradeEvent) {
    }

    String getAccount();

    void onPreMarket();

    void onRegularMarket();

    void start();

    void stop();

    void ping();

    void sync();

    boolean isConnected();

    void checkConnection();

    void checkSocketThread();

    void updateTradingSessions(Interval[] tradingSessions);

    Interval[] getTradingSessions();

}
