package com.nogle.core.trade;

import com.nogle.core.Proxy;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.TradeAppendix;

public interface TradeProxy extends Proxy {

    OrderViewContainer getOrderView(int strategyId, String threadTag, Contract contract, TradeAppendix tradeAppendix);

}
