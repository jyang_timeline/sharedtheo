package com.nogle.core.trade;

import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.Reject;

public interface TradeProxyListener extends MarketOpenAware {

    void onFill(OrderFill fill);

    void onMiss(OrderMiss miss);

    void onAck(Ack creation);

    void onReject(Reject reject);

    void onCancel(Ack cancelAck);

}
