package com.nogle.core.trade;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.Interval;

import com.nogle.core.event.TradeEvent;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.ReplaceOrderEncoder;
import com.timelinecapital.core.config.EngineConfig;

public class WarmupAgent implements TradeConnection {
    private static final Logger log = LogManager.getLogger();

    private SocketChannel socketChannel;
    private final Queue<byte[]> replyQueue = new LinkedBlockingQueue<>();
    private byte[] data = new byte[128];

    public WarmupAgent() {
    }

    // @Override
    // public byte[] getData() {
    // return replyQueue.poll();
    // }

    // @Override
    // public final NewOrderEncoder getNewOrder(final Contract contract, final TimeCondition tif) {
    // final NewOrderEncoder template = new NewOrderEncoder();
    // template.timeInForce(TimeInForce.get(tif.getCode()));
    // template.symbol(contract.getSymbol());
    // return template;
    // }

    // @Override
    // public final ReplaceOrderEncoder getModifyOrder() {
    // return new ReplaceOrderEncoder();
    // }

    // @Override
    // public CancelOrderEncoder getOrderCancel() {
    // return new CancelOrderEncoder();
    // }

    @Override
    public void sendNewOrder(final NewOrderEncoder newOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        // newOrder.setClOrdId(clOrdId);
        // newOrder.setQty(quantity);
        // newOrder.setPrice(price);
        // newOrder.setPositionPolicy(positionPolicy.getPolicyCode());
        // newOrder.setPositionmax(positionMax);
        // clOrdIdToEvent.put(clOrdId, tradeEvent);
        try {
            data = newOrder.buffer().byteBuffer().array();
            replyQueue.add(data);
            // socketChannel.write(ByteBuffer.wrap(data));
            // socketNode.send(newOrder.getData());
        } catch (final Exception e) {
            // clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void sendModifyOrder(final ReplaceOrderEncoder modifyOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
    }

    @Override
    public void sendOrderCancel(final CancelOrderEncoder cancelOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        cancelOrder.clOrdId(clOrdId);
        // clOrdIdToEvent.put(clOrdId, tradeEvent);
        try {
            data = cancelOrder.buffer().byteBuffer().array();
            replyQueue.add(data);
            // socketChannel.write(ByteBuffer.wrap(data));
            // socketNode.send(cancelOrder.getData());
        } catch (final Exception e) {
            // clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void forceCancel(final long clOrdId) {
    }

    @Override
    public String getAccount() {
        return StringUtils.EMPTY;
    }

    @Override
    public void onPreMarket() {
    }

    @Override
    public void onRegularMarket() {
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void start() {
        try {
            socketChannel = SocketChannel.open();
            socketChannel.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
            socketChannel.socket().connect(new InetSocketAddress("127.0.0.1", 19999 + EngineConfig.getEngineId()), 3000);
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop() {
        try {
            socketChannel.close();
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void ping() {
    }

    @Override
    public void sync() {
    }

    @Override
    public void checkConnection() {
    }

    @Override
    public void updateTradingSessions(final Interval[] tradingSessions) {
    }

    @Override
    public Interval[] getTradingSessions() {
        return null;
    }

    // @Override
    // public void clearData() {
    // // TODO Auto-generated method stub
    //
    // }

    @Override
    public void checkSocketThread() {
        // TODO Auto-generated method stub

    }

}
