package com.nogle.core.types;

public enum CommandPrivilege {
    System(0),
    Model(1),
    Self(2);

    private int intValue;

    CommandPrivilege(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public boolean isHigherOrEqual(final CommandPrivilege privilege) {
        if (this.getIntValue() <= privilege.getIntValue()) {
            return true;
        }
        return false;
    }

    public boolean isHigher(final CommandPrivilege privilege) {
        if (this.getIntValue() < privilege.getIntValue()) {
            return true;
        }
        return false;
    }

}
