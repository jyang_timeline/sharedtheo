package com.nogle.core.types;

public class DelayedMessage<T> implements Comparable<DelayedMessage<T>> {
    private final long uuid;
    private final long arrivalTimeMicros;
    private final char type;
    private final T message;

    public DelayedMessage(final long uuid, final long arrivalTimeMicros, final char type, final T message) {
        this.arrivalTimeMicros = arrivalTimeMicros;
        this.type = type;
        this.message = message;
        this.uuid = uuid;
    }

    public boolean hasMessageArrivedBy(final long currentMicros) {
        return currentMicros >= arrivalTimeMicros;
    }

    public long getArrivalTimeMicros() {
        return arrivalTimeMicros;
    }

    public char getType() {
        return type;
    }

    public T getMessage() {
        return message;
    }

    @Override
    public int compareTo(final DelayedMessage<T> o) {
        final int result = Long.compare(arrivalTimeMicros, o.arrivalTimeMicros);
        if (result == 0) {
            return Long.compare(uuid, o.uuid);
        }
        return result;
    }

}
