package com.nogle.core.types;

public interface FeeCalculator {

    double getFee(final long quantity, final double price, final FillType fillType);

}
