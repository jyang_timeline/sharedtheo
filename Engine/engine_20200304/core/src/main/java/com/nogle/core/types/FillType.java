package com.nogle.core.types;

public enum FillType {
    FUT_OPEN('O') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition += fillQty;
        }
    },
    FUT_CLOSE('C') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition -= fillQty;
        }
    },
    FUT_CLOSEOVERNIGHT('Y') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition -= fillQty;
        }
    },

    EQT_LONG('L') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition += fillQty;
        }
    },
    EQT_SHORT('S') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition -= fillQty;
        }
    },
    EQT_MARGIN_LONG('N') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition += fillQty;
        }
    },
    EQT_MARGIN_SHORT('H') {
        @Override
        public long updateHoldingPosition(long holdingPosition, final long fillQty) {
            return holdingPosition -= fillQty;
        }
    },
    ;

    public abstract long updateHoldingPosition(long holdingPosition, long fillQty);

    private byte code;

    FillType(final char code) {
        this.code = (byte) code;
    }

    static final FillType[] FILLTYPES = new FillType[256];
    static {
        for (final FillType tp : values()) {
            FILLTYPES[tp.getCode()] = tp;
        }
    }

    public byte getCode() {
        return code;
    }

    public static FillType decode(final byte code) {
        return FILLTYPES[code];
    }

}
