package com.nogle.core.types;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.TickCalculator;

public class FixedTickCalculator implements TickCalculator {
    private final double tickSize;
    private final double halfTickSize;
    private final double contractUnit;
    private final double priceToTickRatio;

    public FixedTickCalculator(final double tickSize, final double contractUnit) {
        this.tickSize = tickSize;
        halfTickSize = tickSize / 2;
        this.contractUnit = contractUnit;
        priceToTickRatio = 1 / tickSize;
    }

    @Override
    public double getTickSize() {
        return tickSize;
    }

    @Override
    public double toPrecisePrice(final double price) {
        return (int) ((price + halfTickSize) * priceToTickRatio) * tickSize;
    }

    @Override
    public double toPrice(final double ticks) {
        return ticks * tickSize;
    }

    @Override
    public double toValue(final long quantity, final double price) {
        return quantity * price * contractUnit;
    }

    @Override
    public double roundUpToNearestTick(final double price) {
        return Math.ceil(price * priceToTickRatio) * tickSize;
    }

    @Override
    public double roundDownToNearestTick(final double price) {
        return Math.floor(price * priceToTickRatio) * tickSize;
    }

    @Override
    public double roundToNearestTick(final double price) {
        return Math.round(price * priceToTickRatio) * tickSize;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    public static void main(final String[] args) {
        final FixedTickCalculator tickCalc = new FixedTickCalculator(0.005, 5);
        final double price = 5001.0500001;
        System.out.println(price);
        System.out.println(tickCalc.toPrecisePrice(price));

        final double a1 = 0.1;
        final double a2 = 0.2;
        final ByteBuffer buffer2 = ByteBuffer.wrap(new byte[8]);
        buffer2.putDouble(a1 + a2);
        System.out.println(buffer2.array());

        final double a3 = tickCalc.toPrecisePrice(a1 + a2);
        buffer2.rewind();
        buffer2.putDouble(a3);
        System.out.println(buffer2.array());

        buffer2.rewind();
        final double a4 = buffer2.getDouble(0);
        System.out.println(a4);

        final double a = 0.3;
        final ByteBuffer buffer = ByteBuffer.wrap(new byte[8]);
        buffer.putDouble(a);
        System.out.println(buffer.array());

    }

}
