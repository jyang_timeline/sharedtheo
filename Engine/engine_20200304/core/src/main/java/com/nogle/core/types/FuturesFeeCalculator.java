package com.nogle.core.types;

import com.nogle.strategy.types.TickCalculator;

public final class FuturesFeeCalculator implements FeeCalculator {

    private final double floatingOpenFee;
    private final double floatingCloseFee;
    private final double floatingCloseBeforeFee;
    private final double fixedOpenFee;
    private final double fixedCloseFee;
    private final double fixedCloseBeforeFee;
    private TickCalculator tickCalculator = null;

    public FuturesFeeCalculator(
        final double floatingOpenFee, final double floatingCloseFee, final double floatingCloseBeforeFee,
        final double fixedOpenFee, final double fixedCloseFee, final double fixedCloseBeforeFee,
        final TickCalculator tickCalculator) {
        this.floatingOpenFee = floatingOpenFee;
        this.floatingCloseFee = floatingCloseFee;
        this.floatingCloseBeforeFee = floatingCloseBeforeFee;
        this.fixedOpenFee = fixedOpenFee;
        this.fixedCloseFee = fixedCloseFee;
        this.fixedCloseBeforeFee = fixedCloseBeforeFee;
        this.tickCalculator = tickCalculator;
    }

    @Override
    public double getFee(final long quantity, final double price, final FillType fillType) {
        switch (fillType) {
            case FUT_OPEN:
                return getFixedFee(quantity, fixedOpenFee) + getFloatingFee(quantity, price, floatingOpenFee);
            case FUT_CLOSE:
                return getFixedFee(quantity, fixedCloseFee) + getFloatingFee(quantity, price, floatingCloseFee);
            case FUT_CLOSEOVERNIGHT:
                return getFixedFee(quantity, fixedCloseBeforeFee) + getFloatingFee(quantity, price, floatingCloseBeforeFee);
            default:
                return 0;
        }
    }

    private double getFloatingFee(final long quantity, final double price, final double feePercentage) {
        return tickCalculator.toValue(quantity, price) * feePercentage;
    }

    private double getFixedFee(final long quantity, final double fee) {
        return quantity * fee;
    }

}
