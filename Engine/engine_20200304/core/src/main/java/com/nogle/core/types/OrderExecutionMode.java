package com.nogle.core.types;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum OrderExecutionMode {
    DEFAULT(0),
    EAGER(1);

    private int intValue;
    private static final Map<Integer, OrderExecutionMode> intToTypeMap;

    static {
        final Map<Integer, OrderExecutionMode> table = new HashMap<>();
        for (final OrderExecutionMode mode : OrderExecutionMode.values()) {
            table.put(mode.getIntValue(), mode);
        }
        intToTypeMap = Collections.unmodifiableMap(table);
    }

    OrderExecutionMode(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public static OrderExecutionMode fromInt(final int i) {
        return intToTypeMap.get(i);
    }

}
