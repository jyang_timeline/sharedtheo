package com.nogle.core.types;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum RejectReason {

    X('X', "Exchange issues"),
    U('U', "Issue not specified (Unknown)"),
    T('T', "Market is closed/paused (Time)"),
    S('S', "Self-trading prevention"),
    P('P', "Position management issue"),
    O('O', "Order not found"),
    N('N', "Network issue - Production environment"),
    M('M', "Margin issues"),
    L('L', "Limitation of request"),
    I('I', "Instrument issue"),
    F('F', "Invalid Field of request"),
    C('C', "Connection issue - Exchange/Broker"),
    B('B', "Broker API issues"),
    A('A', "Account/Authentication issue");

    private char c;
    private String description;
    private static final Map<Character, RejectReason> charToReasonMap;

    static {
        final Map<Character, RejectReason> table = new HashMap<>();
        for (final RejectReason term : RejectReason.values()) {
            table.put(term.getCharValue(), term);
        }
        charToReasonMap = Collections.unmodifiableMap(table);
    }

    RejectReason(final char c, final String description) {
        this.c = c;
        this.description = description;
    }

    public char getCharValue() {
        return c;
    }

    public String getDescription() {
        return description;
    }

    public static RejectReason fromChar(final char c) {
        return charToReasonMap.get(c);
    }

}
