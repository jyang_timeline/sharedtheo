package com.nogle.core.types;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;

public class SimulatedCancel {
    private final long clOrdId;
    private final Contract contract;
    private final Side side;

    public SimulatedCancel(final long clOrdId, final Contract contract, final Side side) {
        this.clOrdId = clOrdId;
        this.contract = contract;
        this.side = side;
    }

    public long getClOrdId() {
        return clOrdId;
    }

    public Contract getContract() {
        return contract;
    }

    public Side getSide() {
        return side;
    }

}

