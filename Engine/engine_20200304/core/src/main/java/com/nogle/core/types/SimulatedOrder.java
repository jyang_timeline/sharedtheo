package com.nogle.core.types;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.core.util.ExchangeExecutionUtils;

public class SimulatedOrder implements Comparable<SimulatedOrder> {

    private final Contract contract;
    private final Side side;
    private final TimeCondition tif;
    private final long clOrdId;
    private final TradeEvent tradeEvent;

    private final Price price;
    private final double doublePrice;
    private final long mantissaPrice;

    private boolean isReestimationRequired;
    private long quantity;
    private long priority;

    private long enqueuedMicros;

    private String fullDesc;

    public SimulatedOrder(final Contract contract, final Side side, final TimeCondition tif, final long quantity, final double px, final long clOrdId,
        final TradeEvent tradeEvent) {
        this.contract = contract;
        this.side = side;
        this.tif = tif;
        this.quantity = quantity;
        this.clOrdId = clOrdId;
        this.tradeEvent = tradeEvent;

        this.doublePrice = px;
        this.mantissaPrice = PriceUtils.toMantissa(px, contract.getTickCalculator().getTickSize());
        price = ExchangeExecutionUtils.ofPrice();
        this.price.setDouble(doublePrice);
        this.price.setMantissa(mantissaPrice);

        createDesc();
    }

    private void createDesc() {
        fullDesc = clOrdId + ":" + contract + "," + side + "," + quantity + "@" + doublePrice;
    }

    public Contract getContract() {
        return contract;
    }

    public Side getSide() {
        return side;
    }

    public TimeCondition getTimeCondition() {
        return tif;
    }

    public long getClOrdId() {
        return clOrdId;
    }

    public TradeEvent getTradeEvent() {
        return tradeEvent;
    }

    public Price getPrice() {
        return price;
    }

    public double getDoublePrice() {
        return doublePrice;
    }

    public long getMantissaPrice() {
        return mantissaPrice;
    }

    public long getQuantity() {
        return quantity;
    }

    public boolean isReestimationRequired() {
        return isReestimationRequired;
    }

    public long getEnqueuedMicros() {
        return enqueuedMicros;
    }

    public void setPosition(final long position, final long enqueuedMicros) {
        this.setPriority(position, position == 0);
        this.enqueuedMicros = enqueuedMicros;
    }

    public void updatePriority(final long priority) {
        this.setPriority(priority, false);
    }

    private void setPriority(final long priority, final boolean isReestimationRequired) {
        this.priority = priority;
        this.isReestimationRequired = isReestimationRequired;
    }

    public long getPriority() {
        return priority;
    }

    public void fill(final long qty) {
        quantity -= qty;
    }

    @Override
    public int compareTo(final SimulatedOrder other) {
        final double thisPrice = doublePrice;
        final double thatPrice = other.doublePrice;
        if (thisPrice == thatPrice) {
            return Long.compare(priority, other.priority);
        }
        return side.getQuantityDelta() * Double.compare(thatPrice, thisPrice);
    }

    @Override
    public String toString() {
        return fullDesc;
    }

}
