package com.nogle.core.types;

import java.util.Map;

import com.nogle.strategy.types.Side;
import com.timelinecapital.core.types.CommissionInfo;

public class StockFeeCalculator implements FeeCalculator {

    private final double ratioBuy;
    private final double ratioSell;

    public StockFeeCalculator(final Map<Side, CommissionInfo> commissionInfo) {
        ratioBuy = commissionInfo.get(Side.BUY).getRatio();
        ratioSell = commissionInfo.get(Side.SELL).getRatio();
    }

    @Override
    public double getFee(final long quantity, final double price, final FillType fillType) {
        switch (fillType) {
            case FUT_OPEN:
            case FUT_CLOSE:
            case FUT_CLOSEOVERNIGHT:
                return 0d;
            case EQT_LONG:
                return getFee(quantity, price, ratioBuy);
            case EQT_SHORT:
                return getFee(quantity, price, ratioSell);
            case EQT_MARGIN_LONG:
            case EQT_MARGIN_SHORT:
                return 0d;
            default:
                return 0d;
        }
    }

    private double getFee(final long quantity, final double price, final double ratio) {
        return quantity * price * ratio;
    }

}
