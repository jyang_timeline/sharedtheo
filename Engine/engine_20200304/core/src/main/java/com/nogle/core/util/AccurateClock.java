package com.nogle.core.util;

public interface AccurateClock {

    long getPreciseTime();

}
