package com.nogle.core.util;

import java.util.concurrent.atomic.AtomicLong;

import com.timelinecapital.core.config.EngineConfig;

public class ClientOrderIdGenerator {
    private final AtomicLong seqId;
    private final long surfix;
    private final int strategyId;

    private static final long strategyIdOffset = 1000;
    private static final long sequenceNoOffset = strategyIdOffset * 100000;

    public ClientOrderIdGenerator(final int strategyId) {
        this.strategyId = strategyId;
        this.surfix = strategyId * strategyIdOffset + EngineConfig.getEngineId();
        seqId = new AtomicLong(0);
    }

    public void resetId() {
        seqId.getAndSet(0);
    }

    public long getNextId() {
        return seqId.incrementAndGet() * sequenceNoOffset + surfix;
    }

    public int getStrategyId() {
        return strategyId;
    }

    static public long getSequenceNo(final long clientOrderId) {
        return clientOrderId / sequenceNoOffset;
    }

    static public long getEngineId(final long clientOrderId) {
        return clientOrderId % strategyIdOffset;
    }

    static public long getStrategyId(final long clientOrderId) {
        return (clientOrderId % sequenceNoOffset) / strategyIdOffset;
    }

}
