package com.nogle.core.util;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class JobScheduler {
    private static final Logger log = LogManager.getLogger(JobScheduler.class);

    private static final JobScheduler instance = new JobScheduler();

    private Scheduler scheduler;

    public static JobScheduler getInstance() {
        return instance;
    }

    private JobScheduler() {
        this(3);
    }

    private JobScheduler(final int threadcount) {
        try {
            final Properties props = new Properties();
            props.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
            props.setProperty("org.quartz.threadPool.threadCount", Integer.toString(threadcount));
            props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
            props.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");
            final StdSchedulerFactory factory = new StdSchedulerFactory();
            factory.initialize(props);
            scheduler = factory.getScheduler();
            scheduler.start();

        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public void close() {
        try {
            if (!scheduler.isShutdown()) {
                scheduler.clear();
                scheduler.shutdown();
            }
        } catch (final SchedulerException e) {
            e.printStackTrace();
        }
    }

}
