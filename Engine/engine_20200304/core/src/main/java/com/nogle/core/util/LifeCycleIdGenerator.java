package com.nogle.core.util;

import java.util.concurrent.atomic.AtomicLong;

public class LifeCycleIdGenerator {
    private final AtomicLong seqId;

    public LifeCycleIdGenerator() {
        seqId = new AtomicLong(0);
    }

    public void reset() {
        seqId.set(0);
    }

    public long getNextId() {
        return seqId.incrementAndGet();
    }

    public long getId() {
        return seqId.get();
    }
}
