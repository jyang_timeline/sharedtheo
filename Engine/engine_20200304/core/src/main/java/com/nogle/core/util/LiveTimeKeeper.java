package com.nogle.core.util;

import java.time.Clock;
import java.time.temporal.ChronoField;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeConstants;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.timelinecapital.core.config.EngineConfig;

public class LiveTimeKeeper implements TimeKeeper {

    private static final LiveTimeKeeper INSTANCE = new LiveTimeKeeper();

    private final Clock clock = Clock.systemUTC();
    private DateTime calendarDay = new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    private long calendarDayMillis = calendarDay.getMillis();
    private String day = new DateTime().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);

    private LiveTimeKeeper() {
    }

    public static LiveTimeKeeper getInstance() {
        return INSTANCE;
    }

    @Override
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    @Override
    public void setCurrentTimeMillis(final long currentTimeMillis) {
        return;
    }

    @Override
    public long getCurrentTimeMicros() {
        return clock.instant().getEpochSecond() * 1000000l + clock.instant().get(ChronoField.MICRO_OF_SECOND);
    }

    @Override
    public long getCurrentTimeMicrosOnly() {
        return clock.instant().get(ChronoField.MICRO_OF_SECOND);
    }

    @Override
    public void setCurrentTimeMicros(final long currentTimeMicros) {
        return;
    }

    @Override
    public void onCalendarCheck() {
        calendarDay = new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
        calendarDayMillis = calendarDay.getMillis();
    }

    @Override
    public String getCurrentCalendar() {
        return calendarDay.toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT);
    }

    @Override
    public long getCurrentCalendarDayMillis() {
        return calendarDayMillis;
    }

    @Override
    public String getCurrentTradingDay() {
        return day;
    }

    @Override
    public long getCurrentTradingDayOpenedTime() {
        DateTime today = DateTime.parse(day, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        if (DateTimeConstants.MONDAY == today.getDayOfWeek()) {
            today = today.minusDays(3);
        }
        return today.plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay()).getMillis();
    }

    @Override
    public long getCurrentTradingDayClosingTime() {
        return DateTime.parse(day, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT).plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay()).getMillis();
    }

    @Override
    public void setCurrentTradingDay(final long currentTimeMillis) {
        final DateTimeComparator comparator = DateTimeComparator.getTimeOnlyInstance();
        final int result = comparator.compare(
            EngineConfig.getTradingDaySwitchPoint(),
            new DateTime(currentTimeMillis));
        if (result < 0) {
            day = new DateTime(currentTimeMillis).plusDays(1).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
                .toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        } else {
            day = new DateTime(currentTimeMillis).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
                .toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        }
    }

}
