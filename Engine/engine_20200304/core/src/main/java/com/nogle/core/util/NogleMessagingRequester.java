package com.nogle.core.util;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.format.TagValueDecoder;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.core.config.EngineMode;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.Message;
import com.nogle.messaging.MessageConsumer;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.core.config.EngineConfig;

public abstract class NogleMessagingRequester {
    private static final Logger log = LogManager.getLogger(NogleMessagingRequester.class);

    private final TagValueDecoder decoder;
    private final TagValueEncoder encoder;
    private final Object lock;
    private UUID requestID;
    private boolean waitResponse;

    private final String requestTopic;

    public NogleMessagingRequester(final String requestTopic, final String replyTopic) {
        decoder = new TagValueDecoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
        encoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
        this.requestTopic = requestTopic;
        this.lock = new Object();
        this.requestID = UUID.randomUUID();
        this.waitResponse = false;

        Messenger.subscribe(replyTopic, new MessageConsumer() {

            @Override
            public void onMessage(final Message<?> message) {
                final String reply = new String((byte[]) message.getPayload(), StandardCharsets.UTF_8);
                log.info("recv {}\n", reply);
                synchronized (lock) {
                    decoder.decode(reply);
                    if (requestID.compareTo(UUID.fromString(decoder.get(TradeEngineCommunication.requestIDTag))) != 0) {
                        if (!EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
                            log.warn("Unsolicited reply: requestID={} expect={}\n", decoder.get(TradeEngineCommunication.requestIDTag), requestID);
                        } else {
                            waitResponse = true;
                            log.warn("Unwanted reply: {} <-> {}", decoder.get(TradeEngineCommunication.requestIDTag), requestID);
                        }
                    } else {
                        onReply(decoder);
                        waitResponse = false;
                        log.trace("QueryLock released: {}", lock);
                        lock.notifyAll();
                    }
                }
            }

            @Override
            public void onError(final Message<?> message) {
                return;
            }
        });
    }

    private void sendRequest(final String command, final String parameter) {
        // ++requestID;
        requestID = UUID.randomUUID();
        encoder.clear();
        encoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeRequest);
        encoder.append(TradeEngineCommunication.commandTag, command);
        encoder.append(TradeEngineCommunication.parameterTag, parameter);
        encoder.append(TradeEngineCommunication.requestIDTag, requestID.toString());

        try {
            final String toSend = encoder.compose();
            log.info("sent {}\n", toSend);
            Messenger.send(new BinaryMessage(new MessageHeader(this.requestTopic), toSend.getBytes(StandardCharsets.UTF_8)));
        } catch (final Exception e) {
            log.error("send failed: {}", e);
        }
    }

    public void sendBlockingRequest(final String command, final String parameter, final long timeoutInSeconds) {
        final long timeoutInMillis = timeoutInSeconds * 1000;
        final long waitFrom = System.currentTimeMillis();

        waitResponse = true;
        synchronized (lock) {
            try {
                sendRequest(command, parameter);
                while (System.currentTimeMillis() - waitFrom < timeoutInMillis && waitResponse) {
                    lock.wait(timeoutInMillis);
                }

                if (System.currentTimeMillis() - waitFrom >= timeoutInMillis) {
                    /* Timeout alreay, abort the request. */
                    requestID = UUID.randomUUID();
                }
            } catch (final Exception e) {
                log.error("sent blocking request interruped: {}", e);
            }
        }
    }

    abstract public void onReply(final TagValueDecoder decoder);
}
