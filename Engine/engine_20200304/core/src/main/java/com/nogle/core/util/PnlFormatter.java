package com.nogle.core.util;

import java.text.DecimalFormat;

public class PnlFormatter {
    private static final DecimalFormat df = new DecimalFormat("$0.00");

    public static synchronized String format(final double value) {
        return df.format(value);
    }

}

