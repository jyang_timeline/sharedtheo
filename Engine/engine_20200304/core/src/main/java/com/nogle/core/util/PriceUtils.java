package com.nogle.core.util;

import static com.nogle.core.Constants.DEFAULT_NULL_DOUBLE;

import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.type.Price;

public class PriceUtils {

    public static double getInsidePrice(final Side side, final double price1, final double price2) {
        if (Double.isNaN(price1)) {
            return price2;
        }
        if (Double.isNaN(price2)) {
            return price1;
        }
        return isInsideOf(side, price1, price2) ? price1 : price2;
    }

    public static double getOutsidePrice(final Side side, final double price1, final double price2) {
        if (Double.isNaN(price1)) {
            return price2;
        }
        if (Double.isNaN(price2)) {
            return price1;
        }
        return isOutsideOf(side, price1, price2) ? price1 : price2;
    }

    public static long getOutsidePrice(final Side side, final long price1, final long price2) {
        return isOutsideOf(side, price1, price2) ? price1 : price2;
    }

    public static Price getOutsidePrice(final Side side, final Price price1, final Price price2) {
        return isOutsideOf(side, price1, price2) ? price1 : price2;
    }

    /**
     * for BUY, true if price1 is higher than price2;
     *
     * for SELL, true if price1 is lower than price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isInsideOf(final Side side, final double price1, final double price2) {
        return side.getQuantityDelta(price1 - price2) > 0;
    }

    /**
     * for BUY, true if price1 is higher than price2;
     *
     * for SELL, true if price1 is lower than price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isInsideOf(final Side side, final long price1, final long price2) {
        return side.getQuantityDelta(price1 - price2) > 0;
    }

    public static boolean isInsideOf(final Side side, final Price price1, final Price price2) {
        return side.getQuantityDelta(price1.getMantissa() - price2.getMantissa()) > 0;
    }

    /**
     * for BUY, true if price1 is higher than or equal to price2;
     *
     * for SELL, true if price1 is lower than or equal to price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isInsideOfOrEqualTo(final Side side, final double price1, final double price2) {
        return side.getQuantityDelta(price1 - price2) >= 0;
    }

    /**
     * for BUY, true if price1 is higher than or equal to price2;
     *
     * for SELL, true if price1 is lower than or equal to price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isInsideOfOrEqualTo(final Side side, final long price1, final long price2) {
        return side.getQuantityDelta(price1 - price2) >= 0;
    }

    public static boolean isInsideOfOrEqualTo(final Side side, final Price price1, final Price price2) {
        return side.getQuantityDelta(price1.getMantissa() - price2.getMantissa()) >= 0;
    }

    /**
     * for BUY, true if price1 is lower than price2;
     *
     * for SELL, true if price1 is higher than price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isOutsideOf(final Side side, final double price1, final double price2) {
        return side.getQuantityDelta(price1 - price2) < 0;
    }

    /**
     * for BUY, true if price1 is lower than price2;
     *
     * for SELL, true if price1 is higher than price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isOutsideOf(final Side side, final long price1, final long price2) {
        return side.getQuantityDelta(price1 - price2) < 0;
    }

    public static boolean isOutsideOf(final Side side, final Price price1, final Price price2) {
        return side.getQuantityDelta(price1.getMantissa() - price2.getMantissa()) < 0;
    }

    /**
     * for BUY, true if price1 is lower than or equal to price2;
     *
     * for SELL, true if price1 is higher than or equal to price2
     *
     * @param side
     * @param price1
     * @param price2
     * @return
     */
    public static boolean isOutsideOfOrEqualTo(final Side side, final double price1, final double price2) {
        return side.getQuantityDelta(price1 - price2) <= 0;
    }

    public static double getMidPoint(final QuoteType quoteType, final double bidPrice, final double askPrice) {
        if (quoteType == null) {
            return 0;
        }
        switch (quoteType) {
            case QUOTE:
                return (bidPrice + askPrice) / 2;
            case INVALID_ASK:
            case LIMIT_UP:
                return bidPrice;
            case INVALID_BID:
            case LIMIT_DOWN:
                return askPrice;
            case INVALID:
                // signal invalid
                return DEFAULT_NULL_DOUBLE;
            default:
                // signal invalid
                return DEFAULT_NULL_DOUBLE;
        }
    }

    public static long toMantissa(final double price, final double tickSize) {
        return Math.round((price / tickSize));
    }

}
