package com.nogle.core.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.commons.utils.NogleTimeUnit;
import com.timelinecapital.core.config.EngineConfig;

public class SimulatedTimeKeeper implements TimeKeeper {

    private volatile static ThreadLocal<SimulatedTimeKeeper> THREAD_CLOCK = new ThreadLocal<>();

    private long millis = System.currentTimeMillis();
    private long micros = System.currentTimeMillis() * 1000;
    private String day = new DateTime().toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);

    public SimulatedTimeKeeper() {

    }

    public static SimulatedTimeKeeper get() {
        if (THREAD_CLOCK.get() == null) {
            try {
                THREAD_CLOCK.set(new SimulatedTimeKeeper());
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return THREAD_CLOCK.get();
    }

    @Override
    public long getCurrentTimeMillis() {
        return get().millis;
    }

    @Override
    public void setCurrentTimeMillis(final long currentTimeMillis) {
        get().millis = currentTimeMillis;
        get().micros = NogleTimeUnit.MILLISECONDS.toMicros(currentTimeMillis);
    }

    @Override
    public long getCurrentTimeMicros() {
        return get().micros;
    }

    @Override
    public long getCurrentTimeMicrosOnly() {
        return get().micros % 1000000 * 1000;
    }

    @Override
    public void setCurrentTimeMicros(final long currentTimeMicros) {
        get().micros = currentTimeMicros;
        get().millis = NogleTimeUnit.MICROSECONDS.toMillis(currentTimeMicros);
    }

    @Override
    public void onCalendarCheck() {
        // TODO next calendar day is depending on setCurrentTime
    }

    @Override
    public String getCurrentCalendar() {
        return new DateTime(get().millis).toString(NogleTimeFormatter.ISO_DATE_TIME_FORMAT);
    }

    @Override
    public long getCurrentCalendarDayMillis() {
        return new DateTime(get().millis).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).getMillis();
    }

    @Override
    public String getCurrentTradingDay() {
        return get().day;
    }

    @Override
    public long getCurrentTradingDayOpenedTime() {
        DateTime today = DateTime.parse(get().day, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        if (DateTimeConstants.MONDAY == today.getDayOfWeek()) {
            today = today.minusDays(3);
        } else {
            today = today.minusDays(1);
        }
        return today.plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay()).getMillis();
    }

    @Override
    public long getCurrentTradingDayClosingTime() {
        return DateTime.parse(get().day, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT).plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay()).getMillis();
    }

    @Override
    public void setCurrentTradingDay(final long currentTimeMillis) {
        get().day =
            new DateTime(currentTimeMillis).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).toString(NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
    }

}
