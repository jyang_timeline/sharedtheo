package com.nogle.core.util;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortedList<E extends Comparable<E>> extends AbstractList<E> {

    private final ArrayList<E> internal = new ArrayList<>();

    private Comparator<E> comparator;

    private final Comparator<E> ascComparator = new Comparator<E>() {
        @Override
        public int compare(final E o1, final E o2) {
            return o1.compareTo(o2);
        }
    };

    private final Comparator<E> descComparator = new Comparator<E>() {
        @Override
        public int compare(final E o1, final E o2) {
            return o2.compareTo(o1);
        }
    };

    public void setDescending(final boolean isDescending) {
        if (isDescending) {
            comparator = descComparator;
        } else {
            comparator = ascComparator;
        }
    }

    @Override
    public void add(final int position, final E e) {
        internal.add(e);
        Collections.sort(internal, comparator);
    }

    @Override
    public E remove(final int index) {
        return internal.remove(index);
    }

    @Override
    public E get(final int index) {
        return internal.get(index);
    }

    @Override
    public int size() {
        return internal.size();
    }

}
