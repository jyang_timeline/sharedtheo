package com.nogle.core.util;

public interface TimeKeeper {

    public long getCurrentTimeMillis();

    public void setCurrentTimeMillis(final long currentTimeMillis);

    public long getCurrentTimeMicros();

    public long getCurrentTimeMicrosOnly();

    public void setCurrentTimeMicros(final long currentTimeMicros);

    public void onCalendarCheck();

    public String getCurrentCalendar();

    public long getCurrentCalendarDayMillis();

    public String getCurrentTradingDay();

    public long getCurrentTradingDayOpenedTime();

    public long getCurrentTradingDayClosingTime();

    public void setCurrentTradingDay(final long currentTimeMicros);

}
