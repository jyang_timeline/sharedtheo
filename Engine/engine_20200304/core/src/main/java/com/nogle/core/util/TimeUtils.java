package com.nogle.core.util;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.quartz.CronExpression;
import org.quartz.DateBuilder;

import com.nogle.commons.utils.NogleTimeFormatter;

public class TimeUtils {

    public static Interval union(final Interval firstInterval, final Interval secondInterval) {
        final DateTime start = firstInterval.getStart().isBefore(secondInterval.getStart()) ? firstInterval.getStart() : secondInterval.getStart();
        final DateTime end = firstInterval.getEnd().isAfter(secondInterval.getEnd()) ? firstInterval.getEnd() : secondInterval.getEnd();
        return new Interval(start, end);
    }

    public static boolean isCurrentTimeFallsInSchedule(final DateTime startTime, final DateTime stopTime) {
        final DateTime tradingDay = DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        final long transStart = tradingDay.getMillis() + startTime.getMillisOfDay();
        final long transEnd = tradingDay.getMillis() + stopTime.getMillisOfDay();
        if (TradeClock.getCurrentMillis() > transStart && TradeClock.getCurrentMillis() < transEnd) {
            return true;
        }
        return false;
    }

    public static CronExpression dailyAtHourMinuteAndSecond(final int hour, final int minute, final int second) {
        DateBuilder.validateHour(hour);
        DateBuilder.validateMinute(minute);
        DateBuilder.validateSecond(second);
        final String cronExpression = String.format("%d %d %d ? * *", second, minute, hour);
        try {
            return new CronExpression(cronExpression);
        } catch (final ParseException e) {
            // all methods of construction ensure the expression is valid by this point...
            throw new RuntimeException(
                "CronExpression '"
                    + cronExpression
                    + "' is invalid, which should not be possible, please report bug to developers",
                e);
        }
    }

}
