package com.nogle.core.util;

import org.apache.logging.log4j.core.util.Clock;

import com.nogle.core.config.EngineMode;

public class TradeClock implements Clock {

    private static TimeKeeper timer = SimulatedTimeKeeper.get();
    static EngineMode mode;

    public static void setMode(final EngineMode mode) {
        TradeClock.mode = mode;
        if (mode != EngineMode.SIMULATION) {
            timer = LiveTimeKeeper.getInstance();
        }
    }

    public static long getCurrentMillis() {
        return timer.getCurrentTimeMillis();
    }

    public static void setCurrentMillis(final long currentMillis) {
        timer.setCurrentTimeMillis(currentMillis);
    }

    public static long getCurrentMicros() {
        return timer.getCurrentTimeMicros();
    }

    public static long getCurrentMicrosOnly() {
        return timer.getCurrentTimeMicrosOnly();
    }

    public static void setCurrentMicros(final long currentMicros) {
        timer.setCurrentTimeMicros(currentMicros);
    }

    /* For calendar day */
    public static void onCalendarDayChange() {
        timer.onCalendarCheck();
    }

    public static String getCalendar() {
        return timer.getCurrentCalendar();
    }

    public static long getCalendarMillis() {
        return timer.getCurrentCalendarDayMillis();
    }

    /* For trading day */

    public static String getTradingDay() {
        return timer.getCurrentTradingDay();
    }

    public static long getTradingDayOpenedTime() {
        return timer.getCurrentTradingDayOpenedTime();
    }

    public static long getTradingDayClosingTime() {
        return timer.getCurrentTradingDayClosingTime();
    }

    public static long getElapsedTimeToNextCalendarDay() {
        return timer.getCurrentCalendarDayMillis() - timer.getCurrentTradingDayOpenedTime();
    }

    public static void setTradingDay(final long currentMillis) {
        timer.setCurrentTradingDay(currentMillis);
        timer.setCurrentTimeMillis(currentMillis);
    }

    @Override
    public long currentTimeMillis() {
        return getCurrentMillis();
    }

}
