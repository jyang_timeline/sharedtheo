package com.timelinecapital.core;

import java.util.Collections;
import java.util.Map;

import com.nogle.core.config.EngineMode;
import com.nogle.core.contract.ContractInquiry;
import com.nogle.core.market.Market;
import com.nogle.core.strategy.LiveStrategyLoader;
import com.nogle.core.strategy.StrategyLoader;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.strategy.StrategyEventManager;

public class CoreController {

    private static CoreController INSTANCE;

    private volatile static ThreadLocal<StrategyLoader> THREAD_SIM_LOADER = new ThreadLocal<>();
    private static StrategyLoader strategyLoader;

    private final Map<Integer, String> idToName;
    private final Map<Integer, StrategyUpdater> idToUpdater;
    private final Map<Integer, StrategyEventManager> idToEventManager;
    private final Map<Integer, StrategyView> idToStatusView;
    private final Map<Integer, ProtectiveManager> idToRiskControl;
    private final Map<String, Instrument> contracts;

    private final Market market;
    private final RiskManager riskManager;
    private final ContractInquiry inquiry;

    CoreController(final CoreControllerBuilder builder) {
        this.idToName = builder.idToName;
        this.idToUpdater = builder.idToUpdater;
        this.idToEventManager = builder.idToEventManager;
        this.idToStatusView = builder.idToStatusView;
        this.idToRiskControl = builder.idToRiskControl;
        this.contracts = builder.contracts;
        this.market = builder.market;
        this.riskManager = builder.riskManager;
        this.inquiry = builder.inquiry;
    }

    public static CoreController getInstance() {
        return INSTANCE;
    }

    public static void setStrategyLoader(final StrategyLoader strategyLoader) {
        if (THREAD_SIM_LOADER.get() == null) {
            try {
                THREAD_SIM_LOADER.set(strategyLoader);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static StrategyLoader getStrategyLoader() {
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            return THREAD_SIM_LOADER.get();
        }
        if (strategyLoader == null) {
            strategyLoader = new LiveStrategyLoader(INSTANCE.market, INSTANCE.idToUpdater);
        }
        return strategyLoader;
    }

    public Map<Integer, String> getIdToName() {
        return idToName;
    }

    public Map<Integer, StrategyUpdater> getIdToUpdater() {
        return idToUpdater;
    }

    public Map<Integer, StrategyEventManager> getIdToEventManager() {
        return idToEventManager;
    }

    public Map<Integer, StrategyView> getIdToStatusView() {
        return idToStatusView;
    }

    public StrategyView getStatusView(final int strategyId) {
        return idToStatusView.get(strategyId);
    }

    public Map<Integer, ProtectiveManager> getIdToRiskControl() {
        return idToRiskControl;
    }

    public Map<String, Instrument> getInternalContracts() {
        return Collections.unmodifiableMap(contracts);
    }

    public Market getMarket() {
        return market;
    }

    public RiskManager getRiskManager() {
        return riskManager;
    }

    public ContractInquiry getContractInquiry() {
        return inquiry;
    }

    public static class CoreControllerBuilder {

        private Map<Integer, String> idToName;
        private Map<Integer, StrategyUpdater> idToUpdater;
        private Map<Integer, StrategyEventManager> idToEventManager;
        private Map<Integer, StrategyView> idToStatusView;
        private Map<Integer, ProtectiveManager> idToRiskControl;
        private Map<String, Instrument> contracts;
        private RiskManager riskManager;
        private ContractInquiry inquiry;
        private Market market;

        public CoreControllerBuilder withMappingIdToName(final Map<Integer, String> idToName) {
            this.idToName = idToName;
            return this;
        }

        public CoreControllerBuilder withMappingIdToUpdater(final Map<Integer, StrategyUpdater> idToUpdater) {
            this.idToUpdater = idToUpdater;
            return this;
        }

        public CoreControllerBuilder withMappingIdToEventManager(final Map<Integer, StrategyEventManager> idToEventManager) {
            this.idToEventManager = idToEventManager;
            return this;
        }

        public CoreControllerBuilder withMappingIdToStatusView(final Map<Integer, StrategyView> idToStatusView) {
            this.idToStatusView = idToStatusView;
            return this;
        }

        public CoreControllerBuilder withMappingIdToRiskControl(final Map<Integer, ProtectiveManager> idToRiskControl) {
            this.idToRiskControl = idToRiskControl;
            return this;
        }

        public CoreControllerBuilder withMappingSymbolToContract(final Map<String, Instrument> contracts) {
            this.contracts = contracts;
            return this;
        }

        public CoreControllerBuilder forMarket(final Market market) {
            this.market = market;
            return this;
        }

        public CoreControllerBuilder withRiskManager(final RiskManager riskManager) {
            this.riskManager = riskManager;
            return this;
        }

        public CoreControllerBuilder withContractInquiry(final ContractInquiry inquiry) {
            this.inquiry = inquiry;
            return this;
        }

        public CoreController build() {
            INSTANCE = new CoreController(this);
            return INSTANCE;
        }

    }

}
