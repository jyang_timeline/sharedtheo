package com.timelinecapital.core;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.core.scheduler.OneTimeJob;
import com.timelinecapital.core.scheduler.ScheduledEventListener;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class CoreScheduler {
    private static final Logger log = LogManager.getLogger(CoreScheduler.class);

    private static CoreScheduler INSTANCE;

    public static CoreScheduler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CoreScheduler();
        }
        return INSTANCE;
    }

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;

    private CoreScheduler() {
        scheduler = JobScheduler.getInstance().getScheduler();
    }

    public void withSingleJob(final ScheduledEventListener listener, final Date scheduleTime) {
        try {
            jobDataMap.put(ScheduleJobUtil.TRIGGER_KEY_EVENT_SINGLE, listener);
            final JobDetail singleJob = JobBuilder.newJob(OneTimeJob.class).setJobData(jobDataMap).build();
            final Trigger singleTrigger = TriggerBuilder.newTrigger().startAt(scheduleTime).withSchedule(SimpleScheduleBuilder.repeatSecondlyForTotalCount(1)).build();
            scheduler.scheduleJob(singleJob, singleTrigger);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
