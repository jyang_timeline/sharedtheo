package com.timelinecapital.core;

import com.nogle.core.event.TradeEvent;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public interface DummyTrade {

    void onDummyMessage(final Side side, final TimeCondition tif, final long qty, final double price, final int positionMax, final TradeEvent tradeEventHandler) throws Exception;

}
