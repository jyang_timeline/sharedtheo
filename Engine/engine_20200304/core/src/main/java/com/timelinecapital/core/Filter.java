package com.timelinecapital.core;

import com.timelinecapital.core.util.TimelineEnum;

public interface Filter<T> {

    /**
     * The result that can returned from a filter method call.
     */
    enum Result {
        /**
         * The event will be processed without further filtering based on the log Level.
         */
        ACCEPT,
        /**
         * No decision could be made, further filtering should occur.
         */
        NEUTRAL,
        /**
         * The event should not be processed.
         */
        DENY;

        /**
         * Returns the Result for the given string.
         *
         * @param name The Result enum name, case-insensitive. If null, returns, null
         * @return a Result enum value or null if name is null
         */
        public static Result toResult(final String name) {
            return toResult(name, null);
        }

        /**
         * Returns the Result for the given string.
         *
         * @param name The Result enum name, case-insensitive. If null, returns, defaultResult
         * @param defaultResult the Result to return if name is null
         * @return a Result enum value or null if name is null
         */
        public static Result toResult(final String name, final Result defaultResult) {
            return TimelineEnum.valueOf(Result.class, name, defaultResult);
        }
    }

    /**
     * Returns the result that should be returned when the filter does not match the event.
     *
     * @return the Result that should be returned when the filter does not match the event.
     */
    Result getOnMismatch();

    /**
     * Returns the result that should be returned when the filter matches the event.
     *
     * @return the Result that should be returned when the filter matches the event.
     */
    Result getOnMatch();

    /**
     * Filter an event.
     *
     * @param Quote quote event.
     * @return the Result.
     */
    Result filter(T event);

}
