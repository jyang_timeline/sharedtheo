package com.timelinecapital.core;

import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.strategy.types.Contract;
import com.nogle.util.StrategyLifeCycle;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.types.PriceLevelPolicy;

interface Market {

    OrderViewContainer getOrderView(int strategyId, String account, Contract contract, PriceLevelPolicy orderViewType,
        PositionManager positionManager, ProtectiveManager riskControlManager, String threadTag);

    // List<TradeConnection> getTradeConnections();

    void subscribe(final Contract contract, final ChannelListener channelListener);

    void subscribeFeed(final Exchange exchange, final FeedType... feedTypes) throws FeedException;

    // void subscribeDataFeed(Contract contract, DataFeed dataFeedType);

    void updateStrategyStatus(final int strategyId, final StrategyLifeCycle lifeCycle);

    void onLoadingStrategy();

    void warmup();

    boolean isWarmupAvailable();

}
