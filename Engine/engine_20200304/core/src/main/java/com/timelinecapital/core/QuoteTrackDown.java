package com.timelinecapital.core;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.pool.Reusable;

public class QuoteTrackDown implements Reusable<QuoteTrackDown> {

    private QuoteTrackDown next = null;

    private DataType dataType;
    private Contract contract;
    private long sequenceNumber;
    private double value;

    public void setEventContent(final DataType dataType, final Contract contract, final long sequenceNumber, final double value) {
        this.dataType = dataType;
        this.contract = contract;
        this.sequenceNumber = sequenceNumber;
        this.value = value;
    }

    public DataType getDataType() {
        return dataType;
    }

    public Contract getContract() {
        return contract;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public double getValue() {
        return value;
    }

    @Override
    public QuoteTrackDown getNext() {
        return next;
    }

    @Override
    public void setNext(final QuoteTrackDown next) {
        this.next = next;
    }

    @Override
    public void reset() {
        next = null;
        dataType = null;
        contract = null;
        sequenceNumber = 0;
        value = 0.0;
    }

    @Override
    public String toString() {
        return "Event{" +
            dataType + "," +
            contract + "," +
            sequenceNumber + "," +
            value +
            "}";
    }

}
