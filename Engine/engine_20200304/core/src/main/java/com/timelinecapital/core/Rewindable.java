package com.timelinecapital.core;

public interface Rewindable {

    void rewind();

    boolean isReady();

}
