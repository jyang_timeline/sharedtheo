package com.timelinecapital.core;

import org.quartz.Job;

import com.timelinecapital.core.scheduler.ScheduledEventListener;

public interface ScheduledService {

    void setScheduleServiceListener(ScheduledEventListener listener, String schedulerKey);

    void setScheduleServiceListener(ScheduledEventListener listener, String schedulerKey, Class<? extends Job> jobClass);

    void removeScheduleServiceListener();

    enum ScheduleType {
        REPEAT_FOREVER,
        CRON_EXPRESSION,
        ;
    }

}
