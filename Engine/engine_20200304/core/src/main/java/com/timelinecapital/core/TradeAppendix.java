package com.timelinecapital.core;

import org.apache.commons.lang3.StringUtils;

import com.nogle.core.position.PositionTracker;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.stats.ExecReportEvent;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.types.PriceLevelPolicy;

public class TradeAppendix {

    private final Contract contract;
    private final TradingAccount account;
    private final PositionTracker positionTracker;

    private final ProtectiveManager riskControlManager;
    private final PositionManager positionManager;
    private final PriceManager priceManager;
    private final PriceLevelPolicy priceLevelPolicy;
    private final PositionImport importer;
    private final ExecReportEvent statsCollector;

    private final String desc;

    public TradeAppendix(final TradeAppendixBuilder builder) {
        this.contract = builder.contract;
        this.account = builder.account;
        this.positionTracker = builder.positionTracker;
        this.riskControlManager = builder.riskControlManager;
        this.positionManager = builder.positionManager;
        this.priceManager = builder.priceManager;
        this.priceLevelPolicy = builder.priceLevelPolicy;
        this.importer = builder.importer;
        this.statsCollector = builder.statsCollector;
        desc = StringUtils.joinWith(DelimiterUtil.CLASS_DESC_SEPARATOR, priceLevelPolicy, priceManager, positionManager, riskControlManager);
    }

    public Contract getContract() {
        return contract;
    }

    public TradingAccount getAccount() {
        return account;
    }

    public PositionTracker getPositionTracker() {
        return positionTracker;
    }

    public ProtectiveManager getRiskControlManager() {
        return riskControlManager;
    }

    public PositionManager getPositionManager() {
        return positionManager;
    }

    public PriceManager getPriceManager() {
        return priceManager;
    }

    public PriceLevelPolicy getPriceLevelPolicy() {
        return priceLevelPolicy;
    }

    public PositionImport getPositionImporter() {
        return importer;
    }

    public ExecReportEvent getStatsCollector() {
        return statsCollector;
    }

    @Override
    public String toString() {
        return desc;
    }

    public static class TradeAppendixBuilder {
        private Contract contract;
        private TradingAccount account;
        private PositionTracker positionTracker;
        private ProtectiveManager riskControlManager;
        private PositionManager positionManager;
        private PriceManager priceManager;
        private PriceLevelPolicy priceLevelPolicy;
        private PositionImport importer;
        private ExecReportEvent statsCollector;

        public TradeAppendixBuilder withContract(final Contract contract) {
            this.contract = contract;
            return this;
        }

        public TradeAppendixBuilder withAccount(final TradingAccount account) {
            this.account = account;
            return this;
        }

        public TradeAppendixBuilder withPositionTracker(final PositionTracker positionTracker) {
            this.positionTracker = positionTracker;
            return this;
        }

        public TradeAppendixBuilder withRiskManager(final ProtectiveManager riskControlManager) {
            this.riskControlManager = riskControlManager;
            return this;
        }

        public TradeAppendixBuilder withPositionManager(final PositionManager positionManager) {
            this.positionManager = positionManager;
            return this;
        }

        public TradeAppendixBuilder withPriceManager(final PriceManager priceManager) {
            this.priceManager = priceManager;
            return this;
        }

        public TradeAppendixBuilder withPricePolicy(final PriceLevelPolicy priceLevelPolicy) {
            this.priceLevelPolicy = priceLevelPolicy;
            return this;
        }

        public TradeAppendixBuilder withPositionImporter(final PositionImport importer) {
            this.importer = importer;
            return this;
        }

        public TradeAppendixBuilder withStatsCollector(final ExecReportEvent statsCollector) {
            this.statsCollector = statsCollector;
            return this;
        }

        public TradeAppendix build() {
            return new TradeAppendix(this);
        }

    }

}
