package com.timelinecapital.core.commands.system;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.core.Constants;
import com.nogle.core.exception.CommandException;
import com.nogle.core.strategy.CoreBuilder;
import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.commons.CommandExecutionResult;
import com.timelinecapital.commons.commands.engine.RefreshContract;
import com.timelinecapital.commons.commands.engine.RefreshContract.ActionLevel;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.strategy.StrategyEventManager;
import com.timelinecapital.core.types.StrategyEventType;

public class ContractManagement extends SystemCommand {
    private static final Logger log = LogManager.getLogger(ContractManagement.class);

    private static final Map<Integer, Boolean> updatedStrategy = new HashMap<>();

    private static final String description = "To refresh contract information and notify strategy";
    private static final String usage = RefreshContract.name +
        "{\"strategyId\":\"strategy ID or WILDCARD\",\"Level\":\"INSTANCE_CONTRACT or INSTANCE_PRICELIMIT_ONLY\"}";

    private final Map<Integer, StrategyUpdater> idToUpdater;
    private final Map<Integer, StrategyEventManager> idToEventManager;
    private final Map<String, Instrument> systemContracts;

    public ContractManagement(
        final Map<Integer, StrategyUpdater> idToUpdater,
        final Map<Integer, StrategyEventManager> idToEventManager,
        final Map<String, Instrument> systemContracts) {
        this.idToUpdater = idToUpdater;
        this.idToEventManager = idToEventManager;
        this.systemContracts = systemContracts;
    }

    @Override
    void execute(final String param, final ObjectNode node) {
        try {
            final JsonNode rootNode = MAPPER.readTree(param);

            if (!rootNode.has(RefreshContract.actionLevel)) {
                throw new CommandException("Missing field: " + RefreshContract.actionLevel);
            }

            updatedStrategy.clear();

            final JsonNode levelNode = rootNode.get(RefreshContract.actionLevel);
            final ActionLevel level = ActionLevel.valueOf(levelNode.asText());
            switch (level) {
                case INSTANCE_CONTRACT:
                    log.warn("Does not support yet!!");
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                    break;
                case INSTANCE_PRICELIMIT_ONLY: {
                    final String strategyId = rootNode.get(RefreshContract.paramStrategyId).asText();

                    /*
                     * get all contracts price limit information and notify models (all or given one)
                     */
                    if (Constants.WILDCARD.contentEquals(strategyId)) {
                        log.warn(systemContracts);
                        tryRefresh(systemContracts.values());
                        idToEventManager.forEach((id, manager) -> {
                            final Collection<Instrument> contracts = idToUpdater.get(id).getContracts();
                            contracts.forEach(c -> manager.publish(StrategyEventType.ContractRefresh, c));
                            updatedStrategy.put(id, true);
                        });
                        setExecutionResult(CommandExecutionResult.toResult(updatedStrategy.values()));
                    } else {
                        final int id = Integer.parseInt(strategyId);

                        final StrategyUpdater strategyUpdater = idToUpdater.get(id);
                        if (strategyUpdater == null) {
                            throw new CommandException("Mapping of StrategyUpdater does no exist");
                        }
                        final StrategyEventManager eventManager = idToEventManager.get(id);
                        if (eventManager == null) {
                            throw new CommandException("Mapping of StrategyEventManager does not exist");
                        }

                        final Collection<Instrument> contracts = strategyUpdater.getContracts();
                        tryRefresh(contracts);
                        contracts.forEach(c -> eventManager.publish(StrategyEventType.ContractRefresh, c));
                        updatedStrategy.put(id, true);
                        setExecutionResult(CommandExecutionResult.SUCCESS);
                    }
                    break;
                    // return RefreshContract.name + " Success";
                }
                default:
                    break;
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new CommandException(e.getMessage());
        }

    }

    private void tryRefresh(final Collection<Instrument> contracts) {
        CoreBuilder.buildContract(contracts);
    }

    @Override
    public String getName() {
        return RefreshContract.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
