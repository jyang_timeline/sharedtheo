package com.timelinecapital.core.commands.system;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.core.Constants;
import com.nogle.core.exception.CommandException;
import com.nogle.core.trade.TradeConnection;
import com.timelinecapital.commons.CommandExecutionResult;
import com.timelinecapital.commons.commands.engine.RefreshPosition;
import com.timelinecapital.commons.commands.engine.RefreshPosition.Action;

public class CounterBasedPositionManagement extends SystemCommand {
    private static final Logger log = LogManager.getLogger(CounterBasedPositionManagement.class);

    private static final String description = "To get position information from counter and notify strategy";
    private static final String usage = RefreshPosition.name +
        "{\"" + RefreshPosition.actionType + "\":\"" + RefreshPosition.Action.QUERY + "|" + RefreshPosition.Action.UPDATE + "|" + RefreshPosition.Action.SYNC + "\"," +
        "\"account\":\"account or WILDCARD\"}";

    private final List<TradeConnection> tradeConnections;

    public CounterBasedPositionManagement(final List<TradeConnection> tradeConnections) {
        this.tradeConnections = tradeConnections;
    }

    @Override
    void execute(final String param, final ObjectNode node) {
        try {
            final JsonNode rootNode = MAPPER.readTree(param);

            if (!rootNode.has(RefreshPosition.actionType)) {
                throw new CommandException("Missing field: " + RefreshPosition.actionType);
            }

            // updatedStrategy.clear();

            final JsonNode actionNode = rootNode.get(RefreshPosition.actionType);
            final Action action = Action.valueOf(actionNode.asText());
            switch (action) {
                case QUERY: {
                    // TODO
                    log.warn("Does not support yet");
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                    break;
                }
                case UPDATE: {
                    // TODO
                    log.warn("Does not support yet");
                    final String account = rootNode.get(RefreshPosition.paramAccount).asText();

                    /*
                     * get all position information (all or given one)
                     */
                    if (Constants.WILDCARD.contentEquals(account)) {

                    } else {

                    }
                    break;
                }
                case SYNC: {
                    final String account = rootNode.get(RefreshPosition.paramAccount).asText();

                    if (Constants.WILDCARD.contentEquals(account)) {
                        tradeConnections.forEach(t -> {
                            t.sync();
                        });
                    } else {
                        final Optional<TradeConnection> connection = tradeConnections.stream().filter(t -> t.getAccount().contentEquals(account)).findFirst();
                        if (connection.isPresent()) {
                            connection.get().sync();
                        } else {
                            log.warn("No suitable connection found");
                        }
                    }
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                }
                default:
                    break;
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new CommandException(e.getMessage());
        }
    }

    @Override
    public String getName() {
        return RefreshPosition.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
