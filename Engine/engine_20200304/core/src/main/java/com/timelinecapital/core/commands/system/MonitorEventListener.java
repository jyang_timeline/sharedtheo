package com.timelinecapital.core.commands.system;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.core.Constants;
import com.nogle.core.exception.CommandException;
import com.nogle.core.market.Market;
import com.nogle.strategy.api.Command;
import com.timelinecapital.commons.CommandExecutionResult;
import com.timelinecapital.commons.commands.FeedEventTailer;
import com.timelinecapital.commons.commands.FeedEventTailer.ActionLevel;
import com.timelinecapital.core.feed.ChannelListener;

public class MonitorEventListener extends SystemCommand {
    private static final Logger log = LogManager.getLogger(Command.class);

    private static final String description = "To track quote thread's event";
    private static final String usage = FeedEventTailer.name +
        "{\"Id\":\"ID or WILDCARD\",\"numberOfEvents\":\"number of events\",\"Level\":\"INSTANCE_STRATEGY or INSTANCE_THEO\"}";

    private final Map<Long, Set<ChannelListener>> strategyToChannels;

    public MonitorEventListener(final Market market) {
        strategyToChannels = market.getStrategyToChannels();
    }

    @Override
    void execute(final String param, final ObjectNode node) {
        try {
            final JsonNode rootNode = MAPPER.readTree(param);

            if (!rootNode.has(FeedEventTailer.actionLevel)) {
                throw new CommandException("Missing field: " + FeedEventTailer.actionLevel);
            }

            final JsonNode idNode = rootNode.get(FeedEventTailer.parameterId);
            final int numberOfEvents = rootNode.get(FeedEventTailer.parameterCount).asInt();

            final JsonNode levelNode = rootNode.get(FeedEventTailer.actionLevel);
            final ActionLevel level = ActionLevel.valueOf(levelNode.asText());
            switch (level) {
                case INSTANCE_THEO:
                    Set<ChannelListener> channelListeners = new HashSet<>();
                    if (Constants.WILDCARD.contentEquals(idNode.asText())) {
                        channelListeners = strategyToChannels.values().stream()
                            .flatMap(channel -> channel.stream())
                            .filter(channel -> channel.getListenerType().equals(ChannelListener.ListenerType.QUOTE_THREAD_SHARED_THEO))
                            .collect(Collectors.toSet());
                        log.info("on checking all #{}", channelListeners.size());
                        // channelListeners.addAll(strategyToChannels.values());
                        // strategyToChannels.forEach((id, listeners) -> {
                        // listeners.forEach(listener -> {
                        // listener.onFeedMonitoring(numberOfEvents);
                        // });
                        // });
                    } else {
                        log.info("on checking {}", idNode.asLong());
                        final long id = idNode.asLong(0);
                        channelListeners.addAll(strategyToChannels.get(id));
                        // final Set<ChannelListener> channelListeners = strategyToChannels.get(idNode.asLong());

                        // channelListeners.forEach(listener -> {
                        // listener.onFeedMonitoring(numberOfEvents);
                        // });
                    }
                    channelListeners.forEach(listener -> {
                        listener.onFeedMonitoring(numberOfEvents);
                    });
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                    break;
                case INSTANCE_STRATEGY:
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                    break;
                default:
                    setExecutionResult(CommandExecutionResult.FAILED);
                    break;
            }

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new CommandException(e.getMessage());
        }

    }

    @Override
    public String getName() {
        return FeedEventTailer.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
