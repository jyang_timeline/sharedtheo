package com.timelinecapital.core.commands.system;

import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.commons.command.DumpSystemStatus;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.core.EngineStatusView;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;
import com.nogle.util.StrategyLifeCycle;

public class QueryEngineStatus implements Command {

    private static final String description = "List all contract information.";
    private static final String usage = DumpSystemStatus.name +
        " [" + DumpSystemStatus.parameterEngineStatus + "|" +
        DumpSystemStatus.parameterStrategyStatus + "|" +
        DumpSystemStatus.parameterStrategy + "]";

    private static final ObjectMapper mapper = new ObjectMapper();

    private final Map<Integer, StrategyUpdater> idToUpdater;

    public QueryEngineStatus(final Map<Integer, StrategyUpdater> idToUpdater) {
        this.idToUpdater = idToUpdater;
    }

    @Override
    public String onCommand(final String arg) {
        final String[] argArray = arg.split(" ");
        final ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put(TradeEngineCommunication.commandTag, DumpSystemStatus.name);

        switch (argArray.length) {
            case 1:
                switch (argArray[0]) {
                    case DumpSystemStatus.parameterStrategy:
                        objectNode.put(TradeEngineCommunication.parameterTag, DumpSystemStatus.parameterStrategy);
                        final Map<Integer, String> idToName =
                            idToUpdater.values().stream().collect(Collectors.toMap(StrategyUpdater::getStrategyId, StrategyUpdater::getStrategyName));
                        objectNode.put(TradeEngineCommunication.messageTag, idToName.toString());
                        return objectNode.toString();
                    case DumpSystemStatus.parameterStrategyStatus:
                        objectNode.put(TradeEngineCommunication.parameterTag, DumpSystemStatus.parameterStrategyStatus);
                        final Map<Integer, StrategyLifeCycle> idToLife =
                            idToUpdater.values().stream().collect(Collectors.toMap(StrategyUpdater::getStrategyId, updater -> updater.getStrategyStatusView().getLifeCycle()));
                        objectNode.put(TradeEngineCommunication.messageTag, idToLife.toString());
                        return objectNode.toString();
                    case DumpSystemStatus.parameterEngineStatus:
                        objectNode.put(TradeEngineCommunication.parameterTag, DumpSystemStatus.parameterEngineStatus);
                        try {
                            objectNode.put(TradeEngineCommunication.messageTag, mapper.writeValueAsString(EngineStatusView.getInstance().translateToQueryResponse()));
                        } catch (final JsonProcessingException e) {
                            objectNode.put(TradeEngineCommunication.messageTag, "FAILED");
                        }
                        return objectNode.toString();
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        objectNode.put(TradeEngineCommunication.messageTag, "FAILED");
        return objectNode.toString();
    }

    @Override
    public String getName() {
        return DumpSystemStatus.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
