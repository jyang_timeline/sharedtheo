package com.timelinecapital.core.commands.system;

import java.lang.management.ManagementFactory;
import java.util.Map;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.EngineQuery;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.api.Command;

public class QuerySystemLoading implements Command {
    private static final Logger log = LogManager.getLogger(QuerySystemLoading.class);

    private static final String description = "Query engine informations.";
    private static final String usage = EngineQuery.name + " [" +
        EngineQuery.parameterLoading + "|" +
        EngineQuery.parameterStrategyCount +
        "]";
    private static final String defaultLoading = "0";

    private final Map<Integer, StrategyUpdater> idToUpdater;
    private final MBeanServer mbs;
    private ObjectName objectName;

    public QuerySystemLoading(final Map<Integer, StrategyUpdater> idToUpdater) {
        this.idToUpdater = idToUpdater;
        this.mbs = ManagementFactory.getPlatformMBeanServer();
        try {
            this.objectName = ObjectName.getInstance("java.lang:type=OperatingSystem");
        } catch (final Exception e) {
            log.error("Exception when getting object name.", e);
            this.objectName = null;
        }
    }

    private String queryLoading() {
        try {
            final AttributeList list = mbs.getAttributes(objectName, new String[] { "ProcessCpuLoad" });

            if (!list.isEmpty()) {
                Double loading = -1.0;
                int retry = 0;

                /* Might take times to get JVM cpu loading, wait and retry at most 3 times. */
                do {
                    loading = (Double) (((Attribute) list.get(0)).getValue());
                    ++retry;
                    Thread.sleep(1 * 1000);
                } while (loading < 0 && retry < 3);

                /* two decimal percentage value */
                return String.valueOf(((int) (loading * 10000) / 100.0));
            } else {
                log.error("Get ProcessCpuLoad failed.");
            }
        } catch (final Exception e) {
            log.error("Exception when quering cpu loading.", e);
        }

        return defaultLoading;
    }

    private String queryStrategyCount() {
        return String.valueOf(idToUpdater.size());
    }

    @Override
    public String onCommand(final String arg) {
        String ret = null;
        switch (arg) {
            case EngineQuery.parameterLoading:
                ret = queryLoading();
                break;
            case EngineQuery.parameterStrategyCount:
                ret = queryStrategyCount();
                break;
            default:
                ret = "Unsupport parameter: " + arg;
                break;
        }
        return ret;
    }

    @Override
    public String getName() {
        return EngineQuery.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

}
