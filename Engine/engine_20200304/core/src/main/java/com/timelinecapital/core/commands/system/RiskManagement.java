package com.timelinecapital.core.commands.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.commons.command.Config;
import com.nogle.strategy.api.CommandException;
import com.timelinecapital.commons.CommandExecutionResult;
import com.timelinecapital.commons.commands.engine.ConfigRiskManagement;
import com.timelinecapital.commons.commands.engine.ConfigRiskManagement.Action;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.riskmanagement.RiskManager;

public class RiskManagement extends SystemCommand {
    private static final Logger log = LogManager.getLogger(RiskManagement.class);

    private static final Map<String, Pair<Object, Boolean>> updatedConfigs = new HashMap<>();

    private static final String description = "Query or update engine risk management";
    private static final String usage = ConfigRiskManagement.name +
        "{\"" + ConfigRiskManagement.actionType + "\":\"" + ConfigRiskManagement.Action.QUERY + "|" + ConfigRiskManagement.Action.UPDATE + "\"}" +
        "{\"RiskManagement key\": value}";

    private final RiskManager riskManager;

    public RiskManagement(final RiskManager riskManager) {
        this.riskManager = riskManager;
    }

    @Override
    void execute(final String param, final ObjectNode node) {
        try {
            final JsonNode rootNode = MAPPER.readTree(param);

            if (!rootNode.has(ConfigRiskManagement.actionType)) {
                throw new CommandException("Missing field: " + ConfigRiskManagement.actionType);
            }

            final JsonNode actionNode = rootNode.get(ConfigRiskManagement.actionType);
            final Action action = Action.valueOf(actionNode.asText());
            switch (action) {
                case QUERY:
                    riskManager.getRiskManagerConfig().getConfigInMap().forEach((k, v) -> {
                        node.put(k, String.valueOf(v));
                    });
                    setExecutionResult(CommandExecutionResult.EXECUTION_DONE);
                    break;
                case UPDATE:
                    updatedConfigs.clear();

                    final Map<String, Object> input = Config.parseJsonInput(param);
                    input.remove(ConfigRiskManagement.actionType);
                    for (final Entry<String, Object> entry : input.entrySet()) {
                        final boolean perEntryResult = riskManager.updateCondition(entry.getKey(), entry.getValue());
                        if (perEntryResult) {
                            node.put(entry.getKey(), String.valueOf(entry.getValue()));
                        } else {
                            node.put(entry.getKey(), NO_ACTION);
                        }
                        updatedConfigs.put(entry.getKey(), new Pair<Object, Boolean>(entry.getValue(), perEntryResult));
                    }

                    final List<Boolean> executionResults = updatedConfigs.values().stream().map(v -> v.getRight()).collect(Collectors.toList());
                    setExecutionResult(CommandExecutionResult.toResult(executionResults));
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new CommandException("Failed to execute Command", e);
        }
    }

    @Override
    public String getName() {
        return ConfigRiskManagement.name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getUsage() {
        return usage;
    }

    // TODO make it a Unit test
    public static void main(final String[] args) throws ConfigurationException {
        RiskManagerConfig.buildConfig();
        final RiskManager riskManager = new RiskManager(RiskManagerConfig.getInstance());
        final RiskManagement m = new RiskManagement(riskManager);

        final ObjectNode node = MAPPER.createObjectNode();
        node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.QUERY.name());
        // node.put(ConfigRiskManagement.actionType, StringUtils.EMPTY);
        // node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.UPDATE.name());
        // node.put("maxCrossPercentage", "1.0");
        // node.put("maxCancelsPerInterval", "7");
        System.out.println(m.onCommand(node.toString()));
    }

}
