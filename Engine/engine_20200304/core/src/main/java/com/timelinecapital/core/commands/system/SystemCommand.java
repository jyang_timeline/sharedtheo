package com.timelinecapital.core.commands.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.strategy.api.Command;
import com.nogle.strategy.api.CommandException;
import com.timelinecapital.commons.CommandExecutionResult;

public abstract class SystemCommand implements Command {

    static final String DONE = "DONE";
    static final String NO_ACTION = "NO_ACTION";
    static final ObjectMapper MAPPER = new ObjectMapper();

    private CommandExecutionResult executionResult;

    SystemCommand() {
    }

    abstract void execute(String param, ObjectNode node);

    void setExecutionResult(final CommandExecutionResult executionResult) {
        this.executionResult = executionResult;
    }

    @Override
    public String onCommand(final String parameter) throws CommandException {
        final ObjectNode node = MAPPER.createObjectNode();
        execute(parameter, node);
        node.put(TradeEngineCommunication.executionResultTag, executionResult.name());
        return node.toString();
    }

}
