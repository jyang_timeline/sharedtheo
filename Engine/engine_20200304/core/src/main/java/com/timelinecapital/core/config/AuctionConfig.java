package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_AUCTION;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_AUCTION_NOCANCELLATION;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

public class AuctionConfig extends ExchangeBaseConfig {

    private final Map<String, String[]> exchangeAuctionSessions;
    private final Map<String, String[]> exchangeAuctionFreezeTime;

    AuctionConfig(final PropertiesConfiguration properties) {
        super(properties);

        final Configuration auctionSubset = properties.subset(PREFIX_EXCHANGE_AUCTION);
        final Map<String, String[]> aInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            aInternal.put(exchange, auctionSubset.getStringArray(exchange));
        }
        exchangeAuctionSessions = Collections.unmodifiableMap(aInternal);

        final Configuration noCancelSubset = properties.subset(PREFIX_EXCHANGE_AUCTION_NOCANCELLATION);
        final Map<String, String[]> ncInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            ncInternal.put(exchange, noCancelSubset.getStringArray(exchange));
        }
        exchangeAuctionFreezeTime = Collections.unmodifiableMap(ncInternal);
    }

    public Map<String, String[]> getAuctionSessions() {
        return exchangeAuctionSessions;
    }

    public Map<String, String[]> getAuctionFreezeTime() {
        return exchangeAuctionFreezeTime;
    }

}
