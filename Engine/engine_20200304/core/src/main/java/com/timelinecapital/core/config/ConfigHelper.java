package com.timelinecapital.core.config;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigHelper {
    private static final Logger log = LogManager.getLogger(ConfigHelper.class);

    public static void update(final RiskManagerConfig configClass, final String key, final Object value) throws IllegalArgumentException, IllegalAccessException {

        for (final Field field : configClass.getClass().getDeclaredFields()) {
            if (field.getName().contentEquals(key)) {
                if (value.getClass() == field.getType()) {
                    field.set(configClass, value);
                    log.warn("[EngineConfig] updated {} : {}", key, value);
                }
            }
        }

    }

    public static void load(final Class<?> configClass, final String file) {
        try {
            final Properties props = new Properties();
            try (FileInputStream propStream = new FileInputStream(file)) {
                props.load(propStream);
            }
            for (final Field field : configClass.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers())) {
                    field.set(null, getValue(props, field.getName(), field.getType()));
                }
            }
        } catch (final Exception e) {
            throw new RuntimeException("Error loading configuration: " + e, e);
        }
    }

    private static Object getValue(final Properties props, final String name, final Class<?> type) {
        final String value = props.getProperty(name);
        if (value == null) {
            throw new IllegalArgumentException("Missing configuration value: " + name);
        }
        if (type == String.class) {
            return value;
        }
        if (type == boolean.class) {
            return Boolean.parseBoolean(value);
        }
        if (type == int.class) {
            return Integer.parseInt(value);
        }
        if (type == float.class) {
            return Float.parseFloat(value);
        }
        if (type == double.class) {
            return Double.parseDouble(value);
        }
        throw new IllegalArgumentException("Unknown configuration value type: " + type.getName());
    }

    // public static void main(final String[] args) throws ConfigurationException, IllegalArgumentException,
    // IllegalAccessException {
    // EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
    //
    // final RiskManagerConfig config = EngineConfig.getRiskManagerConfig();
    // System.out.println(config);
    // final Object obj = Double.parseDouble("6.0");
    // update(config, "maxCrossPercentage", obj);
    // System.out.println(config);
    // }

}
