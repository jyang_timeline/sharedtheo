package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.messageHubIPKey;
import static com.timelinecapital.core.config.EngineConfigKeys.messageHubPortKey;
import static com.timelinecapital.core.config.EngineConfigKeys.remoteServerURLPropertyKey;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

public class ConnectionConfig {

    private final String messageHubIP;
    private final String messageHubPort;
    private final String remoteServerURL;

    ConnectionConfig(final PropertiesConfiguration properties) {
        messageHubIP = properties.getString(messageHubIPKey);
        messageHubPort = properties.getString(messageHubPortKey);
        remoteServerURL = properties.getString(remoteServerURLPropertyKey, StringUtils.EMPTY);
    }

    String getMessageQueueIP() {
        return messageHubIP;
    }

    String getMessageQueuePort() {
        return messageHubPort;
    }

    String getRemoteServerURL() {
        return remoteServerURL;
    }

}
