package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.CodecVerPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_TIMEFORMAT;
import static com.timelinecapital.core.config.EngineConfigKeys.brokerPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.checkInterfaceKey;
import static com.timelinecapital.core.config.EngineConfigKeys.contractServicePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.enableMultiAccountsKey;
import static com.timelinecapital.core.config.EngineConfigKeys.monitorServicePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.networkTrafficPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.positionPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.positionSyncServicePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.remoteClassLoaderPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.selfDefinedClockKey;
import static com.timelinecapital.core.config.EngineConfigKeys.serverNameKey;
import static com.timelinecapital.core.config.EngineConfigKeys.targetExchangePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.targetProtocolPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.tradingDaySwitchPointKey;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.joda.time.DateTime;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.config.EngineMode;
import com.nogle.core.config.EngineThreadPolicy;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.util.RingCapacityHelper;
import com.timelinecapital.core.util.TradingHoursHelper;

public class EngineConfig {

    private static final String DEFAULT_TIMEFORMAT = "hhmmssffffff";

    private static EngineConfig engineConfig;

    private final WarmupConfig warmupConfig;
    private final ConnectionConfig connectionConfig;
    private final ThreadFactoryConfig threadFactoryConfig;

    private final StatusBaseConfig statusConfig;
    private final StatusIntervalConfig statusIntervalConfig;

    private final SessionConfig sessionConfig;
    private final AuctionConfig auctionConfig;

    private final FeedFilterConfig feedConfig;
    private final FeedExtensionConfig extensionConfig;

    private final PropertiesConfiguration properties;
    private final int engineId;
    private final EngineMode mode;

    private final boolean isSelfDefinedClock;
    private final DateTime tradingDaySwitchTime;

    private final String serverName;
    private final int schemaVersion;
    private final String broker;
    private final String tradeExchange;
    private final String tradeProtocol;

    private final boolean enableMultiAccounts;

    private final boolean isVersionSafeMode;
    private final boolean isLessNetworkTraffic;
    private final boolean isKeepPositionOverNight;
    private final boolean isRemoteClassLoader;

    private final Map<String, String> exchangeToTimeFormat = new HashMap<>();
    private final String[] monitorSchedules;
    private final String[] posSyncSchedules;
    private final String[] contractSchedules;

    private EngineConfig(final int engineId, File configFile, final EngineMode mode) throws ConfigurationException {
        this.engineId = engineId;
        this.mode = mode;

        if (!configFile.isFile()) {
            configFile = new File(this.getClass().getClassLoader().getResource(configFile.getName()).getFile());
        }

        final Parameters params = new Parameters();
        final FileBasedConfigurationBuilder<FileBasedConfiguration> builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
            .configure(params.fileBased().setFile(configFile).setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
        properties = (PropertiesConfiguration) builder.getConfiguration();

        warmupConfig = new WarmupConfig(properties);
        connectionConfig = new ConnectionConfig(properties);
        threadFactoryConfig = new ThreadFactoryConfig(properties);

        statusConfig = new StatusBaseConfig(properties);
        statusIntervalConfig = new StatusIntervalConfig(properties);

        sessionConfig = new SessionConfig(properties);
        auctionConfig = new AuctionConfig(properties);

        feedConfig = new FeedFilterConfig(properties);
        extensionConfig = new FeedExtensionConfig(properties);

        isSelfDefinedClock = properties.getBoolean(selfDefinedClockKey, false);
        tradingDaySwitchTime = DateTime.parse(properties.getString(tradingDaySwitchPointKey), NogleTimeFormatter.ISO_TIME_FORMAT);

        serverName = properties.getString(serverNameKey);
        schemaVersion = properties.getInt(CodecVerPropertyKey, 1);
        broker = properties.getString(brokerPropertyKey);

        tradeExchange = properties.getString(targetExchangePropertyKey);
        tradeProtocol = properties.getString(targetProtocolPropertyKey);

        enableMultiAccounts = properties.getBoolean(enableMultiAccountsKey, true);
        isVersionSafeMode = properties.getBoolean(checkInterfaceKey, true);
        isLessNetworkTraffic = properties.getBoolean(networkTrafficPropertyKey, false);
        isKeepPositionOverNight = properties.getBoolean(positionPropertyKey, false);
        isRemoteClassLoader = properties.getBoolean(remoteClassLoaderPropertyKey, false);

        final Configuration timeformatSubset = properties.subset(PREFIX_EXCHANGE_TIMEFORMAT);
        for (final String exchange : ExchangeBaseConfig.exchangeList) {
            exchangeToTimeFormat.put(exchange, timeformatSubset.getString(exchange, DEFAULT_TIMEFORMAT));
        }

        monitorSchedules = properties.getStringArray(monitorServicePropertyKey);
        posSyncSchedules = properties.getStringArray(positionSyncServicePropertyKey);
        contractSchedules = properties.getStringArray(contractServicePropertyKey);
    }

    public static void buildConfig(final int engineId, final File configFile, final EngineMode mode) throws ConfigurationException {
        if (engineConfig == null) {
            engineConfig = new EngineConfig(engineId, configFile, mode);
            TradingHoursHelper.getInstance();
            RingCapacityHelper.onEngineMode(mode);
        }
    }

    public static EngineMode getEngineMode() {
        return engineConfig.mode;
    }

    public static int getEngineId() {
        return engineConfig.engineId;
    }

    public static boolean isSelfDefinedClock() {
        return engineConfig.isSelfDefinedClock;
    }

    public static DateTime getTradingDaySwitchPoint() {
        return engineConfig.tradingDaySwitchTime;
    }

    public static String getServerName() {
        return engineConfig.serverName;
    }

    public static SbeVersion getSchemaVersion() {
        return SbeVersion.fromInt(engineConfig.schemaVersion);
    }

    public static String getBroker() {
        return engineConfig.broker;
    }

    public static String getTradeExchange() {
        return engineConfig.tradeExchange;
    }

    public static String getTradeProtocol() {
        return engineConfig.tradeProtocol;
    }

    public static String getExchangeTimeFormat(final String exchange) {
        return engineConfig.exchangeToTimeFormat.getOrDefault(exchange, DEFAULT_TIMEFORMAT);
    }

    public static String[] getMonitorSchedules() {
        return engineConfig.monitorSchedules;
    }

    public static String[] getPositionSyncSchedules() {
        return engineConfig.posSyncSchedules;
    }

    public static String[] getContractSchedules() {
        return engineConfig.contractSchedules;
    }

    public static boolean isEnableMultiAccounts() {
        return engineConfig.enableMultiAccounts;
    }

    public static boolean isVersionSafeMode() {
        return engineConfig.isVersionSafeMode;
    }

    public static boolean isLessNetworkTraffic() {
        return engineConfig.isLessNetworkTraffic;
    }

    public static boolean isKeepPositionOverNight() {
        return engineConfig.isKeepPositionOverNight;
    }

    public static boolean isRemoteClassLoader() {
        return engineConfig.isRemoteClassLoader;
    }

    public static boolean isEnableDefaultView() {
        return engineConfig.statusConfig.isEnableDefaultView();
    }

    public static int getDefaultStrategyViewInterval() {
        return engineConfig.statusIntervalConfig.getDefaultStrategyViewInterval();
    }

    public static int getDefaultEngineViewInterval() {
        return engineConfig.statusIntervalConfig.getDefaultEngineViewInterval();
    }

    public static boolean isEnableDeltaView() {
        return engineConfig.statusConfig.isEnableDeltaView();
    }

    public static int getDeltaViewInterval() {
        return engineConfig.statusIntervalConfig.getDeltaViewInterval();
    }

    public static boolean isEnableEngineMetrics() {
        return engineConfig.statusConfig.isEnableEngineMetrics();
    }

    public static int getEngineMetricsInterval() {
        return engineConfig.statusIntervalConfig.getEngineMetricsInterval();
    }

    public static boolean isEnablePositionView() {
        return engineConfig.statusConfig.isEnablePositionsView();
    }

    public static int getPositionViewInterval() {
        return engineConfig.statusIntervalConfig.getPositionViewInterval();
    }

    public static boolean enableWarmup() {
        return engineConfig.warmupConfig.isEnableWarmup();
    }

    public static String getWarmupSymbol() {
        return engineConfig.warmupConfig.getWarmupSymbol();
    }

    public static int getWarmupCycles() {
        return engineConfig.warmupConfig.getWarmupCycles();
    }

    public static String getMessageHubIP() {
        return engineConfig.connectionConfig.getMessageQueueIP();
    }

    public static String getMessageHubPort() {
        return engineConfig.connectionConfig.getMessageQueuePort();
    }

    public static String getRemoteServerURL() {
        return engineConfig.connectionConfig.getRemoteServerURL();
    }

    public static EngineThreadPolicy getSocketPolicy() {
        return engineConfig.threadFactoryConfig.getSocketPolicy();
    }

    public static EngineThreadPolicy getThreadPolicy() {
        return engineConfig.threadFactoryConfig.getThreadPolicy();
    }

    public static int getThreadPoolCount() {
        return engineConfig.threadFactoryConfig.getThreadPoolCount();
    }

    public static int getRingSize(final int ringBufferSize) {
        return engineConfig.threadFactoryConfig.getRingSize();
    }

    public static String getRingStrategy(final String waitStrategy) {
        return engineConfig.threadFactoryConfig.getRingStrategy();
    }

    public static String[] getExchanges() {
        return ExchangeBaseConfig.getExchanges();
    }

    public static Map<String, String[]> getExchangeToTradingSessions() {
        return engineConfig.sessionConfig.getExchangeToTradingSessions();
    }

    public static Map<String, String[]> getExchangeAuctionSessions() {
        return engineConfig.auctionConfig.getAuctionSessions();
    }

    public static Map<String, String[]> getExchangeAuctionFreezeTime() {
        return engineConfig.auctionConfig.getAuctionFreezeTime();
    }

    public static Map<String, Integer> getExchangeToFeedFilter() {
        return engineConfig.feedConfig.getExchangeToFeedFilter();
    }

    public static Map<String, String[]> getExchangeToProtocols() {
        return engineConfig.feedConfig.getExchangeToProtocols();
    }

    public static Map<String, Integer> getExchangeToMDDepthMap() {
        return engineConfig.feedConfig.getExchangeToMDDepthMap();
    }

    public static Map<String, String[]> getExchangeToNICExclusions() {
        return engineConfig.feedConfig.getExchangeToNICExclusions();
    }

    public static Map<String, Boolean> getExchangeToDefaultFeedSpec() {
        return engineConfig.feedConfig.getExchangeToDefaultFeedSpec();
    }

    public static String getBestPriceOrderDetailEndpoint() {
        return engineConfig.extensionConfig.getBestPriceDetailEndpoint();
    }

    public static String[] getBestPriceOrderDetailExchanges() {
        return engineConfig.extensionConfig.getBestPriceDetailExchanges();
    }

    public static String getOrderActionsEndpoint() {
        return engineConfig.extensionConfig.getOrderActionsEndpoint();
    }

    public static String[] getOrderActionsExchanges() {
        return engineConfig.extensionConfig.getOrderActionsExchanges();
    }

    public static String getOrderQueueEndpoint() {
        return engineConfig.extensionConfig.getOrderQueueEndpoint();
    }

    public static String[] getOrderQueueExchanges() {
        return engineConfig.extensionConfig.getOrderQueueExchanges();
    }

    public static int getOrderQueueDepth() {
        return engineConfig.extensionConfig.getOrderQueueDepth();
    }

}
