package com.timelinecapital.core.config;

public class EngineConfigKeys {

    static final String PREFIX_EXCHANGE_SESSIONS = "Exchange.TradingSessions";
    static final String PREFIX_EXCHANGE_AUCTION = "Exchange.Auction";
    static final String PREFIX_EXCHANGE_AUCTION_NOCANCELLATION = "Exchange.Auction.Freeze";

    static final String PREFIX_EXCHANGE_PREFERENCE = "Exchange.Preference";
    static final String PREFIX_EXCHANGE_PROTOCOLS = "Exchange.Protocols";
    static final String PREFIX_EXCHANGE_EXCLUSION = "Exchange.Exclusion";
    static final String PREFIX_EXCHANGE_FILTER = "Exchange.Filter";
    static final String PREFIX_EXCHANGE_DEFAULT_FEEDSPEC = "Exchange.DefaultFeedSpec";
    static final String PREFIX_EXCHANGE_TIMEFORMAT = "Exchange.TimeFormat";
    static final String PREFIX_EXCHANGE_DEPTH = "Exchange.Depth";

    static final String PREFIX_STATUS_BASE = "Status.Base";
    static final String PREFIX_STATUS_INTERVAL = "Status.Interval";

    static final String tradingDaySwitchPointKey = "TradingDaySwitchPoint";
    static final String enableWarmupPropertyKey = "enableWarmup";
    static final String warmupSymbolPropertyKey = "warmupSymbol";
    static final String warmupCyclesPropertyKey = "warmupCycles";

    static final String serverNameKey = "serverName";

    static final String exchangePropertyKey = "Exchange";
    static final String CodecVerPropertyKey = "CodecVer";
    static final String brokerPropertyKey = "Broker";

    static final String targetProtocolPropertyKey = "TargetProtocol";
    static final String targetExchangePropertyKey = "TargetExchange";

    static final String enableMultiAccountsKey = "enableMultiAccounts";

    static final String enableSharedTheoValueDump = "enableSharedTheoValueDump";

    static final String checkInterfaceKey = "isVersionSafeMode";
    static final String selfDefinedClockKey = "isSelfDefinedClock";
    static final String networkTrafficPropertyKey = "isLessNetworkTraffic";
    static final String positionPropertyKey = "isKeepPositionOverNight";

    static final String enableDefaultViewKey = "enableDefaultView";
    static final String defaultStrategyViewIntervalKey = "DefaultStrategyView";
    static final String defaultEngineViewIntervalKey = "DefaultEngineView";

    static final String enableDeltaViewKey = "enableDeltaView";
    static final String deltaViewIntervalKey = "DeltaView";

    static final String enableEngineMetricsKey = "enableEngineMetrics";
    static final String engineMetricsKey = "EngineMetrics";

    static final String enablePositionViewKey = "enablePositionsView";
    static final String positionViewIntervalKey = "PositionsView";

    static final String messageHubIPKey = "messageHubIP";
    static final String messageHubPortKey = "messageHubPort";

    static final String remoteClassLoaderPropertyKey = "isRemoteClassLoader";
    static final String remoteServerURLPropertyKey = "RemoteServerURL";

    static final String socketPolicyPropertyKey = "SocketPolicy";
    static final String threadPolicyPropertyKey = "ThreadPolicy";
    static final String threadPoolCountPropertyKey = "ThreadPoolCount";
    static final String ringSizePropertyKey = "RingSize";
    static final String ringStrategyPropertyKey = "RingStrategy";

    static final String monitorServicePropertyKey = "MonitorService.Schedule";
    static final String positionSyncServicePropertyKey = "PositionSyncService.Schedule";
    static final String contractServicePropertyKey = "ContractService.Schedule";

    static final String orderQueueEndpointPropertyKey = "AdditionalData.OrderQueue.Endpoint";
    static final String orderQueueExchangePropertyKey = "AdditionalData.OrderQueue.Exchange";
    static final String orderQueueDepthPropertyKey = "AdditionalData.OrderQueue.Depth";

    static final String orderActionsEndpointPropertyKey = "AdditionalData.OrderActions.Endpoint";
    static final String orderActionsExchangePropertyKey = "AdditionalData.OrderActions.Exchange";

    static final String orderDetailEndpointPropertyKey = "AdditionalData.BestPriceOrderDetail.Endpoint";
    static final String orderDetailExchangePropertyKey = "AdditionalData.BestPriceOrderDetail.Exchange";

}
