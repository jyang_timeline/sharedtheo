package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.exchangePropertyKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration2.PropertiesConfiguration;

public class ExchangeBaseConfig {

    private static String[] exchanges;
    static final List<String> exchangeList = new ArrayList<>();

    ExchangeBaseConfig(final PropertiesConfiguration properties) {
        if (exchangeList.isEmpty()) {
            exchanges = properties.getStringArray(exchangePropertyKey);
            exchangeList.addAll(Arrays.asList(exchanges));
        }
    }

    static String[] getExchanges() {
        return exchanges;
    }

}
