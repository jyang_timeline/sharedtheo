package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.SimulationConfigKeys.PREFIX_FEE_OF_EXCHANGE;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.types.CommissionInfo;

public class FeeConfig {

    private final Map<Pair<Exchange, String>, Map<Side, CommissionInfo>> brokerToCommissionInfo = new HashMap<>();

    FeeConfig(final PropertiesConfiguration properties, final String broker) {
        for (final Exchange exchange : Exchange.values()) {
            final Map<Side, CommissionInfo> sideMap = new HashMap<>();

            final Configuration buySubset = properties.subset(PREFIX_FEE_OF_EXCHANGE +
                DelimiterUtil.CONFIG_KEY_DELIMETER +
                exchange.name() +
                DelimiterUtil.CONFIG_KEY_DELIMETER +
                Side.BUY.name());

            final Configuration sellSubset = properties.subset(PREFIX_FEE_OF_EXCHANGE +
                DelimiterUtil.CONFIG_KEY_DELIMETER +
                exchange.name() +
                DelimiterUtil.CONFIG_KEY_DELIMETER +
                Side.SELL.name());

            if (buySubset.isEmpty() || sellSubset.isEmpty()) {
                continue;
            }
            sideMap.put(Side.BUY, new CommissionInfo(exchange, Side.BUY, buySubset.getDouble(broker, 0)));
            sideMap.put(Side.SELL, new CommissionInfo(exchange, Side.SELL, sellSubset.getDouble(broker, 0)));

            brokerToCommissionInfo.put(new Pair<>(exchange, broker), sideMap);
        }
    }

    public Map<Pair<Exchange, String>, Map<Side, CommissionInfo>> getBrokerToCommissionInfo() {
        return brokerToCommissionInfo;
    }

}
