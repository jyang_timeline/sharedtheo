package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.orderActionsEndpointPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderActionsExchangePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderDetailEndpointPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderDetailExchangePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderQueueDepthPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderQueueEndpointPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.orderQueueExchangePropertyKey;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

public final class FeedExtensionConfig {

    private final String orderQueueEndpoint;
    private final String[] orderQueueExchanges;
    private final int orderQueueDepth;
    private final String orderActionsEndpoint;
    private final String[] orderActionsExchanges;
    private final String bestPriceDetailEndpoint;
    private final String[] bestPriceDetailExchanges;

    FeedExtensionConfig(final PropertiesConfiguration properties) {
        orderQueueEndpoint = properties.getString(orderQueueEndpointPropertyKey, StringUtils.EMPTY);
        orderQueueExchanges = properties.getStringArray(orderQueueExchangePropertyKey);
        orderQueueDepth = properties.getInt(orderQueueDepthPropertyKey, 50);
        orderActionsEndpoint = properties.getString(orderActionsEndpointPropertyKey, StringUtils.EMPTY);
        orderActionsExchanges = properties.getStringArray(orderActionsExchangePropertyKey);
        bestPriceDetailEndpoint = properties.getString(orderDetailEndpointPropertyKey, StringUtils.EMPTY);
        bestPriceDetailExchanges = properties.getStringArray(orderDetailExchangePropertyKey);
    }

    final String getOrderQueueEndpoint() {
        return orderQueueEndpoint;
    }

    final String[] getOrderQueueExchanges() {
        return orderQueueExchanges;
    }

    final int getOrderQueueDepth() {
        return orderQueueDepth;
    }

    final String getOrderActionsEndpoint() {
        return orderActionsEndpoint;
    }

    final String[] getOrderActionsExchanges() {
        return orderActionsExchanges;
    }

    final String getBestPriceDetailEndpoint() {
        return bestPriceDetailEndpoint;
    }

    final String[] getBestPriceDetailExchanges() {
        return bestPriceDetailExchanges;
    }

}
