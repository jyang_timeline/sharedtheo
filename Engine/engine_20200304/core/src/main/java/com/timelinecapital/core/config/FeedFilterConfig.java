package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_DEFAULT_FEEDSPEC;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_DEPTH;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_EXCLUSION;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_FILTER;
import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_PROTOCOLS;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

import com.timelinecapital.commons.channel.Exchange;

public final class FeedFilterConfig extends ExchangeBaseConfig {

    private final Map<String, Integer> exchangeToFeedFilter;
    private final Map<String, String[]> exchangeToProtocols;
    private final Map<String, Integer> exchangeToMDDepth;
    private final Map<String, String[]> exchangeToNICExclusions;
    private final Map<String, Boolean> exchangeToDefaultFeedSpec;

    FeedFilterConfig(final PropertiesConfiguration properties) {
        super(properties);
        final Configuration filterSubset = properties.subset(PREFIX_EXCHANGE_FILTER);
        final Map<String, Integer> fInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            fInternal.put(exchange, filterSubset.getInteger(exchange, 0));
        }
        exchangeToFeedFilter = Collections.unmodifiableMap(fInternal);

        final Configuration protocolSubset = properties.subset(PREFIX_EXCHANGE_PROTOCOLS);
        final Map<String, String[]> pInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            pInternal.put(exchange, protocolSubset.getStringArray(exchange));
        }
        exchangeToProtocols = Collections.unmodifiableMap(pInternal);

        final Configuration mdDepthSubset = properties.subset(PREFIX_EXCHANGE_DEPTH);
        final Map<String, Integer> dInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            dInternal.put(exchange, mdDepthSubset.getInteger(exchange, Exchange.valueOf(exchange).getMdDepth()));
        }
        exchangeToMDDepth = Collections.unmodifiableMap(dInternal);

        final Configuration nicExclusionSubset = properties.subset(PREFIX_EXCHANGE_EXCLUSION);
        final Map<String, String[]> nInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            nInternal.put(exchange, nicExclusionSubset.getStringArray(exchange));
        }
        exchangeToNICExclusions = Collections.unmodifiableMap(nInternal);

        final Configuration defaultFeedSpecSubset = properties.subset(PREFIX_EXCHANGE_DEFAULT_FEEDSPEC);
        final Map<String, Boolean> sInternal = new HashMap<>();
        for (final String exchange : exchangeList) {
            sInternal.put(exchange, defaultFeedSpecSubset.getBoolean(exchange, true));
        }
        exchangeToDefaultFeedSpec = Collections.unmodifiableMap(sInternal);
    }

    final Map<String, Integer> getExchangeToFeedFilter() {
        return exchangeToFeedFilter;
    }

    final Map<String, String[]> getExchangeToProtocols() {
        return exchangeToProtocols;
    }

    final Map<String, Integer> getExchangeToMDDepthMap() {
        return exchangeToMDDepth;
    }

    final Map<String, String[]> getExchangeToNICExclusions() {
        return exchangeToNICExclusions;
    }

    final Map<String, Boolean> getExchangeToDefaultFeedSpec() {
        return exchangeToDefaultFeedSpec;
    }

}
