package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.SimulationConfigKeys.PREFIX_EXCHANGE_ORDEREXECUTOR;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;

public class OrderMatchingConfig {

    private final Map<String, Map<DataType, Boolean>> exchangeOrderMatching = new HashMap<>();

    OrderMatchingConfig(final PropertiesConfiguration properties) {
        for (final Exchange exchange : Exchange.values()) {
            final Map<DataType, Boolean> matchingMap = new HashMap<>();

            for (final DataType type : DataType.values()) {
                final Configuration orderExecutorSubset = properties.subset(PREFIX_EXCHANGE_ORDEREXECUTOR + DelimiterUtil.CONFIG_KEY_DELIMETER + exchange.name());
                matchingMap.put(type, orderExecutorSubset.getBoolean(type.name(), true));
            }

            exchangeOrderMatching.put(exchange.name(), matchingMap);
        }
    }

    public Map<String, Map<DataType, Boolean>> getOrderMatching() {
        return exchangeOrderMatching;
    }

    public List<DataType> getExcludedOrderMatching(final String exchange) {
        return exchangeOrderMatching.get(exchange).entrySet().stream()
            .filter(entry -> entry.getValue() == Boolean.FALSE)
            .map(map -> map.getKey())
            .collect(Collectors.toList());
    }

}
