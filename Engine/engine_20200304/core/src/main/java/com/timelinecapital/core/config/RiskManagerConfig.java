package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.RiskManagerConfigKeys.EnableStrategyPriceCheck;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MaxCancelsPerIntervalPropertyKey;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MaxCrossPercentagePropertyKey;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MaxDrawdownPropertyKey;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MaxLossPropertyKey;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MaxRejectsPerIntervalPropertyKey;
import static com.timelinecapital.core.config.RiskManagerConfigKeys.MinLotSizePropertyKey;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import com.timelinecapital.commons.util.Pair;

public final class RiskManagerConfig {
    private static final String RISK_CONTROL_FILE = "risk_control.properties";

    private static RiskManagerConfig riskManagerConfig;

    private final Map<String, Supplier<?>> keyToValidator = new HashMap<>();
    private final Map<String, Integer> keyToInteger = new HashMap<>();
    private final Map<String, Double> keyToDouble = new HashMap<>();

    private final ValueCarrier carrier = new ValueCarrier();
    private final double maxDrawdown;
    private final double maxLoss;
    private final int maxRejectsPerInterval;
    private final int maxCancelsPerInterval;
    private final int minLotSize;

    private final Double maxCrossPercentage;

    // should not be changed at run-time
    private final boolean enableStrategyPriceCheck;

    private final PropertiesConfiguration properties;

    private RiskManagerConfig(File configFile) throws ConfigurationException {
        if (!configFile.isFile()) {
            configFile = new File(this.getClass().getClassLoader().getResource(configFile.getName()).getFile());
        }

        final Parameters params = new Parameters();
        final FileBasedConfigurationBuilder<FileBasedConfiguration> builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
            .configure(params.fileBased().setFile(configFile).setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
        properties = (PropertiesConfiguration) builder.getConfiguration();

        maxDrawdown = properties.getDouble(MaxDrawdownPropertyKey, -5000.0d);
        maxLoss = properties.getDouble(MaxLossPropertyKey, -5000.0d);
        maxRejectsPerInterval = properties.getInt(MaxRejectsPerIntervalPropertyKey, 15);
        maxCancelsPerInterval = properties.getInt(MaxCancelsPerIntervalPropertyKey, 60);
        maxCrossPercentage = properties.getDouble(MaxCrossPercentagePropertyKey, Double.NaN);
        minLotSize = properties.getInteger(MinLotSizePropertyKey, 1);

        enableStrategyPriceCheck = properties.getBoolean(EnableStrategyPriceCheck, true);
        load();
    }

    public static RiskManagerConfig getInstance() {
        return riskManagerConfig;
    }

    public static void buildConfig() throws ConfigurationException {
        if (riskManagerConfig == null) {
            riskManagerConfig = new RiskManagerConfig(new File(RISK_CONTROL_FILE));
        }
    }

    public static void forceBuildConfig() throws ConfigurationException {
        riskManagerConfig = new RiskManagerConfig(new File(RISK_CONTROL_FILE));
    }

    private void load() {
        keyToDouble.put(MaxDrawdownPropertyKey, maxDrawdown);
        keyToValidator.put(MaxDrawdownPropertyKey, () -> isValidLostControl(carrier));

        keyToDouble.put(MaxLossPropertyKey, maxLoss);
        keyToValidator.put(MaxLossPropertyKey, () -> isValidLostControl(carrier));

        keyToInteger.put(MaxRejectsPerIntervalPropertyKey, maxRejectsPerInterval);
        keyToValidator.put(MaxRejectsPerIntervalPropertyKey, () -> isValid(carrier));

        keyToInteger.put(MaxCancelsPerIntervalPropertyKey, maxCancelsPerInterval);
        keyToValidator.put(MaxCancelsPerIntervalPropertyKey, () -> isValid(carrier));

        keyToInteger.put(MinLotSizePropertyKey, minLotSize);
        keyToValidator.put(MinLotSizePropertyKey, () -> isValid(carrier));

        keyToDouble.put(MaxCrossPercentagePropertyKey, maxCrossPercentage);
        keyToValidator.put(MaxCrossPercentagePropertyKey, () -> isValidPercentage(carrier));
    }

    private void update(final String key, final Object value, final Class<?> classType) {
        if (value.getClass() == Integer.class || classType == Integer.class) {
            keyToInteger.put(key, Integer.parseInt(value.toString()));
            return;
        }
        if (value.getClass() == Double.class || classType == Double.class) {
            keyToDouble.put(key, Double.parseDouble(value.toString()));
            return;
        }

        throw new IllegalArgumentException("Type not acceptable: " + value.getClass().getName());
    }

    public final void checkAndApply(final String key, final Object value) throws ConfigurationException {
        carrier.setTargetValue(value);

        if (!keyToValidator.containsKey(key)) {
            throw new ConfigurationException("Incorrect risk control key: " + key);
        }

        @SuppressWarnings("unchecked")
        final Pair<Boolean, Class<?>> pair = (Pair<Boolean, Class<?>>) keyToValidator.get(key).get();
        if (!pair.getLeft()) {
            throw new ConfigurationException("Incorrect risk control setting: " + value);
        }

        update(key, value, pair.getRight());
    }

    public final double getMaxDrawdown() {
        return keyToDouble.get(MaxDrawdownPropertyKey);
    }

    public final double getMaxLoss() {
        return keyToDouble.get(MaxLossPropertyKey);
    }

    public final int getMaxRejectsPerInterval() {
        return keyToInteger.get(MaxRejectsPerIntervalPropertyKey);
    }

    public final int getMaxCancelsPerInterval() {
        return keyToInteger.get(MaxCancelsPerIntervalPropertyKey);
    }

    public final int getMinLotSize() {
        return keyToInteger.get(MinLotSizePropertyKey);
    }

    public final double getMaxCrossPercentage() {
        return keyToDouble.get(MaxCrossPercentagePropertyKey);
    }

    public final boolean enableStrategyPriceCheck() {
        return enableStrategyPriceCheck;
    }

    private Pair<Boolean, Class<?>> isValid(final ValueCarrier carrier) {
        if (carrier.getTargetValue().getClass() != Integer.class) {
            try {
                final int value = Integer.parseInt(carrier.getTargetValue().toString());
                return new Pair<>(value > 0, Integer.class);
                // return value > 0;
            } catch (final Exception e) {
                return new Pair<>(false, carrier.getClass());
            }
        }
        // return (Integer) carrier.getTargetValue() > 0;
        return new Pair<>((Integer) carrier.getTargetValue() > 0, Integer.class);
    }

    private Pair<Boolean, Class<?>> isValidLostControl(final ValueCarrier carrier) {
        if (carrier.getTargetValue().getClass() != Double.class) {
            try {
                final double value = Double.parseDouble(carrier.getTargetValue().toString());
                return new Pair<>(value < 0, Double.class);
                // return value < 0;
            } catch (final Exception e) {
                return new Pair<>(false, carrier.getClass());
            }
        }
        // return (Double) carrier.getTargetValue() < 0;
        return new Pair<>((Double) carrier.getTargetValue() < 0, Double.class);
    }

    private Pair<Boolean, Class<?>> isValidPercentage(final ValueCarrier carrier) {
        if (carrier.getTargetValue().getClass() != Double.class) {
            try {
                final double value = Double.parseDouble(carrier.getTargetValue().toString());
                return new Pair<>(value > 0 && value < 10.0d, Double.class);
            } catch (final Exception e) {
                return new Pair<>(false, carrier.getClass());
            }
        }
        // return (Double) carrier.getTargetValue() > 0 && (Double) carrier.getTargetValue() < 10.0d;
        return new Pair<>((Double) carrier.getTargetValue() > 0 && (Double) carrier.getTargetValue() < 10.0d, Double.class);
    }

    public final Map<String, ? extends Number> getConfigInMap() {
        final Stream<Entry<String, ? extends Number>> combined = Stream.concat(keyToInteger.entrySet().stream(), keyToDouble.entrySet().stream());
        return combined.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    // @Override
    // public String toString() {
    // final ObjectNode node = MAPPER.createObjectNode();
    // node.put(MaxDrawdownPropertyKey, maxDrawdown);
    // node.put(MaxLossPropertyKey, maxLoss);
    // node.put(MaxRejectsPerIntervalPropertyKey, maxRejectsPerInterval);
    // node.put(MaxCancelsPerIntervalPropertyKey, maxCancelsPerInterval);
    // node.put(MaxCrossPercentagePropertyKey, maxCrossPercentage);
    // node.put(EnableStrategyPriceCheck, enableStrategyPriceCheck);
    // return node.toString();
    // }

    private class ValueCarrier {

        Object targetValue;

        public void setTargetValue(final Object targetValue) {
            this.targetValue = targetValue;
        }

        public Object getTargetValue() {
            return targetValue;
        }

    }

    // public static void main(final String[] args) throws ConfigurationException {
    // final RiskManagerConfig config = new RiskManagerConfig(new File("risk_control.properties"));
    // config.checkAndApply("maxRejectsPerInterval", 100);
    // config.checkAndApply("minLotSize", 20);
    // System.out.println(config.keyToInteger);
    // System.out.println(config.keyToDouble);
    // config.checkAndApply("maxRejectsPerInterval", -1);
    // }

}
