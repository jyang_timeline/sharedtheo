package com.timelinecapital.core.config;

public class RiskManagerConfigKeys {

    static final String MaxDrawdownPropertyKey = "maxDrawdown";
    static final String MaxLossPropertyKey = "maxLoss";
    static final String MaxRejectsPerIntervalPropertyKey = "maxRejectsPerInterval";
    static final String MaxCancelsPerIntervalPropertyKey = "maxCancelsPerInterval";

    static final String MinLotSizePropertyKey = "minLotSize";

    static final String EnableStrategyPriceCheck = "enableStrategyPriceCheck";
    static final String MaxCrossPercentagePropertyKey = "maxCrossPercentage";

}
