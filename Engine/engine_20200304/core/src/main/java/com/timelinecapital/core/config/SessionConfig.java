package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_EXCHANGE_SESSIONS;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

public class SessionConfig extends ExchangeBaseConfig {

    private final Map<String, String[]> exchangeToTradingSessions;

    SessionConfig(final PropertiesConfiguration properties) {
        super(properties);
        final Configuration sessionSubset = properties.subset(PREFIX_EXCHANGE_SESSIONS);
        final Map<String, String[]> map = new HashMap<>();
        for (final String exchange : exchangeList) {
            map.put(exchange, sessionSubset.getStringArray(exchange));
        }
        exchangeToTradingSessions = Collections.unmodifiableMap(map);
    }

    Map<String, String[]> getExchangeToTradingSessions() {
        return exchangeToTradingSessions;
    }

}
