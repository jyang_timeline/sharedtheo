package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.SimulationConfigKeys.BROKER_KEY;
import static com.timelinecapital.core.config.SimulationConfigKeys.ENABLE_LOAD_CACHE_SHAREDTHEO;
import static com.timelinecapital.core.config.SimulationConfigKeys.ENABLE_ORDERACTION_UPDATE_BOOK;
import static com.timelinecapital.core.config.SimulationConfigKeys.ENABLE_SHAREDTHEO_DUMP;
import static com.timelinecapital.core.config.SimulationConfigKeys.ENABLE_TICK_UPDATE_BOOK;
import static com.timelinecapital.core.config.SimulationConfigKeys.FEED_DEFAULT;
import static com.timelinecapital.core.config.SimulationConfigKeys.PREFIX_EXCHANGE_TIMEFORMAT;
import static com.timelinecapital.core.config.SimulationConfigKeys.PREFIX_PATH;
import static com.timelinecapital.core.config.SimulationConfigKeys.PREFIX_SOURCE;
import static com.timelinecapital.core.config.SimulationConfigKeys.SHAREDTHEO_SAMPLE_INTERVAL;
import static com.timelinecapital.core.config.SimulationConfigKeys.SIMULAOTR_MODE_KEY;
import static com.timelinecapital.core.config.SimulationConfigKeys.THEO_PATH;
import static com.timelinecapital.core.config.SimulationConfigKeys.clockDriftKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.encoderVersionPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.endDatePropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.eventCapacityPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.persistCachePropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.simFillPriceBanningDelayPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.simIOCLatencyPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.simLatencyPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.simModelsPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.simSessionPropertyKey;
import static com.timelinecapital.core.config.SimulationConfigKeys.startDatePropertyKey;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.types.CommissionInfo;
import com.timelinecapital.core.types.SimulatorMode;

public class SimulationConfig {

    private static final String DEFAULT_TIMEFORMAT = "hhmmssffffff";

    private static SimulationConfig simulationConfig;

    private final PropertiesConfiguration properties;
    private final Configuration sourceSubset;
    private final Configuration pathSubset;

    private final OrderMatchingConfig matchingConfig;
    private final String targetBroker;
    private final FeeConfig feeConfig;

    private final String theoPath;
    private boolean enableShareTheoValueDump = false;
    private boolean useCachedTheoIfPossible = false;
    private final int sharetheoSampleInterval;

    private final int historicalDataEncoderVersion;
    private final int quoteEventCapacity;

    private final Map<String, String> exchangeToTimeFormat = new HashMap<>();

    private final long clockDrift;
    private final long simLatencyInMicros;
    private final long simIOCLatencyInMicros;
    private final long simFillPriceBanningDelayMicros;

    private final SimulatorMode simulatorMode;
    private final boolean enableTickUpdateBook;
    private final boolean enableOrderActionUpdateBook;

    private final String[] simModels;

    private final String simSession;
    private final DateTime startDate;
    private final DateTime endDate;

    private final String persistCacheDirectory;

    private SimulationConfig(final File simConfigFile, final String argStartDate, final String argEndDate, final String[] argSimModels) throws ConfigurationException {
        final Parameters params = new Parameters();
        final FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
            new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                .configure(params.fileBased().setFile(simConfigFile).setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
        properties = (PropertiesConfiguration) builder.getConfiguration();

        final Configuration timeformatSubset = properties.subset(PREFIX_EXCHANGE_TIMEFORMAT);
        for (final Exchange exchange : Exchange.values()) {
            exchangeToTimeFormat.put(exchange.name(), timeformatSubset.getString(exchange.name(), DEFAULT_TIMEFORMAT));
        }
        matchingConfig = new OrderMatchingConfig(properties);

        targetBroker = properties.getString(BROKER_KEY);
        feeConfig = new FeeConfig(properties, targetBroker);

        historicalDataEncoderVersion = properties.getInt(encoderVersionPropertyKey);
        quoteEventCapacity = properties.getInt(eventCapacityPropertyKey, 1024);

        sourceSubset = properties.subset(PREFIX_SOURCE);
        pathSubset = properties.subset(PREFIX_PATH);

        theoPath = properties.getString(THEO_PATH);
        enableShareTheoValueDump = properties.getBoolean(ENABLE_SHAREDTHEO_DUMP, false);
        useCachedTheoIfPossible = properties.getBoolean(ENABLE_LOAD_CACHE_SHAREDTHEO, false);
        sharetheoSampleInterval = properties.getInteger(SHAREDTHEO_SAMPLE_INTERVAL, 0);

        clockDrift = properties.getLong(clockDriftKey, 0l);
        simLatencyInMicros = properties.getLong(simLatencyPropertyKey);
        simIOCLatencyInMicros = properties.getLong(simIOCLatencyPropertyKey);
        simFillPriceBanningDelayMicros = properties.getLong(simFillPriceBanningDelayPropertyKey);

        simulatorMode = SimulatorMode.fromInt(properties.getInt(SIMULAOTR_MODE_KEY, 2));
        enableTickUpdateBook = properties.getBoolean(ENABLE_TICK_UPDATE_BOOK, false);
        enableOrderActionUpdateBook = properties.getBoolean(ENABLE_ORDERACTION_UPDATE_BOOK, false);

        if (argSimModels != null && argSimModels.length != 0) {
            simModels = argSimModels;
        } else {
            simModels = properties.getStringArray(simModelsPropertyKey);
        }

        simSession = properties.getString(simSessionPropertyKey);
        if (argStartDate != null && argEndDate != null) {
            startDate = DateTime.parse(argStartDate, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
            endDate = DateTime.parse(argEndDate, NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        } else {
            startDate = DateTime.parse(properties.getString(startDatePropertyKey), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
            endDate = DateTime.parse(properties.getString(endDatePropertyKey), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        }

        persistCacheDirectory = properties.getString(persistCachePropertyKey);
        if (persistCacheDirectory != null) {
            final File directory = new File(persistCacheDirectory);
            if (!directory.exists()) {
                directory.mkdir();
            }
        }
    }

    public static void buildConfig(final File simConfigFile, final String simStart, final String simEnd, final String[] simModels) throws ConfigurationException {
        if (simulationConfig == null) {
            simulationConfig = new SimulationConfig(simConfigFile, simStart, simEnd, simModels);
        }
    }

    public static List<String> getPathSubsets() {
        return IteratorUtils.toList(simulationConfig.pathSubset.getKeys());
    }

    public static String getPathPref(final String key) {
        if (simulationConfig.pathSubset.containsKey(key)) {
            return simulationConfig.pathSubset.getString(key);
        }
        return simulationConfig.pathSubset.getString(FEED_DEFAULT);
    }

    public static String getPath(final String key, final FeedType feed) {
        if (key.contentEquals(PREFIX_SOURCE)) {
            return simulationConfig.sourceSubset.getString(feed.name(), simulationConfig.sourceSubset.getString(FEED_DEFAULT));
        }
        final Configuration subset = simulationConfig.properties.subset(key);
        if (!subset.containsKey(key)) {
            return simulationConfig.properties.getString(key, StringUtils.EMPTY);
        }
        return subset.getString(key, StringUtils.EMPTY);
    }

    public static int getHistoricalDataEncoderVersion() {
        return simulationConfig.historicalDataEncoderVersion;
    }

    public static int getQuoteEventCapacity() {
        return simulationConfig.quoteEventCapacity;
    }

    public static String getExchangeTimeFormat(final String exchange) {
        return simulationConfig.exchangeToTimeFormat.getOrDefault(exchange, DEFAULT_TIMEFORMAT);
    }

    public static Map<DataType, Boolean> getExchangeToOrderExecutorMatching(final String exchange) {
        return simulationConfig.matchingConfig.getOrderMatching().get(exchange);
    }

    public static List<DataType> getExcludedOrderExecutorMatching(final String exchange) {
        return simulationConfig.matchingConfig.getExcludedOrderMatching(exchange);
    }

    public static String getTargetBroker() {
        return simulationConfig.targetBroker;
    }

    public static Map<Pair<Exchange, String>, Map<Side, CommissionInfo>> getBrokerCommissionInfo() {
        return simulationConfig.feeConfig.getBrokerToCommissionInfo();
    }

    public static Map<Side, CommissionInfo> getCommissionInfo(final String broker, final Exchange exchange) {
        return simulationConfig.feeConfig.getBrokerToCommissionInfo().get(new Pair<>(exchange, broker));
    }

    public static long getClockDrift() {
        return simulationConfig.clockDrift;
    }

    public static long getSimLatencyInMicros() {
        return simulationConfig.simLatencyInMicros;
    }

    public static long getSimIOCLatencyInMicros() {
        return simulationConfig.simIOCLatencyInMicros;
    }

    public static long getSimFillPriceBanningDelayMicros() {
        return simulationConfig.simFillPriceBanningDelayMicros;
    }

    public static SimulatorMode getSimulatorMode() {
        return simulationConfig.simulatorMode;
    }

    public static boolean isEnableTickUpdateBook() {
        return simulationConfig.enableTickUpdateBook;
    }

    public static boolean isEnableOrderActionUpdateBook() {
        return simulationConfig.enableOrderActionUpdateBook;
    }

    public static String[] getSimModels() {
        return simulationConfig.simModels;
    }

    public static String getSimSession() {
        return simulationConfig.simSession;
    }

    public static DateTime getStartDate() {
        return simulationConfig.startDate;
    }

    public static DateTime getEndDate() {
        return simulationConfig.endDate;
    }

    public static String getPersistCacheDirectory() {
        return simulationConfig.persistCacheDirectory;
    }

    public static String getTheoPath() {
        return simulationConfig.theoPath;
    }

    public static boolean isDumpSharedTheo() {
        return simulationConfig.enableShareTheoValueDump;
    }

    public static boolean isUsingCachedSharedTheo() {
        return simulationConfig.useCachedTheoIfPossible;
    }

    public static int getSharedTheoSampleInterval() {
        return simulationConfig.sharetheoSampleInterval;
    }

}
