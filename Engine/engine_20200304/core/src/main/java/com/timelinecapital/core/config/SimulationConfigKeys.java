package com.timelinecapital.core.config;

public class SimulationConfigKeys {

    static final String PREFIX_EXCHANGE_TIMEFORMAT = "Exchange.TimeFormat";
    static final String PREFIX_EXCHANGE_ORDEREXECUTOR = "Exchange.OrderExecutor";

    static final String PREFIX_FEE_OF_EXCHANGE = "Fee.Stock";
    static final String BROKER_KEY = "Fee.Stock.Broker";

    static final String PREFIX_PATH = "Path";
    static final String PREFIX_SOURCE = "Source";

    static final String FEED_DEFAULT = "DEFAULT";

    static final String THEO_PATH = "theoPath";
    static final String ENABLE_SHAREDTHEO_DUMP = "enableSharedTheoValueDump";
    static final String ENABLE_LOAD_CACHE_SHAREDTHEO = "useCachedSharedTheoIfPossible";
    static final String SHAREDTHEO_SAMPLE_INTERVAL = "SharedTheoSampleInterval";

    static final String encoderVersionPropertyKey = "dataEncoderVersion";
    static final String eventCapacityPropertyKey = "eventCapacity";

    static final String clockDriftKey = "clockDrift";

    static final String simLatencyPropertyKey = "simLatencyInMicros";
    static final String simIOCLatencyPropertyKey = "simIOCLatencyInMicros";
    static final String simFillPriceBanningDelayPropertyKey = "simFillPriceBanningDelayInMicros";

    static final String SIMULAOTR_MODE_KEY = "simulatorMode";
    static final String ENABLE_TICK_UPDATE_BOOK = "enableTickUpdateBook";
    static final String ENABLE_ORDERACTION_UPDATE_BOOK = "enableOrderActionUpdateBook";

    static final String simModelsPropertyKey = "simModels";

    static final String simSessionPropertyKey = "simSession";
    static final String startDatePropertyKey = "startDate";
    static final String endDatePropertyKey = "endDate";

    static final String persistCachePropertyKey = "persistCache";

}
