package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_STATUS_BASE;
import static com.timelinecapital.core.config.EngineConfigKeys.enableDefaultViewKey;
import static com.timelinecapital.core.config.EngineConfigKeys.enableDeltaViewKey;
import static com.timelinecapital.core.config.EngineConfigKeys.enableEngineMetricsKey;
import static com.timelinecapital.core.config.EngineConfigKeys.enablePositionViewKey;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

public class StatusBaseConfig extends ExchangeBaseConfig {

    private final boolean enableDefaultView;
    private final boolean enableDeltaView;
    private final boolean enableEngineMetrics;
    private final boolean enablePositionsView;

    StatusBaseConfig(final PropertiesConfiguration properties) {
        super(properties);
        final Configuration statusSubset = properties.subset(PREFIX_STATUS_BASE);

        enableDefaultView = statusSubset.getBoolean(enableDefaultViewKey, true);
        enableDeltaView = statusSubset.getBoolean(enableDeltaViewKey, false);
        enableEngineMetrics = statusSubset.getBoolean(enableEngineMetricsKey, false);
        enablePositionsView = statusSubset.getBoolean(enablePositionViewKey, false);
    }

    public boolean isEnableDefaultView() {
        return enableDefaultView;
    }

    public boolean isEnableDeltaView() {
        return enableDeltaView;
    }

    public boolean isEnableEngineMetrics() {
        return enableEngineMetrics;
    }

    public boolean isEnablePositionsView() {
        return enablePositionsView;
    }

}
