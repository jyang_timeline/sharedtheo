package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.PREFIX_STATUS_INTERVAL;
import static com.timelinecapital.core.config.EngineConfigKeys.defaultEngineViewIntervalKey;
import static com.timelinecapital.core.config.EngineConfigKeys.defaultStrategyViewIntervalKey;
import static com.timelinecapital.core.config.EngineConfigKeys.deltaViewIntervalKey;
import static com.timelinecapital.core.config.EngineConfigKeys.engineMetricsKey;
import static com.timelinecapital.core.config.EngineConfigKeys.positionViewIntervalKey;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;

public class StatusIntervalConfig extends ExchangeBaseConfig {

    private final int defaultStrategyViewInterval;
    private final int defaultEngineViewInterval;

    private final int deltaViewInterval;
    private final int engineMetricsInterval;
    private final int positionViewInterval;

    StatusIntervalConfig(final PropertiesConfiguration properties) {
        super(properties);
        final Configuration sIntervalSubset = properties.subset(PREFIX_STATUS_INTERVAL);

        defaultStrategyViewInterval = sIntervalSubset.getInt(defaultStrategyViewIntervalKey, 2);
        defaultEngineViewInterval = sIntervalSubset.getInt(defaultEngineViewIntervalKey, 2);

        deltaViewInterval = sIntervalSubset.getInt(deltaViewIntervalKey, 5);
        engineMetricsInterval = sIntervalSubset.getInt(engineMetricsKey, 5);
        positionViewInterval = sIntervalSubset.getInt(positionViewIntervalKey, 5);
    }

    public int getDefaultStrategyViewInterval() {
        return defaultStrategyViewInterval;
    }

    public int getDefaultEngineViewInterval() {
        return defaultEngineViewInterval;
    }

    public int getDeltaViewInterval() {
        return deltaViewInterval;
    }

    public int getEngineMetricsInterval() {
        return engineMetricsInterval;
    }

    public int getPositionViewInterval() {
        return positionViewInterval;
    }

}
