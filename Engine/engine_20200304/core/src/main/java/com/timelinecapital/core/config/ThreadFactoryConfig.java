package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.ringSizePropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.ringStrategyPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.socketPolicyPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.threadPolicyPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.threadPoolCountPropertyKey;

import org.apache.commons.configuration2.PropertiesConfiguration;

import com.nogle.core.config.EngineThreadPolicy;

public final class ThreadFactoryConfig {

    private final EngineThreadPolicy socketPolicy;
    private final EngineThreadPolicy threadPolicy;
    private final int threadPoolCount;
    private final int ringSize;
    private final String waitStrategy;

    ThreadFactoryConfig(final PropertiesConfiguration properties) {
        socketPolicy = EngineThreadPolicy.fromString(properties.getString(socketPolicyPropertyKey, EngineThreadPolicy.DEDICATE.toString()));
        threadPolicy = EngineThreadPolicy.fromString(properties.getString(threadPolicyPropertyKey, EngineThreadPolicy.DEDICATE.toString()));
        threadPoolCount = properties.getInt(threadPoolCountPropertyKey, 0);
        ringSize = properties.getInt(ringSizePropertyKey, 128);
        waitStrategy = properties.getString(ringStrategyPropertyKey, "DEDICATE");
    }

    final EngineThreadPolicy getSocketPolicy() {
        return socketPolicy;
    }

    final EngineThreadPolicy getThreadPolicy() {
        return threadPolicy;
    }

    final int getThreadPoolCount() {
        return threadPoolCount;
    }

    final int getRingSize() {
        return ringSize;
    }

    final String getRingStrategy() {
        return waitStrategy;
    }

}
