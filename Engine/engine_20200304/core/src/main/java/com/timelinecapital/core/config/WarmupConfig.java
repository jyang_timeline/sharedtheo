package com.timelinecapital.core.config;

import static com.timelinecapital.core.config.EngineConfigKeys.enableWarmupPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.warmupCyclesPropertyKey;
import static com.timelinecapital.core.config.EngineConfigKeys.warmupSymbolPropertyKey;

import org.apache.commons.configuration2.PropertiesConfiguration;

public class WarmupConfig {

    private final boolean enableWarmup;
    private final String warmupSymbol;
    private final int warmupCycles;

    WarmupConfig(final PropertiesConfiguration properties) {
        enableWarmup = properties.getBoolean(enableWarmupPropertyKey, true);
        warmupCycles = properties.getInt(warmupCyclesPropertyKey);
        warmupSymbol = properties.getString(warmupSymbolPropertyKey);
    }

    boolean isEnableWarmup() {
        return enableWarmup;
    }

    String getWarmupSymbol() {
        return warmupSymbol;
    }

    int getWarmupCycles() {
        return warmupCycles;
    }

}
