package com.timelinecapital.core.event;

import java.nio.ByteBuffer;

@Deprecated
public interface DataFeedEvent {

    void onBestPriceOrderDetail(long millis, ByteBuffer marketdata);

    void onOrderActions(long millis, ByteBuffer marketdata);

    void onOrderQueue(long millis, ByteBuffer marketdata);

}
