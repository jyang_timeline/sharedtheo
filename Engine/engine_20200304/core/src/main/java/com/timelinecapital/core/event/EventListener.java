package com.timelinecapital.core.event;

public interface EventListener<T> {

    void onEvent(T event);

}
