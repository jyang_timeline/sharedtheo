package com.timelinecapital.core.event;

public interface GenericStrategyEvent {

    void setKey(String key);

    String getKey();

}
