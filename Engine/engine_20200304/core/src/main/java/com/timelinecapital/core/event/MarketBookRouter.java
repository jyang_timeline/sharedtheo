package com.timelinecapital.core.event;

import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.event.EventHandler;
import com.timelinecapital.core.marketdata.type.MarketBook;

public final class MarketBookRouter implements EventHandler {

    private final StrategyQuoteSnapshot quoteSnapshot;
    private final StrategyUpdater strategyUpdater;
    private MarketBook marketBook;
    private final String handlerName;

    public MarketBookRouter(final StrategyQuoteSnapshot quoteSnapshot, final StrategyUpdater strategyUpdater) {
        this.quoteSnapshot = quoteSnapshot;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + quoteSnapshot.getContract().getSymbol() + "-BookRouter";
    }

    public final void set(final MarketBook marketBook) {
        this.marketBook = marketBook;
    }

    @Override
    public final void handle() {
        quoteSnapshot.setMarketBook(marketBook);
        strategyUpdater.onMarketBook(marketBook);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
