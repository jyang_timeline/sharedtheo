package com.timelinecapital.core.event;

import java.nio.ByteBuffer;

public interface MarketDataEvent {

    void onBook(long millis, ByteBuffer data);

    void onTick(long millis, ByteBuffer data);

    void onSnapshot(long millis, ByteBuffer data);

    void onBestPriceOrderDetail(long millis, ByteBuffer marketdata);

    void onOrderActions(long millis, ByteBuffer marketdata);

    void onOrderQueue(long millis, ByteBuffer marketdata);

}
