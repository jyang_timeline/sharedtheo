package com.timelinecapital.core.event;

public interface MarketOpenAware {

    void rewind();

    boolean isReady();

}
