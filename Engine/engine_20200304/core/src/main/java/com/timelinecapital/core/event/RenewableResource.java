package com.timelinecapital.core.event;

public interface RenewableResource {

    void reset();

}
