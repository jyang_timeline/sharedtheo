package com.timelinecapital.core.event;

import java.nio.ByteBuffer;

@Deprecated
public interface SnapshotFeedEvent {

    void onSnapshot(ByteBuffer data);

}
