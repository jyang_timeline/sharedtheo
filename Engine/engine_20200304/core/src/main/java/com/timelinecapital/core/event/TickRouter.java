package com.timelinecapital.core.event;

import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.event.EventHandler;
import com.timelinecapital.core.marketdata.type.Tick;

public final class TickRouter implements EventHandler {

    private final StrategyQuoteSnapshot quoteSnapshot;
    private final StrategyUpdater strategyUpdater;
    private Tick tick;
    private final String handlerName;

    public TickRouter(final StrategyQuoteSnapshot quoteSnapshot, final StrategyUpdater strategyUpdater) {
        this.quoteSnapshot = quoteSnapshot;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-" + quoteSnapshot.getContract().getSymbol() + "-TickRouter";
    }

    public final void set(final Tick tick) {
        this.tick = tick;
    }

    @Override
    public final void handle() {
        quoteSnapshot.setTick(tick);
        strategyUpdater.onTick(tick);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

}
