package com.timelinecapital.core.event.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.riskmanagement.PriceManager;

public class ContractInfoHandler implements GenericEventHandler<Contract> {

    private final ConcurrentLinkedQueue<Contract> queue = new ConcurrentLinkedQueue<>();
    private final StrategyUpdater strategyUpdater;
    private final Map<Contract, PriceManager> contractToPriceManager;
    private final String handlerName;

    public ContractInfoHandler(final StrategyUpdater strategyUpdater, final Map<Contract, PriceManager> contractToPriceManager) {
        this.strategyUpdater = strategyUpdater;
        this.contractToPriceManager = contractToPriceManager;
        handlerName = ContractInfoHandler.class.getSimpleName();
    }

    @Override
    public void wrap(final Contract event) {
        queue.add(event);
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public void handle() {
        final Contract contract = queue.poll();
        strategyUpdater.onPriceLimitUpdate(contract);
        contractToPriceManager.get(contract).onPriceLimitUpdate();
    }

}
