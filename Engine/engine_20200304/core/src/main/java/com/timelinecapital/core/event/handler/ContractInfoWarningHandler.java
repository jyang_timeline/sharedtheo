package com.timelinecapital.core.event.handler;

import com.nogle.core.EngineStatusView;
import com.nogle.strategy.types.Contract;

public class ContractInfoWarningHandler implements GenericEventHandler<Contract> {

    private final EngineStatusView view;
    private final String handlerName;

    private Contract contract;

    public ContractInfoWarningHandler(final EngineStatusView view) {
        this.view = view;
        handlerName = ContractInfoWarningHandler.class.getSimpleName();
    }

    @Override
    public void wrap(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public void handle() {
        if (contract.getLimitUp() == 0d && contract.getLimitDown() == 0d) {
            view.addWarnings(contract);
        } else {
            view.removeWarnings(contract);
        }
    }

}
