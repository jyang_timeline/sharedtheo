package com.timelinecapital.core.event.handler;

import com.nogle.core.strategy.event.FeedEventHandler;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class DirectFeedEventHandler<T> implements FeedEventHandler<T> {
    protected final int id;
    private final String name;
    protected T data;

    protected DirectFeedEventHandler(int id)
    {
        this.id = id;
        Type base = getClass().getGenericSuperclass();
        Type t = ((ParameterizedType)base).getActualTypeArguments()[0];
        String[] tokens = t.toString().split("\\.");
        this.name = tokens[tokens.length - 1] + "-" + id;
    }

    public T data() {
        return data;
    }

    @Override
    public void wrap(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getName() {
        return name;
    }
}
