package com.timelinecapital.core.event.handler;

import com.nogle.core.strategy.event.EventHandler;

public interface GenericEventHandler<G> extends EventHandler {

    void wrap(G event);

}
