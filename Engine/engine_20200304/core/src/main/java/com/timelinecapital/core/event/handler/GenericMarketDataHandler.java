package com.timelinecapital.core.event.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.event.FeedEventHandler;
import com.timelinecapital.core.event.EventListener;
import com.timelinecapital.strategy.types.MarketData;

public class GenericMarketDataHandler<T extends MarketData> implements FeedEventHandler<T> {

    private final ConcurrentLinkedQueue<T> queue;
    private final EventListener<T> listener;
    private final String handlerName;

    public GenericMarketDataHandler(final Class<T> typeParameterClass, final ConcurrentLinkedQueue<T> queue, final String strategyName, final EventListener<T> listener) {
        this.queue = queue;
        this.listener = listener;
        handlerName = strategyName + "-" + typeParameterClass.getSimpleName();
    }

    @Override
    public final void wrap(final T event) {
        queue.add(event);
    }

    @Override
    public final void handle() {
        final T event = queue.poll();
        listener.onEvent(event);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public final String toString() {
        return handlerName;
    }

}
