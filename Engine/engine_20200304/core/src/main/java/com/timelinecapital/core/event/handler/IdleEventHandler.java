package com.timelinecapital.core.event.handler;

import com.nogle.core.strategy.event.FeedEventHandler;
import com.timelinecapital.strategy.types.MarketData;

public class IdleEventHandler<T> implements FeedEventHandler<T> {

    static IdleEventHandler<? extends MarketData> INSTANCE = new IdleEventHandler<>();

    public static IdleEventHandler<? extends MarketData> get() {
        return INSTANCE;
    }

    @Override
    public void wrap(final T event) {
    }

    @Override
    public final void handle() {
    }

    @Override
    public final String getName() {
        return IdleEventHandler.class.getSimpleName();
    }

    @Override
    public final String toString() {
        return IdleEventHandler.class.getSimpleName();
    }

}
