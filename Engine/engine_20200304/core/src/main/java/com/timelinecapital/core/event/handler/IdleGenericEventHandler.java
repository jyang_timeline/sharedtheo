package com.timelinecapital.core.event.handler;

public enum IdleGenericEventHandler implements GenericEventHandler<Object> {

    INSTANCE;

    @SuppressWarnings("unchecked")
    public static <G> GenericEventHandler<G> get() {
        return (GenericEventHandler<G>) INSTANCE;
    }

    @Override
    public final void wrap(final Object event) {
    }

    @Override
    public final void handle() {
    }

    @Override
    public final String getName() {
        return IdleGenericEventHandler.class.getSimpleName();
    }

    @Override
    public final String toString() {
        return IdleGenericEventHandler.class.getSimpleName();
    }

}
