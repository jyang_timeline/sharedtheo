package com.timelinecapital.core.event.handler;

import com.timelinecapital.core.event.EventListener;
import com.timelinecapital.strategy.types.MarketData;

public enum IdleStrategyEventHandler implements EventListener<MarketData> {

    INSTANCE;

    @SuppressWarnings("unchecked")
    public static <T> EventListener<T> getInstance() {
        return (EventListener<T>) INSTANCE;
    }

    @Override
    public void onEvent(final MarketData event) {
        // Should always be empty here
    }

}
