package com.timelinecapital.core.event.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.event.FeedEventHandler;
import com.nogle.strategy.types.BookView;
import com.timelinecapital.core.event.EventListener;

public class MarketBookHandler<T extends BookView> implements FeedEventHandler<T> {

    private final ConcurrentLinkedQueue<T> marketBookQueue;
    private final EventListener<T> listener;
    private final String handlerName;

    public MarketBookHandler(final ConcurrentLinkedQueue<T> marketBookQueue, final String strategyName, final EventListener<T> listener) {
        this.marketBookQueue = marketBookQueue;
        this.listener = listener;
        handlerName = strategyName + "-BookQueue";
    }

    @Override
    public void wrap(final T book) {
        marketBookQueue.add(book);
    }

    @Override
    public final void handle() {
        final T book = marketBookQueue.poll();
        listener.onEvent(book);
        // quoteSnapshot.setMarketBook(book);
        // strategyUpdater.onMarketBook(book);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public String toString() {
        return handlerName;
    }

}
