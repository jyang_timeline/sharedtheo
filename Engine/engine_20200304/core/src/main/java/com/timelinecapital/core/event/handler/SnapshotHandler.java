package com.timelinecapital.core.event.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.event.FeedEventHandler;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.mktdata.type.SnapshotView;

public class SnapshotHandler<E extends SnapshotView<M, T>, M extends BookView & Quote, T extends TickView & Quote> implements FeedEventHandler<E> {

    private final ConcurrentLinkedQueue<E> snptQueue;
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public SnapshotHandler(final ConcurrentLinkedQueue<E> snptQueue, final StrategyUpdater strategyUpdater) {
        this.snptQueue = snptQueue;
        this.strategyUpdater = strategyUpdater;
        handlerName = strategyUpdater.getStrategyName() + "-SnptQueue";
    }

    @Override
    public void wrap(final E snapshot) {
        snptQueue.add(snapshot);
    }

    @Override
    public final void handle() {
        final E snapshot = snptQueue.poll();
        switch (snapshot.getUpdateType()) {
            case MARKETBOOK_ONLY:
                strategyUpdater.onBookFilter(snapshot.getMarketBook());
                break;
            case TRADE_ONLY:
            case ALL:
                strategyUpdater.onTickFilter(snapshot.getTick());
                strategyUpdater.onBookFilter(snapshot.getMarketBook());
                break;
        }
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public String toString() {
        return handlerName;
    }

}
