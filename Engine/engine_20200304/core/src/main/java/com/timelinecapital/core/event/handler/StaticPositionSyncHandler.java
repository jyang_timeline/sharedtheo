package com.timelinecapital.core.event.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Contract;

public class StaticPositionSyncHandler implements GenericEventHandler<Contract> {

    private final ConcurrentLinkedQueue<Contract> queue = new ConcurrentLinkedQueue<>();
    private final StrategyUpdater strategyUpdater;
    private final String handlerName;

    public StaticPositionSyncHandler(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
        handlerName = StaticPositionSyncHandler.class.getSimpleName();
    }

    @Override
    public void wrap(final Contract event) {
        queue.add(event);
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public void handle() {
        final Contract info = queue.poll();
        strategyUpdater.onPositionSync(info);
    }

}
