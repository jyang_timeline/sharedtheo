package com.timelinecapital.core.event.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.nogle.core.strategy.event.FeedEventHandler;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.core.event.EventListener;

public class TickHandler<T extends TickView> implements FeedEventHandler<T> {

    private final ConcurrentLinkedQueue<T> tickQueue;
    private final EventListener<T> listener;
    private final String handlerName;

    public TickHandler(final ConcurrentLinkedQueue<T> tickQueue, final String strategyName, final EventListener<T> listener) {
        this.tickQueue = tickQueue;
        this.listener = listener;
        handlerName = strategyName + "-TradeQueue";
    }

    @Override
    public void wrap(final T tick) {
        tickQueue.add(tick);
    }

    @Override
    public final void handle() {
        final T tick = tickQueue.poll();
        listener.onEvent(tick);
        // quoteSnapshot.setTick(tick);
        // strategyUpdater.onTick(tick);
    }

    @Override
    public final String getName() {
        return handlerName;
    }

    @Override
    public String toString() {
        return handlerName;
    }

}
