package com.timelinecapital.core.execution;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderLevelEntry;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.types.PriceLevelPolicy;

public final class IOCOrderBookBySide implements OrderBook {
    private static Logger log;

    private long inFlightOrderId;

    private final Side side;
    private final String symbol;
    private OrderHandler orderHandler;

    private PositionPolicy positionPolicy = PositionPolicy.AUTO;
    private long positionMax = 2048000l;
    private long quantity;
    private double price;

    public IOCOrderBookBySide(final Side side, final String symbol) {
        log = LogManager.getLogger(IOCOrderBookBySide.class);
        this.side = side;
        this.symbol = symbol;
    }

    @Override
    public final double getBestPrice() {
        return Double.NaN;
    }

    @Override
    public final List<? extends OrderLevelEntry> getOrderEntries() {
        return Collections.emptyList();
    }

    @Override
    public final PriceLevelPolicy getPriceLevelPolicy() {
        return PriceLevelPolicy.MultiplePriceLevel;
    }

    @Override
    public final long getOutstandingQty(final double price) {
        return 0;
    }

    @Override
    public final long getOutstandingOrderCount() {
        return 0;
    }

    @Override
    public final void setOrderHandler(final OrderHandler orderHandler) {
        this.orderHandler = orderHandler;
    }

    @Override
    public final void setPolicy(final PositionPolicy positionPolicy, final long positionMax) {
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
    }

    @Override
    public final void setQty(final long quantity, final double price) {
        this.quantity = quantity;
        this.price = price;
    }

    @Override
    public final void setQty(final long quantity, final double price, final int queuePosition) {
        this.quantity = quantity;
        this.price = price;
    }

    @Override
    public final void cancelOrder(final long clOrdId) {
    }

    @Override
    public final void clearFrom(final double price) {
    }

    @Override
    public final void clear(final double price) {
    }

    @Override
    public final void clear() {
    }

    @Override
    public final void reset() {
        inFlightOrderId = 0;
    }

    @Override
    public final void rewind() {
        inFlightOrderId = 0;
    }

    @Override
    public final boolean isReady() {
        return inFlightOrderId == 0;
    }

    @Override
    public final void commit(final long updateId, final long commitEventMicros) {
        if (quantity <= 0 || price <= 0 || Double.isNaN(price)) {
            log.warn("Invalid IOC-Order: Blocking {} {}: {} @ {}", symbol, side, quantity, price);
            return;
        }
        if (inFlightOrderId != 0) {
            log.warn("I-Blocking {} {}: in-flight: {}", symbol, side, inFlightOrderId);
            return;
        }

        try {
            final double priceSent = orderHandler.genOrderPrice(side, price);
            final long qtySent = orderHandler.genOrderQuantity(side, quantity, priceSent, 0);
            if (qtySent > 0 && priceSent > 0) {
                final long clOrdId = orderHandler.getNextClOrdId();
                try {
                    inFlightOrderId = clOrdId;
                    orderHandler.newIOCOrder(clOrdId, side, qtySent, price, positionPolicy, positionMax, commitEventMicros);
                } catch (final Exception e) {
                    inFlightOrderId = 0;
                    log.error("[{}] I-Commit FAILED {} {} {} {}", clOrdId, symbol, side, qtySent, priceSent);
                }
            } else {
                log.warn("I-Blocking {} {}: {} @ {}", symbol, side, qtySent, priceSent);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
        quantity = 0;
        price = Double.NaN;
    }

    @Override
    public final long orderAcked(final long clOrdId) {
        inFlightOrderId = clOrdId;
        return 0;
    }

    @Override
    public final void orderRejected(final long clOrdId, final RejectReason rejectReason) {
        inFlightOrderId = 0;
    }

    @Override
    public final void orderFilled(final long clOrdId, final long lastQty, final long remainingQty) {
        if (remainingQty == 0) {
            inFlightOrderId = 0;
        }
    }

    @Override
    public final long cancelAcked(final long clOrdId) {
        inFlightOrderId = 0;
        return 0;
    }

    @Override
    public final void cancelRejected(final long clOrdId, final RejectReason rejectReason) {
    }

    @Override
    public void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
        this.rewind();
    }

}
