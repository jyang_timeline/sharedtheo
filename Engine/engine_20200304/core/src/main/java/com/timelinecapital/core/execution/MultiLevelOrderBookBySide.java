package com.timelinecapital.core.execution;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.RejectReason;
import com.nogle.core.util.PriceUtils;
import com.nogle.core.util.SortedList;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderLevelEntry;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.pool.SuperPool;
import com.timelinecapital.core.types.PriceLevelPolicy;

public final class MultiLevelOrderBookBySide implements OrderBook {
    private static Logger log;

    private final SuperPool<OrderEntry> superPool = new SuperPool<>(OrderEntry.class);
    private final OrderEntryFactory factory = new OrderEntryFactory(superPool);
    private OrderEntryRecycler recycler;

    private final Map<Long, OrderEntry> clOrdIdToEntry = new LinkedHashMap<>(16);
    private final SortedList<OrderEntry> entries = new SortedList<>();
    private Iterator<OrderEntry> iterator;

    private final Side side;
    private final String symbol;
    private OrderHandler orderHandler;

    private PositionPolicy positionPolicy = PositionPolicy.AUTO;
    private long positionMax = 2048000l;

    public MultiLevelOrderBookBySide(final Side side, final String symbol) {
        log = LogManager.getLogger(MultiLevelOrderBookBySide.class);
        this.side = side;
        this.symbol = symbol;
        entries.setDescending(side == Side.BUY);

        try {
            final Constructor<OrderEntryRecycler> c = OrderEntryRecycler.class.getConstructor(int.class, superPool.getClass());
            final int chainSize = superPool.getChainSize();
            recycler = c.newInstance(chainSize, superPool);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final double getBestPrice() {
        if (entries.isEmpty()) {
            return Double.NaN;
        }
        return entries.get(0).getPrice();
    }

    @Override
    public final List<OrderEntry> getOrderEntries() {
        return entries;
    }

    @Override
    public final PriceLevelPolicy getPriceLevelPolicy() {
        return PriceLevelPolicy.MultiplePriceLevel;
    }

    @Override
    public final long getOutstandingQty(final double price) {
        return entries.stream().filter(o -> o.getPrice() == price).mapToLong(o -> (o.getQty() - o.getFilledQty())).sum();
    }

    @Override
    public final long getOutstandingOrderCount() {
        return entries.stream().filter(o -> o.isWorkingOrder()).count();
    }

    @Override
    public final void setOrderHandler(final OrderHandler orderHandler) {
        this.orderHandler = orderHandler;
    }

    @Override
    public final void setPolicy(final PositionPolicy positionPolicy, final long positionMax) {
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
    }

    @Override
    public final void setQty(final long quantity, final double price) {
        setQty(quantity, price, 0);
    }

    @Override
    public final void setQty(final long quantity, final double price, final int queuePosition) {
        final OrderEntry entry = factory.get();
        final double transPrice = orderHandler.genOrderPrice(side, price);
        final long transQuantity = orderHandler.genOrderQuantity(side, quantity, transPrice, entries.size());
        if (transQuantity <= 0) {
            log.warn("Unable to create new order entry: {}@{} -> {}@{}", quantity, price, transQuantity, transPrice);
            recycler.recycle(entry);
            return;
        }
        entry.setOrder(transQuantity, transPrice, queuePosition);
        entries.add(entry);
    }

    @Override
    public final void cancelOrder(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        entry.clear();
        try {
            entry.setSent(true);
            entry.setAcked(false);
            orderHandler.cancelDayOrder(entry.getClOrdId(), side, TradeClock.getCurrentMicrosOnly());
            entry.setDirty(false);
        } catch (final Exception e) {
            entry.setAcked(true);
            entry.setSent(false);
            log.error("[{}] X-Commit FAILED {} {} {}", entry.getClOrdId(), symbol, side, e.getMessage());
        }
    }

    @Override
    public final void clearFrom(final double price) {
        for (final OrderLevelEntry entry : entries) {
            if (PriceUtils.isOutsideOf(side, entry.getPrice(), price) && entry.isWorkingOrder()) {
                entry.clear();
            }
        }
    }

    @Override
    public final void clear(final double price) {
        for (final OrderLevelEntry entry : entries) {
            if (entry.getPrice() == price && entry.isWorkingOrder()) {
                entry.clear();
            }
        }
    }

    @Override
    public final void clear() {
        for (final OrderLevelEntry entry : entries) {
            if (entry.isWorkingOrder()) {
                entry.clear();
            }
        }
    }

    @Override
    public final void reset() {
        clOrdIdToEntry.clear();
        entries.clear();
    }

    @Override
    public final void rewind() {
        clOrdIdToEntry.clear();
        entries.clear();
    }

    @Override
    public final boolean isReady() {
        return clOrdIdToEntry.isEmpty() && entries.isEmpty();
    }

    @Override
    public final void commit(final long updateId, final long commitEventMicros) {
        iterator = entries.iterator();
        // TODO use disruptor to speed up commit work flow
        while (iterator.hasNext()) {
            final OrderEntry entry = iterator.next();
            // OrderEntry: no update or waiting for ACK
            if (!entry.isDirty() || (entry.isSent() && !entry.isAcked())) {
                continue;
            }
            // OrderEntry: To be canceled
            if (entry.isValid() && !entry.isSent() && entry.isAcked()) {
                try {
                    entry.setSent(true);
                    entry.setAcked(false);
                    // orderHandler.cancelDayOrder(entry.getClOrdId(), side, TradeClock.getCurrentMicrosOnly());
                    orderHandler.cancelDayOrder(entry.getClOrdId(), side, commitEventMicros);
                    entry.setDirty(false);
                } catch (final Exception e) {
                    entry.setAcked(true);
                    entry.setSent(false);
                    log.error("[{}] X-Commit FAILED {} {}", entry.getClOrdId(), symbol, side, e.getMessage());
                }
                continue;
            }
            // OrderEntry: Sending New
            if (entry.isValid() && !entry.isSent() && !entry.isAcked() && entry.getQty() > 0) {
                final long clOrdId = orderHandler.getNextClOrdId();
                try {
                    entry.setClOrdId(clOrdId);
                    entry.setSent(true);
                    clOrdIdToEntry.put(clOrdId, entry);
                    // orderHandler.newDayOrder(clOrdId, side, entry.getQty(), entry.getPrice(), positionPolicy, positionMax,
                    // TradeClock.getCurrentMicrosOnly());
                    orderHandler.newDayOrder(clOrdId, side, entry.getQty(), entry.getPrice(), positionPolicy, positionMax, commitEventMicros);
                    entry.setDirty(false);
                } catch (final Exception e) {
                    clOrdIdToEntry.remove(clOrdId);
                    iterator.remove();
                    log.error("[{}] O-Commit FAILED {} {} {} {} {}", clOrdId, symbol, side, entry.getQty(), entry.getPrice(), e.getMessage());
                    recycler.recycle(entry);
                }
                continue;
            }
            // if (!entry.isValid()) {
            log.warn("Invalid order {}", entry);
            iterator.remove();
            recycler.recycle(entry);
            // }
        }
        // signal all commit event is completed
        orderHandler.onBatchOrder(side);
    }

    @Override
    public final long orderAcked(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        if (entry == null) {
            log.warn("[{}] O-Acked but does not exist in Map", clOrdId);
            return 0;
        }
        entry.setAcked(true);
        return entry.getQty();
    }

    @Override
    public final void orderRejected(final long clOrdId, final RejectReason rejectReason) {
        final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
        if (entry == null) {
            log.warn("[{}] O-Reject but does not exist in Map", clOrdId);
            return;
        }
        entries.remove(entry);
        recycler.recycle(entry);
    }

    @Override
    public final void orderFilled(final long clOrdId, final long fillQty, final long remainingQty) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        if (entry == null) {
            log.error("Unknown FILL: ClOrdId should be EOL {}", clOrdId);
            return;
        }
        if (remainingQty == 0) {
            clOrdIdToEntry.remove(clOrdId);
            entries.remove(entry);
            recycler.recycle(entry);
        } else {
            entry.setFilledQty(fillQty);
        }
    }

    @Override
    public final long cancelAcked(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
        if (entry == null) {
            log.warn("[{}] X-Acked but does not exist in Map", clOrdId);
            orderHandler.onScheduledOrder(side);
            return 0;
        }
        entry.setAcked(true);
        entries.remove(entry);
        final long qty = entry.getQty();
        recycler.recycle(entry);

        orderHandler.onScheduledOrder(side);
        return qty;
    }

    @Override
    public final void cancelRejected(final long clOrdId, final RejectReason rejectReason) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        if (entry == null) {
            log.warn("[{}] X-Reject but does not exist in Map (EOL)", clOrdId);
            orderHandler.onScheduledOrder(side);
            return;
        }
        entry.setAcked(true);
        // TODO: ? recycler.recycle(entry)
        orderHandler.onScheduledOrder(side);
    }

    @Override
    public void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
        try {
            final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
            if (entry != null) {
                iterator = entries.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().getClOrdId() == clOrdId) {
                        iterator.remove();
                    }
                }
                recycler.recycle(entry);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    // public static void main(final String[] args) {
    // final MultiLevelOrderBookBySide book1 = new MultiLevelOrderBookBySide(null, Side.BUY, "cu1712");
    // final MultiLevelOrderBookBySide book2 = new MultiLevelOrderBookBySide(null, Side.SELL, "cu1712");
    //
    // final List<OrderEntry> test1 = book1.getOrderEntries();
    // final List<OrderEntry> test2 = book2.getOrderEntries();
    //
    // book1.setQty(5, 45);
    // book1.setQty(3, 50);
    //
    // book2.setQty(5, 55);
    // book2.setQty(1, 60);
    //
    // System.out.println(book1.getBestPrice());
    // System.out.println(book1.getOutstandingQty(50));
    // System.out.println(book1.getOutstandingOrderCount());
    //
    // book1.reset();
    //
    // System.out.println(book2.getBestPrice());
    // System.out.println(book2.getOutstandingQty(55));
    // System.out.println(book2.getOutstandingOrderCount());
    //
    // System.out.println(book1.getBestPrice());
    // System.out.println(book1.getOutstandingQty(50));
    // System.out.println(book1.getOutstandingOrderCount());
    // }

}
