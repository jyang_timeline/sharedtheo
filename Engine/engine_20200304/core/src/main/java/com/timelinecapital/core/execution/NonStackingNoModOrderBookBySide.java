package com.timelinecapital.core.execution;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.RejectReason;
import com.nogle.core.util.PriceUtils;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderLevelEntry;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.types.PriceLevelPolicy;

public final class NonStackingNoModOrderBookBySide implements OrderBook {
    private static Logger log;

    private final Map<Long, ExchangeOrder> clOrdIdToOrder = new LinkedHashMap<>(16);

    private final Side side;
    private final String symbol;
    private OrderHandler orderHandler;

    private volatile boolean dirty;
    private boolean needRecommit;

    private int clientTotalQty = 0;
    private int exchangeTotalQty = 0;
    private int inFlightQty = 0;
    private int blockingCount = 0;

    private PositionPolicy positionPolicy = PositionPolicy.AUTO;
    private long positionMax = 2048000l;
    private long desiredQty;
    private double desiredPrice;

    public NonStackingNoModOrderBookBySide(final Side side, final String symbol) {
        log = LogManager.getLogger(NonStackingNoModOrderBookBySide.class);
        this.side = side;
        this.symbol = symbol;
    }

    @Override
    public final double getBestPrice() {
        if (exchangeTotalQty == 0) {
            return Double.NaN;
        }
        double bestPrice = Double.NaN;
        for (final ExchangeOrder order : clOrdIdToOrder.values()) {
            bestPrice = PriceUtils.getInsidePrice(side, bestPrice, order.price);
        }
        return bestPrice;
    }

    @Override
    public List<? extends OrderLevelEntry> getOrderEntries() {
        // TODO Auto-generated method stub
        return Collections.emptyList();
    }

    @Override
    public final PriceLevelPolicy getPriceLevelPolicy() {
        return PriceLevelPolicy.SinglePriceLevel;
    }

    @Override
    public final long getOutstandingQty(final double price) {
        if (price == getBestPrice()) {
            return exchangeTotalQty;
        } else {
            return 0;
        }
    }

    @Override
    public final long getOutstandingOrderCount() {
        return clOrdIdToOrder.size();
    }

    @Override
    public final void setOrderHandler(final OrderHandler orderHandler) {
        this.orderHandler = orderHandler;
    }

    @Override
    public final void setPolicy(final PositionPolicy positionPolicy, final long positionMax) {
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
    }

    @Override
    public final void setQty(final long quantity, final double price) {
        dirty = dirty || quantity != desiredQty || price != desiredPrice;
        desiredQty = quantity;
        desiredPrice = price;
    }

    @Override
    public final void setQty(final long quantity, final double price, final int queuePosition) {
        dirty = dirty || quantity != desiredQty || price != desiredPrice;
        desiredQty = quantity;
        desiredPrice = price;

        // TODO queue position
    }

    @Override
    public final void cancelOrder(final long clOrdId) {
        final ExchangeOrder order = clOrdIdToOrder.get(clOrdId);
        if (order != null) {
            cancelOrder(clOrdId, order, TradeClock.getCurrentMicrosOnly());
        }
    }

    @Override
    public final void clearFrom(final double price) {
        if (price == desiredPrice) {
            clear();
        }
    }

    @Override
    public final void clear(final double price) {
        if (price == desiredPrice) {
            clear();
        }
    }

    @Override
    public final void clear() {
        dirty = dirty || desiredQty != 0;
        needRecommit = false;
        desiredQty = 0;
        desiredPrice = 0;
    }

    @Override
    public final void reset() {
        blockingCount = exchangeTotalQty = clientTotalQty = inFlightQty = 0;
        clOrdIdToOrder.clear();
        clear();
    }

    @Override
    public final void rewind() {
        blockingCount = exchangeTotalQty = clientTotalQty = inFlightQty = 0;
        clOrdIdToOrder.clear();
        clear();
    }

    @Override
    public final boolean isReady() {
        return clOrdIdToOrder.isEmpty() && !dirty;
    }

    @Override
    public final void commit(final long updateId, final long commitEventMicros) {
        if (!dirty) {
            return;
        }

        if (exchangeTotalQty != clientTotalQty) {
            if (blockingCount > 10) {
                blockingCount++;
                log.error("O-Blocking {} #{} times: Irregular EXPT/ACTL {}/{}", symbol, blockingCount, exchangeTotalQty, clientTotalQty);
            } else {
                blockingCount++;
                log.warn("O-Blocking {} #{} times: EXPT/ACTL {}/{}", symbol, blockingCount, exchangeTotalQty, clientTotalQty);
            }
            needRecommit = true;
            return;
        }
        blockingCount = 0;

        /*
         * Check existing order: cancel or add
         */
        long quantityToSend = desiredQty;
        for (final Entry<Long, ExchangeOrder> entry : clOrdIdToOrder.entrySet()) {
            final ExchangeOrder order = entry.getValue();
            if (desiredPrice != order.price || quantityToSend < order.exchangeQty) {
                cancelOrder(entry.getKey(), order, commitEventMicros);
            } else {
                quantityToSend -= order.exchangeQty;
            }
        }

        /*
         * New order or signal new order after cancel message
         */
        dirty = false;
        if (quantityToSend > 0) {
            if (exchangeTotalQty > clientTotalQty) {
                needRecommit = true;
                dirty = true;
            } else {
                newOrder(quantityToSend, desiredPrice, commitEventMicros);
            }
        }

    }

    private void cancelOrder(final long clOrdId, final ExchangeOrder order, final long commitEventMicros) {
        try {
            inFlightQty += order.exchangeQty;
            clientTotalQty -= order.exchangeQty;
            try {
                order.cancel();
                orderHandler.cancelDayOrder(clOrdId, side, commitEventMicros);
            } catch (final Exception e) {
                clientTotalQty += order.exchangeQty;
                inFlightQty -= order.exchangeQty;
                log.error("[{}] X-Commit FAILED {} {}", clOrdId, symbol, side);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void newOrder(final long quantity, final double price, final long commitEventMicros) {
        try {
            final double priceSent = orderHandler.genOrderPrice(side, price);
            final long qtySent = orderHandler.genOrderQuantity(side, quantity, priceSent, clOrdIdToOrder.size());

            if (qtySent > 0 && priceSent > 0) {
                final long clOrdId = orderHandler.getNextClOrdId();
                clOrdIdToOrder.put(clOrdId, new ExchangeOrder(qtySent, priceSent));
                clientTotalQty += qtySent;
                try {
                    orderHandler.newDayOrder(clOrdId, side, qtySent, priceSent, positionPolicy, positionMax, commitEventMicros);
                } catch (final Exception e) {
                    clientTotalQty -= qtySent;
                    clOrdIdToOrder.remove(clOrdId);
                    log.error("[{}] O-Commit FAILED {} {} {} {}", clOrdId, symbol, side, qtySent, priceSent);
                }
            } else {
                log.info("O-Blocking {} {}: {}@{}", symbol, side, qtySent, priceSent);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final long orderAcked(final long clOrdId) {
        final ExchangeOrder order = clOrdIdToOrder.get(clOrdId);
        if (order == null) {
            log.warn("[{}] O-Acked but does not exist in Map", clOrdId);
            return 0;
        }
        order.ack();
        exchangeTotalQty += order.exchangeQty;
        dirty = true;
        return order.exchangeQty;
    }

    @Override
    public final void orderRejected(final long clOrdId, final RejectReason rejectReason) {
        final ExchangeOrder order = clOrdIdToOrder.remove(clOrdId);
        if (order == null) {
            log.warn("[{}] O-Rejected but does not exist in Map", clOrdId);
            return;
        }
        clientTotalQty -= order.clientQty;
        dirty = true;
    }

    @Override
    public final void orderFilled(final long clOrdId, final long lastQty, final long remainingQty) {
        ExchangeOrder order;
        if (remainingQty == 0) {
            order = clOrdIdToOrder.remove(clOrdId);
        } else {
            order = clOrdIdToOrder.get(clOrdId);
        }

        if (order == null) {
            log.warn("Unknown FILL: ClOrdId is missing {}", clOrdId);
            return;
        }
        order.fill(lastQty);
        exchangeTotalQty -= lastQty;
        clientTotalQty -= lastQty;

        if (remainingQty == 0 && inFlightQty > 0) {
            clientTotalQty += inFlightQty;
            inFlightQty = 0;
        }
        dirty = true;
        needRecommit = false;
    }

    @Override
    public final long cancelAcked(final long clOrdId) {
        final ExchangeOrder order = clOrdIdToOrder.remove(clOrdId);
        if (order == null) {
            log.warn("[{}] X-Acked but does not exist in Map", clOrdId);
            return 0;
        }

        if (inFlightQty - order.exchangeQty < 0) {
            exchangeTotalQty -= order.exchangeQty;
            clientTotalQty -= order.exchangeQty;
            inFlightQty = 0;
            log.warn("[{}] X-Acked without a cancel request", clOrdId);
        } else {
            exchangeTotalQty -= order.exchangeQty;
            clientTotalQty -= order.clientQty;
            inFlightQty -= (order.exchangeQty - order.clientQty);
        }
        order.ack();

        dirty = true;
        if (needRecommit) {
            needRecommit = false;
            log.info("[{} Recommit] X-Acked {} {} {}@{}", clOrdId, symbol, side, desiredQty, desiredPrice);
            commit(clOrdId, TradeClock.getCurrentMicrosOnly());
        }
        return order.exchangeQty;
    }

    @Override
    public final void cancelRejected(final long clOrdId, final RejectReason rejectReason) {
        final ExchangeOrder order = clOrdIdToOrder.get(clOrdId);
        if (order == null) {
            log.info("[{}] is in the status EOL", clOrdId);
            return;
        }
        clientTotalQty += inFlightQty;
        inFlightQty -= (order.exchangeQty - order.clientQty);
        dirty = true;
    }

    @Override
    public void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
        switch (requestType) {
            case OrderEntry:
                this.orderRejected(clOrdId, RejectReason.C);
                break;
            case OrderCancel:
                this.cancelRejected(clOrdId, RejectReason.C);
                break;
            case OrderModify:
                break;
        }
    }

    @Override
    public final int getBlockingCount() {
        return blockingCount;
    }

    private class ExchangeOrder {
        private final double price;
        private long clientQty;
        private long exchangeQty;

        private ExchangeOrder(final long quantity, final double price) {
            clientQty = quantity;
            exchangeQty = 0;
            this.price = price;
        }

        private void fill(final long fillQty) {
            clientQty -= fillQty;
            exchangeQty -= fillQty;
        }

        private void cancel() {
            clientQty = 0;
        }

        private void ack() {
            exchangeQty = clientQty;
        }

    }

}