package com.timelinecapital.core.execution;

import java.util.List;

import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderLevelEntry;
import com.nogle.strategy.types.PositionPolicy;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.event.RenewableResource;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.types.PriceLevelPolicy;

public interface OrderBook extends RenewableResource, MarketOpenAware {

    double getBestPrice();

    List<? extends OrderLevelEntry> getOrderEntries();

    PriceLevelPolicy getPriceLevelPolicy();

    long getOutstandingQty(double price);

    long getOutstandingOrderCount();

    void setOrderHandler(OrderHandler orderHandler);

    void setPolicy(PositionPolicy positionPolicy, long positionMax);

    void setQty(long quantity, double price);

    void setQty(long quantity, double price, int queuePosition);

    void cancelOrder(long clOrdId);

    void clear(double price);

    void clearFrom(double price);

    void clear();

    void commit(long updateId, long commitEventMicros);

    long orderAcked(long clOrdId);

    void orderRejected(long clOrdId, RejectReason rejectReason);

    void orderFilled(long clOrdId, long fillQty, long remainingQty);

    long cancelAcked(long clOrdId);

    void cancelRejected(long clOrdId, RejectReason rejectReason);

    void onRequestFail(long clOrdId, ExecRequestType requestType);

    default int getBlockingCount() {
        return 0;
    }

}
