package com.timelinecapital.core.execution;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.nogle.strategy.types.OrderLevelEntry;
import com.timelinecapital.core.pool.Reusable;

public class OrderEntry implements OrderLevelEntry, Reusable<OrderEntry>, Comparable<OrderEntry> {

    private OrderEntry next = null;

    private boolean dirty = false;
    private boolean sent = false;
    private boolean acked = false;

    private long clOrdId;
    private long filledQty;
    private long qty;
    private double price;
    private int estimatedQueuePosition;

    @Override
    public boolean isDirty() {
        return dirty;
    }

    @Override
    public boolean isSent() {
        return sent;
    }

    @Override
    public boolean isAcked() {
        return acked;
    }

    public boolean isValid() {
        return dirty && price != 0.0;
    }

    public boolean isInflight() {
        return sent && !acked;
    }

    @Override
    public boolean isWorkingOrder() {
        return !dirty && sent && acked && clOrdId != 0;
    }

    @Override
    public long getClOrdId() {
        return clOrdId;
    }

    @Override
    public long getFilledQty() {
        return filledQty;
    }

    @Override
    public long getQty() {
        return qty;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public int getEstimatedQueuePosition() {
        return estimatedQueuePosition;
    }

    @Override
    public void setEstimatedQueuePosition(final int queuePosition) {
        estimatedQueuePosition = queuePosition;
    }

    @Override
    public void clear() {
        qty = 0;
        dirty = true;
        sent &= !acked;
        // if (!acked && sent) {
        // sent = true;
        // } else {
        // sent = false;
        // }
    }

    public void setOrder(final long qty, final double price) {
        setOrder(qty, price, 0);
    }

    public void setOrder(final long qty, final double price, final int estimatedQueuePosition) {
        this.qty = qty;
        this.price = price;
        this.estimatedQueuePosition = estimatedQueuePosition;
        filledQty = 0;
        dirty = true;
        sent = false;
        acked = false;
    }

    public void setDirty(final boolean isDirty) {
        dirty = isDirty;
    }

    public void setSent(final boolean isSent) {
        sent = isSent;
    }

    public void setAcked(final boolean isAcked) {
        acked = isAcked;
    }

    public void setClOrdId(final long clOrdId) {
        this.clOrdId = clOrdId;
    }

    public void setFilledQty(final long filledQty) {
        this.filledQty += filledQty;
    }

    public void setQty(final long qty) {
        this.qty = qty;
        dirty = true;
    }

    @Override
    public OrderEntry getNext() {
        return next;
    }

    @Override
    public void setNext(final OrderEntry next) {
        this.next = next;
    }

    @Override
    public void reset() {
        next = null;
        dirty = false;
        sent = false;
        acked = false;
        clOrdId = 0;
        filledQty = 0;
        qty = 0;
        price = 0;
        estimatedQueuePosition = 0;
    }

    @Override
    public int compareTo(final OrderEntry o) {
        if (this.price > o.price) {
            return 1;
        } else if (this.price < o.price) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("filledQty", filledQty)
            .append("qty", qty)
            .append("price", price)
            .append("clOrdId", clOrdId)
            .append("isDirty", dirty)
            .append("isSent", sent)
            .append("isAcked", acked)
            .toString();
    }

}
