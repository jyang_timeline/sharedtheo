package com.timelinecapital.core.execution;

import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class OrderEntryFactory implements PoolFactory<OrderEntry> {

    private final SuperPool<OrderEntry> superPool;

    private OrderEntry root;

    public OrderEntryFactory(final SuperPool<OrderEntry> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public OrderEntry get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final OrderEntry obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
