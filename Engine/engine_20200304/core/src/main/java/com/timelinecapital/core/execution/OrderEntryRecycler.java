package com.timelinecapital.core.execution;

import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class OrderEntryRecycler implements Recycler<OrderEntry> {

    private final SuperPool<OrderEntry> superPool;

    private OrderEntry root;

    private final int recycleSize;
    private int count = 0;

    public OrderEntryRecycler(final int recycleSize, final SuperPool<OrderEntry> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = OrderEntry.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final OrderEntry obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
