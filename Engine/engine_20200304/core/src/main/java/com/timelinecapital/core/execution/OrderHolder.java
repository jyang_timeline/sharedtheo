package com.timelinecapital.core.execution;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.OrderLevelEntry;
import com.nogle.strategy.types.OrderView;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.event.MarketOpenAware;
import com.timelinecapital.core.execution.handler.ClosePositionHandler;
import com.timelinecapital.core.execution.handler.DayOrderMessageHandler;
import com.timelinecapital.core.execution.handler.FlatPositionHandler;
import com.timelinecapital.core.execution.handler.IOCOrderMessageHandler;
import com.timelinecapital.core.execution.handler.LimitOrderHandler;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.types.PriceLevelPolicy;

public class OrderHolder implements OrderView, MarketOpenAware {
    private static Logger log;

    private final Contract contract;
    private final TimeCondition tif;
    private final Side side;

    private final OrderManagementView mgmtView;
    private final StrategyView strategyView;

    private final OrderBook orderBook;
    private final TradeEvent execRptHandler;

    private final OrderHandler orderHandler;
    private final FlatPositionHandler exitHandler;
    private final ClosePositionHandler closingHandler;

    public OrderHolder(final ClientOrderIdGenerator idGenerator, final Contract contract, final TimeCondition tif, final Side side,
        final TradeAppendix tradeAppendix, final OrderSender orderSender, final String threadContext, final ExecReportSink statsCollector) {

        this.contract = contract;
        this.tif = tif;
        this.side = side;

        mgmtView = new OrderManagementView();
        strategyView = CoreController.getInstance().getStatusView(idGenerator.getStrategyId());

        orderBook = genNewOrderBook(tradeAppendix.getPriceLevelPolicy());
        execRptHandler = genTradeEvent(threadContext, tradeAppendix.getPositionTracker(), statsCollector);

        orderHandler = new LimitOrderHandler(contract, idGenerator, orderSender, tradeAppendix, execRptHandler, mgmtView);
        exitHandler = new FlatPositionHandler(contract, idGenerator, orderSender, tradeAppendix, execRptHandler, mgmtView);
        closingHandler = new ClosePositionHandler(contract, idGenerator, orderSender, tradeAppendix, execRptHandler, mgmtView);

        orderBook.setOrderHandler(orderHandler);

        log = LogManager.getLogger(OrderHolder.class);
    }

    private OrderBook genNewOrderBook(final PriceLevelPolicy orderViewType) {
        switch (tif) {
            case GFD:
                if (PriceLevelPolicy.SinglePriceLevel.equals(orderViewType)) {
                    return new NonStackingNoModOrderBookBySide(side, contract.getSymbol());
                    // TODO replaced with SingleLevelOrderBookBySide(orderHandler, side, contract.getSymbol());
                } else {
                    return new MultiLevelOrderBookBySide(side, contract.getSymbol());
                }
            case IOC:
                return new IOCOrderBookBySide(side, contract.getSymbol());
            default:
                throw new RuntimeException("Unsupported type of TimeCondition");
        }
    }

    private TradeEvent genTradeEvent(final String threadContext, final PositionTracker positionTracker, final ExecReportSink statsCollector) {
        switch (tif) {
            case GFD:
                return new DayOrderMessageHandler(contract, side, threadContext, orderBook, positionTracker, mgmtView, statsCollector);
            case IOC:
                return new IOCOrderMessageHandler(contract, side, threadContext, orderBook, positionTracker, mgmtView, statsCollector);
            default:
                throw new RuntimeException("Unsupported type of TimeCondition");
        }
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final Side getSide() {
        return side;
    }

    @Override
    public final double getBestPrice() {
        return orderBook.getBestPrice();
    }

    @Override
    public final List<? extends OrderLevelEntry> getOutstandingOrders() {
        return orderBook.getOrderEntries();
    }

    @Override
    public final long getOutstandingQty(final double price) {
        return orderBook.getOutstandingQty(price);
    }

    @Override
    public final boolean isSinglePriceOnly() {
        return PriceLevelPolicy.SinglePriceLevel.equals(orderBook.getPriceLevelPolicy());
    }

    public long getOutstandingOrderCount() {
        return orderBook.getOutstandingOrderCount();
    }

    @Override
    public final void setPolicy(final PositionPolicy positionPolicy, final long positionMax) {
        orderBook.setPolicy(positionPolicy, positionMax);
    }

    @Override
    public final void setOrder(final long quantity, final double price) {
        if (mgmtView.getPermission() && Double.isFinite(price)) {
            orderBook.setQty(quantity, price);
        }
    }

    @Override
    public final void setOrder(final long quantity, final double price, final int queuePosition) {
        if (mgmtView.getPermission() && Double.isFinite(price)) {
            orderBook.setQty(quantity, price, queuePosition);
        }
    }

    @Override
    public final void clearOrder(final long clOrdId) {
        orderBook.cancelOrder(clOrdId);
    }

    @Override
    public final void clearOrder(final double price) {
        orderBook.clear(price);
    }

    @Override
    public final void clearOrdersFrom(final double price) {
        orderBook.clearFrom(price);
    }

    @Override
    public final void clearAllOrders() {
        orderBook.clear();
    }

    @Override
    public final void commit(final long updateId) {
        if (mgmtView.getPermission()) {
            mgmtView.setQuoteEventTime(strategyView.getLatestEventTime());
            orderBook.commit(updateId, TradeClock.getCurrentMicrosOnly());
        }
    }

    @Override
    public final void setWaitCancelAck(final boolean isUsingLessMargin) {
        if (isUsingLessMargin) {
            orderBook.setOrderHandler(closingHandler);
            log.info("Using close-position handler for CLOSE: {} {} {}", contract, side, tif);
        } else {
            orderBook.setOrderHandler(orderHandler);
            log.info("Using default handler: {} {} {}", contract, side, tif);
        }
    }

    public boolean getSendOrderPermission() {
        return mgmtView.getPermission();
    }

    public int getOrderSubmissions() {
        return mgmtView.getOrderSubmissions();
    }

    public int getOrderModifications() {
        return mgmtView.getOrderModifications();
    }

    public int getOrderCancellations() {
        return mgmtView.getOrderCancellations();
    }

    public int getOrderFills() {
        return mgmtView.getFills();
    }

    public int getRequestRejects() {
        return mgmtView.getRejects();
    }

    public long getVolume() {
        return mgmtView.getVolume();
    }

    public int getBlockingCount() {
        return orderBook.getBlockingCount();
    }

    public void setSendOrderPermission(final boolean canSendOrder) {
        mgmtView.setPermission(canSendOrder);
        if (canSendOrder) {
            orderBook.setOrderHandler(orderHandler);
        }
    }

    public void setTradeProxyListener(final TradeProxyListener tradeProxyListener) {
        execRptHandler.setTradeProxyListener(tradeProxyListener);
    }

    public void onExitOnly(final OrderViewContainer orderViewContainer) {
        exitHandler.setOrderViewContainder(orderViewContainer);
        orderBook.setOrderHandler(exitHandler);
        log.info("Using trade-out handler for EXIT: {} {} {}", contract, side, tif);
    }

    // public boolean isManualFillInRange(final double price, final long qty) {
    // return positionManager.canImportFill(side, price, qty);
    // }

    public void receiveManualFill(final long clOrdId, final long fillQty, final long remainingQty, final double fillPrice, final double commission) {
        execRptHandler.onFill(clOrdId, fillQty, remainingQty, fillPrice, commission);
    }

    public void sendManualOrder(final double price, final long qty) {
        orderBook.setQty(qty, price);
        orderBook.commit(0L, TradeClock.getCurrentMicros());
    }

    public TradeEvent getTradeEvent() {
        return execRptHandler;
    }

    public void release() {
        this.reset();
    }

    public void reset() {
        orderBook.reset();
    }

    @Override
    public void rewind() {
        mgmtView.setPermission(false);
        mgmtView.clear();
        orderBook.reset();
        execRptHandler.rewind();
    }

    @Override
    public boolean isReady() {
        return execRptHandler.isReady() && orderBook.isReady();
    }

}
