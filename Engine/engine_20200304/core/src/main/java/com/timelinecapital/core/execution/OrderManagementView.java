package com.timelinecapital.core.execution;

public final class OrderManagementView {

    private boolean canSendOrder = false;
    private long quoteEventInMicros;
    private long cancelAckInMicros;

    private int orderSubmissions = 0;
    private int orderModifications = 0;
    private int orderCancellations = 0;
    private int fills = 0;
    private int rejects = 0;
    private long volume = 0;

    OrderManagementView() {
    }

    public final void clear() {
        quoteEventInMicros = 0;
        cancelAckInMicros = 0;
        orderSubmissions = 0;
        orderModifications = 0;
        orderCancellations = 0;
        fills = 0;
        rejects = 0;
        volume = 0;
    }

    public final void setPermission(final boolean canSendOrder) {
        this.canSendOrder = canSendOrder;
    }

    public final boolean getPermission() {
        return canSendOrder;
    }

    public final void setQuoteEventTime(final long quoteEventInMicros) {
        this.quoteEventInMicros = quoteEventInMicros;
    }

    public final void setReportEventTime(final long cancelAckInMicros) {
        this.cancelAckInMicros = cancelAckInMicros;
    }

    public final long getQuoteEventTime() {
        return quoteEventInMicros;
    }

    public final long getReportEventTime() {
        return cancelAckInMicros;
    }

    public final void onOrderSubmit() {
        orderSubmissions++;
    }

    public final void onOrderModify() {
        orderModifications++;
    }

    public final void onOrderCancel() {
        orderCancellations++;
    }

    public final void onFill(final long qty) {
        fills++;
        volume += qty;
    }

    public final void onReject() {
        rejects++;
    }

    public final int getOrderSubmissions() {
        return orderSubmissions;
    }

    public final int getOrderModifications() {
        return orderModifications;
    }

    public final int getOrderCancellations() {
        return orderCancellations;
    }

    public final int getFills() {
        return fills;
    }

    public final int getRejects() {
        return rejects;
    }

    public final long getVolume() {
        return volume;
    }

}
