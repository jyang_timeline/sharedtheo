package com.timelinecapital.core.execution;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.RejectReason;
import com.nogle.core.util.PriceUtils;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.execution.handler.OrderHandler;
import com.timelinecapital.core.pool.SuperPool;
import com.timelinecapital.core.types.PriceLevelPolicy;

public class SingleLevelOrderBookBySide implements OrderBook {

    private static Logger log;

    private final SuperPool<OrderEntry> superPool = new SuperPool<>(OrderEntry.class);

    private final OrderEntryFactory factory = new OrderEntryFactory(superPool);
    private OrderEntryRecycler recycler;

    private final Map<Long, OrderEntry> clOrdIdToEntry = new LinkedHashMap<>(16);
    private final List<OrderEntry> entries = new ArrayList<>(16);
    private Iterator<OrderEntry> iterator;

    private final Side side;
    private final String symbol;
    private OrderHandler orderHandler;

    private volatile boolean dirty;

    private PositionPolicy positionPolicy = PositionPolicy.AUTO;
    private long positionMax = 1024L;
    private long desiredQty;
    private double desiredPrice;
    private int estimatedPosition;

    public SingleLevelOrderBookBySide(final OrderHandler orderHandler, final Side side, final String symbol) {
        log = LogManager.getLogger(SingleLevelOrderBookBySide.class);
        this.orderHandler = orderHandler;
        this.side = side;
        this.symbol = symbol;

        try {
            final Constructor<OrderEntryRecycler> c = OrderEntryRecycler.class.getConstructor(int.class, superPool.getClass());
            final int chainSize = superPool.getChainSize();
            recycler = c.newInstance(chainSize, superPool);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public double getBestPrice() {
        if (clOrdIdToEntry.isEmpty()) {
            return Double.NaN;
        }
        double bestPrice = Double.NaN;
        for (final OrderEntry entry : clOrdIdToEntry.values()) {
            bestPrice = PriceUtils.getInsidePrice(side, bestPrice, entry.getPrice());
        }
        return bestPrice;
    }

    @Override
    public List<OrderEntry> getOrderEntries() {
        return entries;
    }

    @Override
    public PriceLevelPolicy getPriceLevelPolicy() {
        return PriceLevelPolicy.SinglePriceLevel;
    }

    @Override
    public long getOutstandingQty(final double price) {
        return entries.stream().filter(o -> o.getPrice() == price).mapToLong(o -> o.getQty()).sum();
    }

    @Override
    public long getOutstandingOrderCount() {
        return entries.stream().filter(o -> o.isWorkingOrder()).count();
    }

    @Override
    public void setOrderHandler(final OrderHandler orderHandler) {
        this.orderHandler = orderHandler;
    }

    @Override
    public void setPolicy(final PositionPolicy positionPolicy, final long positionMax) {
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
    }

    @Override
    public void setQty(final long quantity, final double price) {
        setQty(quantity, price, 0);
    }

    @Override
    public final void setQty(final long quantity, final double price, final int queuePosition) {
        dirty = dirty || quantity != desiredQty || price != desiredPrice;
        desiredQty = quantity;
        desiredPrice = price;
        estimatedPosition = queuePosition;
    }

    @Override
    public final void cancelOrder(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        entry.clear();
        try {
            entry.setSent(true);
            entry.setAcked(false);
            orderHandler.cancelDayOrder(entry.getClOrdId(), side, TradeClock.getCurrentMicrosOnly());
            entry.setDirty(false);
        } catch (final Exception e) {
            entry.setAcked(true);
            entry.setSent(false);
            log.error("[{}] X-Commit FAILED {} {}", entry.getClOrdId(), symbol, side, e.getMessage());
        }
    }

    @Override
    public final void clearFrom(final double price) {
        if (PriceUtils.isInsideOf(side, price, desiredPrice)) {
            clear();
        }
    }

    @Override
    public void clear(final double price) {
        if (price == desiredPrice) {
            clear();
        }
    }

    @Override
    public void clear() {
        dirty = dirty || desiredQty != 0;
        entries.stream().filter(o -> o.isWorkingOrder()).forEach(o -> o.clear());
    }

    @Override
    public void reset() {
        clOrdIdToEntry.clear();
        entries.clear();
    }

    @Override
    public void rewind() {
        clOrdIdToEntry.clear();
        entries.clear();
        clear();
    }

    @Override
    public boolean isReady() {
        return clOrdIdToEntry.isEmpty() && !dirty;
    }

    @Override
    public void commit(final long updateId, final long eventMicros) {
        if (!dirty) {
            return;
        }

        long qtyToSend = desiredQty;
        iterator = entries.iterator();
        while (iterator.hasNext()) {
            final OrderEntry entry = iterator.next();

            // waiting for ACK
            if (entry.isInflight()) {
                if (entry.getPrice() == desiredPrice) {
                    qtyToSend -= entry.getQty();
                }
                continue;
            }

            // OrderEntry: not at the given price
            if (entry.getPrice() != desiredPrice && entry.isWorkingOrder()) {
                log.debug("Price changed, sending cancel {}", entry);
                try {
                    entry.clear();
                    entry.setSent(true);
                    entry.setAcked(false);
                    orderHandler.cancelDayOrder(entry.getClOrdId(), side, TradeClock.getCurrentMicrosOnly());
                    entry.setDirty(false);
                } catch (final Exception e) {
                    entry.setAcked(true);
                    entry.setSent(false);
                    log.error("[{}] X-Commit FAILED {} {} {}", entry.getClOrdId(), symbol, side, e.getMessage());
                }
                continue;
            }

            // OrderEntry: no update or waiting for ACK
            if (!entry.isDirty() || entry.isInflight()) {
                continue;
            }

            // OrderEntry: To be canceled
            if (entry.isValid() && !entry.isInflight()) {
                log.debug("sending cancel {}", entry);
                try {
                    entry.setSent(true);
                    entry.setAcked(false);
                    orderHandler.cancelDayOrder(entry.getClOrdId(), side, TradeClock.getCurrentMicrosOnly());
                    entry.setDirty(false);
                } catch (final Exception e) {
                    entry.setAcked(true);
                    entry.setSent(false);
                    log.error("[{}] X-Commit FAILED {} {} {}", entry.getClOrdId(), symbol, side, e.getMessage());
                }
                continue;
            }

            // Check quantity
            if (entry.isWorkingOrder()) {
                qtyToSend -= entry.getQty();
            }
        }

        dirty = false;
        if (qtyToSend > 0 && desiredPrice != 0 && desiredPrice != Double.NaN && desiredPrice != Double.MAX_VALUE) {
            final OrderEntry entry = factory.get();
            entry.setOrder(qtyToSend, desiredPrice, estimatedPosition);
            log.debug("sending new {}", entry);
            final long clOrdId = orderHandler.getNextClOrdId();
            try {
                entry.setClOrdId(clOrdId);
                entry.setSent(true);
                entries.add(entry);
                clOrdIdToEntry.put(clOrdId, entry);
                orderHandler.newDayOrder(clOrdId, side, entry.getQty(), entry.getPrice(), positionPolicy, positionMax, TradeClock.getCurrentMicrosOnly());
                entry.setDirty(false);
            } catch (final Exception e) {
                clOrdIdToEntry.remove(clOrdId);
                entries.remove(entry);
                log.error("[{}] O-Commit FAILED {} {} {} {}", clOrdId, symbol, side, entry.getQty(), entry.getPrice());
                recycler.recycle(entry);
            }
        }
    }

    @Override
    public long orderAcked(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        if (entry == null) {
            log.warn("[{}] O-Acked but does not exist in Map", clOrdId);
            return 0;
        }
        entry.setAcked(true);
        return entry.getQty();
    }

    @Override
    public void orderRejected(final long clOrdId, final RejectReason rejectReason) {
        final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
        if (entry == null) {
            log.warn("[{}] O-Reject but does not exist in Map", clOrdId);
            return;
        }
        entries.remove(entry);
        recycler.recycle(entry);
    }

    @Override
    public void orderFilled(final long clOrdId, final long fillQty, final long remainingQty) {
        final OrderEntry entry = clOrdIdToEntry.get(clOrdId);
        if (entry == null) {
            log.error("Unknown FILL: ClOrdId is missing {}", clOrdId);
            return;
        }
        if (remainingQty == 0) {
            clOrdIdToEntry.remove(clOrdId);
            entries.remove(entry);
            recycler.recycle(entry);
        } else {
            entry.setFilledQty(fillQty);
        }
    }

    @Override
    public long cancelAcked(final long clOrdId) {
        final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
        if (entry == null) {
            log.warn("[{}] X-Acked but does not exist in Map", clOrdId);
            return 0;
        }
        entry.setAcked(true);
        entries.remove(entry);
        final long qty = entry.getQty();
        recycler.recycle(entry);
        return qty;
    }

    @Override
    public void cancelRejected(final long clOrdId, final RejectReason rejectReason) {
        final OrderEntry entry = clOrdIdToEntry.remove(clOrdId);
        if (entry == null) {
            log.warn("[{}] X-Reject but does not exist in Map (EOL)", clOrdId);
            return;
        }
        recycler.recycle(entry);
    }

    @Override
    public void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
        // TODO Auto-generated method stub

    }

}
