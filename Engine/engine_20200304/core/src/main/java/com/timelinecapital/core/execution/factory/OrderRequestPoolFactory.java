package com.timelinecapital.core.execution.factory;

import java.lang.reflect.Constructor;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.core.pool.SuperPool;

public class OrderRequestPoolFactory {
    private static final Logger log = LogManager.getLogger(OrderRequestPoolFactory.class);
    private static OrderRequestPoolFactory INSTANCE = new OrderRequestPoolFactory();

    private static final int DEFAULT_QUEUE_SIZE = 128;
    private static BlockingQueue<RequestInstance> objects;

    private static SuperPool<RequestInstance> orderEntryPool;
    private static RequestFactory entryObjectFactory;
    private static RequestRecycler entryObjectRecycler;

    private OrderRequestPoolFactory() {
        orderEntryPool = new SuperPool<>(RequestInstance.class);
        entryObjectFactory = new RequestFactory(orderEntryPool);
        try {
            final Constructor<RequestRecycler> c = RequestRecycler.class.getConstructor(int.class, orderEntryPool.getClass());
            entryObjectRecycler = c.newInstance(orderEntryPool.getChainSize(), orderEntryPool);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        objects = new LinkedBlockingQueue<>();
        for (int i = 0; i < DEFAULT_QUEUE_SIZE; i++) {
            objects.add(entryObjectFactory.get());
        }
    }

    public OrderRequestPoolFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OrderRequestPoolFactory();
        }
        return INSTANCE;
    }

    public static void recycleOrderObject(final RequestInstance orderEntryObject) {
        objects.add(orderEntryObject);
        entryObjectRecycler.recycle(orderEntryObject);
    }

    public static RequestInstance getOrderObject() {
        RequestInstance obj = objects.poll();
        if (obj == null) {
            obj = entryObjectFactory.get();
            log.info("Generating new RequestInstance due to pre-allocated queue is empty");
        }
        return obj;
    }

    // public class MultithreadingDemo extends Thread {
    // private final Logger log = LogManager.getLogger();
    //
    // @Override
    // public void run() {
    // for (int i = 0; i < 15; i++) {
    // final RequestInstance object = OrderRequestPoolFactory.getOrderObject();
    // log.error(object);
    // OrderRequestPoolFactory.recycleOrderObject(object);
    // }
    // System.out.println("My thread is in running state.");
    // }
    // }
    //
    // public static void main(final String[] args) {
    // final OrderRequestPoolFactory instance = new OrderRequestPoolFactory();
    // final MultithreadingDemo demo1 = instance.new MultithreadingDemo();
    // final MultithreadingDemo demo2 = instance.new MultithreadingDemo();
    // final MultithreadingDemo demo3 = instance.new MultithreadingDemo();
    // final MultithreadingDemo demo4 = instance.new MultithreadingDemo();
    // demo1.start();
    // demo2.start();
    // demo3.start();
    // demo4.start();
    // }

}
