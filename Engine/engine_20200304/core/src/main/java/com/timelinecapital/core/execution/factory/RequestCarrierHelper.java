package com.timelinecapital.core.execution.factory;

import java.nio.ByteBuffer;

import org.agrona.concurrent.UnsafeBuffer;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.TimeInForce;

public class RequestCarrierHelper {

    public static NewOrderEncoder getNewOrder(final String symbol) {
        final UnsafeBuffer buffer = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.NewOrder_MsgLength));
        final NewOrderEncoder template = new NewOrderEncoder();
        template.wrap(buffer, 0);
        template.header((byte) TradeServerProtocol.Header_NewOrderChar);
        template.symbol(symbol);
        return template;
    }

    public static NewOrderEncoder getNewOrder(final String symbol, final TimeCondition tif) {
        final UnsafeBuffer buffer = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.NewOrder_MsgLength));
        final NewOrderEncoder template = new NewOrderEncoder();
        template.wrap(buffer, 0);
        template.header((byte) TradeServerProtocol.Header_NewOrderChar);
        template.timeInForce(TimeInForce.get(tif.getCode()));
        template.symbol(symbol);
        return template;
    }

    public static CancelOrderEncoder getCancelOrder() {
        final UnsafeBuffer buffer = new UnsafeBuffer(ByteBuffer.allocate(TradeServerProtocol.CancelOrder_MsgLength));
        final CancelOrderEncoder template = new CancelOrderEncoder();
        template.wrap(buffer, 0);
        template.header((byte) TradeServerProtocol.Header_CancelOrderChar);
        return template;
    }

}
