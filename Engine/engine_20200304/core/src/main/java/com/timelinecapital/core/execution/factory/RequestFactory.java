package com.timelinecapital.core.execution.factory;

import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class RequestFactory implements PoolFactory<RequestInstance> {

    private final SuperPool<RequestInstance> superPool;

    private RequestInstance root;

    RequestFactory(final SuperPool<RequestInstance> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public RequestInstance get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final RequestInstance obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
