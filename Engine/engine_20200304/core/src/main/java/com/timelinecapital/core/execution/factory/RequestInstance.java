package com.timelinecapital.core.execution.factory;

import java.nio.ByteBuffer;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.TimeInForce;
import com.timelinecapital.core.pool.Reusable;

public class RequestInstance implements Reusable<RequestInstance> {

    private RequestInstance next = null;

    private long sourceEventTime;
    private long eventTime;
    private long enqueuedTime;
    private ExecRequestType requestType;

    // private byte[] data;
    private ByteBuffer buffer;
    private NewOrderEncoder entryEncoder;
    private CancelOrderEncoder cancelEncoder;

    // private Contract contract;
    private Side side;
    private TimeCondition tif;
    private long clOrderId;
    private long quantity;
    private double price;
    private PositionPolicy positionPolicy;
    private long positionMax;

    private TradeEvent tradeEvent;

    public void setOrderEntry(final NewOrderEncoder entryEncoder, final Side side, final TimeCondition tif,
        final long clOrderId, final long quantity, final double price, final PositionPolicy positionPolicy, final long positionMax, final TradeEvent tradeEvent) {
        requestType = ExecRequestType.OrderEntry;
        // this.contract = contract;
        this.entryEncoder = entryEncoder;
        this.side = side;
        this.tif = tif;
        this.clOrderId = clOrderId;
        this.quantity = quantity;
        this.price = price;
        this.positionPolicy = positionPolicy;
        this.positionMax = positionMax;
        this.tradeEvent = tradeEvent;
    }

    public void setCancelEntry(final CancelOrderEncoder cancelEncoder, final long clOrderId, final TradeEvent tradeEvent) {
        requestType = ExecRequestType.OrderCancel;
        this.cancelEncoder = cancelEncoder;
        this.clOrderId = clOrderId;
        this.tradeEvent = tradeEvent;
    }

    public void setEnqueueInfo(final long clOrderId, final TradeEvent tradeEvent, final ExecRequestType requestType) {
        this.requestType = requestType;
        this.clOrderId = clOrderId;
        this.tradeEvent = tradeEvent;
    }

    public void setTime(final long sourceEventTime, final long eventTime) {
        this.sourceEventTime = sourceEventTime;
        this.eventTime = eventTime;
        enqueuedTime = TradeClock.getCurrentMicrosOnly();
    }

    public void onBinaryData() {
        if (ExecRequestType.OrderEntry.equals(requestType)) {
            entryEncoder.timeInForce(TimeInForce.get(tif.getCode()));
            entryEncoder.side(com.timelinecapital.commons.binary.Side.get(side.getCode()));
            entryEncoder.clOrdId(clOrderId);
            entryEncoder.orderQty(quantity);
            entryEncoder.price(price);
            entryEncoder.positionPolicy(positionPolicy.getPolicyCode());
            entryEncoder.positionMax(positionMax);
            buffer = entryEncoder.buffer().byteBuffer();
            // entryEncoder.setTimeCondition(tif.getCode());
            // entryEncoder.setSide(side.getCode());
            // entryEncoder.setClOrdId(clOrderId);
            // entryEncoder.setQty(quantity);
            // entryEncoder.setPrice(price);
            // entryEncoder.setPositionPolicy(positionPolicy.getPolicyCode());
            // entryEncoder.setPositionmax(positionMax);
            // data = entryEncoder.getData();
        } else {
            cancelEncoder.clOrdId(clOrderId);
            buffer = cancelEncoder.buffer().byteBuffer();
            // cancelEncoder.setClOrdId(clOrderId);
            // data = cancelEncoder.getData();
        }
    }

    public ByteBuffer getBinary() {
        return buffer;
    }

    // public byte[] getBinaryData() {
    // return data;
    // }

    public long getSourceEventTime() {
        return sourceEventTime;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEnqueuedTime(final long enqueuedTime) {
        this.enqueuedTime = enqueuedTime;
    }

    public long getEnqueuedTime() {
        return enqueuedTime;
    }

    public ExecRequestType getRequestType() {
        return requestType;
    }

    // public Contract getContract() {
    // return contract;
    // }

    public NewOrderEncoder getEntryEncoder() {
        return entryEncoder;
    }

    public CancelOrderEncoder getCancelEncoder() {
        return cancelEncoder;
    }

    public Side getSide() {
        return side;
    }

    public TimeCondition getTimeCondition() {
        return tif;
    }

    public long getClOrderId() {
        return clOrderId;
    }

    public long getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public PositionPolicy getPositionPolicy() {
        return positionPolicy;
    }

    public long getPositionMax() {
        return positionMax;
    }

    public TradeEvent getTradeEvent() {
        return tradeEvent;
    }

    @Override
    public RequestInstance getNext() {
        return next;
    }

    @Override
    public void setNext(final RequestInstance next) {
        this.next = next;
    }

    @Override
    public void reset() {
        next = null;
        // contract = null;
        // data = null;
        buffer = null;
        clOrderId = 0;
        quantity = 0;
        price = 0;
        tradeEvent = null;
    }

}
