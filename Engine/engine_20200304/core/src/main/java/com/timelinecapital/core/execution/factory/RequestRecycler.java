package com.timelinecapital.core.execution.factory;

import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class RequestRecycler implements Recycler<RequestInstance> {

    private final SuperPool<RequestInstance> superPool;

    private RequestInstance root;

    private final int recycleSize;
    private int count = 0;

    public RequestRecycler(final int recycleSize, final SuperPool<RequestInstance> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = RequestInstance.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final RequestInstance obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}