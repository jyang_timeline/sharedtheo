package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.riskmanagement.PositionManager;

public final class ClosePositionHandler extends OrderHandler {
    private static final Logger log = LogManager.getLogger(ClosePositionHandler.class);

    private final PositionManager positionManager;
    private boolean fenceToPostponeOrders = false;
    private long eventIdToPostponeOrders = 0;

    public ClosePositionHandler(final Contract contract, final ClientOrderIdGenerator idGenerator, final OrderSender sender, final TradeAppendix appendix, final TradeEvent event,
        final OrderManagementView view) {
        super(contract, idGenerator, sender, appendix, event, view);
        this.positionManager = appendix.getPositionManager();
    }

    @Override
    public final long genOrderQuantity(final Side side, final long quantity, final double price, final int existingOrders) {
        final long qtySent = positionManager.adjustQuantity(side, quantity, positionTracker.getPosition());
        if (qtySent <= 0) {
            return 0L;
        }
        if (!positionManager.canSendNewOrder(side, price, qtySent, existingOrders)) {
            return 0L;
        }
        return qtySent;
    }

    @Override
    public final void newDayOrder(final long clOrdId, final Side side, final long qtySent, final double priceSent, final PositionPolicy positionPolicy,
        final long positionMax, final long commitEventMicros) throws Exception {
        // if (isNextOrderQueuing) {
        sender.enqueue(clOrdId, side, qtySent, priceSent, positionPolicy, positionMax, event);
        // } else {
        // orderSender.sendNewDayOrder(clOrdId, side, qtySent, priceSent, positionPolicy, positionMax, tradeEvent,
        // orderViewStatus.getQuoteEventTime(), commitEventMicros);
        // log.debug("[{}] DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, qtySent, priceSent);
        // }
        view.onOrderSubmit();
    }

    @Override
    public final void cancelDayOrder(final long clOrdId, final Side side, final long commitEventMicros) throws Exception {
        super.cancelDayOrder(clOrdId, side, commitEventMicros);
        fenceToPostponeOrders = true;
        eventIdToPostponeOrders = clOrdId;
    }

    @Override
    public final void onScheduledOrder(final Side side) {
        sender.dequeue(side, view.getReportEventTime());
        fenceToPostponeOrders = false;
        eventIdToPostponeOrders = 0;
    }

    @Override
    public final void onBatchOrder(final Side side) {
        if (!fenceToPostponeOrders) {
            sender.dequeue(side, view.getReportEventTime());
            fenceToPostponeOrders = false;
            eventIdToPostponeOrders = 0;
        } else {
            log.info("{} [{}] orders are still waiting for EVENT: {} {} {}", contract.getSymbol(), sender.getNoOrders(side),
                ExecRequestType.OrderCancel, side, eventIdToPostponeOrders);
        }
    }

}
