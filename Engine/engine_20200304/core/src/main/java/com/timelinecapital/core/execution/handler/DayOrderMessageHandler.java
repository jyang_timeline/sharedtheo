package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.position.PositionTracker;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.OrderBook;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.stats.ExecReportSink;

public final class DayOrderMessageHandler extends ExecutionReportHandler {
    private static final Logger log = LogManager.getLogger(DayOrderMessageHandler.class);

    public DayOrderMessageHandler(final Contract contract, final Side side, final String threadContext, final OrderBook orderBook, final PositionTracker positionTracker,
        final OrderManagementView view, final ExecReportSink statsCollector) {
        super(contract, side, threadContext, orderBook, positionTracker, view, statsCollector);
        tif = TimeCondition.GFD;
    }

    @Override
    public final void onCancelAck(final long clOrdId) {
        /*
         * Update OrderBook
         */
        positionTracker.trackCancel(clOrdId);
        final long cancelQty = orderBook.cancelAcked(clOrdId);

        /*
         * Invoke event post processor
         */
        final Ack cancelAck = ExecReportFactory.getAck();
        cancelAck.setAck(contract, side, tif, ExecType.CANCEL_ACK, clOrdId);
        tradeProxyListener.onCancel(cancelAck);

        /*
         * Update statistics
         */
        view.onOrderCancel();
        statsCollector.onCancelAck(cancelQty, side);
        log.debug("[{}] X-Acked: {}  O#{} C#{} F#{} R#{}", clOrdId, contract,
            view.getOrderSubmissions(), view.getOrderCancellations(), view.getFills(), view.getRejects());
    }

    @Override
    public final void onCancelReject(final long clOrdId, final RejectReason rejectReason) {
        /*
         * Update OrderBook
         */
        orderBook.cancelRejected(clOrdId, rejectReason);

        /*
         * Invoke event post processor
         */
        final Reject cancelReject = ExecReportFactory.getReject();
        cancelReject.setReject(contract, side, tif, ExecType.CANCEL_REJECT, clOrdId, rejectReason);
        tradeProxyListener.onReject(cancelReject);

        /*
         * Update statistics
         */
        statsCollector.onCancelReject();
        log.warn("[{}] X-Reject: {} {} {}", clOrdId, contract, rejectReason, rejectReason.getDescription());
    }

    @Override
    public final void onOrderReject(final long clOrdId, final RejectReason rejectReason) {
        super.onReject(clOrdId, rejectReason);
        log.warn("[{}] O-Reject: {} {} {}", clOrdId, contract, rejectReason, rejectReason.getDescription());
    }

}
