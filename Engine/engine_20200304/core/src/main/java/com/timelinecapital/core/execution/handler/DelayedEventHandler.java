package com.timelinecapital.core.execution.handler;

import com.nogle.strategy.types.Side;

public interface DelayedEventHandler {

    default void onScheduledOrder(final Side side) {
    }

    default void onBatchOrder(final Side side) {
    }

}
