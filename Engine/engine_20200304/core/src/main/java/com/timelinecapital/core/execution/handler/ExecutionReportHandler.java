package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.OrderBook;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.stats.ExecReportSink;

public abstract class ExecutionReportHandler implements TradeEvent {
    private static final Logger log = LogManager.getLogger(ExecutionReportHandler.class);

    protected final Contract contract;
    protected TimeCondition tif;
    protected final Side side;

    protected final OrderBook orderBook;
    protected final PositionTracker positionTracker;
    protected final OrderManagementView view;
    protected final ExecReportSink statsCollector;

    protected TradeProxyListener tradeProxyListener;

    private final String threadContext;

    ExecutionReportHandler(final Contract contract, final Side side, final String threadContext, final OrderBook orderBook, final PositionTracker positionTracker,
        final OrderManagementView view, final ExecReportSink statsCollector) {
        this.contract = contract;
        this.side = side;
        this.threadContext = threadContext;
        this.orderBook = orderBook;
        this.positionTracker = positionTracker;
        this.view = view;
        this.statsCollector = statsCollector;
    }

    @Override
    public final void setTradeProxyListener(final TradeProxyListener tradeProxyListener) {
        this.tradeProxyListener = tradeProxyListener;
    }

    @Override
    public final TradeEvent onEventContext() {
        ThreadContext.put("ROUTINGKEY", threadContext);
        return this;
    }

    @Override
    public final void onOrderAck(final long clOrdId) {
        /*
         * Update OrderBook
         */
        final long orderQty = orderBook.orderAcked(clOrdId);

        /*
         * Invoke event post processor
         */
        final Ack orderAck = ExecReportFactory.getAck();
        orderAck.setAck(contract, side, tif, ExecType.NEWORDER_ACK, clOrdId);
        tradeProxyListener.onAck(orderAck);

        /*
         * Update statistics
         */
        statsCollector.onOrderAck(orderQty, side);
        log.debug("[{}] O-Acked: {} O#{} C#{} F#{} R#{}", clOrdId, contract,
            view.getOrderSubmissions(), view.getOrderCancellations(), view.getFills(), view.getRejects());
    }

    @Override
    public final void onFill(final long clOrdId, final long fillQty, final long remainingQty, final double fillPrice, final double commission) {
        /*
         * Update OrderBook
         */
        positionTracker.trackFill(side, fillQty, fillPrice, commission);
        orderBook.orderFilled(clOrdId, fillQty, remainingQty);

        /*
         * Invoke event post processor
         */
        final OrderFill fill = ExecReportFactory.getFill();
        fill.setFill(contract, side, tif, clOrdId, fillQty, fillPrice, commission);
        fill.setRemainingQty(remainingQty);
        tradeProxyListener.onFill(fill);

        /*
         * Update statistics
         */
        view.onFill(fillQty);
        statsCollector.onFill(side, fillPrice, fillQty, commission);
        statsCollector.ofExposure(positionTracker.getExposure());
        log.debug("[{}] Fill: {} {} {}@{} rem: {}", clOrdId, contract, side, fillQty, fillPrice, remainingQty);
    }

    void onReject(final long clOrdId, final RejectReason rejectReason) {
        /*
         * Update OrderBook
         */
        orderBook.orderRejected(clOrdId, rejectReason);

        /*
         * Invoke event post processor
         */
        final Reject orderReject = ExecReportFactory.getReject();
        orderReject.setReject(contract, side, tif, ExecType.REJECT, clOrdId, rejectReason);
        tradeProxyListener.onReject(orderReject);

        /*
         * Update statistics
         */
        view.onReject();
        statsCollector.onOrderReject();
    }

    @Override
    public void onRequestFail(final long clOrdId, final ExecRequestType requestType) {
        orderBook.onRequestFail(clOrdId, requestType);
        log.warn("[{}] Request FAIL", clOrdId);
    }

    @Override
    public void rewind() {
        tradeProxyListener.rewind();
        view.clear();
    }

    @Override
    public boolean isReady() {
        return tradeProxyListener.isReady();
    }

}
