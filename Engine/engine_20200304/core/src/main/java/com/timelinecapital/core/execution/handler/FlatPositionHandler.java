package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;

public final class FlatPositionHandler extends OrderHandler {
    private static final Logger log = LogManager.getLogger(FlatPositionHandler.class);

    private final ProtectiveManager protectiveManager;
    private OrderViewContainer orderViewContainer;

    public FlatPositionHandler(final Contract contract, final ClientOrderIdGenerator idGenerator, final OrderSender sender, final TradeAppendix appendix, final TradeEvent event,
        final OrderManagementView view) {
        super(contract, idGenerator, sender, appendix, event, view);
        this.protectiveManager = appendix.getRiskControlManager();
    }

    @Override
    public final long genOrderQuantity(final Side side, final long quantity, final double price, final int existingOrders) {
        return reviseQty(side, quantity, price);
    }

    public void setOrderViewContainder(final OrderViewContainer orderViewContainer) {
        this.orderViewContainer = orderViewContainer;
    }

    private long reviseQty(final Side side, final long quantity, final double price) {
        if (positionTracker.getPosition() == 0) {
            if (view.getPermission() && !orderViewContainer.hasOutstandingOrders()) {
                view.setPermission(false);
                log.warn("{} is in EXIT mode and opening position of {} has been set to {}", contract, side, false);
            }
            return 0;
        }
        return protectiveManager.adjustQuantity(side, quantity, positionTracker.getPosition());
    }

}
