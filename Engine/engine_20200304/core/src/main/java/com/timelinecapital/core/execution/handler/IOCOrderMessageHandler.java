package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.position.PositionTracker;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.OrderBook;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.stats.ExecReportSink;

public final class IOCOrderMessageHandler extends ExecutionReportHandler {
    private static final Logger log = LogManager.getLogger(IOCOrderMessageHandler.class);

    private long sentQty;
    private double sentPrice;

    public IOCOrderMessageHandler(final Contract contract, final Side side, final String threadContext, final OrderBook orderBook, final PositionTracker positionTracker,
        final OrderManagementView view, final ExecReportSink statsCollector) {
        super(contract, side, threadContext, orderBook, positionTracker, view, statsCollector);
        tif = TimeCondition.IOC;
    }

    @Override
    public final void setSentQyt(final long sentQyt) {
        this.sentQty = sentQyt;
    }

    @Override
    public final void setSentPrice(final double sentPrice) {
        this.sentPrice = sentPrice;
    }

    @Override
    public final void onCancelAck(final long clOrdId) {
        /*
         * Update OrderBook
         */
        final long cancelQty = orderBook.cancelAcked(clOrdId);

        /*
         * Invoke event post processor
         */
        final OrderMiss miss = ExecReportFactory.getMiss();
        miss.setMiss(contract, side, tif, clOrdId, sentQty, sentPrice);
        tradeProxyListener.onMiss(miss);

        /*
         * Update statistics
         */
        statsCollector.onCancelAck(cancelQty, side);
        log.debug("[{}] I-Missed: {} {}", clOrdId, contract, sentQty);
    }

    @Override
    public final void onCancelReject(final long clOrdId, final RejectReason rejectReason) {
        throw new RuntimeException("IOC should not receive Cancel Reject");
    }

    @Override
    public final void onOrderReject(final long clOrdId, final RejectReason rejectReason) {
        super.onReject(clOrdId, rejectReason);
        log.warn("[{}] I-Reject: {} {} {}", clOrdId, contract, rejectReason, rejectReason.getDescription());
    }

}
