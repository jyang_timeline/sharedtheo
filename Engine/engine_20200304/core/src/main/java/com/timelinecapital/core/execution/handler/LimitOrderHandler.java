package com.timelinecapital.core.execution.handler;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.riskmanagement.PositionManager;

public final class LimitOrderHandler extends OrderHandler {

    private final PositionManager positionManager;

    public LimitOrderHandler(final Contract contract, final ClientOrderIdGenerator idGenerator, final OrderSender sender, final TradeAppendix appendix, final TradeEvent event,
        final OrderManagementView view) {
        super(contract, idGenerator, sender, appendix, event, view);
        this.positionManager = appendix.getPositionManager();
    }

    @Override
    public final long genOrderQuantity(final Side side, final long quantity, final double price, final int existingOrders) {
        final long qtySent = positionManager.adjustQuantity(side, quantity, positionTracker.getPosition());
        if (qtySent <= 0) {
            return 0L;
        }
        if (!positionManager.canSendNewOrder(side, price, qtySent, existingOrders)) {
            return 0L;
        }
        return qtySent;
    }

}
