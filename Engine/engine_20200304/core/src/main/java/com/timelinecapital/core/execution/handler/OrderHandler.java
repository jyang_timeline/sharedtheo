package com.timelinecapital.core.execution.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.execution.OrderManagementView;
import com.timelinecapital.core.riskmanagement.PriceManager;

public abstract class OrderHandler implements DelayedEventHandler {
    private static final Logger log = LogManager.getLogger(OrderHandler.class);

    protected final Contract contract;
    protected final OrderSender sender;
    protected final TradeEvent event;
    protected final OrderManagementView view;
    protected final PositionTracker positionTracker;

    private final ClientOrderIdGenerator idGenerator;
    private final PriceManager priceManager;

    public OrderHandler(final Contract contract, final ClientOrderIdGenerator idGenerator, final OrderSender sender, final TradeAppendix appendix,
        final TradeEvent event, final OrderManagementView view) {
        this.contract = contract;
        this.sender = sender;
        this.event = event;
        this.view = view;
        this.positionTracker = appendix.getPositionTracker();

        this.idGenerator = idGenerator;
        this.priceManager = appendix.getPriceManager();
    }

    public final long getNextClOrdId() {
        return idGenerator.getNextId();
    }

    public final Contract getTradingSymbol() {
        return contract;
    }

    public final double genOrderPrice(final Side side, final double price) {
        return priceManager.adjustPrice(side, price);
    }

    public abstract long genOrderQuantity(final Side side, final long quantity, final double price, final int existingOrders);

    public void newDayOrder(final long clOrdId, final Side side, final long qtySent, final double priceSent, final PositionPolicy positionPolicy,
        final long positionMax, final long commitEventMicros) throws Exception {
        sender.sendNewDayOrder(clOrdId, side, qtySent, priceSent, positionPolicy, positionMax, event, view.getQuoteEventTime(), commitEventMicros);
        view.onOrderSubmit();
        log.debug("[{}] DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, qtySent, priceSent);
    }

    public void cancelDayOrder(final long clOrdId, final Side side, final long commitEventMicros) throws Exception {
        sender.sendOrderCancel(clOrdId, side, event, view.getQuoteEventTime(), commitEventMicros);
        log.debug("[{}] OrderCancel: {} {}", clOrdId, contract.getSymbol(), side);
    }

    public final void newIOCOrder(final long clOrdId, final Side side, final long quantity, final double price, final PositionPolicy positionPolicy,
        final long positionMax, final long commitEventMicros) throws Exception {
        event.setSentQyt(quantity);
        event.setSentPrice(price);
        sender.sendNewIOCOrder(clOrdId, side, quantity, price, positionPolicy, positionMax, event, view.getQuoteEventTime(), commitEventMicros);
        view.onOrderSubmit();
        log.debug("[{}] IOCOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, quantity, price);
    }

}
