package com.timelinecapital.core.execution.report;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class Ack extends ExecReport<Ack> {

    private Ack next = null;
    private ExecType execType;
    private long qty;
    private double price;

    public void setAck(final Contract contract, final Side side, final TimeCondition tif, final ExecType execType, final long clOrderId, final long qty, final double price) {
        super.setReport(contract, side, tif, clOrderId);
        this.execType = execType;
        this.qty = qty;
        this.price = price;
    }

    public void setAck(final Contract contract, final Side side, final TimeCondition tif, final ExecType execType, final long clOrderId) {
        super.setReport(contract, side, tif, clOrderId);
        this.execType = execType;
    }

    @Override
    public ExecType getExecType() {
        return execType;
    }

    public long getQty() {
        return qty;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public Ack getNext() {
        return next;
    }

    @Override
    public void setNext(final Ack next) {
        this.next = next;
    }

    @Override
    public void reset() {
        super.reset();
        next = null;
        // symbol = null;
        // clOrderId = 0;
        qty = 0;
        price = 0;
    }

}
