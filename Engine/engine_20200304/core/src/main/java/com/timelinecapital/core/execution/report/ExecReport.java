package com.timelinecapital.core.execution.report;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecutionReport;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.pool.Reusable;

public abstract class ExecReport<T extends ExecutionReport> implements ExecutionReport, Reusable<T> {

    Contract contract;
    Side side;
    TimeCondition tif;
    long clOrderId;

    void setReport(final Contract contract, final Side side, final TimeCondition tif, final long clOrderId) {
        this.contract = contract;
        this.side = side;
        this.tif = tif;
        this.clOrderId = clOrderId;
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public Side getSide() {
        return side;
    }

    @Override
    public TimeCondition getTimeCondition() {
        return tif;
    }

    @Override
    public long getClOrderId() {
        return clOrderId;
    }

    @Override
    public void reset() {
        contract = null;
        clOrderId = 0;
    }

}
