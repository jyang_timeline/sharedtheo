package com.timelinecapital.core.execution.report;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class OrderFill extends ExecReport<OrderFill> implements Fill {

    private OrderFill next = null;
    private long lastQty;
    private double lastPrice;
    private double commission;
    private long remainingQty;

    public void setFill(final Contract symbol, final Side side, final TimeCondition tif, final long clOrderId, final long lastQty, final double lastPrice,
        final double commission) {
        super.setReport(symbol, side, tif, clOrderId);
        this.lastQty = lastQty;
        this.lastPrice = lastPrice;
        this.commission = commission;
    }

    public void setClOrderId(final long clOrderId) { // back door for split order
        this.clOrderId = clOrderId;
    }

    @Override
    public ExecType getExecType() {
        return ExecType.FILL;
    }

    @Override
    public boolean isCompleteFill() {
        return remainingQty > 0 ? false : true;
    }

    @Override
    public long getFillQty() {
        return lastQty;
    }

    public void setFillQty(final long lastQty) {
        this.lastQty = lastQty;
    }

    @Override
    public double getFillPrice() {
        return lastPrice;
    }

    public void setFillPrice(final double lastPrice) {
        this.lastPrice = lastPrice;
    }

    @Override
    public double getCommission() {
        return commission;
    }

    public void setCommission(final double commission) {
        this.commission = commission;
    }

    public long getRemainingQty() {
        return remainingQty;
    }

    public void setRemainingQty(final long remainingQty) {
        this.remainingQty = remainingQty;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("symbol", contract.getSymbol())
            .append("clOrderId", clOrderId)
            .append("price", lastPrice).append("quantity", lastQty)
            .append("side", side).append("remainingQty", remainingQty)
            .toString();
    }

    @Override
    public OrderFill getNext() {
        return next;
    }

    @Override
    public void setNext(final OrderFill next) {
        this.next = next;
    }

    @Override
    public void reset() {
        super.reset();
        next = null;
        lastQty = 0;
        lastPrice = 0;
        commission = 0;
        remainingQty = 0;
    }

}
