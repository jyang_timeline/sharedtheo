package com.timelinecapital.core.execution.report;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Miss;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class OrderMiss extends ExecReport<OrderMiss> implements Miss {

    private OrderMiss next = null;
    private long missQty;
    private double missPrice;

    public void setMiss(final Contract contract, final Side side, final TimeCondition tif, final long clOrderId, final long missQty, final double missPrice) {
        super.setReport(contract, side, tif, clOrderId);
        this.missQty = missQty;
        this.missPrice = missPrice;
    }

    @Override
    public ExecType getExecType() {
        return ExecType.NEWORDER_ACK;
    }

    @Override
    public long getMissQty() {
        return missQty;
    }

    @Override
    public double getMissPrice() {
        return missPrice;
    }

    @Override
    public OrderMiss getNext() {
        return next;
    }

    @Override
    public void setNext(final OrderMiss next) {
        this.next = next;
    }

    @Override
    public void reset() {
        super.reset();
        next = null;
        missQty = 0;
        missPrice = 0;
    }

}
