package com.timelinecapital.core.execution.report;

import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;

public class Reject extends ExecReport<Reject> {

    private Reject next = null;
    private ExecType execType;
    private RejectReason rejectReason;

    public void setReject(final Contract contract, final Side side, final TimeCondition tif, final ExecType execType, final long clOrderId, final RejectReason rejectReason) {
        super.setReport(contract, side, tif, clOrderId);
        this.execType = execType;
        this.rejectReason = rejectReason;
    }

    @Override
    public ExecType getExecType() {
        return execType;
    }

    public RejectReason getRejectReason() {
        return rejectReason;
    }

    @Override
    public Reject getNext() {
        return next;
    }

    @Override
    public void setNext(final Reject next) {
        this.next = next;
    }

    @Override
    public void reset() {
        super.reset();
        next = null;
        // symbol = null;
        // clOrderId = 0;
    }

}
