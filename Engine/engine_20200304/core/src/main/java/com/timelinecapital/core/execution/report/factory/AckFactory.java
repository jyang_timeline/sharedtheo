package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class AckFactory implements PoolFactory<Ack> {

    private final SuperPool<Ack> superPool;

    private Ack root;

    public AckFactory(final SuperPool<Ack> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public Ack get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final Ack obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
