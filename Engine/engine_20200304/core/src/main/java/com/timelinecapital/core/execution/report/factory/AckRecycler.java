package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class AckRecycler implements Recycler<Ack> {

    private final SuperPool<Ack> superPool;

    private Ack root;

    private final int recycleSize;
    private int count = 0;

    public AckRecycler(final int recycleSize, final SuperPool<Ack> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = Ack.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final Ack obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
