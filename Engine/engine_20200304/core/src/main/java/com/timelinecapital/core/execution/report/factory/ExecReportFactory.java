package com.timelinecapital.core.execution.report.factory;

import java.lang.reflect.Constructor;

import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.pool.SuperPool;

public class ExecReportFactory {
    private static ExecReportFactory INSTANCE = new ExecReportFactory();

    private static SuperPool<Ack> ackPool;
    private static SuperPool<Reject> rejectPool;
    private static SuperPool<OrderFill> fillPool;
    private static SuperPool<OrderMiss> missPool;

    private static AckFactory ackFactory;
    private static RejectFactory rejectFactory;
    private static FillFactory fillFactory;
    private static MissFactory missFactory;

    private static AckRecycler ackRecycler;
    private static RejectRecycler rejectRecycler;
    private static FillRecycler fillRecycler;
    private static MissRecycler missRecycler;

    ExecReportFactory() {
        ackPool = new SuperPool<>(Ack.class);
        rejectPool = new SuperPool<>(Reject.class);
        fillPool = new SuperPool<>(OrderFill.class);
        missPool = new SuperPool<>(OrderMiss.class);

        ackFactory = new AckFactory(ackPool);
        rejectFactory = new RejectFactory(rejectPool);
        fillFactory = new FillFactory(fillPool);
        missFactory = new MissFactory(missPool);

        try {
            final Constructor<AckRecycler> cAck = AckRecycler.class.getConstructor(int.class, ackPool.getClass());
            ackRecycler = cAck.newInstance(ackPool.getChainSize(), ackPool);
            final Constructor<RejectRecycler> cReject = RejectRecycler.class.getConstructor(int.class, rejectPool.getClass());
            rejectRecycler = cReject.newInstance(rejectPool.getChainSize(), rejectPool);
            final Constructor<FillRecycler> cFill = FillRecycler.class.getConstructor(int.class, fillPool.getClass());
            fillRecycler = cFill.newInstance(fillPool.getChainSize(), fillPool);
            final Constructor<MissRecycler> cMiss = MissRecycler.class.getConstructor(int.class, missPool.getClass());
            missRecycler = cMiss.newInstance(missPool.getChainSize(), missPool);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public ExecReportFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ExecReportFactory();
        }
        return INSTANCE;
    }

    public static void recycleAck(final Ack ack) {
        ackRecycler.recycle(ack);
    }

    public static void recycleReject(final Reject reject) {
        rejectRecycler.recycle(reject);
    }

    public static void recycleFill(final OrderFill fill) {
        fillRecycler.recycle(fill);
    }

    public static void recycleMiss(final OrderMiss miss) {
        missRecycler.recycle(miss);
    }

    public static Ack getAck() {
        return ackFactory.get();
    }

    public static Reject getReject() {
        return rejectFactory.get();
    }

    public static OrderFill getFill() {
        return fillFactory.get();
    }

    public static OrderMiss getMiss() {
        return missFactory.get();
    }

}
