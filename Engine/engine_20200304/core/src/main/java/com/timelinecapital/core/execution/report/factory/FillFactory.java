package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class FillFactory implements PoolFactory<OrderFill> {

    private final SuperPool<OrderFill> superPool;

    private OrderFill root;

    public FillFactory(final SuperPool<OrderFill> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public OrderFill get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final OrderFill obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
