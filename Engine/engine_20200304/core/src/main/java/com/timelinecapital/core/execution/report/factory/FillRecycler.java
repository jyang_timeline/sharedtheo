package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class FillRecycler implements Recycler<OrderFill> {

    private final SuperPool<OrderFill> superPool;

    private OrderFill root;

    private final int recycleSize;
    private int count = 0;

    public FillRecycler(final int recycleSize, final SuperPool<OrderFill> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = OrderFill.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final OrderFill obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
