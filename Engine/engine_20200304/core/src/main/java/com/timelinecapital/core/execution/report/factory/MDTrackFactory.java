package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.QuoteTrackDown;
import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class MDTrackFactory implements PoolFactory<QuoteTrackDown> {

    private final SuperPool<QuoteTrackDown> superPool;

    private QuoteTrackDown root;

    public MDTrackFactory(final SuperPool<QuoteTrackDown> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public QuoteTrackDown get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final QuoteTrackDown obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
