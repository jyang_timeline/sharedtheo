package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.QuoteTrackDown;
import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class MDTrackRecycler implements Recycler<QuoteTrackDown> {

    private final SuperPool<QuoteTrackDown> superPool;

    private QuoteTrackDown root;

    private final int recycleSize;
    private int count = 0;

    public MDTrackRecycler(final int recycleSize, final SuperPool<QuoteTrackDown> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = QuoteTrackDown.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final QuoteTrackDown obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
