package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class MissFactory implements PoolFactory<OrderMiss> {

    private final SuperPool<OrderMiss> superPool;

    private OrderMiss root;

    public MissFactory(final SuperPool<OrderMiss> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public OrderMiss get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final OrderMiss obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
