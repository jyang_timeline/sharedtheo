package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.OrderMiss;
import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class MissRecycler implements Recycler<OrderMiss> {

    private final SuperPool<OrderMiss> superPool;

    private OrderMiss root;

    private final int recycleSize;
    private int count = 0;

    public MissRecycler(final int recycleSize, final SuperPool<OrderMiss> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = OrderMiss.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final OrderMiss obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
