package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.pool.PoolFactory;
import com.timelinecapital.core.pool.SuperPool;

public class RejectFactory implements PoolFactory<Reject> {

    private final SuperPool<Reject> superPool;

    private Reject root;

    public RejectFactory(final SuperPool<Reject> superPool) {
        this.superPool = superPool;
        root = this.superPool.getChain();
    }

    @Override
    public Reject get() {
        if (root == null) {
            root = superPool.getChain();
        }
        final Reject obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

}
