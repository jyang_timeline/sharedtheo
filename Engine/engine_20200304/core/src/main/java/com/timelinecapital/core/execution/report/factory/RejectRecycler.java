package com.timelinecapital.core.execution.report.factory;

import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.pool.Recycler;
import com.timelinecapital.core.pool.SuperPool;

public class RejectRecycler implements Recycler<Reject> {

    private final SuperPool<Reject> superPool;

    private Reject root;

    private final int recycleSize;
    private int count = 0;

    public RejectRecycler(final int recycleSize, final SuperPool<Reject> superPool) {
        this.superPool = superPool;
        this.recycleSize = recycleSize;
        try {
            root = Reject.class.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for ClientNewOrderSingleImpl : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final Reject obj) {
        if (obj.getNext() == null) {
            obj.reset();
            obj.setNext(root.getNext());
            root.setNext(obj);
            if (++count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                count = 0;
            }
        }
    }

}
