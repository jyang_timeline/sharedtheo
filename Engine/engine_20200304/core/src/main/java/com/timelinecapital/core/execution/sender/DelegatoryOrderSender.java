package com.timelinecapital.core.execution.sender;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.strategy.event.TradeEventTranslator;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.SchedulableOrder;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.core.execution.factory.OrderRequestPoolFactory;
import com.timelinecapital.core.execution.factory.RequestCarrierHelper;
import com.timelinecapital.core.execution.factory.RequestInstance;

public final class DelegatoryOrderSender implements OrderSender, SchedulableOrder {
    private static final Logger log = LogManager.getLogger(DelegatoryOrderSender.class);

    private final ConcurrentLinkedQueue<RequestInstance> bidOrderQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<RequestInstance> askOrderQueue = new ConcurrentLinkedQueue<>();

    private final TradeConnection tradeConnection;
    private final Contract contract;
    private final NewOrderEncoder entryEncoder;
    private final CancelOrderEncoder cancelEncoder;

    private int bidOrders = 0;
    private int askOrders = 0;

    private final ThreadLocal<TradeEventTranslator> threadLocalTranslator = new ThreadLocal<>();
    private final TradeEventTranslator translator;

    public DelegatoryOrderSender(final Contract contract, final TradeConnection tradeConnection) {
        this.tradeConnection = tradeConnection;
        this.contract = contract;
        entryEncoder = RequestCarrierHelper.getNewOrder(contract.getSymbol());
        cancelEncoder = RequestCarrierHelper.getCancelOrder();
        translator = new TradeEventTranslator();
    }

    TradeEventTranslator getCachedTranslator() {
        TradeEventTranslator result = threadLocalTranslator.get();
        if (result == null) {
            result = new TradeEventTranslator();
            threadLocalTranslator.set(result);
        }
        return result;
    }

    private void initTranslator(
        final TradeEventTranslator translator,
        final NewOrderEncoder encoder,
        final ExecRequestType requestType,
        final TimeCondition tif, final Side side,
        final long clOrdId,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) {
        translator.setBasicValues(encoder, requestType, tif, side, clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent, sourceEventMicros, commitEventMicros);
    }

    private void initTranslator(
        final TradeEventTranslator translator,
        final CancelOrderEncoder encoder,
        final ExecRequestType requestType,
        final long clOrdId,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) {
        translator.setBasicValues(encoder, requestType, clOrdId, tradeEvent, sourceEventMicros, commitEventMicros);
    }

    @Override
    public final void sendNewDayOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        // final TradeEventTranslator translator = getCachedTranslator();
        initTranslator(translator, entryEncoder, ExecRequestType.OrderEntry, TimeCondition.GFD, side,
            clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent, sourceEventMicros, commitEventMicros);
        tradeConnection.sendNewOrder(translator, clOrdId, tradeEvent);
    }

    @Override
    public final void sendNewIOCOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        // final TradeEventTranslator translator = getCachedTranslator();
        initTranslator(translator, entryEncoder, ExecRequestType.OrderEntry, TimeCondition.IOC, side,
            clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent, sourceEventMicros, commitEventMicros);
        tradeConnection.sendNewOrder(translator, clOrdId, tradeEvent);
    }

    @Override
    public final void sendOrderCancel(
        final long clOrdId,
        final Side side,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        // final TradeEventTranslator translator = getCachedTranslator();
        initTranslator(translator, cancelEncoder, ExecRequestType.OrderCancel, clOrdId, tradeEvent, sourceEventMicros, commitEventMicros);
        tradeConnection.sendOrderCancel(translator, clOrdId, tradeEvent);
    }

    @Override
    public final void enqueue(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEventHandler) {
        final RequestInstance entryObject = OrderRequestPoolFactory.getOrderObject();
        entryObject.setOrderEntry(entryEncoder, side, TimeCondition.GFD, clOrdId, quantity, price, positionPolicy, positionMax, tradeEventHandler);
        entryObject.setEnqueueInfo(clOrdId, tradeEventHandler, ExecRequestType.OrderEntry);
        if (side == Side.BUY) {
            bidOrderQueue.add(entryObject);
            bidOrders++;
            log.debug("[{}] Queuing DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, quantity, price);
        } else {
            askOrderQueue.add(entryObject);
            askOrders++;
            log.debug("[{}] Queuing DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, quantity, price);
        }
    }

    @Override
    public final void dequeue(final Side side, final long sourceEventMicros) {
        switch (side) {
            case BUY:
                while (!bidOrderQueue.isEmpty()) {
                    final RequestInstance entryObject = bidOrderQueue.poll();
                    // final TradeEventTranslator translator = getCachedTranslator();
                    initTranslator(translator, entryEncoder, ExecRequestType.OrderEntry, entryObject.getTimeCondition(), entryObject.getSide(), entryObject.getClOrderId(),
                        entryObject.getQuantity(), entryObject.getPrice(), entryObject.getPositionPolicy(), entryObject.getPositionMax(), entryObject.getTradeEvent(),
                        sourceEventMicros, TradeClock.getCurrentMicros());
                    tradeConnection.sendNewOrder(translator, entryObject.getClOrderId(), entryObject.getTradeEvent());
                    log.info("[{}] Dequeuing DayOrder: {} {}", entryObject.getClOrderId(), contract.getSymbol(), side);
                    OrderRequestPoolFactory.recycleOrderObject(entryObject);
                }
                bidOrders = 0;
                break;
            case SELL:
                while (!askOrderQueue.isEmpty()) {
                    final RequestInstance entryObject = askOrderQueue.poll();
                    // final TradeEventTranslator translator = getCachedTranslator();
                    initTranslator(translator, entryEncoder, ExecRequestType.OrderEntry, entryObject.getTimeCondition(), entryObject.getSide(), entryObject.getClOrderId(),
                        entryObject.getQuantity(), entryObject.getPrice(), entryObject.getPositionPolicy(), entryObject.getPositionMax(), entryObject.getTradeEvent(),
                        sourceEventMicros, TradeClock.getCurrentMicros());
                    entryObject.setTime(sourceEventMicros, TradeClock.getCurrentMicrosOnly());
                    tradeConnection.sendNewOrder(translator, entryObject.getClOrderId(), entryObject.getTradeEvent());
                    log.info("[{}] Dequeuing DayOrder: {} {}", entryObject.getClOrderId(), contract.getSymbol(), side);
                    OrderRequestPoolFactory.recycleOrderObject(entryObject);
                }
                askOrders = 0;
                break;
            default:
                break;
        }
    }

    @Override
    public final int getNoOrders(final Side side) {
        if (side == Side.BUY) {
            return bidOrders;
        } else {
            return askOrders;
        }
    }

}
