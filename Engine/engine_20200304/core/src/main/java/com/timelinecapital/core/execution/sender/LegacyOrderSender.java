package com.timelinecapital.core.execution.sender;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeConnection;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.core.execution.factory.RequestCarrierHelper;

public final class LegacyOrderSender implements OrderSender {

    private final TradeConnection tradeConnection;
    private final NewOrderEncoder GFDOrder;
    private final NewOrderEncoder IOCOrder;
    private final CancelOrderEncoder cancelOrder;

    public LegacyOrderSender(final Contract contract, final TradeConnection tradeConnection) {
        this.tradeConnection = tradeConnection;
        GFDOrder = RequestCarrierHelper.getNewOrder(contract.getSymbol(), TimeCondition.GFD);
        IOCOrder = RequestCarrierHelper.getNewOrder(contract.getSymbol(), TimeCondition.IOC);
        cancelOrder = RequestCarrierHelper.getCancelOrder();
    }

    @Override
    public final void sendNewDayOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceMicros,
        final long commitMicros) throws Exception {
        GFDOrder.side(com.timelinecapital.commons.binary.Side.get(side.getCode()));
        GFDOrder.clOrdId(clOrdId);
        GFDOrder.orderQty(quantity);
        GFDOrder.price(price);
        GFDOrder.positionPolicy(positionPolicy.getPolicyCode());
        GFDOrder.positionMax(positionMax);
        tradeConnection.sendNewOrder(GFDOrder, clOrdId, tradeEvent, sourceMicros);
    }

    @Override
    public final void sendNewIOCOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceMicros,
        final long commitMicros) throws Exception {
        IOCOrder.side(com.timelinecapital.commons.binary.Side.get(side.getCode()));
        IOCOrder.clOrdId(clOrdId);
        IOCOrder.orderQty(quantity);
        IOCOrder.price(price);
        IOCOrder.positionPolicy(positionPolicy.getPolicyCode());
        IOCOrder.positionMax(positionMax);
        tradeConnection.sendNewOrder(IOCOrder, clOrdId, tradeEvent, sourceMicros);
    }

    @Override
    public final void sendOrderCancel(
        final long clOrdId,
        final Side side,
        final TradeEvent tradeEventHandler,
        final long sourceMicros,
        final long commitMicros) throws Exception {
        cancelOrder.clOrdId(clOrdId);
        tradeConnection.sendOrderCancel(cancelOrder, clOrdId, tradeEventHandler, sourceMicros);
    }

    @Override
    public final void enqueue(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEventHandler) {
    }

    @Override
    public final void dequeue(final Side side, final long commitMicros) {
    }

    @Override
    public final int getNoOrders(final Side side) {
        return 0;
    }

}
