package com.timelinecapital.core.execution.sender;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.SchedulableOrder;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.PositionPolicy;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.core.execution.factory.OrderRequestPoolFactory;
import com.timelinecapital.core.execution.factory.RequestCarrierHelper;
import com.timelinecapital.core.execution.factory.RequestInstance;

public final class VanillaOrderSender implements OrderSender, SchedulableOrder {
    private static final Logger log = LogManager.getLogger(VanillaOrderSender.class);

    private final ConcurrentLinkedQueue<RequestInstance> bidOrderQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<RequestInstance> askOrderQueue = new ConcurrentLinkedQueue<>();

    private final TradeConnection tradeConnection;
    private final Contract contract;
    private final NewOrderEncoder entryEncoder;
    private final CancelOrderEncoder cancelEncoder;

    private int bidOrders = 0;
    private int askOrders = 0;

    public VanillaOrderSender(final Contract contract, final TradeConnection tradeConnection) {
        this.tradeConnection = tradeConnection;
        this.contract = contract;
        entryEncoder = RequestCarrierHelper.getNewOrder(contract.getSymbol());
        cancelEncoder = RequestCarrierHelper.getCancelOrder();
    }

    @Override
    public final void sendNewDayOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        final RequestInstance entryObject = OrderRequestPoolFactory.getOrderObject();
        entryObject.setOrderEntry(entryEncoder, side, TimeCondition.GFD, clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent);
        entryObject.setTime(sourceEventMicros, commitEventMicros);
        tradeConnection.sendNewOrder(entryObject, clOrdId, tradeEvent);
    }

    @Override
    public final void sendNewIOCOrder(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        final RequestInstance entryObject = OrderRequestPoolFactory.getOrderObject();
        entryObject.setOrderEntry(entryEncoder, side, TimeCondition.IOC, clOrdId, quantity, price, positionPolicy, positionMax, tradeEvent);
        entryObject.setTime(sourceEventMicros, commitEventMicros);
        tradeConnection.sendNewOrder(entryObject, clOrdId, tradeEvent);
    }

    @Override
    public final void sendOrderCancel(
        final long clOrdId,
        final Side side,
        final TradeEvent tradeEvent,
        final long sourceEventMicros,
        final long commitEventMicros) throws Exception {
        final RequestInstance entryObject = OrderRequestPoolFactory.getOrderObject();
        entryObject.setCancelEntry(cancelEncoder, clOrdId, tradeEvent);
        entryObject.setTime(sourceEventMicros, commitEventMicros);
        tradeConnection.sendOrderCancel(entryObject, clOrdId, tradeEvent);
    }

    @Override
    public final void enqueue(
        final long clOrdId,
        final Side side,
        final long quantity,
        final double price,
        final PositionPolicy positionPolicy,
        final long positionMax,
        final TradeEvent tradeEventHandler) {
        final RequestInstance entryObject = OrderRequestPoolFactory.getOrderObject();
        entryObject.setOrderEntry(entryEncoder, side, TimeCondition.GFD, clOrdId, quantity, price, positionPolicy, positionMax, tradeEventHandler);
        entryObject.setEnqueueInfo(clOrdId, tradeEventHandler, ExecRequestType.OrderEntry);
        if (side == Side.BUY) {
            bidOrderQueue.add(entryObject);
            bidOrders++;
            log.debug("[{}] Queuing DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, quantity, price);
        } else {
            askOrderQueue.add(entryObject);
            askOrders++;
            log.debug("[{}] Queuing DayOrder: {} {} {}@{}", clOrdId, contract.getSymbol(), side, quantity, price);
        }
    }

    @Override
    public final void dequeue(final Side side, final long sourceEventMicros) {
        switch (side) {
            case BUY:
                while (!bidOrderQueue.isEmpty()) {
                    final RequestInstance entryObject = bidOrderQueue.poll();
                    entryObject.setTime(sourceEventMicros, TradeClock.getCurrentMicrosOnly());
                    log.info("[{}] Dequeuing DayOrder: {} {}", entryObject.getClOrderId(), contract.getSymbol(), side);
                    tradeConnection.sendNewOrder(entryObject, entryObject.getClOrderId(), entryObject.getTradeEvent());
                }
                bidOrders = 0;
                break;
            case SELL:
                while (!askOrderQueue.isEmpty()) {
                    final RequestInstance entryObject = askOrderQueue.poll();
                    entryObject.setTime(sourceEventMicros, TradeClock.getCurrentMicrosOnly());
                    log.info("[{}] Dequeuing DayOrder: {} {}", entryObject.getClOrderId(), contract.getSymbol(), side);
                    tradeConnection.sendNewOrder(entryObject, entryObject.getClOrderId(), entryObject.getTradeEvent());
                }
                askOrders = 0;
                break;
            default:
                break;
        }
    }

    @Override
    public final int getNoOrders(final Side side) {
        if (side == Side.BUY) {
            return bidOrders;
        } else {
            return askOrders;
        }
    }

}
