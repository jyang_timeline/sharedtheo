package com.timelinecapital.core.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.utils.SymbolQueryForm;
import com.nogle.core.contract.DefaultContractBuilder;
import com.nogle.core.exception.InvalidCodeNameException;
import com.nogle.core.exception.InvalidSymbolException;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.util.QueryLookupType;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.types.PriceLevelPolicy;
import com.timelinecapital.core.util.SymbolPatternUtil;

public class ContractFactory extends FactoryBase {
    private static final Logger log = LogManager.getLogger(ContractFactory.class);

    public static final String selfDefinedExchange = "FAKE";

    private static final String DEFAULT_RANK = "1";
    private static final String TYPE_VOLUME = "v";
    private static final String TYPE_OPENINTEREST = "o";

    private static final Map<String, Instrument> keyToContract = new ConcurrentHashMap<>();
    private static final Map<String, FeeCalculator> keyToFeeCalculator = new ConcurrentHashMap<>();

    private ContractFactory() {
    }

    public static void releaseContractMap() {
        keyToContract.clear();
        keyToFeeCalculator.clear();
    }

    public static PriceLevelPolicy getOrderViewType(final String contractString) {
        final Matcher matcher = SymbolPatternUtil.PREFERENCE.matcher(contractString);
        if (matcher.matches()) {
            return PriceLevelPolicy.MultiplePriceLevel;
        }
        return PriceLevelPolicy.SinglePriceLevel;
    }

    public static Map<String, Map<String, Instrument>> getByTradingDays(final Map<String, String> contractDescription, final String startDay, final String endDay)
        throws InvalidSymbolException {
        final Collection<SymbolQueryForm> forms = new ArrayList<>();
        for (final Entry<String, String> entry : contractDescription.entrySet()) {
            final String lookupKey = entry.getKey();
            final String contractString = entry.getValue();

            log.info(contractString);

            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(contractString);
            if (exchangeMatcher.matches()) {
                final Matcher indicesMatcher = SymbolPatternUtil.CN_INDICES.matcher(contractString);
                final Matcher stockMatcher = SymbolPatternUtil.CN_STOCKS.matcher(contractString);

                if (indicesMatcher.matches()) {
                    final String symbol = StringUtils.isEmpty(indicesMatcher.group(2)) ? indicesMatcher.group(5) : indicesMatcher.group(2);
                    final String exchange = StringUtils.isEmpty(indicesMatcher.group(3)) ? indicesMatcher.group(6) : indicesMatcher.group(3);
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, symbol, QueryLookupType.FULL_NAME);
                    forms.add(form);
                } else if (stockMatcher.matches()) {
                    final String symbol = stockMatcher.group(1);
                    final String exchange = stockMatcher.group(3);
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, symbol, QueryLookupType.FULL_NAME);
                    forms.add(form);
                }
                continue;
            }

            final Matcher matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(contractString);
            if (matcher.matches()) {
                final String exchange = matcher.group(4);
                final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, matcher.group(1), QueryLookupType.FULL_NAME);
                forms.add(form);
                continue;
            }

            final Matcher matcherProductRank = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(contractString);
            if (matcherProductRank.matches()) {
                final String exchange = matcherProductRank.group(5);
                final String productGroup = matcherProductRank.group(2);
                final String rank = matcherProductRank.group(4);
                if (StringUtils.isEmpty(rank)) {
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, productGroup, QueryLookupType.VOLUME, Integer.parseInt(DEFAULT_RANK));
                    forms.add(form);
                } else {
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, productGroup, QueryLookupType.VOLUME, Integer.parseInt(rank));
                    forms.add(form);
                }
                continue;
            }

            final Matcher matcherWithActivityRank = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(contractString);
            if (matcherWithActivityRank.matches()) {
                final String exchange = matcherWithActivityRank.group(7);
                final String productGroup = matcherWithActivityRank.group(1);

                final String rankType = StringUtils.isEmpty(matcherWithActivityRank.group(4)) ? matcherWithActivityRank.group(5) : matcherWithActivityRank.group(4);
                final String rank = StringUtils.isEmpty(matcherWithActivityRank.group(6)) ? matcherWithActivityRank.group(3) : matcherWithActivityRank.group(6);

                if (rankType.contentEquals(TYPE_VOLUME)) {
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, productGroup, QueryLookupType.VOLUME, Integer.parseInt(rank));
                    forms.add(form);
                } else if (rankType.contentEquals(TYPE_OPENINTEREST)) {
                    final SymbolQueryForm form = new SymbolQueryForm(lookupKey, exchange, productGroup, QueryLookupType.OPEN_INTEREST, Integer.parseInt(rank));
                    forms.add(form);
                }
                continue;
            }

        }
        return getSimulationContracts(forms, startDay, endDay);
    }

    public static Instrument getByTradingDay(final String contractString, final String day) throws InvalidCodeNameException {
        Instrument contract = fromCache(contractString, day);
        if (contract != null) {
            return contract;
        }

        final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(contractString);
        if (exchangeMatcher.matches()) {
            final Matcher indicesMatcher = SymbolPatternUtil.CN_INDICES.matcher(contractString);
            final Matcher stockMatcher = SymbolPatternUtil.CN_STOCKS.matcher(contractString);

            String exchange;
            if (indicesMatcher.matches()) {
                final String symbol = StringUtils.isEmpty(indicesMatcher.group(2)) ? indicesMatcher.group(5) : indicesMatcher.group(2);
                exchange = StringUtils.isEmpty(indicesMatcher.group(3)) ? indicesMatcher.group(6) : indicesMatcher.group(3);
                contract = getIndices(symbol, exchange, TradeClock.getTradingDay());
            } else if (stockMatcher.matches()) {
                final String symbol = stockMatcher.group(1);
                exchange = stockMatcher.group(3);
                contract = getByName(symbol, exchange, TradeClock.getTradingDay());
            } else {
                throw new InvalidSymbolException("Pattern does not match: " + contractString + " @" + day);
            }

            updateCache(contract, contractString, StringUtils.EMPTY, StringUtils.EMPTY, exchange, day);
            instruments.put(contract.getSymbol(), contract);
            return contract;
        }

        final Matcher matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(contractString);
        if (matcher.matches()) {
            final String exchange = matcher.group(4);
            contract = getByName(matcher.group(1), exchange, TradeClock.getTradingDay());

            updateCache(contract, contractString, StringUtils.EMPTY, StringUtils.EMPTY, exchange, day);
            instruments.put(contract.getSymbol(), contract);
            return contract;
        }

        final Matcher matcherProductRank = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(contractString);
        if (matcherProductRank.matches()) {
            final String productGroup = matcherProductRank.group(2);
            final String exchange = matcherProductRank.group(5);

            final String rank = matcherProductRank.group(4);
            if (StringUtils.isEmpty(rank)) {
                contract = getByGroupRank(productGroup, DEFAULT_RANK, TYPE_VOLUME, exchange, TradeClock.getTradingDay());
                updateCache(contract, contractString, TYPE_VOLUME, DEFAULT_RANK, exchange, day);
            } else {
                contract = getByGroupRank(productGroup, rank, TYPE_VOLUME, exchange, TradeClock.getTradingDay());
                updateCache(contract, contractString, TYPE_VOLUME, rank, exchange, day);
            }

            instruments.put(contract.getSymbol(), contract);
            return contract;
        }

        final Matcher matcherWithActivityRank = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(contractString);
        if (matcherWithActivityRank.matches()) {
            final String exchange = matcherWithActivityRank.group(7);
            final String productGroup = matcherWithActivityRank.group(1);

            final String rankType = StringUtils.isEmpty(matcherWithActivityRank.group(4)) ? matcherWithActivityRank.group(5) : matcherWithActivityRank.group(4);
            final String rank = StringUtils.isEmpty(matcherWithActivityRank.group(6)) ? matcherWithActivityRank.group(3) : matcherWithActivityRank.group(6);
            contract = getByGroupRank(productGroup, rank, rankType, exchange, TradeClock.getTradingDay());

            updateCache(contract, contractString, rankType, rank, exchange, day);
            instruments.put(contract.getSymbol(), contract);
            return contract;
        }

        throw new InvalidCodeNameException(contractString);
    }

    private static Instrument fromCache(final String contractString, final String day) {
        final String dailyContractKey = makeContractToDayKey(contractString, day);
        return keyToContract.get(dailyContractKey);
    }

    private static void updateCache(final Instrument contract, final String contractString, final String type, final String rank, final String exchange, final String day) {
        if (contract == null) {
            return;
        }
        keyToContract.put(makeContractToDayKey(contract.getSymbol(), day), contract);
        keyToContract.put(makeContractToDayKey(contractString, day), contract);
        if (!StringUtils.isEmpty(type)) {
            if (!StringUtils.isEmpty(rank)) {
                keyToContract.put(makeContractToDayKey(contract.getProductGroup() + "?" + type + "&" + rank + "@" + exchange, day), contract);
            } else {
                keyToContract.put(makeContractToDayKey(contract.getProductGroup() + "?" + type + "@" + exchange, day), contract);
            }
        }
    }

    public static void tryRenew(final Collection<Instrument> instruments) {
        builder.refresh(instruments);
    }

    public static Instrument getSelfDefinedContract(final String contractString) {
        return builder.getSelfDefinedContract(contractString);
    }

    public static Instrument getDummyContract(final String symbol, final String exchange) {
        return new DefaultContractBuilder().getByName(symbol, exchange, StringUtils.EMPTY);
    }

    public static FeeCalculator getFeeCalculator(final Contract contract, final String day) {
        FeeCalculator feeCalculator = keyToFeeCalculator.get(makeFeeCalculatorKey(contract, day));
        if (feeCalculator == null) {
            feeCalculator = builder.getFeeCalculator(contract, day);
            keyToFeeCalculator.put(makeFeeCalculatorKey(contract, day), feeCalculator);
        }
        return feeCalculator;
    }

    static Map<String, Map<String, Instrument>> getSimulationContracts(final Collection<SymbolQueryForm> queryInfos, final String startDay, final String endDay) {
        final Map<String, Map<String, Instrument>> result = builder.getAllByInterval(queryInfos, startDay, endDay);
        if (result.isEmpty()) {
            throw new InvalidSymbolException("Contract not found: " + queryInfos + " between " + startDay + " " + endDay);
        }
        return result;
    }

    static Instrument getIndices(final String codeName, final String exchange, final String date) {
        final Instrument instrument = builder.getIndices(codeName, exchange, date);
        if (instrument == null) {
            throw new InvalidSymbolException("Indices not found: " + codeName + "@" + exchange);
        }
        return instrument;
    }

    static Instrument getByName(final String codeName, final String exchange, final String date) {
        final Instrument contract = builder.getByName(codeName, exchange, date);
        if (contract == null) {
            throw new InvalidSymbolException("Contract not found: " + codeName + "@" + exchange);
        }
        return contract;
    }

    static Instrument getByGroup(final String productGroup, final String type, final String exchange, final String date) {
        final Instrument contract = builder.getByGroup(productGroup, type, exchange, date);
        if (contract == null) {
            throw new InvalidSymbolException("Contract not found: " + productGroup + "@" + exchange);
        }
        return contract;
    }

    static Instrument getByGroupRank(final String productGroup, final String rank, final String rankType, final String exchange, final String date) {
        final Instrument contract = builder.getByGroupRank(productGroup, rank, rankType, exchange, date);
        if (contract == null) {
            throw new InvalidSymbolException("Contract not found: " + productGroup + "#" + rank + "@" + exchange);
        }
        return contract;
    }

    private static String makeContractToDayKey(final String symbol, final String day) {
        return symbol + "@" + day;
    }

    private static String makeFeeCalculatorKey(final Contract contract, final String day) {
        return contract.getSymbol() + contract.getExchange() + day;
    }

    // public static void main(final String[] args) {
    // Matcher matcher = contractPattern.matcher("cu@SHFE");
    // System.out.println(matcher.matches());
    // matcher = contractWithActivityPattern.matcher("cu?v@SHFE");
    // System.out.println(matcher.matches());
    // matcher = contractWithActivityRankingPattern.matcher("cu?o&2@SHFE");
    // System.out.println(matcher.matches());
    // matcher = preferencePattern.matcher("cu@SHFE[#]");
    // System.out.println(matcher.matches());
    // matcher = preferencePattern.matcher("HG@CME[#]");
    // System.out.println(matcher.matches());
    // matcher = contractPattern.matcher("HGZ7@CME");
    // System.out.println(matcher.matches());
    // matcher = contractPattern.matcher("HG@CME");
    // System.out.println(matcher.matches());

    // matcher = stockPattern.matcher("600533@SZSE[#]");
    // matcher = stockSymbolPattern.matcher("605533@SZSE");
    // matcher = stockPattern.matcher("605533.SZSE@SZSE");
    // matcher = stockPattern.matcher("605533.SZSE@SZSE[#]");
    // if (matcher.matches()) {
    // for (int i = 0; i < matcher.groupCount(); i++) {
    // System.out.println(matcher.group(i));
    // }
    // }
    // }

}
