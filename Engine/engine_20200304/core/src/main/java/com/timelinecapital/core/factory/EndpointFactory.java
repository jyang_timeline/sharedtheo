package com.timelinecapital.core.factory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.nogle.core.contract.Endpoints;
import com.nogle.core.exception.InvalidFeedEndpointException;
import com.nogle.core.exception.InvalidTradeEndpointException;

public class EndpointFactory extends FactoryBase {
    private static final Map<String, Endpoints> keyToEndpoint = new ConcurrentHashMap<>();

    private EndpointFactory() {
    }

    public static void releaseEndpointMap() {
        keyToEndpoint.clear();
    }

    public static Endpoints getFeedEndpoints(final String exchange, final String protocol) {
        Endpoints endpoint = keyToEndpoint.get(makeFeedEndpointKey(exchange, protocol));
        if (endpoint == null) {
            endpoint = builder.getFeedEndpoints(exchange, protocol);
            if (endpoint == null) {
                throw new InvalidFeedEndpointException("Endpoint not found by exchange/protocol: " + exchange + "/" + protocol);
            }
            keyToEndpoint.put(makeFeedEndpointKey(exchange, protocol), endpoint);
        }
        return endpoint;
    }

    public static Endpoints getTradeEndpoints(final String protocol, final String account) {
        Endpoints endpoint = keyToEndpoint.get(makeTradeEndpointKey(protocol, account));
        if (endpoint == null) {
            endpoint = builder.getTradeEndpoints(account, protocol);
            if (endpoint == null) {
                throw new InvalidTradeEndpointException("Endpoint not found by protocol: " + protocol);
            }
            keyToEndpoint.put(makeTradeEndpointKey(protocol, account), endpoint);
        }
        return endpoint;
    }

    private static String makeFeedEndpointKey(final String exchange, final String protocol) {
        return exchange + protocol;
    }

    private static String makeTradeEndpointKey(final String protocol, final String account) {
        return protocol + account;
    }

}
