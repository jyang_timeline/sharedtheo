package com.timelinecapital.core.factory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Map;

import com.nogle.core.config.EngineMode;
import com.nogle.core.contract.CachedPersistContractBuilder;
import com.nogle.core.contract.ContractBuilder;
import com.nogle.core.contract.ContractInquiry;
import com.nogle.core.contract.DefaultContractBuilder;
import com.nogle.core.contract.FileContractBuilder;
import com.nogle.core.contract.PersistContractBuilder;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.instrument.Instrument;

public class FactoryBase {

    static ContractBuilder builder;
    static Map<String, Instrument> instruments;

    public static ContractInquiry init(
        final EngineMode mode,
        final Map<String, Instrument> instruments,
        final String serverName,
        final String infoFile,
        final boolean hasLocalContractInfo,
        final boolean isUsingCache,
        final boolean isSingleThread) throws FileNotFoundException {

        if (builder == null) {
            FactoryBase.instruments = instruments;
            switch (getMode(mode, hasLocalContractInfo, isUsingCache, isSingleThread)) {
                case FROM_PERSIST:
                    builder = fromPersist(serverName);
                    break;
                case FROM_LOCALINFO:
                    builder = fromLocal(infoFile);
                    break;
                case FROM_LOCALCACHE:
                    final ContractBuilder inquiryHelper = fromPersist(serverName);
                    builder = fromCache(inquiryHelper);
                    break;
                case FROM_LOCALCACHE_ONLY:
                    builder = fromCache(new DefaultContractBuilder());
                    break;
                case STUB:
                    builder = new DefaultContractBuilder();
                    break;
            }
        }
        return builder;
    }

    static FileContractBuilder fromLocal(final String filename) throws FileNotFoundException {
        if (Files.exists(Path.of(filename), LinkOption.NOFOLLOW_LINKS)) {
            final InputStream inputStream = new FileInputStream(filename);
            return new FileContractBuilder(inputStream);
        }
        final InputStream inputStream = ContractFactory.class.getClassLoader().getResourceAsStream(filename);
        return new FileContractBuilder(inputStream);
    }

    static CachedPersistContractBuilder fromCache(final ContractBuilder inquiryHelper) {
        return new CachedPersistContractBuilder(inquiryHelper);
    }

    static PersistContractBuilder fromPersist(final String serverName) {
        return new PersistContractBuilder(serverName);
    }

    static String getTargetBroker(final EngineMode mode) {
        switch (mode) {
            case SIMULATION:
                return SimulationConfig.getTargetBroker();
            case PRODUCTION:
                return EngineConfig.getBroker();
            default:
                return EngineConfig.getBroker();
        }
    }

    private static InquiryMode getMode(final EngineMode mode, final boolean hasLocalContractInfo, final boolean isUsingCache, final boolean isSingleThread) {
        if (EngineMode.PRODUCTION.equals(mode)) {
            return InquiryMode.FROM_PERSIST;
        }
        if (EngineMode.SIMULATION.equals(mode)) {
            if (isUsingCache) {
                if (isSingleThread) {
                    return InquiryMode.FROM_LOCALCACHE_ONLY;
                }
                return InquiryMode.FROM_LOCALCACHE;
            }
            return InquiryMode.FROM_PERSIST;
        }
        if (EngineMode.TESTING.equals(mode) && hasLocalContractInfo) {
            return InquiryMode.FROM_LOCALINFO;
        }
        return InquiryMode.STUB;
    }

    enum InquiryMode {
        FROM_PERSIST,
        FROM_LOCALINFO,
        FROM_LOCALCACHE,
        FROM_LOCALCACHE_ONLY,
        STUB,
        ;
    }

}
