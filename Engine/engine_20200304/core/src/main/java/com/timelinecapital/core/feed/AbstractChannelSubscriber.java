package com.timelinecapital.core.feed;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ChannelConfig;
import com.timelinecapital.commons.channel.FeedContext;
import com.timelinecapital.commons.channel.FeedListener;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.core.feed.control.InstrumentManager;

public abstract class AbstractChannelSubscriber implements Channel {
    protected static final Logger log = LogManager.getLogger(AbstractChannelSubscriber.class);

    // private volatile long lastPcktReceived = 0;
    private long pcktCount = 0;

    final List<ChannelListener> listeners = new ArrayList<>();
    final FeedListener feedListener = new FeedListenerImpl();
    final InstrumentManager instrumentManager;
    final ChannelController channelController;

    AbstractChannelSubscriber(final String networkInterface, final int port) {
        final String channelId = networkInterface + "#" + port;
        instrumentManager = new InstrumentManager(channelId);
        channelController = new ChannelController(channelId, instrumentManager);
    }

    public abstract void buildFeedWorker(ChannelConfig config) throws FeedException;

    @Override
    public void registerListener(final Contract contract, final FeedType feedType, final ChannelListener channelListener) {
        if (channelListener != null) {
            synchronized (listeners) {
                listeners.add(channelListener);
                instrumentManager.getInstrumentController(contract).register(feedType, channelListener);
            }
        }
    }

    @Override
    public void removeListener(final Contract contract, final FeedType feedType, final ChannelListener channelListener) {
        if (channelListener != null) {
            synchronized (listeners) {
                listeners.remove(channelListener);
                instrumentManager.getInstrumentController(contract).discontinue(feedType, channelListener);
            }
        }
    }

    @Override
    public List<ChannelListener> getListeners() {
        return listeners;
    }

    public long getPacketCount() {
        return pcktCount;
    }

    class FeedListenerImpl implements FeedListener {

        @Override
        public void onFeedStarted(final FeedType feedType) {
            listeners.forEach(channelListener -> channelListener.onFeedStarted(feedType));
            pcktCount = 0;
        }

        @Override
        public void onFeedStopped(final FeedType feedType) {
            listeners.forEach(channelListener -> channelListener.onFeedStopped(feedType));
        }

        @Override
        public void onPacket(final FeedContext feedContext, final Packet packet) {
            // lastPcktReceived = System.currentTimeMillis();
            channelController.routePacket(feedContext, packet);
            pcktCount++;
        }

    }

}
