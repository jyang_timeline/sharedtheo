package com.timelinecapital.core.feed;

import java.util.List;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedState;
import com.timelinecapital.commons.channel.FeedType;

public interface Channel {

    /**
     * Closes MDP Channel and releases all resources.
     */
    void close();

    /**
     * Gets current State of the channel.
     *
     * @return Channel State
     */
    FeedState getState();

    /**
     * Registers Channel Listener.
     *
     * @param channelListener Instance of Channel Listener
     * @see ChannelListener
     */
    void registerListener(Contract contract, FeedType feedType, ChannelListener channelListener);

    /**
     * Removes Channel Listener.
     *
     * @param channelListener Instance of Channel Listener
     * @see ChannelListener
     */
    void removeListener(Contract contract, FeedType feedType, ChannelListener channelListener);

    /**
     * Gets all registered Channel Listeners.
     *
     * @return List of ChannelListeners
     * @see ChannelListener
     */
    List<ChannelListener> getListeners();

    /**
     * Starts defined feed.
     *
     * @param feedType
     */
    void startFeed(Exchange exchange, FeedType feedType);

    /**
     * Stops defined feed.
     *
     * @param feedType
     */
    void stopFeed(Exchange exchange, FeedType feedType);

    /**
     * Stops all Feeds.
     */
    void stopAllFeeds();

    /**
     * Subscribes to the given security.
     *
     * @param securityId Security ID
     * @return true if subscribed with success
     */
    boolean subscribe(Contract contract);

    /**
     * Removes subscription to the given security.
     *
     * @param securityId Security ID
     */
    void discontinueSecurity(int securityId);

}
