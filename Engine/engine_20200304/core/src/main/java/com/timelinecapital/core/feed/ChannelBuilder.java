package com.timelinecapital.core.feed;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.channel.FeedWorker;

class ChannelBuilder {

    public static final int DEF_INCR_QUEUE_SIZE = 15000;

    private final String channelId;

    private final Map<FeedType, String> feedNetworkInterfaces = new HashMap<>();
    private ChannelListener channelListener;
    private ScheduledExecutorService scheduler;
    private int incrQueueSize = DEF_INCR_QUEUE_SIZE;
    private int rcvBufSize = FeedWorker.RCV_BUFFER_SIZE;

    public ChannelBuilder(final String channelId) {
        this.channelId = channelId;
    }

    public ChannelBuilder(final String channelId, final URI cfgURI, final URI schemaURI) {
        this.channelId = channelId;
    }

    public ChannelBuilder setNetworkInterface(final FeedType feedType, final String networkInterface) {
        feedNetworkInterfaces.put(feedType, networkInterface);
        return this;
    }

    public ChannelBuilder usingListener(final ChannelListener channelListener) {
        this.channelListener = channelListener;
        return this;
    }

    public ChannelBuilder usingScheduler(final ScheduledExecutorService scheduler) {
        this.scheduler = scheduler;
        return this;
    }

    public ChannelBuilder usingIncrQueueSize(final int incrQueueSize) {
        this.incrQueueSize = incrQueueSize;
        return this;
    }

    public ChannelBuilder usingRcvBufSize(final int rcvBufSize) {
        this.rcvBufSize = rcvBufSize;
        return this;
    }

    public Channel build() {
        try {
            // final Channel channel = new ChannelImpl(
            // incrQueueSize,
            // rcvBufSize,
            // feedNetworkInterfaces);
            // if (channelListener != null) {
            // channel.registerListener(channelListener);
            // }
            // return channel;
            return null;
        } catch (final Exception e) {
            throw new IllegalStateException("Failed to build Channel", e);
        }
    }
}
