package com.timelinecapital.core.feed;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.commons.channel.FeedContext;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.feed.control.InstrumentController;
import com.timelinecapital.core.feed.control.InstrumentManager;

public class ChannelController {
    private static final Logger log = LogManager.getLogger(ChannelController.class);

    final String channelId;
    private final InstrumentManager instrumentManager;

    public ChannelController(final String channelId, final InstrumentManager instrumentManager) {
        this.channelId = channelId;
        this.instrumentManager = instrumentManager;
    }

    public final void routePacket(final FeedContext feedContext, final Packet packet) {
        // final FeedType msgFeedType = feedContext.getFeedType();
        final DataType dataType = DataType.decode(packet.getMsgType());
        switch (dataType) {
            case SNAPSHOT:
                handleSnapshot(packet);
                break;
            case REALTIME_MARKETBOOK:
            case MARKETBOOK:
                handleMarketBook(packet);
                break;
            case TICK:
                handleTrade(packet);
                break;
            case BESTORDERDETAIL:
                handleBestPriceOrderDetail(packet);
                break;
            case ORDERACTIONS:
                handleRealTimeOrderAction(packet);
                break;
            case ORDERQUEUE:
                handleRealTimeOrderQueue(packet);
                break;
            default:
                log.debug("Message has been ignored due to its SemanticMsgType '{}'", dataType);
        }
    }

    final void handleSnapshot(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onSnapshot(packet);
        }
    }

    final void handleMarketBook(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onMarketBook(packet);
        }
    }

    final void handleTrade(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onTrade(packet);
        }
    }

    final void handleBestPriceOrderDetail(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onBestPriceOrderDetail(packet);
        }
    }

    final void handleRealTimeOrderAction(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onOrderActions(packet);
        }
    }

    final void handleRealTimeOrderQueue(final Packet packet) {
        final InstrumentController instrumentController = instrumentManager.getInstrumentController(packet);
        if (instrumentController != null) {
            instrumentController.onOrderQueue(packet);
        }
    }

}
