package com.timelinecapital.core.feed;

import java.lang.reflect.Constructor;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.QuoteTrackDown;
import com.timelinecapital.core.execution.report.factory.MDTrackFactory;
import com.timelinecapital.core.execution.report.factory.MDTrackRecycler;
import com.timelinecapital.core.pool.SuperPool;
import com.timelinecapital.strategy.types.MarketData;

public class ChannelEventTracker<T extends MarketData> implements ChannelListenerTracker<T> {
    private static final Logger log = LogManager.getLogger(ChannelEventTracker.class);

    private final SuperPool<QuoteTrackDown> objectPool;
    private final MDTrackFactory trackFactory;
    private MDTrackRecycler trackRecycler;

    private final long id;
    private Queue<QuoteTrackDown> eventQueue;
    private boolean isFull = false;

    ChannelEventTracker(final long id) {
        this.id = id;
        objectPool = new SuperPool<>(QuoteTrackDown.class);
        trackFactory = new MDTrackFactory(objectPool);
        try {
            final Constructor<MDTrackRecycler> cMDTrack = MDTrackRecycler.class.getConstructor(int.class, objectPool.getClass());
            trackRecycler = cMDTrack.newInstance(objectPool.getChainSize(), objectPool);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void setCapacity(final Queue<QuoteTrackDown> eventQueue) {
        this.eventQueue = eventQueue;
    }

    @Override
    public boolean isFull() {
        return isFull;
    }

    @Override
    public void track(final DataType dataType, final T event, final double value) {
        final QuoteTrackDown object = trackFactory.get();
        object.setEventContent(dataType, event.getContract(), event.getSequenceNo(), value);
        isFull = !eventQueue.offer(object);
        log.info("statshing {} {} {} {}", id, dataType, event, value);
    }

    @Override
    public void flush() {
        while (!eventQueue.isEmpty()) {
            final QuoteTrackDown obj = eventQueue.poll();
            log.info("{},{}", id, obj);
            trackRecycler.recycle(obj);
        }
        eventQueue.clear();
    }

}
