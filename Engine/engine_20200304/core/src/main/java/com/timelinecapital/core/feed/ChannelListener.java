package com.timelinecapital.core.feed;

import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.strategy.types.MarketData;

public interface ChannelListener {

    enum ListenerType {
        QUOTE_THREAD_SHARED_THEO,
        QUOTE_THREAD_STRATEGY,
        FORK_THREAD_SHARED_THEO,
        FORK_THREAD_STRATEGY,
        OTHER,
        ;
    }

    /**
     * Called when a Channel Feed is started.
     *
     * @param feedType Type of MDP Feed (e.g. snapshot or incremental)
     */
    void onFeedStarted(final FeedType feedType);

    /**
     * Called when a Channel Feed is stopped.
     *
     * @param feedType type of MDP Feed (e.g. snapshot or incremental)
     */
    void onFeedStopped(final FeedType feedType);

    /**
     * Called when Channel Feed is under monitoring
     *
     * @param capacity of number of events for monitoring
     */
    void onFeedMonitoring(int capacity);

    /**
     * Called when a Channel Feed received new MDP Packet.
     *
     * @param <T>
     *
     * @param feedType Type of MDP Feed (e.g. snapshot or incremental)
     * @param mdpPacket MDP Packet which is just received and will be handled after this callback
     */
    <T extends MarketData> void onEvent(final DataType dataType, T event);

    /**
     * Returned listener type of an instance
     *
     * @return listener type
     */
    ListenerType getListenerType();

}
