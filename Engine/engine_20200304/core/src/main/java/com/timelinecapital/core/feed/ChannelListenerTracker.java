package com.timelinecapital.core.feed;

import java.util.Queue;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.QuoteTrackDown;

public interface ChannelListenerTracker<T> {

    void setCapacity(Queue<QuoteTrackDown> queue);

    boolean isFull();

    void track(DataType dataType, T event, double value);

    void flush();

}
