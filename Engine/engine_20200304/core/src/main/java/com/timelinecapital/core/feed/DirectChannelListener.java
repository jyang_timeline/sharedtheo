package com.timelinecapital.core.feed;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import com.nogle.core.strategy.event.FeedEventHandler;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.CoreScheduler;
import com.timelinecapital.core.scheduler.ScheduledEventListener;
import com.timelinecapital.strategy.types.MarketData;

public class DirectChannelListener implements ChannelListener {
    private static final Logger log = LogManager.getLogger(DirectChannelListener.class);

    @SuppressWarnings("rawtypes")
    private final FeedEventHandler[] handlers = new FeedEventHandler[DataType.values().length];
    private final long id;
    private final ListenerType listenerType;
    private final String fullDesc;
    private final MarketDataTracker tracker;
    private final TrackerShutdownTask task;

    public DirectChannelListener(final long id, final ListenerType listenerType) {
        this.id = id;
        this.listenerType = listenerType;
        fullDesc = createFullDesc();
        tracker = new MarketDataTracker(id);
        task = new TrackerShutdownTask();
    }

    public void registerEventHandler(final FeedEventHandler<?> eventHandler, final DataType dataType) {
        handlers[dataType.getSeqValue()] = eventHandler;
        log.debug("DirectChannelListener{theo-{}} Registered event handler: {} {} {}", id, eventHandler, dataType, Arrays.asList(handlers));
    }

    @Override
    public void onFeedStarted(final FeedType feedType) {
    }

    @Override
    public void onFeedStopped(final FeedType feedType) {
    }

    @Override
    public void onFeedMonitoring(final int capacity) {
        tracker.startWith(capacity);
        CoreScheduler.getInstance().withSingleJob(task, new DateTime().plusSeconds(10).toDate());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends MarketData> void onEvent(final DataType dataType, final T event) {
        final FeedEventHandler<T> eventHandler = handlers[dataType.getSeqValue()];
        eventHandler.wrap(event);
        eventHandler.handle();
        tracker.onEvent(dataType, event, eventHandler.getReturnedValue());
    }

    @Override
    public ListenerType getListenerType() {
        return listenerType;
    }

    private String createFullDesc() {
        return "DirectChannelListener{" +
            "Id=" + id +
            '}';
    }

    @Override
    public String toString() {
        return fullDesc;
    }

    class TrackerShutdownTask implements ScheduledEventListener {
        @Override
        public void onScheduledEvent() {
            log.info("{} is about to flush all tracked objects: #{}", id, tracker.getCaptured());
            tracker.endAndReset();
        }
    }

}
