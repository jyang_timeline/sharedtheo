package com.timelinecapital.core.feed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.contract.Endpoints;
import com.nogle.core.exception.ProxyActivationException;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ChannelConfig;
import com.timelinecapital.commons.channel.ConnectionConfig;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.channel.TransportProtocol;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.factory.EndpointFactory;
import com.timelinecapital.core.util.EndpointHelper;

public class FeedProxy {
    private static final Logger log = LogManager.getLogger(FeedProxy.class);

    private final Map<ChannelIdentifier, ChannelConfig> channels = new HashMap<>();
    private final Map<ChannelConfig, LiveChannelSubscriber> channelToSubscriber = new HashMap<>();
    private final List<Channel> channelSubscribers;

    public FeedProxy(final List<Channel> channelSubscribers) {
        this.channelSubscribers = channelSubscribers;
        init();
    }

    private void init() {
        for (final String exchange : EngineConfig.getExchanges()) {
            final String[] protocols = EngineConfig.getExchangeToProtocols().get(exchange);

            for (final String protocol : protocols) {
                final Endpoints endpoints = EndpointFactory.getFeedEndpoints(exchange, protocol);
                endpoints.get().forEach((k, v) -> {
                    buildChannelInfo(v, exchange, protocol, k);
                });
            }
        }

        if (StringUtils.isNotEmpty(EngineConfig.getBestPriceOrderDetailEndpoint())) {
            for (final String exchange : EngineConfig.getBestPriceOrderDetailExchanges()) {
                buildChannelInfo(
                    EngineConfig.getBestPriceOrderDetailEndpoint(),
                    exchange,
                    EngineConfig.getTradeProtocol(),
                    FeedType.BestPriceOrderDetail);
            }
        }
        if (StringUtils.isNotEmpty(EngineConfig.getOrderQueueEndpoint())) {
            for (final String exchange : EngineConfig.getOrderQueueExchanges()) {
                buildChannelInfo(
                    EngineConfig.getOrderQueueEndpoint(),
                    exchange,
                    EngineConfig.getTradeProtocol(),
                    FeedType.OrderQueue);
            }
        }
        if (StringUtils.isNotEmpty(EngineConfig.getOrderActionsEndpoint())) {
            for (final String exchange : EngineConfig.getOrderActionsExchanges()) {
                buildChannelInfo(
                    EngineConfig.getOrderActionsEndpoint(),
                    exchange,
                    EngineConfig.getTradeProtocol(),
                    FeedType.OrderActions);
            }
        }
        log.debug("[Channels] Start with channel: {}", channels);
    }

    private void buildChannelInfo(final String endpoint, final String exchange, final String protocol, final FeedType feedType) {
        final String ip = EndpointHelper.getIP(endpoint);
        final int port = EndpointHelper.getPort(endpoint);
        final String networkInterface = EndpointHelper.getNetworkInterface(endpoint);
        final ContentType contentType = EndpointHelper.getContentType(endpoint);

        if (isExcluded(networkInterface, exchange)) {
            log.warn("[Channels] {} from {} has been excluded because of properties", exchange, networkInterface);
            return;
        }

        final ChannelIdentifier identifier = new ChannelIdentifier(port, networkInterface);
        if (!channels.containsKey(identifier)) {
            final ChannelConfig channelConfig = new ChannelConfig(EngineConfig.getBroker(), protocol, networkInterface, port);
            channels.put(identifier, channelConfig);
        }
        updateChannelDesc(identifier, exchange, feedType);

        final ConnectionConfig cfg = new ConnectionConfig(feedType, contentType, TransportProtocol.UDP, ip, port, networkInterface);
        channels.get(identifier).addConnection(Exchange.valueOf(exchange), cfg);
    }

    private boolean isExcluded(final String nic, final String exchange) {
        if (EngineConfig.getExchangeToNICExclusions().containsKey(exchange)) {
            final String[] nics = EngineConfig.getExchangeToNICExclusions().get(exchange);
            return Arrays.stream(nics).anyMatch(nic::contentEquals);
        }
        return false;
    }

    private void updateChannelDesc(final ChannelIdentifier identifier, final String exchange, final FeedType feedType) {
        channels.keySet().forEach(id -> {
            if (id.equals(identifier)) {
                id.addExchange(Exchange.valueOf(exchange));
                id.addFeedType(feedType);
                log.debug("Updated: {} {} {}", identifier, id.exchanges, id.feedTypes);
            }
        });
    }

    public void subscribe(final Exchange exchange, final FeedType feedType) {
        final List<ChannelIdentifier> identifiers =
            channels.keySet().stream().filter(id -> id.exchanges.contains(exchange) && id.feedTypes.contains(feedType)).collect(Collectors.toList());
        identifiers.forEach(i -> {
            final ChannelConfig channelConfig = channels.get(i);
            if (channelToSubscriber.get(channelConfig) == null) {
                log.info("[Connection] ******** Subscribe to client endpoint: {} ********", channelConfig);
                try {
                    final LiveChannelSubscriber channelSubscriber = new LiveChannelSubscriber(channelConfig);
                    channelSubscriber.buildFeedWorker(channelConfig);
                    channelSubscriber.startFeed(exchange, feedType);
                    channelToSubscriber.put(channelConfig, channelSubscriber);
                    channelSubscribers.add(channelSubscriber);
                } catch (final FeedException e) {
                    log.error(e.getMessage(), e);
                    throw new ProxyActivationException("Fail to launch subscriber");
                }
            }
        });
    }

    private List<ChannelConfig> getChannelConfigs(final Exchange exchange, final FeedType feedType) {
        final List<ChannelConfig> configs = new ArrayList<>();
        final List<ChannelIdentifier> identifiers =
            channels.keySet().stream().filter(id -> id.exchanges.contains(exchange) && id.feedTypes.contains(feedType)).collect(Collectors.toList());
        for (final ChannelIdentifier identifier : identifiers) {
            configs.add(channels.get(identifier));
        }
        return configs;
    }

    public void subscribeContract(final Contract contract, final FeedType feedType) {
        for (final ChannelConfig channelConfig : getChannelConfigs(Exchange.valueOf(contract.getExchange()), feedType)) {
            channelToSubscriber.get(channelConfig).subscribe(contract);
        }
    }

    public void registerListener(final Contract contract, final FeedType feedType, final ChannelListener channelListener) {
        log.trace("[-REG-] {} {}", feedType, getChannelConfigs(Exchange.valueOf(contract.getExchange()), feedType).size());
        for (final ChannelConfig channelConfig : getChannelConfigs(Exchange.valueOf(contract.getExchange()), feedType)) {
            channelToSubscriber.get(channelConfig).registerListener(contract, feedType, channelListener);
        }
    }

    public void deregisterListener(final Contract contract, final FeedType feedType, final ChannelListener channelListener) {
        log.trace("[DEREG] {} {} {}", contract, feedType, getChannelConfigs(Exchange.valueOf(contract.getExchange()), feedType).size());
        for (final ChannelConfig channelConfig : getChannelConfigs(Exchange.valueOf(contract.getExchange()), feedType)) {
            channelToSubscriber.get(channelConfig).removeListener(contract, feedType, channelListener);
        }
    }

    private class ChannelIdentifier {
        int port;
        String networkInterface;
        int hashCode;
        Set<FeedType> feedTypes = new HashSet<>();
        Set<Exchange> exchanges = new HashSet<>();

        public ChannelIdentifier(final int port, final String networkInterface) {
            this.port = port;
            this.networkInterface = networkInterface;
            hashCode = buildHash();
        }

        private int buildHash() {
            final int prime = 31;
            int result = 1;
            result = prime * result + port;
            result = prime * result + (networkInterface == null ? 0 : networkInterface.hashCode());
            return result;
        }

        void addExchange(final Exchange exchange) {
            exchanges.add(exchange);
        }

        void addFeedType(final FeedType contentType) {
            feedTypes.add(contentType);
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (this.port == (((ChannelIdentifier) obj).port) && this.networkInterface.equals(((ChannelIdentifier) obj).networkInterface)) {
                return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

    }

}
