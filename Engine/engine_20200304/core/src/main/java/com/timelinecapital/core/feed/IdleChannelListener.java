package com.timelinecapital.core.feed;

import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.strategy.types.MarketData;

public final class IdleChannelListener implements ChannelListener {

    private static IdleChannelListener instance = new IdleChannelListener();

    public static IdleChannelListener getInstance() {
        return instance;
    }

    @Override
    public final void onFeedStarted(final FeedType feedType) {
    }

    @Override
    public final void onFeedStopped(final FeedType feedType) {
    }

    @Override
    public final void onFeedMonitoring(final int capacity) {
    }

    @Override
    public final <T extends MarketData> void onEvent(final DataType dataType, final T event) {
    }

    @Override
    public final ListenerType getListenerType() {
        return ListenerType.OTHER;
    }

}
