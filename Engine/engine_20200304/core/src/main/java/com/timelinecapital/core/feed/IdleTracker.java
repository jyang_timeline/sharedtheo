package com.timelinecapital.core.feed;

import java.util.Queue;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.QuoteTrackDown;
import com.timelinecapital.strategy.types.MarketData;

public class IdleTracker<T extends MarketData> implements ChannelListenerTracker<T> {

    private static IdleTracker<MarketData> instance = new IdleTracker<>();

    public static IdleTracker<MarketData> getInstance() {
        return instance;
    }

    private IdleTracker() {
    }

    @Override
    public void setCapacity(final Queue<QuoteTrackDown> eventQueue) {
    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public void track(final DataType dataType, final T event, final double value) {
    }

    @Override
    public void flush() {
    }

}
