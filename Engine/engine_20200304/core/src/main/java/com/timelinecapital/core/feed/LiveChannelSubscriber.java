package com.timelinecapital.core.feed;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.core.EngineStatusView;
import com.nogle.core.config.EngineThreadPolicy;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ChannelConfig;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedState;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.channel.FeedWorker;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.util.AffinityThreadFactory;
import com.timelinecapital.core.util.TaskThreadFactory;

import net.openhft.affinity.AffinityStrategies;

public class LiveChannelSubscriber extends AbstractChannelSubscriber {

    private static final int RCV_BUFFER_SIZE = 4 * 1024 * 1024;

    private final Map<ChannelConfig, Pair<FeedWorker, Thread>> feeds = new ConcurrentHashMap<>();
    private final ChannelConfig channelConfig;

    public LiveChannelSubscriber(final ChannelConfig channelConfig) {
        super(channelConfig.getNetworkInterface(), channelConfig.getPort());
        this.channelConfig = channelConfig;
    }

    @Override
    public void buildFeedWorker(final ChannelConfig config) throws FeedException {
        if (!feeds.containsKey(config)) {
            synchronized (this) {
                if (!feeds.containsKey(config)) {
                    final FeedWorker feedWorker = new FeedWorker(config, RCV_BUFFER_SIZE);
                    feedWorker.addListener(feedListener);
                    feeds.put(config, MutablePair.of(feedWorker, null));

                    final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, feedWorker.getName());
                    // probe.setConnectionKey(feedWorker.getName());
                    probe.setConnected(false);
                    EngineStatusView.getInstance().addConnectionProbe(probe);

                    log.warn("FeedWorker {} has been initialized", feedWorker.getCfg());
                }
            }
        }
    }

    @Override
    public void startFeed(final Exchange exchange, final FeedType feedType) {
        final Pair<FeedWorker, Thread> feedThread = feeds.get(channelConfig);
        final FeedWorker feedWorker = feedThread.getLeft();
        if (!feedWorker.cancelShutdownIfStarted()) {
            if (!feedWorker.isActive()) {
                ThreadFactory threadFactory;
                if (EngineThreadPolicy.DEDICATE.equals(EngineConfig.getSocketPolicy())) {
                    threadFactory = new AffinityThreadFactory(feedWorker.getName(), false, AffinityStrategies.ANY);
                } else {
                    threadFactory = TaskThreadFactory.createThreadFactory(feedWorker.getName(), 0);
                }
                final Thread thread = threadFactory.newThread(feedWorker);
                feedThread.setValue(thread);
                thread.start();

                EngineStatusView.getInstance().updateConnectionProbeStatus(feedWorker.getName(), true);
            }
        }
    }

    @Override
    public void stopFeed(final Exchange exchange, final FeedType feedType) {
        final Pair<FeedWorker, Thread> feedThread = feeds.get(channelConfig);
        final FeedWorker feedWorker = feedThread.getLeft();
        if (feedWorker.isActive()) {
            feedWorker.shutdown();

            EngineStatusView.getInstance().updateConnectionProbeStatus(feedWorker.getName(), false);
        }
    }

    @Override
    public void stopAllFeeds() {
        // TODO Auto-generated method stub
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public FeedState getState() {
        return FeedState.ACTIVE;
    }

    @Override
    public boolean subscribe(final Contract contract) {
        instrumentManager.registerSecurity(contract);
        return true;
    }

    @Override
    public void discontinueSecurity(final int securityId) {
        // TODO Auto-generated method stub
    }

    class FeedIdentifier {
        private final Exchange exchange;
        private final FeedType feedType;
        private final int hashCode;

        public FeedIdentifier(final Exchange exchange, final FeedType feedType) {
            this.exchange = exchange;
            this.feedType = feedType;
            hashCode = buildHash();
        }

        private int buildHash() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (exchange == null ? 0 : exchange.hashCode());
            result = prime * result + (feedType == null ? 0 : feedType.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (this.exchange.equals(((FeedIdentifier) obj).exchange) && this.feedType.equals(((FeedIdentifier) obj).feedType)) {
                return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

    }

    // public static void main(final String[] args) {
    // System.setProperty("logFilename", "123");
    // final LiveChannelSubscriber subscriber = new LiveChannelSubscriber();
    // try {
    // subscriber.buildFeedWorker(Exchange.DCE, FeedType.Snapshot);
    // subscriber.startFeed(FeedType.Snapshot);
    // } catch (final Exception e) {
    // e.printStackTrace();
    // }
    // System.out.println("123");
    // }

}
