package com.timelinecapital.core.feed;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.EventTask;
import com.nogle.core.strategy.event.FeedEventHandler;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.strategy.types.MarketData;

public class MDChannelListener implements ChannelListener {
    private static final Logger log = LogManager.getLogger(MDChannelListener.class);

    @SuppressWarnings("rawtypes")
    private final FeedEventHandler[] handlers = new FeedEventHandler[DataType.values().length];
    private final long strategyId;
    private final EventTask task;
    private final ListenerType listenerType;
    private final String fullDesc;
    // private final MarketDataTracker tracker;

    public MDChannelListener(final long strategyId, final EventTask task) {
        this.strategyId = strategyId;
        this.task = task;
        listenerType = ListenerType.FORK_THREAD_STRATEGY;
        fullDesc = createFullDesc();
        // tracker = new MarketDataTracker(strategyId);
    }

    public void registerEventHandler(final FeedEventHandler<?> eventHandler, final DataType dataType) {
        handlers[dataType.getSeqValue()] = eventHandler;
        log.debug("{} Registered event handler: {} {} {}", this.toString(), eventHandler, dataType, Arrays.asList(handlers));
    }

    @Override
    public void onFeedStarted(final FeedType feedType) {
    }

    @Override
    public void onFeedStopped(final FeedType feedType) {
    }

    @Override
    public void onFeedMonitoring(final int capacity) {
        log.warn("{} with {} does not have any tracker running", strategyId, task);
        // tracker.startWith(capacity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends MarketData> void onEvent(final DataType dataType, final T event) {
        final FeedEventHandler<T> eventHandler = handlers[dataType.getSeqValue()];
        eventHandler.wrap(event);
        task.invoke(eventHandler);
        // tracker.onEvent(dataType, event, 0.0d);
    }

    @Override
    public ListenerType getListenerType() {
        return listenerType;
    }

    private String createFullDesc() {
        return "MDChannelListener{" +
            "StrategyId=" + strategyId +
            " ,EventTask=" + task.getTaskName() +
            '}';
    }

    @Override
    public String toString() {
        return fullDesc;
    }

}
