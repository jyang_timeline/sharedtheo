package com.timelinecapital.core.feed;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.QuoteTrackDown;
import com.timelinecapital.strategy.types.MarketData;

public class MarketDataTracker {
    private static final Logger log = LogManager.getLogger(MarketDataTracker.class);

    private final long id;
    private final int maxCapacity = 10000;
    private final AtomicBoolean isTrackerRunning = new AtomicBoolean(false);

    private final ChannelListenerTracker<MarketData> eventTracker;
    private ChannelListenerTracker<MarketData> currentTracker = IdleTracker.getInstance();
    private Queue<QuoteTrackDown> eventQueue;

    public MarketDataTracker(final long id) {
        eventTracker = new ChannelEventTracker<>(id);
        this.id = id;
    }

    public long getKey() {
        return id;
    }

    public int getCaptured() {
        return eventQueue.size();
    }

    public void startWith(int capacity) {
        if (isTrackerRunning.get()) {
            log.warn("Tracker job is already running");
            return;
        }
        if (capacity <= 0 || capacity >= maxCapacity) {
            capacity = maxCapacity;
        }
        eventQueue = new ArrayDeque<>(capacity);
        eventTracker.setCapacity(eventQueue);
        currentTracker = eventTracker;
        isTrackerRunning.set(true);
    }

    public void endAndReset() {
        eventTracker.flush();
        currentTracker = IdleTracker.getInstance();
        log.info("{} has all data flushed {}", id, eventTracker);
        isTrackerRunning.set(false);
    }

    public void onEvent(final DataType dataType, final MarketData event, final double value) {
        currentTracker.track(dataType, event, value);
        if (currentTracker.isFull()) {
            log.info("{} using idle tracker", id);
            currentTracker = IdleTracker.getInstance();
        }
    }

}
