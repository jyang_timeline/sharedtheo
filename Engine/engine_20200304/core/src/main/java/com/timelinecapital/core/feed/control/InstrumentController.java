package com.timelinecapital.core.feed.control;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.feed.IdleChannelListener;
import com.timelinecapital.core.mktdata.QuoteView;

public class InstrumentController {
    private static final Logger log = LogManager.getLogger(InstrumentController.class);

    private final QuoteView quoteView;
    private final String channelId;
    private final ChannelListener[][] listeners = new ChannelListener[FeedType.values().length][1];

    private volatile boolean enable = true;

    public InstrumentController(final QuoteView quoteView, final String channelId) {
        this.quoteView = quoteView;
        this.channelId = channelId;
        for (int i = 0; i < FeedType.values().length; i++) {
            listeners[i][0] = IdleChannelListener.getInstance();
        }
        log.info("Inintiated {} : {}", channelId, this);
    }

    public void register(final FeedType feedType, final ChannelListener channelListener) {
        if (channelListener != null) {
            synchronized (listeners) {
                switch (feedType) {
                    case Snapshot:
                        quoteView.onFeedSubscribe(DataType.SNAPSHOT);
                        break;
                    case MarketBook:
                        quoteView.onFeedSubscribe(DataType.MARKETBOOK);
                        break;
                    case Trade:
                        quoteView.onFeedSubscribe(DataType.TICK);
                        break;
                    case BestPriceOrderDetail:
                        quoteView.onFeedSubscribe(DataType.BESTORDERDETAIL);
                        break;
                    case OrderActions:
                        quoteView.onFeedSubscribe(DataType.ORDERACTIONS);
                        break;
                    case OrderQueue:
                        quoteView.onFeedSubscribe(DataType.ORDERQUEUE);
                        break;
                }
                listeners[feedType.getIndex()] = ArrayUtils.removeElement(listeners[feedType.getIndex()], IdleChannelListener.getInstance());
                listeners[feedType.getIndex()] = ArrayUtils.add(listeners[feedType.getIndex()], channelListener);
                log.trace("{} Added. CurrentListener: {} {} {}", channelId, quoteView.getContract().getSymbol(), feedType, listeners[feedType.getIndex()]);
            }
        }
    }

    public void discontinue(final FeedType feedType, final ChannelListener channelListener) {
        if (channelListener != null) {
            synchronized (listeners) {
                listeners[feedType.getIndex()] = ArrayUtils.removeElement(listeners[feedType.getIndex()], channelListener);
                if (listeners[feedType.getIndex()].length == 0) {
                    listeners[feedType.getIndex()] = ArrayUtils.add(listeners[feedType.getIndex()], IdleChannelListener.getInstance());
                }
                log.trace("Removed. CurrentListener: {} {} {}", quoteView.getContract().getSymbol(), feedType, listeners[feedType.getIndex()]);
            }
        }
    }

    public final void onSnapshot(final Packet packet) {
        if (enable) {
            quoteView.updateMarketDataSnapshot(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.Snapshot.getIndex()]) {
                channelListener.onEvent(DataType.SNAPSHOT, quoteView.getMarketDataSnapshot());
            }
            quoteView.onFeedStatistics(DataType.SNAPSHOT);
            quoteView.notifySnapshotWatchdog();
        }
    }

    public final void onMarketBook(final Packet packet) {
        if (enable) {
            quoteView.updateBook(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.MarketBook.getIndex()]) {
                channelListener.onEvent(DataType.MARKETBOOK, quoteView.getBook());
            }
            quoteView.onFeedStatistics(DataType.MARKETBOOK);
            quoteView.notifyBookWatchdog();
        }
    }

    public final void onTrade(final Packet packet) {
        if (enable) {
            quoteView.updateTick(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.Trade.getIndex()]) {
                channelListener.onEvent(DataType.TICK, quoteView.getTick());
            }
            quoteView.onFeedStatistics(DataType.TICK);
        }
    }

    public final void onBestPriceOrderDetail(final Packet packet) {
        if (enable) {
            quoteView.updateBestPriceOrderDetail(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.BestPriceOrderDetail.getIndex()]) {
                channelListener.onEvent(DataType.BESTORDERDETAIL, quoteView.getBestPriceOrderDetail());
            }
            quoteView.onFeedStatistics(DataType.BESTORDERDETAIL);
        }
    }

    public final void onOrderActions(final Packet packet) {
        if (enable) {
            quoteView.updateOrderActions(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.OrderActions.getIndex()]) {
                channelListener.onEvent(DataType.ORDERACTIONS, quoteView.getOrderAction());
            }
            quoteView.onFeedStatistics(DataType.ORDERACTIONS);
        }
    }

    public final void onOrderQueue(final Packet packet) {
        if (enable) {
            quoteView.updateOrderQueue(TradeClock.getCurrentMicros(), packet);
            for (final ChannelListener channelListener : listeners[FeedType.OrderQueue.getIndex()]) {
                channelListener.onEvent(DataType.ORDERQUEUE, quoteView.getOrderQueue());
            }
            quoteView.onFeedStatistics(DataType.ORDERQUEUE);
        }
    }

    public final void enable() {
        enable = true;
    }

    public final void disable() {
        enable = false;
    }

}
