package com.timelinecapital.core.feed.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.koloboke.collect.map.ObjObjMap;
import com.koloboke.collect.map.hash.HashObjObjMaps;
import com.nogle.strategy.types.Contract;
import com.nogle.util.HashWrapper;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.InstrumentParser;
import com.timelinecapital.core.mktdata.QuoteView;
import com.timelinecapital.core.mktdata.QuoteViewFactory;

public class InstrumentManager {
    private static final Logger log = LoggerFactory.getLogger(InstrumentManager.class);

    private final ObjObjMap<Contract, InstrumentController> contractToController = HashObjObjMaps.newMutableMap();
    private final ObjObjMap<HashWrapper<byte[]>, InstrumentController> wrapperToController = HashObjObjMaps.newMutableMap();

    private final byte[] data = new byte[34];

    private final InstrumentParser parser;
    private final HashWrapper<byte[]> wrapper;
    private final String channelId;

    public InstrumentManager(final String channelId) {
        this.channelId = channelId;
        parser = ParserInstanceFactory.getInstrumentParser();
        wrapper = getWrapper(null);
    }

    public InstrumentController getInstrumentController(final Packet packet) {
        packet.getMsgSymbol(data);
        wrapper.setObject(data);
        return wrapperToController.get(wrapper);
    }

    public InstrumentController getInstrumentController(final Contract contract) {
        return contractToController.get(contract);
    }

    public void registerSecurity(final Contract contract) {
        final HashWrapper<byte[]> wrapper = getHashWrapper(contract.getSymbol());
        final InstrumentController instrumentController = wrapperToController.get(wrapper);
        if (instrumentController == null) {
            final QuoteView quoteView = QuoteViewFactory.fromContract(contract);
            final InstrumentController controller = new InstrumentController(quoteView, channelId);
            wrapperToController.put(wrapper, controller);
            contractToController.put(contract, controller);
            log.trace("Register: {} {} {}", contract, wrapperToController, contractToController);
        } else {
            instrumentController.enable();
        }
    }

    public void discontinueSecurity(final Contract contract) {
        final HashWrapper<byte[]> wrapper = getHashWrapper(contract.getSymbol());
        final InstrumentController instrumentController = wrapperToController.get(wrapper);
        if (instrumentController == null) {
            log.warn("discontinueSecurity method was called but there is no security with id '{}'", contract.getSymbol());
        } else {
            instrumentController.disable();
        }
    }

    private HashWrapper<byte[]> getWrapper(final byte[] binaryData) {
        return new HashWrapper<>(binaryData, data -> parser.hashSymbol(data), (data1, data2) -> parser.hasSameSymbol(data1, data2));
    }

    private HashWrapper<byte[]> getHashWrapper(final String symbol) {
        parser.setSymbol(symbol);
        return getWrapper(parser.getData());
    }

    // public static void main(final String[] args) {
    // final InstrumentManager instrumentManager = new InstrumentManager("1", new ArrayList<>());
    // final HashWrapper<byte[]> wrapper = instrumentManager.getHashWrapper("cu1801");
    // System.out.println(wrapper);
    //
    // final QuoteView quoteView = new ArrayQuoteView(ContractFactory.getDummyContract("cu1801", "SHFE"), 1);
    // instrumentManager.registerSecurity(quoteView);
    //
    // final Packet mdpPacket = Packet.instance();
    // final ByteBuffer byteBuffer = ByteBuffer.allocateDirect(Constants.PACKET_MAX_SIZE).order(ByteOrder.LITTLE_ENDIAN);
    // mdpPacket.wrapFromBuffer(byteBuffer);
    // final byte version = '1';
    // byteBuffer.put(version);
    // final byte type = '1';
    // byteBuffer.put(type);
    // final byte[] dst = new byte[32];
    // final byte[] src = "cu1801".getBytes();
    // System.arraycopy(src, 0, dst, 0, src.length);
    // byteBuffer.put(dst);
    //
    // final InstrumentController controller = instrumentManager.getInstrumentController(mdpPacket);
    // System.out.println(controller);
    // }

}
