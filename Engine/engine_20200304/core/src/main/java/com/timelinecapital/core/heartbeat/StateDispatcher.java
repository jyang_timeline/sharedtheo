package com.timelinecapital.core.heartbeat;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.commons.utils.StrategyHeartbeat;
import com.nogle.core.EngineStatusView;
import com.nogle.core.strategy.StrategyView;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.commons.metrics.MetricsTopics;
import com.timelinecapital.core.util.BinaryMessageTopics;

public class StateDispatcher {
    private static final Logger log = LogManager.getLogger(StateDispatcher.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final TagValueEncoder scheduleMsgEncoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
    private static final TagValueEncoder stateChangeEncoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);

    private static final BinaryMessage engineMessage = new BinaryMessage(new MessageHeader(BinaryMessageTopics.HEADER_HBT_ENGINE), null);
    private static final BinaryMessage strategyMessage = new BinaryMessage(new MessageHeader(BinaryMessageTopics.HEADER_HBT_STRATEGY), null);

    private static final BinaryMessage ENGINE_METRICS_CARRIER = new BinaryMessage(new MessageHeader(MetricsTopics.HEADER_METRICS_ENGINE), null);

    private static final StrategyHeartbeat[] singleStrategyArray = new StrategyHeartbeat[1];
    private final List<StrategyHeartbeat> singleStrategyWrapper;

    public StateDispatcher() {
        singleStrategyWrapper = Arrays.asList(singleStrategyArray);
    }

    public void publishEngineState(final EngineStatusView view) {
        try {
            final String engineHeartbeat = mapper.writeValueAsString(view.translateToHeartbeat());
            send(engineHeartbeat, engineMessage);
            log.info(engineHeartbeat);
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void publishEngineMetrics(final EngineStatusView view) {
        try {
            final String metrics = mapper.writeValueAsString(view.translateToMetrics());
            send(metrics, ENGINE_METRICS_CARRIER);
            log.info(metrics);
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void publishStrategyState(final StrategyView view) {
        singleStrategyArray[0] = view.onStatusRefresh();
        try {
            stateChangeEncoder.clear();
            stateChangeEncoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeStrategyInformation);
            stateChangeEncoder.append(TradeEngineCommunication.engineModelsDetailTag, mapper.writeValueAsString(singleStrategyWrapper));
            send(stateChangeEncoder.compose(), strategyMessage);
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void publishStrategyState(final List<StrategyView> views) {
        final List<StrategyHeartbeat> list = views.stream().map(v -> v.onStatusRefresh()).collect(Collectors.toList());
        try {
            scheduleMsgEncoder.clear();
            scheduleMsgEncoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeStrategyInformation);
            scheduleMsgEncoder.append(TradeEngineCommunication.engineModelsDetailTag, mapper.writeValueAsString(list));
            final String strategyHeartbeat = scheduleMsgEncoder.compose();
            send(strategyHeartbeat, strategyMessage);

            log.debug(strategyHeartbeat);

        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void send(final String message, final BinaryMessage carrier) {
        carrier.setPayLoad(message.getBytes(StandardCharsets.UTF_8));
        try {
            Messenger.send(carrier);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
