package com.timelinecapital.core.heartbeat;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.commons.type.SymbolPositionInfo;
import com.timelinecapital.core.stats.PositionSummary;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.util.BinaryMessageTopics;

public class TradeSummaryDispatcher {
    private static final Logger log = LogManager.getLogger(TradeSummaryDispatcher.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final TagValueEncoder scheduleMsgEncoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);

    private static final BinaryMessage strategyMessage = new BinaryMessage(new MessageHeader(BinaryMessageTopics.HEADER_HBT_CONTRACT), null);

    private final Map<TradingAccount, PositionSummary> positionOverviews;

    public TradeSummaryDispatcher(final Map<TradingAccount, PositionSummary> positionOverviews) {
        this.positionOverviews = positionOverviews;
    }

    public void publish() {
        for (final Entry<TradingAccount, PositionSummary> entry : positionOverviews.entrySet()) {

            final SymbolPositionInfo info = entry.getValue().translateTo();
            try {
                scheduleMsgEncoder.clear();
                scheduleMsgEncoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeStrategyInformation);
                scheduleMsgEncoder.append(TradeEngineCommunication.engineModelsDetailTag, mapper.writeValueAsString(info));
                send(scheduleMsgEncoder.compose(), strategyMessage);

                log.debug(scheduleMsgEncoder.compose());

            } catch (final JsonProcessingException e) {
                log.error(e.getMessage(), e);
            }
        }

    }

    private static void send(final String message, final BinaryMessage carrier) {
        carrier.setPayLoad(message.getBytes(StandardCharsets.UTF_8));
        try {
            Messenger.send(carrier);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
