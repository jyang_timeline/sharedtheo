package com.timelinecapital.core.instrument;

import com.nogle.strategy.types.Contract;

public interface Instrument extends Contract {

    void updatePriceLimit(double limitUp, double limitDown);

    SecurityType getSecurityType();

}
