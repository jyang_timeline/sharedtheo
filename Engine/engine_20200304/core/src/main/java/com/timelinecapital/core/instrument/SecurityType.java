package com.timelinecapital.core.instrument;

public enum SecurityType {

    FUTURES('F', true),
    STOCKS('E', true),
    INDICES('I', false),
    WARRANTS('W', true),
    OPTIONS('O', true),
    ;

    private final byte code;
    private final boolean hasPriceLimits;

    SecurityType(final char code, final boolean hasPriceLimits) {
        this.code = (byte) code;
        this.hasPriceLimits = hasPriceLimits;
    }

    static final SecurityType[] SEC_TYPE = new SecurityType[256];
    static {
        for (final SecurityType tp : values()) {
            SEC_TYPE[tp.getCode()] = tp;
        }
    }

    public byte getCode() {
        return code;
    }

    public boolean hasPriceLimits() {
        return hasPriceLimits;
    }

    public static SecurityType decode(final byte code) {
        return SEC_TYPE[code];
    }

}
