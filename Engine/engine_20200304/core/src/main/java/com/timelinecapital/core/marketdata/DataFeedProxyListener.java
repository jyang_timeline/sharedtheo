package com.timelinecapital.core.marketdata;

import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.QuoteOrderView;
import com.timelinecapital.strategy.types.OrderQueueView;

public interface DataFeedProxyListener {

    default void onBestPriceOrderDetail(final BestOrderView event) {
    }

    default void onOrderActions(final QuoteOrderView event) {
    }

    default void onOrderQueue(final OrderQueueView event) {
    }

}
