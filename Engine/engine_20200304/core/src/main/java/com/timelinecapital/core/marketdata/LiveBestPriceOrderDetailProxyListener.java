package com.timelinecapital.core.marketdata;

import com.nogle.core.EventTask;
import com.nogle.strategy.types.BestOrderView;
import com.timelinecapital.core.event.handler.BestPriceOrderDetailHandler;

@Deprecated
public class LiveBestPriceOrderDetailProxyListener implements DataFeedProxyListener {

    private final EventTask task;
    private final BestPriceOrderDetailHandler<BestOrderView> eventHandler;

    public LiveBestPriceOrderDetailProxyListener(final EventTask task, final BestPriceOrderDetailHandler<BestOrderView> eventHandler) {
        this.task = task;
        this.eventHandler = eventHandler;
    }

    @Override
    public final void onBestPriceOrderDetail(final BestOrderView event) {
        eventHandler.wrap(event);
        task.invoke(eventHandler);
    }

}
