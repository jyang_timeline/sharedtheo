package com.timelinecapital.core.marketdata;

import com.nogle.core.EventTask;
import com.nogle.strategy.types.QuoteOrderView;
import com.timelinecapital.core.event.handler.OrderActionsHandler;

@Deprecated
public class LiveOrderActionsProxyListener implements DataFeedProxyListener {

    private final EventTask task;
    private final OrderActionsHandler<QuoteOrderView> eventHandler;

    public LiveOrderActionsProxyListener(final EventTask task, final OrderActionsHandler<QuoteOrderView> eventHandler) {
        this.task = task;
        this.eventHandler = eventHandler;
    }

    @Override
    public final void onOrderActions(final QuoteOrderView event) {
        eventHandler.wrap(event);
        task.invoke(eventHandler);
    }

}
