package com.timelinecapital.core.marketdata;

import com.nogle.core.EventTask;
import com.timelinecapital.core.event.handler.OrderQueueHandler;
import com.timelinecapital.strategy.types.OrderQueueView;

@Deprecated
public class LiveOrderQueueProxyListener implements DataFeedProxyListener {

    private final EventTask task;
    private final OrderQueueHandler<OrderQueueView> eventHandler;

    public LiveOrderQueueProxyListener(final EventTask task, final OrderQueueHandler<OrderQueueView> eventHandler) {
        this.task = task;
        this.eventHandler = eventHandler;
    }

    @Override
    public void onOrderQueue(final OrderQueueView event) {
        eventHandler.wrap(event);
        task.invoke(eventHandler);
    }

}
