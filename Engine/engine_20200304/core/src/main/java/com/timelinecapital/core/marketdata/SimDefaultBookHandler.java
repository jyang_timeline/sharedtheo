package com.timelinecapital.core.marketdata;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.marketdata.type.BDBookView;

public class SimDefaultBookHandler implements SimulatedMarketBookHandler {

    private static final Set<DataType> VALUES = new HashSet<>(Arrays.asList(DataType.MARKETBOOK, DataType.REALTIME_MARKETBOOK));

    public SimDefaultBookHandler() {
    }

    @Override
    public void onMarketBookData(final PriceFeedProxyListener priceFeedProxyListener, final BDBookView marketbook) {
        if (VALUES.contains(marketbook.getDataType())) {
            priceFeedProxyListener.onMarketBook(marketbook);
        }
    }

}
