package com.timelinecapital.core.marketdata;

import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class SimDefaultTradeHandler implements SimulatedTradeHandler {

    public SimDefaultTradeHandler() {
    }

    @Override
    public void onTradeData(final PriceFeedProxyListener priceFeedProxyListener, final BDTickView tick) {
        priceFeedProxyListener.onTick(tick);
    }

}
