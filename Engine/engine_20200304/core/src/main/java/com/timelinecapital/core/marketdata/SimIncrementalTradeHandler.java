package com.timelinecapital.core.marketdata;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.core.marketdata.type.BDTickView;

public class SimIncrementalTradeHandler implements SimulatedTradeHandler {

    private static final Set<TickType> VALUES = new HashSet<>(Arrays.asList(TickType.BUYER, TickType.SELLER));

    public SimIncrementalTradeHandler() {
    }

    @Override
    public void onTradeData(final PriceFeedProxyListener priceFeedProxyListener, final BDTickView tick) {
        if (VALUES.contains(tick.getType())) {
            priceFeedProxyListener.onTick(tick);
        }
    }

}
