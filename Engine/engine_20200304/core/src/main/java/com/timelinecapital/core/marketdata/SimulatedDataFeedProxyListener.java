package com.timelinecapital.core.marketdata;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.QuoteOrderView;
import com.timelinecapital.strategy.types.OrderQueueView;

@Deprecated
public class SimulatedDataFeedProxyListener implements DataFeedProxyListener {

    private final StrategyUpdater strategyUpdater;

    public SimulatedDataFeedProxyListener(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    @Override
    public void onBestPriceOrderDetail(final BestOrderView event) {
        strategyUpdater.onBestPriceOrderDetail(event);
    }

    @Override
    public void onOrderActions(final QuoteOrderView event) {
        strategyUpdater.onOrderActions(event);
    }

    @Override
    public void onOrderQueue(final OrderQueueView event) {
        strategyUpdater.onOrderQueues(event);
    }

}
