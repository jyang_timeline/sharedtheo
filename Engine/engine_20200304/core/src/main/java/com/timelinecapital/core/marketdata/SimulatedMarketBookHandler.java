package com.timelinecapital.core.marketdata;

import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.timelinecapital.core.marketdata.type.BDBookView;

public interface SimulatedMarketBookHandler {

    void onMarketBookData(PriceFeedProxyListener priceFeedProxyListener, BDBookView marketbook);

}
