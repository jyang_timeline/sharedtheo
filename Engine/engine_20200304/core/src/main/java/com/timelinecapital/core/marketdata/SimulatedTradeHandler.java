package com.timelinecapital.core.marketdata;

import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.timelinecapital.core.marketdata.type.BDTickView;

public interface SimulatedTradeHandler {

    void onTradeData(PriceFeedProxyListener priceFeedProxyListener, BDTickView tick);

}
