package com.timelinecapital.core.marketdata.dummy;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.HeaderParser;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.CNExchangeTimeAdaptor;
import com.timelinecapital.core.marketdata.type.Quote;

public class DummyBestPriceOrderDetailInstance extends CNExchangeTimeAdaptor implements BDBestPriceOrderQueueView {

    private final Contract contract;
    private final Exchange exchange;

    private final long[] bidOrders = new long[10];
    private final long[] askOrders = new long[10];

    private static BDBestPriceOrderQueueView instance = new DummyBestPriceOrderDetailInstance(ContractFactory.getDummyContract("", Exchange.UNKNOWN.name()), null);

    static BDBestPriceOrderQueueView getInstance() {
        return instance;
    }

    DummyBestPriceOrderDetailInstance(final Contract contract, final HeaderParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
    }

    @Override
    public void setData(final ByteBuffer data, final long updateTimeMillis) {
    }

    @Override
    public void setConsumed() {
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public long getSequenceNo() {
        return 0;
    }

    @Override
    public long getExchangeSequenceNo() {
        return 0;
    }

    @Override
    public long getUpdateTimeMicros() {
        return 0;
    }

    @Override
    public long getUpdateTimeMillis() {
        return 0;
    }

    @Override
    public double getBestBidPrice() {
        return 0;
    }

    @Override
    public long getBestBidOrderQty(final int index) {
        return 0;
    }

    @Override
    public long[] getBestBidOrderQty() {
        return bidOrders;
    }

    @Override
    public double getBestAskPrice() {
        return 0;
    }

    @Override
    public long getBestAskOrderQty(final int index) {
        return 0;
    }

    @Override
    public long[] getBestAskOrderQty() {
        return askOrders;
    }

    @Override
    public DataType getDataType() {
        return DataType.BESTORDERDETAIL;
    }

    @Override
    public ContentType getContentType() {
        return ContentType.BestPriceOrderDetail;
    }

    @Override
    public Exchange getExchange() {
        return exchange;
    }

    @Override
    public String getKey() {
        return contract.getSymbol();
    }

    @Override
    public int compareTo(final Quote o) {
        return 0;
    }

}
