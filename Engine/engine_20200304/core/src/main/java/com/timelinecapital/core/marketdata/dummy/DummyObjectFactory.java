package com.timelinecapital.core.marketdata.dummy;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;

public class DummyObjectFactory {

    public static BDBestPriceOrderQueueView getBestPriceInstance(final Contract contract, final SbeVersion version) {
        return DummyBestPriceOrderDetailInstance.getInstance();
    }

    public static BDOrderActionsView getOrderActionsInstance(final Contract contract, final SbeVersion version) {
        return DummyOrderActionsInstance.getInstance();
    }

    public static BDOrderQueueView getOrderQueueInstance(final Contract contract, final SbeVersion version) {
        return DummyOrderQueueInstance.getInstance();
    }

}
