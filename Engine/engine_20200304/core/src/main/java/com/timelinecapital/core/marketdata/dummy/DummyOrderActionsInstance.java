package com.timelinecapital.core.marketdata.dummy;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.HeaderParser;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.CNExchangeTimeAdaptor;
import com.timelinecapital.core.marketdata.type.Quote;

public class DummyOrderActionsInstance extends CNExchangeTimeAdaptor implements BDOrderActionsView {

    private final Contract contract;
    private final Exchange exchange;

    private static BDOrderActionsView instance = new DummyOrderActionsInstance(ContractFactory.getDummyContract("", Exchange.UNKNOWN.name()), null);

    static BDOrderActionsView getInstance() {
        return instance;
    }

    DummyOrderActionsInstance(final Contract contract, final HeaderParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
    }

    @Override
    public void setData(final ByteBuffer data, final long updateTimeMillis) {
    }

    @Override
    public void setConsumed() {
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public long getSequenceNo() {
        return 0;
    }

    @Override
    public long getExchangeSequenceNo() {
        return 0;
    }

    @Override
    public long getUpdateTimeMicros() {
        return 0;
    }

    @Override
    public long getUpdateTimeMillis() {
        return 0;
    }

    @Override
    public ExecRequestType getExecRequestType() {
        return ExecRequestType.OrderEntry;
    }

    @Override
    public long getQuoteOrderId() {
        return 0;
    }

    @Override
    public Side getSide() {
        return Side.INVALID;
    }

    @Override
    public TimeCondition geTimeCondition() {
        return TimeCondition.GFD;
    }

    @Override
    public OrderType getOrderType() {
        return OrderType.LIMIT;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    @Override
    public long getQuantity() {
        return 0;
    }

    @Override
    public DataType getDataType() {
        return DataType.ORDERACTIONS;
    }

    @Override
    public ContentType getContentType() {
        return ContentType.RealTimeOrderAction;
    }

    @Override
    public Exchange getExchange() {
        return exchange;
    }

    @Override
    public String getKey() {
        return contract.getSymbol();
    }

    @Override
    public int compareTo(final Quote o) {
        return 0;
    }

}
