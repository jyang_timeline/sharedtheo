package com.timelinecapital.core.marketdata.dummy;

import java.nio.ByteBuffer;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.HeaderParser;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.CNExchangeTimeAdaptor;
import com.timelinecapital.core.marketdata.type.Quote;

public class DummyOrderQueueInstance extends CNExchangeTimeAdaptor implements BDOrderQueueView {

    private final Contract contract;
    private final Exchange exchange;
    private final int[] ordersQty = new int[200];

    private static BDOrderQueueView instance = new DummyOrderQueueInstance(ContractFactory.getDummyContract("", Exchange.UNKNOWN.name()), null);

    static BDOrderQueueView getInstance() {
        return instance;
    }

    DummyOrderQueueInstance(final Contract contract, final HeaderParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
    }

    @Override
    public void setData(final ByteBuffer data, final long updateTimeMillis) {
    }

    @Override
    public void setConsumed() {
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public long getSequenceNo() {
        return 0;
    }

    @Override
    public long getExchangeSequenceNo() {
        return 0;
    }

    @Override
    public long getUpdateTimeMicros() {
        return 0;
    }

    @Override
    public long getUpdateTimeMillis() {
        return 0;
    }

    @Override
    public Side getSide() {
        return Side.INVALID;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    @Override
    public int getQueueSize() {
        return 0;
    }

    @Override
    public int getOrderQueueQty(final int index) {
        return 0;
    }

    @Override
    public int[] getOrderQueues() {
        return ordersQty;
    }

    @Override
    public DataType getDataType() {
        return DataType.ORDERQUEUE;
    }

    @Override
    public ContentType getContentType() {
        return ContentType.RealTimeOrderQueue;
    }

    @Override
    public Exchange getExchange() {
        return exchange;
    }

    @Override
    public String getKey() {
        return contract.getSymbol();
    }

    @Override
    public int compareTo(final Quote o) {
        return 0;
    }

}
