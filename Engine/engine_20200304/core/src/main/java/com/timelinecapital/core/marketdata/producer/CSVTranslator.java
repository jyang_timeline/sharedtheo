package com.timelinecapital.core.marketdata.producer;

public interface CSVTranslator<T, A> {

    void translateTo(T dataType, A encoder, long clockDrift, String[] asciiData);

}
