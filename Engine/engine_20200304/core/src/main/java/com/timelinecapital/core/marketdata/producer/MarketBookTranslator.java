package com.timelinecapital.core.marketdata.producer;

import org.apache.commons.lang3.math.NumberUtils;

import com.nogle.core.marketdata.producer.metadata.MarketBookV3;
import com.nogle.core.marketdata.producer.metadata.QuoteHeaderEXGInfo;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.type.DataType;

public class MarketBookTranslator {

    public static final CSVTranslator<DataType, MarketBookEncoder> DYNAMIC_BOOK_TRANSLATOR = new CSVTranslator<>() {

        @Override
        public void translateTo(final DataType dataType, final MarketBookEncoder encoder, final long clockDrift, final String[] asciiData) {
            encoder.setVersion(asciiData[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
            encoder.setType(asciiData[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
            encoder.setSymbol(asciiData[QuoteHeaderEXGInfo.Symbol.getIndex()]);
            encoder.setSequenceNo(NumberUtils.toLong(asciiData[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
            encoder.setExchangeSequenceNo(NumberUtils.toLong(asciiData[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
            encoder.setMicroTime(NumberUtils.toLong(asciiData[QuoteHeaderEXGInfo.Timestamp.getIndex()]) + clockDrift);
            encoder.setExchangeTime(NumberUtils.toLong(asciiData[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

            encoder.setQuoteType(asciiData[MarketBookV3.QuoteType.getIndex()].getBytes()[0]);
            final int bidDepth = NumberUtils.toInt(asciiData[MarketBookV3.BidDepth.getIndex()]);
            encoder.setBidDepth(bidDepth);
            final int askDepth = NumberUtils.toInt(asciiData[MarketBookV3.AskDepth.getIndex()]);
            encoder.setAskDepth(askDepth);

            for (int i = 0; i < bidDepth; i++) {
                final String[] bid = asciiData[MarketBookV3.BookInfo.getIndex() + i].split("@");
                encoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
                encoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
            }
            for (int i = 0; i < askDepth; i++) {
                final String[] ask = asciiData[MarketBookV3.BookInfo.getIndex() + bidDepth + i].split("@");
                encoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
                encoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
            }
        }
    };

}
