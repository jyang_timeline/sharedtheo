package com.timelinecapital.core.marketdata.source;

import com.timelinecapital.core.config.SimulationConfig;

public class CustomDataFinder implements DataSource {

    private final String pathPattern;

    public CustomDataFinder(final String customKey) {
        pathPattern = SimulationConfig.getPathPref(customKey);
    }

    @Override
    public String getPathPattern() {
        return pathPattern;
    }

}