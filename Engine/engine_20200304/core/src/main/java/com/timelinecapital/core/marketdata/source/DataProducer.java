package com.timelinecapital.core.marketdata.source;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.exception.DataParserException;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.SimulationConfig;

public class DataProducer {
    private static final Logger log = LogManager.getLogger(DataProducer.class);

    private static final Pattern pathPattern = Pattern.compile("(\\$)([A-Za-z.]+)");
    private static final String fileNamePattern = "%1$tY%1$tm%1$td.txt";
    private static final String fromContractInfo = "ContractInfo";
    private static final String DEFAULT_SOURCE = "DEFAULT";

    private final Map<String, DataSource> pathBuilders = new HashMap<>();

    public DataProducer() {
        final List<String> subsetKeys = SimulationConfig.getPathSubsets();
        for (final String key : subsetKeys) {
            if (DataType.contains(key)) {
                pathBuilders.put(key, new FeedDataFinder(DataType.valueOf(key)));
            } else if (Exchange.contains(key)) {
                pathBuilders.put(key, new ExchangeDataFinder(key));
            } else {
                pathBuilders.put(key, new CustomDataFinder(key));
            }
        }
        pathBuilders.forEach((k, v) -> log.info("Pre-defined source {}", k));
    }

    private String getPattern(final String key, final Contract contract, final FeedType feed) {
        if (pathBuilders.containsKey(key)) {
            return pathBuilders.get(key).getPathPattern();
        }
        if (pathBuilders.containsKey(contract.getExchange())) {
            return pathBuilders.get(contract.getExchange()).getPathPattern();
        }
        if (pathBuilders.containsKey(feed.name())) {
            return pathBuilders.get(feed.name()).getPathPattern();
        }
        return pathBuilders.get(DEFAULT_SOURCE).getPathPattern();
    }

    private String getPath(final String key, final Contract contract, final FeedType feed) {
        final String path = SimulationConfig.getPath(key, feed);
        log.debug("current Path: {} {} {}", key, feed, path);
        if (fromContractInfo.contentEquals(path)) {
            return PathInfo.valueOf(key).getPath(contract);
        } else {
            return path;
        }
    }

    public Map<Contract, File[]> getHistoricalData(final Date date, final Map<String, ? extends Contract> contracts) throws DataParserException {
        final Map<Contract, File[]> contractToFile = new HashMap<>();
        final String filename = String.format(fileNamePattern, date);

        for (final Entry<String, ? extends Contract> entry : contracts.entrySet()) {
            final String pathStructure = getPattern(entry.getKey(), entry.getValue(), FeedType.Snapshot);

            String fullname = StringUtils.EMPTY;
            final Matcher matcher = pathPattern.matcher(pathStructure);
            while (matcher.find()) {
                fullname = Paths.get(fullname, getPath(matcher.group(2), entry.getValue(), FeedType.Snapshot)).toString();
            }
            final File parentFolder = Paths.get(fullname).toFile();
            log.info("Path for historical data: {}", fullname);

            final File file = FileUtils.getFile(parentFolder, filename);
            if (!file.exists()) {
                // throw new DataParserException("Quote file does not exist: " + file.getAbsolutePath());
                log.error("Continued with missing file: {}", file.getAbsolutePath());
                continue;
            }
            contractToFile.put(entry.getValue(), new File[] { file });
        }
        return contractToFile;
    }

    public Map<Contract, File[]> getFeedDataFiles(final Date date, final Map<FeedType, List<Contract>> lookupKeyToContract) throws DataParserException {
        final Map<Contract, File[]> contractToFile = new HashMap<>();
        final String filename = String.format(fileNamePattern, date);

        for (final Entry<FeedType, List<Contract>> feedTypeEntries : lookupKeyToContract.entrySet()) {
            for (final Contract contract : feedTypeEntries.getValue()) {
                final String pathStructure = getPattern(feedTypeEntries.getKey().name(), contract, feedTypeEntries.getKey());

                String fullname = StringUtils.EMPTY;
                final Matcher matcher = pathPattern.matcher(pathStructure);
                while (matcher.find()) {
                    fullname = Paths.get(fullname, getPath(matcher.group(2), contract, feedTypeEntries.getKey())).toString();
                }
                final File parentFolder = Paths.get(fullname).toFile();
                log.info("Path for {} data: {}", feedTypeEntries.getKey(), fullname);

                final File file = FileUtils.getFile(parentFolder, filename);
                if (!file.exists()) {
                    // throw new DataParserException("Quote file does not exist: " + file.getAbsolutePath());
                    log.error("Continued with missing file: {}", file.getAbsolutePath());
                    continue;
                }
                contractToFile.compute(contract, (k, v) -> ArrayUtils.add(v, file));
            }
        }
        return contractToFile;
    }

    private enum PathInfo {

        exchange {
            @Override
            public String getPath(final Contract contract) {
                return contract.getExchange();
            }
        },
        productGroup {
            @Override
            public String getPath(final Contract contract) {
                return contract.getProductGroup();
            }
        },
        symbol {
            @Override
            public String getPath(final Contract contract) {
                return contract.getSymbol();
            }
        };

        public abstract String getPath(Contract contract);
    }

}
