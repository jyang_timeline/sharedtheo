package com.timelinecapital.core.marketdata.source;

public interface DataSource {

    String getPathPattern();

    // String getPathPattern(DataType type);

    // String getPathPattern(String exchange);

    // String getContractPathPattern(String key);

}
