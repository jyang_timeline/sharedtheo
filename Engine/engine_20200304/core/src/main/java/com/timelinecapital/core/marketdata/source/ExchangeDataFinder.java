package com.timelinecapital.core.marketdata.source;

import com.timelinecapital.core.config.SimulationConfig;

public class ExchangeDataFinder implements DataSource {

    private final String pathPattern;

    public ExchangeDataFinder(final String exchange) {
        pathPattern = SimulationConfig.getPathPref(exchange);
    }

    @Override
    public String getPathPattern() {
        return pathPattern;
    }

}
