package com.timelinecapital.core.marketdata.source;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.SimulationConfig;

public class FeedDataFinder implements DataSource {

    private final String pathPattern;

    public FeedDataFinder(final DataType type) {
        pathPattern = SimulationConfig.getPathPref(type.name());
    }

    @Override
    public String getPathPattern() {
        return pathPattern;
    }

}
