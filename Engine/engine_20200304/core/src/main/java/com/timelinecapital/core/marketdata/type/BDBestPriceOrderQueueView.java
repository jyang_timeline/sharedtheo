package com.timelinecapital.core.marketdata.type;

import com.nogle.strategy.types.BestOrderView;

/**
 * As sub-interface stand for Binary-Data-BestPriceOrderQueue to support dummy object factory when running simulation
 *
 * @author Mark
 *
 */
public interface BDBestPriceOrderQueueView extends BestOrderView, BufferedMarketData, SimulationEvent {

}
