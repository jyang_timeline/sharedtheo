package com.timelinecapital.core.marketdata.type;

import com.nogle.strategy.types.BookView;

/**
 * As sub-interface stand for Binary-Data-MarketBook to support dummy object factory when running simulation
 *
 * * @author Mark
 *
 */
public interface BDBookView extends BookView, BufferedMarketData, SimulationEvent {

    void setOpenInterest(long openInterest);

    void setVolume(long volume);

}
