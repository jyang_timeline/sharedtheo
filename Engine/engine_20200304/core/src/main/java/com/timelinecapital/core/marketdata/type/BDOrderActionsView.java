package com.timelinecapital.core.marketdata.type;

import com.nogle.strategy.types.QuoteOrderView;

/**
 * As sub-interface stand for Binary-Data-OrderActions to support dummy object factory when running simulation
 *
 * @author Mark
 *
 */
public interface BDOrderActionsView extends QuoteOrderView, BufferedMarketData, SimulationEvent {

}
