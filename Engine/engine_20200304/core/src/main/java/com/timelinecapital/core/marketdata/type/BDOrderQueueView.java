package com.timelinecapital.core.marketdata.type;

import com.timelinecapital.strategy.types.OrderQueueView;

/**
 * As sub-interface stand for Binary-Data-OrderQueue to support dummy object factory when running simulation
 *
 * @author Mark
 *
 */
public interface BDOrderQueueView extends OrderQueueView, BufferedMarketData, SimulationEvent {

}
