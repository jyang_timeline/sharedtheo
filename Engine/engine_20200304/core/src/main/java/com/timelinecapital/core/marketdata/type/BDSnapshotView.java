package com.timelinecapital.core.marketdata.type;

import com.timelinecapital.core.mktdata.type.SnapshotView;

public interface BDSnapshotView extends SnapshotView<BDBookView, BDTickView>, BufferedMarketData, SimulationEvent {

}
