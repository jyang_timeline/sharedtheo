package com.timelinecapital.core.marketdata.type;

import com.nogle.strategy.types.TickView;

/**
 * As sub-interface stand for Binary-Data-Tick to support dummy object factory when running simulation
 *
 * @author Mark
 *
 */
public interface BDTickView extends TickView, BufferedMarketData, SimulationEvent {

}
