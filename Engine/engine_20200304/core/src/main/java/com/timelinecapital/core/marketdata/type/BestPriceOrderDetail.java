package com.timelinecapital.core.marketdata.type;

import java.nio.ByteBuffer;
import java.util.stream.IntStream;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.BestPriceOrderDetailParser;
import com.timelinecapital.commons.type.DataType;

public final class BestPriceOrderDetail extends CNExchangeTimeAdaptor implements BDBestPriceOrderQueueView {

    private final Contract contract;
    private final Exchange exchange;
    private final BestPriceOrderDetailParser parser;

    private final long[] bidOrders = new long[10];
    private final long[] askOrders = new long[10];

    private long updateTimeMillis;
    private BestPriceOrderDetail last;
    private boolean active;

    public BestPriceOrderDetail(final Contract contract, final BestPriceOrderDetailParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
        this.parser = parser;
        this.parser.setSymbol(contract.getSymbol());
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    /**
     * To update order detail at best price data directly from the source
     *
     * @param data
     * @param updateTimeMillis
     */
    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;
        clearCache();
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final DataType getDataType() {
        return DataType.BESTORDERDETAIL;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.BestPriceOrderDetail;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final double getBestBidPrice() {
        return parser.getBestBidPrice();
    }

    @Override
    public final long getBestBidOrderQty(final int index) {
        return parser.getBestBidOrderQty(index);
    }

    @Override
    public final long[] getBestBidOrderQty() {
        IntStream.range(0, 10).forEachOrdered(i -> bidOrders[i] = parser.getBestBidOrderQty(i));
        return bidOrders;
    }

    @Override
    public final double getBestAskPrice() {
        return parser.getBestAskPrice();
    }

    @Override
    public final long getBestAskOrderQty(final int index) {
        return parser.getBestAskOrderQty(index);
    }

    @Override
    public final long[] getBestAskOrderQty() {
        IntStream.range(0, 10).forEachOrdered(i -> askOrders[i] = parser.getBestAskOrderQty(i));
        return askOrders;
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

    @Override
    public final void setPrevious(final Quote obj) {
        last = (BestPriceOrderDetail) obj;
    }

    @Override
    public final BestPriceOrderDetail getPrevious() {
        return last;
    }

}
