package com.timelinecapital.core.marketdata.type;

import java.nio.ByteBuffer;

public interface BufferedMarketData {

    void setData(ByteBuffer data, long updateTimeMillis);

}
