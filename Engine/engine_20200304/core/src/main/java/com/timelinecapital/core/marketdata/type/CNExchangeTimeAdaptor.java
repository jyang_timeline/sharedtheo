package com.timelinecapital.core.marketdata.type;

import java.util.concurrent.atomic.AtomicBoolean;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.parser.HeaderParser;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public class CNExchangeTimeAdaptor {

    private final AtomicBoolean cached = new AtomicBoolean(false);
    private final HeaderParser parser;
    private final TimeWrapper timeWrapper;

    private long sourceTimeMicros;

    protected CNExchangeTimeAdaptor(final HeaderParser parser, final Contract contract) {
        this.parser = parser;
        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);
    }

    final void clearCache() {
        cached.set(false);
    }

    public final long getExchangeUpdateTimeMicros() {
        if (cached.get()) {
            return sourceTimeMicros;
        }
        sourceTimeMicros = timeWrapper.get(parser.getSourceMicroTime());
        cached.set(true);
        return sourceTimeMicros;
    }

    public final long getExchangeUpdateTimeMillis() {
        if (cached.get()) {
            return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
        }
        sourceTimeMicros = timeWrapper.get(parser.getSourceMicroTime());
        cached.set(true);
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    public final long getExchangeDisplayTimeMicros() {
        return parser.getSourceMicroTime();
    }

}
