package com.timelinecapital.core.marketdata.type;

import static com.nogle.core.Constants.AT;
import static com.nogle.core.Constants.COMMA;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.UpdateFlag;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.parser.UpdateFlagParser;

public final class MarketBook extends CNExchangeTimeAdaptor implements BDBookView {

    private final StringBuilder sb = new StringBuilder();

    private final Contract contract;
    private final MarketBookParser parser;
    private final ContentType contentType;
    private final Exchange exchange;

    private long updateTimeMillis;
    private long openInterest;
    private long volume;

    private MarketBook last;
    private boolean active;

    public MarketBook(final Contract contract, final MarketBookParser parser, final ContentType contentType) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
        this.parser = parser;
        this.parser.setSymbol(contract.getSymbol());
        this.contentType = contentType;
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    /**
     * To update MarketBook raw data directly from the source
     *
     * @param data
     * @param updateTimeMillis
     */
    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;
        clearCache();
        volume = parser.getVolume();
        openInterest = parser.getOpenInterest();
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    public final void setUpdateTime(final long updateTimeMillis) {
        this.updateTimeMillis = updateTimeMillis;
    }

    @Override
    public final void setOpenInterest(final long openInterest) {
        this.openInterest = openInterest;
    }

    @Override
    public final void setVolume(final long volume) {
        this.volume = volume;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final DataType getDataType() {
        // Run-time data type due to self-defined data
        return DataType.decode(parser.getType());
    }

    @Override
    public final ContentType getContentType() {
        return contentType;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final UpdateFlag getClosestBookUpdate() {
        return UpdateFlagParser.fromInt(parser.getUpdateFlag());
    }

    @Override
    public final QuoteType getQuoteType() {
        return QuoteType.decode(parser.getQuoteType());
    }

    @Override
    public final int getBidDepth() {
        return parser.getBidDepth();
    }

    @Override
    public final int getAskDepth() {
        return parser.getAskDepth();
    }

    @Override
    public final long getBidQty(final int index) {
        return parser.getBidQty(index);
    }

    @Override
    public final long getBidQty() {
        return getBidQty(0);
    }

    @Override
    public final double getBidPrice(final int index) {
        return parser.getBidPrice(index);
    }

    @Override
    public final double getBidPrice() {
        return getBidPrice(0);
    }

    @Override
    public final long getAskQty(final int index) {
        return parser.getAskQty(index);
    }

    @Override
    public final long getAskQty() {
        return getAskQty(0);
    }

    @Override
    public final double getAskPrice(final int index) {
        return parser.getAskPrice(index);
    }

    @Override
    public final double getAskPrice() {
        return getAskPrice(0);
    }

    @Override
    public final long getOpenInterest() {
        return openInterest;
    }

    @Override
    public final long getVolume() {
        return volume;
    }

    @Override
    public final String toString() {
        sb.setLength(0);
        sb.append(contract).append(COMMA);
        sb.append(getUpdateTimeMicros()).append(COMMA);
        sb.append(getSequenceNo()).append(COMMA);
        sb.append(getClosestBookUpdate()).append(COMMA);
        for (int i = 0; i < 5; i++) {
            sb.append(getBidQty(i)).append(AT).append(getBidPrice(i)).append(COMMA);
            sb.append(getAskQty(i)).append(AT).append(getAskPrice(i)).append(COMMA);
        }
        sb.append(getOpenInterest()).append(COMMA);
        sb.append(getVolume());
        return sb.toString();
    }

    @Override
    public final int hashCode() {
        final HashCodeBuilder hashCode = new HashCodeBuilder(17, 31);
        hashCode.append(contract);
        hashCode.append(parser.getBinary());
        hashCode.append(updateTimeMillis);
        return hashCode.toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

    @Override
    public final void setPrevious(final Quote obj) {
        last = (MarketBook) obj;
    }

    @Override
    public final MarketBook getPrevious() {
        return last;
    }

}
