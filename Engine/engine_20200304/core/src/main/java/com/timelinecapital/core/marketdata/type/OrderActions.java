package com.timelinecapital.core.marketdata.type;

import static com.nogle.core.Constants.COMMA;

import java.nio.ByteBuffer;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.OrderActionsParser;
import com.timelinecapital.commons.type.DataType;

public final class OrderActions extends CNExchangeTimeAdaptor implements BDOrderActionsView {

    private final StringBuilder sb = new StringBuilder();

    private final Contract contract;
    private final Exchange exchange;
    private final OrderActionsParser parser;

    private long updateTimeMillis;
    private OrderActions last;
    private boolean active;

    public OrderActions(final Contract contract, final OrderActionsParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
        this.parser = parser;
        this.parser.setSymbol(contract.getSymbol());
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    /**
     * To update order actions raw data directly from the source
     *
     * @param data
     * @param updateTimeMillis
     */
    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;
        clearCache();
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final DataType getDataType() {
        return DataType.ORDERACTIONS;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.RealTimeOrderAction;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final ExecRequestType getExecRequestType() {
        return ExecRequestType.decode(parser.getActionType());
    }

    @Override
    public final long getQuoteOrderId() {
        return parser.getOrderId();
    }

    @Override
    public final Side getSide() {
        return Side.decode(parser.getOrderSide());
    }

    @Override
    public final TimeCondition geTimeCondition() {
        return TimeCondition.decode(parser.getOrderTimeCondition());
    }

    @Override
    public final OrderType getOrderType() {
        return OrderType.decode(parser.getOrderType());
    }

    @Override
    public final double getPrice() {
        return parser.getOrderPrice();
    }

    @Override
    public final long getQuantity() {
        return parser.getOrderQty();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

    @Override
    public final String toString() {
        sb.setLength(0);
        sb.append(contract).append(COMMA);
        sb.append(getUpdateTimeMicros()).append(COMMA);
        sb.append(getSequenceNo()).append(COMMA);
        sb.append(getPrice()).append(COMMA);
        sb.append(getQuantity());
        return sb.toString();
    }

    @Override
    public final void setPrevious(final Quote obj) {
        last = (OrderActions) obj;
    }

    @Override
    public final OrderActions getPrevious() {
        return last;
    }

}
