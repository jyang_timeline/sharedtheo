package com.timelinecapital.core.marketdata.type;

import static com.nogle.core.Constants.COMMA;

import java.nio.ByteBuffer;
import java.util.stream.IntStream;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.OrderQueueParser;
import com.timelinecapital.commons.type.DataType;

public final class OrderQueue extends CNExchangeTimeAdaptor implements BDOrderQueueView {

    private final StringBuilder sb = new StringBuilder();

    private final Contract contract;
    private final Exchange exchange;
    private final OrderQueueParser parser;

    private final int[] orders;

    private long updateTimeMillis;
    private OrderQueue last;
    private boolean active;

    public OrderQueue(final Contract contract, final OrderQueueParser parser) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
        this.parser = parser;
        this.parser.setSymbol(contract.getSymbol());
        orders = new int[parser.getMaxLength()];
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    /**
     * To update order queue raw data directly from the source
     *
     * @param data
     * @param updateTimeMillis
     */
    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;
        clearCache();
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final DataType getDataType() {
        return DataType.ORDERQUEUE;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.RealTimeOrderAction;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final Side getSide() {
        return Side.decode(parser.getSide());
    }

    @Override
    public final double getPrice() {
        return parser.getPrice();
    }

    @Override
    public final int getQueueSize() {
        return parser.getCount();
    }

    @Override
    public final int getOrderQueueQty(final int idx) {
        return parser.getOrderQty(idx);
    }

    @Override
    public final int[] getOrderQueues() {
        IntStream.range(0, parser.getCount()).forEachOrdered(i -> orders[i] = parser.getOrderQty(i));
        return orders;
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

    @Override
    public final String toString() {
        sb.setLength(0);
        sb.append(contract).append(COMMA);
        sb.append(getUpdateTimeMicros()).append(COMMA);
        sb.append(getSequenceNo()).append(COMMA);
        sb.append(getSide()).append(COMMA);
        sb.append(getPrice()).append(COMMA);
        sb.append(getQueueSize());
        return sb.toString();
    }

    @Override
    public final void setPrevious(final Quote obj) {
        last = (OrderQueue) obj;
    }

    @Override
    public final OrderQueue getPrevious() {
        return last;
    }

}
