package com.timelinecapital.core.marketdata.type;

import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.strategy.types.MarketData;

public interface Quote extends MarketData, Comparable<Quote> {

    ContentType getContentType();

    Exchange getExchange();

    String getKey();

}
