package com.timelinecapital.core.marketdata.type;

import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.pool.ReverseChainable;

public interface SimulationEvent extends Quote, ReverseChainable<Quote> {

    long getUpdateTimeMillis();

    DataType getDataType();

    void setConsumed();

    boolean isActive();

    @Override
    default void setPrevious(final Quote pvs) {
    }

    @Override
    default Quote getPrevious() {
        return this;
    }

}
