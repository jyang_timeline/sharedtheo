package com.timelinecapital.core.marketdata.type;

import static com.nogle.core.Constants.AT;
import static com.nogle.core.Constants.TAB;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.parser.SnapshotParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.types.UpdateType;
import com.timelinecapital.core.util.ContentTypeHelper;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public class Snapshot implements BDSnapshotView {

    private final StringBuilder sb = new StringBuilder();

    private final Contract contract;
    private final SnapshotParser parser;
    private final ContentType contentType;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private final Tick tick;
    private final MarketBook marketbook;

    private long updateTimeMillis;
    private boolean active;

    public Snapshot(final Contract contract, final SbeVersion codecVersion) {
        this.contract = contract;
        parser = ParserInstanceFactory.getSnptParser(codecVersion);
        parser.setSymbol(contract.getSymbol());
        contentType = ContentTypeHelper.fromFeedPreference(contract.getExchange());
        exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);
        tick = new Tick(contract, parser, contentType);
        marketbook = new MarketBook(contract, parser, contentType);
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;

        // Force cached value reset on SnapshotQuoteView. Performance warning!!
        tick.clearCache();
        marketbook.clearCache();

        tick.setUpdateTime(updateTimeMillis);
        marketbook.setUpdateTime(updateTimeMillis);
        marketbook.setOpenInterest(parser.getOpenInterest());
        marketbook.setVolume(parser.getVolume());
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    public final long getOpenInterest() {
        return parser.getOpenInterest();
    }

    public final long getVolume() {
        return parser.getVolume();
    }

    @Override
    public final UpdateType getUpdateType() {
        return UpdateType.fromInt(parser.getSnapshotUpdateFlag());
    }

    @Override
    public final BDTickView getTick() {
        return tick;
    }

    @Override
    public final BDBookView getMarketBook() {
        return marketbook;
    }

    @Override
    public final String toString() {
        sb.setLength(0);
        sb.append(contract).append(TAB);
        sb.append(parser.getMicroTime()).append(TAB);
        sb.append(getSequenceNo()).append(TAB);
        sb.append(tick.getType()).append(TAB);
        sb.append(tick.getPrice()).append(TAB).append(tick.getQuantity()).append(TAB);

        sb.append(marketbook.getClosestBookUpdate()).append(TAB);
        for (int i = 0; i < 5; i++) {
            sb.append(marketbook.getBidQty(i)).append(AT).append(marketbook.getBidPrice(i)).append(TAB);
            sb.append(marketbook.getAskQty(i)).append(AT).append(marketbook.getAskPrice(i)).append(TAB);
        }
        sb.append(getOpenInterest()).append(TAB);
        sb.append(getVolume());
        return sb.toString();
    }

    @Override
    public final int hashCode() {
        final HashCodeBuilder hashCode = new HashCodeBuilder(17, 31);
        hashCode.append(contract);
        hashCode.append(parser.getData());
        return hashCode.toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public ContentType getContentType() {
        return contentType;
    }

    @Override
    public Exchange getExchange() {
        return exchange;
    }

    @Override
    public String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public long getExchangeUpdateTimeMicros() {
        return timeWrapper.get(parser.getSourceMicroTime());
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return parser.getSourceMicroTime();
    }

    @Override
    public DataType getDataType() {
        return DataType.SNAPSHOT;
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
