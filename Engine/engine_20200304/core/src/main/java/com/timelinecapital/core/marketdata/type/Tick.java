package com.timelinecapital.core.marketdata.type;

import static com.nogle.core.Constants.COMMA;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.marketdata.parser.TradeParser;
import com.timelinecapital.commons.type.DataType;

public final class Tick extends CNExchangeTimeAdaptor implements BDTickView {

    private final StringBuilder sb = new StringBuilder();

    private final Contract contract;
    private final TradeParser parser;
    private final ContentType contentType;
    private final Exchange exchange;

    private long updateTimeMillis;
    private Tick last;
    private boolean active;

    public Tick(final Contract contract, final TradeParser parser, final ContentType contentType) {
        super(parser, contract);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());
        this.parser = parser;
        this.parser.setSymbol(contract.getSymbol());
        this.contentType = contentType;
        updateTimeMillis = TradeClock.getCurrentMillis();
        active = false;
    }

    /**
     * To update tick raw data directly from the source
     *
     * @param data
     * @param updateTimeMillis
     */
    @Override
    public final void setData(final ByteBuffer data, final long updateTimeMillis) {
        parser.setBinary(data);
        active = true;
        this.updateTimeMillis = updateTimeMillis;
        clearCache();
    }

    @Override
    public void setConsumed() {
        active = false;
    }

    @Override
    public final boolean isActive() {
        return active;
    }

    public final void setUpdateTime(final long updateTimeMillis) {
        this.updateTimeMillis = updateTimeMillis;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final DataType getDataType() {
        // Run-time data type due to self-defined data
        return DataType.decode(parser.getType());
    }

    @Override
    public final ContentType getContentType() {
        return contentType;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return parser.getSequenceNo();
    }

    @Override
    public final long getExchangeSequenceNo() {
        return parser.getSourceSequenceNo();
    }

    @Override
    public final long getUpdateTimeMicros() {
        return parser.getMicroTime();
    }

    @Override
    public final long getUpdateTimeMillis() {
        return updateTimeMillis;
    }

    @Override
    public final TickType getType() {
        return TickType.decode(parser.getTradeType());
    }

    @Override
    public final long getQuantity() {
        return parser.getQuantity();
    }

    @Override
    public final double getPrice() {
        return parser.getPrice();
    }

    @Override
    public final long getOpenInterest() {
        return parser.getOpenInterest();
    }

    @Override
    public final long getVolume() {
        return parser.getVolume();
    }

    @Override
    public final String toString() {
        sb.setLength(0);
        sb.append(contract).append(COMMA);
        sb.append(getUpdateTimeMicros()).append(COMMA);
        sb.append(getSequenceNo()).append(COMMA);
        sb.append(getType()).append(COMMA);
        sb.append(getPrice()).append(COMMA).append(getQuantity()).append(COMMA);
        sb.append(getOpenInterest()).append(COMMA);
        sb.append(getVolume());
        return sb.toString();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(contract).append(getPrice()).append(getQuantity()).append(updateTimeMillis).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

    @Override
    public final void setPrevious(final Quote obj) {
        last = (Tick) obj;
    }

    @Override
    public final Tick getPrevious() {
        return last;
    }

}