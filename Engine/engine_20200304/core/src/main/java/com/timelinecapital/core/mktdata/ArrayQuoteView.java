package com.timelinecapital.core.mktdata;

import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.TickView;
import com.nogle.util.RoundRobinWrapper;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.mktdata.type.MDOrderQueueView;
import com.timelinecapital.core.mktdata.type.MarketDataSnapshot;
import com.timelinecapital.core.mktdata.type.VanillaBestPriceOrderDetail;
import com.timelinecapital.core.mktdata.type.VanillaMarketBook;
import com.timelinecapital.core.mktdata.type.VanillaOrderActions;
import com.timelinecapital.core.mktdata.type.VanillaOrderQueue;
import com.timelinecapital.core.mktdata.type.VanillaTick;
import com.timelinecapital.core.util.RingCapacityHelper;
import com.timelinecapital.strategy.types.OrderQueueView;

public final class ArrayQuoteView extends QuoteView {

    private final int capacity;

    private final VanillaMarketBook[] books;
    private final VanillaTick[] ticks;
    private final MarketDataSnapshot[] snapshots;
    private final VanillaBestPriceOrderDetail[] orderDetails;
    private final VanillaOrderActions[] orderActions;
    private final MDOrderQueueView[] orderQueues;

    private final RoundRobinWrapper<VanillaMarketBook> roundRobinBooks;
    private final RoundRobinWrapper<VanillaTick> roundRobinTicks;
    private final RoundRobinWrapper<MarketDataSnapshot> roundRobinSnapshots;
    private final RoundRobinWrapper<VanillaBestPriceOrderDetail> roundRobinOrderDetails;
    private final RoundRobinWrapper<VanillaOrderActions> roundRobinOrderActions;
    private final RoundRobinWrapper<MDOrderQueueView> roundRobinOrderQueues;

    public ArrayQuoteView(final Contract contract, final SbeVersion version) {
        super(contract);
        capacity = RingCapacityHelper.getQuoteEventCapacity();

        books = new VanillaMarketBook[capacity];
        ticks = new VanillaTick[capacity];
        snapshots = new MarketDataSnapshot[capacity];
        orderDetails = new VanillaBestPriceOrderDetail[capacity];
        orderActions = new VanillaOrderActions[capacity];
        orderQueues = new MDOrderQueueView[capacity];

        for (int i = 0; i < capacity; i++) {
            books[i] = new VanillaMarketBook(contract, version, Exchange.valueOf(contract.getExchange()).getMdDepth());
            ticks[i] = new VanillaTick(contract, version);
            snapshots[i] = new MarketDataSnapshot(contract, version, Exchange.valueOf(contract.getExchange()).getMdDepth());
            orderDetails[i] = new VanillaBestPriceOrderDetail(contract, version);
            orderActions[i] = new VanillaOrderActions(contract, version);
            orderQueues[i] = VanillaOrderQueue.getInstance(contract, version);
        }
        roundRobinBooks = new RoundRobinWrapper<>(books);
        roundRobinTicks = new RoundRobinWrapper<>(ticks);
        roundRobinSnapshots = new RoundRobinWrapper<>(snapshots);
        roundRobinOrderDetails = new RoundRobinWrapper<>(orderDetails);
        roundRobinOrderActions = new RoundRobinWrapper<>(orderActions);
        roundRobinOrderQueues = new RoundRobinWrapper<>(orderQueues);
    }

    @Override
    public final BookView getBook() {
        return roundRobinBooks.get();
    }

    @Override
    public final TickView getTick() {
        return roundRobinTicks.get();
    }

    @Override
    public final MarketDataSnapshot getMarketDataSnapshot() {
        return roundRobinSnapshots.get();
    }

    @Override
    public final BestOrderView getBestPriceOrderDetail() {
        return roundRobinOrderDetails.get();
    }

    @Override
    public final QuoteOrderView getOrderAction() {
        return roundRobinOrderActions.get();
    }

    @Override
    public final OrderQueueView getOrderQueue() {
        return roundRobinOrderQueues.get();
    }

    @Override
    public final void updateBook(final long updateTimeMicros, final Packet packet) {
        roundRobinBooks.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void updateTick(final long updateTimeMicros, final Packet packet) {
        roundRobinTicks.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void updateMarketDataSnapshot(final long updateTimeMicros, final Packet packet) {
        roundRobinSnapshots.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void updateBestPriceOrderDetail(final long updateTimeMicros, final Packet packet) {
        roundRobinOrderDetails.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void updateOrderActions(final long updateTimeMicros, final Packet packet) {
        roundRobinOrderActions.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void updateOrderQueue(final long updateTimeMicros, final Packet packet) {
        roundRobinOrderQueues.next().update(packet, updateTimeMicros);
    }

    @Override
    public final void notifySnapshotWatchdog() {
        feedEvent.onQuote(roundRobinSnapshots.get().getQuoteType());
    }

    @Override
    public final void notifyBookWatchdog() {
        feedEvent.onQuote(roundRobinBooks.get().getQuoteType());
    }

    @Override
    public final void release() {
        for (int i = 0; i < capacity; i++) {
            books[i] = null;
            ticks[i] = null;
            snapshots[i] = null;
            orderDetails[i] = null;
            orderActions[i] = null;
            orderQueues[i] = null;
        }
        roundRobinBooks.clear();
        roundRobinTicks.clear();
        roundRobinSnapshots.clear();
        roundRobinOrderDetails.clear();
        roundRobinOrderActions.clear();
        roundRobinOrderQueues.clear();
    }

}
