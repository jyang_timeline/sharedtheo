package com.timelinecapital.core.mktdata;

import com.nogle.core.EngineStatusView;
import com.nogle.core.event.ExchangeEvent;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.mktdata.type.MarketDataSnapshot;
import com.timelinecapital.core.stats.FeedEvent;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.strategy.types.OrderQueueView;

public abstract class QuoteView {

    protected final Contract contract;
    protected final FeedEvent feedEvent;

    private final ExchangeEvent exchangeEvent;

    public QuoteView(final Contract contract) {
        this.contract = contract;
        feedEvent = OverviewFactory.getFeedSBean(contract);
        exchangeEvent = EngineStatusView.getInstance().getExchangeEvent(contract);
    }

    public final Contract getContract() {
        return contract;
    }

    public abstract BookView getBook();

    public abstract TickView getTick();

    public abstract MarketDataSnapshot getMarketDataSnapshot();

    public abstract BestOrderView getBestPriceOrderDetail();

    public abstract QuoteOrderView getOrderAction();

    public abstract OrderQueueView getOrderQueue();

    public abstract void updateBook(long updateTimeMicros, Packet packet);

    public abstract void updateTick(long updateTimeMicros, Packet packet);

    public abstract void updateMarketDataSnapshot(long updateTimeMicros, Packet packet);

    public abstract void updateBestPriceOrderDetail(long updateTimeMicros, Packet packet);

    public abstract void updateOrderActions(long updateTimeMicros, Packet packet);

    public abstract void updateOrderQueue(long updateTimeMicros, Packet packet);

    public abstract void notifyBookWatchdog();

    public abstract void notifySnapshotWatchdog();

    public abstract void release();

    public final void onFeedStatistics(final DataType dataType) {
        exchangeEvent.onMarketData(dataType);
        feedEvent.onMarketData(dataType);
    }

    public final void onFeedSubscribe(final DataType dataType) {
        exchangeEvent.withDataType(dataType);
    }

}
