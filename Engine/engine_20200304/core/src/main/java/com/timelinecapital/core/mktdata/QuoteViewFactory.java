package com.timelinecapital.core.mktdata;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.config.EngineConfig;

public class QuoteViewFactory {
    private static final Logger log = LogManager.getLogger(QuoteViewFactory.class);

    private static final Map<Contract, com.timelinecapital.core.mktdata.QuoteView> quoteViews = new HashMap<>();

    static com.timelinecapital.core.mktdata.QuoteView getQuoteView(final Contract contract) {
        log.debug("[View] {} is using MD: v.{}", contract.getSymbol(), EngineConfig.getSchemaVersion());
        return new ArrayQuoteView(contract, EngineConfig.getSchemaVersion());
    }

    public static com.nogle.core.marketdata.QuoteView getLegacyQuoteView(final Contract contract, final SbeVersion version) {
        log.debug("[LegacyView] {} is using MD: v.{}", contract.getSymbol(), version);
        return new com.nogle.core.marketdata.ArrayQuoteView(contract, version);
    }

    public static com.timelinecapital.core.mktdata.QuoteView fromContract(final Contract contract) {
        com.timelinecapital.core.mktdata.QuoteView quoteView = quoteViews.get(contract);
        if (quoteView == null) {
            quoteView = getQuoteView(contract);
            quoteViews.put(contract, quoteView);
        }
        return quoteView;
    }

}
