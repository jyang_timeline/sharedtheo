package com.timelinecapital.core.mktdata.type;

public class BookEntry {
    private long qty;
    private double price;

    public BookEntry() {
        this.qty = 0l;
        this.price = 0.0d;
    }

    public long getQuantity() {
        return qty;
    }

    public double getPrice() {
        return price;
    }

    public void updateQuantity(final long qty) {
        this.qty = qty;
    }

    public void updatePrice(final double price) {
        this.price = price;
    }

    public void clear() {
        qty = 0l;
        price = 0.0d;
    }
}
