package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.StringUtils;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.marketdata.type.Quote;

public final class IdleOrderQueueInstance implements MDOrderQueueView {

    private static IdleOrderQueueInstance instance = new IdleOrderQueueInstance();

    private static final int[] queues = new int[0];

    public static IdleOrderQueueInstance getInstance() {
        return instance;
    }

    @Override
    public String getKey() {
        return Exchange.UNKNOWN.name();
    }

    @Override
    public Contract getContract() {
        return ContractFactory.getDummyContract(StringUtils.EMPTY, Exchange.UNKNOWN.name());
    }

    @Override
    public ContentType getContentType() {
        return ContentType.RealTimeOrderQueue;
    }

    @Override
    public Exchange getExchange() {
        return Exchange.UNKNOWN;
    }

    @Override
    public long getSequenceNo() {
        return 0;
    }

    @Override
    public long getExchangeSequenceNo() {
        return 0;
    }

    @Override
    public long getUpdateTimeMicros() {
        return 0;
    }

    @Override
    public long getUpdateTimeMillis() {
        return 0;
    }

    @Override
    public long getExchangeUpdateTimeMicros() {
        return 0;
    }

    @Override
    public long getExchangeUpdateTimeMillis() {
        return 0;
    }

    @Override
    public long getExchangeDisplayTimeMicros() {
        return 0;
    }

    @Override
    public Side getSide() {
        return Side.INVALID;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    @Override
    public int getQueueSize() {
        return 0;
    }

    @Override
    public int getOrderQueueQty(final int arg0) {
        return 0;
    }

    @Override
    public int[] getOrderQueues() {
        return queues;
    }

    @Override
    public int compareTo(final Quote o) {
        return 0;
    }

    @Override
    public void update(final Packet packet, final long microsecond) {
    }

}
