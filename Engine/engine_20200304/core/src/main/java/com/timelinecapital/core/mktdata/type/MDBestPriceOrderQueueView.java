package com.timelinecapital.core.mktdata.type;

import com.nogle.strategy.types.BestOrderView;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;

public interface MDBestPriceOrderQueueView extends BestOrderView, Quote, Updatable<Packet> {

}
