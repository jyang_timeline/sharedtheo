package com.timelinecapital.core.mktdata.type;

import com.nogle.strategy.types.BookView;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;

public interface MDBookView extends BookView, Quote, Updatable<Packet> {

    void setContentType(ContentType contentType);

    void setVolume(long volume);

    void setOpenInterest(long openInterest);

}
