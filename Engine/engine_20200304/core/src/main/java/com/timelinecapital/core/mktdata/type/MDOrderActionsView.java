package com.timelinecapital.core.mktdata.type;

import com.nogle.strategy.types.QuoteOrderView;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;

public interface MDOrderActionsView extends QuoteOrderView, Quote, Updatable<Packet> {

}
