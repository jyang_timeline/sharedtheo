package com.timelinecapital.core.mktdata.type;

import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.strategy.types.OrderQueueView;

public interface MDOrderQueueView extends OrderQueueView, Quote, Updatable<Packet> {

}
