package com.timelinecapital.core.mktdata.type;

import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;

public interface MDSnapshotView extends SnapshotView<MDBookView, MDTickView>, Quote, Updatable<Packet> {

}
