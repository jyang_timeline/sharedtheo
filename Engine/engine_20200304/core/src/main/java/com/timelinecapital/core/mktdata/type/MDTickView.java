package com.timelinecapital.core.mktdata.type;

import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.marketdata.type.Quote;

public interface MDTickView extends TickView, Quote, Updatable<Packet> {

    void setContentType(ContentType contentType);

}
