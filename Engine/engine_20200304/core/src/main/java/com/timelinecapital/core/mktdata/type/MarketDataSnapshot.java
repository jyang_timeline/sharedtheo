package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.impl.SnapshotProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.types.UpdateType;
import com.timelinecapital.core.util.ContentTypeHelper;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public class MarketDataSnapshot implements MDSnapshotView {

    private final SnapshotProtocol protocol;
    private final Contract contract;
    private final Exchange exchange;

    private final TimeWrapper timeWrapper;

    private long internalTimeMicros;
    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;
    private long displayTimeMicros;

    private int updateType;
    private QuoteType quoteType;
    private ContentType contentType;

    private final MDTickView tickView;
    private final MDBookView bookView;

    public MarketDataSnapshot(final Contract contract, final SbeVersion version, final int depth) {
        this.protocol = SchemaFactory.getSnapshotProtocol(version);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        tickView = new VanillaTick(contract, version, protocol.tradeOffset());
        bookView = new VanillaMarketBook(contract, version, depth, protocol.marketbookOffset());
        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);

        updateType = packet.buffer().position(protocol.snapshotUpdateFlagOffset()).getInt32();

        tickView.update(packet, microsecond);
        bookView.update(packet, microsecond);
        contentType = ContentTypeHelper.fromDepth(bookView.getBidDepth(), bookView.getAskDepth());
        tickView.setContentType(contentType);
        bookView.setContentType(contentType);
        bookView.setVolume(tickView.getVolume());
        bookView.setOpenInterest(tickView.getOpenInterest());
        quoteType = bookView.getQuoteType();

        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final UpdateType getUpdateType() {
        return UpdateType.fromInt(updateType);
    }

    public final QuoteType getQuoteType() {
        return quoteType;
    }

    @Override
    public final ContentType getContentType() {
        return contentType;
    }

    @Override
    public final MDTickView getTick() {
        return tickView;
    }

    @Override
    public final MDBookView getMarketBook() {
        return bookView;
    }

    @Override
    public final int hashCode() {
        final HashCodeBuilder hashCode = new HashCodeBuilder(17, 31);
        hashCode.append(contract);
        hashCode.append(updateTimeMicros);
        return hashCode.toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
