package com.timelinecapital.core.mktdata.type;

import com.timelinecapital.core.types.UpdateType;

public interface SnapshotView<M, T> {

    UpdateType getUpdateType();

    M getMarketBook();

    T getTick();

}
