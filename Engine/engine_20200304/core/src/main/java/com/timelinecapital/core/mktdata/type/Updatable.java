package com.timelinecapital.core.mktdata.type;

import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.core.util.TimeWrapper;

public interface Updatable<T extends Packet> {

    static long toEpochTime(final TimeWrapper wrapper, final long exchangeTimeMicros) {
        return wrapper.get(exchangeTimeMicros);
    }

    void update(T packet, long microsecond);

}
