package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.BestPriceOrderQueueProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public final class VanillaBestPriceOrderDetail implements MDBestPriceOrderQueueView {

    private final BestPriceOrderQueueProtocol protocol;

    private final Contract contract;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private long recvTimeMicros;
    private long internalTimeMicros;

    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;

    private long displayTimeMicros;

    private double bidPrice;
    private final long[] bidOrders = new long[10];
    private double askPrice;
    private final long[] askOrders = new long[10];

    public VanillaBestPriceOrderDetail(final Contract contract, final SbeVersion version) {
        this(contract, version, 0);
    }

    public VanillaBestPriceOrderDetail(final Contract contract, final SbeVersion version, final int offset) {
        this.protocol = SchemaFactory.getBestPriceOrderDetailProtocol(version);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);
        // sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        bidPrice = packet.buffer().position(protocol.bidPriceOffset()).getDouble();
        askPrice = packet.buffer().position(protocol.askPriceOffset()).getDouble();
        for (int i = 0; i < 10; i++) {
            bidOrders[i] = packet.buffer().position(protocol.bidQtyOffset(i)).getInt64();
            askOrders[i] = packet.buffer().position(protocol.askQtyOffset(i)).getInt64();
        }
        recvTimeMicros = packet.getRecvTime();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    final void updateWithCustomization(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = microsecond;
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        bidPrice = packet.buffer().position(protocol.bidPriceOffset()).getDouble();
        askPrice = packet.buffer().position(protocol.askPriceOffset()).getDouble();
        for (int i = 0; i < 10; i++) {
            bidOrders[i] = packet.buffer().position(protocol.bidQtyOffset(i)).getInt64();
            askOrders[i] = packet.buffer().position(protocol.askQtyOffset(i)).getInt64();
        }
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.BestPriceOrderDetail;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    @Override
    public final long getRecvTimeMicros() {
        return recvTimeMicros;
    }

    @Override
    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(updateTimeMicros);
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final double getBestBidPrice() {
        return bidPrice;
    }

    @Override
    public final long getBestBidOrderQty(final int index) {
        return bidOrders[index];
    }

    @Override
    public final long[] getBestBidOrderQty() {
        return bidOrders;
    }

    @Override
    public final double getBestAskPrice() {
        return askPrice;
    }

    @Override
    public final long getBestAskOrderQty(final int index) {
        return askOrders[index];
    }

    @Override
    public final long[] getBestAskOrderQty() {
        return askOrders;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(contract).append(getBestBidPrice()).append(getBestAskPrice()).append(updateTimeMicros).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
