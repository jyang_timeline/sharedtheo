package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.UpdateFlag;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.MarketBookProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.parser.UpdateFlagParser;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public final class VanillaMarketBook implements MDBookView {

    private final MarketBookProtocol protocol;

    // private DataType dataType = DataType.MARKETBOOK;
    private ContentType contentType = ContentType.All_Quote;
    private final Contract contract;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private long recvTimeMicros;
    private long internalTimeMicros;

    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;

    private long displayTimeMicros;

    private int updateFlag;
    private byte quoteType;
    private int bidDepth;
    private int askDepth;
    private final BookEntry[] bidLevels;
    private final BookEntry[] askLevels;

    // additional data for compatibility
    private long volume;
    private long openInterest;

    public VanillaMarketBook(final Contract contract, final SbeVersion version, final int depth) {
        this(contract, version, depth, 0);
    }

    public VanillaMarketBook(final Contract contract, final SbeVersion version, final int depth, final int offset) {
        this.protocol = SchemaFactory.getMarketBookSchema(version, offset);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        bidLevels = new BookEntry[depth];
        askLevels = new BookEntry[depth];
        for (int i = 0; i < depth; i++) {
            bidLevels[i] = new BookEntry();
            askLevels[i] = new BookEntry();
        }
        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        // dataType = DataType.decode(packet.buffer().position(protocol.typeOffset()).getInt8());
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);

        updateFlag = packet.buffer().position(protocol.updateFlagOffset()).getInt32();
        quoteType = packet.buffer().position(protocol.quoteTypeOffset()).getInt8();
        bidDepth = packet.buffer().position(protocol.bidDepthOffset()).getInt32();
        askDepth = packet.buffer().position(protocol.askDepthOffset()).getInt32();
        for (int i = 0; i < bidDepth; i++) {
            bidLevels[i].updateQuantity(packet.buffer().position(protocol.bidQtyOffset(i)).getInt64());
            bidLevels[i].updatePrice(packet.buffer().position(protocol.bidPriceOffset(i)).getDouble());
        }
        for (int i = 0; i < askDepth; i++) {
            askLevels[i].updateQuantity(packet.buffer().position(protocol.askQtyOffset(i, bidDepth)).getInt64());
            askLevels[i].updatePrice(packet.buffer().position(protocol.askPriceOffset(i, bidDepth)).getDouble());
        }

        recvTimeMicros = packet.getRecvTime();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    public final void updateWithCustomization(final Packet packet, final long microsecond) {
        // dataType = DataType.decode(packet.buffer().position(protocol.typeOffset()).getInt8());
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = microsecond;
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        updateFlag = packet.buffer().position(protocol.updateFlagOffset()).getInt32();
        quoteType = packet.buffer().position(protocol.quoteTypeOffset()).getInt8();
        bidDepth = packet.buffer().position(protocol.bidDepthOffset()).getInt32();
        askDepth = packet.buffer().position(protocol.askDepthOffset()).getInt32();
        for (int i = 0; i < bidDepth; i++) {
            bidLevels[i].updateQuantity(packet.buffer().position(protocol.bidQtyOffset(i)).getInt64());
            bidLevels[i].updatePrice(packet.buffer().position(protocol.bidPriceOffset(i)).getDouble());
        }
        for (int i = 0; i < askDepth; i++) {
            askLevels[i].updateQuantity(packet.buffer().position(protocol.askQtyOffset(i, bidDepth)).getInt64());
            askLevels[i].updatePrice(packet.buffer().position(protocol.askPriceOffset(i, bidDepth)).getDouble());
        }

        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void setOpenInterest(final long openInterest) {
        this.openInterest = openInterest;
    }

    @Override
    public final void setVolume(final long volume) {
        this.volume = volume;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final void setContentType(final ContentType contentType) {
        this.contentType = contentType;
    }

    @Override
    public final ContentType getContentType() {
        return contentType;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    @Override
    public final long getRecvTimeMicros() {
        return recvTimeMicros;
    }

    @Override
    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(updateTimeMicros);
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final UpdateFlag getClosestBookUpdate() {
        return UpdateFlagParser.fromInt(updateFlag);
    }

    @Override
    public final QuoteType getQuoteType() {
        return QuoteType.decode(quoteType);
    }

    @Override
    public final int getBidDepth() {
        return bidDepth;
    }

    @Override
    public final int getAskDepth() {
        return askDepth;
    }

    @Override
    public final long getBidQty(final int index) {
        return bidLevels[index].getQuantity();
    }

    @Override
    public final long getBidQty() {
        return getBidQty(0);
    }

    @Override
    public final double getBidPrice(final int index) {
        return bidLevels[index].getPrice();
    }

    @Override
    public final double getBidPrice() {
        return getBidPrice(0);
    }

    @Override
    public final long getAskQty(final int index) {
        return askLevels[index].getQuantity();
    }

    @Override
    public final long getAskQty() {
        return getAskQty(0);
    }

    @Override
    public final double getAskPrice(final int index) {
        return askLevels[index].getPrice();
    }

    @Override
    public final double getAskPrice() {
        return getAskPrice(0);
    }

    @Override
    public final long getOpenInterest() {
        return openInterest;
    }

    @Override
    public final long getVolume() {
        return volume;
    }

    @Override
    public final int hashCode() {
        final HashCodeBuilder hashCode = new HashCodeBuilder(17, 31);
        hashCode.append(contract);
        hashCode.append(updateTimeMicros);
        return hashCode.toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
