package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.OrderActionProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public final class VanillaOrderActions implements MDOrderActionsView {

    private final OrderActionProtocol protocol;

    private final Contract contract;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private long recvTimeMicros;
    private long internalTimeMicros;

    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;
    private long displayTimeMicros;

    private ExecRequestType requestType;
    private long orderId;
    private Side orderSide;
    private TimeCondition orderTif;
    private OrderType orderType;
    private double orderPrice;
    private long orderQty;

    public VanillaOrderActions(final Contract contract, final SbeVersion version) {
        this(contract, version, 0);
    }

    VanillaOrderActions(final Contract contract, final SbeVersion version, final int offset) {
        this.protocol = SchemaFactory.getReatTimeOrderActionsProtocol(version);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);

        requestType = ExecRequestType.decode(packet.buffer().position(protocol.actionOffset()).getInt8());
        orderId = packet.buffer().position(protocol.orderIdOffset()).getInt64();
        orderSide = Side.decode(packet.buffer().position(protocol.orderSideOffset()).getInt8());
        orderTif = TimeCondition.decode(packet.buffer().position(protocol.orderTimeConditionOffset()).getInt8());
        orderType = OrderType.decode(packet.buffer().position(protocol.orderTypeOffset()).getInt8());
        orderPrice = packet.buffer().position(protocol.orderPriceOffset()).getDouble();
        orderQty = packet.buffer().position(protocol.orderQtyOffset()).getInt64();

        recvTimeMicros = packet.getRecvTime();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    final void updateWithCustomization(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = microsecond;
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        requestType = ExecRequestType.decode(packet.buffer().position(protocol.actionOffset()).getInt8());
        orderId = packet.buffer().position(protocol.orderIdOffset()).getInt64();
        orderSide = Side.decode(packet.buffer().position(protocol.orderSideOffset()).getInt8());
        orderTif = TimeCondition.decode(packet.buffer().position(protocol.orderTimeConditionOffset()).getInt8());
        orderType = OrderType.decode(packet.buffer().position(protocol.orderTypeOffset()).getInt8());
        orderPrice = packet.buffer().position(protocol.orderPriceOffset()).getDouble();
        orderQty = packet.buffer().position(protocol.orderQtyOffset()).getInt64();

        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.RealTimeOrderAction;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    @Override
    public final long getRecvTimeMicros() {
        return recvTimeMicros;
    }

    @Override
    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(updateTimeMicros);
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final ExecRequestType getExecRequestType() {
        return requestType;
    }

    @Override
    public final long getQuoteOrderId() {
        return orderId;
    }

    @Override
    public final OrderType getOrderType() {
        return orderType;
    }

    @Override
    public final Side getSide() {
        return orderSide;
    }

    @Override
    public final TimeCondition geTimeCondition() {
        return orderTif;
    }

    @Override
    public final double getPrice() {
        return orderPrice;
    }

    @Override
    public final long getQuantity() {
        return orderQty;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(contract).append(getQuoteOrderId()).append(updateTimeMicros).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return 0;
    }

}
