package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.BestPriceOrderQueueBySideProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public final class VanillaOrderQueue implements MDOrderQueueView {

    private final BestPriceOrderQueueBySideProtocol protocol;

    private final Contract contract;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private long recvTimeMicros;
    private long internalTimeMicros;

    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;

    private long displayTimeMicros;

    private Side side;
    private double price;
    private short count;
    private final int[] ordersQty;

    public static MDOrderQueueView getInstance(final Contract contract, final SbeVersion version) {
        switch (version) {
            case PROT_DYNAMICLENGTH_BOOK:
                return new VanillaOrderQueue(contract, version);
            default:
                return IdleOrderQueueInstance.getInstance();
        }
    }

    public VanillaOrderQueue(final Contract contract, final SbeVersion version) {
        this(contract, version, 0);
    }

    VanillaOrderQueue(final Contract contract, final SbeVersion version, final int offset) {
        this.protocol = SchemaFactory.getBestPriceOrderQueueProtocol(version);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
        ordersQty = new int[EngineConfig.getOrderQueueDepth()];
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);

        side = Side.decode(packet.buffer().position(protocol.sideOffset()).getInt8());
        price = packet.buffer().position(protocol.priceOffset()).getDouble();
        count = packet.buffer().position(protocol.countOffset()).getInt16();

        for (int i = 0; i < getQueueLength(count); i++) {
            ordersQty[i] = packet.buffer().position(protocol.orderQueueOffset(i)).getInt32();
        }

        recvTimeMicros = packet.getRecvTime();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    final void updateWithCustomization(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = microsecond;
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        side = Side.decode(packet.buffer().position(protocol.sideOffset()).getInt8());
        price = packet.buffer().position(protocol.priceOffset()).getDouble();
        count = packet.buffer().position(protocol.countOffset()).getInt16();

        for (int i = 0; i < getQueueLength(count); i++) {
            ordersQty[i] = packet.buffer().position(protocol.orderQueueOffset(i)).getInt32();
        }

        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    private int getQueueLength(final int floatingLength) {
        return floatingLength < BestPriceOrderQueueBySideProtocol.MAX_LENGTH ? floatingLength : BestPriceOrderQueueBySideProtocol.MAX_LENGTH;
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final ContentType getContentType() {
        return ContentType.RealTimeOrderQueue;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    @Override
    public final long getRecvTimeMicros() {
        return recvTimeMicros;
    }

    @Override
    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(updateTimeMicros);
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final Side getSide() {
        return side;
    }

    @Override
    public final double getPrice() {
        return price;
    }

    @Override
    public final int getQueueSize() {
        return count;
    }

    @Override
    public final int getOrderQueueQty(final int index) {
        if (index >= count) {
            return -1;
        }
        return ordersQty[index];
    }

    @Override
    public final int[] getOrderQueues() {
        return ordersQty;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(contract).append(getPrice()).append(getQueueSize()).append(updateTimeMicros).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
