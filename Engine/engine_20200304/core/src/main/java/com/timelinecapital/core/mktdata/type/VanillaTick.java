package com.timelinecapital.core.mktdata.type;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.Packet;
import com.timelinecapital.commons.marketdata.protocol.TradeProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.marketdata.type.Quote;
import com.timelinecapital.core.parser.SchemaFactory;
import com.timelinecapital.core.util.TimeAdapterFactory;
import com.timelinecapital.core.util.TimeWrapper;

public final class VanillaTick implements MDTickView {

    private final TradeProtocol protocol;

    // private DataType dataType = DataType.TICK;
    private ContentType contentType = ContentType.All_Quote;
    private final Contract contract;
    private final Exchange exchange;
    private final TimeWrapper timeWrapper;

    private long recvTimeMicros;
    private long internalTimeMicros;

    private long sequence;
    private long updateTimeMicros;

    private long sourceSequence;
    private long sourceTimeMicros;

    private long displayTimeMicros;

    private byte tradeType;
    private long qty;
    private double price;

    private long volume;
    private long openInterest;

    public VanillaTick(final Contract contract, final SbeVersion version) {
        this(contract, version, 0);
    }

    public VanillaTick(final Contract contract, final SbeVersion version, final int offset) {
        this.protocol = SchemaFactory.getTradeSchema(version, offset);
        this.contract = contract;
        this.exchange = Exchange.valueOf(contract.getExchange());

        timeWrapper = TimeAdapterFactory.getTimeWrapper(TradeClock.getCalendarMillis(), contract);

        updateTimeMicros = TradeClock.getCurrentMicros();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final void update(final Packet packet, final long microsecond) {
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = packet.buffer().position(protocol.timestampOffset()).getInt64();
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        displayTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();
        sourceTimeMicros = Updatable.toEpochTime(timeWrapper, displayTimeMicros);

        tradeType = packet.buffer().position(protocol.tradeTypeOffset()).getInt8();
        qty = packet.buffer().position(protocol.tradeQtyOffset()).getInt64();
        price = packet.buffer().position(protocol.tradePriceOffset()).getDouble();
        volume = packet.buffer().position(protocol.volumeOffset()).getInt64();
        openInterest = packet.buffer().position(protocol.openInterestOffset()).getInt64();

        recvTimeMicros = packet.getRecvTime();
        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    public final void updateWithCustomization(final Packet packet, final long microsecond) {
        // dataType = DataType.decode(packet.buffer().position(protocol.typeOffset()).getInt8());
        sequence = packet.buffer().position(protocol.sequenceNoOffset()).getInt64();
        updateTimeMicros = microsecond;
        sourceSequence = packet.buffer().position(protocol.sourceSequenceNoOffset()).getInt64();
        sourceTimeMicros = packet.buffer().position(protocol.sourceTimestampOffset()).getInt64();

        tradeType = packet.buffer().position(protocol.tradeTypeOffset()).getInt8();
        qty = packet.buffer().position(protocol.tradeQtyOffset()).getInt64();
        price = packet.buffer().position(protocol.tradePriceOffset()).getDouble();
        volume = packet.buffer().position(protocol.volumeOffset()).getInt64();
        openInterest = packet.buffer().position(protocol.openInterestOffset()).getInt64();

        internalTimeMicros = TradeClock.getCurrentMicros();
    }

    @Override
    public final String getKey() {
        return contract.getSymbol();
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final void setContentType(final ContentType contentType) {
        this.contentType = contentType;
    }

    @Override
    public final ContentType getContentType() {
        return contentType;
    }

    @Override
    public final Exchange getExchange() {
        return exchange;
    }

    @Override
    public final long getSequenceNo() {
        return sequence;
    }

    @Override
    public final long getExchangeSequenceNo() {
        return sourceSequence;
    }

    @Override
    public final long getUpdateTimeMicros() {
        return updateTimeMicros;
    }

    @Override
    public final long getRecvTimeMicros() {
        return recvTimeMicros;
    }

    @Override
    public final long getInternalTimeMicros() {
        return internalTimeMicros;
    }

    @Override
    public final long getUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(updateTimeMicros);
    }

    @Override
    public final long getExchangeUpdateTimeMicros() {
        return sourceTimeMicros;
    }

    @Override
    public final long getExchangeUpdateTimeMillis() {
        return NogleTimeUnit.MICROSECONDS.toMillis(sourceTimeMicros);
    }

    @Override
    public final long getExchangeDisplayTimeMicros() {
        return displayTimeMicros;
    }

    @Override
    public final TickType getType() {
        return TickType.decode(tradeType);
    }

    @Override
    public final long getQuantity() {
        return qty;
    }

    @Override
    public final double getPrice() {
        return price;
    }

    @Override
    public final long getOpenInterest() {
        return openInterest;
    }

    @Override
    public final long getVolume() {
        return volume;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(contract).append(getPrice()).append(getQuantity()).append(updateTimeMicros).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public final int compareTo(final Quote o) {
        final int result = Long.compare(getUpdateTimeMicros(), o.getUpdateTimeMicros());
        if (result == 0) {
            return Long.compare(getSequenceNo(), o.getSequenceNo());
        }
        return result;
    }

}
