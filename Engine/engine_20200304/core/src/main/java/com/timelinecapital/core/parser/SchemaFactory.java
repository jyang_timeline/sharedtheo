package com.timelinecapital.core.parser;

import com.nogle.core.exception.UnsupportedBinaryVersion;
import com.timelinecapital.commons.marketdata.protocol.BestPriceOrderQueueBySideProtocol;
import com.timelinecapital.commons.marketdata.protocol.BestPriceOrderQueueProtocol;
import com.timelinecapital.commons.marketdata.protocol.HeaderProtocol;
import com.timelinecapital.commons.marketdata.protocol.MarketBookProtocol;
import com.timelinecapital.commons.marketdata.protocol.OrderActionProtocol;
import com.timelinecapital.commons.marketdata.protocol.TradeProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.BestPriceOrderDetailProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.DynamicDepthMarketBookProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.ExchangeInfoHeaderProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.MultipleDepthMarketBookProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.OrderQueueProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.RealTimeOrderActionsProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.SnapshotProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.TradeEntryProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.TradeSummaryProtocol;
import com.timelinecapital.commons.marketdata.protocol.impl.VanillaHeaderProtocol;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class SchemaFactory {

    public static MarketBookProtocol getMarketBookSchema(final SbeVersion version, final int offset) {
        HeaderProtocol header;
        switch (version) {
            case PROT_BASIC:
                header = new VanillaHeaderProtocol(version);
                break;
            case PROT_WITH_EXGTIME:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                return new DynamicDepthMarketBookProtocol(offset, header);
            default:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
        }
        return new MultipleDepthMarketBookProtocol(offset, header);
    }

    public static TradeProtocol getTradeSchema(final SbeVersion version, final int offset) {
        HeaderProtocol header;
        switch (version) {
            case PROT_BASIC:
                header = new VanillaHeaderProtocol(version);
                break;
            case PROT_WITH_EXGTIME:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                return new TradeEntryProtocol(offset, header);
            default:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
        }
        return new TradeSummaryProtocol(offset, header);
    }

    public static BestPriceOrderQueueProtocol getBestPriceOrderDetailProtocol(final SbeVersion version) {
        HeaderProtocol header;
        switch (version) {
            case PROT_BASIC:
                header = new VanillaHeaderProtocol(version);
                break;
            case PROT_WITH_EXGTIME:
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
            default:
                throw new UnsupportedBinaryVersion("Unsupported version in BestOrderDetail: " + version);
        }
        return new BestPriceOrderDetailProtocol(0, header);
    }

    public static OrderActionProtocol getReatTimeOrderActionsProtocol(final SbeVersion version) {
        HeaderProtocol header;
        switch (version) {
            case PROT_BASIC:
                header = new VanillaHeaderProtocol(version);
                break;
            case PROT_WITH_EXGTIME:
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
            default:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
        }
        return new RealTimeOrderActionsProtocol(0, header);
    }

    public static SnapshotProtocol getSnapshotProtocol(final SbeVersion version) {
        HeaderProtocol header;
        int tickLength;
        int bookLength;
        switch (version) {
            case PROT_BASIC:
                header = new VanillaHeaderProtocol(version);
                tickLength = new TradeSummaryProtocol(0, header).getMessageLength();
                bookLength = new MultipleDepthMarketBookProtocol(0, header).getMessageLength();
                break;
            case PROT_WITH_EXGTIME:
                header = new ExchangeInfoHeaderProtocol(version);
                tickLength = new TradeSummaryProtocol(0, header).getMessageLength();
                bookLength = new MultipleDepthMarketBookProtocol(0, header).getMessageLength();
                break;
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                tickLength = new TradeEntryProtocol(0, header).getMessageLength();
                bookLength = new DynamicDepthMarketBookProtocol(0, header).getMessageLength();
                break;
            default:
                throw new UnsupportedBinaryVersion("Unsupported version in Snapshot: " + version);
        }
        // final int tradeLength = new TradeSummaryProtocol(0, header).getMessageLength();
        // final int bookLength = new MultipleDepthMarketBookProtocol(0, header).getMessageLength();
        return new SnapshotProtocol(header, tickLength, bookLength);
    }

    public static BestPriceOrderQueueBySideProtocol getBestPriceOrderQueueProtocol(final SbeVersion version) {
        HeaderProtocol header;
        switch (version) {
            case PROT_DYNAMICLENGTH_BOOK:
                header = new ExchangeInfoHeaderProtocol(version);
                break;
            default:
                throw new UnsupportedBinaryVersion("Unsupported version in OrderQueue: " + version);
        }
        return new OrderQueueProtocol(0, header);
    }

}
