package com.timelinecapital.core.parser;

import com.nogle.strategy.types.UpdateFlag;
import com.timelinecapital.commons.util.BitwiseOperator;

public class UpdateFlagParser {

    // public static final int MASK_TOP = 0x01 ^ 0x02;
    // public static final int MASK_SECOND = 0x04 ^ 0x08;
    // public static final int MASK_THIRD = 0x10 ^ 0x20;
    // public static final int MASK_FOURTH = 0x40 ^ 0x80;
    // public static final int MASK_FIFTH = 0x100 ^ 0x200;

    public static UpdateFlag fromInt(final int updateFlag) {
        final int position = BitwiseOperator.firstNonZeroBit(updateFlag);
        switch (position) {
            case 0:
            case 1:
                return UpdateFlag.TOP;
            case 2:
            case 3:
                return UpdateFlag.SECOND;
            case 4:
            case 5:
                return UpdateFlag.THIRD;
            case 6:
            case 7:
                return UpdateFlag.FOURTH;
            case 8:
            case 9:
                return UpdateFlag.FIFTH;
            default:
                return UpdateFlag.NONE;
        }
    }

}
