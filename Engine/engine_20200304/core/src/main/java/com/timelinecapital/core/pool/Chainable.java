package com.timelinecapital.core.pool;

public interface Chainable<T> {

    public T getNext();

    public void setNext(T nxt);

}
