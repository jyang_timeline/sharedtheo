package com.timelinecapital.core.pool;

public interface PoolFactory<T extends Reusable<T>> {

    public T get();

}
