package com.timelinecapital.core.pool;

public class PoolFactoryImpl<T extends Reusable<T>> implements PoolFactory<T> {

    private final SuperPool<T> superPool;
    private T root;

    PoolFactoryImpl(final SuperPool<T> superPool) {
        this.superPool = superPool;
        allocate();
    }

    @Override
    public T get() {
        if (root == null) {
            allocate();
        }
        final T obj = root;
        root = root.getNext();
        obj.setNext(null);
        return obj;
    }

    private void allocate() {
        root = superPool.getChain();
    }

}