package com.timelinecapital.core.pool;

public interface Recycler<T extends Reusable<T>> {

    /**
     * recycle object
     *
     * @NOTE next pointer must be cleared before invoking recycle or object will NOT be recycled
     * @param obj
     */
    public void recycle(T obj);

}
