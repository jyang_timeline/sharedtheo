package com.timelinecapital.core.pool;

public final class RecyclerImpl<T extends Reusable<T>> implements Recycler<T> {

    private final SuperPool<T> superPool;
    private final T root;

    private final Class<T> poolClass;
    private final int recycleSize;
    private int _count = 0;

    public RecyclerImpl(final Class<T> poolClass, final int recycleSize, final SuperPool<T> superPool) {
        this.poolClass = poolClass;
        this.superPool = superPool;
        this.recycleSize = recycleSize;

        try {
            root = this.poolClass.getDeclaredConstructor().newInstance();
        } catch (final Exception e) {
            throw new RuntimeException("Unable to create recycle root for " + poolClass.getSimpleName() + " : " + e.getMessage(), e);
        }
    }

    @Override
    public void recycle(final T obj) {
        if (obj == null) {
            return;
        }
        if (obj.getNext() == null) {
            obj.reset();

            obj.setNext(root.getNext());
            root.setNext(obj);

            if (++_count == recycleSize) {
                superPool.returnChain(root.getNext());
                root.setNext(null);
                _count = 0;
            }
        }
    }
}
