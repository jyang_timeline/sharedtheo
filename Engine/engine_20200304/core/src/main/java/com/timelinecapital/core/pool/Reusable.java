package com.timelinecapital.core.pool;

public interface Reusable<T> extends Chainable<T> {

    public void reset();

}
