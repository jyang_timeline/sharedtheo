package com.timelinecapital.core.pool;

import com.timelinecapital.core.marketdata.type.Quote;

public interface ReverseChainable<T extends Quote> {

    T getPrevious();

    void setPrevious(T pvs);

}
