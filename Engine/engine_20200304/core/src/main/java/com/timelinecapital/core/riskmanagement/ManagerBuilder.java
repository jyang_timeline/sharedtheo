package com.timelinecapital.core.riskmanagement;

import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.types.RenewableCondition;
import com.nogle.strategy.types.Config;

class ManagerBuilder {

    static <T extends RenewableCondition> T necessaryCondition(final Config config, final T condition) throws ConditionNotFoundException {
        if (condition.checkUpdate(config) == false) {
            throw new ConditionNotFoundException(condition.toString() + "config check failed");
        }
        condition.onConfigChange(config);
        return condition;
    }

    static <T extends RenewableCondition> T optionalCondition(final Config config, final T condition) {
        if (condition.checkUpdate(config) == false) {
            return null;
        }
        condition.onConfigChange(config);
        return condition;
    }

    static <T extends RenewableCondition> T baseCondition(final Config config, final T condition) {
        condition.onConfigChange(config);
        return condition;
    }

}
