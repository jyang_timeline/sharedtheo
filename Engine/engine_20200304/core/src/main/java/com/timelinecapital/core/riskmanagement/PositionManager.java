package com.timelinecapital.core.riskmanagement;

import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.MaxPositionPerSide;
import com.nogle.core.position.condition.types.PermissionForCancelCondition;
import com.nogle.core.position.condition.types.PermissionForModCondition;
import com.nogle.core.position.condition.types.PermissionForNewCondition;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.core.position.condition.types.RenewableCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Side;
import com.nogle.util.ArrayUtils;

public class PositionManager implements Validatable<Config>, Updateable {

    private RenewableCondition[] conditions;
    private QuantityCondition[] qtyConditions;
    private PermissionForNewCondition[] permsNewConditions;
    private PermissionForModCondition[] permsModConditions;
    private PermissionForCancelCondition[] permsCnlConditions;

    private String fullDesc;

    PositionManager() {
        clear();
    }

    void createDesc() {
        fullDesc = "PositionManager{" +
            "Qty=" + qtyConditions.length +
            ", NEW=" + permsNewConditions.length +
            ", CXL=" + permsCnlConditions.length +
            '}';
    }

    void clear() {
        conditions = new RenewableCondition[0];
        qtyConditions = new QuantityCondition[0];
        permsNewConditions = new PermissionForNewCondition[0];
        permsModConditions = new PermissionForModCondition[0];
        permsCnlConditions = new PermissionForCancelCondition[0];
    }

    public void reset() {
        for (final RenewableCondition condition : conditions) {
            condition.reset();
        }
    }

    @Override
    public void acceptThrows(final Config config) throws Exception {
        for (final RenewableCondition condition : conditions) {
            if (condition.checkUpdate(config) == false) {
                throw new ConditionNotFoundException("Error in position condition.");
            }
        }
    }

    @Override
    public void updateConfig(final Config config) {
        for (final RenewableCondition positionCondition : conditions) {
            positionCondition.onConfigChange(config);
        }
    }

    void addQuantityCondition(final QuantityCondition qtyCondition) {
        qtyConditions = com.nogle.util.ArrayUtils.addElement(qtyConditions, qtyCondition);
        conditions = com.nogle.util.ArrayUtils.addElement(conditions, qtyCondition);
    }

    void addPermissionNewCondition(final PermissionForNewCondition newCondition) {
        permsNewConditions = ArrayUtils.addElement(permsNewConditions, newCondition);
        conditions = com.nogle.util.ArrayUtils.addElement(conditions, newCondition);
    }

    void addPermissionModCondition(final PermissionForModCondition modCondition) {
        permsModConditions = ArrayUtils.addElement(permsModConditions, modCondition);
        conditions = com.nogle.util.ArrayUtils.addElement(conditions, modCondition);
    }

    void addPermissionCancelCondition(final PermissionForCancelCondition cnlCondition) {
        permsCnlConditions = ArrayUtils.addElement(permsCnlConditions, cnlCondition);
        conditions = com.nogle.util.ArrayUtils.addElement(conditions, cnlCondition);
    }

    public long adjustQuantity(final Side side, final long quantity, final long currentPosition) {
        long adjustedQuantity = quantity;
        for (final QuantityCondition qtyCondition : qtyConditions) {
            adjustedQuantity = qtyCondition.checkCondition(side, adjustedQuantity, currentPosition);
        }
        return adjustedQuantity;
    }

    public boolean isValidFill(final Side side, final double price, final long quantity) {
        long adjustedQuantity = quantity;
        for (final QuantityCondition qtyCondition : qtyConditions) {
            if (qtyCondition instanceof MaxPositionPerSide) {
                adjustedQuantity = qtyCondition.checkCondition(side, adjustedQuantity, 0);
            }
        }
        return adjustedQuantity >= quantity;
    }

    public boolean canSendNewOrder(final Side side, final double price, final long quantity, final long currentStatus) {
        for (final PermissionForNewCondition permissionCondition : permsNewConditions) {
            if (!permissionCondition.canNewOrder(side, quantity, price, currentStatus)) {
                return false;
            }
        }
        return true;
    }

    public boolean canSendOrderMod(final Side side, final double price, final long quantity, final long timestampInMillis, final long currentStatus) {
        for (final PermissionForModCondition permissionCondition : permsModConditions) {
            if (!permissionCondition.canModifyOrder(timestampInMillis, side, quantity, price, currentStatus)) {
                return false;
            }
        }
        return true;
    }

    public boolean canCancelOrder(final long timestampInMillis, final long currentStatus) {
        for (final PermissionForCancelCondition permissionCondition : permsCnlConditions) {
            if (!permissionCondition.canCancelOrder(timestampInMillis, currentStatus)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return fullDesc;
    }

}
