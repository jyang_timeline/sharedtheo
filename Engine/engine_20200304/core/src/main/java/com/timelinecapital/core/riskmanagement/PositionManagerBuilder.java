package com.timelinecapital.core.riskmanagement;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.MaxOrderQuantity;
import com.nogle.core.position.condition.MaxOrdersPerSide;
import com.nogle.core.position.condition.MaxPositionPerSide;
import com.nogle.core.position.condition.MinOrderAgeBeforeCancel;
import com.nogle.core.position.condition.MinOrderQuantity;
import com.nogle.core.position.condition.SubsequentOrderDelay;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.config.EngineConfig;

public class PositionManagerBuilder extends ManagerBuilder {

    static public PositionManager buildDummyPositionManager() {
        if (EngineConfig.getEngineMode().equals(EngineMode.PRODUCTION)) {
            throw new RuntimeException("Not allow to use STUB class in PRODUCTION mode");
        }
        return new PositionManager();
    }

    static public PositionManager buildPositionManager(final Contract contract, final Config config) throws ConditionNotFoundException {
        // final Contract contract = quoteSnapshot.getContract();
        final PositionManager positionManager = new PositionManager();

        /* Necessary conditions */
        positionManager.addQuantityCondition(necessaryCondition(config, new MaxOrderQuantity(contract)));
        positionManager.addQuantityCondition(necessaryCondition(config, new MaxPositionPerSide(contract)));
        positionManager.addPermissionNewCondition(necessaryCondition(config, new MaxOrdersPerSide(contract)));

        /* Optional conditions */
        positionManager.addQuantityCondition(optionalCondition(config, new MinOrderQuantity(contract)));
        positionManager.addPermissionNewCondition(optionalCondition(config, new SubsequentOrderDelay(contract)));
        positionManager.addPermissionCancelCondition(optionalCondition(config, new MinOrderAgeBeforeCancel(contract)));

        positionManager.createDesc();
        return positionManager;
    }

}
