package com.timelinecapital.core.riskmanagement;

import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.types.PriceCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Side;

public class PriceManager implements Validatable<Config>, Updateable {

    private PriceCondition[] priceConditions;

    String fullDesc;

    PriceManager() {
        clear();
    }

    void createDesc() {
        fullDesc = "PriceManager{" +
            "Price=" + priceConditions.length +
            '}';
    }

    void clear() {
        priceConditions = new PriceCondition[0];
    }

    public void reset() {
        for (final PriceCondition condition : priceConditions) {
            condition.reset();
        }
    }

    @Override
    public void acceptThrows(final Config config) throws Exception {
        for (final PriceCondition condition : priceConditions) {
            if (condition.checkUpdate(config) == false) {
                throw new ConditionNotFoundException("Error in price condition.");
            }
        }
    }

    @Override
    public void updateConfig(final Config config) {
        for (final PriceCondition condition : priceConditions) {
            condition.onConfigChange(config);
        }
    }

    void addPriceCondition(final PriceCondition priceCondition) {
        priceConditions = com.nogle.util.ArrayUtils.addElement(priceConditions, priceCondition);
    }

    public double adjustPrice(final Side side, final double price) {
        double adjustedPrice = price;
        for (final PriceCondition priceCondition : priceConditions) {
            adjustedPrice = priceCondition.checkCondition(side, adjustedPrice);
        }
        return adjustedPrice;
    }

    public void onPriceLimitUpdate() {
        for (final PriceCondition priceCondition : priceConditions) {
            priceCondition.onRangeUpdate();
        }
    }

    @Override
    public String toString() {
        return fullDesc;
    }

}
