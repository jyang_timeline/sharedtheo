package com.timelinecapital.core.riskmanagement;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.MaxCrossPercentage;
import com.nogle.core.position.condition.MaxCrossTicks;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.QuoteSnapshot;
import com.timelinecapital.core.config.EngineConfig;

public class PriceManagerBuilder extends ManagerBuilder {

    static public PriceManager buildDummyPriceManager() {
        if (EngineConfig.getEngineMode().equals(EngineMode.PRODUCTION)) {
            throw new RuntimeException("Not allow to use STUB class in PRODUCTION mode");
        }
        return new PriceManager();
    }

    static public PriceManager buildPriceManager(final RiskManager riskManager, final QuoteSnapshot quoteSnapshot, final Config config)
        throws ConditionNotFoundException {
        final PriceManager priceManager = new PriceManager();

        /* Optional conditions */
        priceManager.addPriceCondition(optionalCondition(config, new MaxCrossTicks(riskManager.getRiskManagerConfig(), quoteSnapshot)));

        final MaxCrossPercentage maxCrossPercentage = optionalCondition(config, new MaxCrossPercentage(riskManager.getRiskManagerConfig(), quoteSnapshot));
        if (maxCrossPercentage != null) {
            priceManager.addPriceCondition(maxCrossPercentage);
            riskManager.addBaseCondition(maxCrossPercentage);
        }

        priceManager.createDesc();
        return priceManager;
    }

}
