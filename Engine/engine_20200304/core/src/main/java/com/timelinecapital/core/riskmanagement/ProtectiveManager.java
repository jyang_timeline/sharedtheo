package com.timelinecapital.core.riskmanagement;

import java.util.Arrays;
import java.util.Optional;

import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Side;

public class ProtectiveManager implements Validatable<Config>, Updateable {

    private BurstinessCondition[] burstConditions;
    private QuantityCondition[] qtyConditions;

    private String fullDesc;

    ProtectiveManager() {
        clear();
    }

    void createDesc() {
        fullDesc = "RiskControlManager{" +
            "Qty=" + qtyConditions.length +
            ", Bst=" + burstConditions.length +
            '}';
    }

    void clear() {
        burstConditions = new BurstinessCondition[0];
        qtyConditions = new QuantityCondition[0];
    }

    @Override
    public void acceptThrows(final Config config) throws Exception {
        for (final BurstinessCondition burstCondition : burstConditions) {
            if (burstCondition.checkUpdate(config) == false) {
                throw new ConditionNotFoundException("Error in protective condition.");
            }
        }
    }

    @Override
    public void updateConfig(final Config config) {
        for (final BurstinessCondition burstCondition : burstConditions) {
            burstCondition.onConfigChange(config);
        }
    }

    public boolean isMonitoring() {
        boolean isActive = true;
        for (final BurstinessCondition burstCondition : burstConditions) {
            isActive &= burstCondition.isActive();
        }
        return isActive;
    }

    public void launch() {
        for (final BurstinessCondition burstCondition : burstConditions) {
            burstCondition.enableRiskControl();
        }
    }

    void addBurstCondition(final BurstinessCondition condition) {
        burstConditions = com.nogle.util.ArrayUtils.addElement(burstConditions, condition);
    }

    public BurstinessCondition[] getBurstConditions() {
        return burstConditions;
    }

    public <T> Optional<T> getConditionsByBurstiness(final Class<T> clazz) {
        return Arrays.asList(burstConditions).stream().filter(clazz::isInstance).map(clazz::cast).findFirst();
    }

    void addQuantityCondition(final QuantityCondition qtyCondition) {
        qtyConditions = com.nogle.util.ArrayUtils.addElement(qtyConditions, qtyCondition);
    }

    public QuantityCondition[] getQtyConditions() {
        return qtyConditions;
    }

    public <T> Optional<T> getConditionsByQuantity(final Class<T> clazz) {
        return Arrays.asList(qtyConditions).stream().filter(clazz::isInstance).map(clazz::cast).findFirst();
    }

    public long adjustQuantity(final Side side, final long quantity, final long currentPosition) {
        long adjustedQuantity = quantity;
        for (final QuantityCondition qtyCondition : qtyConditions) {
            adjustedQuantity = qtyCondition.checkCondition(side, adjustedQuantity, currentPosition);
        }
        return adjustedQuantity;
    }

    @Override
    public String toString() {
        return fullDesc;
    }

}
