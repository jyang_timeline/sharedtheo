package com.timelinecapital.core.riskmanagement;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.condition.MaxCancelsPerInterval;
import com.nogle.core.position.condition.MaxDrawdown;
import com.nogle.core.position.condition.MaxLoss;
import com.nogle.core.position.condition.MaxOrderQuantityWhenClosing;
import com.nogle.core.position.condition.MaxRejectsPerInterval;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Config;
import com.timelinecapital.core.config.EngineConfig;

public class ProtectiveManagerBuilder extends ManagerBuilder {

    public static ProtectiveManager buildDummyRiskControlManager() {
        if (EngineConfig.getEngineMode().equals(EngineMode.PRODUCTION)) {
            throw new RuntimeException("Not allow to use STUB class in PRODUCTION mode");
        }
        return new ProtectiveManager();
    }

    public static ProtectiveManager buildRiskControlManager(final RiskManager riskManager, final Config config, final StrategyUpdater strategyUpdater)
        throws ConditionNotFoundException {
        final ProtectiveManager riskControlManager = new ProtectiveManager();

        final MaxDrawdown maxDrawdown = baseCondition(config, new MaxDrawdown(riskManager.getRiskManagerConfig(), strategyUpdater));
        riskControlManager.addQuantityCondition(maxDrawdown);
        riskControlManager.addBurstCondition(maxDrawdown);
        riskManager.addBaseCondition(maxDrawdown);

        final MaxLoss maxLoss = baseCondition(config, new MaxLoss(riskManager.getRiskManagerConfig(), strategyUpdater));
        riskControlManager.addQuantityCondition(maxLoss);
        riskControlManager.addBurstCondition(maxLoss);
        riskManager.addBaseCondition(maxLoss);

        final MaxRejectsPerInterval maxRejectsPerInterval = baseCondition(config, new MaxRejectsPerInterval(riskManager.getRiskManagerConfig(), strategyUpdater));
        riskControlManager.addBurstCondition(maxRejectsPerInterval);
        riskManager.addBaseCondition(maxRejectsPerInterval);

        final MaxCancelsPerInterval maxCancelsPerInterval = baseCondition(config, new MaxCancelsPerInterval(riskManager.getRiskManagerConfig(), strategyUpdater));
        riskControlManager.addBurstCondition(maxCancelsPerInterval);
        riskManager.addBaseCondition(maxCancelsPerInterval);

        final MaxOrderQuantityWhenClosing maxOrderQuantityWhenClosing = baseCondition(config, new MaxOrderQuantityWhenClosing());
        riskControlManager.addQuantityCondition(maxOrderQuantityWhenClosing);
        riskManager.addBaseCondition(maxOrderQuantityWhenClosing);

        riskControlManager.createDesc();
        return riskControlManager;
    }

}
