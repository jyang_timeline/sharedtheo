package com.timelinecapital.core.riskmanagement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.position.condition.types.BaseCondition;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.config.RiskManagerConfig;

public class RiskManager {
    private static final Logger log = LogManager.getLogger(RiskManager.class);

    private final Map<Integer, ProtectiveManager> strategyToProtectiveManager = new HashMap<>();
    private final Map<Integer, Map<Contract, PriceManager>> strategyToPriceManager = new HashMap<>();
    private final Map<Integer, Map<Contract, PositionManager>> strategyToPositionManagers = new HashMap<>();

    private final List<BaseCondition> baseConditions = new ArrayList<>();

    private final AtomicBoolean hasUpdate = new AtomicBoolean(false);
    private final RiskManagerConfig config;

    public RiskManager(final RiskManagerConfig config) {
        this.config = config;
    }

    public RiskManagerConfig getRiskManagerConfig() {
        return config;
    }

    public void addProtectiveManager(final int strategyId, final ProtectiveManager protectiveManager) {
        strategyToProtectiveManager.put(strategyId, protectiveManager);
    }

    public Collection<ProtectiveManager> getProtectiveManagers() {
        return strategyToProtectiveManager.values();
    }

    public ProtectiveManager getProtectiveManager(final int strategyId) {
        return strategyToProtectiveManager.get(strategyId);
    }

    public void addPriceManager(final int strategyId, final Map<Contract, PriceManager> priceManagers) {
        strategyToPriceManager.put(strategyId, priceManagers);
    }

    public Collection<PriceManager> getPriceManagers() {
        return strategyToPriceManager.values().stream().flatMap(entry -> entry.values().stream()).collect(Collectors.toList());
    }

    public Map<Contract, PriceManager> getPriceManager(final int strategyId) {
        return strategyToPriceManager.get(strategyId);
    }

    public void addPositionManager(final int strategyId, final Map<Contract, PositionManager> positionManagers) {
        strategyToPositionManagers.put(strategyId, positionManagers);
    }

    public Collection<PositionManager> getPositionManagers() {
        return strategyToPositionManagers.values().stream().flatMap(entry -> entry.values().stream()).collect(Collectors.toList());
    }

    public Map<Contract, PositionManager> getPositionManager(final int strategyId) {
        return strategyToPositionManagers.get(strategyId);
    }

    public void addBaseCondition(final BaseCondition baseCondition) {
        baseConditions.add(baseCondition);
    }

    // public boolean updateCondition(final int strategyId, final String conditionKey, final Object value) {
    // return false;
    // }

    public boolean updateCondition(final String conditionKey, final Object value) {
        try {
            config.checkAndApply(conditionKey, value);
        } catch (final ConfigurationException e) {
            log.warn(e.getMessage(), e);
            return false;
        }

        if (baseConditions.isEmpty()) {
            log.info("Update core risk management setting only. Strategy does not subscribe to {}", conditionKey);
            return true;
        }

        hasUpdate.set(false);
        baseConditions.forEach(condition -> {
            if (condition.getRenewableKey().contentEquals(conditionKey)) {
                condition.onDefaultChanage();
                hasUpdate.set(true);
            }
        });

        return hasUpdate.get();
    }

}
