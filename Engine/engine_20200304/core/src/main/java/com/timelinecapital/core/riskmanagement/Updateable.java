package com.timelinecapital.core.riskmanagement;

import com.nogle.strategy.types.Config;

public interface Updateable {

    void updateConfig(final Config config);

}
