package com.timelinecapital.core.riskmanagement;

import java.util.function.Consumer;

import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.strategy.types.Config;

@FunctionalInterface
public interface Validatable<T extends Config> extends Consumer<T> {

    // boolean checkUpdate(final Config config);

    // void updateConfig(final Config config);

    @Override
    default void accept(final T elem) {
        try {
            acceptThrows(elem);
        } catch (final Exception e) {
            new ConditionNotFoundException("Error in position condition.");
        }
    }

    void acceptThrows(T elem) throws Exception;

}
