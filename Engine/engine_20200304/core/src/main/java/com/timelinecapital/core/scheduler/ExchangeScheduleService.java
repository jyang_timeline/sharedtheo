package com.timelinecapital.core.scheduler;

public interface ExchangeScheduleService {

    void onPreOpening();

    void onOpening();

    void onOpeningFreeze();

    void onAuctionExecution();

    void onContinuousTrading();

    void onClosing();

    void onClosingFreeze();

}
