package com.timelinecapital.core.scheduler;

public interface MarketOpeningScheduleListener {

    void onPreOpeningTask();

}
