package com.timelinecapital.core.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.core.ScheduledService;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class NotificationService implements ScheduledService {
    private static final Logger log = LogManager.getLogger(NotificationService.class);

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;
    private final List<TriggerKey> triggerKeys = new ArrayList<>();
    private final Trigger notifyTrigger;

    private JobDetail jobDetail;

    enum CheckPoint {
        NOTIFY,
        NOTIFY_ONCE,
        ;
    }

    public NotificationService(final String triggerGroup, final CronExpression expression) {
        scheduler = JobScheduler.getInstance().getScheduler();
        final TriggerKey key = TriggerKey.triggerKey(CheckPoint.NOTIFY.toString(), triggerGroup);
        notifyTrigger = TriggerBuilder.newTrigger()
            .withIdentity(key)
            .withSchedule(CronScheduleBuilder.cronSchedule(expression))
            .build();
        triggerKeys.add(key);
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey) {
        throw new RuntimeException("Job should be specified");
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey, final Class<? extends Job> jobClass) {
        try {
            jobDataMap.put(schedulerKey, listener);
            jobDetail = JobBuilder.newJob(jobClass).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, notifyTrigger);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void setSingleJobListener(final ScheduledEventListener listener, final int triggerCount) {
        try {
            jobDataMap.put(ScheduleJobUtil.JOB_KEY_ONETIME, listener);
            final JobDetail oneTimeJob = JobBuilder.newJob(StrategySnapshotRefreshJob.class).setJobData(jobDataMap).build();
            final TriggerKey key = TriggerKey.triggerKey(CheckPoint.NOTIFY_ONCE.toString(), "ONE_TIME");
            final Trigger oneTimeTrigger = TriggerBuilder.newTrigger()
                .withIdentity(key)
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForTotalCount(triggerCount, 1)).build();
            triggerKeys.add(key);
            scheduler.scheduleJob(oneTimeJob, oneTimeTrigger);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void removeScheduleServiceListener() {
        log.warn("About to unschedule all notifications");
        triggerKeys.forEach(key -> {
            try {
                scheduler.unscheduleJob(key);
            } catch (final SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

}
