package com.timelinecapital.core.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.nogle.core.scheduler.IdleScheduleListener;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class PositionSyncJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        final MarketOpeningScheduleListener listener =
            (MarketOpeningScheduleListener) dataMap.getOrDefault(ScheduleJobUtil.JOB_KEY_PREOPENING_POS_SYNC, IdleScheduleListener.getInstance());
        listener.onPreOpeningTask();
    }

}
