package com.timelinecapital.core.scheduler;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class PreOpeningService {
    private static final Logger log = LogManager.getLogger(PreOpeningService.class);

    private static final String TRIGGER_GROUP = "PreOpeningService";

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;

    private final Set<Trigger> preOpeningTriggers = new HashSet<>();
    private final Set<Trigger> positionSyncTriggers = new HashSet<>();
    private final Set<Trigger> marketOpenTriggers = new HashSet<>();

    enum CheckPoint {
        CONTRACT_PRICE_LIMIT_CHECK,
        POSITION_SYNC_CHECK,
        MARKET_OPEN_CHECK,
        ;
    }

    public PreOpeningService() throws NumberFormatException, ParseException {
        scheduler = JobScheduler.getInstance().getScheduler();
        int scheduleNumber = 0;

        for (final String contractSchedule : EngineConfig.getContractSchedules()) {
            scheduleNumber++;
            final String[] timestamp = contractSchedule.split(":");
            final CronExpression cronExpression =
                new CronExpression(String.format("%d %d %d ? * *", Integer.parseInt(timestamp[2]), Integer.parseInt(timestamp[1]), Integer.parseInt(timestamp[0])));
            preOpeningTriggers.add(TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(scheduleNumber, CheckPoint.CONTRACT_PRICE_LIMIT_CHECK), TRIGGER_GROUP)
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                .build());
        }

        for (final String positionSchedule : EngineConfig.getPositionSyncSchedules()) {
            scheduleNumber++;
            final String[] timestamp = positionSchedule.split(":");
            final CronExpression cronExpression =
                new CronExpression(String.format("%d %d %d ? * *", Integer.parseInt(timestamp[2]), Integer.parseInt(timestamp[1]), Integer.parseInt(timestamp[0])));
            positionSyncTriggers.add(TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(scheduleNumber, CheckPoint.POSITION_SYNC_CHECK), TRIGGER_GROUP)
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                .build());
        }

        for (final String monitorSchedule : EngineConfig.getMonitorSchedules()) {
            scheduleNumber++;
            final String[] timestamp = monitorSchedule.split(":");
            final CronExpression cronExpression =
                new CronExpression(String.format("%d %d %d ? * *", Integer.parseInt(timestamp[2]), Integer.parseInt(timestamp[1]), Integer.parseInt(timestamp[0])));
            marketOpenTriggers.add(TriggerBuilder.newTrigger()
                .withIdentity(buildTriggerName(scheduleNumber, CheckPoint.MARKET_OPEN_CHECK), TRIGGER_GROUP)
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                .build());
        }

    }

    public void setPreOpeningInfoRefresh(final MarketOpeningScheduleListener listener) {
        try {
            jobDataMap.put(ScheduleJobUtil.JOB_KEY_PREOPENING_INFO, listener);
            final JobDetail jobDetail = JobBuilder.newJob(PreOpeningInfoRefreshJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, preOpeningTriggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void setPreOpeningPositionSync(final MarketOpeningScheduleListener listener) {
        try {
            jobDataMap.put(ScheduleJobUtil.JOB_KEY_PREOPENING_POS_SYNC, listener);
            final JobDetail jobDetail = JobBuilder.newJob(PositionSyncJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, positionSyncTriggers, true);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void setMarketOpen(final MarketOpeningScheduleListener listener) {
        try {
            jobDataMap.put(ScheduleJobUtil.JOB_KEY_MARKET_OPEN, listener);
            final JobDetail jobDetail = JobBuilder.newJob(MarketOpenJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, marketOpenTriggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    private String buildTriggerName(final int scheduleNumber, final CheckPoint checkPoint) {
        final StringBuilder sb = new StringBuilder();
        sb.append(scheduleNumber).append(DelimiterUtil.SESSION_DESCRIPTOR_DELIMITER).append(checkPoint);
        return sb.toString();
    }

}
