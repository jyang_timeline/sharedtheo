package com.timelinecapital.core.scheduler;

public interface ScheduledEventListener {

    void onScheduledEvent();

}
