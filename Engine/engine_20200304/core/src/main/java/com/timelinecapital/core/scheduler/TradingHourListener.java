package com.timelinecapital.core.scheduler;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.strategy.state.ExchangeSession;

public interface TradingHourListener {

    void onTradingHourChange(Exchange exchange, ExchangeSession session);

}
