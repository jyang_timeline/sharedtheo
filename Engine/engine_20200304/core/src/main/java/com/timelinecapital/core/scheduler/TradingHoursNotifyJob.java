package com.timelinecapital.core.scheduler;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.util.ScheduleJobUtil;
import com.timelinecapital.strategy.state.ExchangeSession;

public class TradingHoursNotifyJob implements Job {

    @SuppressWarnings("unchecked")
    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        final List<TradingHourListener> listeners = (List<TradingHourListener>) dataMap.get(ScheduleJobUtil.JOB_KEY_TRADINGHOUR_CHANGE);
        final List<Pair<Exchange, ExchangeSession>> pairs = (List<Pair<Exchange, ExchangeSession>>) dataMap.get(ScheduleJobUtil.JOB_KEY_TRADINGHOUR_EXCHANGE_LIST);

        listeners.stream().forEach(listener -> {
            pairs.stream().forEach(pair -> {
                listener.onTradingHourChange(pair.getLeft(), pair.getRight());
            });
        });
    }

}
