package com.timelinecapital.core.scheduler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.util.ScheduleJobUtil;
import com.timelinecapital.core.util.TradingHoursHelper;
import com.timelinecapital.strategy.state.ExchangeSession;

public class TradingHoursNotifyService {
    private static final Logger log = LogManager.getLogger(TradingHoursNotifyService.class);

    private final Scheduler scheduler;

    private final Set<Trigger> triggers = new HashSet<>();
    private final Map<Long, List<Pair<Exchange, ExchangeSession>>> timeToExchangeSession = new HashMap<>();
    private final List<TradingHourListener> listeners = new ArrayList<>();

    enum CheckPoint {
        SESSION_CHANGE;
    }

    public TradingHoursNotifyService() {
        scheduler = JobScheduler.getInstance().getScheduler();

        TradingHoursHelper.getSessionLookup().entrySet().stream().forEach(
            entry -> entry.getValue().stream().forEach(
                si -> timeToExchangeSession.computeIfAbsent(si.getInterval().getStartMillis(), k -> new ArrayList<>())
                    .add(new Pair<>(Exchange.valueOf(entry.getKey()), si.getSession()))));

        timeToExchangeSession.entrySet().forEach(entry -> {
            final DateTime time = new DateTime(entry.getKey());
            try {
                final CronExpression expression = new CronExpression(String.format("%d %d %d ? * *", time.getSecondOfMinute(), time.getMinuteOfHour(), time.getHourOfDay()));

                final JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(ScheduleJobUtil.JOB_KEY_TRADINGHOUR_EXCHANGE_LIST, entry.getValue());
                jobDataMap.put(ScheduleJobUtil.JOB_KEY_TRADINGHOUR_CHANGE, listeners);
                final JobDetail jobDetail = JobBuilder.newJob(TradingHoursNotifyJob.class).usingJobData(jobDataMap).withIdentity(time.toString()).build();

                final Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(buildTriggerName(CheckPoint.SESSION_CHANGE, time))
                    .withSchedule(CronScheduleBuilder.cronSchedule(expression))
                    .build();
                scheduler.scheduleJob(jobDetail, trigger);
                triggers.add(trigger);
            } catch (final ParseException e) {
                log.error(e.getMessage(), e);
            } catch (final SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    public void setTradingHourListener(final TradingHourListener listener) {
        listeners.add(listener);
    }

    private String buildTriggerName(final CheckPoint checkPoint, final DateTime time) {
        final StringBuilder sb = new StringBuilder();
        sb.append(checkPoint).append(DelimiterUtil.SESSION_DESCRIPTOR_DELIMITER).append(time);
        return sb.toString();
    }

}
