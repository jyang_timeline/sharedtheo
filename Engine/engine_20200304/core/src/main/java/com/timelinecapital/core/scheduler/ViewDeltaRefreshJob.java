package com.timelinecapital.core.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.nogle.core.scheduler.IdleScheduleListener;
import com.timelinecapital.core.util.ScheduleJobUtil;

public class ViewDeltaRefreshJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        final ScheduledEventListener listener =
            (ScheduledEventListener) dataMap.getOrDefault(ScheduleJobUtil.JOB_KEY_HEARTBEAT_DELTA, IdleScheduleListener.getInstance());
        listener.onScheduledEvent();
    }

}
