package com.timelinecapital.core.scheduler;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import com.nogle.core.util.JobScheduler;
import com.timelinecapital.core.ScheduledService;

public class WatchdogService implements ScheduledService {
    private static final Logger log = LogManager.getLogger(WatchdogService.class);

    private static final String TRIGGER_GROUP = "Watchdog";

    private final JobDataMap jobDataMap = new JobDataMap();
    private final Scheduler scheduler;
    private final Set<Trigger> triggers = new HashSet<>();
    private JobDetail jobDetail;

    enum CheckPoint {
        PRICE_LIMIT_CHECK;
    }

    public WatchdogService() throws ParseException {
        scheduler = JobScheduler.getInstance().getScheduler();
        final TriggerKey key = TriggerKey.triggerKey(CheckPoint.PRICE_LIMIT_CHECK.toString(), TRIGGER_GROUP);

        final CronExpression expression = new CronExpression(String.format("0/15 * * ? * *"));
        triggers.add(TriggerBuilder.newTrigger()
            .withIdentity(key)
            .withSchedule(CronScheduleBuilder.cronSchedule(expression))
            .build());
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey) {
        try {
            jobDataMap.put(schedulerKey, listener);
            jobDetail = JobBuilder.newJob(WatchdogJob.class).setJobData(jobDataMap).build();
            scheduler.scheduleJob(jobDetail, triggers, true);
        } catch (final SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void setScheduleServiceListener(final ScheduledEventListener listener, final String schedulerKey, final Class<? extends Job> jobClass) {
        throw new RuntimeException("Method not ready");
    }

    @Override
    public void removeScheduleServiceListener() {
        log.warn("About to unschedule watchdog service");
        triggers.forEach(t -> {
            try {
                scheduler.unscheduleJob(t.getKey());
            } catch (final SchedulerException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

}
