package com.timelinecapital.core.sharedtheo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class BasicCachedShareTheoReader implements CachedSharedTheoReader {

    private BufferedReader reader;

    private double currentValue = Double.NaN;
    private long nextTimeStampMicros;
    private double nextValue;

    BasicCachedShareTheoReader(final File cacheFile) {
        try {
            reader = new BufferedReader(new FileReader(cacheFile));
        } catch (final Exception e) {
        }
        next();
    }

    @Override
    public double update(final long timestampMicros) {
        while (!(nextTimeStampMicros > timestampMicros) && nextTimeStampMicros != Long.MAX_VALUE) {
            next();
        }
        return currentValue;
    }

    private void next() {
        try {
            if (nextTimeStampMicros != Long.MAX_VALUE) {
                final String newline = reader.readLine();
                if (newline != null) {
                    currentValue = nextValue;

                    final String[] words = newline.split(",");
                    nextValue = Double.parseDouble(words[1]);
                    nextTimeStampMicros = Long.parseLong(words[0]);
                } else {
                    currentValue = nextValue;

                    nextValue = Double.NaN;
                    nextTimeStampMicros = Long.MAX_VALUE;
                }
            }
        } catch (final IOException e) {
            // do nothing
        }

    }
}
