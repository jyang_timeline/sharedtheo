package com.timelinecapital.core.sharedtheo;

import java.io.File;
import java.io.FileWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Config;

public class BasicSharedTheoDumper implements SharedTheoDumper {

    private final Config config;
    private FileWriter writer;
    private final Logger logger = LogManager.getLogger(BasicSharedTheoDumper.class);

    BasicSharedTheoDumper(final Config config) {
        this.config = config;
    }

    @Override
    public void dump(final long timestampMicros, final double value) {
        if (writer == null) {
            try {
                final File dir = SharedTheoPathUtils.getDirPath(config).toFile();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                if (SharedTheoPathUtils.getPath(config).toFile().exists()) {
                    logger.warn("{} exists. Ignore Dumping.", SharedTheoPathUtils.getPath(config).toString());
                } else {
                    this.writer = new FileWriter(SharedTheoPathUtils.getPath(config).toFile());
                }
            } catch (final Exception e) {
                logger.warn("SharedTheoDumper cannot be created on {}", SharedTheoPathUtils.getPath(config).toString());
            }
        }

        if (writer != null) {
            try {
                writer.write(String.format("%d,%.6f\n", timestampMicros, value));
                writer.flush();
            } catch (final Exception e) {
            }
        }
    }
}
