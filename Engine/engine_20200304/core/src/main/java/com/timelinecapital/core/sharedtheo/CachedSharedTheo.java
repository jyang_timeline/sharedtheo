package com.timelinecapital.core.sharedtheo;

import java.io.File;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.strategy.api.SharedTheo;

public class CachedSharedTheo implements SharedTheo {

    private final CachedSharedTheoReader reader;

    CachedSharedTheo(final File cacheFile) {
        reader = new BasicCachedShareTheoReader(cacheFile);
    }

    @Override
    public double getValue() {
        return reader.update(TradeClock.getCurrentMicros());
    }

    @Override
    public void onMarketBook(final BookView bookView) {
        // do nothing
    }

    @Override
    public void onTick(final TickView tickView) {
        // do nothing
    }

    @Override
    public void onConfigChange(final Config config) {
        // do nothing
    }
}
