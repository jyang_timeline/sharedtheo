package com.timelinecapital.core.sharedtheo;

import java.io.File;

import com.timelinecapital.strategy.api.SharedTheo;
import com.timelinecapital.strategy.api.SharedTheoBuilder;
import com.timelinecapital.strategy.api.TheoServices;

public class CachedSharedTheoBuilder implements SharedTheoBuilder {
    private final File cacheFile;

    CachedSharedTheoBuilder(final File cacheFile) {
        this.cacheFile = cacheFile;
    }

    @Override
    public SharedTheo build(final TheoServices theoServices) {
        return new CachedSharedTheo(cacheFile);
    }
}
