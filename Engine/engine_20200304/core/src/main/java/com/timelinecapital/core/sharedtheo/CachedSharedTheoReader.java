package com.timelinecapital.core.sharedtheo;

public interface CachedSharedTheoReader {
    double update(long timestampMicros);
}
