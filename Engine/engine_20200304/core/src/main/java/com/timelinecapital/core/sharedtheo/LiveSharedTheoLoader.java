package com.timelinecapital.core.sharedtheo;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.nogle.core.market.Market;
import com.nogle.strategy.types.Config;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.strategy.api.SharedTheoBuilder;

public class LiveSharedTheoLoader extends SharedTheoLoader {
    public LiveSharedTheoLoader(final Market market) {
        super(market);
    }

    @Override
    @NotNull
    SharedTheoBuilder getSharedTheoBuilder(final ClassLoader jarClassLoader, final Config config)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final String classPath = config.getString(ShareTheoConfig.Key.ClassPath.toString());
        return (SharedTheoBuilder) Class.forName(classPath, true, jarClassLoader).getDeclaredConstructor().newInstance();
    }

    @Override
    void subscribeFeeds(final Config config, final SharedTheoHolder theoHolder, final Map<String, Instrument> contracts) {
        final Map<Instrument, List<FeedType>> contractToFeeds = buildFeedInfo(config, contracts);
        subscribeToFeed(contractToFeeds);
        subscribeToFeedListener(theoHolder, contractToFeeds);
    }
}
