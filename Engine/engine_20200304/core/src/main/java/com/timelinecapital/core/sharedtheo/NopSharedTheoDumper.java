package com.timelinecapital.core.sharedtheo;

public class NopSharedTheoDumper implements SharedTheoDumper {
    @Override
    public void dump(long timestamp, double value) {
        // do nothing
    }
}
