package com.timelinecapital.core.sharedtheo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import com.nogle.strategy.types.Config;

public class PropertiesConfig implements Config {
    private final Properties properties = new Properties();

    public PropertiesConfig(final InputStream propertiesStream) throws IOException {
        properties.load(propertiesStream);
    }

    @Override
    public boolean containKeys(final String... keys) {
        for (final String key : keys) {
            if (!properties.containsKey(key)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Set<String> getKeys() {
        return properties.keySet().stream().map(o -> (String) o).collect(Collectors.toSet());
    }

    @Override
    public Boolean getBoolean(final String key) {
        return Boolean.valueOf((String) properties.get(key));
    }

    @Override
    public Boolean getBoolean(final String key, final Boolean defaultValue) {
        if (properties.containsKey(key)) {
            return getBoolean(key);
        }
        return defaultValue;
    }

    @Override
    public Integer getInteger(final String key) {
        return Integer.valueOf((String) properties.get(key));
    }

    @Override
    public Integer getInteger(final String key, final Integer defaultValue) {
        if (properties.containsKey(key)) {
            return getInteger(key);
        }
        return defaultValue;
    }

    @Override
    public Double getDouble(final String key) {
        return Double.valueOf((String) properties.get(key));
    }

    @Override
    public Double getDouble(final String key, final Double defaultValue) {
        if (properties.containsKey(key)) {
            return getDouble(key);
        }
        return defaultValue;
    }

    @Override
    public String getString(final String key) {
        return (String) properties.get(key);
    }

    @Override
    public String getString(final String key, final String defaultValue) {
        if (properties.containsKey(key)) {
            return getString(key);
        }
        return defaultValue;
    }
}
