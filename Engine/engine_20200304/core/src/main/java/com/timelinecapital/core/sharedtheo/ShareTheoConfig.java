package com.timelinecapital.core.sharedtheo;

public class ShareTheoConfig {
    enum Key {
        Name("name"),
        ClassPath("classPath");

        private final String key;

        Key(final String key) {
            this.key = key;
        }

        @Override
        public String toString() {
            return key;
        }
    }
}
