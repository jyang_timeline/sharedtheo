package com.timelinecapital.core.sharedtheo;

public interface SharedTheoDumper {
    void dump(long t, double val);
}
