package com.timelinecapital.core.sharedtheo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.timelinecapital.strategy.api.SharedTheo;

public class SharedTheoHolder {

    protected static final Logger logger = LogManager.getLogger(SharedTheoHolder.class);
    private final int id;
    private final SharedTheo theo;

    SharedTheoHolder(final int id, final SharedTheo theo) {
        this.id = id;
        this.theo = theo;
    }

    public int id() {
        return id;
    }

    SharedTheo theo() {
        return theo;
    }
}
