package com.timelinecapital.core.sharedtheo;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.DataParserException;
import com.nogle.core.exception.InvalidCodeNameException;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.market.Market;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.TradeLogger;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.commons.exception.FeedException;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.event.handler.DirectFeedEventHandler;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.feed.ChannelListener;
import com.timelinecapital.core.feed.DirectChannelListener;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.mktdata.type.MarketDataSnapshot;
import com.timelinecapital.core.util.ExchangeProtocolUtil;
import com.timelinecapital.strategy.api.SharedTheo;
import com.timelinecapital.strategy.api.SharedTheoBuilder;
import com.timelinecapital.strategy.api.TheoServices;
import com.timelinecapital.strategy.types.OrderQueueView;

public abstract class SharedTheoLoader {

    protected static final Logger log = LogManager.getLogger(SharedTheoLoader.class);
    private static final int THEO_ID_START = 1000000;
    protected final Market market;
    private final Map<String, SharedTheoHolder> nameToSharedTheo;
    private int idOffset = 0;

    public SharedTheoLoader(final Market market) {
        this.market = market;
        this.nameToSharedTheo = new HashMap<>();
    }

    public void buildSharedTheo(final ClassLoader jarClassLoader, final String propertiesName) throws StrategyException {
        try {
            final InputStream propertiesStream = jarClassLoader.getResourceAsStream(propertiesName);
            if (propertiesStream != null) {
                if (propertiesStream.available() == 0) {
                    propertiesStream.reset();
                }
                final Config config = new PropertiesConfig(propertiesStream);
                loadCascadedSharedTheos(jarClassLoader, config);

                final SharedTheoBuilder theoBuilder = getSharedTheoBuilder(jarClassLoader, config);
                final Map<String, Instrument> contracts = buildContract(config);
                final SharedTheoHolder theoHolder = getSharedTheoHolder(propertiesName, config, theoBuilder, contracts);
                subscribeFeeds(config, theoHolder, contracts);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new StrategyException("Failed to create SharedTheo instance", e);
        }
    }

    private void loadCascadedSharedTheos(final ClassLoader jarClassLoader, final Config config) {
        config.getKeys().stream().filter(s -> s.startsWith("SharedTheo.")).forEach(key -> {
            final String theoPropertiesName = config.getString(key);
            final SharedTheo theo = get(theoPropertiesName);
            if (theo == null) {
                try {
                    buildSharedTheo(jarClassLoader, config.getString(key));
                } catch (final StrategyException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    abstract void subscribeFeeds(Config config, final SharedTheoHolder theoHolder, final Map<String, Instrument> contracts) throws DataParserException, IOException;

    @NotNull
    private SharedTheoHolder getSharedTheoHolder(final String propertiesName, final Config config, final SharedTheoBuilder theoBuilder, final Map<String, Instrument> contracts) {
        final TradeLogger logger = new TradeLoggerImpl(LogManager.getLogger(StringUtils.substringBefore(config.getString(ShareTheoConfig.Key.Name.toString()), ".")));
        final TheoServices theoServices = new TheoServicesImpl(contracts, logger);
        final SharedTheo theo = theoBuilder.build(theoServices);
        theo.onConfigChange(config);
        final int id = getNextUniqueTheoId();
        final SharedTheoHolder theoHolder = new SharedTheoHolder(id, theo);
        nameToSharedTheo.put(propertiesName, theoHolder);
        return theoHolder;
    }

    @NotNull
    abstract SharedTheoBuilder getSharedTheoBuilder(final ClassLoader jarClassLoader, final Config config)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

    private int getNextUniqueTheoId() {
        return THEO_ID_START + ++idOffset;
    }

    private Map<String, Instrument> buildContract(final Config config) {
        final Map<String, Instrument> contracts = new HashMap<>();

        config.getKeys().stream().filter(s -> s.startsWith("Contract.")).forEach(key -> {
            final String value = config.getString(key);
            try {
                final Instrument contract = ContractFactory.getByTradingDay(value, TradeClock.getTradingDay());
                contracts.put(key, contract);
            } catch (final InvalidCodeNameException e) {
                log.error(e.getMessage(), e);
            }
        });

        log.info("TradingDay: {}, Contracts: {}\n", TradeClock.getTradingDay(), contracts.values());

        return contracts;
    }

    Map<Instrument, List<FeedType>> buildFeedInfo(final Config config, final Map<String, Instrument> contracts) {
        final Map<String, List<FeedType>> keyToFeeds = new HashMap<>();
        final Map<Instrument, List<FeedType>> contractToFeeds = new HashMap<>();

        for (final Map.Entry<String, Instrument> contractEntry : contracts.entrySet()) {
            final String representKey = StringUtils.substringAfter(contractEntry.getKey(), DelimiterUtil.CONTRACT_DESCRIPTOR_DELIMETER);
            if (!keyToFeeds.containsKey(representKey)) {
                keyToFeeds.put(representKey, new ArrayList<>());
            }
            keyToFeeds.get(representKey).addAll(ExchangeProtocolUtil.getQuoteFeedType(contractEntry.getValue().getExchange(), contractEntry.getValue().getSecurityType()));
            contractToFeeds.put(contractEntry.getValue(), keyToFeeds.get(representKey));
        }

        final Map<Instrument, List<FeedType>> contractToAdditionalFeeds = buildAdditionalData(config, contracts);
        contractToAdditionalFeeds.forEach((contract, feeds) -> feeds.forEach((feedType -> contractToFeeds.get(contract).add(feedType))));

        log.info("Contracts to Feed: {}", contractToFeeds);
        return contractToFeeds;
    }

    private Map<Instrument, List<FeedType>> buildAdditionalData(final Config config, final Map<String, Instrument> contracts) {
        final Map<Instrument, List<FeedType>> contractToAdditionalDataFeeds = new HashMap<>();

        config.getKeys().stream().filter(s -> s.startsWith("AdditionalData.")).forEach(key -> {
            final String value = config.getString(key);
            final String[] contractKeys = value.split(DelimiterUtil.CONTRACT_DELIMETER);
            for (final String contractKey : contractKeys) {
                final Instrument contract = contracts.get("Contract." + contractKey);
                final String feedName = StringUtils.substringAfter(key, DelimiterUtil.CONTRACT_DESCRIPTOR_DELIMETER);
                final FeedType feedType = FeedType.valueOf(feedName);
                if (!contractToAdditionalDataFeeds.containsKey(contract)) {
                    contractToAdditionalDataFeeds.put(contract, new ArrayList<>());
                }
                contractToAdditionalDataFeeds.get(contract).add(feedType);
            }
        });

        return contractToAdditionalDataFeeds;
    }

    void subscribeToFeed(final Map<Instrument, List<FeedType>> contractToFeeds) {
        contractToFeeds.forEach((contract, feedTypes) -> {
            final Exchange exchange = Exchange.valueOf(contract.getExchange());
            final FeedType[] feedTypesArray = feedTypes.toArray(new FeedType[0]);
            try {
                market.subscribeFeed(exchange, feedTypesArray);
            } catch (final FeedException e) {
                log.error("Failed to subscribe feeds {} -> {}: {}", exchange, feedTypes, e.getMessage());
            }
        });

    }

    void subscribeToFeedListener(final SharedTheoHolder theoHolder, final Map<Instrument, List<FeedType>> contractToFeeds) {
        final DirectChannelListener channelListener = new DirectChannelListener(theoHolder.id(), ChannelListener.ListenerType.QUOTE_THREAD_SHARED_THEO);
        final List<FeedType> distinctFeedTypes = contractToFeeds.values().stream().flatMap(List::stream).distinct().collect(Collectors.toList());
        distinctFeedTypes.forEach(feedType -> {
            switch (feedType) {
                case OrderActions:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<QuoteOrderView>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            theoHolder.theo().onQuoteOrderDetail(data);
                        }
                    }, DataType.ORDERACTIONS);
                    break;
                case OrderQueue:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<OrderQueueView>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            // theoHolder.theo().onOrderQueue(data);
                        }
                    }, DataType.ORDERQUEUE);
                    break;
                case BestPriceOrderDetail:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<BestOrderView>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            theoHolder.theo().onBestOrderDetail(data);
                        }
                    }, DataType.BESTORDERDETAIL);
                    break;
                case Snapshot:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<MarketDataSnapshot>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            switch (data.getUpdateType()) {
                                case TRADE_ONLY:
                                    theoHolder.theo().onTick(data.getTick());
                                    break;
                                case MARKETBOOK_ONLY:
                                    theoHolder.theo().onMarketBook(data.getMarketBook());
                                    break;
                                case ALL:
                                    theoHolder.theo().onTick(data.getTick());
                                    theoHolder.theo().onMarketBook(data.getMarketBook());
                                    break;
                            }
                        }
                    }, DataType.SNAPSHOT);
                    break;
                case Trade:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<TickView>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            theoHolder.theo().onTick(data);
                        }
                    }, DataType.TICK);
                    break;
                case MarketBook:
                    channelListener.registerEventHandler(new DirectFeedEventHandler<BookView>(theoHolder.id()) {
                        @Override
                        public void handle() {
                            theoHolder.theo().onMarketBook(data);
                        }
                    }, DataType.MARKETBOOK);
                    break;
            }
        });

        contractToFeeds.forEach((contract, feedTypes) -> feedTypes.forEach(feedType -> {
            market.subscribeContract(theoHolder.id(), contract, feedType, channelListener);
        }));

    }

    public SharedTheo get(final String theoName) {
        final SharedTheoHolder theoHolder = nameToSharedTheo.get(theoName);
        if (theoHolder != null) {
            return theoHolder.theo();
        } else {
            return null;
        }
    }

    public void clear() {
        nameToSharedTheo.clear();
    }

    class TheoServicesImpl implements TheoServices {

        private final Map<String, Instrument> contracts;
        private final boolean isSimulation = EngineMode.SIMULATION.equals(EngineConfig.getEngineMode());
        private final TradeLogger logger;

        TheoServicesImpl(final Map<String, Instrument> contracts, final TradeLogger logger) {
            this.contracts = contracts;
            this.logger = logger;
        }

        @Override
        public Instrument getContract(final String key) {
            return contracts.get(key);
        }

        @Override
        public TradeLogger getLogger() {
            return logger;
        }

        @Override
        public boolean isSimulation() {
            return isSimulation;
        }

        @Override
        public SharedTheo getSharedTheo(final String theoPropertiesName) {
            return get(theoPropertiesName);
        }
    }

    class TradeLoggerImpl implements TradeLogger {

        private final Logger logger;

        TradeLoggerImpl(final Logger logger) {
            this.logger = logger;
        }

        @Override
        public void error(final String message) {
            logger.error(message);
        }

        @Override
        public void error(final String message, final Object... arguments) {
            logger.error(message, arguments);
        }

        @Override
        public void warn(final String message) {
            logger.warn(message);
        }

        @Override
        public void warn(final String message, final Object... arguments) {
            logger.warn(message, arguments);
        }

        @Override
        public void info(final String message) {
            logger.info(message);
        }

        @Override
        public void info(final String message, final Object... arguments) {
            logger.info(message, arguments);
        }

        @Override
        public void debug(final String message) {
            logger.debug(message);
        }

        @Override
        public void debug(final String message, final Object... arguments) {
            logger.debug(message, arguments);
        }
    }
}
