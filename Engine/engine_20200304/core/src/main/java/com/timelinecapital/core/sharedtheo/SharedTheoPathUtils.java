package com.timelinecapital.core.sharedtheo;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.timelinecapital.core.config.SimulationConfig;

class SharedTheoPathUtils {
    static Path getPath(final Config config) {
        return Paths.get(SimulationConfig.getTheoPath(), config.getString("name"), TradeClock.getTradingDay());
    }

    static Path getDirPath(final Config config) {
        return Paths.get(SimulationConfig.getTheoPath(), config.getString("name"));
    }
}
