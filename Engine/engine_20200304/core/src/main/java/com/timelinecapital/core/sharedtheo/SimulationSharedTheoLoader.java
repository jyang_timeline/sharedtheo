package com.timelinecapital.core.sharedtheo;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.exception.DataParserException;
import com.nogle.core.market.Market;
import com.nogle.core.market.SimulatedMarket;
import com.nogle.core.marketdata.PriceFeedProxyListener;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.strategy.config.SimulatedTimeFrame;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.strategy.api.SharedTheoBuilder;

public class SimulationSharedTheoLoader extends SharedTheoLoader {

    private final boolean dumpTheo = SimulationConfig.isDumpSharedTheo();
    private final SimulatedMarket simMarket;

    public SimulationSharedTheoLoader(final Market market) {
        super(market);
        simMarket = (SimulatedMarket) market;
        log.warn("dumpTheo = {}", dumpTheo);
    }

    @Override
    @NotNull
    SharedTheoBuilder getSharedTheoBuilder(final ClassLoader jarClassLoader, final Config config)
        throws InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        if (dumpTheo && cacheExists(config)) {
            log.info("SharedTheo file found {}. Use Cache.", SharedTheoPathUtils.getPath(config));
            return new CachedSharedTheoBuilder(SharedTheoPathUtils.getPath(config).toFile());
        }

        final String classPath = config.getString(ShareTheoConfig.Key.ClassPath.toString());
        return (SharedTheoBuilder) Class.forName(classPath, true, jarClassLoader).getDeclaredConstructor().newInstance();
    }

    private boolean cacheExists(final Config config) {
        return SimulationConfig.isUsingCachedSharedTheo()
            && SharedTheoPathUtils.getPath(config).toFile().exists();
    }

    @Override
    void subscribeFeeds(final Config config, final SharedTheoHolder theoHolder, final Map<String, Instrument> contracts) throws DataParserException, IOException {
        if (dumpTheo && cacheExists(config)) {
            return; // no need to subscribe feed since we'll load from cache
        }

        final Map<Instrument, List<FeedType>> contractToFeeds = buildFeedInfo(config, contracts);
        subscribeFeedSimulation(config, theoHolder, contracts, contractToFeeds);
    }

    private void subscribeFeedSimulation(final Config config, final SharedTheoHolder theoHolder, final Map<String, Instrument> contracts,
        final Map<Instrument, List<FeedType>> contractToFeeds) throws DataParserException, IOException {

        final DateTime day = DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT);
        final Map<Contract, File[]> marketDataFiles = simMarket.getPriceFeedSources(new SimulatedTimeFrame(day, day), contracts);
        final Map<Contract, File[]> feedFiles = simMarket.getAdditionalPriceFeedSources(new SimulatedTimeFrame(day, day), getFeedToContracts(contractToFeeds));
        simMarket.prepareQuotes(marketDataFiles, feedFiles);

        final SharedTheoDumper dumper = getDumper(config);

        for (final Entry<Instrument, List<FeedType>> entry : contractToFeeds.entrySet()) {
            final QuoteView quoteView = simMarket.subscribeFeed(entry.getKey(), entry.getValue());

            quoteView.addPriceFeedProxyListener(new PriceFeedProxyListener() {
                @Override
                public void onMarketBook(final BDBookView marketBook) {
                    theoHolder.theo().onMarketBook(marketBook);
                    dumper.dump(marketBook.getUpdateTimeMicros(), theoHolder.theo().getValue());
                }

                @Override
                public void onTick(final BDTickView tick) {
                    theoHolder.theo().onTick(tick);
                    dumper.dump(tick.getUpdateTimeMicros(), theoHolder.theo().getValue());
                }

                @Override
                public void onBestPriceOrderDetail(final BDBestPriceOrderQueueView event) {
                    theoHolder.theo().onBestOrderDetail(event);
                    dumper.dump(event.getUpdateTimeMicros(), theoHolder.theo().getValue());
                }

                @Override
                public void onOrderActions(final BDOrderActionsView event) {
                    theoHolder.theo().onQuoteOrderDetail(event);
                    dumper.dump(event.getUpdateTimeMicros(), theoHolder.theo().getValue());
                }

                @Override
                public void onOrderQueue(final BDOrderQueueView event) {
                    theoHolder.theo().onOrderQueue(event);
                    dumper.dump(event.getUpdateTimeMicros(), theoHolder.theo().getValue());
                }

            }, theoHolder.id(), entry.getValue());
        }

    }

    private Map<FeedType, List<Contract>> getFeedToContracts(final Map<Instrument, List<FeedType>> contractToFeeds) {
        final Map<FeedType, List<Contract>> feedToContracts = new HashMap<>();
        contractToFeeds.forEach((contract, feeds) -> {
            for (final FeedType f : feeds) {
                if (f == FeedType.Snapshot || f == FeedType.MarketBook || f == FeedType.Trade) {
                    // skip types which we already handled as PriceFeed
                    continue;
                }
                final List<Contract> contracts = feedToContracts.computeIfAbsent(f, k -> new LinkedList<>());
                contracts.add(contract);
            }
        });
        return feedToContracts;
    }

    private SharedTheoDumper getDumper(final Config config) {
        final boolean shouldDumpTheoValue = dumpTheo && !cacheExists(config);
        SharedTheoDumper dumper = new NopSharedTheoDumper();
        if (shouldDumpTheoValue) {
            final File cacheFile = SharedTheoPathUtils.getPath(config).toFile();
            try {
                dumper = new TimeIntervalSharedTheoDumper(cacheFile, SimulationConfig.getSharedTheoSampleInterval());
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        }

        return dumper;
    }
}
