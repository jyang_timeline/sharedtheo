package com.timelinecapital.core.sharedtheo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TimeIntervalSharedTheoDumper implements SharedTheoDumper {

    private final FileWriter writer;
    private final Logger logger = LogManager.getLogger(TimeIntervalSharedTheoDumper.class);
    private long preTimeStampMicros = Long.MIN_VALUE;
    private double preVal = Double.NaN;
    private final long interval;

    TimeIntervalSharedTheoDumper(final File file, final long interval) throws IOException {
        this.interval = interval;
        final File dir = file.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }
        this.writer = new FileWriter(file);

        logger.info("SharedTheo Sampling Interval: {}", interval);
    }

    @Override
    public void dump(final long timestampMicros, final double value) {
        if ((timestampMicros - interval >= preTimeStampMicros) && (value != preVal)) {
            preTimeStampMicros = timestampMicros;
            preVal = value;
            try {
                writer.write(String.format("%d,%.6f\n", timestampMicros, value));
                writer.flush();
            } catch (final Exception e) {
            }
        }
    }
}
