package com.timelinecapital.core.stats;

public interface AccountStatistics {

    String getName();

    int getIndex();

    ExecReportSink getStatisticsUpdater();

}
