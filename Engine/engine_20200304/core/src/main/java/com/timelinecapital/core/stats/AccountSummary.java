package com.timelinecapital.core.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.trade.TradingAccount;

public final class AccountSummary implements Overview {

    private final Map<Contract, ExecReportSink> execOverviews = new HashMap<>();
    private final List<Double> expousres = new ArrayList<>();

    private final AtomicInteger index = new AtomicInteger(0);
    private final OverviewUpdater updater = new OverviewUpdater();
    private final TradingAccount account;

    private double exposuresLong;
    private double exposuresShort;

    private double volume;
    private double value;
    private long orders;
    private long fills;
    private long cancels;
    private long rejects;

    public AccountSummary(final TradingAccount account) {
        this.account = account;
    }

    @Override
    public final String getName() {
        return account.getAccountName();
    }

    @Override
    public final OverviewUpdater getStatisticsUpdater() {
        return updater;
    }

    @Override
    public final int getIndex() {
        final int idx = index.getAndIncrement();
        expousres.add(idx, 0.0d);
        return idx;
    }

    @Override
    public final void reset() {
        execOverviews.forEach((k, v) -> v.reset());
    }

    @Override
    public final double getExposure(final Side side) {
        switch (side) {
            case BUY:
                return exposuresLong;
            case SELL:
                return exposuresShort;
            default:
                return exposuresLong + Math.abs(exposuresShort);
        }
    }

    @Override
    public final double tradeVolume() {
        return volume;
    }

    @Override
    public final double tradeValue() {
        return value;
    }

    @Override
    public final long orders() {
        return orders;
    }

    @Override
    public final long fills() {
        return fills;
    }

    @Override
    public final long cancels() {
        return cancels;
    }

    @Override
    public final long rejects() {
        return rejects;
    }

    final class OverviewUpdater implements ExecReportSink {

        @Override
        public void onFill(final Side side, final double price, final long qty, final double commission) {
            fills++;
            volume += qty;
            value += price * qty;
        }

        @Override
        public void onOrderAck(final long orderQty, final Side side) {
            orders++;
        }

        @Override
        public void onCancelAck(final long cancelQty, final Side side) {
            cancels++;
        }

        @Override
        public void onOrderReject() {
            rejects++;
        }

        @Override
        public void onCancelReject() {
        }

        @Override
        public void reset() {
            orders = cancels = rejects = 0l;
        }

        @Override
        public void updateExposure(final int index, final double exposure) {
            expousres.set(index, exposure);
            exposuresLong = expousres.stream().filter(value -> value > 0.0d).mapToDouble(value -> value).sum();
            exposuresShort = expousres.stream().filter(value -> value < 0.0d).mapToDouble(value -> value).sum();
        }

    }

}
