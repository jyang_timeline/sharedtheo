package com.timelinecapital.core.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.watchdog.ContractWatcher;
import com.timelinecapital.core.watchdog.PriceLimits;
import com.timelinecapital.strategy.types.PositionType;

public final class ContractSummary implements FeedEvent, ExecReportStats {

    private final Map<String, ExecReportEvent> updaters = new HashMap<>();
    private final List<ContractWatcher> watchers = new ArrayList<>();
    private final PriceLimits priceWatcher;

    private final long[] feedEvents = new long[DataType.values().length];

    private final Contract contract;
    // private final ExecReportSink contractStatistics;

    private double exposure;
    private double totalChargedFee;
    private long volume;
    private long position;

    private int fills;
    private int orderAcks;
    private int cancelAcks;
    private int orderRejects;
    private int cancelRejects;

    public ContractSummary(final Contract contract) {
        this.contract = contract;
        // contractStatistics = new ContractBasedExecReportSummary();
        priceWatcher = new PriceLimits(contract);
        watchers.add(priceWatcher);
        for (int i = 0; i < feedEvents.length; i++) {
            feedEvents[i] = 0;
        }
    }

    final ExecReportEvent withAccount(final Overview accountView) {
        ExecReportEvent updater = updaters.get(accountView.getName());
        if (updater == null) {
            final int index = accountView.getIndex();
            updater = new AccountBasedExecReportSummary(accountView.getStatisticsUpdater(), index);
            updaters.put(accountView.getName(), updater);
        }
        return updater;
    }

    // public final ExecReportSink getExecReportViewUpdater(final String account) {
    // final ExecReportSink updater = updaters.get(account);
    // if (updater == null) {
    // return contractStatistics;
    // }
    // return updater;
    // }

    public final Contract getContract() {
        return contract;
    }

    @Override
    public final double getExposure() {
        return exposure;
    }

    @Override
    public final double getTotalChargedFee() {
        return totalChargedFee;
    }

    @Override
    public final long getVolume() {
        return volume;
    }

    @Override
    public final long getPosition() {
        return position;
    }

    @Override
    public final long getFills() {
        return fills;
    }

    @Override
    public final long getOrderAcks() {
        return orderAcks;
    }

    @Override
    public final long getCancelAcks() {
        return cancelAcks;
    }

    @Override
    public final long getOrderRejects() {
        return orderRejects;
    }

    @Override
    public final long getPicks() {
        return cancelRejects;
    }

    public final List<ContractWatcher> getWatchers() {
        return watchers;
    }

    @Override
    public final void onMarketData(final DataType dataType) {
        feedEvents[dataType.getSeqValue()]++;
    }

    @Override
    public final void onQuote(final QuoteType quoteType) {
        priceWatcher.onEvent(quoteType);
    }

    @Override
    public final synchronized void reset() {
        totalChargedFee = 0.0d;
        volume = position = 0l;
        fills = orderAcks = cancelAcks = orderRejects = cancelRejects = 0;
        for (int i = 0; i < feedEvents.length; i++) {
            feedEvents[i] = 0;
        }
    }

    final class ContractBasedExecReportSummary implements ExecReportSink {

        @Override
        public void onFill(final Side side, final double price, final long qty, final double commission) {
            ContractSummary.this.fills++;
            ContractSummary.this.volume += qty;
            ContractSummary.this.position += side.getQuantityDelta(qty);
            ContractSummary.this.totalChargedFee += commission;
        }

        @Override
        public void onOrderAck(final long orderQty, final Side side) {
            ContractSummary.this.orderAcks++;
        }

        @Override
        public void onCancelAck(final long cancelQty, final Side side) {
            ContractSummary.this.cancelAcks++;
        }

        @Override
        public void onOrderReject() {
            ContractSummary.this.orderRejects++;
        }

        @Override
        public void onCancelReject() {
            ContractSummary.this.cancelRejects++;
        }

        @Override
        public void reset() {
            ContractSummary.this.reset();
        }

    }

    final class AccountBasedExecReportSummary implements ExecReportEvent {

        private final ExecReportSink accountSummary;
        private final int index;

        private PositionInfo info;

        private double exposure;
        private double totalChargedFee;
        private long volume;
        private long position;

        private long fills;
        private long orderAcks;
        private long cancelAcks;
        private long orderRejects;
        private long cancelRejects;

        public AccountBasedExecReportSummary(final ExecReportSink accountSummary, final int index) {
            this.accountSummary = accountSummary;
            this.index = index;
        }

        @Override
        public void onStaticPositionInfo(final PositionInfo info) {
            this.info = info;
        }

        @Override
        public long getStaticPositionInfo(final PositionType type) {
            return info.getQty(type);
        }

        @Override
        public final double getExposure() {
            return exposure;
        }

        @Override
        public final double getTotalChargedFee() {
            return totalChargedFee;
        }

        @Override
        public final long getVolume() {
            return volume;
        }

        @Override
        public final long getPosition() {
            return position;
        }

        @Override
        public final long getFills() {
            return fills;
        }

        @Override
        public final long getOrderAcks() {
            return orderAcks;
        }

        @Override
        public final long getCancelAcks() {
            return cancelAcks;
        }

        @Override
        public final long getOrderRejects() {
            return orderRejects;
        }

        @Override
        public final long getPicks() {
            return cancelRejects;
        }

        @Override
        public final void ofExposure(final double exposure) {
            this.exposure += exposure;
            ContractSummary.this.exposure += exposure;
            accountSummary.updateExposure(index, exposure);
        }

        @Override
        public final void onFill(final Side side, final double price, final long qty, final double commission) {
            fills++;
            volume += qty;
            position += side.getQuantityDelta(qty);
            totalChargedFee += commission;
            ContractSummary.this.fills++;
            ContractSummary.this.volume += qty;
            ContractSummary.this.position += side.getQuantityDelta(qty);
            ContractSummary.this.totalChargedFee += commission;
            accountSummary.onFill(side, price, qty, commission);
            info.onFill(qty, side);
        }

        @Override
        public final void onOrderAck(final long orderQty, final Side side) {
            orderAcks++;
            ContractSummary.this.orderAcks++;
            accountSummary.onOrderAck(orderQty, side);
            info.onOrderAck(orderQty, side);
        }

        @Override
        public final void onCancelAck(final long cancelQty, final Side side) {
            cancelAcks++;
            ContractSummary.this.cancelAcks++;
            accountSummary.onCancelAck(cancelQty, side);
            info.onCancelAck(cancelQty, side);
        }

        @Override
        public final void onOrderReject() {
            orderRejects++;
            ContractSummary.this.orderRejects++;
            accountSummary.onOrderReject();
        }

        @Override
        public final void onCancelReject() {
            cancelRejects++;
            ContractSummary.this.cancelRejects++;
            accountSummary.onCancelReject();
        }

        @Override
        public final void reset() {
            totalChargedFee = 0.0d;
            volume = position = 0l;
            fills = orderAcks = cancelAcks = orderRejects = cancelRejects = 0;
            accountSummary.reset();
        }

    }

}
