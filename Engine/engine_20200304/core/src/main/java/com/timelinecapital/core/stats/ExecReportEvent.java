package com.timelinecapital.core.stats;

import com.timelinecapital.strategy.types.PositionType;

public interface ExecReportEvent extends ExecReportSink, ExecReportStats {

    void onStaticPositionInfo(PositionInfo info);

    long getStaticPositionInfo(PositionType type);

}
