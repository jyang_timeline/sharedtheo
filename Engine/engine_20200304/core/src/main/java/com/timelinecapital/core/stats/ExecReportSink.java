package com.timelinecapital.core.stats;

import com.nogle.strategy.types.Side;

public interface ExecReportSink extends ExposureView {

    void onFill(final Side side, double price, final long qty, final double commission);

    void onOrderAck(long orderQty, Side side);

    void onCancelAck(long cancelQty, Side side);

    void onOrderReject();

    void onCancelReject();

    void reset();

}
