package com.timelinecapital.core.stats;

public interface ExecReportStats {

    default double getExposure() {
        return 0;
    }

    long getVolume();

    long getPosition();

    double getTotalChargedFee();

    long getFills();

    long getOrderAcks();

    long getCancelAcks();

    long getOrderRejects();

    long getPicks();

}
