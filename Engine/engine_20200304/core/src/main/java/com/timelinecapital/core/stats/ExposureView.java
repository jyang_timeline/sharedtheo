package com.timelinecapital.core.stats;

public interface ExposureView {

    default void ofExposure(final double exposure) {
    }

    default void updateExposure(final int index, final double exposure) {
    }

}
