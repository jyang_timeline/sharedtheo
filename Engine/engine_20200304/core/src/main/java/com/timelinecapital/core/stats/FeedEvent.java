package com.timelinecapital.core.stats;

import com.nogle.strategy.types.QuoteType;
import com.timelinecapital.commons.type.DataType;

public interface FeedEvent {

    void onMarketData(DataType dataType);

    void onQuote(QuoteType quoteType);

    void reset();

}
