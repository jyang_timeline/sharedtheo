package com.timelinecapital.core.stats;

import com.timelinecapital.view.TradeStatistics;

public interface Overview extends TradeStatistics, AccountStatistics {

    void reset();

}
