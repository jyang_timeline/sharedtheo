package com.timelinecapital.core.stats;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.trade.TradingAccount;

public class OverviewFactory {

    private static final Map<Contract, ContractSummary> contractOverviews = new HashMap<>();
    private static final Map<TradingAccount, AccountSummary> accountOverviews = new HashMap<>();
    private static final Map<TradingAccount, PositionSummary> positionOverviews = new HashMap<>();

    public static Map<Contract, ContractSummary> getContractOverviews() {
        return Collections.unmodifiableMap(contractOverviews);
    }

    public static Map<TradingAccount, PositionSummary> getPositionOverviews() {
        return Collections.unmodifiableMap(positionOverviews);
    }

    public static Overview getOverview(final TradingAccount account) {
        return get(account);
    }

    public static FeedEvent getFeedSBean(final Contract contract) {
        return get(contract);
    }

    public static PositionImport getPositionImport(final TradingAccount account) {
        return getImporter(account);
    }

    public static ExecReportEvent getExecReportSBean(final Contract contract, final TradingAccount account) {
        final AccountSummary accountStats = get(account);
        final ContractSummary contractStats = get(contract);
        return contractStats.withAccount(accountStats);
    }

    public static void reset() {
        contractOverviews.forEach((k, v) -> v.reset());
        accountOverviews.forEach((k, v) -> v.reset());
        accountOverviews.clear();
        contractOverviews.clear();
    }

    private static AccountSummary get(final TradingAccount account) {
        AccountSummary accountStats = accountOverviews.get(account);
        if (accountStats == null) {
            accountStats = new AccountSummary(account);
            accountOverviews.put(account, accountStats);
        }
        return accountStats;
    }

    private static ContractSummary get(final Contract contract) {
        ContractSummary contractStats = contractOverviews.get(contract);
        if (contractStats == null) {
            contractStats = new ContractSummary(contract);
            contractOverviews.put(contract, contractStats);
        }
        return contractStats;
    }

    private static PositionSummary getImporter(final TradingAccount account) {
        PositionSummary positionStats = positionOverviews.get(account);
        if (positionStats == null) {
            positionStats = new PositionSummary(account, EngineConfig.getEngineId());
            positionOverviews.put(account, positionStats);
        }
        return positionStats;
    }

}
