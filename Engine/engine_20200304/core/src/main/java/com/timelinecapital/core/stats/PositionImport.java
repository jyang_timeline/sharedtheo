package com.timelinecapital.core.stats;

import com.timelinecapital.commons.binary.PositionResponseDecoder;

public interface PositionImport {

    void registerWith(String symbol, ExecReportEvent updater);

    void checkStaticPositions(String symbol);

    PositionInfo get(String symbol);

    void update(String symbol, PositionInfo info);

    void updateFrom(PositionResponseDecoder decoder);

    void sink();

}
