package com.timelinecapital.core.stats;

import com.nogle.strategy.types.Side;
import com.timelinecapital.strategy.types.PositionType;

public class PositionInfo {

    private final long[] infos = new long[PositionType.values().length];
    private final String symbol;

    private final StringBuilder sb = new StringBuilder();

    private boolean valid = false;
    /*
     * TODAY Long for default equity trading
     */
    private long workingLong = 0;

    /*
     * OVERNIGHT Short for default equity trading
     */
    private long workingShort = 0;

    /*
     * TODAY Long for default equity trading
     */
    private long positionLong = 0;

    /*
     * OVERNIGHT Short for default equity trading
     */
    private long positionShort = 0;

    PositionInfo(final String symbol) {
        this.symbol = symbol;
        for (int i = 0; i < PositionType.values().length; i++) {
            infos[i] = 0;
        }
    }

    public void update(final long qty, final PositionType type) {
        infos[type.getIdx()] = qty;
        valid = true;
    }

    public void onOrderAck(final long orderQty, final Side side) {
        valid = true;
        switch (side) {
            case BUY:
                workingLong += orderQty;
                break;
            case SELL:
                workingShort += orderQty;
                break;
            default:
                break;
        }
    }

    public void onCancelAck(final long cancelQty, final Side side) {
        valid = true;
        switch (side) {
            case BUY:
                workingLong -= cancelQty;
                if (workingLong < 0) {
                    workingLong = 0;
                }
                break;
            case SELL:
                workingShort -= cancelQty;
                if (workingShort < 0) {
                    workingShort = 0;
                }
                break;
            default:
                break;
        }
    }

    public void onFill(final long fillQty, final Side side) {
        valid = true;
        switch (side) {
            case BUY:
                positionLong += fillQty;
                workingLong -= fillQty;
                if (workingLong < 0) {
                    workingLong = 0;
                }
                break;
            case SELL:
                positionShort += fillQty;
                workingShort -= fillQty;
                if (workingShort < 0) {
                    workingShort = 0;
                }
                break;
            default:
                break;
        }
    }

    public String getSymbol() {
        return symbol;
    }

    public long getQty(final PositionType type) {
        return infos[type.getIdx()];
    }

    /*
     * Return number of available positions from overnight. should be a static value each trading day
     */
    long getOvernightAvailable() {
        return infos[PositionType.OVERNIGHT_LONG.getIdx()] + infos[PositionType.CLOSED_OVERNIGHT_LONG.getIdx()];
    }

    /*
     * Return number of available positions today. might be change in the same trading day
     */
    long getTodayAvailable() {
        return infos[PositionType.LONG.getIdx()] + infos[PositionType.CLOSED_LONG.getIdx()];
    }

    long getOvernightPosition() {
        return infos[PositionType.CLOSED_OVERNIGHT_LONG.getIdx()] - infos[PositionType.CLOSED_OVERNIGHT_SHORT.getIdx()] + positionShort;
    }

    long getTodayPosition() {
        return infos[PositionType.CLOSED_LONG.getIdx()] - infos[PositionType.CLOSED_SHORT.getIdx()] + positionLong;
    }

    long getOvernightWorking() {
        return workingShort;
    }

    long getTodayWorking() {
        return workingLong;
    }

    public boolean isValid() {
        return valid;
    }

    @Override
    public String toString() {
        sb.setLength(0);
        sb.append("\n").append(symbol).append("\n");
        for (int i = 0; i < infos.length; i++) {
            sb.append(PositionType.fromIdx(i).name()).append(":").append(infos[i]).append("\n");
        }
        return sb.toString();
    }

}
