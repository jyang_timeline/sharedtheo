package com.timelinecapital.core.stats;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.strategy.StrategyUpdater;
import com.timelinecapital.commons.binary.PositionResponseDecoder;
import com.timelinecapital.commons.type.PositionInfoType;
import com.timelinecapital.commons.type.SymbolPositionInfo;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.trade.TradingAccount;
import com.timelinecapital.core.types.StrategyEventType;
import com.timelinecapital.strategy.types.PositionType;

public class PositionSummary implements PositionImport {
    private static final Logger log = LogManager.getLogger(PositionSummary.class);

    private final Map<String, PositionInfo> positionCaches = new HashMap<>();
    private final Map<String, ExecReportEvent> infoUpdaters = new HashMap<>();

    private final SymbolPositionInfo translatedInfo;

    PositionSummary(final TradingAccount account, final int engineId) {
        translatedInfo = new SymbolPositionInfo(engineId, account.getAccountName());
    }

    private void updateCoreInstruments(final String symbol, final PositionInfo info) {
        if (infoUpdaters.containsKey(symbol)) {
            infoUpdaters.get(symbol).onStaticPositionInfo(info);
        }
    }

    @Override
    public void registerWith(final String symbol, final ExecReportEvent updater) {
        infoUpdaters.put(symbol, updater);
    }

    @Override
    public void checkStaticPositions(final String symbol) {
        infoUpdaters.get(symbol).onStaticPositionInfo(get(symbol));
    }

    @Override
    public PositionInfo get(final String symbol) {
        return positionCaches.computeIfAbsent(symbol, key -> new PositionInfo(key));
    }

    @Override
    public void update(final String symbol, final PositionInfo info) {
        positionCaches.put(symbol, info);
        updateCoreInstruments(symbol, info);
    }

    @Override
    public void updateFrom(final PositionResponseDecoder decoder) {
        final String symbol = decoder.symbol();
        final PositionInfo info = positionCaches.computeIfAbsent(symbol, key -> new PositionInfo(key));
        info.update(decoder.tdLongQty(), PositionType.LONG);
        info.update(decoder.tdShortQty(), PositionType.SHORT);
        info.update(decoder.tdLongQtyClz(), PositionType.CLOSED_LONG);
        info.update(decoder.tdShortQtyClz(), PositionType.CLOSED_SHORT);
        info.update(decoder.ydLongQty(), PositionType.OVERNIGHT_LONG);
        info.update(decoder.ydShortQty(), PositionType.OVERNIGHT_SHORT);
        info.update(decoder.ydLongQtyClz(), PositionType.CLOSED_OVERNIGHT_LONG);
        info.update(decoder.ydShortQtyClz(), PositionType.CLOSED_OVERNIGHT_SHORT);
        updateCoreInstruments(symbol, info);
        log.debug("[SYNC] upate PositionInfo: {} ", info.toString());
    }

    @Override
    public void sink() {
        CoreController.getInstance().getIdToEventManager().forEach((id, eventManager) -> {
            final StrategyUpdater updater = CoreController.getInstance().getIdToUpdater().get(id);
            if (updater != null) {
                updater.getContracts().forEach(c -> {
                    if (positionCaches.containsKey(c.getSymbol())) {
                        eventManager.publish(StrategyEventType.PositionSync, c);
                    }
                });
            } else {
                log.warn("[NO_SYNC] updater does not exisit: {}, skipped", id);
            }
        });

    }

    public SymbolPositionInfo translateTo() {
        positionCaches.entrySet().stream().filter(entry -> entry.getValue().isValid()).forEach(entry -> {
            translatedInfo.setPositionInfo(entry.getKey(), PositionInfoType.OVERNIGHT,
                entry.getValue().getOvernightPosition(), entry.getValue().getOvernightAvailable(), entry.getValue().getOvernightWorking());
            translatedInfo.setPositionInfo(entry.getKey(), PositionInfoType.TODAY,
                entry.getValue().getTodayPosition(), entry.getValue().getTodayAvailable(), entry.getValue().getTodayWorking());
        });
        return translatedInfo;
    }

}
