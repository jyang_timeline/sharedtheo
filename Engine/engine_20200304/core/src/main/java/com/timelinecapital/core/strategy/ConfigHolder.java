package com.timelinecapital.core.strategy;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.protocol.StrategyConfigProtocol.Type;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.InvalidConfigurationException;
import com.nogle.strategy.types.Config;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;

public class ConfigHolder implements Config {
    private static final Logger log = LogManager.getLogger(ConfigHolder.class);

    private final Map<String, Integer> keyToInteger;
    private final Map<String, Double> keyToDouble;
    private final Map<String, String> keyToString;
    private final Map<String, Boolean> keyToBoolean;
    private final Map<String, String> keyToContract;
    private final Map<String, Long> keyToOvernightPositions;
    private final Map<String, String> keyToAdditionalData;
    private final Map<String, Interval> keyToSchedule;
    private final Map<String, String> keyValue;

    public ConfigHolder() {
        keyToInteger = new HashMap<>();
        keyToDouble = new HashMap<>();
        keyToString = new HashMap<>();
        keyToBoolean = new HashMap<>();
        keyToContract = new HashMap<>();
        keyToOvernightPositions = new HashMap<>();
        keyToAdditionalData = new HashMap<>();
        keyToSchedule = new HashMap<>();
        keyValue = new HashMap<>();
    }

    @Override
    public boolean containKeys(final String... keys) {
        final Set<String> propertyKeys = keyValue.keySet();
        for (final String key : keys) {
            if (!propertyKeys.contains(key)) {
                return false;
            }

            if (keyValue.get(key).isEmpty()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Set<String> getKeys() {
        return keyValue.keySet();
    }

    @Override
    public Integer getInteger(final String key) {
        return keyToInteger.get(key);
    }

    @Override
    public Integer getInteger(final String key, final Integer defaultValue) {
        if (keyToInteger.containsKey(key)) {
            return keyToInteger.get(key);
        }
        keyToInteger.put(key, defaultValue);
        return defaultValue;
    }

    @Override
    public Double getDouble(final String key) {
        return keyToDouble.get(key);
    }

    @Override
    public Double getDouble(final String key, final Double defaultValue) {
        if (keyToDouble.containsKey(key)) {
            return keyToDouble.get(key);
        }
        keyToDouble.put(key, defaultValue);
        return defaultValue;
    }

    @Override
    public String getString(final String key) {
        return keyToString.get(key);
    }

    @Override
    public String getString(final String key, final String defaultValue) {
        if (keyToString.containsKey(key)) {
            return keyToString.get(key);
        }
        keyToString.put(key, defaultValue);
        return defaultValue;
    }

    @Override
    public Boolean getBoolean(final String key) {
        return keyToBoolean.get(key);
    }

    @Override
    public Boolean getBoolean(final String key, final Boolean defaultValue) {
        if (keyToBoolean.containsKey(key)) {
            return keyToBoolean.get(key);
        }
        keyToBoolean.put(key, defaultValue);
        return defaultValue;
    }

    public Collection<String> getContracts() {
        return keyToContract.values();
    }

    public Map<String, String> getContractMap() {
        return keyToContract;
    }

    public Map<String, Long> getOvernightPositions() {
        return keyToOvernightPositions;
    }

    public Map<String, String> getAdditionalData() {
        return keyToAdditionalData;
    }

    public Collection<Interval> getSchedules() {
        return keyToSchedule.values();
    }

    public Collection<String> getSharedTheos() {
        return keyToString.entrySet().stream()
            .filter(entry -> entry.getKey().startsWith("SharedTheo."))
            .map(Map.Entry::getValue)
            .collect(Collectors.toSet());
    }

    public void clear() {
        keyToInteger.clear();
        keyToDouble.clear();
        keyToString.clear();
        keyToBoolean.clear();
        keyToContract.clear();
        keyToOvernightPositions.clear();
        keyToAdditionalData.clear();
        keyToSchedule.clear();
        keyValue.clear();
    }

    public void importKeyValue(final String key, final String value) {
        String[] times = null;
        keyValue.put(key, value);

        try {
            if (StrategyConfigProtocol.Key.isType(key, Type.Integer)) {
                keyToInteger.put(StrategyConfigProtocol.Key.untypedKey(key), Integer.parseInt(value));
            } else if (StrategyConfigProtocol.Key.isType(key, Type.Double)) {
                keyToDouble.put(StrategyConfigProtocol.Key.untypedKey(key), Double.parseDouble(value));
            } else if (StrategyConfigProtocol.Key.isType(key, Type.Boolean)) {
                keyToBoolean.put(StrategyConfigProtocol.Key.untypedKey(key), Boolean.parseBoolean(value));
            } else if (StrategyConfigProtocol.Key.isType(key, Type.Contract)) {
                keyToContract.put(StrategyConfigProtocol.Key.untypedKey(key), value);
            } else if (StrategyConfigProtocol.Key.isType(key, Type.OvernightPositions)) {
                keyToOvernightPositions.put(StrategyConfigProtocol.Key.untypedKey(key), Long.parseLong(value));
            } else if (StrategyConfigProtocol.Key.isType(key, Type.AdditionalData)) {
                keyToAdditionalData.put(StrategyConfigProtocol.Key.untypedKey(key), value);
            } else if (StrategyConfigProtocol.Key.isType(key, Type.Schedule)) {
                times = value.split("-");

                final DateTime start = NogleTimeFormatter.ISO_TIME_FORMAT.parseDateTime(times[0]);
                DateTime stop = NogleTimeFormatter.ISO_TIME_FORMAT.parseDateTime(times[1]);
                if (start.compareTo(stop) > 0) {
                    stop = stop.plusDays(1);
                }

                final DateTime switchpoint = EngineConfig.getTradingDaySwitchPoint();
                if (start.isAfter(switchpoint) && stop.isAfter(switchpoint)) {
                    if (stop.isAfter(switchpoint.plusDays(1))) {
                        stop = EngineConfig.getTradingDaySwitchPoint().plusDays(1);
                    }
                }
                if (start.isBefore(switchpoint) && stop.isAfter(switchpoint)) {
                    stop = EngineConfig.getTradingDaySwitchPoint();
                }

                // TOOD #issue: schedule was skipped
                if (EngineConfig.getEngineMode().equals(EngineMode.SIMULATION) && !SimulationConfig.getSimSession().isEmpty()) {
                    final String[] simSessions = SimulationConfig.getSimSession().split(DelimiterUtil.SESSION_DELIMITER);
                    for (final String eachSession : simSessions) {
                        if (key.contains(eachSession)) {
                            keyToSchedule.put(StrategyConfigProtocol.Key.untypedKey(key), new Interval(start, stop));
                        }
                    }
                } else {
                    keyToSchedule.put(StrategyConfigProtocol.Key.untypedKey(key), new Interval(start, stop));
                }
            } else if (StrategyConfigProtocol.Key.isType(key, Type.String)) {
                keyToString.put(StrategyConfigProtocol.Key.untypedKey(key), value);
            } else {
                keyToString.put(key, value);
            }
        } catch (final NumberFormatException ne) {
            log.warn("key=" + key + " value=" + value + "import error: {}", ne.getMessage());
        } catch (final Exception e) {
            log.warn("key=" + key + " value=" + value + "import error.", e);
        }
    }

    public void check() throws InvalidConfigurationException {
        for (final StrategyConfigProtocol.Key key : StrategyConfigProtocol.Key.values()) {
            if (key.isNecessary()) {
                if (!keyValue.containsKey(key.toTypedString())) {
                    throw new InvalidConfigurationException("Lack of necessary key: " + key.toTypedString());
                }
            }
        }

        if (keyToContract.isEmpty()) {
            throw new InvalidConfigurationException("Lack of contract.");
        }

        if (keyToSchedule.isEmpty()) {
            throw new InvalidConfigurationException("Lack of schedule.");
        }

        for (final Interval theInterval : keyToSchedule.values()) {
            for (final Interval interval : keyToSchedule.values()) {
                if (theInterval.overlaps(interval) && !theInterval.isEqual(interval)) {
                    final DateTime theStart = theInterval.getStart();
                    final DateTime theEnd = theInterval.getEnd();
                    final DateTime start = interval.getStart();
                    final DateTime end = interval.getEnd();

                    throw new InvalidConfigurationException(
                        "Overlap of schedules: " + theStart.getHourOfDay() + ":" + theStart.getMinuteOfHour() + ":" + theStart.getSecondOfMinute() + "-" + theEnd.getHourOfDay()
                            + ":" + theEnd.getMinuteOfHour() + ":" + theEnd.getSecondOfMinute() + " with " + start.getHourOfDay() + ":" + start.getMinuteOfHour() + ":"
                            + start.getSecondOfMinute() + "-" + end.getHourOfDay() + ":" + end.getMinuteOfHour() + ":" + end.getSecondOfMinute());
                }
            }
        }
    }
}
