package com.timelinecapital.core.strategy;

import com.nogle.strategy.types.Config;

public interface ConfigListener {

    void checkConfig(Config config) throws Exception;

    void onConfigChange(Config config);

}

