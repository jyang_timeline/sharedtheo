package com.timelinecapital.core.strategy;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.Interval;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.commons.utils.json.HFTJsonObject;
import com.nogle.core.exception.InvalidConfigurationException;
import com.nogle.core.types.OrderExecutionMode;
import com.nogle.strategy.types.Config;
import com.nogle.util.Swapper;

public class StrategyConfig implements Config, UpdatableConfig {
    private static final Logger log = LogManager.getLogger(StrategyConfig.class);

    private static final String DEFAULT_NAME_PREFIX = "Unknown";

    private final Set<ConfigListener> configListeners = new HashSet<>();
    private final Swapper<ConfigHolder> configHolders;
    private final ClassLoader classLoader;
    private final String account;
    private final String unknownName;

    public StrategyConfig(final ClassLoader classLoader, final String account, final String configs) throws InvalidConfigurationException {
        this(classLoader, account);
        final InputStream inputStream = new ByteArrayInputStream(configs.getBytes(StandardCharsets.UTF_8));
        loadProperties(inputStream);
    }

    public StrategyConfig(final ClassLoader classLoader, final String account, final HFTJsonObject jsonObj) throws InvalidConfigurationException {
        this(classLoader, account);
        loadJSONMap(jsonObj);
    }

    private StrategyConfig(final ClassLoader classLoader, final String account) {
        this.classLoader = classLoader;
        this.account = account;
        this.unknownName = DEFAULT_NAME_PREFIX + account;
        this.configHolders = new Swapper<>(new ConfigHolder(), new ConfigHolder());
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public String getAccount() {
        return account;
    }

    @Override
    public boolean containKeys(final String... keys) {
        return configHolders.getCurrent().containKeys(keys);
    }

    @Override
    public Integer getInteger(final String key) {
        return configHolders.getCurrent().getInteger(key);
    }

    @Override
    public Integer getInteger(final String key, final Integer defaultValue) {
        return configHolders.getCurrent().getInteger(key, defaultValue);
    }

    @Override
    public Double getDouble(final String key) {
        return configHolders.getCurrent().getDouble(key);
    }

    @Override
    public Double getDouble(final String key, final Double defaultValue) {
        return configHolders.getCurrent().getDouble(key, defaultValue);
    }

    @Override
    public String getString(final String key) {
        return configHolders.getCurrent().getString(key);
    }

    @Override
    public String getString(final String key, final String defaultValue) {
        return configHolders.getCurrent().getString(key, defaultValue);
    }

    @Override
    public Boolean getBoolean(final String key) {
        return configHolders.getCurrent().getBoolean(key);
    }

    @Override
    public Boolean getBoolean(final String key, final Boolean defaultValue) {
        return configHolders.getCurrent().getBoolean(key, defaultValue);
    }

    @Override
    public Set<String> getKeys() {
        return configHolders.getCurrent().getKeys();
    }

    @Override
    public void addConfigListener(final int strategyId, final ConfigListener configListener) {
        configListeners.add(configListener);
    }

    @Override
    public void removeConfigListener(final int strategyId) {
        configListeners.clear();
    }

    /**
     * Return a map of <String, String> to identify contract lookup pair. Ex: Strategy.Self=cu@SHFE
     *
     * @return Map of model symbol key and contract lookup value
     */
    public Map<String, String> getStrategyContracts() {
        return configHolders.getCurrent().getContractMap();
    }

    /**
     * Return a map of <String, Long> to identify overnight positions pair. Ex: Strategy.Self=50
     *
     * @return Map of symbol key and symbol's overnight positions
     */
    public Map<String, Long> getOvernightPositions() {
        return configHolders.getCurrent().getOvernightPositions();
    }

    /**
     * Return a map of <String, String> to identify feed subscriber pair. Ex: FeedA=Self,Ref3
     *
     * @return Map of feed type and model symbol key
     */
    public Map<String, String> getAdditionalDataSubscriber() {
        return configHolders.getCurrent().getAdditionalData();
    }

    public Collection<Interval> getSchedules() {
        return configHolders.getCurrent().getSchedules();
    }

    public Collection<String> getSharedTheos() {
        return configHolders.getCurrent().getSharedTheos();
    }

    public String getName() {
        return configHolders.getCurrent().getString(StrategyConfigProtocol.Key.strategyName.toUntypedString(), unknownName);
    }

    public OrderExecutionMode getOrderExecutionMode() {
        if (configHolders.getCurrent().getInteger(StrategyConfigProtocol.Key.strategyOrderEagerMode.toUntypedString()) == null) {
            return OrderExecutionMode.DEFAULT;
        }
        return OrderExecutionMode.fromInt(configHolders.getCurrent().getInteger(StrategyConfigProtocol.Key.strategyOrderEagerMode.toUntypedString()));
    }

    private void checkAndApply() throws Exception {
        final ConfigHolder config = configHolders.getSpare();

        config.check();

        for (final ConfigListener configListener : configListeners) {
            configListener.checkConfig(config);
        }

        configHolders.swap();

        for (final ConfigListener configListener : configListeners) {
            configListener.onConfigChange(config);
        }
    }

    private void load(final Runnable action) throws InvalidConfigurationException {
        try {
            configHolders.getSpare().clear();

            action.run();

            checkAndApply();
            log.info("StrategyConfig loads successfully");

        } catch (final Exception e) {
            e.printStackTrace();
            log.error("Unable to load config.", e);
            throw new InvalidConfigurationException(e);
        }
    }

    private void loadProperties(final InputStream inputStream) throws InvalidConfigurationException {
        final Properties properties = new Properties();

        try {
            properties.load(inputStream);
            if (properties.isEmpty()) {
                throw new InvalidConfigurationException("properties should not be empty");
            }

            log.info("Try to load properties: {}", properties);
            load(() -> properties.forEach((key, value) -> configHolders.getSpare().importKeyValue(String.valueOf(key), String.valueOf(value))));

        } catch (final IOException e) {
            e.printStackTrace();
            log.error("Unable to load file");
        }
    }

    public void loadJSONMap(final HFTJsonObject jsonObj) throws InvalidConfigurationException {
        log.info("try to load JSON: {}", jsonObj.getString(StrategyConfigProtocol.Key.strategyName.toTypedString()));
        log.trace("Properties: {}", jsonObj);
        load(() -> jsonObj.forEachKey(key -> configHolders.getSpare().importKeyValue(key, jsonObj.getString(key))));
    }
}
