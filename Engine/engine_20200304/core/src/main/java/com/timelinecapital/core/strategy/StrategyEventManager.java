package com.timelinecapital.core.strategy;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.EventTask;
import com.timelinecapital.core.event.handler.GenericEventHandler;
import com.timelinecapital.core.event.handler.IdleGenericEventHandler;
import com.timelinecapital.core.types.StrategyEventType;

public class StrategyEventManager {
    private static final Logger log = LogManager.getLogger(StrategyEventManager.class);

    private final Map<StrategyEventType, GenericEventHandler<?>> eventHandlers = new HashMap<>();
    private final EventTask eventTask;

    public StrategyEventManager(final EventTask eventTask) {
        this.eventTask = eventTask;
    }

    public void register(final StrategyEventType eventType, final GenericEventHandler<?> eventHandler) {
        eventHandlers.put(eventType, eventHandler);
        log.info("Strategy event {} - {} registered", eventType, eventHandler.getName());
    }

    @SuppressWarnings("unchecked")
    public <T> void publish(final StrategyEventType eventType, final T event) {
        final GenericEventHandler<T> eventHandler = (GenericEventHandler<T>) eventHandlers.getOrDefault(eventType, IdleGenericEventHandler.get());
        eventHandler.wrap(event);
        eventTask.invoke(eventHandler);
    }

}
