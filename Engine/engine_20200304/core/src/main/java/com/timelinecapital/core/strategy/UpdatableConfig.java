package com.timelinecapital.core.strategy;

public interface UpdatableConfig {

    public void addConfigListener(int strategyId, final ConfigListener configListener);

    public void removeConfigListener(int strategyId);

    // public Collection<String> getContracts();

}
