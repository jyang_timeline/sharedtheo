package com.timelinecapital.core.trade;

import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.Handler;
import com.nogle.commons.socket.MessageInfo;
import com.nogle.commons.socket.SocketNode;
import com.nogle.commons.socket.exception.NotConnectedException;
import com.nogle.commons.socket.exception.SocketOperationException;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.LatencyProbe;
import com.nogle.core.Probe;
import com.nogle.core.event.IdleTradeEvent;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.TimeUtils;
import com.nogle.core.util.TradeClock;
import com.nogle.util.Util;
import com.nogle.util.parser.AckParser;
import com.nogle.util.parser.FillParser;
import com.nogle.util.parser.PingParser;
import com.nogle.util.parser.RejectParser;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.PingEncoder;
import com.timelinecapital.commons.binary.ReplaceOrderEncoder;
import com.timelinecapital.core.stats.PositionImport;

public abstract class AbstractTradeNode implements TradeConnection {
    private static final Logger log = LogManager.getLogger(AbstractTradeNode.class);

    static final String NEWORDER = "NEW";
    static final String CXLORDER = "CXL";

    final Map<Long, TradeEvent> clOrdIdToEvent = new ConcurrentHashMap<>(65536);
    final SocketNode socketNode;
    final String targetAccount;
    final PositionImport positionImporter;

    private final byte[] replyBuffer = new byte[1024];
    private Interval[] tradingSessions = new Interval[0];

    private final AtomicBoolean signalPing = new AtomicBoolean(false);
    private final PingEncoder pingEncoder = new PingEncoder();

    private final Probe probe = new LatencyProbe();

    public AbstractTradeNode(final SocketNode socketNode, final int timeoutInSecond, final ConnectionProbe connProbe,
        final String targetAccount, final PositionImport positionImporter) {
        this.socketNode = socketNode;
        this.targetAccount = targetAccount;
        this.positionImporter = positionImporter;

        this.socketNode.setReceiveTimeout(timeoutInSecond);
        this.socketNode.setReceiveBuffer(replyBuffer, 0, replyBuffer.length);
        this.socketNode.setHandler(new TradeHandler());
        this.socketNode.setProbe(connProbe);
    }

    public byte[] getBuffer() {
        return replyBuffer;
    }

    @Override
    public final void sendNewOrder(final NewOrderEncoder newOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        try {
            clOrdIdToEvent.put(clOrdId, tradeEvent);
            socketNode.send(newOrder.buffer().byteBuffer());
            probe.showQuoteToTrade(clOrdId, NEWORDER, eventTimeMicros);
        } catch (final NotConnectedException | SocketException | SocketOperationException e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            socketNode.reconnect();
            throw e;
        } catch (final Exception e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public final void sendModifyOrder(final ReplaceOrderEncoder modifyOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
    }

    @Override
    public final void sendOrderCancel(final CancelOrderEncoder cancelOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        try {
            clOrdIdToEvent.put(clOrdId, tradeEvent);
            socketNode.send(cancelOrder.buffer().byteBuffer());
            probe.showQuoteToTrade(clOrdId, CXLORDER, eventTimeMicros);
        } catch (final NotConnectedException | SocketException | SocketOperationException e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            socketNode.reconnect();
            throw e;
        } catch (final Exception e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public final void forceCancel(final long clOrdId) {
        try {
            final CancelOrderEncoder template = new CancelOrderEncoder();
            template.clOrdId(clOrdId);
            socketNode.send(template.buffer().byteBuffer());
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final String getAccount() {
        return targetAccount;
    }

    @Override
    public final void ping() {
        pingEncoder.requestID(DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT).getMillis());
        try {
            if (!socketNode.checkAvailability(pingEncoder.buffer().byteBuffer())) {
                log.error("TradeAgent may not be available @ {}", new Date());
                // TODO revoke the permission of continuing trading ( open->close, close->close )
                socketNode.reconnect();
                return;
            }
            log.info("TradeAgent is ready to go");
        } catch (final InterruptedException e) {
            log.warn("Connectivity monitoring service has been interrupted.");
        }
    }

    @Override
    public void sync() {
        // TODO
    }

    @Override
    public final boolean isConnected() {
        return socketNode.isConnected();
    }

    @Override
    public final void checkConnection() {
        socketNode.onConnectionCheck();
    }

    @Override
    public void checkSocketThread() {
        if (!socketNode.isNodeStarted()) {
            log.warn("TradeAgent is starting SocketNode's thread");
            socketNode.start();
        }
    }

    // public void updateConnection(final String connType, final String addr, final int port, final String device) throws
    // SocketOperationException {
    // socketNode.redirect(addr, port, device);
    // }

    @Override
    public final void updateTradingSessions(final Interval[] newTradingSessions) {
        if (tradingSessions.length == 0 || newTradingSessions.length == 0) {
            tradingSessions = newTradingSessions;
            socketNode.setTradingSessions(tradingSessions);
            return;
        }
        for (int m = 0; m < tradingSessions.length; m++) {
            for (int k = 0; k < newTradingSessions.length; k++) {
                if (tradingSessions[m].overlaps(newTradingSessions[k])) {
                    tradingSessions[m] = TimeUtils.union(tradingSessions[m], newTradingSessions[k]);
                }
            }
        }
        socketNode.setTradingSessions(tradingSessions);
    }

    @Override
    public final Interval[] getTradingSessions() {
        return tradingSessions;
    }

    private class TradeHandler implements Handler {

        private final TradeEvent idleEvent = IdleTradeEvent.getInstance();
        private final AckParser ack = new AckParser(replyBuffer);
        private final FillParser fill = new FillParser(replyBuffer);
        private final RejectParser reject = new RejectParser(replyBuffer);
        private final PingParser ping = new PingParser(replyBuffer);

        private long clOrdId = 0;
        private int length = 0;
        private int offset = 0;

        private TradeHandler() {
        }

        @Override
        public void onChannelReopen() {
            socketNode.reconnect();
        }

        @Override
        public void onData(final MessageInfo info, final ByteBuffer bufferData) {
            length += info.getLength();
            while (true) {
                if (offset >= length) {
                    break;
                }

                clOrdId = 0;
                switch ((char) bufferData.array()[TradeServerProtocol.Ack_HeaderOffset + offset]) {
                    case TradeServerProtocol.Header_OrderAckChar:
                        handleAck();
                        handleEvent().onOrderAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_ModifyAckChar:
                        handleAck();
                        handleEvent().onModAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_CancelAckChar:
                        handleAck();
                        handleEvent().onCancelAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_OrderRejectChar:
                        handleReject();
                        handleEvent().onOrderReject(clOrdId, RejectReason.fromChar(reject.getRejectReason()));
                        break;
                    case TradeServerProtocol.Header_ModifyRejectChar:
                        handleReject();
                        handleEvent().onModReject(clOrdId, RejectReason.fromChar(reject.getRejectReason()));
                        break;
                    case TradeServerProtocol.Header_CancelRejectChar:
                        handleReject();
                        handleEvent().onCancelReject(clOrdId, RejectReason.fromChar(reject.getRejectReason()));
                        break;
                    case TradeServerProtocol.Header_FillChar:
                        handleFill();
                        handleEvent().onFill(clOrdId, fill.getLastQty(), fill.getRemainingQty(), fill.getLastPrice(), fill.getCommission());
                        break;
                    case TradeServerProtocol.Header_PingResponseChar:
                        handlePing();
                        log.info("Receive ping response {}", ping.getUUID());
                        break;
                    default:
                        log.error("Unknown type={}, data-length={}, length={}, offset={}",
                            (char) bufferData.array()[TradeServerProtocol.Ack_HeaderOffset + offset], bufferData.array().length, length, offset);
                        log.warn("Reset length and offset to 0");
                        length = 0;
                        offset = 0;
                        socketNode.setReceiveBuffer(replyBuffer, 0, replyBuffer.length);
                        socketNode.reconnect();
                        break;
                }

                if (clOrdId <= 0) {
                    if (signalPing.get()) {
                        signalPing.set(false);
                        log.warn("On ping response...continue");
                        continue;
                    } else {
                        log.warn("Data not being processed: {} {}", offset, length);
                        break;
                    }
                }
            }

            length -= offset;

            Util.removeFromHead(bufferData.array(), offset, length);
            if (offset != 0) {
                socketNode.setReceiveBuffer(replyBuffer, length, replyBuffer.length - length);
                offset = 0;
            }
        }

        private void handleAck() {
            if (length < TradeServerProtocol.Ack_MsgLength + offset) {
                clOrdId = 0;
                log.warn("Missing Ack of insufficient data {} {}", length, offset);
            } else {
                ack.setOffset(offset);
                clOrdId = ack.getClOrdId();
                offset += TradeServerProtocol.Ack_MsgLength;
            }
        }

        private void handleReject() {
            if (length < TradeServerProtocol.Reject_MsgLength + offset) {
                clOrdId = 0;
                log.warn("Missing Reject of insufficient data {} {}", length, offset);
            } else {
                reject.setOffset(offset);
                clOrdId = reject.getClOrdId();
                offset += TradeServerProtocol.Reject_MsgLength;
            }
        }

        private void handleFill() {
            if (length < TradeServerProtocol.Fill_MsgLength + offset) {
                clOrdId = 0;
                log.warn("Missing Fill of insufficient data {} {}", length, offset);
            } else {
                fill.setOffset(offset);
                clOrdId = fill.getClOrdId();
                offset += TradeServerProtocol.Fill_MsgLength;
            }
        }

        private void handlePing() {
            if (length < TradeServerProtocol.Ping_MsgLength + offset) {
                return;
            } else {
                ping.setOffset(offset);
                offset += TradeServerProtocol.Ping_MsgLength;
                signalPing.set(true);
            }
        }

        private TradeEvent handleEvent() {
            final TradeEvent event = (clOrdId > 0) ? clOrdIdToEvent.get(clOrdId) : null;
            if (event != null) {
                event.onEventContext();
                return event;
            } else {
                return idleEvent;
            }
        }
    }

}
