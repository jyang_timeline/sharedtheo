package com.timelinecapital.core.trade;

import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.agrona.DirectBuffer;
import org.agrona.concurrent.UnsafeBuffer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.Handler;
import com.nogle.commons.socket.MessageInfo;
import com.nogle.commons.socket.SocketNode;
import com.nogle.commons.socket.exception.NotConnectedException;
import com.nogle.commons.socket.exception.SocketOperationException;
import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.LatencyProbe;
import com.nogle.core.Probe;
import com.nogle.core.event.IdleTradeEvent;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.TimeUtils;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.binary.BooleanType;
import com.timelinecapital.commons.binary.CancelAckDecoder;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.CancelRejectDecoder;
import com.timelinecapital.commons.binary.FillDecoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.OrderAckDecoder;
import com.timelinecapital.commons.binary.OrderRejectDecoder;
import com.timelinecapital.commons.binary.PingDecoder;
import com.timelinecapital.commons.binary.PingEncoder;
import com.timelinecapital.commons.binary.PositionRequestEncoder;
import com.timelinecapital.commons.binary.PositionResponseDecoder;
import com.timelinecapital.commons.binary.ReplaceOrderEncoder;
import com.timelinecapital.core.stats.PositionImport;

abstract class DirectTradeNode implements TradeConnection {
    private static final Logger log = LogManager.getLogger(DirectTradeNode.class);

    static final String NEWORDER = "NEW";
    static final String CXLORDER = "CXL";
    static final int ACTING_VERSION = 0;
    static final int HEADER_OFFSET = 0;

    final Map<Long, TradeEvent> clOrdIdToEvent = new ConcurrentHashMap<>(65536);
    final SocketNode socketNode;
    final TradingAccount targetAccount;

    private Interval[] tradingSessions = new Interval[0];

    private final DirectBuffer replyBuffer = new UnsafeBuffer(ByteBuffer.allocateDirect(2048));

    private final AtomicBoolean signalPing = new AtomicBoolean(false);
    private final AtomicBoolean signalConcat = new AtomicBoolean(false);
    private final AtomicBoolean signalPosSync = new AtomicBoolean(false);
    private final PingEncoder pingEncoder = new PingEncoder();
    private final PositionRequestEncoder syncEncoder = new PositionRequestEncoder();

    private final Probe probe = new LatencyProbe();

    private final PositionImport importer;

    public DirectTradeNode(final SocketNode socketNode, final int timeoutSec, final ConnectionProbe probe, final TradingAccount targetAccount, final PositionImport importer) {
        this.targetAccount = targetAccount;
        this.importer = importer;
        this.socketNode = socketNode;
        this.socketNode.setReceiveTimeout(timeoutSec);
        this.socketNode.setReceiveBuffer(replyBuffer.byteBuffer(), 0, replyBuffer.capacity());
        this.socketNode.setHandler(new UnsafeTradeHandler());
        this.socketNode.setProbe(probe);

        pingEncoder.wrap(new UnsafeBuffer(ByteBuffer.allocate(PingEncoder.BLOCK_LENGTH)), 0);
        syncEncoder.wrap(new UnsafeBuffer(ByteBuffer.allocate(PositionRequestEncoder.BLOCK_LENGTH)), 0);
    }

    @Override
    public final void sendNewOrder(final NewOrderEncoder newOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        try {
            clOrdIdToEvent.put(clOrdId, tradeEvent);
            socketNode.send(newOrder.buffer().byteBuffer());
            probe.showQuoteToTrade(clOrdId, NEWORDER, eventTimeMicros);
        } catch (final NotConnectedException | SocketException | SocketOperationException e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            socketNode.reconnect();
            throw e;
        } catch (final Exception e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public final void sendModifyOrder(final ReplaceOrderEncoder modifyOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
    }

    @Override
    public final void sendOrderCancel(final CancelOrderEncoder cancelOrder, final long clOrdId, final TradeEvent tradeEvent, final long eventTimeMicros) throws Exception {
        try {
            clOrdIdToEvent.put(clOrdId, tradeEvent);
            socketNode.send(cancelOrder.buffer().byteBuffer());
            probe.showQuoteToTrade(clOrdId, CXLORDER, eventTimeMicros);
        } catch (final NotConnectedException | SocketException | SocketOperationException e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            socketNode.reconnect();
            throw e;
        } catch (final Exception e) {
            clOrdIdToEvent.remove(clOrdId);
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public final void forceCancel(final long clOrdId) {
        try {
            final CancelOrderEncoder template = new CancelOrderEncoder();
            template.clOrdId(clOrdId);
            socketNode.send(template.buffer().byteBuffer());
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final String getAccount() {
        return targetAccount.getAccountName();
    }

    @Override
    public final void ping() {
        pingEncoder.header((byte) TradeServerProtocol.Header_PingChar);
        pingEncoder.requestID(DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT).getMillis());
        try {
            if (!socketNode.checkAvailability(pingEncoder.buffer().byteBuffer())) {
                log.error("TradeAgent may not be available @ {}", new Date());
                // TODO revoke the permission of continuing trading ( open->close, close->close )
                socketNode.reconnect();
                return;
            }
            log.info("TradeAgent is ready to go");
        } catch (final InterruptedException e) {
            log.warn("Connectivity monitoring service has been interrupted.");
        }
    }

    @Override
    public final void sync() {
        syncEncoder.header((byte) TradeServerProtocol.Header_PositionRequestChar);
        syncEncoder.requestID(UUID.randomUUID().getMostSignificantBits());
        try {
            signalPosSync.set(true);
            socketNode.send(syncEncoder.buffer().byteBuffer());
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final boolean isConnected() {
        return socketNode.isConnected();
    }

    @Override
    public final void checkConnection() {
        socketNode.onConnectionCheck();
    }

    @Override
    public void checkSocketThread() {
        if (!socketNode.isNodeStarted()) {
            log.warn("TradeAgent is starting SocketNode's thread");
            socketNode.start();
        }
    }

    @Override
    public final void updateTradingSessions(final Interval[] newTradingSessions) {
        if (tradingSessions.length == 0 || newTradingSessions.length == 0) {
            tradingSessions = newTradingSessions;
            socketNode.setTradingSessions(tradingSessions);
            return;
        }
        for (int m = 0; m < tradingSessions.length; m++) {
            for (int k = 0; k < newTradingSessions.length; k++) {
                if (tradingSessions[m].overlaps(newTradingSessions[k])) {
                    tradingSessions[m] = TimeUtils.union(tradingSessions[m], newTradingSessions[k]);
                }
            }
        }
        socketNode.setTradingSessions(tradingSessions);
    }

    @Override
    public final Interval[] getTradingSessions() {
        return tradingSessions;
    }

    private class UnsafeTradeHandler implements Handler {

        private final TradeEvent idleEvent = IdleTradeEvent.getInstance();
        private final OrderAckDecoder oAck = new OrderAckDecoder();
        private final CancelAckDecoder xAck = new CancelAckDecoder();
        private final FillDecoder fill = new FillDecoder();
        private final OrderRejectDecoder oRej = new OrderRejectDecoder();
        private final CancelRejectDecoder xRej = new CancelRejectDecoder();
        private final PingDecoder ping = new PingDecoder();
        private final PositionResponseDecoder pos = new PositionResponseDecoder();

        private long clOrdId = 0;
        private int length = 0;
        private int offset = 0;

        private ByteBuffer temp;

        private UnsafeTradeHandler() {
        }

        @Override
        public void onChannelReopen() {
            socketNode.reconnect();
        }

        @Override
        public void onData(final MessageInfo info, final ByteBuffer data) {
            length += info.getLength();
            while (true) {
                if (offset >= length) {
                    break;
                }

                clOrdId = 0;
                switch ((char) data.get(HEADER_OFFSET + offset)) {
                    case TradeServerProtocol.Header_OrderAckChar:
                        handleOAck();
                        handleEvent().onOrderAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_ModifyAckChar:
                        handleOAck();
                        handleEvent().onModAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_CancelAckChar:
                        handleXAck();
                        handleEvent().onCancelAck(clOrdId);
                        break;
                    case TradeServerProtocol.Header_OrderRejectChar:
                        handleOReject();
                        handleEvent().onOrderReject(clOrdId, RejectReason.fromChar((char) oRej.rejectReason()));
                        break;
                    case TradeServerProtocol.Header_ModifyRejectChar:
                        handleOReject();
                        handleEvent().onModReject(clOrdId, RejectReason.fromChar((char) oRej.rejectReason()));
                        break;
                    case TradeServerProtocol.Header_CancelRejectChar:
                        handleXReject();
                        handleEvent().onCancelReject(clOrdId, RejectReason.fromChar((char) xRej.rejectReason()));
                        break;
                    case TradeServerProtocol.Header_FillChar:
                        handleFill();
                        handleEvent().onFill(clOrdId, fill.fillQty(), fill.remainQty(), fill.fillPrice(), fill.commission());
                        break;
                    case TradeServerProtocol.Header_PingResponseChar:
                        handlePing();
                        log.info("Receive ping response {} {}", ping.requestID(), TradeClock.getCurrentMicros());
                        break;
                    case TradeServerProtocol.Header_PositionResponseChar:
                        handlePositionRenew();
                        handleSync();
                        break;
                    default:
                        log.warn("Unknown type={}, data-length={}, length={}, offset={}",
                            data.get(HEADER_OFFSET + offset), data.limit(), length, offset);
                        log.warn("Reset length and offset to 0");
                        length = 0;
                        offset = 0;
                        replyBuffer.byteBuffer().rewind();
                        socketNode.setReceiveBuffer(replyBuffer.byteBuffer(), 0, replyBuffer.capacity());
                        socketNode.reconnect();
                        break;
                }

                if (clOrdId <= 0) {
                    if (signalPing.get()) {
                        signalPing.set(false);
                        log.debug("On ping response...continue");
                        continue;
                    } else if (signalConcat.get()) {
                        signalConcat.set(false);
                        log.debug("Data concat from: offset={} length={}", offset, length);
                        temp = ByteBuffer.allocate(length - offset);
                        data.position(offset).get(temp.array(), 0, length - offset);
                        resetWith(temp);
                        return;
                    } else if (signalPosSync.get()) {
                        continue;
                    } else {
                        log.warn("Data not being processed: offset={} length={}", offset, length);
                    }
                }
            }

            reset();
        }

        private void resetWith(final ByteBuffer data) {
            replyBuffer.byteBuffer().rewind();
            replyBuffer.byteBuffer().put(data);
            length -= offset;
            if (offset != 0) {
                socketNode.setReceiveBuffer(replyBuffer.byteBuffer(), length, replyBuffer.capacity() - length);
                offset = 0;
            }
        }

        private void reset() {
            replyBuffer.byteBuffer().rewind();
            length -= offset;
            if (offset != 0) {
                socketNode.setReceiveBuffer(replyBuffer.byteBuffer(), length, replyBuffer.capacity() - length);
                offset = 0;
            }
        }

        private void handleOAck() {
            if (length < OrderAckDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
                log.warn("Missing Ack of insufficient data {} {}", length, offset);
            } else {
                oAck.wrap(replyBuffer, offset, OrderAckDecoder.BLOCK_LENGTH, ACTING_VERSION);
                clOrdId = oAck.clOrdId();
                offset = oAck.limit();
            }
        }

        private void handleXAck() {
            if (length < CancelAckDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
                log.warn("Missing Ack of insufficient data {} {}", length, offset);
            } else {
                xAck.wrap(replyBuffer, offset, CancelAckDecoder.BLOCK_LENGTH, ACTING_VERSION);
                clOrdId = xAck.clOrdId();
                offset = xAck.limit();
            }
        }

        private void handleOReject() {
            if (length < OrderRejectDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
                log.warn("Missing Reject of insufficient data {} {}", length, offset);
            } else {
                oRej.wrap(replyBuffer, offset, OrderRejectDecoder.BLOCK_LENGTH, ACTING_VERSION);
                clOrdId = oRej.clOrdId();
                offset = oRej.limit();
            }
        }

        private void handleXReject() {
            if (length < CancelRejectDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
                log.warn("Missing Reject of insufficient data {} {}", length, offset);
            } else {
                xRej.wrap(replyBuffer, offset, CancelRejectDecoder.BLOCK_LENGTH, ACTING_VERSION);
                clOrdId = xRej.clOrdId();
                offset = xRej.limit();
            }
        }

        private void handleFill() {
            if (length < FillDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
                log.warn("Missing Fill of insufficient data {} {}", length, offset);
            } else {
                fill.wrap(replyBuffer, offset, FillDecoder.BLOCK_LENGTH, ACTING_VERSION);
                clOrdId = fill.clOrdId();
                offset = fill.limit();
            }
        }

        private void handlePing() {
            if (length < PingDecoder.BLOCK_LENGTH + offset) {
                return;
            } else {
                ping.wrap(replyBuffer, offset, PingDecoder.BLOCK_LENGTH, ACTING_VERSION);
                offset = ping.limit();
                signalPing.set(true);
            }
        }

        private void handlePositionRenew() {
            if (length < PositionResponseDecoder.BLOCK_LENGTH + offset) {
                clOrdId = 0;
                signalConcat.set(true);
            } else {
                pos.wrap(replyBuffer, offset, PositionResponseDecoder.BLOCK_LENGTH, ACTING_VERSION);
                offset = pos.limit();
            }
        }

        private TradeEvent handleEvent() {
            return clOrdIdToEvent.getOrDefault(clOrdId, idleEvent);
        }

        private void handleSync() {
            if (signalConcat.get()) {
                return;
            }

            final BooleanType isLast = pos.isLast();
            signalPosSync.set(BooleanType.FIX_FALSE == isLast);
            if (BooleanType.FIX_TRUE == isLast) {
                importer.sink();
                return;
            }

            final String account = pos.account();
            if (!targetAccount.getAccountName().contentEquals(account)) {
                log.trace("{} does not match {}", targetAccount, account);
                // TODO probably need to return when account does not match
            }
            importer.updateFrom(pos);
            signalPosSync.set(true);
        }

    }

}
