package com.timelinecapital.core.trade;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.TimeoutException;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.SocketNode;
import com.nogle.core.LatencyProbe;
import com.nogle.core.Probe;
import com.nogle.core.config.EngineThreadPolicy;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.strategy.event.TradeEventTranslator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.factory.RequestInstance;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.util.AffinityThreadFactory;
import com.timelinecapital.core.util.TaskThreadFactory;

import net.openhft.affinity.AffinityStrategies;

public class ExecutorInterpretNode extends DirectTradeNode {
    private static final Logger log = LogManager.getLogger(DirectTradeNode.class);

    private final AtomicBoolean started = new AtomicBoolean(false);
    private final Disruptor<RequestInstance> trader;
    private final TransactionDispatcher dispatcher;

    @SuppressWarnings("unchecked")
    public ExecutorInterpretNode(final SocketNode node, final int timeoutSec, final ConnectionProbe probe, final TradingAccount account, final PositionImport importer) {
        super(node, timeoutSec, probe, account, importer);

        ThreadFactory threadFactory;
        if (EngineThreadPolicy.DEDICATE.equals(EngineConfig.getSocketPolicy())) {
            threadFactory = new AffinityThreadFactory("RequestSender", false, AffinityStrategies.ANY);
        } else {
            threadFactory = TaskThreadFactory.createThreadFactory("RequestSender", 0);
        }

        trader = new Disruptor<>(
            requestFactory, 128,
            threadFactory,
            ProducerType.MULTI,
            new BusySpinWaitStrategy());

        dispatcher = new TransactionDispatcher();

        trader.handleEventsWith(dispatcher);
        trader.handleExceptionsFor(dispatcher).with(requestFailHandler);
        trader.start();
        started.set(true);
    }

    @Override
    public final void sendNewOrder(final TradeEventTranslator translator, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        trader.publishEvent(translator);
    }

    @Override
    public final void sendOrderCancel(final TradeEventTranslator translator, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        trader.publishEvent(translator);
    }

    @Override
    public final void onPreMarket() {
        try {
            clOrdIdToEvent.clear();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void onRegularMarket() {
        try {
            clOrdIdToEvent.clear();
            socketNode.onRegularConnection();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void start() {
        if (!socketNode.isNodeStarted()) {
            socketNode.start();
            if (!started.get()) {
                trader.start();
                started.set(true);
            }
        }
    }

    @Override
    public final void stop() {
        socketNode.stop();
        try {
            trader.shutdown(10, TimeUnit.SECONDS);
            started.compareAndSet(true, false);
        } catch (final TimeoutException e) {
            log.error("Unable to close RingBuffer: {}", e.getMessage());
        }
    }

    private static final ExceptionHandler<RequestInstance> requestFailHandler = new ExceptionHandler<>() {

        @Override
        public void handleEventException(final Throwable ex, final long sequence, final RequestInstance requestInstance) {
            try {
                if (requestInstance == null) {
                    log.error("ExceptionHandler of Request - execution failed / RequestInst NULL  {} {}", ex.getMessage(), ex);
                    return;
                }
                if (requestInstance.getTradeEvent() == null) {
                    log.error("ExceptionHandler of Request - execution failed / TradeEvent NULL {} {}", ex.getMessage(), ex);
                    return;
                }
                requestInstance.getTradeEvent().onEventContext().onRequestFail(requestInstance.getClOrderId(), requestInstance.getRequestType());
                log.error("ExceptionHandler of Request - execution failed {} {}", ex.getMessage(), ex);
            } catch (final Exception e) {
                log.error("ExceptionHandler of Request - execution failed {} {}", e.getMessage(), e);
            }
        }

        @Override
        public void handleOnStartException(final Throwable ex) {
            log.error("ExceptionHandler of Request - onStart {} {}", ex.getMessage(), ex);
        }

        @Override
        public void handleOnShutdownException(final Throwable ex) {
            log.error("ExceptionHandler of Request - onShutdown {} {}", ex.getMessage(), ex);
        }
    };

    private static final EventFactory<RequestInstance> requestFactory = new EventFactory<>() {
        @Override
        public RequestInstance newInstance() {
            return new RequestInstance();
        }
    };

    private class TransactionDispatcher implements EventHandler<RequestInstance> {
        private static final String NEWORDER = "NEW";
        private static final String CXLORDER = "CXL";
        private final Probe probe = new LatencyProbe();

        TransactionDispatcher() {
        }

        @Override
        public void onEvent(final RequestInstance requestInstance, final long sequence, final boolean endOfBatch) throws Exception {
            requestInstance.onBinaryData();
            final long prepareTime = TradeClock.getCurrentMicrosOnly();
            socketNode.send(requestInstance.getBinary());
            if (ExecRequestType.OrderEntry.equals(requestInstance.getRequestType())) {
                probe.showQuoteToTrade(requestInstance.getClOrderId(), NEWORDER, requestInstance.getSourceEventTime(), requestInstance.getEventTime(),
                    requestInstance.getEnqueuedTime(), prepareTime);
            } else {
                probe.showQuoteToTrade(requestInstance.getClOrderId(), CXLORDER, requestInstance.getSourceEventTime(), requestInstance.getEventTime(),
                    requestInstance.getEnqueuedTime(), prepareTime);
            }
            requestInstance.reset();
        }
    }

}