package com.timelinecapital.core.trade;

import java.util.concurrent.ConcurrentLinkedDeque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.SocketNode;
import com.nogle.core.LatencyProbe;
import com.nogle.core.Probe;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.binary.CancelOrderEncoder;
import com.timelinecapital.commons.binary.NewOrderEncoder;
import com.timelinecapital.commons.binary.TimeInForce;
import com.timelinecapital.core.execution.factory.OrderRequestPoolFactory;
import com.timelinecapital.core.execution.factory.RequestInstance;
import com.timelinecapital.core.stats.PositionImport;

import net.openhft.affinity.AffinityLock;

public class SimpleTradeNode extends AbstractTradeNode {
    private static final Logger log = LogManager.getLogger(AbstractTradeNode.class);

    private final RequestDealer requestDealer;

    public SimpleTradeNode(final SocketNode socketNode, final int timeoutInSecond, final ConnectionProbe connProbe,
        final String targetAccount, final PositionImport positionImporter) {
        super(socketNode, timeoutInSecond, connProbe, targetAccount, positionImporter);
        requestDealer = new RequestDealer();
    }

    @Override
    public final void sendNewOrder(final RequestInstance orderEntryObject, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        requestDealer.add(orderEntryObject);
    }

    @Override
    public final void sendOrderCancel(final RequestInstance orderEntryObject, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        requestDealer.add(orderEntryObject);
    }

    @Override
    public final void onPreMarket() {
        try {
            socketNode.onPreMarketConnection();
            if (!requestDealer.isAlive()) {
                requestDealer.start();
            }
            clOrdIdToEvent.clear();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void onRegularMarket() {
        try {
            clOrdIdToEvent.clear();
            socketNode.onRegularConnection();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void start() {
        if (!socketNode.isNodeStarted()) {
            socketNode.start();
        }
        if (!requestDealer.isAlive()) {
            requestDealer.start();
        }
    }

    @Override
    public final void stop() {
        socketNode.stop();
        if (requestDealer.isAlive()) {
            requestDealer.stop = true;
        }
    }

    @Override
    public final void checkSocketThread() {
        super.checkSocketThread();
        if (!requestDealer.isAlive()) {
            log.warn("[CheckSocket] TradeAgent is starting RequestDealer's thread");
            requestDealer.start();
        }
    }

    private class RequestDealer extends Thread {

        private final ConcurrentLinkedDeque<RequestInstance> transactions = new ConcurrentLinkedDeque<>();
        private NewOrderEncoder orderEncoder;
        private CancelOrderEncoder cancelEncoder;

        private final Probe probe = new LatencyProbe();
        private static final String NEWORDER = "NEW";
        private static final String CXLORDER = "CXL";

        boolean stop;

        void add(final RequestInstance request) {
            request.setEnqueuedTime(TradeClock.getCurrentMicrosOnly());
            transactions.add(request);
        }

        @Override
        public void run() {
            Thread.currentThread().setName(RequestDealer.class.getSimpleName());

            log.warn("[TransactThread] ******** START new Thread : {} ********", Thread.currentThread().getName());
            RequestInstance request = null;
            try (AffinityLock lock = AffinityLock.acquireLock()) {
                while (!stop) {
                    try {
                        request = transactions.poll();
                        if (request != null) {
                            switch (request.getRequestType()) {
                                case OrderCancel: {
                                    final long clOrdId = request.getClOrderId();
                                    cancelEncoder = request.getCancelEncoder();
                                    cancelEncoder.clOrdId(clOrdId);
                                    final long prepareTime = TradeClock.getCurrentMicrosOnly();
                                    socketNode.send(cancelEncoder.buffer().byteBuffer());
                                    probe.showQuoteToTrade(clOrdId, CXLORDER, request.getSourceEventTime(), request.getEventTime(), request.getEnqueuedTime(), prepareTime);
                                    OrderRequestPoolFactory.recycleOrderObject(request);
                                    break;
                                }
                                case OrderEntry: {
                                    final long clOrdId = request.getClOrderId();
                                    orderEncoder = request.getEntryEncoder();
                                    orderEncoder.clOrdId(clOrdId);
                                    orderEncoder.side(com.timelinecapital.commons.binary.Side.get(request.getSide().getCode()));
                                    orderEncoder.timeInForce(TimeInForce.get(request.getTimeCondition().getCode()));
                                    orderEncoder.orderQty(request.getQuantity());
                                    orderEncoder.price(request.getPrice());
                                    orderEncoder.positionPolicy(request.getPositionPolicy().getPolicyCode());
                                    orderEncoder.positionMax(request.getPositionMax());
                                    final long prepareTime = TradeClock.getCurrentMicrosOnly();
                                    socketNode.send(orderEncoder.buffer().byteBuffer());
                                    probe.showQuoteToTrade(clOrdId, NEWORDER, request.getSourceEventTime(), request.getEventTime(), request.getEnqueuedTime(), prepareTime);
                                    OrderRequestPoolFactory.recycleOrderObject(request);
                                    break;
                                }
                                default:
                                    log.error("Unknown request: {} {}", request.getRequestType(), request);
                                    break;
                            }
                        }
                    } catch (final Exception e) {
                        clOrdIdToEvent.remove(request.getClOrderId());
                        if (request.getTradeEvent() != null) {
                            request.getTradeEvent().onRequestFail(request.getClOrderId(), request.getRequestType());
                        }
                        log.error("Request error: {} {}", request.getClOrderId(), request.getRequestType());
                        log.error("TransactQueue with exceptions", e);
                    }
                }
            }

        }

    }

}
