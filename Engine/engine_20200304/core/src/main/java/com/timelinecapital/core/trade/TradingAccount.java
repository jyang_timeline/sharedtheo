package com.timelinecapital.core.trade;

public class TradingAccount {

    private final String targetAccount;

    public TradingAccount(final String targetAccount) {
        this.targetAccount = targetAccount;
    }

    public String getAccountName() {
        return targetAccount;
    }

}
