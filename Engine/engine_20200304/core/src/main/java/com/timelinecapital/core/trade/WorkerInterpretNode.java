package com.timelinecapital.core.trade;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.EventTranslatorTwoArg;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.TimeoutException;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.SocketNode;
import com.nogle.core.LatencyProbe;
import com.nogle.core.Probe;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.trade.RequestWrapper;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.types.ExecRequestType;
import com.timelinecapital.core.execution.factory.OrderRequestPoolFactory;
import com.timelinecapital.core.execution.factory.RequestInstance;
import com.timelinecapital.core.stats.PositionImport;
import com.timelinecapital.core.util.AffinityThreadFactory;

import net.openhft.affinity.AffinityStrategies;

public class WorkerInterpretNode extends DirectTradeNode {
    private static final Logger log = LogManager.getLogger(DirectTradeNode.class);

    private final Disruptor<RequestWrapper> trader;
    private final TransactionDispatcher dispatcher;

    @SuppressWarnings("unchecked")
    public WorkerInterpretNode(final SocketNode node, final int timeoutSec, final ConnectionProbe probe, final TradingAccount account, final PositionImport importer) {
        super(node, timeoutSec, probe, account, importer);

        trader = new Disruptor<>(
            requestFactory, 128,
            new AffinityThreadFactory("RequestSender", AffinityStrategies.ANY),
            ProducerType.MULTI,
            new BusySpinWaitStrategy());

        dispatcher = new TransactionDispatcher();

        trader.handleEventsWith(dispatcher);
        trader.handleExceptionsFor(dispatcher).with(requestFailHandler);
        trader.start();
    }

    @Override
    public final void sendNewOrder(final RequestInstance entry, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        trader.getRingBuffer().publishEvent(TRANSLATOR_REQUEST_ENTRY, entry, tradeEvent);
        OrderRequestPoolFactory.recycleOrderObject(entry);
    }

    @Override
    public final void sendOrderCancel(final RequestInstance cancel, final long clOrdId, final TradeEvent tradeEvent) {
        clOrdIdToEvent.put(clOrdId, tradeEvent);
        trader.getRingBuffer().publishEvent(TRANSLATOR_REQUEST_ENTRY, cancel, tradeEvent);
        OrderRequestPoolFactory.recycleOrderObject(cancel);
    }

    @Override
    public final void onPreMarket() {
        try {
            clOrdIdToEvent.clear();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void onRegularMarket() {
        try {
            clOrdIdToEvent.clear();
            socketNode.onRegularConnection();
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public final void start() {
        if (!socketNode.isNodeStarted()) {
            socketNode.start();
        }
    }

    @Override
    public final void stop() {
        socketNode.stop();
        try {
            trader.shutdown(10, TimeUnit.SECONDS);
        } catch (final TimeoutException e) {
            log.error("Unable to close RingBuffer: {}", e.getMessage());
        }
    }

    private static final ExceptionHandler<RequestWrapper> requestFailHandler = new ExceptionHandler<>() {

        @Override
        public void handleEventException(final Throwable ex, final long sequence, final RequestWrapper event) {
            event.getTradeEvent().onRequestFail(event.getClOrdId(), event.getRequestType());
            log.error("ExceptionHandler of Request - execution failed {} {}", ex.getMessage(), ex);
        }

        @Override
        public void handleOnStartException(final Throwable ex) {
            log.error("ExceptionHandler of Request - onStart {} {}", ex.getMessage(), ex);
        }

        @Override
        public void handleOnShutdownException(final Throwable ex) {
            log.error("ExceptionHandler of Request - onShutdown {} {}", ex.getMessage(), ex);
        }
    };

    private static final EventFactory<RequestWrapper> requestFactory = new EventFactory<>() {
        @Override
        public RequestWrapper newInstance() {
            return new RequestWrapper();
        }
    };

    private static final EventTranslatorTwoArg<RequestWrapper, RequestInstance, TradeEvent> TRANSLATOR_REQUEST_ENTRY =
        new EventTranslatorTwoArg<>() {

            @Override
            public void translateTo(final RequestWrapper wrapper, final long sequence, final RequestInstance request, final TradeEvent tradeEvent) {
                wrapper.initFrom(request, tradeEvent);
            }
        };

    private class TransactionDispatcher implements EventHandler<RequestWrapper> {
        private static final String NEWORDER = "NEW";
        private static final String CXLORDER = "CXL";
        private final Probe probe = new LatencyProbe();

        TransactionDispatcher() {
        }

        @Override
        public void onEvent(final RequestWrapper wrapper, final long sequence, final boolean endOfBatch) throws Exception {
            final long prepareTime = TradeClock.getCurrentMicrosOnly();
            socketNode.send(wrapper.getBinary());
            if (wrapper.getRequestType().equals(ExecRequestType.OrderEntry)) {
                probe.showQuoteToTrade(wrapper.getClOrdId(), NEWORDER, wrapper.getSourceTime(), wrapper.getCommitTime(), wrapper.getHandoverTime(), prepareTime);
            } else {
                probe.showQuoteToTrade(wrapper.getClOrdId(), CXLORDER, wrapper.getSourceTime(), wrapper.getCommitTime(), wrapper.getHandoverTime(), prepareTime);
            }
        }
    }

}