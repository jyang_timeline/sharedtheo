package com.timelinecapital.core.trade.simulation;

import java.util.List;

import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalClock;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalOrderBookManager;

class EventHandlerFactory {

    static OrderMatching<?> getEventHandler(
        final Contract contract,
        final DataType dataType,
        final InternalOrderBookManager stateTracker,
        final InternalClock clock,
        final ExchangeEventQueue delayedQueue,
        final FillFactory fillFactory) {

        switch (dataType) {
            case REALTIME_MARKETBOOK:
            case MARKETBOOK: {
                final MDMarketBookEvent<BookView> handler = getMarketBookEventHandler(contract, stateTracker, clock, delayedQueue, fillFactory);
                return handler;
            }
            case TICK: {
                final MDTradeEvent<TickView> handler = getTradeEventHandelr(contract, stateTracker, clock, delayedQueue, fillFactory);
                return handler;
            }
            default:
                break;
        }
        return null;
    }

    static MDTradeEvent<TickView> getTradeEventHandelr(final Contract contract, final InternalOrderBookManager stateTracker, final InternalClock clock,
        final ExchangeEventQueue delayedQueue, final FillFactory fillFactory) {
        final List<DataType> dataTypes = SimulationConfig.getExcludedOrderExecutorMatching(contract.getExchange());
        if (dataTypes.contains(DataType.TICK)) {
            return idleTradeHandler;
        }
        return new TradeEventHandler(stateTracker, clock, delayedQueue, fillFactory, contract.getTickCalculator().getTickSize());
    }

    static MDMarketBookEvent<BookView> getMarketBookEventHandler(final Contract contract, final InternalOrderBookManager stateTracker, final InternalClock clock,
        final ExchangeEventQueue delayedQueue, final FillFactory fillFactory) {
        final List<DataType> dataTypes = SimulationConfig.getExcludedOrderExecutorMatching(contract.getExchange());
        if (dataTypes.contains(DataType.MARKETBOOK) || dataTypes.contains(DataType.REALTIME_MARKETBOOK)) {
            return idleMarketBookHandler;
        }
        return new MarketBookEventHandler(stateTracker, clock, delayedQueue, fillFactory);
    }

    static MDTradeEvent<TickView> idleTradeHandler = new MDTradeEvent<>() {

        public void tryOrderMatching(final long eventTimeMicros, final TickView evet) {
        }

        public void set(final ExchangeMarketBook marketbook) {
        };

        public void update(final long eventTimeMicros, final TickView tickView) {
        };

    };

    static MDMarketBookEvent<BookView> idleMarketBookHandler = new MDMarketBookEvent<>() {

        public void tryOrderMatching(final long eventTimeMicros, final BookView evet) {
        }

        public void set(final ExchangeMarketBook marketbook) {
        };

        public void update(final long eventTimeMicros, final BookView bookView) {
        }

    };

}
