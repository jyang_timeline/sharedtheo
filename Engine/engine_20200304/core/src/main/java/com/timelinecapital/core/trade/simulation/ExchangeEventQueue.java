package com.timelinecapital.core.trade.simulation;

import com.nogle.strategy.types.Fill;
import com.timelinecapital.core.execution.report.Ack;

public interface ExchangeEventQueue {

    void queueDelayedOrderAck(long ackMicros, long clOrdId);

    void queueDelayedModAck(long ackMicros, long clOrdId);

    void queueDelayedCancelAck(long ackMicros, Ack ack);

    void queueFill(long fillMicros, Fill fill);

    void queueDelayedFill(long fillMicros, Fill fill);

    void queueDelayedMissedAck(long ackMicros, Ack ack);

    void queueDelayedIOCFill(long fillMicros, Fill fill);

    void queueDelayedOrderReject(long rejectMicros, long clOrdId);

    void queueDelayedModReject(long rejectMicros, long clOrdId);

    void queueDelayedCancelReject(long rejectMicros, long clOrdId);

}
