package com.timelinecapital.core.trade.simulation;

import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.type.Price;

public interface ExchangeMarketBook {

    Contract getContract();

    Price getAtMarketPrice(Side side);

    Price getAtMarketPrice(Side side, int level);

    long getHittableQty(Side side);

    long getHittableQty(Side side, int level);

    long getAvailableQty(Side side, Price targetPrice);

    long getQty(Side side, Price targetPrice);

    void replaceWith(long exchangeTimeMicros, BookView marketbook);

    void updateFrom(long exchangeTimeMicros, BookView marketbook);

    void updateFrom(long exchangeTimeMicros, TickView tick);

    void onCrossOrderMatching(long fillResetMicros, Side aggressorSide, Price orderPrice, long orderQty);

    void onOrderMatching(long fillResetMicros, Side aggressorSide, Price orderPrice, long orderQty);

    interface Clearable {

        void clear();

    }

}
