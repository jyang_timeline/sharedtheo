package com.timelinecapital.core.trade.simulation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.commons.type.StockPriceCNDouble;

public class ExecutorMarketBookWithDoublePrice implements ExchangeMarketBook {
    private static final Logger log = LogManager.getLogger(ExecutorMarketBookWithDoublePrice.class);

    private static final int defaultDepth = 10;

    private final OrderBookPriceEntry[] bidLevels = new OrderBookPriceEntry[defaultDepth * 2];
    private final OrderBookPriceEntry[] askLevels = new OrderBookPriceEntry[defaultDepth * 2];
    private final Contract contract;

    public ExecutorMarketBookWithDoublePrice(final Contract contract) {
        this.contract = contract;
        init();
    }

    private void init() {
        for (int i = 0; i < defaultDepth * 2; i++) {
            bidLevels[i] = new OrderBookPriceEntry(Side.BUY);
            askLevels[i] = new OrderBookPriceEntry(Side.SELL);
        }
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public Price getAtMarketPrice(final Side side) {
        return this.getAtMarketPrice(side, 0);
    }

    @Override
    public Price getAtMarketPrice(final Side side, final int level) {
        switch (side) {
            case BUY:
                return askLevels[level].getPrice();
            case SELL:
                return bidLevels[level].getPrice();
            default:
                throw new RuntimeException("Invalid type");
        }
    }

    @Override
    public long getHittableQty(final Side side) {
        return this.getHittableQty(side, 0);
    }

    @Override
    public long getHittableQty(final Side side, final int level) {
        switch (side) {
            case BUY:
                return askLevels[level].hittableQty;
            case SELL:
                return bidLevels[level].hittableQty;
            default:
                throw new RuntimeException("Invalid type");
        }
    }

    @Override
    public long getAvailableQty(final Side side, final Price targetPrice) {
        long hittableQty = 0;
        switch (side) {
            case BUY:
                for (int i = 0; i < askLevels.length; i++) {
                    if (Double.compare(askLevels[i].getPrice().asDouble(), targetPrice.asDouble()) <= 0 && askLevels[i].hittableQty > 0) {
                        hittableQty += askLevels[i].hittableQty;
                    }
                }
                return hittableQty;
            case SELL:
                for (int i = 0; i < bidLevels.length; i++) {
                    if (Double.compare(bidLevels[i].getPrice().asDouble(), targetPrice.asDouble()) >= 0 && bidLevels[i].hittableQty > 0) {
                        hittableQty += bidLevels[i].hittableQty;
                    }
                }
                return hittableQty;
            default:
                throw new RuntimeException("Invalid type");
        }
    }

    @Override
    public long getQty(final Side side, final Price targetPrice) {
        switch (side) {
            case BUY:
                for (int i = 0; i < bidLevels.length; i++) {
                    if (bidLevels[i].getQty() > 0 && Double.compare(bidLevels[i].price.asDouble(), targetPrice.asDouble()) == 0) {
                        return bidLevels[i].getQty();
                    }
                }
                return 0;
            case SELL:
                for (int i = 0; i < askLevels.length; i++) {
                    if (askLevels[i].getQty() > 0 && Double.compare(askLevels[i].price.asDouble(), targetPrice.asDouble()) == 0) {
                        return askLevels[i].getQty();
                    }
                }
                return 0;
            default:
                throw new RuntimeException("Invalid type");
        }
    }

    @Override
    public void replaceWith(final long exchangeTimeMicros, final BookView marketbook) {
        if (!bidLevels[0].isValid()) {
            log.warn("Initializing SimMarketBook BID {} {}", contract, marketbook.getBidPrice());
            for (int i = 0; i < marketbook.getBidDepth(); i++) {
                bidLevels[i].price.setDouble(marketbook.getBidPrice());
                bidLevels[i].qty = marketbook.getBidQty();
            }
        } else {
            int m = defaultDepth;
            int n = marketbook.getBidDepth();
            while (m > 0 && n > 0) {
                if (Double.compare(bidLevels[m - 1].price.asDouble(), marketbook.getBidPrice(n - 1)) < 0) {
                    bidLevels[m + n - 1].refreshFromAnotherEntry(bidLevels[m - 1]);
                    m--;
                } else if (Double.compare(bidLevels[m - 1].price.asDouble(), (marketbook.getBidPrice(n - 1))) == 0) {
                    bidLevels[m + n - 1].refreshFromAnotherEntry(bidLevels[m - 1]);
                    bidLevels[m + n - 1].setQty(marketbook.getBidQty(n - 1), exchangeTimeMicros);
                    n--;
                    for (int i = m + n - 1; i > 0; i--) {
                        bidLevels[i].refreshFromAnotherEntry(bidLevels[i - 1]);
                        bidLevels[i - 1].clear();
                    }
                } else {
                    bidLevels[m + n - 1].price.setDouble(marketbook.getBidPrice(n - 1));
                    bidLevels[m + n - 1].setQty(marketbook.getBidQty(n - 1), exchangeTimeMicros);
                    n--;
                }
            }
            while (n > 0) {
                bidLevels[n - 1].price.setDouble(marketbook.getBidPrice(n - 1));
                bidLevels[n - 1].setQty(marketbook.getBidQty(n - 1), exchangeTimeMicros);
                n--;
            }
            final int shift = m;
            while (m > 0 && m < defaultDepth * 2) {
                bidLevels[m - shift].refreshFromAnotherEntry(bidLevels[m]);
                for (int i = m; i > m - shift; i--) {
                    bidLevels[i].clear();
                }
                m++;
            }
        }

        if (!askLevels[0].isValid()) {
            log.warn("Initializing SimMarketBook ASK {} {}", contract, marketbook.getAskPrice());
            for (int i = 0; i < marketbook.getAskDepth(); i++) {
                askLevels[i].price.setDouble(marketbook.getAskPrice());
                askLevels[i].qty = marketbook.getAskQty();
            }
        } else {
            int m = defaultDepth;
            int n = marketbook.getAskDepth();
            while (m > 0 && n > 0) {
                if (Double.compare(askLevels[m - 1].price.asDouble(), marketbook.getAskPrice(n - 1)) > 0) {
                    askLevels[m + n - 1].refreshFromAnotherEntry(askLevels[m - 1]);
                    m--;
                } else if (Double.compare(askLevels[m - 1].price.asDouble(), (marketbook.getAskPrice(n - 1))) == 0) {
                    askLevels[m + n - 1].refreshFromAnotherEntry(askLevels[m - 1]);
                    askLevels[m + n - 1].setQty(marketbook.getAskQty(n - 1), exchangeTimeMicros);
                    n--;
                    for (int i = m + n - 1; i > 0; i--) {
                        askLevels[i].refreshFromAnotherEntry(askLevels[i - 1]);
                        askLevels[i - 1].clear();
                    }
                } else {
                    askLevels[m + n - 1].price.setDouble(marketbook.getAskPrice(n - 1));
                    askLevels[m + n - 1].setQty(marketbook.getAskQty(n - 1), exchangeTimeMicros);
                    n--;
                }
            }
            while (n > 0) {
                askLevels[n - 1].price.setDouble(marketbook.getAskPrice(n - 1));
                askLevels[n - 1].setQty(marketbook.getAskQty(n - 1), exchangeTimeMicros);
                n--;
            }
            final int shift = m;
            while (m > 0 && m < defaultDepth * 2) {
                askLevels[m - shift].refreshFromAnotherEntry(askLevels[m]);
                for (int i = m; i > m - shift; i--) {
                    askLevels[i].clear();
                }
                m++;
            }
        }

    }

    @Override
    public void updateFrom(final long exchangeTimeMicros, final BookView marketbook) {
        // current best bid is lower than assuming one
        while (Double.compare(marketbook.getBidPrice(), bidLevels[0].price.asDouble()) < 0 && bidLevels[0].isValid()) {
            for (int k = 0; k < bidLevels.length - 1; k++) {
                bidLevels[k].refreshFromAnotherEntry(bidLevels[k + 1]);
            }
            bidLevels[bidLevels.length - 1].clear();
        }

        for (int directIdx = 0; directIdx < marketbook.getBidDepth(); directIdx++) {
            final double price = marketbook.getBidPrice(directIdx);
            final long qty = marketbook.getBidQty(directIdx);

            for (int virtualIdx = directIdx; virtualIdx < bidLevels.length; virtualIdx++) {
                if (bidLevels[virtualIdx].isValid() && Double.compare(price, bidLevels[virtualIdx].price.asDouble()) < 0) {
                    continue;
                } else if (Double.compare(price, bidLevels[virtualIdx].price.asDouble()) == 0) {
                    bidLevels[virtualIdx].setQty(qty, exchangeTimeMicros);
                    break;
                } else {
                    for (int k = bidLevels.length - 1; k > virtualIdx; k--) {
                        bidLevels[k].refreshFromAnotherEntry(bidLevels[k - 1]);
                    }
                    bidLevels[virtualIdx].price.setDouble(price);
                    bidLevels[virtualIdx].setQty(qty, exchangeTimeMicros);
                    break;
                }
            }
        }

        // current best bid is lower than assuming one
        while (Double.compare(marketbook.getAskPrice(), askLevels[0].price.asDouble()) > 0 && askLevels[0].isValid()) {
            for (int k = 0; k < askLevels.length - 1; k++) {
                askLevels[k].refreshFromAnotherEntry(askLevels[k + 1]);
            }
            askLevels[askLevels.length - 1].clear();
        }

        for (int directIdx = 0; directIdx < marketbook.getAskDepth(); directIdx++) {
            final double price = marketbook.getAskPrice(directIdx);
            final long qty = marketbook.getAskQty(directIdx);

            for (int virtualIdx = directIdx; virtualIdx < askLevels.length; virtualIdx++) {
                if (askLevels[virtualIdx].isValid() && Double.compare(price, askLevels[virtualIdx].price.asDouble()) > 0) {
                    continue;
                } else if (Double.compare(price, askLevels[virtualIdx].price.asDouble()) == 0) {
                    askLevels[virtualIdx].setQty(qty, exchangeTimeMicros);
                    break;
                } else {
                    for (int k = askLevels.length - 1; k > virtualIdx; k--) {
                        askLevels[k].refreshFromAnotherEntry(askLevels[k - 1]);
                    }
                    askLevels[virtualIdx].price.setDouble(price);
                    askLevels[virtualIdx].setQty(qty, exchangeTimeMicros);
                    break;
                }
            }
        }
    }

    @Override
    public void updateFrom(final long exchangeTimeMicros, final TickView tick) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onCrossOrderMatching(final long fillResetMicros, final Side aggressorSide, final Price orderPrice, final long orderQty) {
        long qty = orderQty;
        switch (aggressorSide) {
            case BUY:
                for (int i = 0; i < bidLevels.length && qty > 0; i++) {
                    if (PriceUtils.isInsideOfOrEqualTo(Side.BUY, bidLevels[i].price.asDouble(), orderPrice.asDouble())) {
                        if (qty > bidLevels[i].hittableQty) {
                            qty -= bidLevels[i].hittableQty;
                            bidLevels[i].hittableQty = 0;
                        } else {
                            bidLevels[i].hittableQty -= qty;
                            qty = 0;
                        }
                        bidLevels[i].fillResetMicros = fillResetMicros;
                        bidLevels[i].dirty = true;
                    }
                }
                break;
            case SELL:
                for (int i = 0; i < askLevels.length && qty > 0; i++) {
                    if (PriceUtils.isInsideOfOrEqualTo(Side.SELL, askLevels[i].price.asDouble(), orderPrice.asDouble())) {
                        if (qty > askLevels[i].hittableQty) {
                            qty -= askLevels[i].hittableQty;
                            askLevels[i].hittableQty = 0;
                        } else {
                            askLevels[i].hittableQty -= qty;
                            qty = 0;
                        }
                        askLevels[i].fillResetMicros = fillResetMicros;
                        askLevels[i].dirty = true;
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onOrderMatching(final long fillResetMicros, final Side aggressorSide, final Price orderPrice, final long orderQty) {
        switch (aggressorSide) {
            case BUY:
                for (int i = 0; i < bidLevels.length; i++) {
                    if (Double.compare(orderPrice.asDouble(), bidLevels[i].price.asDouble()) != 0) {
                        continue;
                    }
                    bidLevels[i].hittableQty -= orderQty;
                    bidLevels[i].fillResetMicros = fillResetMicros;
                    bidLevels[i].dirty = true;
                }
                break;
            case SELL:
                for (int i = 0; i < askLevels.length; i++) {
                    if (Double.compare(orderPrice.asDouble(), askLevels[i].price.asDouble()) != 0) {
                        continue;
                    }
                    askLevels[i].hittableQty -= orderQty;
                    askLevels[i].fillResetMicros = fillResetMicros;
                    askLevels[i].dirty = true;
                }
                break;
            default:
                break;
        }
    }

    class OrderBookPriceEntry {
        private final Price price = new StockPriceCNDouble();
        private long qty;
        private long hittableQty;
        private long fillResetMicros;
        private boolean dirty;
        private Clearable clearable;

        OrderBookPriceEntry(final Side side) {
            hittableQty = qty;
            fillResetMicros = Long.MAX_VALUE;
            dirty = false;
            if (side == Side.BUY) {
                clearable = new Clearable() {

                    @Override
                    public void clear() {
                        price.setMin();
                    }
                };
            } else {
                clearable = new Clearable() {

                    @Override
                    public void clear() {
                        price.setNull();
                    }
                };

            }
        }

        public Price getPrice() {
            return price;
        }

        public long getQty() {
            return hittableQty;
        }

        boolean isValid() {
            return qty > 0 && !Double.isNaN(price.asDouble()) && price.asDouble() != 0;
        }

        void clear() {
            clearable.clear();
            this.qty = hittableQty = 0;
            this.fillResetMicros = Long.MAX_VALUE;
            this.dirty = false;
        }

        void refreshFromAnotherEntry(final OrderBookPriceEntry entry) {
            this.price.setDouble(entry.getPrice().asDouble());
            this.qty = entry.qty;
            this.hittableQty = entry.hittableQty;
            this.fillResetMicros = entry.fillResetMicros;
            this.dirty = entry.dirty;
        }

        void setQty(final long qty, final long exchangeTimeMicros) {
            if (!dirty) {
                this.qty = hittableQty = qty;
                fillResetMicros = Long.MAX_VALUE;
            } else {
                // Set to fill some orders
                if (exchangeTimeMicros >= fillResetMicros) {
                    // Can be reset
                    dirty = false;
                    this.qty = hittableQty = qty;
                    fillResetMicros = Long.MAX_VALUE;
                } else {
                    hittableQty += qty - this.qty;
                    this.qty = qty;
                    if (hittableQty < 0) {
                        hittableQty = 0;
                    }
                }
            }
        }
    }

    public static void main(final String[] args) {
        final long invallid = 0l;
        final Long[] levels = new Long[] { 3944l, 3942l, 3941l, 3940l, invallid, invallid, invallid, invallid, invallid, invallid };
        final Long[] input = new Long[] { 3943l };

        final int depth = 10;
        int m = 5;
        int n = input.length;

        // Asks?
        while (m > 0 && n > 0) {
            if (levels[m - 1] < input[n - 1]) {
                levels[m + n - 1] = levels[m - 1];
                m--;
            } else if (levels[m - 1].equals(input[n - 1])) {
                levels[m + n - 1] = input[n - 1];
                n--;
                for (int i = m + n - 1; i > 0; i--) {
                    levels[i] = levels[i - 1];
                    levels[i - 1] = invallid;
                }
            } else {
                levels[m + n - 1] = input[n - 1];
                n--;
            }
        }
        while (n > 0) {
            levels[n - 1] = input[n - 1];
            n--;
        }
        final int shift = m;
        while (m > 0 && m < depth) {
            levels[m - shift] = levels[m];
            for (int i = m; i > m - shift; i--) {
                levels[i] = invallid;
            }
            m++;
        }
        System.out.println();
    }

}
