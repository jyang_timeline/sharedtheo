package com.timelinecapital.core.trade.simulation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.commons.type.StockPriceCNMantissa;
import com.timelinecapital.core.types.OrderBookPriceLevel;
import com.timelinecapital.core.util.AggressorToRestingOrderHelper;

public class ExecutorMarketBookWithMantissaPrice implements ExchangeMarketBook {
    private static final Logger log = LogManager.getLogger(ExecutorMarketBookWithMantissaPrice.class);

    private static final int defaultDepth = 10;

    private final Map<Side, OrderBookPriceLevel[]> internalLevels = new HashMap<>(2, 1.0f);
    private final Contract contract;
    private final double tickSize;

    public ExecutorMarketBookWithMantissaPrice(final Contract contract) {
        this.contract = contract;
        this.tickSize = contract.getTickCalculator().getTickSize();
        init();
    }

    private void init() {
        internalLevels.computeIfAbsent(Side.BUY, key -> new OrderBookPriceEntry[defaultDepth * 2]);
        internalLevels.computeIfAbsent(Side.SELL, key -> new OrderBookPriceEntry[defaultDepth * 2]);

        internalLevels.entrySet().forEach(entry -> {
            for (int i = 0; i < defaultDepth * 2; i++) {
                entry.getValue()[i] = new OrderBookPriceEntry(entry.getKey());
                entry.getValue()[i].clear();
            }
        });
    }

    @Override
    public Contract getContract() {
        return contract;
    }

    @Override
    public Price getAtMarketPrice(final Side side) {
        return this.getAtMarketPrice(side, 0);
    }

    @Override
    public Price getAtMarketPrice(final Side side, final int level) {
        return internalLevels.get(side.switchSide())[level].getPrice();
    }

    @Override
    public long getHittableQty(final Side side) {
        return this.getHittableQty(side, 0);
    }

    @Override
    public long getHittableQty(final Side side, final int level) {
        return internalLevels.get(side.switchSide())[level].getHittableQty();
    }

    @Override
    public long getAvailableQty(final Side side, final Price targetPrice) {
        long hittableQty = 0;
        final OrderBookPriceLevel[] bookLevels = internalLevels.get(side.switchSide());
        for (int i = 0; i < bookLevels.length; i++) {
            if (bookLevels[i].getPrice() == targetPrice && bookLevels[i].getHittableQty() > 0) {
                hittableQty += bookLevels[i].getHittableQty();
            }
        }
        return hittableQty;
    }

    @Override
    public long getQty(final Side side, final Price targetPrice) {
        final OrderBookPriceLevel[] bookLevels = internalLevels.get(side);
        for (int i = 0; i < bookLevels.length; i++) {
            if (bookLevels[i].getPrice() == targetPrice && bookLevels[i].getHittableQty() > 0) {
                return bookLevels[i].getHittableQty();
            }
        }
        return 0;
    }

    @Override
    public void replaceWith(final long exchangeTimeMicros, final BookView marketbook) {
        for (final Side side : Side.values()) {
            if (Side.INVALID.equals(side)) {
                continue;
            }
            final OrderBookPriceLevel[] bookLevels = internalLevels.get(side);
            if (!bookLevels[0].isValid()) {
                final long px = PriceUtils.toMantissa(side.getPrice(marketbook), tickSize);
                log.warn("Initializing SimMarketBook {} {} {}", side, contract, px);
                for (int i = 0; i < side.getDepth(marketbook); i++) {
                    bookLevels[i].getPrice().setMantissa(px);
                    bookLevels[i].setQty(side.getQty(marketbook));
                }
            } else {
                int m = defaultDepth;
                int n = side.getDepth(marketbook);

                while (m > 0 && n > 0) {
                    final long px = PriceUtils.toMantissa(side.getPrice(marketbook, n - 1), tickSize);

                    if (PriceUtils.isInsideOf(side, px, bookLevels[m - 1].getPrice().getMantissa())) {
                        if (n == side.getDepth(marketbook)) {
                            bookLevels[m + n - 1].refreshFromAnotherEntry(bookLevels[m - 1]);
                        }
                        bookLevels[m - 1].clear();
                        m--;
                    } else if (bookLevels[m - 1].getPrice().getMantissa() == px) {
                        bookLevels[m + n - 1].refreshFromAnotherEntry(bookLevels[m - 1]);
                        bookLevels[m + n - 1].tryUpdateQty(side.getQty(marketbook, n - 1), exchangeTimeMicros);
                        bookLevels[m - 1].clear();
                        n--;
                        for (int i = m + n - 1; i > 0; i--) {
                            bookLevels[i].refreshFromAnotherEntry(bookLevels[i - 1]);
                            bookLevels[i - 1].clear();
                        }
                    } else {
                        bookLevels[m + n - 1].getPrice().setMantissa(px);
                        bookLevels[m + n - 1].tryUpdateQty(side.getQty(marketbook, n - 1), exchangeTimeMicros);
                        n--;
                    }
                }
                while (n > 0) {
                    bookLevels[n - 1].getPrice().setMantissa(PriceUtils.toMantissa(side.getPrice(marketbook, n - 1), tickSize));
                    bookLevels[n - 1].tryUpdateQty(side.getQty(marketbook, n - 1), exchangeTimeMicros);
                    n--;
                }
                final int shift = m;
                while (m > 0 && m < defaultDepth * 2) {
                    bookLevels[m - shift].refreshFromAnotherEntry(bookLevels[m]);
                    for (int i = m; i > m - shift; i--) {
                        bookLevels[i].clear();
                    }
                    m++;
                }

                for (int x = 0; x < defaultDepth * 2; x++) {
                    if (!bookLevels[x].isValid()) {
                        for (int y = x; y < defaultDepth * 2; y++) {
                            if (bookLevels[y].isValid()) {
                                bookLevels[x].refreshFromAnotherEntry(bookLevels[y]);
                                bookLevels[y].clear();
                                break;
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    public void updateFrom(final long exchangeTimeMicros, final BookView marketbook) {
        for (final Side side : Side.values()) {
            if (Side.INVALID.equals(side)) {
                continue;
            }
            final OrderBookPriceLevel[] bookLevels = internalLevels.get(side);

            final long px = PriceUtils.toMantissa(side.getPrice(marketbook), exchangeTimeMicros);

            while (PriceUtils.isInsideOf(side, bookLevels[0].getPrice().getMantissa(), px) && bookLevels[0].isValid()) {
                for (int k = 0; k < bookLevels.length - 1; k++) {
                    bookLevels[k].refreshFromAnotherEntry(bookLevels[k + 1]);
                }
                bookLevels[bookLevels.length - 1].clear();
            }

            for (int directIdx = 0; directIdx < side.getDepth(marketbook); directIdx++) {
                final long pxFromBook = PriceUtils.toMantissa(side.getPrice(marketbook, directIdx), tickSize);
                final long qty = side.getQty(marketbook, directIdx);

                for (int virtualIdx = directIdx; virtualIdx < bookLevels.length; virtualIdx++) {
                    if (bookLevels[virtualIdx].isValid() && PriceUtils.isInsideOf(side, bookLevels[virtualIdx].getPrice().getMantissa(), pxFromBook)) {
                        continue;
                    } else if (pxFromBook == bookLevels[virtualIdx].getPrice().getMantissa()) {
                        bookLevels[virtualIdx].tryUpdateQty(qty, exchangeTimeMicros);
                        break;
                    } else {
                        for (int k = bookLevels.length - 1; k > virtualIdx; k--) {
                            bookLevels[k].refreshFromAnotherEntry(bookLevels[k - 1]);
                        }
                        bookLevels[virtualIdx].getPrice().setMantissa(pxFromBook);
                        bookLevels[virtualIdx].tryUpdateQty(qty, exchangeTimeMicros);
                        break;
                    }
                }
            }

        }
    }

    @Override
    public void updateFrom(final long exchangeTimeMicros, final TickView tick) {
        final long px = PriceUtils.toMantissa(tick.getPrice(), tickSize);
        final long qty = tick.getQuantity();
        final Side restingSide = AggressorToRestingOrderHelper.ofCrossingSide(tick.getType());
        final OrderBookPriceLevel[] levels = internalLevels.get(restingSide);

        // Trade price is inside of book -> BUYER lower than SELL | SELLER higher than BUY

        // Trade price is equal to top of book -> BUYER match SELL[0] | SELLER match BUY[0]

        // Trade price is outside of book -> BUYER higher than SELL | SELLER lower than BUY
        for (int idx = 0; idx < levels.length; idx++) {
            if (PriceUtils.isOutsideOf(restingSide, levels[idx].getPrice().getMantissa(), px)) {
                log.debug("Trade {} {} can not update Book {} {}", px, qty, levels[idx].getPrice().getMantissa(), levels[idx].getQty());
                break;
            }
            if (levels[idx].getPrice().getMantissa() == px) {
                if (levels[idx].getHittableQty() == 0) {
                    log.debug("Book {} {} with HittableQty as 0", levels[idx].getPrice().getMantissa(), levels[idx].getQty());
                }
                levels[idx].setHittableQty(reduce(levels[idx].getHittableQty(), qty));
            } else {
                levels[idx].setHittableQty(0);
                log.debug("Book {} {} has HittableQty as 0 by Trade {} {}", levels[idx].getPrice().getMantissa(), levels[idx].getQty(), px, qty);
            }
        }
    }

    @Override
    public void onCrossOrderMatching(final long fillResetMicros, final Side aggressorSide, final Price orderPrice, final long orderQty) {
        long qty = orderQty;
        final OrderBookPriceLevel[] levels = internalLevels.get(aggressorSide);
        for (int i = 0; i < levels.length && qty > 0; i++) {
            if (PriceUtils.isInsideOfOrEqualTo(aggressorSide, levels[i].getPrice(), orderPrice)) {
                if (qty > levels[i].getHittableQty()) {
                    qty -= levels[i].getHittableQty();
                    levels[i].setHittableQty(0);
                } else {
                    levels[i].setHittableQty(levels[i].getHittableQty() - qty);
                    qty = 0;
                }
                levels[i].setResetTimeMicros(fillResetMicros);
                levels[i].setDirty();
            }
        }
    }

    @Override
    public void onOrderMatching(final long fillResetMicros, final Side aggressorSide, final Price orderPrice, final long orderQty) {
        final OrderBookPriceLevel[] levels = internalLevels.get(aggressorSide);
        for (int i = 0; i < levels.length; i++) {
            if (levels[i].getPrice() != orderPrice) {
                continue;
            }
            levels[i].setHittableQty(levels[i].getHittableQty() - orderQty);
            levels[i].setResetTimeMicros(fillResetMicros);
            levels[i].setDirty();
        }
    }

    void merge(final OrderBookPriceEntry[] target, final Price[] source) {
        final int m = defaultDepth;
        final int n = source.length;

        int i = m - 1, j = n - 1, k = m + n - 1;
        while (j >= 0) {
            // nums1[k--] = (i >= 0 && nums1[i] > nums2[j]) ? nums1[i--] : nums2[j--];
            final long px = (i >= 0 && target[i].price.getMantissa() > source[j].getMantissa()) ? target[i--].getPrice().getMantissa() : source[j--].getMantissa();
            target[k--].price.setMantissa(px);
        }
    }

    long reduce(final long base, final long incoming) {
        return base > incoming ? base - incoming : 0;
    }

    class OrderBookPriceEntry implements OrderBookPriceLevel {
        private final Price price = new StockPriceCNMantissa();
        private final StringBuilder sb = new StringBuilder();

        private long qty;
        private long hittableQty;
        private long fillResetMicros;
        private boolean dirty;
        private Clearable clearable;

        OrderBookPriceEntry(final Side side) {
            hittableQty = qty;
            fillResetMicros = Long.MAX_VALUE;
            dirty = false;
            switch (side) {
                case BUY: {
                    clearable = new Clearable() {

                        @Override
                        public void clear() {
                            price.setMin();
                        }
                    };
                }
                    break;
                case SELL: {
                    clearable = new Clearable() {

                        @Override
                        public void clear() {
                            price.setNull();
                        }
                    };
                }
                    break;
                default:
                    break;
            }
        }

        @Override
        public Price getPrice() {
            return price;
        }

        @Override
        public long getQty() {
            return qty;
        }

        @Override
        public void setQty(final long qty) {
            this.qty = qty;
        }

        @Override
        public long getHittableQty() {
            return hittableQty;
        }

        @Override
        public void setHittableQty(final long hittableQty) {
            this.hittableQty = hittableQty;
        }

        @Override
        public long getResetTimeMicros() {
            return fillResetMicros;
        }

        @Override
        public void setResetTimeMicros(final long resetTimeMicros) {
            this.fillResetMicros = resetTimeMicros;
        }

        @Override
        public boolean isDirty() {
            return dirty;
        }

        @Override
        public void setDirty() {
            this.dirty = true;
        }

        @Override
        public boolean isValid() {
            return qty > 0 && price.getMantissa() != 0 && price.getMantissa() != Long.MAX_VALUE && price.getMantissa() != Long.MIN_VALUE;
        }

        @Override
        public void clear() {
            clearable.clear();
            this.qty = hittableQty = 0;
            this.fillResetMicros = Long.MAX_VALUE;
            this.dirty = false;
        }

        @Override
        public void refreshFromAnotherEntry(final OrderBookPriceLevel entry) {
            price.setMantissa(entry.getPrice().getMantissa());
            this.qty = entry.getQty();
            this.hittableQty = entry.getHittableQty();
            this.fillResetMicros = entry.getResetTimeMicros();
            this.dirty = entry.isDirty();
        }

        @Override
        public void tryUpdateQty(final long qty, final long exchangeTimeMicros) {
            if (!dirty) {
                this.qty = hittableQty = qty;
                fillResetMicros = Long.MAX_VALUE;
            } else {
                // Set to fill some orders
                if (exchangeTimeMicros >= fillResetMicros) {
                    // Can be reset
                    dirty = false;
                    this.qty = hittableQty = qty;
                    fillResetMicros = Long.MAX_VALUE;
                } else {
                    hittableQty += qty - this.qty;
                    this.qty = qty;
                    if (hittableQty < 0) {
                        hittableQty = 0;
                    }
                }
            }
        }

        @Override
        public String toString() {
            sb.setLength(0);
            sb.append(qty).append("@").append(price.getMantissa());
            return sb.toString();
        }
    }

    public static void main(final String[] args) {
        final long invallid = 0l;
        final Long[] levels = new Long[] { 7758l, 7757l, 7756l, 7753l, 7750l, invallid, invallid, invallid, invallid, invallid };
        final Long[] input = new Long[] { 7759l, 7758l, 7756l, 7753l, 7751l };

        final int depth = 10;
        int m = 5;
        int n = input.length;

        // Asks?
        while (m > 0 && n > 0) {
            if (levels[m - 1] < input[n - 1]) {
                if (n == input.length) {
                    levels[m + n - 1] = levels[m - 1];
                }
                levels[m - 1] = invallid;
                m--;
            } else if (levels[m - 1].equals(input[n - 1])) {
                levels[m + n - 1] = input[n - 1];
                levels[m - 1] = invallid;
                n--;
                for (int i = m + n - 1; i > 0; i--) {
                    levels[i] = levels[i - 1];
                    levels[i - 1] = invallid;
                }
            } else {
                levels[m + n - 1] = input[n - 1];
                n--;
            }
            System.out.println(Arrays.toString(levels));
        }
        while (n > 0) {
            levels[n - 1] = input[n - 1];
            n--;
        }
        final int shift = m;
        while (m > 0 && m < depth) {
            levels[m - shift] = levels[m];
            for (int i = m; i > m - shift; i--) {
                levels[i] = invallid;
            }
            m++;
        }
        System.out.println(Arrays.toString(levels));
    }

}
