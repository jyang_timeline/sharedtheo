package com.timelinecapital.core.trade.simulation;

import com.nogle.strategy.types.BookView;

public interface MDMarketBookEvent<M extends BookView> extends OrderMatching<M> {

    void set(final ExchangeMarketBook marketbook);

    void update(final long eventTimeMicros, final M bookView);

}
