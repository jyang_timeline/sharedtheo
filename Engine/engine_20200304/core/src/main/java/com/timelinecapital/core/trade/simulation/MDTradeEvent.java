package com.timelinecapital.core.trade.simulation;

import com.nogle.strategy.types.TickView;

public interface MDTradeEvent<T extends TickView> extends OrderMatching<T> {

    void set(final ExchangeMarketBook marketbook);

    void update(final long eventTimeMicros, final T tickView);

}
