package com.timelinecapital.core.trade.simulation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalClock;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalOrderBookManager;

public class MarketBookEventHandler implements MDMarketBookEvent<BookView> {
    private static final Logger log = LogManager.getLogger(MarketBookEventHandler.class);

    private final InternalOrderBookManager bookManager;
    private final InternalClock clock;
    private final ExchangeEventQueue delayedQueue;
    private final FillFactory fillFactory;

    private ExchangeMarketBook simMarketBook;

    public MarketBookEventHandler(final InternalOrderBookManager bookManager, final InternalClock clock, final ExchangeEventQueue delayedQueue, final FillFactory fillFactory) {
        this.bookManager = bookManager;
        this.clock = clock;
        this.delayedQueue = delayedQueue;
        this.fillFactory = fillFactory;
    }

    @Override
    public void set(final ExchangeMarketBook simMarketBook) {
        this.simMarketBook = simMarketBook;
    }

    @Override
    public void update(final long eventTimeMicros, final BookView bookView) {
        simMarketBook.replaceWith(eventTimeMicros, bookView);
    }

    /*-
     * Order has been put into MarketBook to be filled/cancelled
     * 1. Order price is lower than best offers (BUY) / higher than best asks (SELL)
     *     -> try to update priority by MarketBook (BUY -> bids; SELL -> offers)
     * 2. Order price is equal or higher than best offers (BUY) / equal or lower than best bids (SELL)
     *     -> hittableQty is greater than orderQty (from best offers (BUY) / best bids (SELL))
     *     -> fillPrice should be placed price due to market's moving
     *     -> reduce best offer (BUY) / best bid qty to avoid fills?
     *     -> generate fills
     *
     *     -> hittableQty can not FULLY fill order at best offer (BUY) / best bid (SELL)
     *     -> order still queuing
     *     -> generate partial fills
     */
    @Override
    public void tryOrderMatching(final long eventTimeMicros, final BookView bookView) {
        for (final Side side : Side.values()) {
            if (Side.INVALID.equals(side)) {
                continue;
            }

            long hittableQty = 0;
            do {
                final SimulatedOrder order = bookManager.get(side);
                if (order == null) {
                    break;
                }

                final Price priceToCross = simMarketBook.getAtMarketPrice(order.getSide());

                if (PriceUtils.isInsideOf(order.getSide(), priceToCross.getMantissa(), order.getMantissaPrice())) {
                    tryUpdatePriority(order);
                    break;
                }

                hittableQty = simMarketBook.getAvailableQty(order.getSide(), order.getPrice());

                if (hittableQty <= 0) {
                    log.debug("[NoEvent] Cross {} (hittable: {}) {}", priceToCross, hittableQty, order);
                    break;
                }

                clock.setFillPriceResetMicros(eventTimeMicros + clock.getFillPriceBanningDelayMicros());

                final OrderFill fill = fillFactory.get();
                if (hittableQty >= order.getQuantity()) { // Fully filled at a given price, fill price = order price
                    final double commission = bookManager.getCommission(order.getSide(), order.getQuantity(), order.getDoublePrice());
                    fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), order.getQuantity(), order.getDoublePrice(), commission);

                    bookManager.remove(order);
                    simMarketBook.onCrossOrderMatching(clock.getFillPriceResetMicros(), order.getSide().switchSide(), order.getPrice(), order.getQuantity());

                    hittableQty -= order.getQuantity();
                    log.debug("[Exec] FullFill onBook ; {} ", order);
                } else {
                    final double commission = bookManager.getCommission(order.getSide(), hittableQty, order.getDoublePrice());
                    fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), hittableQty, order.getDoublePrice(), commission);
                    fill.setRemainingQty(order.getQuantity() - hittableQty);

                    order.fill(hittableQty);
                    order.updatePriority(0);
                    simMarketBook.onCrossOrderMatching(clock.getFillPriceResetMicros(), order.getSide().switchSide(), order.getPrice(), hittableQty);

                    hittableQty = 0;
                    log.debug("[Exec] PartialFill onBook ; {} (Hit: {})", order, hittableQty);
                }

                delayedQueue.queueDelayedFill(eventTimeMicros, fill);
                log.debug("[Exec] Fill at Book: [{}] {} {}@{}/{}@{} {}", bookView.getSequenceNo(), bookView.getContract(),
                    simMarketBook.getHittableQty(Side.BUY, 0), simMarketBook.getAtMarketPrice(Side.BUY),
                    simMarketBook.getHittableQty(Side.SELL, 0), simMarketBook.getAtMarketPrice(Side.SELL),
                    eventTimeMicros);
            } while (bookManager.get(side) != null && hittableQty > 0);

        }
    }

    private void tryUpdatePriority(final SimulatedOrder order) {
        final long qty = simMarketBook.getQty(order.getSide(), order.getPrice());
        if (order.isReestimationRequired() || (order.getPriority() > qty && qty > 0)) {
            log.debug("[Exec] {} priority to {} from {}", order.getClOrdId(), order.getPriority(), qty);
            order.updatePriority(qty);
        }
    }

}
