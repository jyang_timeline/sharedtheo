package com.timelinecapital.core.trade.simulation;

public interface OrderMatching<T> {

    void tryOrderMatching(long eventTimeMicros, T evet);

}
