package com.timelinecapital.core.trade.simulation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

public class SimMatchingUtils {

    public static long getFillableQty(final long resting, final long hittable) {
        if (resting > hittable) {
            return hittable;
        }
        return resting;
    }

    public static long getRemainingQty(final long resting, final long hittable) {
        if (resting < hittable) {
            return 0;
        }
        return resting - hittable;
    }

    public static <T> List<T> interleaveSort(final List<T> list1, final List<T> list2, final Comparator<T> cmp) {
        // sort the lists and set them up as queues
        Deque<T> x = list1.stream().sorted(cmp).collect(Collectors.toCollection(ArrayDeque::new));
        Deque<T> y = list2.stream().sorted(cmp).collect(Collectors.toCollection(ArrayDeque::new));

        final List<T> result = new ArrayList<>(x.size() + y.size());

        while (!x.isEmpty() && !y.isEmpty()) {
            if (cmp.compare(x.peek(), y.peek()) > 0) {
                result.add(y.poll());
            } else {
                // whenever we pick from X, swap queues so next time we will favor the other list
                result.add(x.poll());
                final Deque<T> t = x;
                x = y;
                y = t;
            }
        }
        // since the while loop terminates when one of the queues is empty
        // we should add the left-over elements
        result.addAll(x);
        result.addAll(y);

        return result;
    }

}
