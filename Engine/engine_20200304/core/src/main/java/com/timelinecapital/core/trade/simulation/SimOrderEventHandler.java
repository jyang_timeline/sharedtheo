package com.timelinecapital.core.trade.simulation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.DefaultFilter;
import com.nogle.core.trade.SimulatedExchange.DelayedQueue;
import com.nogle.core.trade.SimulatedExchange.ExecutionEvent;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.core.Filter;
import com.timelinecapital.core.Filter.Result;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.strategy.state.ExchangeSession;

public class SimOrderEventHandler {
    private static final Logger log = LogManager.getLogger(SimOrderEventHandler.class);

    private static final Filter<ExecutionEvent> DEFAULT_FILTER = DefaultFilter.createFilter(Result.ACCEPT, Result.DENY);

    // Internal OrderBook for matching
    private final SimOrderExecutor orderExecutor;
    private final DelayedQueue outgoingQueue;

    private Filter<ExecutionEvent> filter;

    public SimOrderEventHandler(final Contract symbol, final FeeCalculator feeCalculator, final DelayedQueue outgoingQueue,
        final AckFactory ackFactory, final FillFactory fillFactory) {
        orderExecutor = new SimOrderExecutor(symbol, feeCalculator, outgoingQueue, ackFactory, fillFactory);
        this.outgoingQueue = outgoingQueue;
        filter = DEFAULT_FILTER;
    }

    public void onTick(final long exchangeTimeMicros, final BDTickView tick) {
        switch (tick.getType()) {
            case UNKNOWN:
                return;
            default:
                // TODO apply filter to 'onTick'
                // orderExecutor.onTick(exchangeTimeMicros, tick);
                orderExecutor.matchOnTrade(exchangeTimeMicros, tick);
                break;
        }
    }

    public void onMarketBook(final long exchangeTimeMicros, final BDBookView marketbook) {
        orderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        if (Result.ACCEPT == filter.filter(ExecutionEvent.MARKETBOOK_MATCHING)) {
            orderExecutor.matchOrder(exchangeTimeMicros, marketbook);
        } else {
            log.trace("Session: no matching using MarketBook");
        }
    }

    public void processNewOrder(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        if (Result.ACCEPT == filter.filter(ExecutionEvent.ORDER_INSERT)) {
            switch (simulatedOrder.getTimeCondition()) {
                case GFS:
                case GFD:
                    outgoingQueue.queueDelayedOrderAck(arrivalTimeMicros, simulatedOrder.getClOrdId());
                    break;
                default:
                    break;
            }
        } else {
            outgoingQueue.queueDelayedOrderReject(arrivalTimeMicros, simulatedOrder.getClOrdId());
        }

        if (Result.ACCEPT == filter.filter(ExecutionEvent.MARKETBOOK_MATCHING)) {
            orderExecutor.onOrderInsertAndTryMatching(arrivalTimeMicros, simulatedOrder);
        } else {
            log.trace("Session: processing order without matching");
            orderExecutor.onOrderInsert(arrivalTimeMicros, simulatedOrder);
        }
    }

    public void processOrderCancel(final long arrivalTimeMicros, final SimulatedCancel simulatedCancel) {
        if (Result.ACCEPT == filter.filter(ExecutionEvent.ORDER_CANCEL)) {
            orderExecutor.onOrderCancel(arrivalTimeMicros, simulatedCancel);
        } else {
            outgoingQueue.queueDelayedCancelReject(arrivalTimeMicros, simulatedCancel.getClOrdId());
        }
    }

    public void setSession(final ExchangeSession session, final Filter<ExecutionEvent> filter) {
        this.filter = filter;
    }

    public void clearFilter() {
        filter = DEFAULT_FILTER;
    }

}
