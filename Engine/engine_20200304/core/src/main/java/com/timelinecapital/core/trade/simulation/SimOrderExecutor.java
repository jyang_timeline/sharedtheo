package com.timelinecapital.core.trade.simulation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FillType;
import com.nogle.core.types.SimulatedCancel;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.util.ExchangeExecutionUtils;
import com.timelinecapital.core.util.FeeHelper;

public class SimOrderExecutor {
    private static final Logger log = LogManager.getLogger(SimOrderExecutor.class);

    private long fillPriceBanningDelayMicros = NogleTimeUnit.SECONDS.toMicros(30);

    private final ExchangeMarketBook simMarketBook;
    private BDBookView histMarketbook;

    private final String exchange;
    private final FeeCalculator feeCalculator;
    private final ExchangeEventQueue delayedQueue;
    private final AckFactory ackFactory;
    private final FillFactory fillFactory;

    private final InternalClock clock;
    private final InternalOrderBookManager bookManager;
    private final MDTradeEvent<TickView> tradeHandler;
    private final MDMarketBookEvent<BookView> bookHandler;

    @SuppressWarnings("unchecked")
    public SimOrderExecutor(final Contract contract, final FeeCalculator feeCalculator, final ExchangeEventQueue delayedQueue,
        final AckFactory ackFactory, final FillFactory fillFactory) {
        this.exchange = contract.getExchange();
        this.feeCalculator = feeCalculator;
        this.delayedQueue = delayedQueue;
        this.ackFactory = ackFactory;
        this.fillFactory = fillFactory;

        simMarketBook = ExchangeExecutionUtils.ofExecutorBook(contract);

        clock = new InternalClock();
        bookManager = new InternalOrderBookManager(new EffectiveOrders());

        tradeHandler = (MDTradeEvent<TickView>) EventHandlerFactory.getEventHandler(contract, DataType.TICK, bookManager, clock, delayedQueue, fillFactory);
        bookHandler = (MDMarketBookEvent<BookView>) EventHandlerFactory.getEventHandler(contract, DataType.MARKETBOOK, bookManager, clock, delayedQueue, fillFactory);

        tradeHandler.set(simMarketBook);
        bookHandler.set(simMarketBook);
    }

    public void setFillPriceBanningTime(final long fillPriceBanningDelayMicros) {
        this.fillPriceBanningDelayMicros = fillPriceBanningDelayMicros;
    }

    public Map<Long, SimulatedOrder> getInternalMap() {
        return Collections.unmodifiableMap(bookManager.clOrdIdToOrder);
    }

    public ExchangeMarketBook getInternalBook() {
        return simMarketBook;
    }

    public void onTick(final long exchangeTimeMicros, final BDTickView tick) {
        tradeHandler.update(exchangeTimeMicros, tick);
    }

    public void onMarketBook(final long exchangeTimeMicros, final BDBookView book) {
        bookHandler.update(exchangeTimeMicros, book);
        clock.updateEventTime(exchangeTimeMicros);
        histMarketbook = book;
    }

    public void onOrderInsert(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        clock.setArrivalTimeMicros(arrivalTimeMicros);
        handleRemaining(simulatedOrder);
    }

    public void onOrderInsertAndTryMatching(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        clock.setArrivalTimeMicros(arrivalTimeMicros);

        final Side side = simulatedOrder.getSide();
        final Price restingPrice = simulatedOrder.getPrice();
        final Price hittablePrice = simMarketBook.getAtMarketPrice(side);

        if (PriceUtils.isInsideOfOrEqualTo(side, restingPrice, hittablePrice)) {
            handleExecutablePrice(simulatedOrder);
        }
        if (simulatedOrder.getQuantity() > 0) {
            handleRemaining(simulatedOrder);
        }
    }

    public void onOrderMod(final long arrivalTimeMicros, final SimulatedOrder simulatedOrder) {
        if (handleOrderMod(simulatedOrder)) {
            delayedQueue.queueDelayedModAck(arrivalTimeMicros, simulatedOrder.getClOrdId());
        } else {
            delayedQueue.queueDelayedModReject(arrivalTimeMicros, simulatedOrder.getClOrdId());
        }
    }

    public void onOrderCancel(final long arrivalTimeMicros, final SimulatedCancel simulatedCancel) {
        if (internalCancel(simulatedCancel.getClOrdId())) {
            final Ack ack = ackFactory.get();
            ack.setAck(simulatedCancel.getContract(), simulatedCancel.getSide(), TimeCondition.GFD, ExecType.CANCEL_ACK, simulatedCancel.getClOrdId());
            delayedQueue.queueDelayedCancelAck(arrivalTimeMicros, ack);
        } else {
            delayedQueue.queueDelayedCancelReject(arrivalTimeMicros, simulatedCancel.getClOrdId());
        }
    }

    public void matchOnTrade(final long exchangeTimeMicros, final BDTickView trade) {
        clock.updateEventTime(exchangeTimeMicros);
        tradeHandler.tryOrderMatching(exchangeTimeMicros, trade);
    }

    public void matchOrder(final long exchangeTimeMicros, final BDBookView book) {
        clock.updateEventTime(exchangeTimeMicros);
        bookHandler.tryOrderMatching(exchangeTimeMicros, book);
    }

    @SuppressWarnings("unused")
    private void showExchangeBook(final Side side) {
        for (int i = 0; i < 5; i++) {
            log.debug("[{}] {} {} RealBook: {}@{} <> ExchangeBook: {}@{}", i, simMarketBook.getContract(), side.switchSide(),
                side.switchSide().getQty(histMarketbook, i), side.switchSide().getPrice(histMarketbook, i),
                simMarketBook.getHittableQty(side, i), simMarketBook.getAtMarketPrice(side, i));
        }
    }

    boolean handleOrderMod(final SimulatedOrder order) {
        if (internalCancel(order.getClOrdId())) {
            internalAdd(order);
            return true;
        }
        return false;
    }

    boolean internalCancel(final long clOrdId) {
        if (bookManager.contains(clOrdId)) {
            bookManager.remove(clOrdId);
            return true;
        }
        return false;
    }

    @Deprecated
    void internalAdd(final SimulatedOrder order) {
        final Side side = order.getSide();
        final long restingPrice = order.getMantissaPrice();
        final Price hittablePrice = simMarketBook.getAtMarketPrice(side);

        if (PriceUtils.isInsideOfOrEqualTo(side, restingPrice, hittablePrice.getMantissa())) {
            handleExecutablePrice(order);
        }
        if (order.getQuantity() > 0) {
            handleRemaining(order);
        }
    }

    private void toFill(final SimulatedOrder order, final Price px, final long qty, final long remainingQty, final int bookLevel) {
        final double price = px.asDouble();
        final double commission = bookManager.getCommission(order.getSide(), qty, price);

        final OrderFill fill = fillFactory.get();
        fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), qty, price, commission);
        fill.setRemainingQty(remainingQty);
        switch (order.getTimeCondition()) {
            case GFS:
            case GFD:
                delayedQueue.queueDelayedFill(clock.arrivalTimeMicros, fill);
                break;
            case IOC:
                delayedQueue.queueDelayedIOCFill(clock.arrivalTimeMicros, fill);
            default:
                break;
        }
        log.debug("[Exec] Fill: {} Book: {}@{}/{}@{} ({})", order,
            histMarketbook.getBidQty(bookLevel), histMarketbook.getBidPrice(bookLevel),
            histMarketbook.getAskQty(bookLevel), histMarketbook.getAskPrice(bookLevel),
            histMarketbook.getExchangeDisplayTimeMicros());
    }

    private void toQueue(final SimulatedOrder order, final long position) {
        order.setPosition(position, clock.arrivalTimeMicros);
        bookManager.add(order);
        log.debug("[Exec] Queued: {} #{}", order, order.getPriority());
    }

    private void toMiss(final SimulatedOrder order) {
        final Ack ack = ackFactory.get();
        ack.setAck(order.getContract(), order.getSide(), order.getTimeCondition(), ExecType.NEWORDER_ACK, order.getClOrdId());
        delayedQueue.queueDelayedMissedAck(clock.arrivalTimeMicros, ack);
        log.debug("[Exec] Missed: {} Book: {}@{}/{}@{} ({})", order,
            histMarketbook.getBidQty(), histMarketbook.getBidPrice(), histMarketbook.getAskQty(), histMarketbook.getAskPrice(), histMarketbook.getExchangeDisplayTimeMicros());
    }

    private void handleExecutablePrice(final SimulatedOrder order) {
        int bookLevel = 0;

        Price bestHittablePrice = simMarketBook.getAtMarketPrice(order.getSide()); // current book - price
        long bestHittableQty = simMarketBook.getHittableQty(order.getSide()); // current book - qty
        if (bestHittableQty <= 0) {
            log.debug("[NoEvent] Marketbook price {} is inside of {} but predicted qty {} is not available",
                bestHittablePrice.asDouble(), order.getPrice().asDouble(), bestHittableQty);
            return;
        }

        do {
            final Price fillPrice = PriceUtils.getOutsidePrice(order.getSide(), order.getPrice(), bestHittablePrice);
            final long fillQty = SimMatchingUtils.getFillableQty(order.getQuantity(), bestHittableQty);
            final long remainingQty = SimMatchingUtils.getRemainingQty(order.getQuantity(), bestHittableQty);

            order.fill(fillQty);
            toFill(order, fillPrice, fillQty, remainingQty, bookLevel);

            clock.setFillPriceResetMicros(clock.arrivalTimeMicros + fillPriceBanningDelayMicros);

            simMarketBook.onOrderMatching(clock.fillPriceResetMicros, order.getSide().switchSide(), fillPrice, fillQty);
            if (simMarketBook.getHittableQty(order.getSide()) <= 0) {
                bookLevel++;
                bestHittablePrice = simMarketBook.getAtMarketPrice(order.getSide(), bookLevel);
                bestHittableQty = simMarketBook.getHittableQty(order.getSide(), bookLevel);
            }
        } while (order.getQuantity() > 0 && bestHittableQty > 0 && PriceUtils.isInsideOfOrEqualTo(order.getSide(), order.getPrice(), bestHittablePrice));
    }

    private void handleRemaining(final SimulatedOrder order) {
        switch (order.getTimeCondition()) {
            case GFS:
            case GFD:
                toQueue(order, simMarketBook.getQty(order.getSide(), order.getPrice()));
                break;
            case IOC:
                toMiss(order);
                break;
            default:
                break;
        }
    }

    // private void tryUpdatePriority(final SimulatedOrder order) {
    // final long qty = simMarketBook.getQty(order.getSide(), order.getPrice());
    // if (order.isReestimationRequired() || (order.getPriority() > qty && qty > 0)) {
    // log.debug("Update order {} priority {} <from {}", order.getClOrdId(), order.getPriority(), qty);
    // order.updatePriority(qty);
    // }
    // }

    class InternalClock {
        private long fillPriceResetMicros;
        private long arrivalTimeMicros;
        private long lastQuoteEventTime;
        private long currentQuoteEventTime;

        long getFillPriceBanningDelayMicros() {
            return fillPriceBanningDelayMicros;
        }

        public long getFillPriceResetMicros() {
            return fillPriceResetMicros;
        }

        public void setFillPriceResetMicros(final long fillPriceResetMicros) {
            this.fillPriceResetMicros = fillPriceResetMicros;
        }

        public void setArrivalTimeMicros(final long arrivalTimeMicros) {
            this.arrivalTimeMicros = arrivalTimeMicros;
        }

        public void setLastQuoteEventTime(final long lastQuoteEventTime) {
            this.lastQuoteEventTime = lastQuoteEventTime;
        }

        public void setCurrentQuoteEventTime(final long currentQuoteEventTime) {
            this.currentQuoteEventTime = currentQuoteEventTime;
        }

        long getFillEventTime(final long eventTimeMicros, final long enqueuedTimeMicros, final double ratio) {
            if (enqueuedTimeMicros <= this.lastQuoteEventTime) {
                return this.lastQuoteEventTime + (long) ((eventTimeMicros - this.lastQuoteEventTime) * (ratio >= 1.0 ? 1.0 : ratio));
            } else {
                return enqueuedTimeMicros + (long) ((eventTimeMicros - enqueuedTimeMicros) * (ratio >= 1.0 ? 1.0 : ratio));
            }
        }

        void updateEventTime(final long eventTimeMicros) {
            if (eventTimeMicros != this.currentQuoteEventTime) {
                this.setLastQuoteEventTime(this.currentQuoteEventTime);
                this.setCurrentQuoteEventTime(eventTimeMicros);
            }
        }
    }

    class InternalOrderBookManager {
        private final Map<Long, SimulatedOrder> clOrdIdToOrder = new HashMap<>();
        private final EffectiveOrders enqueueOrders;
        private long holdingPosition = 0l;

        public InternalOrderBookManager(final EffectiveOrders enqueueOrders) {
            this.enqueueOrders = enqueueOrders;
        }

        void add(final SimulatedOrder order) {
            clOrdIdToOrder.put(order.getClOrdId(), order);
            enqueueOrders.add(order);
        }

        void remove(final SimulatedOrder order) {
            enqueueOrders.remove(order);
            clOrdIdToOrder.remove(order.getClOrdId());
        }

        void remove(final long clOrdId) {
            final SimulatedOrder order = clOrdIdToOrder.remove(clOrdId);
            enqueueOrders.remove(order);
        }

        boolean contains(final long clOrdId) {
            return clOrdIdToOrder.containsKey(clOrdId);
        }

        SimulatedOrder get(final TickType tradeAggressor, final Side matchingSide) {
            if (tradeAggressor.getCode() == matchingSide.getCode()) {
                return enqueueOrders.getBy(matchingSide).peek();
            } else {
                return enqueueOrders.get(tradeAggressor).peek();
            }
        }

        SimulatedOrder get(final Side side) {
            return enqueueOrders.getBy(side).peek();
        }

        void sort(final TickType tickType, final long price, final long availableQty) {
            enqueueOrders.get(tickType).stream().filter(s -> s.getMantissaPrice() == price).forEach((so) -> {
                if (so.getPriority() <= availableQty) {
                    so.updatePriority(0);
                    log.info("[Queued] Setting position to {} {} {} {}@{}",
                        so.getContract(), so.getSide(), so.getClOrdId(), so.getQuantity(), so.getPrice().asDouble());
                } else if (so.getPriority() == 0) {
                    log.info("[Queued] Order already first {} {} {} {}@{}",
                        so.getContract(), so.getSide(), so.getClOrdId(), so.getQuantity(), so.getPrice().asDouble());
                } else {
                    so.updatePriority(so.getPriority() - availableQty);
                    log.debug("[Queued] Update order priority {} {} {} {} {}@{}",
                        so.getPriority(), so.getContract(), so.getSide(), so.getClOrdId(), so.getQuantity(), so.getPrice().asDouble());
                }
            });
        }

        double getCommission(final Side side, final long qty, final double price) {
            final FillType fillType = FeeHelper.getFillType(exchange, side, qty, holdingPosition);
            holdingPosition = fillType.updateHoldingPosition(holdingPosition, qty);
            return FeeHelper.getFee(feeCalculator, f -> f.getFee(qty, price, fillType));
        }

    }

    private class EffectiveOrders {
        private final Queue<SimulatedOrder> idles = new PriorityQueue<>();
        private final Queue<SimulatedOrder> bidOrders = new PriorityQueue<>();
        private final Queue<SimulatedOrder> askOrders = new PriorityQueue<>();

        EffectiveOrders() {
        }

        void add(final SimulatedOrder order) {
            this.getBy(order.getSide()).add(order);
        }

        void remove(final SimulatedOrder order) {
            this.getBy(order.getSide()).remove(order);
        }

        Queue<SimulatedOrder> getBy(final Side side) {
            switch (side) {
                case BUY:
                    return bidOrders;
                case SELL:
                    return askOrders;
                default:
                    throw new RuntimeException("Invalid tick type request");
            }
        }

        Queue<SimulatedOrder> get(final TickType type) {
            switch (type) {
                case BUYER:
                    return askOrders;
                case SELLER:
                    return bidOrders;
                case NO_AGGRESSOR:
                    return idles;
                default:
                    throw new RuntimeException("Invalid tick type request");
            }
        }

    }

}
