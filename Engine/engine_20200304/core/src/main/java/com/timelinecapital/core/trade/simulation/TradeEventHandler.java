package com.timelinecapital.core.trade.simulation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.TickView;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalClock;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor.InternalOrderBookManager;
import com.timelinecapital.core.util.AggressorToRestingOrderHelper;

public class TradeEventHandler implements MDTradeEvent<TickView> {
    private static final Logger log = LogManager.getLogger(TradeEventHandler.class);

    private final InternalOrderBookManager bookManager;
    private final InternalClock clock;
    private final ExchangeEventQueue delayedQueue;
    private final FillFactory fillFactory;
    private final double tickSize;

    private ExchangeMarketBook simMarketBook;

    public TradeEventHandler(final InternalOrderBookManager bookManager, final InternalClock clock, final ExchangeEventQueue delayedQueue, final FillFactory fillFactory,
        final double tickSize) {
        this.bookManager = bookManager;
        this.clock = clock;
        this.delayedQueue = delayedQueue;
        this.fillFactory = fillFactory;
        this.tickSize = tickSize;
    }

    @Override
    public void set(final ExchangeMarketBook marketbook) {
        this.simMarketBook = marketbook;
    }

    @Override
    public void update(final long eventTimeMicros, final TickView tickView) {
        simMarketBook.updateFrom(eventTimeMicros, tickView);
    }

    @Override
    public void tryOrderMatching(final long eventTimeMicros, final TickView trade) {
        final long tradePx = PriceUtils.toMantissa(trade.getPrice(), tickSize);
        long fillableQty = trade.getQuantity();

        SimulatedOrder order;

        /*-
         * Check resting order side:
         *   SELLER price is the same as AskOrders
         *   BUYER  price is the same as BidOrders
         */
        while ((order = bookManager.get(trade.getType(), AggressorToRestingOrderHelper.ofEnteringSide(trade.getType()))) != null
            && PriceUtils.isOutsideOfOrEqualTo(order.getSide(), tradePx, order.getMantissaPrice())) {

            final double commission = bookManager.getCommission(order.getSide(), order.getQuantity(), order.getDoublePrice());
            final OrderFill fill = fillFactory.get();
            fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), order.getQuantity(), order.getDoublePrice(), commission);
            bookManager.remove(order);

            final long fillTimeMicros = clock.getFillEventTime(eventTimeMicros, order.getEnqueuedMicros(), ((double) order.getQuantity() / fillableQty));

            delayedQueue.queueDelayedFill(fillTimeMicros, fill);
            log.debug("[Exec] Fill: {} cross by Trade: {} {} {}@{}", order, trade.getContract(), trade.getType(), trade.getQuantity(), trade.getPrice());
        }

        /*-
         * Check aggressor side:
         *   BUYER  -> AskOrders
         *   SELLER -> BidOrders
         */
        while ((order = bookManager.get(trade.getType(), AggressorToRestingOrderHelper.ofCrossingSide(trade.getType()))) != null
            && fillableQty > 0) {

            if (PriceUtils.isInsideOf(order.getSide(), tradePx, order.getMantissaPrice())) {
                log.debug("[NoEvent] Trade price ({}) {} <-> {} ({}) Resting price", trade.getType(), trade.getPrice(), order.getPrice().asDouble(), order.getSide());
                break;
            }

            final long priorityQty = order.getPriority();
            if (fillableQty <= priorityQty) {
                log.debug("[NoEvent] Trade qty ({}) {}@{} <-> {}@{} ({}) Resting qty #{}",
                    trade.getType(), fillableQty, trade.getPrice(), priorityQty, order.getPrice().asDouble(), order.getSide(), priorityQty);
                // TODO should update priority?
                break;
            }

            final long qtyToFill = priorityQty + order.getQuantity();
            long fillTimeMicros = eventTimeMicros;

            final OrderFill fill = fillFactory.get();
            if (fillableQty >= qtyToFill) {
                final double commission = bookManager.getCommission(order.getSide(), order.getQuantity(), order.getDoublePrice());
                fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), order.getQuantity(), order.getDoublePrice(), commission);

                bookManager.remove(order);

                fillTimeMicros = clock.getFillEventTime(eventTimeMicros, order.getEnqueuedMicros(), ((double) qtyToFill / fillableQty));
                fillableQty -= qtyToFill;
                log.debug("[Exec] FullFill onTrade ; {}", order);
            } else {
                final long filledQty = fillableQty - priorityQty;
                final double commission = bookManager.getCommission(order.getSide(), filledQty, order.getDoublePrice());
                fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), filledQty, order.getDoublePrice(), commission);
                fill.setRemainingQty(order.getQuantity() - filledQty);

                order.fill(filledQty);
                fillTimeMicros = clock.getFillEventTime(eventTimeMicros, order.getEnqueuedMicros(), ((double) priorityQty / fillableQty));
                order.updatePriority(0);

                fillableQty = fillableQty - filledQty - priorityQty;
                log.debug("[Exec] PartialFill onTrade ; {} (Hit: {})", order, filledQty);
            }

            delayedQueue.queueDelayedFill(fillTimeMicros, fill);
            log.debug("[Exec] Fill at Trade {} {} {}@{} ", trade.getContract(), trade.getType(), trade.getQuantity(), trade.getPrice());
        }

        final long availableQty = fillableQty;
        bookManager.sort(trade.getType(), tradePx, availableQty);

    }

}
