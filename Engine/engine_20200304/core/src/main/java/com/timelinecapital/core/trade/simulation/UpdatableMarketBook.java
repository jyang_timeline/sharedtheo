package com.timelinecapital.core.trade.simulation;

import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.TickView;

@Deprecated
public interface UpdatableMarketBook {

    // void replaceWith(long exchangeTimeMicros, MarketBook marketbook);

    // void updateFrom(long exchangeTimeMicros, MarketBook marketbook);

    default void updateFrom(final long exchangeTimeMicros, final TickView tick) {
    }

    default void updateFrom(final long exchangeTimeMicros, final QuoteOrderView orderAction) {
    }

    default void updateFrom(final long exchangeTimeMicros, final BestOrderView bestOrder) {
    }

}
