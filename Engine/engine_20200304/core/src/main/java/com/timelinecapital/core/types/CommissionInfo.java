package com.timelinecapital.core.types;

import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.Exchange;

public class CommissionInfo {

    private final Exchange exchange;
    private final Side side;
    private final double ratio;

    public CommissionInfo(final Exchange exchange, final Side side, final double ratio) {
        this.exchange = exchange;
        this.side = side;
        this.ratio = ratio;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public Side getSide() {
        return side;
    }

    public double getRatio() {
        return ratio;
    }

}
