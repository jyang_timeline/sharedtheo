package com.timelinecapital.core.types;

public enum EndpointType {

    FEED(1),
    TRADE(2),
    ;

    private final int type;

    EndpointType(final int type) {
        this.type = type;
    }

    public int toInt() {
        return type;
    }

    public static EndpointType fromInt(final int input) {
        switch (input) {
            case 1:
                return FEED;
            case 2:
                return TRADE;
            default:
                return null;
        }
    }

}
