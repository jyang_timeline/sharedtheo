package com.timelinecapital.core.types;

import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.util.ExchangeProtocolUtil;

public class FeedInfo {

    private final Exchange exchange;
    private final FeedType feedType;
    private final ContentType contentType;
    private final boolean isSnapshotOnly;

    public FeedInfo(final Exchange exchange, final FeedType feedType, final ContentType contentType) {
        this.exchange = exchange;
        this.feedType = feedType;
        this.contentType = contentType;
        isSnapshotOnly = ExchangeProtocolUtil.isQuoteSnapshot(exchange.name());
    }

    public Exchange getExchange() {
        return exchange;
    }

    public FeedType getFeedType() {
        return feedType;
    }

    public ContentType getFeedPreference() {
        return contentType;
    }

    public boolean isSnapshotOnly() {
        return isSnapshotOnly;
    }

}
