package com.timelinecapital.core.types;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum FeedPreference {

    DEFAULT(0),
    LEVEL1_ONLY(1),
    LEVEL2_ONLY(2),
    ALL(3);

    private static final Map<Integer, FeedPreference> codes;

    private final int code;

    static {
        final Map<Integer, FeedPreference> tmp = new HashMap<Integer, FeedPreference>(FeedPreference.values().length);
        for (final FeedPreference item : FeedPreference.values()) {
            tmp.put(item.code, item);
        }
        codes = Collections.unmodifiableMap(tmp);
    }

    private FeedPreference(final int code) {
        this.code = code;
    }

    public static FeedPreference lookup(final Integer code) {
        return codes.get(code);
    }

    public int getCode() {
        return this.code;
    }

}
