package com.timelinecapital.core.types;

import com.timelinecapital.commons.type.Price;

public interface OrderBookPriceLevel {

    Price getPrice();

    long getQty();

    void setQty(long qty);

    long getHittableQty();

    void setHittableQty(long hittableQty);

    long getResetTimeMicros();

    void setResetTimeMicros(long resetTimeMicros);

    boolean isDirty();

    void setDirty();

    boolean isValid();

    void clear();

    void refreshFromAnotherEntry(final OrderBookPriceLevel entry);

    void tryUpdateQty(long qty, long exchangeTimeMicros);

}
