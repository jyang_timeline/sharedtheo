package com.timelinecapital.core.types;

public enum PriceLevelPolicy {

    SinglePriceLevel,

    MultiplePriceLevel;

}
