package com.timelinecapital.core.types;

import org.joda.time.Interval;

import com.timelinecapital.strategy.state.ExchangeSession;

public class SessionIdentifier implements Comparable<SessionIdentifier> {
    private final ExchangeSession session;
    private final Interval interval;
    private final String desc;

    public SessionIdentifier(final ExchangeSession session, final Interval interval) {
        this.session = session;
        this.interval = interval;
        desc = session.name() + ":" + interval.getStart() + "/" + interval.getEnd();
    }

    public ExchangeSession getSession() {
        return session;
    }

    public Interval getInterval() {
        return interval;
    }

    @Override
    public int compareTo(final SessionIdentifier o) {
        int result = Integer.compare(session.getSeq(), o.session.getSeq());
        if (result == 0) {
            result = Long.compare(interval.getStartMillis(), o.interval.getStartMillis());
            if (result == 0) {
                return Long.compare(interval.getEndMillis(), o.interval.getEndMillis());
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return desc;
    }

}
