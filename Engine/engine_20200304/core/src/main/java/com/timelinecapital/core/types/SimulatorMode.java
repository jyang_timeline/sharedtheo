package com.timelinecapital.core.types;

public enum SimulatorMode {

    BASIC(1),
    PSEUDO_BOOK_DOUBLE_PRICE(2),
    PSEUDO_BOOK_MANTISSA_PRICE(3);

    private int intValue;

    private SimulatorMode(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public static SimulatorMode fromInt(final int input) {
        switch (input) {
            case 1:
                return BASIC;
            case 2:
                return PSEUDO_BOOK_DOUBLE_PRICE;
            case 3:
                return PSEUDO_BOOK_MANTISSA_PRICE;
            default:
                return BASIC;
        }
    }

}
