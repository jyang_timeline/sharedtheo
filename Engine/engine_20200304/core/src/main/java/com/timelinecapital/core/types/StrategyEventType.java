package com.timelinecapital.core.types;

public enum StrategyEventType {

    ContractRefresh,
    PositionSync,
    TimeUpdate,
    ;

}
