package com.timelinecapital.core.types;

public enum UpdateType {

    TRADE_ONLY(1),

    MARKETBOOK_ONLY(2),

    ALL(3);

    private final int type;

    UpdateType(final int type) {
        this.type = type;
    }

    public int toInt() {
        return type;
    }

    public static UpdateType fromInt(final int input) {
        switch (input) {
            case 1:
                return TRADE_ONLY;
            case 2:
                return MARKETBOOK_ONLY;
            case 3:
            default:
                return ALL;
        }
    }

}
