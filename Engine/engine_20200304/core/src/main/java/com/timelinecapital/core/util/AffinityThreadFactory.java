package com.timelinecapital.core.util;

import java.util.concurrent.ThreadFactory;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.openhft.affinity.AffinityLock;
import net.openhft.affinity.AffinityStrategies;
import net.openhft.affinity.AffinityStrategy;

public final class AffinityThreadFactory implements ThreadFactory {
    private final String assignedName;
    private final boolean daemon;
    private final boolean coreLock;
    @NotNull
    private final AffinityStrategy[] strategies;
    @Nullable
    private AffinityLock lastAffinityLock = null;
    private int id = 1;

    public AffinityThreadFactory(final String assignedName, final AffinityStrategy... strategies) {
        this(assignedName, false, false, strategies);
    }

    public AffinityThreadFactory(final String assignedName, final boolean coreLock, final AffinityStrategy... strategies) {
        this(assignedName, false, coreLock, strategies);
    }

    public AffinityThreadFactory(final String assignedName, final boolean daemon, final boolean coreLock, @NotNull final AffinityStrategy... strategies) {
        this.assignedName = assignedName;
        this.daemon = daemon;
        this.coreLock = coreLock;
        this.strategies = strategies.length == 0 ? new AffinityStrategy[] { AffinityStrategies.ANY } : strategies;
    }

    @NotNull
    @Override
    public synchronized Thread newThread(@NotNull final Runnable runnable) {
        final String threadName = id <= 1 ? assignedName : (assignedName + '-' + id);
        id++;
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                AffinityLock al;
                if (coreLock) {
                    al = AffinityLock.acquireCore();
                } else {
                    al = lastAffinityLock == null ? AffinityLock.acquireLock() : lastAffinityLock.acquireLock(strategies);
                }
                try {
                    if (al.cpuId() >= 0) {
                        lastAffinityLock = al;
                    }
                    runnable.run();
                } finally {
                    al.release();
                }
            }
        }, threadName);
        t.setDaemon(daemon);
        return t;
    }

}
