package com.timelinecapital.core.util;

import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickType;

public class AggressorToRestingOrderHelper {

    public static Side ofEnteringSide(final TickType tickType) {
        switch (tickType) {
            case BUYER:
                return Side.BUY;
            case SELLER:
                return Side.SELL;
            default:
                return Side.INVALID;
        }
    }

    public static Side ofCrossingSide(final TickType tickType) {
        switch (tickType) {
            case BUYER:
                return Side.SELL;
            case SELLER:
                return Side.BUY;
            default:
                return Side.INVALID;
        }
    }

}
