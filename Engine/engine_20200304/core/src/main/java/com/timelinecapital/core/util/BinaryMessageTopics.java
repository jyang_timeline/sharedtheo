package com.timelinecapital.core.util;

import com.nogle.messaging.Messenger;

public class BinaryMessageTopics {

    public static final String HEADER_HBT_ENGINE = "TE/Engine/Heartbeat";

    public static final String HEADER_HBT_CONTRACT = "TE/Contract/Heartbeat";

    public static final String HEADER_HBT_STRATEGY = Messenger.getClientId() + "/Strategy/Heartbeat";
    public static final String HEADER_LOG_STRATEGY = Messenger.getClientId() + "/Strategy/Log";

}
