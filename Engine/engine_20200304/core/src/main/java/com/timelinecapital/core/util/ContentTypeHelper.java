package com.timelinecapital.core.util;

import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.types.FeedPreference;

public class ContentTypeHelper {

    public static ContentType fromFeedPreference(final String exchange) {
        final FeedPreference pref = FeedPreference.lookup(EngineConfig.getExchangeToFeedFilter().get(exchange));
        switch (pref) {
            case LEVEL1_ONLY:
                return ContentType.Level1_Quote;
            case LEVEL2_ONLY:
                return ContentType.Level2_Quote;
            case DEFAULT:
            case ALL:
                return ContentType.All_Quote;
        }
        return ContentType.All_Quote;
    }

    public static ContentType fromDepth(final int bidDepth, final int askDepth) {
        if (bidDepth <= 1 && askDepth <= 1) {
            return ContentType.Level1_Quote;
        }
        return ContentType.Level2_Quote;
    }

}
