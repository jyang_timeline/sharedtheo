package com.timelinecapital.core.util;

import com.lmax.disruptor.ExceptionHandler;
import com.nogle.core.strategy.event.StrategyEvent;

final class DisruptorDefaultExceptionHandler implements ExceptionHandler<StrategyEvent> {

    @Override
    public final void handleEventException(final Throwable throwable, final long sequence, final StrategyEvent event) {
        final StringBuilder sb = new StringBuilder(512);
        sb.append("EventTask error handling event seq=").append(sequence).append(", value='");
        try {
            sb.append(event);
        } catch (final Exception ignored) {
            sb.append("[ERROR calling ").append(event.getClass()).append(".toString(): ");
            sb.append(ignored).append("]");
        }
        sb.append("':");
        System.err.println(sb);
        throwable.printStackTrace();
    }

    @Override
    public final void handleOnStartException(final Throwable throwable) {
        System.err.println("EventTask error starting:");
        throwable.printStackTrace();
    }

    @Override
    public final void handleOnShutdownException(final Throwable throwable) {
        System.err.println("EventTask error shutting down:");
        throwable.printStackTrace();
    }

}
