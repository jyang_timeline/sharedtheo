package com.timelinecapital.core.util;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.Integers;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.SleepingWaitStrategy;
import com.lmax.disruptor.TimeoutBlockingWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.nogle.core.strategy.event.StrategyEvent;
import com.timelinecapital.core.config.EngineConfig;

public final class DisruptorUtil {
    private static final Logger log = LogManager.getLogger(DisruptorUtil.class);
    private static final int RINGBUFFER_MIN_SIZE = 128;
    private static final int RINGBUFFER_DEFAULT_SIZE = 256;
    // private static final int RINGBUFFER_NO_GC_DEFAULT_SIZE = 4 * 1024;

    private DisruptorUtil() {
    }

    public static WaitStrategy createWaitStrategy() {
        final String strategy = EngineConfig.getRingStrategy("BUSYSPIN");
        log.info("RingBuffer wait strategy {}", strategy);
        final String strategyUp = strategy.toUpperCase(Locale.ROOT); // TODO Refactor into Strings.toRootUpperCase(String)
        switch (strategyUp) { // TODO Define a DisruptorWaitStrategy enum?
            case "SLEEP":
                return new SleepingWaitStrategy();
            case "YIELD":
                return new YieldingWaitStrategy();
            case "BLOCK":
                return new BlockingWaitStrategy();
            case "BUSYSPIN":
                return new BusySpinWaitStrategy();
            case "TIMEOUT":
                return new TimeoutBlockingWaitStrategy(1, TimeUnit.MICROSECONDS);
            default:
                return new BusySpinWaitStrategy();
        }
    }

    public static int calculateRingBufferSize() {
        int ringBufferSize = RINGBUFFER_DEFAULT_SIZE;
        final int userPreferredRBSize = EngineConfig.getRingSize(ringBufferSize);
        try {
            int size = userPreferredRBSize;
            if (size < RINGBUFFER_MIN_SIZE) {
                size = RINGBUFFER_MIN_SIZE;
                log.warn("Invalid RingBufferSize {}, using minimum size {}.", userPreferredRBSize, RINGBUFFER_MIN_SIZE);
            }
            ringBufferSize = size;
        } catch (final Exception ex) {
            log.warn("Invalid RingBufferSize {}, using default size {}.", userPreferredRBSize, ringBufferSize);
        }
        return Integers.ceilingNextPowerOfTwo(ringBufferSize);
    }

    public static ExceptionHandler<StrategyEvent> getStrategyEventExceptionHandler() {
        return new DisruptorDefaultExceptionHandler();
    }

    // static ExceptionHandler<AsyncLoggerConfigDisruptor.Log4jEventWrapper> getAsyncLoggerConfigExceptionHandler() {
    // final String cls = PropertiesUtil.getProperties().getStringProperty("AsyncLoggerConfig.ExceptionHandler");
    // if (cls == null) {
    // return new AsyncLoggerConfigDefaultExceptionHandler();
    // }
    // try {
    // @SuppressWarnings("unchecked")
    // final Class<? extends ExceptionHandler<AsyncLoggerConfigDisruptor.Log4jEventWrapper>> klass =
    // (Class<? extends ExceptionHandler<AsyncLoggerConfigDisruptor.Log4jEventWrapper>>) LoaderUtil.loadClass(cls);
    // return klass.newInstance();
    // } catch (final Exception ignored) {
    // LOGGER.debug("Invalid AsyncLoggerConfig.ExceptionHandler value: error creating {}: ", cls, ignored);
    // return new AsyncLoggerConfigDefaultExceptionHandler();
    // }
    // }

    // /**
    // * Returns the thread ID of the background appender thread. This allows us to detect Logger.log() calls initiated from the
    // * appender thread, which may cause deadlock when the RingBuffer is full. (LOG4J2-471)
    // *
    // * @param executor runs the appender thread
    // * @return the thread ID of the background appender thread
    // */
    // public static long getExecutorThreadId(final ExecutorService executor) {
    // final Future<Long> result = executor.submit(new Callable<Long>() {
    // @Override
    // public Long call() {
    // return Thread.currentThread().getId();
    // }
    // });
    // try {
    // return result.get();
    // } catch (final Exception ex) {
    // final String msg = "Could not obtain executor thread Id. "
    // + "Giving up to avoid the risk of application deadlock.";
    // throw new IllegalStateException(msg, ex);
    // }
    // }
}