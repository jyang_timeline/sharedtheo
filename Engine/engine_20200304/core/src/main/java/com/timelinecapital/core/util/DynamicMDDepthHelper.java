package com.timelinecapital.core.util;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.config.EngineConfig;

public class DynamicMDDepthHelper {

    public static int getMaximumBookDepth(final String exchange) {
        return EngineConfig.getExchangeToMDDepthMap().getOrDefault(exchange, Exchange.valueOf(exchange).getMdDepth());
    }

    public static int getMaximumOrderQueueDepth() {
        return EngineConfig.getOrderQueueDepth();
    }

}
