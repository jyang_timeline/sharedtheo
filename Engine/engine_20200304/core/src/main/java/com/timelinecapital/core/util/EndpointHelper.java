package com.timelinecapital.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.contract.Endpoints;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.util.DelimiterUtil;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.types.FeedPreference;

public class EndpointHelper {
    private static final Logger log = LogManager.getLogger(EndpointHelper.class);

    private static final Pattern TRADE_ENDPOINT = Pattern.compile("(tcp:\\/\\/)([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+):([0-9]+)");
    private static final Pattern COMMON_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.[0-9]\\.[0-9]+):([0-9]+):(.+)");
    private static final Pattern L1_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.)0(\\.17)(:[0-9]+:.+)");
    private static final Pattern L2_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.)1(\\.17)(:[0-9]+:.+)");
    private static final Pattern BESTPRICE_ORDERDETAIL_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.)0(\\.18)(:[0-9]+:.+)");
    private static final Pattern REALTIME_ORDERACTION_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.)0(\\.19)(:[0-9]+:.+)");
    private static final Pattern REALTIME_ORDERQUEUE_ENDPOINT = Pattern.compile("(mc:\\/\\/)(239\\.255\\.)0(\\.20)(:[0-9]+:.+)");

    private static final String KEY_PREFIX_MULTICAST = "M";
    private static final String KEY_PREFIX_TRADE = "T";

    public static String[] getSnapshotEndpoint(final Endpoints endpoints, final String exchange) {
        final FeedPreference preference = FeedPreference.lookup(EngineConfig.getExchangeToFeedFilter().get(exchange));
        switch (preference) {
            case ALL:
                log.info("Requiring all feeds as input");
                final String snapshotEndpoint = endpoints.getSnapshot();
                return new String[] { snapshotEndpoint, trans(snapshotEndpoint) };
            case LEVEL1_ONLY:
            case LEVEL2_ONLY:
                break;
            case DEFAULT:
            default:
                log.info("Using default endpoint from lookup");
                return new String[] { endpoints.getSnapshot() };
        }
        return new String[] { endpoints.getSnapshot() };
    }

    public static String getIP(final String endpoint) {
        final Matcher matcher = COMMON_ENDPOINT.matcher(endpoint);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return StringUtils.EMPTY;
    }

    public static int getPort(final String endpoint) {
        final Matcher matcher = COMMON_ENDPOINT.matcher(endpoint);
        if (matcher.matches()) {
            return Integer.parseInt(matcher.group(3));
        }
        return 0;
    }

    public static int getTradePort(final String endpoint) {
        final Matcher matcher = TRADE_ENDPOINT.matcher(endpoint);
        if (matcher.matches()) {
            return Integer.parseInt(matcher.group(3));
        }
        return 0;
    }

    public static String getNetworkInterface(final String endpoint) {
        final Matcher matcher = COMMON_ENDPOINT.matcher(endpoint);
        if (matcher.matches()) {
            return matcher.group(4);
        }
        return StringUtils.EMPTY;
    }

    public static String getPresentName(final ConnectionType connectionType, final String endpoint) {
        switch (connectionType) {
            case QUOTE:
                return KEY_PREFIX_MULTICAST
                    + StringUtils.substring(endpoint, StringUtils.ordinalIndexOf(endpoint, DelimiterUtil.IP_DELIMETER, 2) + 1).replaceAll("\\.", StringUtils.EMPTY);
            case TRADE:
                return KEY_PREFIX_TRADE + getTradePort(endpoint) + ":lo";
        }
        return StringUtils.EMPTY;
    }

    public static ContentType getContentType(final String endpoint) {
        final Matcher matcherL1 = L1_ENDPOINT.matcher(endpoint);
        if (matcherL1.matches()) {
            return ContentType.Level1_Quote;
        }
        final Matcher matcherL2 = L2_ENDPOINT.matcher(endpoint);
        if (matcherL2.matches()) {
            return ContentType.Level2_Quote;
        }
        final Matcher matcherBPOD = BESTPRICE_ORDERDETAIL_ENDPOINT.matcher(endpoint);
        if (matcherBPOD.matches()) {
            return ContentType.BestPriceOrderDetail;
        }
        final Matcher matcherRTOA = REALTIME_ORDERACTION_ENDPOINT.matcher(endpoint);
        if (matcherRTOA.matches()) {
            return ContentType.RealTimeOrderAction;
        }
        final Matcher matcherRTOQ = REALTIME_ORDERQUEUE_ENDPOINT.matcher(endpoint);
        if (matcherRTOQ.matches()) {
            return ContentType.RealTimeOrderQueue;
        }
        return ContentType.Level1_Quote;
    }

    private static String trans(final String endpoint) {
        final Matcher matcherLevel1 = L1_ENDPOINT.matcher(endpoint);
        if (matcherLevel1.matches()) {
            return matcherLevel1.group(1) + matcherLevel1.group(2) + 1 + matcherLevel1.group(3) + matcherLevel1.group(4);
        }
        final Matcher matcherLevel2 = L2_ENDPOINT.matcher(endpoint);
        if (matcherLevel2.matches()) {
            return matcherLevel2.group(1) + matcherLevel2.group(2) + 0 + matcherLevel2.group(3) + matcherLevel2.group(4);
        }
        return endpoint;
    }

}
