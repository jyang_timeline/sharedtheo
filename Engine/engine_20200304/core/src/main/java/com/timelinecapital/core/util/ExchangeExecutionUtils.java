package com.timelinecapital.core.util;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.commons.type.StockPriceCNDouble;
import com.timelinecapital.commons.type.StockPriceCNMantissa;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.trade.simulation.ExchangeMarketBook;
import com.timelinecapital.core.trade.simulation.ExecutorMarketBookWithDoublePrice;
import com.timelinecapital.core.trade.simulation.ExecutorMarketBookWithMantissaPrice;

public class ExchangeExecutionUtils {

    public static Price ofPrice() {
        switch (SimulationConfig.getSimulatorMode()) {
            case BASIC:
            case PSEUDO_BOOK_DOUBLE_PRICE:
                return new StockPriceCNDouble();
            case PSEUDO_BOOK_MANTISSA_PRICE:
                return new StockPriceCNMantissa();
        }
        return new StockPriceCNDouble();
    }

    public static ExchangeMarketBook ofExecutorBook(final Contract contract) {
        switch (SimulationConfig.getSimulatorMode()) {
            case BASIC:
                throw new RuntimeException("BASIC does not use executor book");
            case PSEUDO_BOOK_DOUBLE_PRICE:
                return new ExecutorMarketBookWithDoublePrice(contract);
            case PSEUDO_BOOK_MANTISSA_PRICE:
                return new ExecutorMarketBookWithMantissaPrice(contract);
        }
        throw new RuntimeException("Eexecutor book not specified");
    }

}
