package com.timelinecapital.core.util;

import java.util.Arrays;
import java.util.List;

import com.nogle.core.config.EngineMode;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.instrument.SecurityType;

public class ExchangeProtocolUtil {

    private static List<FeedType> sourceByDiff = Arrays.asList(FeedType.Trade, FeedType.MarketBook);
    private static List<FeedType> sourceBySnpt = Arrays.asList(FeedType.Snapshot);
    private static List<FeedType> indices = Arrays.asList(FeedType.Trade);

    public static boolean isQuoteSnapshot(final String exchange) {
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            return false;
        }
        return Exchange.valueOf(exchange).isSnapshot() == EngineConfig.getExchangeToDefaultFeedSpec().get(exchange).booleanValue();
    }

    public static List<FeedType> getQuoteFeedType(final String exchange, final SecurityType securityType) {
        if (SecurityType.INDICES.equals(securityType)) {
            return indices;
        }
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            return sourceByDiff;
        }
        if (Exchange.valueOf(exchange).isSnapshot() == EngineConfig.getExchangeToDefaultFeedSpec().get(exchange).booleanValue()) {
            return sourceBySnpt;
        }
        return sourceByDiff;
    }

}
