package com.timelinecapital.core.util;

public enum ExchangeTimeFormat {

    CN_ISO8061_MILLIS("hhmmssfff"),
    CN_ISO8061_MICROS("hhmmssffffff"),
    UNIX("epoch"),
    ;

    private final String format;

    private ExchangeTimeFormat(final String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public static ExchangeTimeFormat fromString(final String format) {
        for (final ExchangeTimeFormat e : ExchangeTimeFormat.values()) {
            if (e.format.contentEquals(format)) {
                return e;
            }
        }
        return ExchangeTimeFormat.UNIX;
    }

}
