package com.timelinecapital.core.util;

import java.util.function.Function;

import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FillType;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.Exchange;

public class FeeHelper {

    public static boolean isEquityTrading(final String exchange) {
        return Exchange.SSE.name().contentEquals(exchange) || Exchange.SZSE.name().contentEquals(exchange);
    }

    public static FillType getFillType(final String exchange, final Side side, final long fillQty, final long holdingPosition) {
        if (FeeHelper.isEquityTrading(exchange)) {
            switch (side) {
                case BUY:
                    return FillType.EQT_LONG;
                case SELL:
                    return FillType.EQT_SHORT;
                default:
                    throw new RuntimeException("Not supported");
            }
        } else {
            if (holdingPosition == 0 || holdingPosition < fillQty) {
                return FillType.FUT_OPEN;
            } else {
                return FillType.FUT_CLOSE;
            }
        }
    }

    public static double getFee(final FeeCalculator c, final Function<FeeCalculator, Double> f) {
        return f.apply(c);
    }

}
