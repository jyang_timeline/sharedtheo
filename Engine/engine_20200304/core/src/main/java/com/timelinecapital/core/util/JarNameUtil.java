package com.timelinecapital.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JarNameUtil {
    private static final Logger log = LogManager.getLogger(JarNameUtil.class);

    private static final Pattern ACCOUNT_PATTERN = Pattern.compile("(\\S+)&&(\\S+)#(\\S+)(\\.jar)");
    private static final Pattern DEFAULT_PATTERN = Pattern.compile("(\\S+)&&([^#]+)(\\.jar)");

    public static String getBrokerAccountName(final String fileName) {
        log.info("Checking {}", fileName);
        final Matcher accountMatcher = ACCOUNT_PATTERN.matcher(fileName);
        if (accountMatcher.matches()) {
            return accountMatcher.group(2);
        }
        return StringUtils.EMPTY;
    }

    public static String getTraderAccountName(final String fileName) {
        final Matcher accountMatcher = ACCOUNT_PATTERN.matcher(fileName);
        if (accountMatcher.matches()) {
            return accountMatcher.group(1);
        }
        final Matcher defaultMatcher = DEFAULT_PATTERN.matcher(fileName);
        if (defaultMatcher.matches()) {
            return defaultMatcher.group(1);
        }
        return StringUtils.EMPTY;
    }

}
