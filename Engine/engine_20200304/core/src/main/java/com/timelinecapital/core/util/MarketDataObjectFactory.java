package com.timelinecapital.core.util;

import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.UpdateFlag;
import com.timelinecapital.core.factory.ContractFactory;

public class MarketDataObjectFactory {

    public static TickView getTick() {
        return tick;
    }

    public static BookView getMarketBook() {
        return marketbook;
    }

    private static Contract contract = ContractFactory.getSelfDefinedContract("IF1601@CFFEX");

    private static TickView tick = new TickView() {

        @Override
        public long getVolume() {
            return 0;
        }

        @Override
        public long getUpdateTimeMillis() {
            return 0;
        }

        @Override
        public long getUpdateTimeMicros() {
            return 0;
        }

        @Override
        public TickType getType() {
            return TickType.NO_AGGRESSOR;
        }

        @Override
        public long getSequenceNo() {
            return 0;
        }

        @Override
        public long getQuantity() {
            return 0;
        }

        @Override
        public double getPrice() {
            return 0;
        }

        @Override
        public long getOpenInterest() {
            return 0;
        }

        @Override
        public long getExchangeDisplayTimeMicros() {
            return 0;
        }

        @Override
        public long getExchangeUpdateTimeMillis() {
            return 0;
        }

        @Override
        public long getExchangeUpdateTimeMicros() {
            return 0;
        }

        @Override
        public long getExchangeSequenceNo() {
            return 0;
        }

        @Override
        public Contract getContract() {
            return contract;
        }
    };

    private static BookView marketbook = new BookView() {

        @Override
        public long getVolume() {
            return 0;
        }

        @Override
        public long getUpdateTimeMillis() {
            return 0;
        }

        @Override
        public long getUpdateTimeMicros() {
            return 0;
        }

        @Override
        public long getSequenceNo() {
            return 0;
        }

        @Override
        public QuoteType getQuoteType() {
            return QuoteType.INVALID;
        }

        @Override
        public long getOpenInterest() {
            return 0;
        }

        @Override
        public long getExchangeDisplayTimeMicros() {
            return 0;
        }

        @Override
        public long getExchangeUpdateTimeMillis() {
            return 0;
        }

        @Override
        public long getExchangeUpdateTimeMicros() {
            return 0;
        }

        @Override
        public long getExchangeSequenceNo() {
            return 0;
        }

        @Override
        public Contract getContract() {
            return contract;
        }

        @Override
        public UpdateFlag getClosestBookUpdate() {
            return UpdateFlag.TOP;
        }

        @Override
        public long getBidQty(final int arg0) {
            return 0;
        }

        @Override
        public long getBidQty() {
            return 0;
        }

        @Override
        public double getBidPrice(final int arg0) {
            return 0;
        }

        @Override
        public double getBidPrice() {
            return 0;
        }

        @Override
        public int getBidDepth() {
            return 0;
        }

        @Override
        public long getAskQty(final int arg0) {
            return 0;
        }

        @Override
        public long getAskQty() {
            return 0;
        }

        @Override
        public double getAskPrice(final int arg0) {
            return 0;
        }

        @Override
        public double getAskPrice() {
            return 0;
        }

        @Override
        public int getAskDepth() {
            return 0;
        }
    };

}
