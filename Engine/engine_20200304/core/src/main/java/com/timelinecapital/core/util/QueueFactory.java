package com.timelinecapital.core.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.timelinecapital.commons.channel.FeedType;
import com.timelinecapital.core.marketdata.type.BDBestPriceOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDBookView;
import com.timelinecapital.core.marketdata.type.BDOrderActionsView;
import com.timelinecapital.core.marketdata.type.BDOrderQueueView;
import com.timelinecapital.core.marketdata.type.BDSnapshotView;
import com.timelinecapital.core.marketdata.type.BDTickView;
import com.timelinecapital.core.mktdata.type.MarketDataSnapshot;
import com.timelinecapital.core.mktdata.type.VanillaBestPriceOrderDetail;
import com.timelinecapital.core.mktdata.type.VanillaMarketBook;
import com.timelinecapital.core.mktdata.type.VanillaOrderActions;
import com.timelinecapital.core.mktdata.type.VanillaOrderQueue;
import com.timelinecapital.core.mktdata.type.VanillaTick;

public class QueueFactory {

    static Map<Long, Map<FeedType, ConcurrentLinkedQueue<?>>> strategyToFeedEventQueue = new ConcurrentHashMap<>();

    static Map<Long, ConcurrentLinkedQueue<BDOrderQueueView>> strategyToOrderQueueView = new ConcurrentHashMap<>();
    static Map<Long, ConcurrentLinkedQueue<BDOrderActionsView>> strategyToOrderActionView = new ConcurrentHashMap<>();
    static Map<Long, ConcurrentLinkedQueue<BDBestPriceOrderQueueView>> strategyToBestPriceQueueView = new ConcurrentHashMap<>();
    static Map<Long, ConcurrentLinkedQueue<BDBookView>> strategyToMarketBookView = new ConcurrentHashMap<>();
    static Map<Long, ConcurrentLinkedQueue<BDTickView>> strategyToTickView = new ConcurrentHashMap<>();
    static Map<Long, ConcurrentLinkedQueue<BDSnapshotView>> strategyToSnapshot = new ConcurrentHashMap<>();

    public static ConcurrentLinkedQueue<?> getWithConcreteInstance(final long strategyId, final FeedType feedType) {
        Map<FeedType, ConcurrentLinkedQueue<?>> feedToQueue = strategyToFeedEventQueue.get(strategyId);
        if (feedToQueue == null) {
            feedToQueue = new HashMap<>();
            strategyToFeedEventQueue.put(strategyId, feedToQueue);
        }
        if (feedToQueue.get(feedType) == null) {
            switch (feedType) {
                case OrderQueue: {
                    final ConcurrentLinkedQueue<VanillaOrderQueue> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
                case OrderActions: {
                    final ConcurrentLinkedQueue<VanillaOrderActions> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
                case BestPriceOrderDetail: {
                    final ConcurrentLinkedQueue<VanillaBestPriceOrderDetail> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
                case MarketBook: {
                    final ConcurrentLinkedQueue<VanillaMarketBook> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
                case Trade: {
                    final ConcurrentLinkedQueue<VanillaTick> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
                case Snapshot: {
                    final ConcurrentLinkedQueue<MarketDataSnapshot> queue = new ConcurrentLinkedQueue<>();
                    feedToQueue.put(feedType, queue);
                    break;
                }
            }
        }

        return strategyToFeedEventQueue.get(strategyId).get(feedType);
    }

    public static ConcurrentLinkedQueue<BDOrderQueueView> getOrderQueueView(final long strategyId) {
        return strategyToOrderQueueView.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

    public static ConcurrentLinkedQueue<BDOrderActionsView> getOrderActionsView(final long strategyId) {
        return strategyToOrderActionView.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

    public static ConcurrentLinkedQueue<BDBestPriceOrderQueueView> getBestPriceQueueView(final long strategyId) {
        return strategyToBestPriceQueueView.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

    public static ConcurrentLinkedQueue<BDBookView> getMarketBookView(final long strategyId) {
        return strategyToMarketBookView.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

    public static ConcurrentLinkedQueue<BDTickView> getTickView(final long strategyId) {
        return strategyToTickView.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

    public static ConcurrentLinkedQueue<BDSnapshotView> getSnapshot(final long strategyId) {
        return strategyToSnapshot.computeIfAbsent(strategyId, key -> new ConcurrentLinkedQueue<>());
    }

}
