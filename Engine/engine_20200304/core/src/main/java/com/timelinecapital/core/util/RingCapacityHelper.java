package com.timelinecapital.core.util;

import com.nogle.core.config.EngineMode;
import com.timelinecapital.core.config.SimulationConfig;

public class RingCapacityHelper {

    private static int DEFAULT_QUOTE_CAPACITY = 256;

    private static EngineMode mode;

    public static void onEngineMode(final EngineMode mode) {
        RingCapacityHelper.mode = mode;
    }

    public static int getQuoteEventCapacity() {
        if (EngineMode.SIMULATION.equals(mode)) {
            return SimulationConfig.getQuoteEventCapacity();
        }
        return DEFAULT_QUOTE_CAPACITY;
    }

}
