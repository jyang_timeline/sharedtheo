package com.timelinecapital.core.util;

public class ScheduleJobUtil {

    public static final String JOB_KEY_ONETIME = "one_time_notify";

    public static final String JOB_KEY_HEARTBEAT_STRATEGY_SNPT = "heartbeat_strategy_snpt";
    public static final String JOB_KEY_HEARTBEAT_ENGINE_SNPT = "heartbeat_engine_snpt";

    public static final String JOB_KEY_HEARTBEAT_DELTA = "heartbeat_delta";
    public static final String JOB_KEY_HEARTBEAT_METRICS = "heartbeat_metrics";
    public static final String JOB_KEY_HEARTBEAT_POSITION_VIEW = "heartbeat_position_view";

    public static final String JOB_KEY_CONNECTION_CHECK = "connection_check";
    public static final String JOB_KEY_WATCHDOG = "watchdog";
    public static final String JOB_KEY_TRADINGDAY_CHANGE = "day_change";

    public static final String JOB_KEY_TRADINGHOUR_EXCHANGE_LIST = "trading_hour_exchange_list";
    public static final String JOB_KEY_TRADINGHOUR_CHANGE = "trading_hour_change";

    public static final String JOB_KEY_PREOPENING_POS_SYNC = "preopening_position_sync";
    public static final String JOB_KEY_PREOPENING_INFO = "preopening_info";
    public static final String JOB_KEY_MARKET_OPEN = "market_open";

    public static final String TRIGGER_KEY_EVENT_SINGLE = "single_task";
    public static final String TRIGGER_KEY_TIME_CONSTRAINT = "timing_job";

}
