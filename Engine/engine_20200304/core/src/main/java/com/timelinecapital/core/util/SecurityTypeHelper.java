package com.timelinecapital.core.util;

import java.util.regex.Matcher;

import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.instrument.SecurityType;

public class SecurityTypeHelper {

    static final DataType[] DTYPE_SSE = new DataType[] { DataType.MARKETBOOK, DataType.TICK, DataType.ORDERQUEUE };
    static final DataType[] DTYPE_SZSE = new DataType[] { DataType.MARKETBOOK, DataType.TICK, DataType.ORDERACTIONS, DataType.ORDERQUEUE };

    static final DataType[] DTYPE_CFFEX = new DataType[] { DataType.SNAPSHOT };
    static final DataType[] DTYPE_SHFE = new DataType[] { DataType.SNAPSHOT };
    static final DataType[] DTYPE_INE = new DataType[] { DataType.SNAPSHOT };
    static final DataType[] DTYPE_DCE = new DataType[] { DataType.SNAPSHOT };
    static final DataType[] DTYPE_CZCE = new DataType[] { DataType.SNAPSHOT };

    static final DataType[] DTYPE_DEFAULT = new DataType[] { DataType.MARKETBOOK, DataType.TICK };

    public static SecurityType getSecurityType(final String codeName, final String exchange) {
        if (Exchange.SSE.name().contentEquals(exchange) || Exchange.SZSE.name().contentEquals(exchange)) {
            final Matcher sseIndices = SymbolPatternUtil.SSE_INDICES.matcher(codeName);
            final Matcher szseIndices = SymbolPatternUtil.SZSE_INDICES.matcher(codeName);

            if (sseIndices.matches() || szseIndices.matches()) {
                return SecurityType.INDICES;
            } else {
                return SecurityType.STOCKS;
            }
        }

        if (Exchange.CFFEX.name().contentEquals(exchange) || Exchange.SHFE.name().contentEquals(exchange) || Exchange.INE.name().contentEquals(exchange) ||
            Exchange.DCE.name().contentEquals(exchange) || Exchange.CZCE.name().contentEquals(exchange)) {
            final Matcher futures = SymbolPatternUtil.CODENAME_FUTURES.matcher(codeName);
            final Matcher options = SymbolPatternUtil.CODENAME_OPTION_ON_FUTURES.matcher(codeName);

            if (futures.matches()) {
                return SecurityType.FUTURES;
            }
            if (options.matches()) {
                return SecurityType.OPTIONS;
            }
        }

        return SecurityType.FUTURES;
    }

    public static DataType[] getSecurityAvailableDataType(final Exchange exchange) {
        switch (exchange) {
            case CFFEX:
                return DTYPE_CFFEX;
            case SHFE:
                return DTYPE_SHFE;
            case INE:
                return DTYPE_INE;
            case DCE:
                return DTYPE_DCE;
            case CZCE:
                return DTYPE_CZCE;
            case SSE:
                return DTYPE_SSE;
            case SZSE:
                return DTYPE_SZSE;
            default:
                return DTYPE_DEFAULT;
        }
    }

}
