package com.timelinecapital.core.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;

public class SimulationCodecUtils {
    private static final Logger log = LogManager.getLogger(SimulationCodecUtils.class);

    private static final Pattern versionPattern = Pattern.compile("(#Version:)([0-9]+$)");

    static SbeVersion getCodecVersion(final File file) throws IOException {
        SbeVersion codecVersion = SbeVersion.PROT_BASIC;

        final Stream<String> fileStream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
        final String firstLine = fileStream.findFirst().get();
        fileStream.close();

        final Matcher matcher = versionPattern.matcher(firstLine);
        if (matcher.matches()) {
            codecVersion = SbeVersion.fromInt(Integer.parseInt(matcher.group(2)));
        }
        log.info("Codec version: {}", codecVersion);
        return codecVersion;
    }

    static SbeVersion getCodecVersion(final Collection<File> files) throws IOException {
        SbeVersion codecVersion = SbeVersion.PROT_BASIC;
        for (final File file : files) {
            final Stream<String> fileStream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            final String firstLine = fileStream.findFirst().get();
            fileStream.close();

            final Matcher matcher = versionPattern.matcher(firstLine);
            if (matcher.matches()) {
                codecVersion = SbeVersion.fromInt(Integer.parseInt(matcher.group(2)));
            } else {
                codecVersion = SbeVersion.PROT_BASIC;
            }
        }
        log.info("Codec version: {}", codecVersion);
        return codecVersion;
    }

    public static void updateCodecVersionMapping(final Map<Contract, File[]> contractToFile, final Map<Contract, SbeVersion> contractToCodecVersion) throws IOException {
        for (final Entry<Contract, File[]> entry : contractToFile.entrySet()) {
            contractToCodecVersion.put(entry.getKey(), getCodecVersion(entry.getValue()[0]));
        }
    }

}
