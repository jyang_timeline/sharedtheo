package com.timelinecapital.core.util;

import java.util.regex.Pattern;

public class SymbolPatternUtil {

    // private static final Pattern contractPattern = Pattern.compile("(([A-Za-z]+)([0-9]*))@([A-Z]+)");
    // private static final Pattern contractWithRankingPattern = Pattern.compile("(([A-Za-z]+)&([0-9]*))@([A-Z]+)");

    public static final Pattern CN_STOCK_EXCHANGE = Pattern.compile("(.*)@(SSE|SZSE)(\\[#\\])*");

    public static final Pattern CN_INDICES = Pattern.compile("((000[0-9]{3}.SSE)@(SSE))|((399[0-9]{3}.SZSE)@(SZSE))");

    public static final Pattern CN_STOCKS = Pattern.compile("([0-9]+(.SSE|.SZSE)*)@(SSE|SZSE)(\\[#\\])*");

    public static final Pattern SSE_INDICES = Pattern.compile("(000[0-9]{3}.SSE)");

    public static final Pattern SZSE_INDICES = Pattern.compile("(399[0-9]{3}.SZSE)");

    public static final Pattern CODENAME_STOCK = Pattern.compile("(([0-9]*)|([0-9]*.[A-Z]+))");

    public static final Pattern CODENAME_FUTURES = Pattern.compile("(^([A-Za-z]+)([0-9]+))");

    public static final Pattern CODENAME_OPTION_ON_FUTURES = Pattern.compile("(^([A-Za-z]+)([0-9]+)(C|P)([0-9]+))");

    public static final Pattern PREFERENCE = Pattern.compile(".*(\\[#\\])$");

    public static final Pattern SYMBOL_DESCRIPTOR_STOCK = Pattern.compile("(([0-9]*)|([0-9]*.[A-Z]+))@([A-Z]+)(\\[#\\])*");

    public static final Pattern SYMBOL_DESCRIPTOR_FUTURES = Pattern.compile("(^([A-Za-z]+)([0-9]{3,4}))@([A-Z]+)(\\[#\\])*");

    public static final Pattern SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK = Pattern.compile("(^([A-Za-z]+)(&([0-9]+))*)@([A-Z]+)(\\[#\\])*");

    public static final Pattern SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK = Pattern.compile("^([A-Za-z]+)(&([0-9]+)\\?(o|v)|\\?(o|v)&([0-9]+))@([A-Z]+)(\\[#\\])*");

    public static final Pattern SYMBOL_DESCRIPTOR_FUTURES_VOL_RNK = Pattern.compile("(^([A-Za-z]+)(\\?(o|v))*)@([A-Z]+)(\\[#\\])*");

    // public static final Pattern SYMBOL_DESCRIPTOR_FUTURES_ACTIVITY =
    // Pattern.compile("(^([A-Za-z]+)(\\?o|\\?v)*)@([A-Z]+)(\\[#\\])*");

    // public static final Pattern SYMBOL_DESCRIPTOR_FUTURES_RANK =
    // Pattern.compile("(^([A-Za-z]+)&([0-9]*)(\\?o|\\?v)*|^([A-Za-z]+)(\\?o|\\?v)*&([0-9]*))@([A-Z]+)(\\[#\\])*");

}
