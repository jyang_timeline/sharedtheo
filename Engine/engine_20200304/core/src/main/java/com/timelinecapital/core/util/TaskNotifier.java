package com.timelinecapital.core.util;

public class TaskNotifier {

    Object lockObject = new Object();

    public void doWait() {
        synchronized (lockObject) {
            try {
                lockObject.wait();
            } catch (final InterruptedException e) {
            }
        }
    }

    public void doNotify() {
        synchronized (lockObject) {
            lockObject.notify();
        }
    }

}
