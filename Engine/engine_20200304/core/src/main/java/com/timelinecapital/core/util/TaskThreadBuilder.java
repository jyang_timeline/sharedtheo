package com.timelinecapital.core.util;

import java.util.concurrent.ThreadFactory;

public class TaskThreadBuilder {
    private String namePrefix = null;
    private int id;
    private boolean daemon = false;
    private int priority = Thread.NORM_PRIORITY;

    public TaskThreadBuilder setNamePrefix(final String namePrefix, final int id) {
        if (namePrefix == null) {
            throw new NullPointerException();
        }
        this.namePrefix = namePrefix;
        this.id = id;
        return this;
    }

    public TaskThreadBuilder setDaemon(final boolean daemon) {
        this.daemon = daemon;
        return this;
    }

    public TaskThreadBuilder setPriority(final int priority) {
        if (priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException(String.format("Thread priority (%s) must be <= %s", priority, Thread.MAX_PRIORITY));
        }
        this.priority = priority;
        return this;
    }

    public ThreadFactory build() {
        return build(this);
    }

    private static ThreadFactory build(final TaskThreadBuilder builder) {
        final String namePrefix = builder.namePrefix;
        final Integer id = builder.id;
        final Boolean daemon = builder.daemon;
        final Integer priority = builder.priority;

        return new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable runnable) {
                final Thread thread = new Thread(runnable);
                if (namePrefix != null) {
                    thread.setName(namePrefix + "-" + id);
                }
                if (daemon != null) {
                    thread.setDaemon(daemon);
                }
                if (priority != null) {
                    thread.setPriority(priority);
                }
                return thread;
            }
        };
    }
}
