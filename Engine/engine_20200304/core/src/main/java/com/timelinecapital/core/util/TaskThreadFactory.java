package com.timelinecapital.core.util;

import java.util.concurrent.ThreadFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskThreadFactory implements ThreadFactory {
    private static final Logger log = LogManager.getLogger(TaskThreadFactory.class);

    public static TaskThreadFactory createThreadFactory(final String threadName, final int id) {
        return new TaskThreadFactory(threadName, id, false, Thread.NORM_PRIORITY);
    }

    private String threadNamePrefix = StringUtils.EMPTY;
    private final int id;
    private boolean daemon = false;
    private int priority = Thread.NORM_PRIORITY;

    /**
     * Constructs an initialized thread factory.
     *
     * @param threadName The thread factory name.
     * @param id The thread id
     * @param daemon Whether to create daemon threads.
     * @param priority The thread priority.
     */
    TaskThreadFactory(final String threadName, final int id, final boolean daemon, final int priority) {
        this.threadNamePrefix = threadName;
        this.id = id;
        this.daemon = daemon;
        this.priority = priority;
    }

    @Override
    public Thread newThread(final Runnable runnable) {
        final Thread thread = new Thread(runnable, threadNamePrefix + "-" + id);
        if (thread.isDaemon() != daemon) {
            thread.setDaemon(daemon);
        }
        if (thread.getPriority() != priority) {
            thread.setPriority(priority);
        }
        log.warn("[EventThread] ******** START new Thread : {} ********", threadNamePrefix + "-" + id);
        return thread;
    }

}
