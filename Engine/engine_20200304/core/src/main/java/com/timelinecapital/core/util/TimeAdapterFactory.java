package com.timelinecapital.core.util;

import java.util.HashMap;
import java.util.Map;

import com.nogle.core.config.EngineMode;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.util.Pair;
import com.timelinecapital.core.config.EngineConfig;

public class TimeAdapterFactory {

    private static final int TIME_MILLIS_LENGTH = 8;
    private static final int TIME_MICROS_LENGTH = 11;

    private static final Map<Pair<ExchangeTimeFormat, Long>, TimeWrapper> formatToWrapper = new HashMap<>();
    private static final Map<Pair<String, Long>, TimeWrapper> symbolToWrapper = new HashMap<>();

    /**
     * To identify exchange time format from historical data. This method should be referred only in simulation mode
     *
     * @param calendarDay
     * @param sample
     * @param symbol
     * @return TimeWrapper
     */
    public static TimeWrapper getTimeWrapper(final long calendarDay, final String sample, final String symbol) {
        final int digits = sample.length();

        ExchangeTimeFormat format;
        if (digits == TIME_MILLIS_LENGTH || digits == TIME_MILLIS_LENGTH + 1) {
            format = ExchangeTimeFormat.CN_ISO8061_MILLIS;
        } else if (digits == TIME_MICROS_LENGTH || digits == TIME_MICROS_LENGTH + 1) {
            format = ExchangeTimeFormat.CN_ISO8061_MICROS;
        } else {
            format = ExchangeTimeFormat.UNIX;
        }

        final Pair<ExchangeTimeFormat, Long> key = new Pair<>(format, calendarDay);
        return formatToWrapper.computeIfAbsent(key, k -> new TimeWrapper(format, k.getRight()));
    }

    public static TimeWrapper getTimeWrapper(final long calendarDay, final Contract contract) {
        ExchangeTimeFormat format;

        final EngineMode mode = EngineConfig.getEngineMode();
        if (SbeVersion.PROT_BASIC.equals(mode.getSchemaVersion())) {
            format = ExchangeTimeFormat.UNIX;
        } else {
            format = ExchangeTimeFormat.fromString(mode.getExchangeTimeFormat(contract.getExchange()));
        }

        final Pair<String, Long> key = new Pair<>(contract.getSymbol(), calendarDay);
        return symbolToWrapper.computeIfAbsent(key, k -> new TimeWrapper(format, key.getRight()));
    }

}
