package com.timelinecapital.core.util;

public enum TimeLength {

    ISO8601_CN_IN_MICROSECOND("%012d"),
    ISO8601_CN_IN_MILLISECOND("%09d"),
    ;

    private final String format;

    private TimeLength(final String format) {
        this.format = format;
    }

    // public TimeLength lookup(String timeString) {
    //
    // }

    public String getFormat() {
        return format;
    }

}
