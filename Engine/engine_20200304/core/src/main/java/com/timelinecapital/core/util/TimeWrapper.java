package com.timelinecapital.core.util;

import java.time.LocalTime;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.util.TradeClock;
import com.timelinecapital.commons.util.FastDivision;
import com.timelinecapital.commons.util.FastDivision.Magic;

public class TimeWrapper {

    private final long calendarDay;
    private final SourceTimeAdapter adapter;

    TimeWrapper(final ExchangeTimeFormat format, final long calendarDay) {
        this.calendarDay = calendarDay;

        switch (format) {
            case CN_ISO8061_MICROS:
                adapter = new LocalTimeOfMicrosecondAdapter();
                break;
            case CN_ISO8061_MILLIS:
                adapter = new LocalTimeOfMillisecondAdapter();
                break;
            case UNIX:
            default:
                adapter = new NoConversionAdapter();
                break;
        }
    }

    public long get(final long micros) {
        return adapter.getEpochMicrosecond(micros);
    }

    interface SourceTimeAdapter {

        long getEpochMicrosecond(long micros);

    }

    final class EpochTimeAdapter implements SourceTimeAdapter {

        @Override
        public long getEpochMicrosecond(final long micros) {
            return calendarDay + getHourMultiply(micros) + getMinuteMultiply(micros) + getSecondMultiply(micros) + getSubSecondMultiply(micros);
        }

        private long getHourMultiply(final long micros) {
            return FastDivision.divideUnsignedFast(micros, TimeMask.HOUR.getDenoMagic()) * TimeMask.HOUR.getMultiplier();
        }

        private long getMinuteMultiply(final long micros) {
            return FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.MINUTE.getDenoMagic(), TimeMask.MINUTE.getModMagic()) * TimeMask.MINUTE.getMultiplier();
        }

        private long getSecondMultiply(final long micros) {
            return FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.SECOND.getDenoMagic(), TimeMask.SECOND.getModMagic()) * TimeMask.SECOND.getMultiplier();
        }

        private long getSubSecondMultiply(final long micros) {
            return FastDivision.remainderUnsignedFast(micros, TimeMask.SUBSECOND.getModMagic());
        }

    }

    final class LocalTimeOfMillisecondAdapter implements SourceTimeAdapter {

        @Override
        public long getEpochMicrosecond(final long millis) {
            return NogleTimeUnit.MILLISECONDS.toMicros(calendarDay) +
                NogleTimeUnit.NANOSECONDS.toMicros(
                    LocalTime.parse(String.format(TimeLength.ISO8601_CN_IN_MILLISECOND.getFormat(), millis),
                        NogleTimeFormatter.SOURCE_TIME_FORMAT_MILLIS).toNanoOfDay());
        }

    }

    final class LocalTimeOfMicrosecondAdapter implements SourceTimeAdapter {

        @Override
        public long getEpochMicrosecond(final long micros) {
            return NogleTimeUnit.MILLISECONDS.toMicros(calendarDay) +
                NogleTimeUnit.NANOSECONDS.toMicros(
                    LocalTime.parse(String.format(TimeLength.ISO8601_CN_IN_MICROSECOND.getFormat(), micros),
                        NogleTimeFormatter.SOURCE_TIME_FORMAT).toNanoOfDay());
        }

    }

    final class NoConversionAdapter implements SourceTimeAdapter {

        @Override
        public long getEpochMicrosecond(final long micros) {
            return micros;
        }

    }

    enum TimeMask {

        HOUR(1_00_00_000000l, 10, 3600_000_000l),
        MINUTE(1_00_000000l, 100, 60_000_000l),
        SECOND(1_000000l, 100, 1_000_000l),
        MILLISECOND(1000l, 1000, 1000l),
        MICROSECOND(1l, 1000, 1l),
        SUBSECOND(1l, 1_000000l, 1l),
        ;

        private long denominator;
        private long mod;
        private long multiplier;
        private Magic denoMagic;
        private Magic modMagic;

        private TimeMask(final long denominator, final long mod, final long multiplier) {
            this.denominator = denominator;
            this.mod = mod;
            this.multiplier = multiplier;
            denoMagic = FastDivision.magicUnsigned(denominator);
            modMagic = FastDivision.magicUnsigned(mod);
        }

        public long getDenominator() {
            return denominator;
        }

        public long getMod() {
            return mod;
        }

        public long getMultiplier() {
            return multiplier;
        }

        public Magic getDenoMagic() {
            return denoMagic;
        }

        public Magic getModMagic() {
            return modMagic;
        }

    }

    public static void main(final String[] args) {
        final long micros = 91509080955l;
        final long calendarDay = TradeClock.getCalendarMillis() * 1000l;

        final long hoursShift = (long) Math.pow(10, "hhmmssffffff".length() - 2);
        final long minutsShift = (long) Math.pow(10, "hhmmssffffff".length() - 4);
        final long secondShift = (long) Math.pow(10, "hhmmssffffff".length() - 6);

        final long digitH = (micros / hoursShift);
        final long digitM = (micros / minutsShift);

        final FastDivision.Magic magic = FastDivision.magicSigned(100);
        final long digitMM = FastDivision.remainderSignedFast(digitM, magic);
        final FastDivision.Magic magic2 = FastDivision.magicSigned(10000);
        // System.out.println(FastDivision.modSignedFast(digitM * 100 + 9, magic2));

        // System.out.println(digitH);
        // System.out.println(digitM);
        // System.out.println(digitMM);

        // System.out.println(FastDivision.divideUnsignedFast(micros, TimeMask.HOUR.getDenoMagic()));
        // System.out.println(FastDivision.remainderUnsignedFast(FastDivision.divideUnsignedFast(micros,
        // TimeMask.MINUTE.getDenoMagic()), TimeMask.MINUTE.getModMagic()));

        System.out.println(FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.MINUTE.getDenoMagic(), TimeMask.MINUTE.getModMagic()));
        System.out.println(FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.SECOND.getDenoMagic(), TimeMask.SECOND.getModMagic()));
        System.out.println(FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.MILLISECOND.getDenoMagic(), TimeMask.MILLISECOND.getModMagic()));
        System.out.println(FastDivision.divideAndRemainderUnSignedFast(micros, TimeMask.MICROSECOND.getDenoMagic(), TimeMask.MICROSECOND.getModMagic()));
        // System.out.println(calendarDay + hour);
        // System.out.println(calendarDay + minute);
    }

}