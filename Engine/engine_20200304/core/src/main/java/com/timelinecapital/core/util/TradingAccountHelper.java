package com.timelinecapital.core.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.timelinecapital.core.trade.TradingAccount;

public class TradingAccountHelper {

    private static final Map<String, TradingAccount> accounts = new HashMap<>();

    private static final String ACCOUNT_UNKNOWN = "*";
    private static final TradingAccount DUMMY_ACCOUNT = new TradingAccount(ACCOUNT_UNKNOWN);

    public static TradingAccount getAccountInstance(final String account) {
        if (StringUtils.isEmpty(account)) {
            return DUMMY_ACCOUNT;
        }
        return accounts.computeIfAbsent(account, key -> new TradingAccount(key));
    }

}
