package com.timelinecapital.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nogle.commons.utils.NogleTimeFormatter;
import com.nogle.core.util.TradeClock;
import com.nogle.util.ArrayUtils;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.types.SessionIdentifier;
import com.timelinecapital.strategy.state.ExchangeSession;

public class TradingHoursHelper {
    private static final Logger log = LogManager.getLogger(TradingHoursHelper.class);

    private static final Interval[] EMPTYINTERVAL = new Interval[1];

    private static Map<String, Interval[]> TRADING_HOURS = new HashMap<>();
    private static Map<String, List<SessionIdentifier>> SESSION_LOOKUP = new HashMap<>();
    private static TradingHoursHelper INSTANCE;

    private TradingHoursHelper() {
        TRADING_HOURS.clear();
        EMPTYINTERVAL[0] = new Interval(TradeClock.getTradingDayOpenedTime(), TradeClock.getTradingDayClosingTime());
        build();
    }

    public static TradingHoursHelper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TradingHoursHelper();
        }
        return INSTANCE;
    }

    private void build() {
        try {
            final DateTime endTime = DateTime.parse(TradeClock.getTradingDay(), NogleTimeFormatter.SIMPLE_DATE_TIME_FORMAT)
                .plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay());

            for (final Map.Entry<String, String[]> entry : EngineConfig.getExchangeToTradingSessions().entrySet()) {
                final String[] tradingSessions = entry.getValue();

                Interval[] intervals = new Interval[0];
                for (int i = 0; i < tradingSessions.length; i++) {
                    final int separator = tradingSessions[i].indexOf('/');
                    final DateTime start = DateTime.parse(tradingSessions[i].substring(0, separator), NogleTimeFormatter.ISO_TIME_FORMAT)
                        .withYear(endTime.getYear())
                        .withMonthOfYear(endTime.getMonthOfYear())
                        .withDayOfMonth(endTime.getDayOfMonth());
                    DateTime end = DateTime.parse(tradingSessions[i].substring(separator + 1), NogleTimeFormatter.ISO_TIME_FORMAT)
                        .withYear(endTime.getYear())
                        .withMonthOfYear(endTime.getMonthOfYear())
                        .withDayOfMonth(endTime.getDayOfMonth());
                    if (end.isBefore(start)) {
                        end = end.plusDays(1);
                    }

                    intervals = ArrayUtils.addElement(intervals, new Interval(start, end));
                }
                TRADING_HOURS.put(entry.getKey(), intervals);
            }

            updateCoreSession(TradeClock.getCalendarMillis(), SESSION_LOOKUP);
            updateAuctionSession(TradeClock.getCalendarMillis(), SESSION_LOOKUP);
            updateAuctionsFreezeSession(TradeClock.getCalendarMillis(), SESSION_LOOKUP);

        } catch (final Exception e) {
            TRADING_HOURS = Collections.emptyMap();
            log.warn("An exception occurred when building sessions {}", e.getMessage());
        }
    }

    public static Map<String, List<SessionIdentifier>> getSessionLookup() {
        return SESSION_LOOKUP;
    }

    public static ExchangeSession getExchangeSession(final String exchange, final long timeInMillis) {
        if (SESSION_LOOKUP.containsKey(exchange)) {
            final List<SessionIdentifier> identifiers = SESSION_LOOKUP.get(exchange);
            final Optional<SessionIdentifier> match = identifiers.stream().filter(identifier -> timeInMillis >= identifier.getInterval().getStartMillis() &&
                timeInMillis < identifier.getInterval().getEndMillis()).findFirst();
            if (match.isPresent()) {
                return match.get().getSession();
            }
            return ExchangeSession.NO_ORDERS;
        }
        return ExchangeSession.CORE_TRADING;
    }

    public static void updateCoreSession(final long tradingDay, final Map<String, List<SessionIdentifier>> exchangeSessions) {
        final DateTime endTime = new DateTime(tradingDay).plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay());

        for (final Map.Entry<String, String[]> entry : EngineConfig.getExchangeToTradingSessions().entrySet()) {
            final String[] tradingSessions = entry.getValue();

            final List<SessionIdentifier> identifiers = new ArrayList<>();
            for (int i = 0; i < tradingSessions.length; i++) {
                final int separator = tradingSessions[i].indexOf('/');
                final DateTime start = DateTime.parse(tradingSessions[i].substring(0, separator), NogleTimeFormatter.ISO_TIME_FORMAT)
                    .withYear(endTime.getYear())
                    .withMonthOfYear(endTime.getMonthOfYear())
                    .withDayOfMonth(endTime.getDayOfMonth());
                DateTime end = DateTime.parse(tradingSessions[i].substring(separator + 1), NogleTimeFormatter.ISO_TIME_FORMAT)
                    .withYear(endTime.getYear())
                    .withMonthOfYear(endTime.getMonthOfYear())
                    .withDayOfMonth(endTime.getDayOfMonth());
                if (end.isBefore(start)) {
                    end = end.plusDays(1);
                }

                identifiers.add(new SessionIdentifier(ExchangeSession.CORE_TRADING, new Interval(start, end)));
            }

            Collections.sort(identifiers);
            exchangeSessions.put(entry.getKey(), identifiers);
        }
    }

    public static void updateAuctionSession(final long tradingDay, final Map<String, List<SessionIdentifier>> exchangeSession) {
        final DateTime endTime = new DateTime(tradingDay).plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay());

        for (final Map.Entry<String, String[]> entry : EngineConfig.getExchangeAuctionSessions().entrySet()) {
            final String[] sessions = entry.getValue();

            if (sessions.length == 0) {
                final List<SessionIdentifier> identifiers = exchangeSession.computeIfAbsent(entry.getKey(), key -> new ArrayList<>());
                // final SessionIdentifier fullSession = identifiers.stream().filter(i ->
                // i.getSession().equals(ExchangeSession.CORE_TRADING)).findFirst().get();
                identifiers.add(new SessionIdentifier(ExchangeSession.OPENING,
                    new Interval(TradeClock.getTradingDayOpenedTime(), TradeClock.getTradingDayOpenedTime())));
                identifiers.add(new SessionIdentifier(ExchangeSession.CLOSING,
                    new Interval(TradeClock.getTradingDayClosingTime(), TradeClock.getTradingDayClosingTime())));
                Collections.sort(identifiers);
                continue;
            }

            Interval[] intervals = new Interval[0];
            for (int i = 0; i < sessions.length; i++) {
                final int separator = sessions[i].indexOf('/');
                final DateTime start = DateTime.parse(sessions[i].substring(0, separator), NogleTimeFormatter.ISO_TIME_FORMAT)
                    .withYear(endTime.getYear())
                    .withMonthOfYear(endTime.getMonthOfYear())
                    .withDayOfMonth(endTime.getDayOfMonth());
                DateTime end = DateTime.parse(sessions[i].substring(separator + 1), NogleTimeFormatter.ISO_TIME_FORMAT)
                    .withYear(endTime.getYear())
                    .withMonthOfYear(endTime.getMonthOfYear())
                    .withDayOfMonth(endTime.getDayOfMonth());
                if (end.isBefore(start)) {
                    end = end.plusDays(1);
                }

                intervals = ArrayUtils.addElement(intervals, new Interval(start, end));
            }
            Arrays.sort(intervals, new Comparator<Interval>() {
                @Override
                public int compare(final Interval x, final Interval y) {
                    return x.getStart().compareTo(y.getStart());
                }
            });

            final List<SessionIdentifier> identifiers = exchangeSession.computeIfAbsent(entry.getKey(), key -> new ArrayList<>());
            identifiers.add(new SessionIdentifier(ExchangeSession.OPENING, intervals[0]));
            if (intervals.length > 1) {
                identifiers.add(new SessionIdentifier(ExchangeSession.CLOSING, intervals[1]));
            }
            Collections.sort(identifiers);
        }
    }

    public static void updateAuctionsFreezeSession(final long tradingDay, final Map<String, List<SessionIdentifier>> exchangeSession) {

        final DateTime endTime = new DateTime(tradingDay).plus(EngineConfig.getTradingDaySwitchPoint().getMillisOfDay());

        for (final Map.Entry<String, String[]> entry : EngineConfig.getExchangeAuctionFreezeTime().entrySet()) {
            final String exchange = entry.getKey();
            final String[] freezeTimes = entry.getValue();

            if (freezeTimes.length == 0) {
                final List<SessionIdentifier> identifiers = exchangeSession.computeIfAbsent(entry.getKey(), key -> new ArrayList<>());
                // final SessionIdentifier fullSession = identifiers.stream().filter(i ->
                // i.getSession().equals(ExchangeSession.CORE_TRADING)).findFirst().get();
                identifiers.add(new SessionIdentifier(ExchangeSession.OPENING_FREEZE,
                    new Interval(TradeClock.getTradingDayOpenedTime(), TradeClock.getTradingDayOpenedTime())));
                identifiers.add(new SessionIdentifier(ExchangeSession.CLOSING_FREEZE,
                    new Interval(TradeClock.getTradingDayClosingTime(), TradeClock.getTradingDayClosingTime())));
                Collections.sort(identifiers);
                continue;
            }

            final List<SessionIdentifier> auctionSession = exchangeSession.get(exchange).stream()
                .filter(s -> s.getSession().equals(ExchangeSession.OPENING) || s.getSession().equals(ExchangeSession.CLOSING))
                .collect(Collectors.toList());

            for (int i = 0; i < freezeTimes.length; i++) {
                final DateTime freezeTime = DateTime.parse(freezeTimes[i], NogleTimeFormatter.ISO_TIME_FORMAT)
                    .withYear(endTime.getYear())
                    .withMonthOfYear(endTime.getMonthOfYear())
                    .withDayOfMonth(endTime.getDayOfMonth());

                auctionSession.stream().filter(v -> v.getInterval().contains(freezeTime.getMillis())).findFirst().ifPresent(v -> {
                    final List<SessionIdentifier> identifiers = exchangeSession.computeIfAbsent(exchange, e -> new ArrayList<>());
                    final ExchangeSession s = v.getSession() == ExchangeSession.OPENING ? ExchangeSession.OPENING_FREEZE : ExchangeSession.CLOSING_FREEZE;
                    identifiers.add(new SessionIdentifier(s, new Interval(freezeTime.getMillis(), v.getInterval().getEndMillis())));
                    Collections.sort(identifiers);
                });
            }
        }
    }

    public static Interval[] getTradingHours(final String exchange) {
        return TRADING_HOURS.getOrDefault(exchange, EMPTYINTERVAL);
    }

    public static boolean isTradingHours(final int offset) {
        boolean isRegularMarketTime = false;
        final DateTime now = new DateTime();
        for (final Interval[] sessionArray : TRADING_HOURS.values()) {
            for (int i = 0; i < sessionArray.length; i++) {
                if (now.isAfter(sessionArray[i].getStart().minusMinutes(offset)) && now.isBefore(sessionArray[i].getEnd())) {
                    isRegularMarketTime = true;
                    break;
                }
            }
        }
        return isRegularMarketTime;
    }

    /*
     * Before market opened - Only limit orders can be enqueued
     */
    public static long getPreOpeningSession(final String exchange, final Map<String, Interval[]> exchangeToTradingHours) {
        return exchangeToTradingHours.getOrDefault(exchange, EMPTYINTERVAL)[0].getEndMillis();
    }

    public static Interval getOpeningSession(final String exchange, final Map<String, List<SessionIdentifier>> auctionSessions) {
        return auctionSessions.get(exchange).stream().filter(i -> ExchangeSession.OPENING.equals(i.getSession())).findAny().get().getInterval();
    }

    public static Interval getOpeningSessionFreezePeriod(final String exchange, final Map<String, List<SessionIdentifier>> auctionFreezeSessions) {
        return auctionFreezeSessions.get(exchange).stream().filter(i -> ExchangeSession.OPENING_FREEZE.equals(i.getSession())).findAny().get().getInterval();
    }

    public static Interval getClosingSession(final String exchange, final Map<String, List<SessionIdentifier>> auctionSessions) {
        return auctionSessions.get(exchange).stream().filter(i -> ExchangeSession.CLOSING.equals(i.getSession())).findAny().get().getInterval();
    }

    public static Interval getClosingSessionFreezePeriod(final String exchange, final Map<String, List<SessionIdentifier>> auctionFreezeSessions) {
        return auctionFreezeSessions.get(exchange).stream().filter(i -> ExchangeSession.CLOSING_FREEZE.equals(i.getSession())).findAny().get().getInterval();
    }

}
