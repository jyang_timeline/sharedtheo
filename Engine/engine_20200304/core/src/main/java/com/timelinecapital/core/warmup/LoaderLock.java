package com.timelinecapital.core.warmup;

public class LoaderLock {

    Object myMonitorObject = new Object();

    public void doWait() {
        synchronized (myMonitorObject) {
            try {
                myMonitorObject.wait();
            } catch (final InterruptedException e) {
            }
        }
    }

    public void doWait(final long waitSeconds) {
        synchronized (myMonitorObject) {
            try {
                myMonitorObject.wait(waitSeconds);
            } catch (final InterruptedException e) {
            }
        }
    }

    public void doNotify() {
        synchronized (myMonitorObject) {
            myMonitorObject.notify();
        }
    }

}
