package com.timelinecapital.core.warmup;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.UnsupportedBinaryVersion;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderType;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.BestPriceOrderDetailEncoder;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderActionsEncoder;
import com.timelinecapital.commons.marketdata.encoder.OrderQueueEncoder;
import com.timelinecapital.commons.marketdata.encoder.SnapshotEncoder;
import com.timelinecapital.commons.marketdata.encoder.TradeEncoder;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.factory.ContractFactory;

public class MulticastPacketGenerator {

    private final SnapshotEncoder snapshotEncoder;
    private final MarketBookEncoder bookEncoder;
    private final TradeEncoder tickEncoder;
    private BestPriceOrderDetailEncoder orderDetailEncoder;
    private OrderActionsEncoder actionEncoder;
    private OrderQueueEncoder queueEncoder;

    private final Random random = new Random();

    private static final AtomicLong seq = new AtomicLong(0);

    private final List<Contract> contracts = new ArrayList<>();
    private final InetAddress group;
    private final int port;
    private final SbeVersion version;

    DatagramChannel channel;
    InetSocketAddress inetSocketAddress;
    // private final MulticastSocket socket;

    public MulticastPacketGenerator(final String host, final int port, final SbeVersion version) throws IOException {
        this.group = InetAddress.getByName(host);
        this.port = port;
        this.version = version;
        channel = DatagramChannel.open(StandardProtocolFamily.INET);
        inetSocketAddress = new InetSocketAddress(host, port);

        final NetworkInterface networkInterface = NetworkInterface.getByName("lo");
        // final NetworkInterface networkInterface = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
        channel.setOption(StandardSocketOptions.IP_MULTICAST_IF, networkInterface);
        channel.setOption(StandardSocketOptions.SO_REUSEADDR, true);

        switch (version) {
            case PROT_BASIC:
            case PROT_WITH_EXGTIME:
                snapshotEncoder = EncoderInstanceFactory.getSnptEncoder(version);
                bookEncoder = EncoderInstanceFactory.getBookEncoder(version, 0);
                tickEncoder = EncoderInstanceFactory.getTickEncoder(version, 0);
                orderDetailEncoder = EncoderInstanceFactory.getBPODEncoder(version);
                actionEncoder = EncoderInstanceFactory.getActionEncoder(version);
                break;
            case PROT_DYNAMICLENGTH_BOOK:
                snapshotEncoder = EncoderInstanceFactory.getSnptEncoder(version);
                bookEncoder = EncoderInstanceFactory.getBookEncoder(version, 0);
                tickEncoder = EncoderInstanceFactory.getTickEncoder(version, 0);
                actionEncoder = EncoderInstanceFactory.getActionEncoder(version);
                queueEncoder = EncoderInstanceFactory.getQueueEncoder(version);
                break;
            default:
                throw new UnsupportedBinaryVersion("Unknown binary version: " + version);
        }
    }

    public void add(final Contract contract) {
        contracts.add(contract);
    }

    public void test(final int seq, final QuoteType quoteType, final DataType dataType) {
        int length;
        if (seq > 10) {
            length = 10;
        } else {
            length = seq;
        }

        contracts.forEach(contract -> {
            try {
                switch (dataType) {
                    case SNAPSHOT:
                        snptGenerator(contract, quoteType);
                        multicast(snapshotEncoder.getData());
                        break;
                    case MARKETBOOK:
                        mbGenerator(contract, quoteType, length);
                        multicast(bookEncoder.getBuffer());
                        break;
                    case TICK:
                        tkGenerator(contract);
                        multicast(tickEncoder.getBuffer());
                        break;
                    case BESTORDERDETAIL:
                        odGenerator(contract);
                        multicast(orderDetailEncoder.getData());
                        break;
                    case ORDERACTIONS:
                        oaGenerator(contract);
                        multicast(actionEncoder.getData());
                        break;
                    case ORDERQUEUE:
                        oqGenerator(contract);
                        multicast(queueEncoder.getData());
                        break;
                    default:
                        break;
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void snptGenerator(final Contract contract, final QuoteType quoteType) {
        final double base = 1 + random.nextDouble() * 100;

        snapshotEncoder.setType(DataType.SNAPSHOT.getCode());
        snapshotEncoder.setSnapshotUpdateFlag(random.nextInt(3) + 1);
        snapshotEncoder.setSymbol(contract.getSymbol());
        snapshotEncoder.setTradeType(TickType.UNKNOWN.getCode());
        snapshotEncoder.setPrice(base);
        snapshotEncoder.setQuantity(random.nextInt(100) + 1);
        snapshotEncoder.setOpenInterest(random.nextInt(1000) + 1);
        snapshotEncoder.setVolume(random.nextInt(1000) + 1);

        snapshotEncoder.setQuoteType(quoteType.getCode());
        snapshotEncoder.setBidDepth(1);
        snapshotEncoder.setAskDepth(1);
        snapshotEncoder.setBidPrice(0, base - 1);
        snapshotEncoder.setBidQty(0, random.nextInt(100) + 1);
        snapshotEncoder.setAskPrice(0, base + 1);
        snapshotEncoder.setAskQty(0, random.nextInt(100) + 1);
    }

    private void mbGenerator(final Contract contract, final QuoteType quoteType, final int idx) {
        bookEncoder.getBuffer().rewind();

        bookEncoder.setVersion((byte) version.getVersion());
        bookEncoder.setType(DataType.MARKETBOOK.getCode());
        bookEncoder.setSymbol(contract.getSymbol());
        bookEncoder.setSequenceNo(seq.getAndIncrement());
        bookEncoder.setMicroTime(NogleTimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()));

        bookEncoder.setUpdateFlag(3);
        bookEncoder.setQuoteType(quoteType.getCode());

        final int bidDepth = idx;
        int askDepth;
        if (idx == 0 || idx > 9) {
            askDepth = idx;
        } else {
            askDepth = idx - 1;
        }

        final double base = 1 + random.nextDouble() * 100;
        bookEncoder.setBidDepth(bidDepth);
        for (int i = 0; i < bidDepth; i++) {
            bookEncoder.setBidPrice(i, base - ((i + 1) * 1));
            bookEncoder.setBidQty(i, random.nextInt(100) + 1);
        }
        bookEncoder.setAskDepth(askDepth);
        for (int i = 0; i < askDepth; i++) {
            bookEncoder.setAskPrice(i, base + ((i + 1) * 1));
            bookEncoder.setAskQty(i, random.nextInt(100) + 1);
        }
    }

    private void tkGenerator(final Contract contract) {
        tickEncoder.getBuffer().rewind();

        tickEncoder.setVersion((byte) version.getVersion());
        tickEncoder.setType(DataType.TICK.getCode());
        tickEncoder.setSymbol(contract.getSymbol());
        tickEncoder.setSequenceNo(seq.getAndIncrement());
        tickEncoder.setMicroTime(NogleTimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()));

        final double base = 1 + random.nextDouble() * 100;
        tickEncoder.setPrice(base);
        tickEncoder.setQuantity(random.nextInt(10) + 1);
        tickEncoder.setTradeType(random.nextBoolean() ? Side.SELL.getCode() : Side.BUY.getCode());
        tickEncoder.setOpenInterest(random.nextInt(500));
        tickEncoder.setVolume(random.nextInt(100));
    }

    private void odGenerator(final Contract contract) {
        orderDetailEncoder.setVersion((byte) version.getVersion());
        orderDetailEncoder.setType(DataType.BESTORDERDETAIL.getCode());
        orderDetailEncoder.setSymbol(contract.getSymbol());
        orderDetailEncoder.setSequenceNo(seq.getAndIncrement());
        orderDetailEncoder.setMicroTime(NogleTimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()));

        final double base = 1 + random.nextDouble() * 100;
        orderDetailEncoder.setBestBidPrice(base);
        orderDetailEncoder.setBestAskPrice(base + 2.0);
        for (int index = 0; index < 10; index++) {
            orderDetailEncoder.setBestBidOrderQty(index, index + 5);
            orderDetailEncoder.setBestAskOrderQty(index, (index + 1) * 2);
        }
    }

    private void oaGenerator(final Contract contract) {
        actionEncoder.setVersion((byte) version.getVersion());
        actionEncoder.setType(DataType.ORDERACTIONS.getCode());
        actionEncoder.setSymbol(contract.getSymbol());
        actionEncoder.setSequenceNo(seq.getAndIncrement());
        actionEncoder.setMicroTime(NogleTimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()));

        final double base = 1 + random.nextDouble() * 100;
        actionEncoder.setActionType(ExecRequestType.OrderEntry.getCode());
        actionEncoder.setOrderType(random.nextBoolean() ? OrderType.LIMIT.getCode() : OrderType.MARKET.getCode());
        actionEncoder.setOrderId(random.nextInt(100) + 1);
        actionEncoder.setOrderSide(random.nextBoolean() ? Side.BUY.getCode() : Side.SELL.getCode());
        actionEncoder.setOrderTimeCondition(TimeCondition.GFD.getCode());
        actionEncoder.setOrderPrice(base);
        actionEncoder.setOrderQty(random.nextInt(100) + 1);
    }

    private void oqGenerator(final Contract contract) {
        queueEncoder.setVersion((byte) version.getVersion());
        queueEncoder.setType(DataType.ORDERQUEUE.getCode());
        queueEncoder.setSymbol(contract.getSymbol());
        queueEncoder.setSequenceNo(seq.getAndIncrement());
        queueEncoder.setMicroTime(NogleTimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()));

        final double base = 1 + random.nextDouble() * 100;
        final byte side = random.nextBoolean() ? Side.BUY.getCode() : Side.SELL.getCode();
        queueEncoder.setSide(side);
        queueEncoder.setPrice(side, base);
        queueEncoder.setCount((short) 10);
        for (int i = 0; i < 10; i++) {
            queueEncoder.setOrder(side, i, (i + 1) * 3);
        }
    }

    void multicast(final byte[] buff) throws IOException {
        final DatagramPacket packet = new DatagramPacket(buff, buff.length, group, port);
        channel.socket().send(packet);
    }

    void multicast(final ByteBuffer data) throws IOException {
        channel.send(data, inetSocketAddress);
    }

    public static void main(final String[] args) throws ConfigurationException, IOException, InterruptedException {
        EngineConfig.buildConfig(1, new File("trading.properties"), EngineMode.PRODUCTION);

        if (args.length < 7) {
            System.err.println(
                "Arguments should be: Contract Exchange QuoteType - ex: multicast(Group) multicast(Port) Version(0,1,2,3) DataType(MB,TK) IF1901 CFFEX N NumberOfEvents Interval?");
            System.exit(1);
        }

        final String multicastHost = args[0];
        final int port = Integer.parseInt(args[1]);
        final SbeVersion version = SbeVersion.fromInt(Integer.parseInt(args[2]));
        final MulticastPacketGenerator generator = new MulticastPacketGenerator(multicastHost, port, version);

        final DataType dataType = DataType.decode((byte) args[3].charAt(0));

        final String symbol = args[4];
        final String exchange = args[5];
        final QuoteType quoteType = QuoteType.decode((byte) args[6].charAt(0));

        final Contract target = ContractFactory.getDummyContract(symbol, exchange);
        generator.add(target);

        for (int i = 0; i < Integer.parseInt(args[7]); i++) {
            generator.test(i, quoteType, dataType);
            if (Boolean.parseBoolean(args[8])) {
                Thread.sleep(1000);
            }
        }
    }

}
