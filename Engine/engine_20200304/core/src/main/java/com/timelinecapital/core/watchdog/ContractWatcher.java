package com.timelinecapital.core.watchdog;

import com.nogle.strategy.types.Contract;

public interface ContractWatcher {

    Contract getContract();

    boolean hasWarning();

}
