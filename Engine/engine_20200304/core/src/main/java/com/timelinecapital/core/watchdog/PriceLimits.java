package com.timelinecapital.core.watchdog;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;

public class PriceLimits implements ContractWatcher {

    private static QuoteType normal = QuoteType.QUOTE;
    private QuoteType current = QuoteType.QUOTE;

    private final Contract contract;

    public PriceLimits(final Contract contract) {
        this.contract = contract;
    }

    @Override
    public final Contract getContract() {
        return contract;
    }

    @Override
    public final boolean hasWarning() {
        return (normal.getShift() & current.getShift()) == 0;
    }

    public void onEvent(final QuoteType quoteType) {
        current = quoteType;
        if (current == null) {
            current = QuoteType.INVALID;
        }
    }

}
