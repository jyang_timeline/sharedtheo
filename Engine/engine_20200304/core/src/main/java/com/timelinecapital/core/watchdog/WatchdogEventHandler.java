package com.timelinecapital.core.watchdog;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.nogle.core.strategy.StrategyView;
import com.nogle.core.strategy.event.EventHandler;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.AlarmCodes;
import com.timelinecapital.commons.AlarmInst;
import com.timelinecapital.core.stats.ContractSummary;

public class WatchdogEventHandler implements EventHandler {

    private final Set<Contract> contractWithWarnings = new HashSet<>();
    private final Set<StrategyView> viewWithWarnings = new HashSet<>();

    private final Map<Contract, ContractSummary> contractSummaries;
    private final List<StrategyView> views;

    private final String handlerName;

    public WatchdogEventHandler(final Map<Contract, ContractSummary> contractSummaries, final List<StrategyView> views) {
        this.contractSummaries = contractSummaries;
        this.views = views;
        handlerName = "EngineWatchdog";
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public void handle() {
        contractWithWarnings.removeIf(warnInst -> contractSummaries.get(warnInst).getWatchers().stream().allMatch(watcher -> !watcher.hasWarning()));

        viewWithWarnings.stream()
            .filter(v -> !v.getContracts().stream().anyMatch(c -> contractWithWarnings.contains(c) && v.hasPositions(c)))
            .forEach(v -> v.disarm());
        viewWithWarnings.removeIf(v -> !v.isAlarm());

        contractWithWarnings.addAll(contractSummaries.entrySet().stream()
            .filter(inst -> inst.getValue().getWatchers().stream().anyMatch(watcher -> watcher.hasWarning()))
            .map(inst -> inst.getKey())
            .collect(Collectors.toSet()));

        if (!contractWithWarnings.isEmpty()) {
            views.stream()
                .filter(v -> v.getContracts().stream().anyMatch(c -> contractWithWarnings.contains(c) && v.hasPositions(c)))
                .forEach(v -> {
                    v.armWith(AlarmCodes.PRICE_LIMITS_WITH_POSITIONS, AlarmInst.Symbol, intersct(contractWithWarnings, v.getContracts()));
                    viewWithWarnings.add(v);
                });
        }
    }

    private static String intersct(final Set<Contract> ofWarnigns, final Set<Contract> ofStrategy) {
        return ofStrategy.stream().filter(ofWarnigns::contains).findFirst().orElse(ofStrategy.iterator().next()).getSymbol();
    }

}
