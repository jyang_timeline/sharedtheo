package com.timelinecapital.strategy;

public interface PnLStatement {

    double getRealizedPnl();

    double getTotalPnl();

    double getPnl();

    double getOpenPnl();

    double getClosedPnl();

    double getCommission();

}
