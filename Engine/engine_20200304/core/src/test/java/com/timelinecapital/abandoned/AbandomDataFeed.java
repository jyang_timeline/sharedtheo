package com.timelinecapital.abandoned;

public enum AbandomDataFeed {

    BESTORDERDETAIL(0),

    QUOTEORDERDETAIL(1),

    QUOTE_ALL(2),

    QUOTE_LEVEL1(3),

    QUOTE_LEVEL2(4),

    ;

    private int typeIndex;

    private AbandomDataFeed(final int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public int getTypeIndex() {
        return typeIndex;
    }

}
