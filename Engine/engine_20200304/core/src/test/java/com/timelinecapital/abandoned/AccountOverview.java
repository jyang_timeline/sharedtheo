package com.timelinecapital.abandoned;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.stats.FeedEvent;
import com.timelinecapital.core.stats.Overview;

@Deprecated
public class AccountOverview implements Overview {

    private final Map<Contract, FeedEvent> feedOverviews;
    private final Map<Contract, ExecReportSink> execOverviews = new HashMap<>();
    private final List<Double> expousres = new ArrayList<>();

    private final AtomicInteger index = new AtomicInteger(0);
    private final OverviewUpdater updater = new OverviewUpdater();
    private final String account;

    private double exposuresLong;
    private double exposuresShort;

    private double volume;
    private double value;
    private long orders;
    private long fills;
    private long cancels;
    private long rejects;

    public AccountOverview(final String account, final Map<Contract, FeedEvent> feedOverviews) {
        this.account = account;
        this.feedOverviews = feedOverviews;
    }

    public String getAccount() {
        return account;
    }

    // @Override
    // public ExecReportSink getExecutionReportEvent(final Contract contract) {
    // ExecReportSink execOverview = execOverviews.get(contract);
    // if (execOverview == null) {
    // execOverview = new ContractExecutionReportSummary(contract, updater, index.getAndAdd(1));
    // execOverviews.put(contract, execOverview);
    // expousres.add(0.0d);
    // }
    // return execOverview;
    // }

    @Override
    public void reset() {
        feedOverviews.forEach((k, v) -> v.reset());
        execOverviews.forEach((k, v) -> v.reset());
    }

    @Override
    public double getExposure(final Side side) {
        switch (side) {
            case BUY:
                return exposuresLong;
            case SELL:
                return exposuresShort;
            default:
                return exposuresLong + Math.abs(exposuresShort);
        }
    }

    @Override
    public double tradeVolume() {
        return volume;
    }

    @Override
    public double tradeValue() {
        return value;
    }

    @Override
    public long orders() {
        return orders;
    }

    @Override
    public long fills() {
        return fills;
    }

    @Override
    public long cancels() {
        return cancels;
    }

    @Override
    public long rejects() {
        return rejects;
    }

    class OverviewUpdater implements ExecReportSink {

        @Override
        public void onFill(final Side side, final double price, final long qty, final double commission) {
            fills++;
            volume += qty;
            value += price * qty;
        }

        @Override
        public void onOrderAck(final long orderQty, final Side side) {
            orders++;
        }

        @Override
        public void onCancelAck(final long cancelQty, final Side side) {
            cancels++;
        }

        @Override
        public void onOrderReject() {
            rejects++;
        }

        @Override
        public void onCancelReject() {
        }

        @Override
        public void reset() {
            orders = cancels = rejects = 0l;
        }

        @Override
        public void updateExposure(final int index, final double exposure) {
            expousres.set(index, exposure);
            exposuresLong = expousres.stream().filter(value -> value > 0.0d).mapToDouble(value -> value).sum();
            exposuresShort = expousres.stream().filter(value -> value < 0.0d).mapToDouble(value -> value).sum();
        }

    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getIndex() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ExecReportSink getStatisticsUpdater() {
        // TODO Auto-generated method stub
        return null;
    }

}
