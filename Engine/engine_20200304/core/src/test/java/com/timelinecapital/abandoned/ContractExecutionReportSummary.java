package com.timelinecapital.abandoned;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.stats.ExecReportSink;

@Deprecated
public class ContractExecutionReportSummary implements ExecReportSink {

    private final Contract contract;
    private final ExecReportSink accountSummary;
    private final int index;

    private double exposure;

    private double totalChargedFee;
    private long volume;
    private long position;

    private int fills;
    private int orderAcks;
    private int cancelAcks;
    private int orderRejects;
    private int cancelRejects;

    public ContractExecutionReportSummary(final Contract contract, final ExecReportSink accountSummary, final int index) {
        this.contract = contract;
        this.accountSummary = accountSummary;
        this.index = index;
    }

    public Contract getContract() {
        return contract;
    }

    public double getExposure() {
        return exposure;
    }

    public double getTotalChargedFee() {
        return totalChargedFee;
    }

    public long getVolume() {
        return volume;
    }

    public long getPosition() {
        return position;
    }

    public int getFills() {
        return fills;
    }

    public int getOrderAcks() {
        return orderAcks;
    }

    public int getCancelAcks() {
        return cancelAcks;
    }

    public int getOrderRejects() {
        return orderRejects;
    }

    public int getCancelRejects() {
        return cancelRejects;
    }

    @Override
    public final void ofExposure(final double exposure) {
        this.exposure += exposure;
        accountSummary.updateExposure(index, exposure);
    }

    @Override
    public final void onFill(final Side side, final double price, final long qty, final double commission) {
        fills++;
        volume += qty;
        position += side.getQuantityDelta(qty);
        totalChargedFee += commission;
        accountSummary.onFill(side, price, qty, commission);
    }

    @Override
    public final void onOrderAck(final long orderQty, final Side side) {
        orderAcks++;
        accountSummary.onOrderAck(orderQty, side);
    }

    @Override
    public final void onCancelAck(final long cancelQty, final Side side) {
        cancelAcks++;
        accountSummary.onCancelAck(cancelQty, side);
    }

    @Override
    public final void onOrderReject() {
        orderRejects++;
        accountSummary.onOrderReject();
    }

    @Override
    public final void onCancelReject() {
        cancelRejects++;
        accountSummary.onCancelReject();
    }

    @Override
    public final synchronized void reset() {
        totalChargedFee = 0.0d;
        volume = position = 0l;
        fills = orderAcks = cancelAcks = orderRejects = cancelRejects = 0;
        accountSummary.reset();
    }

}
