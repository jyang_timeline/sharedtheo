package com.timelinecapital.abandoned;

import java.util.ArrayList;
import java.util.List;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.timelinecapital.commons.type.DataType;
import com.timelinecapital.core.stats.FeedEvent;
import com.timelinecapital.core.watchdog.ContractWatcher;
import com.timelinecapital.core.watchdog.PriceLimits;

@Deprecated
public class ContractFeedSummary implements FeedEvent {

    private final Contract contract;
    private final PriceLimits priceWatcher;

    private final List<ContractWatcher> watchers = new ArrayList<>();
    private final long[] feedEvents = new long[DataType.values().length];

    public ContractFeedSummary(final Contract contract) {
        this.contract = contract;
        priceWatcher = new PriceLimits(contract);
        watchers.add(priceWatcher);

        for (int i = 0; i < feedEvents.length; i++) {
            feedEvents[i] = 0;
        }
    }

    public Contract getContract() {
        return contract;
    }

    public List<ContractWatcher> getWatchers() {
        return watchers;
    }

    @Override
    public void onMarketData(final DataType dataType) {
        feedEvents[dataType.getSeqValue()]++;
    }

    @Override
    public void onQuote(final QuoteType quoteType) {
        priceWatcher.onEvent(quoteType);
    }

    @Override
    public void reset() {
        for (int i = 0; i < feedEvents.length; i++) {
            feedEvents[i] = 0;
        }
    }

}
