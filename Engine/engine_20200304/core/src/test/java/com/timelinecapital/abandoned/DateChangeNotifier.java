package com.timelinecapital.abandoned;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DateChangeNotifier implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final DateChangedListener[] listeners = (DateChangedListener[]) context.getMergedJobDataMap().get("listeners");
        for (final DateChangedListener listener : listeners) {
            listener.onTradingDayChange(context.getFireTime());
        }
    }

}
