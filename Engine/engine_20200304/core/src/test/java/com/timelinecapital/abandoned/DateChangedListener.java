package com.timelinecapital.abandoned;

import java.util.Date;

import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.util.TradeClock;

public class DateChangedListener {

    final StrategyUpdater strategyUpdater;

    public DateChangedListener(final StrategyUpdater strategyUpdater) {
        this.strategyUpdater = strategyUpdater;
    }

    public void onTradingDayChange(final Date date) {
        TradeClock.setTradingDay(date.getTime());
        // TODO rebuild strategy here?
        // strategyUpdater.onScheduleUpdate();
        // TODO get position from previous day
    }
}
