package com.timelinecapital.abandoned;

import java.util.HashMap;
import java.util.Map;

import com.nogle.commons.utils.ContractReport;
import com.nogle.commons.utils.WorkingOrder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecutionReport;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.watchdog.ContractWatcher;

/**
 * This is a system wide statistics of event of a contract. If multiple strategies are trading the same contract, this class can
 * not identify which strategy has more events
 *
 */
public class MarketEvent {

    private final Map<Long, WorkingOrder> workingBuyOrders = new HashMap<>(256);
    private final Map<Long, WorkingOrder> workingSellOrders = new HashMap<>(256);
    private final Contract contract;
    private final ContractReport contractReport;

    private final ContractWatcher[] warnings;

    private double totalChargedFee;
    private long volume;
    private long position;

    private long marketdataCount;
    private int fills;
    private int orderAcks;
    private int cancelAcks;
    private int orderRejects;
    private int cancelRejects;

    public MarketEvent(final Contract contract, final ContractWatcher[] warnings) {
        this.contract = contract;
        this.warnings = warnings;
        contractReport = new ContractReport(contract.getSymbol());
    }

    public Contract getContract() {
        return contract;
    }

    public long getVolume() {
        return volume;
    }

    public long getPosition() {
        return position;
    }

    public double getTotalChargedFee() {
        return totalChargedFee;
    }

    public long getMarketdataCount() {
        return marketdataCount;
    }

    public int getFillCount() {
        return fills;
    }

    public int getOrderAckCount() {
        return orderAcks;
    }

    public int getCancelAckCount() {
        return cancelAcks;
    }

    public int getOrderRejectCount() {
        return orderRejects;
    }

    public int getPickCount() {
        return cancelRejects;
    }

    public Map<Long, WorkingOrder> getWorkingBuyOrders() {
        return workingBuyOrders;
    }

    public Map<Long, WorkingOrder> getWorkingSellOrders() {
        return workingSellOrders;
    }

    public ContractReport toReport() {
        contractReport.setQuotes(marketdataCount);
        contractReport.setCommission(totalChargedFee);
        contractReport.setWorkingBuyOrders(workingBuyOrders.values());
        contractReport.setWorkingSellOrders(workingSellOrders.values());
        contractReport.setVolume(volume);
        contractReport.setPosition(position);
        contractReport.setFills(fills);
        contractReport.setOrders(orderAcks);
        contractReport.setCancels(cancelAcks);
        contractReport.setRejects(orderRejects);
        contractReport.setPicks(cancelRejects);
        return contractReport;
    }

    // public void onMarketdata() {
    // marketdataCount++;
    // }

    public void onFill(final Side side, final long qty, final double commission) {
        fills++;
        volume += qty;
        position += side.getQuantityDelta(qty);
        totalChargedFee += commission;
    }

    public void onOrderAck() {
        orderAcks++;
    }

    public void onCancelAck() {
        cancelAcks++;
    }

    public void onOrderReject() {
        orderRejects++;
    }

    public void onCancelReject() {
        cancelRejects++;
    }

    public void insertWorkingOrders(final Ack report) {
        if (TimeCondition.IOC.equals(report.getTimeCondition())) {
            return;
        }
        switch (report.getSide()) {
            case BUY:
                workingBuyOrders.put(report.getClOrderId(), new WorkingOrder(report.getPrice(), report.getQty()));
                break;
            case SELL:
                workingSellOrders.put(report.getClOrderId(), new WorkingOrder(report.getPrice(), report.getQty()));
                break;
            default:
                throw new RuntimeException("ExecutionReport with an invalid Side");
        }
    }

    public void updateWorkingOrders(final Fill report) {
        switch (report.getSide()) {
            case BUY: {
                final WorkingOrder order = workingBuyOrders.get(report.getClOrderId());
                if (order != null) {
                    order.reduceQty(report.getFillQty());
                    if (order.getQty() <= 0) {
                        workingBuyOrders.remove(report.getClOrderId());
                    }
                }
                break;
            }
            case SELL: {
                final WorkingOrder order = workingSellOrders.get(report.getClOrderId());
                if (order != null) {
                    order.reduceQty(report.getFillQty());
                    if (order.getQty() <= 0) {
                        workingSellOrders.remove(report.getClOrderId());
                    }
                }
                break;
            }
            default:
                throw new RuntimeException("ExecutionReport with an invalid Side");
        }
    }

    public void removeWorkingOrders(final ExecutionReport report) {
        switch (report.getSide()) {
            case BUY:
                workingBuyOrders.remove(report.getClOrderId());
                break;
            case SELL:
                workingSellOrders.remove(report.getClOrderId());
                break;
            default:
                throw new RuntimeException("ExecutionReport with an invalid Side");
        }
    }

    public synchronized void reset() {
        workingBuyOrders.clear();
        workingSellOrders.clear();
        totalChargedFee = 0.0d;
        volume = position = marketdataCount = 0L;
        fills = orderAcks = cancelAcks = orderRejects = cancelRejects = 0;
    }

}
