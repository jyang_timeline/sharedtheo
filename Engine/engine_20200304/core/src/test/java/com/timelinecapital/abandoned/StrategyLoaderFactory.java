package com.timelinecapital.abandoned;

import java.util.Map;

import com.nogle.core.config.EngineMode;
import com.nogle.core.market.Market;
import com.nogle.core.strategy.StrategyLoader;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.LiveStrategyLoader;
import com.timelinecapital.core.config.EngineConfig;

public class StrategyLoaderFactory {
    private static Map<Integer, StrategyUpdater> idToUpdater;
    private static Market market;
    private static StrategyLoader strategyLoader;

    private volatile static ThreadLocal<StrategyLoader> THREAD_SIM_LOADER = new ThreadLocal<StrategyLoader>();

    public static StrategyLoader get() {
        if (strategyLoader == null) {
            if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
                return THREAD_SIM_LOADER.get();
            }
            // strategyLoader = new LiveStrategyLoader(market, idToUpdater);
            strategyLoader = new LiveStrategyLoader(market, idToUpdater);
        }
        return strategyLoader;
    }

    public static void add(final int strategyId, final StrategyLoader strategyLoader) {
        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            if (THREAD_SIM_LOADER.get() == null) {
                try {
                    THREAD_SIM_LOADER.set(strategyLoader);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void createLoaderFactory(final Market market, final Map<Integer, StrategyUpdater> idToUpdater) {
        StrategyLoaderFactory.market = market;
        StrategyLoaderFactory.idToUpdater = idToUpdater;
    }

}
