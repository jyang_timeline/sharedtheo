package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.strategy.api.CommandException;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.QuoteType;
import com.timelinecapital.commons.CommandExecutionResult;
import com.timelinecapital.commons.commands.engine.ConfigRiskManagement;
import com.timelinecapital.core.commands.system.RiskManagement;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CommandTest {

    @Mock
    private Contract contract;
    @Mock
    private QuoteSnapshot quoteSnapShot;
    @Mock
    private BookView bookView;
    @Mock
    private Config config;
    @Mock
    private StrategyUpdater strategyUpdater;
    @Mock
    private StrategyView view;

    private final int strategyId = 1;
    private final double tickSize = 1.0;
    private final double currentAsk = 100.0;
    private final double currentBid = 95.0;

    // private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final FixedTickCalculator fixedTickCalculator;

    private final TypeReference<HashMap<String, String>> typeRef = new TypeReference<>() {
    };
    private final ObjectMapper mapper = new ObjectMapper();
    private RiskManager riskManager;

    public CommandTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);

        RiskManagerConfig.forceBuildConfig();
        // final CoreControllerBuilder builder = new CoreControllerBuilder();
        // builder.withMappingIdToStatusView(idToStatusView).build();

        fixedTickCalculator = new FixedTickCalculator(tickSize, 10.0);
    }

    @Before
    public void setUp() {
        // idToStatusView.put(strategyId, view);

        Mockito.when(config.getInteger("Strategy.Condition.maxOrderQuantity")).thenReturn(10);
        Mockito.when(config.getInteger("Strategy.Condition.maxPositionPerSide")).thenReturn(20);
        Mockito.when(config.getInteger("Strategy.Condition.maxOrdersPerSide")).thenReturn(5);
        Mockito.when(config.getInteger("Strategy.Condition.minOrderQuantity")).thenReturn(null);
        Mockito.when(config.getInteger("Strategy.Condition.subsequentOrderDelay")).thenReturn(null);

        Mockito.when(config.getInteger("Strategy.Condition.maxRejectsPerInterval", RiskManagerConfig.getInstance().getMaxRejectsPerInterval()))
            .thenReturn(RiskManagerConfig.getInstance().getMaxRejectsPerInterval());
        Mockito.when(config.getInteger("Strategy.Condition.maxCancelsPerInterval", RiskManagerConfig.getInstance().getMaxCancelsPerInterval()))
            .thenReturn(RiskManagerConfig.getInstance().getMaxCancelsPerInterval());

        Mockito.when(quoteSnapShot.getContract()).thenReturn(contract);
        Mockito.when(quoteSnapShot.getContract().getTickCalculator()).thenReturn(fixedTickCalculator);
        Mockito.when(quoteSnapShot.getBookView()).thenReturn(bookView);
        Mockito.when(quoteSnapShot.getQuoteType()).thenReturn(QuoteType.QUOTE);

        Mockito.when(bookView.getAskPrice()).thenReturn(currentAsk);
        Mockito.when(bookView.getBidPrice()).thenReturn(currentBid);
    }

    @Test
    public void testUpdatePriceManager() throws ConditionNotFoundException, JsonParseException, JsonMappingException, IOException {
        riskManager = new RiskManager(RiskManagerConfig.getInstance());
        final PriceManager manager = PriceManagerBuilder.buildPriceManager(riskManager, quoteSnapShot, config);

        final Map<Contract, PriceManager> temp = new HashMap<>();
        temp.put(contract, manager);
        riskManager.addPriceManager(strategyId, temp);

        final RiskManagement command = new RiskManagement(riskManager);

        final ObjectNode node = mapper.createObjectNode();
        node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.UPDATE.name());
        node.put("maxCrossPercentage", "3.0");

        final Map<String, String> result = mapper.readValue(command.onCommand(node.toString()), typeRef);
        assertEquals(CommandExecutionResult.SUCCESS.toString(), result.get(TradeEngineCommunication.executionResultTag));
        assertEquals("3.0", result.get("maxCrossPercentage"));
    }

    @Test
    public void testUpdateProtectiveManager() throws ConditionNotFoundException, JsonParseException, JsonMappingException, CommandException, IOException {
        riskManager = new RiskManager(RiskManagerConfig.getInstance());
        final ProtectiveManager manager = ProtectiveManagerBuilder.buildRiskControlManager(riskManager, config, strategyUpdater);

        riskManager.addProtectiveManager(strategyId, manager);

        final RiskManagement command = new RiskManagement(riskManager);

        final ObjectNode node = mapper.createObjectNode();
        node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.UPDATE.name());
        node.put("maxRejectsPerInterval", "10");
        Map<String, String> result = mapper.readValue(command.onCommand(node.toString()), typeRef);
        assertEquals(CommandExecutionResult.SUCCESS.toString(), result.get(TradeEngineCommunication.executionResultTag));
        assertEquals("10", result.get("maxRejectsPerInterval"));

        node.removeAll();
        node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.UPDATE.name());
        node.put("maxCancelsPerInterval", "7");
        result = mapper.readValue(command.onCommand(node.toString()), typeRef);
        assertEquals(CommandExecutionResult.SUCCESS.toString(), result.get(TradeEngineCommunication.executionResultTag));
        assertEquals("7", result.get("maxCancelsPerInterval"));

        node.removeAll();
        node.put(ConfigRiskManagement.actionType, ConfigRiskManagement.Action.UPDATE.name());
        node.put("maxCancelsPerInterval", "7.7");
        result = mapper.readValue(command.onCommand(node.toString()), typeRef);
        assertEquals(CommandExecutionResult.FAILED.toString(), result.get(TradeEngineCommunication.executionResultTag));
    }

}
