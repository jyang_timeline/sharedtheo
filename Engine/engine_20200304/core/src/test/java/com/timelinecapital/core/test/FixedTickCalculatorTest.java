package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.types.FixedTickCalculator;

@RunWith(MockitoJUnitRunner.class)
public class FixedTickCalculatorTest {

    @Test
    public void fixedTickCalculatorTest1() {
        final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(0.1, 1000);

        assertEquals(fixedTickCalculator.toPrice(20), 2, 0);
        assertEquals(fixedTickCalculator.toValue(7, 31.2), 218400, 0);

        assertEquals(fixedTickCalculator.roundUpToNearestTick(31.2), 31.2, 0.01);
        assertEquals(fixedTickCalculator.roundUpToNearestTick(31.15), 31.2, 0.01);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31.2), 31.2, 0.01);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31.15), 31.1, 0.01);
    }

    @Test
    public void fixedTickCalculatorTest2() {
        final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(0.5, 1000);

        assertEquals(fixedTickCalculator.roundUpToNearestTick(999), 999, 0.01);
        assertEquals(fixedTickCalculator.roundUpToNearestTick(999.15), 999.5, 0.01);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(999), 999, 0.01);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(999.15), 999, 0.01);
    }

    @Test
    public void fixedTickCalculatorTest3() {
        final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(1, 1000);

        assertEquals(fixedTickCalculator.roundUpToNearestTick(31), 31, 0.1);
        assertEquals(fixedTickCalculator.roundUpToNearestTick(31.15), 32, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31), 31, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31.15), 31, 0.1);
    }

    @Test
    public void fixedTickCalculatorTest4() {
        final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(5, 1000);

        assertEquals(fixedTickCalculator.roundUpToNearestTick(35), 35, 0.1);
        assertEquals(fixedTickCalculator.roundUpToNearestTick(31.15), 35, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(35), 35, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31.15), 30, 0.1);
    }

    @Test
    public void fixedTickCalculatorTest5() {
        final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(1, 200);

        assertEquals(fixedTickCalculator.roundUpToNearestTick(31), 31, 0.1);
        assertEquals(fixedTickCalculator.roundUpToNearestTick(31.15), 32, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31), 31, 0.1);
        assertEquals(fixedTickCalculator.roundDownToNearestTick(31.15), 31, 0.1);
    }
}
