package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;

import com.nogle.core.config.EngineMode;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.position.condition.types.QuantityCondition;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.strategy.event.ProtectionHandler;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.strategy.StrategyConfig;

//@RunWith(MockitoJUnitRunner.class)
public class FuturesLossControlTest {

    @Mock
    private StrategyConfig config;
    @Mock
    private StrategyUpdater strategyUpdater;

    private final int strategyId = 1;
    private final String strategyName = "Strategy";

    private final double drawdownLimit = 5000;
    private final double lossLimit = 3000;
    private final long defaultQty = 1;
    private final double defaultCommission = 0;

    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Map<Contract, StrategyQuoteSnapshot> contractToQuoteSnapshot = new HashMap<>();
    private final Map<Contract, OrderViewContainer> contractToOrderview = new HashMap<>();

    private final PositionTracker positionTracker;
    private final Contract contract;
    private final AtomicLong clOrdId = new AtomicLong();

    private final RiskManager riskManager;

    private QuantityCondition maxLoss;
    private StrategyView view;
    private ProtectionHandler protectionHandler;

    public FuturesLossControlTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        riskManager = new RiskManager(RiskManagerConfig.getInstance());
        riskManager.getRiskManagerConfig().checkAndApply("minLotSize", 1);

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");

        positionTracker = new PositionTracker(contract);
    }

    @Before
    public void setUp() throws Exception {
        view = Mockito.spy(new StrategyView(
            strategyId,
            strategyName,
            contractToQuoteSnapshot,
            contractToOrderview));
        idToStatusView.put(strategyId, view);

        Mockito.when(config.getDouble("Strategy.Condition.maxDrawdown")).thenReturn(drawdownLimit);
        Mockito.when(config.getDouble("Strategy.Condition.maxLoss")).thenReturn(lossLimit);
        Mockito.when(config.getInteger("Strategy.Condition.maxRejectsPerInterval", riskManager.getRiskManagerConfig().getMaxRejectsPerInterval())).thenReturn(5);
        Mockito.when(config.getInteger("Strategy.Condition.maxCancelsPerInterval", riskManager.getRiskManagerConfig().getMaxCancelsPerInterval())).thenReturn(10);
        final ProtectiveManager riskControlManager = ProtectiveManagerBuilder.buildRiskControlManager(riskManager, config, strategyUpdater);
        protectionHandler = new ProtectionHandler(riskControlManager);

        Mockito.when(strategyUpdater.getStrategyStatusView()).thenReturn(view);
        Mockito.when(strategyUpdater.getStrategyName()).thenReturn(strategyName);

        maxLoss = riskControlManager.getQtyConditions()[1];
    }

    private Fill generateFills(final long orderId, final Side side, final double price) {
        final OrderFill fill = ExecReportFactory.getFill();
        fill.setFill(contract, side, TimeCondition.GFD, orderId, 1L, price, 0.0d);
        fill.setRemainingQty(0);
        return fill;
    }

    private void generatePNL(final double cumPnl, final double closePnl) {
        final double basePrice = 2500;
        double buyPrice = basePrice;
        double sellPrice = basePrice;
        if (closePnl >= 0) {
            buyPrice = basePrice;
            sellPrice = basePrice + Math.abs(closePnl);
        } else {
            sellPrice = basePrice;
            buyPrice = basePrice + Math.abs(closePnl);
        }
        Mockito.when(view.getTotalPnl()).thenReturn(cumPnl + closePnl);

        Fill fill = generateFills(clOrdId.addAndGet(1), Side.BUY, buyPrice);
        positionTracker.trackFill(Side.BUY, defaultQty, buyPrice, defaultCommission);
        protectionHandler.add(fill);
        protectionHandler.handle();

        fill = generateFills(clOrdId.addAndGet(1), Side.SELL, sellPrice);
        positionTracker.trackFill(Side.SELL, defaultQty, sellPrice, defaultCommission);
        protectionHandler.add(fill);
        protectionHandler.handle();
    }

    // @Test
    public void testLossControl() throws Exception {
        generatePNL(0, 500);
        generatePNL(500, -3800);
        final boolean isOnWatch = Whitebox.getInternalState(maxLoss, "isOnWatch");
        assertTrue(isOnWatch);

        long sentQty = 10;
        long revisedQty = maxLoss.checkCondition(Side.BUY, sentQty, 10);
        assertEquals(0, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.SELL, sentQty, 10);
        assertEquals(sentQty, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.BUY, sentQty, -10);
        assertEquals(sentQty, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.SELL, sentQty, -10);
        assertEquals(0, revisedQty);

        sentQty = 15;
        revisedQty = maxLoss.checkCondition(Side.BUY, sentQty, 10);
        assertEquals(0, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.SELL, sentQty, 10);
        assertEquals(sentQty - 5, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.BUY, sentQty, -10);
        assertEquals(sentQty - 5, revisedQty);

        revisedQty = maxLoss.checkCondition(Side.SELL, sentQty, -10);
        assertEquals(0, revisedQty);
    }

    // @Test
    public void dummy() {

    }

}
