package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.execution.IOCOrderBookBySide;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.types.PriceLevelPolicy;

@RunWith(MockitoJUnitRunner.class)
public class IOCOrderBookTest {

    @Mock
    private Config config;

    @Mock
    private TradeProxyListener tradeProxyListener;
    @Mock
    private StrategyView view;
    @Mock
    private ExecReportSink statsCollector;
    @Mock
    private OrderSender orderSender;
    @Mock
    private TradeAppendix tradeAppendix;

    private final int strategyId = 1;
    private final String strategyName = "JUnitTest";
    private final long updateId = 0L;
    private final double price = 100.0;

    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Contract contract;
    private final ClientOrderIdGenerator idGenerator;
    private final AtomicLong clOrdId;

    private final PositionTracker positionTracker;

    private OrderHolder orderHolder;
    private IOCOrderBookBySide orderBook;

    public IOCOrderBookTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        new RiskManager(RiskManagerConfig.getInstance());

        final CoreControllerBuilder builder = new CoreControllerBuilder();
        builder.withMappingIdToStatusView(idToStatusView).build();

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
        idGenerator = new ClientOrderIdGenerator(strategyId);
        clOrdId = new AtomicLong(idGenerator.getNextId());

        positionTracker = new PositionTracker(contract);
    }

    @Before
    public void setUp() throws ConditionNotFoundException {
        idToStatusView.put(strategyId, view);

        Mockito.when(config.getInteger("Strategy.Condition.maxOrderQuantity")).thenReturn(1);
        Mockito.when(config.getInteger("Strategy.Condition.maxPositionPerSide")).thenReturn(1);
        Mockito.when(config.getInteger("Strategy.Condition.minOrderQuantity")).thenReturn(null);
        Mockito.when(config.getInteger("Strategy.Condition.maxOrdersPerSide")).thenReturn(1);
        Mockito.when(config.getInteger("Strategy.Condition.subsequentOrderDelay")).thenReturn(null);
        final PositionManager positionManager = PositionManagerBuilder.buildPositionManager(contract, config);

        final ProtectiveManager riskControlManager = ProtectiveManagerBuilder.buildDummyRiskControlManager();
        final PriceManager priceManager = PriceManagerBuilder.buildDummyPriceManager();

        Mockito.when(tradeAppendix.getRiskControlManager()).thenReturn(riskControlManager);
        Mockito.when(tradeAppendix.getPositionManager()).thenReturn(positionManager);
        Mockito.when(tradeAppendix.getPriceManager()).thenReturn(priceManager);
        Mockito.when(tradeAppendix.getPositionTracker()).thenReturn(positionTracker);
        Mockito.when(tradeAppendix.getPriceLevelPolicy()).thenReturn(PriceLevelPolicy.SinglePriceLevel);

        orderHolder = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.BUY, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolder.setTradeProxyListener(tradeProxyListener);
        orderHolder.setSendOrderPermission(true);

        Whitebox.setInternalState(orderHolder, StrategyView.class, view);
        orderBook = Whitebox.getInternalState(orderHolder, "orderBook");
    }

    private long getInFlightOrderId() {
        return Whitebox.getInternalState(orderBook, "inFlightOrderId");
    }

    @Test
    public void testInvalidPriceOrQuantity() {
        orderBook.setQty(3L, 0d);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(0, getInFlightOrderId());

        orderBook.setQty(0L, price);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(0, getInFlightOrderId());

        orderBook.setQty(3L, Double.NaN);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(0, getInFlightOrderId());

        orderBook.setQty(3L, Double.MAX_VALUE);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertNotEquals(0, getInFlightOrderId());
    }

    /*
     * For IOC orders with position manager to check the permission of sending Orders
     */
    @Test
    public void testIOCOrders() {
        orderBook.setQty(1, price);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.addAndGet(100000000), getInFlightOrderId());
        orderBook.orderAcked(clOrdId.get());
        assertEquals(clOrdId.get(), getInFlightOrderId());

        orderBook.orderFilled(clOrdId.get(), 1, 0);
        assertEquals(0, getInFlightOrderId());
        positionTracker.trackFill(Side.BUY, 1, 100.0, 0);

        orderBook.setQty(1, price);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(0, getInFlightOrderId());
    }

    /*
     * For IOC orders to block orders between acked and filled
     */
    @Test
    public void testIOCOrderInFlight() {
        orderBook.setQty(1, 100);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.addAndGet(100000000), getInFlightOrderId());

        orderBook.orderAcked(clOrdId.get());
        assertEquals(clOrdId.get(), getInFlightOrderId());

        orderBook.setQty(1, price);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.get(), getInFlightOrderId());

        orderBook.orderFilled(clOrdId.get(), 1, 0);
        assertEquals(0, getInFlightOrderId());
        positionTracker.trackFill(Side.BUY, 1, 100.0, 0);

        orderBook.setQty(1, price);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(0, getInFlightOrderId());
    }

    @Test
    public void testIOCReset() {
        orderBook.setQty(1, 100);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.addAndGet(100000000), getInFlightOrderId());

        orderBook.setQty(1, 101);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.get(), getInFlightOrderId());

        orderBook.reset();
        assertEquals(0, getInFlightOrderId());

        orderBook.setQty(1, 101);
        orderBook.commit(updateId, System.currentTimeMillis());
        assertEquals(clOrdId.addAndGet(100000000), getInFlightOrderId());

        orderBook.orderAcked(clOrdId.get());
        assertEquals(clOrdId.get(), getInFlightOrderId());
    }

}
