package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeConnection;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.MultiLevelOrderBookBySide;
import com.timelinecapital.core.execution.OrderEntry;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.execution.sender.DelegatoryOrderSender;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.types.PriceLevelPolicy;

@RunWith(MockitoJUnitRunner.class)
public class MultiLevelOrderBookTest {

    @Mock
    private Config config;

    @Mock
    private TradeProxyListener tradeProxyListener;
    @Mock
    private StrategyView view;
    @Mock
    private ExecReportSink statsCollector;
    @Mock
    private TradeAppendix tradeAppendix;
    @Mock
    private PositionTracker positionTracker;

    private final int strategyId = 1;
    private final String strategyName = "JUnitTest";
    private final long updateId = 1;
    private final double price = 100.0;
    private final long delta = 100000000;

    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Contract contract;
    private final ClientOrderIdGenerator idGenerator;
    private final AtomicLong clOrdId;

    private final OrderSender orderSender;

    private OrderHolder orderHolderBuy;
    private OrderHolder orderHolderSell;
    private MultiLevelOrderBookBySide orderbookBuy;
    private MultiLevelOrderBookBySide orderbookSell;

    public MultiLevelOrderBookTest() throws ConfigurationException, ConditionNotFoundException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);

        final CoreControllerBuilder builder = new CoreControllerBuilder();
        builder.withMappingIdToStatusView(idToStatusView).build();

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
        idGenerator = new ClientOrderIdGenerator(strategyId);
        clOrdId = new AtomicLong(idGenerator.getNextId());

        orderSender = new DelegatoryOrderSender(contract, Mockito.mock(TradeConnection.class));
    }

    @Before
    public void setUp() throws ConditionNotFoundException {
        Mockito.when(positionTracker.getPosition()).thenReturn(0L);

        Mockito.when(config.getInteger("Strategy.Condition.maxOrderQuantity")).thenReturn(10);
        Mockito.when(config.getInteger("Strategy.Condition.maxPositionPerSide")).thenReturn(20);
        Mockito.when(config.getInteger("Strategy.Condition.minOrderQuantity")).thenReturn(null);
        Mockito.when(config.getInteger("Strategy.Condition.maxOrdersPerSide")).thenReturn(5);
        Mockito.when(config.getInteger("Strategy.Condition.subsequentOrderDelay")).thenReturn(null);
        final PositionManager positionManager = PositionManagerBuilder.buildPositionManager(contract, config);

        final PriceManager priceManager = PriceManagerBuilder.buildDummyPriceManager();

        Mockito.when(tradeAppendix.getPositionManager()).thenReturn(positionManager);
        Mockito.when(tradeAppendix.getPriceManager()).thenReturn(priceManager);
        Mockito.when(tradeAppendix.getPositionTracker()).thenReturn(positionTracker);
        Mockito.when(tradeAppendix.getPriceLevelPolicy()).thenReturn(PriceLevelPolicy.MultiplePriceLevel);

        orderHolderBuy = getOrderHolder(Side.BUY);
        orderHolderBuy.setTradeProxyListener(tradeProxyListener);
        orderHolderBuy.setSendOrderPermission(true);

        orderHolderSell = getOrderHolder(Side.SELL);
        orderHolderSell.setTradeProxyListener(tradeProxyListener);
        orderHolderSell.setSendOrderPermission(true);

        Whitebox.setInternalState(orderHolderBuy, StrategyView.class, view);
        Whitebox.setInternalState(orderHolderSell, StrategyView.class, view);

        orderbookBuy = Whitebox.getInternalState(orderHolderBuy, "orderBook");
        orderbookSell = Whitebox.getInternalState(orderHolderSell, "orderBook");
    }

    private OrderHolder getOrderHolder(final Side side) {
        return new OrderHolder(idGenerator, contract, TimeCondition.GFD, side, tradeAppendix, orderSender, strategyName, statsCollector);
    }

    private Map<Long, OrderEntry> getBidClOrdIdToOrderMap() {
        return Whitebox.getInternalState(orderbookBuy, "clOrdIdToEntry");
    }

    private Map<Long, OrderEntry> getAskClOrdIdToOrderMap() {
        return Whitebox.getInternalState(orderbookSell, "clOrdIdToEntry");
    }

    @Test
    public void test_1_InvalidPriceOrQuantity() {
        orderHolderBuy.setOrder(3L, 0d);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        orderHolderBuy.commit(updateId);
        assertEquals(0, orderbookBuy.getOrderEntries().size());

        orderHolderBuy.setOrder(0L, price);
        assertEquals(0, orderbookBuy.getOrderEntries().size());
        orderHolderBuy.commit(updateId + 1);
        assertEquals(0, orderbookBuy.getOrderEntries().size());

        orderHolderBuy.setOrder(3L, Double.NaN);
        assertEquals(0, orderbookBuy.getOrderEntries().size());
        orderHolderBuy.commit(updateId + 2);
        assertEquals(0, orderbookBuy.getOrderEntries().size());
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> CancelOrder(Price wrong) -> No Action
     *     -> CancelOrder(price) -> CancelAcked(A)
     */
    @Test
    public void test_2_SingleOrderAndSingleCancel() {
        orderHolderBuy.setOrder(6L, price);
        orderHolderBuy.commit(updateId);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId.addAndGet(delta)));
        final OrderEntry entry = orderbookBuy.getOrderEntries().get(0);
        assertTrue(entry.isSent());
        assertFalse(entry.isAcked());
        assertFalse(entry.isDirty());

        orderbookBuy.orderAcked(clOrdId.get());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(1, orderbookBuy.getOutstandingOrderCount());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId.get()));
        assertTrue(entry.isSent());
        assertTrue(entry.isAcked());
        assertFalse(entry.isDirty());

        orderbookBuy.clear(price + 5);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId.get()));
        assertTrue(entry.isSent());
        assertTrue(entry.isAcked());
        assertFalse(entry.isDirty());
        orderbookBuy.commit(updateId + 1, System.currentTimeMillis());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(1, orderbookBuy.getOutstandingOrderCount());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId.get()));
        assertTrue(entry.isSent());
        assertTrue(entry.isAcked());
        assertFalse(entry.isDirty());

        orderbookBuy.clear();
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertFalse(entry.isSent());
        assertTrue(entry.isAcked());
        assertTrue(entry.isDirty());
        orderbookBuy.commit(updateId + 2, System.currentTimeMillis());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertTrue(entry.isSent());
        assertFalse(entry.isAcked());
        assertFalse(entry.isDirty());

        orderbookBuy.cancelAcked(clOrdId.get());
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId.get()));
        assertEquals(0, orderbookBuy.getOrderEntries().size());
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> NewOrder(B) -> OrderAcked(B)
     *   -> CancelOrder(B) -> CancelAcked(B)
     *     -> CancelOrder(A) -> CancelAcked(A)
     */
    @Test
    public void test_3_MultipleOrderAndCancel_Buy() {
        orderHolderBuy.setOrder(6L, price);
        orderHolderBuy.commit(updateId);
        final long clOrdId_first = clOrdId.addAndGet(delta);
        orderbookBuy.orderAcked(clOrdId_first);
        OrderEntry entry1 = orderbookBuy.getOrderEntries().get(0);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertTrue(entry1.isSent());
        assertTrue(entry1.isAcked());
        assertFalse(entry1.isDirty());

        orderHolderBuy.setOrder(6L, price + 5);
        orderHolderBuy.commit(updateId + 1);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        orderbookBuy.orderAcked(clOrdId_second);
        final OrderEntry entry2 = orderbookBuy.getOrderEntries().get(0);
        entry1 = orderbookBuy.getOrderEntries().get(1);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(entry2.isSent());
        assertTrue(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.clear(price + 5);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertFalse(entry2.isSent());
        assertTrue(entry2.isAcked());
        assertTrue(entry2.isDirty());
        orderbookBuy.commit(updateId + 2, System.currentTimeMillis());
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.cancelAcked(clOrdId_second);
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertEquals(1, orderbookBuy.getOrderEntries().size());

        System.out.println(orderbookBuy.getBestPrice());

        orderbookBuy.clear();
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertFalse(entry1.isSent());
        assertTrue(entry1.isAcked());
        assertTrue(entry1.isDirty());
        orderbookBuy.commit(updateId + 3, System.currentTimeMillis());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertTrue(entry1.isSent());
        assertFalse(entry1.isAcked());
        assertFalse(entry1.isDirty());

        orderbookBuy.cancelAcked(clOrdId_first);
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertEquals(0, orderbookBuy.getOrderEntries().size());
    }

    @Test
    public void test_3_MultipleOrderAndCancel_Sell() {
        orderHolderSell.setOrder(6L, price);
        orderHolderSell.commit(updateId);
        final long clOrdId_first = clOrdId.addAndGet(delta);
        orderbookSell.orderAcked(clOrdId_first);
        OrderEntry entry_top = orderbookSell.getOrderEntries().get(0);
        assertEquals(1, orderbookSell.getOrderEntries().size());
        assertTrue(getAskClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertTrue(entry_top.isSent());
        assertTrue(entry_top.isAcked());
        assertFalse(entry_top.isDirty());

        orderHolderSell.setOrder(6L, price + 5);
        orderHolderSell.commit(updateId + 1);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        orderbookSell.orderAcked(clOrdId_second);
        entry_top = orderbookSell.getOrderEntries().get(0);
        final OrderEntry entry_second = orderbookSell.getOrderEntries().get(1);
        assertEquals(2, orderbookSell.getOrderEntries().size());
        assertTrue(getAskClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(entry_second.isSent());
        assertTrue(entry_second.isAcked());
        assertFalse(entry_second.isDirty());

        orderbookSell.clear(price + 5);
        assertEquals(2, orderbookSell.getOrderEntries().size());
        assertTrue(getAskClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertFalse(entry_second.isSent());
        assertTrue(entry_second.isAcked());
        assertTrue(entry_second.isDirty());
        orderbookSell.commit(updateId + 2, System.currentTimeMillis());
        assertEquals(2, orderbookSell.getOrderEntries().size());
        assertTrue(getAskClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(entry_second.isSent());
        assertFalse(entry_second.isAcked());
        assertFalse(entry_second.isDirty());

        orderbookSell.cancelAcked(clOrdId_second);
        assertFalse(getAskClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(getAskClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertEquals(1, orderbookSell.getOrderEntries().size());

        orderbookSell.clear();
        assertEquals(1, orderbookSell.getOrderEntries().size());
        assertFalse(entry_top.isSent());
        assertTrue(entry_top.isAcked());
        assertTrue(entry_top.isDirty());
        orderbookSell.commit(updateId + 3, System.currentTimeMillis());
        assertEquals(1, orderbookSell.getOrderEntries().size());
        assertTrue(entry_top.isSent());
        assertFalse(entry_top.isAcked());
        assertFalse(entry_top.isDirty());

        orderbookSell.cancelAcked(clOrdId_first);
        assertFalse(getAskClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertEquals(0, orderbookSell.getOrderEntries().size());
    }

    /*-
     * NewOrder(A) -> NewOrder(B)
     *   -> OrderAcked(A) -> OrderAcked(B)
     *     -> Fill(A) -> CancelOrder(B) -> Fill(B) -> CancelReject(B)
     */
    @Test
    public void test_4_MultipleOrderThenCommit() {
        orderHolderBuy.setOrder(6L, price + 5);
        orderHolderBuy.setOrder(6L, price);
        final OrderEntry entry1 = orderbookBuy.getOrderEntries().get(0);
        final OrderEntry entry2 = orderbookBuy.getOrderEntries().get(1);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertFalse(entry1.isSent());
        assertFalse(entry1.isAcked());
        assertTrue(entry1.isDirty());
        assertFalse(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertTrue(entry2.isDirty());

        orderHolderBuy.commit(updateId + 1);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        final long clOrdId_first = clOrdId.addAndGet(delta);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        assertTrue(entry1.isSent());
        assertFalse(entry1.isAcked());
        assertFalse(entry1.isDirty());
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.orderAcked(clOrdId_first);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(1, orderbookBuy.getOutstandingOrderCount());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));
        assertTrue(entry1.isSent());
        assertTrue(entry1.isAcked());
        assertFalse(entry1.isDirty());

        orderbookBuy.orderAcked(clOrdId_second);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
        assertTrue(entry2.isSent());
        assertTrue(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.orderFilled(clOrdId_first, 6, 0);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(1, orderbookBuy.getOutstandingOrderCount());
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));

        orderbookBuy.clear();
        assertFalse(entry2.isSent());
        assertTrue(entry2.isAcked());
        assertTrue(entry2.isDirty());
        orderbookBuy.commit(updateId + 2, System.currentTimeMillis());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.orderFilled(clOrdId_second, 3, 3);
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertTrue(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));

        orderbookBuy.clear();
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        // assertTrue(entry2.isDirty());
        assertFalse(entry2.isDirty());
        orderbookBuy.commit(updateId + 3, System.currentTimeMillis());
        assertEquals(1, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());

        orderbookBuy.orderFilled(clOrdId_second, 3, 0);
        assertEquals(0, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));

        orderbookBuy.cancelRejected(clOrdId_second, RejectReason.X);
        assertEquals(0, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_second));
    }

    /*-
     * NewOrder(A) -> NewOrder(B) -> NewOrder(C)
     *   -> CancelOrder(C) (Not Sending)
     *     -> OrderAcked(A), OrderAcked(B), OrderAcked(C)
     *       -> CancelOrder(C) -> CancelOrder(C) (Not Sending)
     *         -> NewOrder(D) (Same price/qty as C) -> CancelAcked(C) -> OrderAcked(D)
     */
    @Test
    public void test_5_MultipleOrderAndDelayedAcked() {
        orderHolderBuy.setOrder(6L, price + 5);
        orderHolderBuy.setOrder(6L, price - 5);
        OrderEntry entry1 = orderbookBuy.getOrderEntries().get(0);
        OrderEntry entry2 = orderbookBuy.getOrderEntries().get(1);

        orderHolderBuy.commit(updateId + 1);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(entry1.getPrice(), price + 5, 0);
        assertFalse(entry1.isWorkingOrder());
        assertEquals(entry2.getPrice(), price - 5, 0);
        assertFalse(entry2.isWorkingOrder());

        orderHolderBuy.setOrder(3L, price);
        entry1 = orderbookBuy.getOrderEntries().get(0);
        entry2 = orderbookBuy.getOrderEntries().get(1);
        OrderEntry entry3 = orderbookBuy.getOrderEntries().get(2);

        orderHolderBuy.commit(updateId + 2);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(entry1.getPrice(), price + 5, 0);
        assertEquals(entry2.getPrice(), price, 0);
        assertEquals(entry3.getPrice(), price - 5, 0);

        final long clOrdId_first = clOrdId.addAndGet(delta);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        final long clOrdId_third = clOrdId.addAndGet(delta);
        assertTrue(entry1.isSent());
        assertFalse(entry1.isAcked());
        assertFalse(entry1.isDirty());
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());
        assertTrue(entry3.isSent());
        assertFalse(entry3.isAcked());
        assertFalse(entry3.isDirty());

        orderHolderBuy.clearOrder(price);
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());
        orderHolderBuy.commit(updateId + 3);
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.orderAcked(clOrdId_first);
        orderbookBuy.orderAcked(clOrdId_second);
        orderbookBuy.orderAcked(clOrdId_third);
        orderHolderBuy.commit(updateId + 4);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(3, orderbookBuy.getOutstandingOrderCount());

        orderHolderBuy.clearOrder(price);
        orderHolderBuy.commit(updateId + 5);
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());

        orderHolderBuy.clearOrder(price);
        orderHolderBuy.commit(updateId + 5);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
        assertTrue(entry2.isSent());
        assertFalse(entry2.isAcked());
        assertFalse(entry2.isDirty());

        orderbookBuy.setQty(3L, price);
        entry3 = orderbookBuy.getOrderEntries().get(2);
        assertFalse(entry3.isSent());
        assertFalse(entry3.isAcked());
        assertTrue(entry3.isDirty());
        orderHolderBuy.commit(updateId + 6);
        assertTrue(entry3.isSent());
        assertFalse(entry3.isAcked());
        assertFalse(entry3.isDirty());

        orderbookBuy.cancelAcked(clOrdId_third);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
        final long clOrdId_fourth = clOrdId.addAndGet(delta);
        orderbookBuy.orderAcked(clOrdId_fourth);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(3, orderbookBuy.getOutstandingOrderCount());
    }

    @Test
    public void test_6_CancelRejectAndFill() {
        orderHolderBuy.setOrder(6L, price + 5);
        orderHolderBuy.setOrder(6L, price - 5);
        OrderEntry entry_top = orderbookBuy.getOrderEntries().get(0);
        OrderEntry entry_second = orderbookBuy.getOrderEntries().get(1);

        orderHolderBuy.commit(updateId + 1);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(entry_top.getPrice(), price + 5, 0);
        assertFalse(entry_top.isWorkingOrder());
        assertEquals(entry_second.getPrice(), price - 5, 0);
        assertFalse(entry_second.isWorkingOrder());

        orderHolderBuy.setOrder(3L, price);
        entry_top = orderbookBuy.getOrderEntries().get(0);
        entry_second = orderbookBuy.getOrderEntries().get(1);
        final OrderEntry entry_third = orderbookBuy.getOrderEntries().get(2);

        orderHolderBuy.commit(updateId + 2);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(entry_top.getPrice(), price + 5, 0);
        assertEquals(entry_second.getPrice(), price, 0);
        assertEquals(entry_third.getPrice(), price - 5, 0);

        final long clOrdId_first = clOrdId.addAndGet(delta);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        final long clOrdId_third = clOrdId.addAndGet(delta);
        assertTrue(entry_top.isSent());
        assertFalse(entry_top.isAcked());
        assertFalse(entry_top.isDirty());
        assertTrue(entry_second.isSent());
        assertFalse(entry_second.isAcked());
        assertFalse(entry_second.isDirty());
        assertTrue(entry_third.isSent());
        assertFalse(entry_third.isAcked());
        assertFalse(entry_third.isDirty());

        orderbookBuy.orderAcked(clOrdId_first);
        orderbookBuy.orderAcked(clOrdId_second);
        orderbookBuy.orderAcked(clOrdId_third);
        orderHolderBuy.commit(updateId + 4);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(3, orderbookBuy.getOutstandingOrderCount());

        orderHolderBuy.clearOrder(price + 5);
        orderHolderBuy.commit(updateId + 5);
        assertTrue(entry_top.isSent());
        assertFalse(entry_top.isAcked());
        assertFalse(entry_top.isDirty());
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
        assertEquals(orderbookBuy.getBestPrice(), price + 5, 0);

        orderbookBuy.cancelRejected(clOrdId_first, RejectReason.X);
        assertTrue(entry_top.isSent());
        assertTrue(entry_top.isAcked());
        assertFalse(entry_top.isDirty());
        orderbookBuy.orderFilled(clOrdId_first, 6L, 0L);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
        assertFalse(getBidClOrdIdToOrderMap().containsKey(clOrdId_first));
    }

    @Test
    public void test_7_PriceManagement() {
        orderHolderBuy.setOrder(6L, price + 5);
        assertEquals(price + 5, orderbookBuy.getOrderEntries().get(0).getPrice(), 0);
        orderHolderBuy.setOrder(6L, price + 20);
        assertEquals(price + 20, orderbookBuy.getOrderEntries().get(0).getPrice(), 0);
        // orderHolderBuy.setOrder(6L, price + 30);
        // assertEquals(askPrice + 20 - 1, orderbookBuy.getOrderEntries().get(0).getPrice(), 0);
        // orderHolderBuy.setOrder(6L, askPrice + 20);
        // assertEquals(askPrice + 20 - 1, orderbookBuy.getOrderEntries().get(0).getPrice(), 0);
        // assertEquals(4, orderbookBuy.getOrderEntries().size());

        orderHolderSell.setOrder(6L, price - 5);
        assertEquals(price - 5, orderbookSell.getOrderEntries().get(0).getPrice(), 0);
        orderHolderSell.setOrder(6L, price - 20);
        assertEquals(price - 20, orderbookSell.getOrderEntries().get(0).getPrice(), 0);
        // orderHolderSell.setOrder(6L, price - 30);
        // assertEquals(bidPrice - 20 + 1, orderbookSell.getOrderEntries().get(0).getPrice(), 0);
        // orderHolderSell.setOrder(6L, bidPrice - 20);
        // assertEquals(bidPrice - 20 + 1, orderbookSell.getOrderEntries().get(0).getPrice(), 0);
        // assertEquals(4, orderbookSell.getOrderEntries().size());
    }

    @Test
    public void test_8_PositionManagement() {
        orderHolderBuy.setOrder(10, price);
        assertEquals(10, orderbookBuy.getOrderEntries().get(0).getQty(), 0);
        orderHolderBuy.setOrder(15, price + 5);
        assertEquals(10, orderbookBuy.getOrderEntries().get(0).getQty(), 0);
        orderHolderBuy.setOrder(1, price + 10);
        orderHolderBuy.setOrder(1, price + 10);
        orderHolderBuy.setOrder(1, price + 10);
        orderHolderBuy.setOrder(1, price + 10);
        assertEquals(5, orderbookBuy.getOrderEntries().size());

        orderHolderBuy.commit(updateId);
        for (final OrderEntry orderEntry : getBidClOrdIdToOrderMap().values()) {
            assertTrue(orderEntry.isInflight());
            orderbookBuy.orderAcked(orderEntry.getClOrdId());
            assertTrue(orderEntry.isWorkingOrder());
        }

        orderHolderSell.setOrder(10, price);
        assertEquals(10, orderbookSell.getOrderEntries().get(0).getQty(), 0);
        orderHolderSell.setOrder(15, price - 5);
        assertEquals(10, orderbookSell.getOrderEntries().get(0).getQty(), 0);
        orderHolderSell.setOrder(1, price - 10);
        orderHolderSell.setOrder(1, price - 10);
        orderHolderSell.setOrder(1, price - 10);
        orderHolderSell.setOrder(1, price - 10);
        assertEquals(5, orderbookSell.getOrderEntries().size());

        orderHolderSell.commit(updateId);
        for (final OrderEntry orderEntry : getAskClOrdIdToOrderMap().values()) {
            assertTrue(orderEntry.isInflight());
            orderbookSell.orderAcked(orderEntry.getClOrdId());
            assertTrue(orderEntry.isWorkingOrder());
        }
    }

    /*-
     * NewOrder(A) -> NewOrder(B)
     *   -> OrderAcked(A) -> OrderAcked(B)
     *     -> ClearOrders(A,B) -> NewOrder(C) -> QueueOrder(C)
     */
    @Test
    public void test_9_PostponeOrders_CancelFirst() {
        orderHolderBuy.setOrder(6L, price + 5);
        orderHolderBuy.setOrder(6L, price);
        orderHolderBuy.commit(updateId + 1);
        final long clOrdId_first = clOrdId.addAndGet(delta);
        final long clOrdId_second = clOrdId.addAndGet(delta);
        orderbookBuy.orderAcked(clOrdId_first);
        orderbookBuy.orderAcked(clOrdId_second);

        orderHolderBuy.setWaitCancelAck(true);
        orderHolderBuy.clearAllOrders();
        orderHolderBuy.setOrder(3L, price - 5);
        orderHolderBuy.commit(updateId + 2);
        assertEquals(3, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(1, orderSender.getNoOrders(Side.BUY));

        orderbookBuy.cancelAcked(clOrdId_first);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(0, orderSender.getNoOrders(Side.BUY));
    }

    @Test
    public void test_10_PostponeOrders_NewFirst() {
        orderHolderSell.setOrder(6L, price + 5);
        orderHolderSell.setOrder(6L, price);
        orderHolderSell.commit(updateId + 1);
        final long clOrdId_first = clOrdId.addAndGet(delta);
        final long clOrdId_second = clOrdId.addAndGet(delta);

        orderbookSell.orderAcked(clOrdId_first);
        orderbookSell.orderAcked(clOrdId_second);

        orderHolderSell.setWaitCancelAck(true);
        orderHolderSell.clearAllOrders();
        orderHolderSell.setOrder(3L, price - 5);
        orderHolderSell.commit(updateId + 2);
        assertEquals(3, orderbookSell.getOrderEntries().size());
        assertEquals(0, orderbookSell.getOutstandingOrderCount());
        assertEquals(1, orderSender.getNoOrders(Side.SELL));

        orderbookSell.cancelAcked(clOrdId_first);
        assertEquals(2, orderbookSell.getOrderEntries().size());
        assertEquals(0, orderbookSell.getOutstandingOrderCount());
        assertEquals(0, orderSender.getNoOrders(Side.SELL));
    }

    @Test
    public void test_11_PostponeOrders_NoCancel() {
        orderHolderSell.setWaitCancelAck(true);
        orderHolderSell.setOrder(6L, price + 5);
        orderHolderSell.setOrder(6L, price);
        orderHolderSell.commit(updateId + 1);
        assertEquals(2, orderbookSell.getOrderEntries().size());
        assertEquals(0, orderbookSell.getOutstandingOrderCount());
        assertEquals(0, orderSender.getNoOrders(Side.SELL));

        long clOrdId_first = clOrdId.addAndGet(delta);
        long clOrdId_second = clOrdId.addAndGet(delta);
        orderbookSell.orderAcked(clOrdId_first);
        orderbookSell.orderAcked(clOrdId_second);
        assertEquals(2, orderbookSell.getOutstandingOrderCount());

        orderHolderBuy.setWaitCancelAck(true);
        orderHolderBuy.setOrder(6L, price - 5);
        orderHolderBuy.setOrder(6L, price - 10);
        orderHolderBuy.commit(updateId + 2);
        assertEquals(2, orderbookBuy.getOrderEntries().size());
        assertEquals(0, orderbookBuy.getOutstandingOrderCount());
        assertEquals(0, orderSender.getNoOrders(Side.BUY));

        clOrdId_first = clOrdId.addAndGet(delta);
        clOrdId_second = clOrdId.addAndGet(delta);
        orderbookBuy.orderAcked(clOrdId_first);
        orderbookBuy.orderAcked(clOrdId_second);
        assertEquals(2, orderbookBuy.getOutstandingOrderCount());
    }

}
