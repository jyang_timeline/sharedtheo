package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.NonStackingNoModOrderBookBySide;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.types.PriceLevelPolicy;

@RunWith(MockitoJUnitRunner.class)
public class NonStackingOrderBookTest {

    @Mock
    private TradeProxyListener tradeProxyListener;
    @Mock
    private StrategyView view;
    @Mock
    private ExecReportSink statsCollector;
    @Mock
    private OrderSender orderSender;
    @Mock
    private TradeAppendix tradeAppendix;
    @Mock
    private PositionTracker positionTracker;

    private final int strategyId = 1;
    private final String strategyName = "JUnitTest";
    private final long updateId = 256;
    private double price = 100.0;

    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Contract contract;
    private final ClientOrderIdGenerator idGenerator;
    private final AtomicLong clOrdId;

    private OrderHolder orderHolder;
    private NonStackingNoModOrderBookBySide orderBook;

    public NonStackingOrderBookTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);

        final CoreControllerBuilder builder = new CoreControllerBuilder();
        builder.withMappingIdToStatusView(idToStatusView).build();

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
        idGenerator = new ClientOrderIdGenerator(strategyId);
        clOrdId = new AtomicLong(idGenerator.getNextId());
    }

    @Before
    public void setUp() throws ConditionNotFoundException {
        Mockito.when(positionTracker.getPosition()).thenReturn(0L);

        final PositionManager positionManager = PositionManagerBuilder.buildDummyPositionManager();
        final PriceManager priceManager = PriceManagerBuilder.buildDummyPriceManager();

        Mockito.when(tradeAppendix.getPositionManager()).thenReturn(positionManager);
        Mockito.when(tradeAppendix.getPriceManager()).thenReturn(priceManager);
        Mockito.when(tradeAppendix.getPositionTracker()).thenReturn(positionTracker);
        Mockito.when(tradeAppendix.getPriceLevelPolicy()).thenReturn(PriceLevelPolicy.SinglePriceLevel);

        orderHolder = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.BUY, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolder.setTradeProxyListener(tradeProxyListener);
        orderHolder.setSendOrderPermission(true);

        Whitebox.setInternalState(orderHolder, StrategyView.class, view);
        orderBook = Whitebox.getInternalState(orderHolder, "orderBook");
    }

    private int getExpectedTotalQty() {
        return Whitebox.getInternalState(orderBook, "clientTotalQty");
    }

    private int getExistingTotalQty() {
        return Whitebox.getInternalState(orderBook, "exchangeTotalQty");
    }

    private int getInFlightQty() {
        return Whitebox.getInternalState(orderBook, "inFlightQty");
    }

    private Map<?, ?> getClOrdIdToOrderMap() {
        return Whitebox.getInternalState(orderBook, "clOrdIdToOrder");
    }

    @Test
    public void testInvalidPriceOrQuantity() {
        orderHolder.setOrder(3L, 0d);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(3L, Double.NaN);
        orderHolder.commit(updateId + 2);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        // orderHolder.setOrder(3L, Double.MAX_VALUE);
        // orderHolder.commit(updateId + 3);
        // assertEquals(getExpectedTotalQty(), getExistingTotalQty());
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> NewOrder(B) with different price will trigger CancelOrder(A)
     *     -> CancelAcked(A) -> Re-Commit(B) -> OrderAcked(B)
     */
    @Test
    public void testCancelAcked() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, --price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(), 0L);
        assertEquals(getExistingTotalQty(), 6L);

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), 6L);
        assertEquals(getExistingTotalQty(), 0L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A) -> CancelByTrader(A) -> CancelAcked(A)
     */
    @Test
    public void testCancelAckedFromTrader() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> NewOrder(B) with different price will trigger CancelOrder(A)
     *     -> FillAck(A), no CancelAcked(A)
     */
    @Test
    public void testFillBeforeCancelAcked() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, --price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(), 0L);
        assertEquals(getExistingTotalQty(), 6L);

        orderBook.orderFilled(clOrdId.get(), 2L, 4L);
        assertEquals(getExpectedTotalQty(), -2L);
        assertEquals(getExistingTotalQty(), 4L);

        orderBook.orderFilled(clOrdId.get(), 3L, 1L);
        assertEquals(getExpectedTotalQty(), -5L);
        assertEquals(getExistingTotalQty(), 1L);

        orderBook.orderFilled(clOrdId.get(), 1L, 0L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> NewOrder(B) with different price will trigger CancelOrder(A)
     *     -> FillAck(A, partial) -> CancelAcked(A)
     */
    @Test
    public void testPartialFillBeforeCancelAcked() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, --price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(), 0L);
        assertEquals(getExistingTotalQty(), 6L);

        orderBook.orderFilled(clOrdId.get(), 2L, 4L);
        assertEquals(getExpectedTotalQty(), -2L);
        assertEquals(getExistingTotalQty(), 4L);

        orderBook.orderFilled(clOrdId.get(), 3L, 1L);
        assertEquals(getExpectedTotalQty(), -5L);
        assertEquals(getExistingTotalQty(), 1L);

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A) -> FillAck(A, partial)
     *   -> CancelByTrader(A) -> CancelAcked(A)
     */
    @Test
    public void testPartialFillWithCancelAckedFromTrader() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.get(), 2L, 4L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.get(), 3L, 1L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A)
     *   -> NewOrder(B) with quantity greater than Order(A) -> OrderAcked(B)
     *     -> FillAck(A), FillAck(B, partial)
     *       -> CancelByTrader(B) -> CancelAcked(B)
     */
    @Test
    public void testAppendOrderWithCancelAckedFromTrader() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 6L);
        final long firstClOrdId = clOrdId.addAndGet(100000000);

        orderBook.orderAcked(firstClOrdId);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(9L, price);
        orderHolder.commit(updateId);
        assertEquals(getExpectedTotalQty(), 9L);
        final long secondClOrdId = clOrdId.addAndGet(100000000);

        orderBook.orderAcked(secondClOrdId);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.get(), 3L, 6L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.addAndGet(-100000000), 2L, 4L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> NewOrder(B), wait for OrderAcked(A), signal re-commit
     *   -> OrderAcked(A) -> Re-Commit(B) with lesser quantity
     *     -> CancelOrder(A), wait for CancelAcked(A), signal re-commit
     *       -> CancelAcked(A) -> Re-Commit(B) -> OrderAcked(B)
     */
    // @Test
    public void testRecommitWithConsequentNewOrder() {
        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId);
        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A) -> CancelOrder(A)
     *   -> FillAck(A, partial) -> FillAck(A, partial) -> CancelOrder(A) -> FillAck(A, partial)
     */
    @Test
    public void testOrderCancelsBetweenPartialFills() {
        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 5L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.orderFilled(clOrdId.get(), 2L, 3L);
        orderBook.orderFilled(clOrdId.get(), 1L, 2L);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), -3L);

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), -3L);

        orderBook.orderFilled(clOrdId.get(), 2L, 0L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A) -> CancelOrder(A)
     *   -> FillAck(A, partial) -> CancelReject(A) -> FillAck(A)
     */
    @Test
    public void testOrderRejectWithPartialFills() {
        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 5L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.orderFilled(clOrdId.get(), 1L, 4L);
        assertEquals(getExistingTotalQty(), 4L);
        assertEquals(getExpectedTotalQty(), -1L);

        orderBook.cancelRejected(clOrdId.get(), RejectReason.U);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.get(), 4L, 0L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A) -> OrderAcked(A) -> CancelOrder(A)
     *   -> FillAck(A, partial) -> FillAck(A, partial) -> CancelOrder(A) -> CancelAcked(A)
     */
    @Test
    public void testOrderCancelsAfterPartialFills() {
        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 5L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.orderFilled(clOrdId.get(), 2L, 3L);
        orderBook.orderFilled(clOrdId.get(), 1L, 2L);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), -3L);

        orderHolder.setOrder(0L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), -3L);

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*
     * For multiple day orders at the same price
     */
    @Test
    public void testMultipleDayOrderSamePrice() {
        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(7L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 5L);
        assertEquals(getExpectedTotalQty(), 7L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderBook.orderFilled(clOrdId.addAndGet(-100000000), 3L, 2L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 4L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(200000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 5L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.cancelAcked(clOrdId.addAndGet(-100000000));
        assertEquals(getExistingTotalQty(), 3L);
        assertEquals(getExpectedTotalQty(), 2L);
        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        assertEquals(getInFlightQty(), 0);
    }

    /*-
     * NewOrder(A, 2@100) -> OrderAcked(A) -> NewOrder(B, 6@100) -> OrderAcked(B)
     *   -> NewOrder(C, 3@101) -> SendingNewOrder(C), CancelOrder(A), CancelOrder(B)
     *       -> OrderAcked(C) -> CancelAcked(A) -> CancelAcked(B)
     */
    // @Test
    public void testOrderEagerMode() {
        // orderBook.updateOrderExecutor(OrderExecutionMode.EAGER);

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(3L, price + 1);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 9L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(-200000000));
        assertEquals(getExistingTotalQty(), 7L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 3L);
        assertEquals(getExpectedTotalQty(), 3L);
    }

    /*-
     * NewOrder(A, 2@100) -> OrderAcked(A) -> NewOrder(B, 6@100) -> OrderAcked(B)
     *   -> NewOrder(C, 3@101) -> SendingNewOrder(C), CancelOrder(A), CancelOrder(B)
     *       -> FillAck(A, 2) -> OrderAcked(C) -> CancelReject(A) -> CancelAcked(B) -> FillAck(C, 3)
     */
    // @Test
    public void testOrderEagerModeWithFillAndReject() {
        // orderBook.updateOrderExecutor(OrderExecutionMode.EAGER);

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(3L, price + 1);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderBook.orderFilled(clOrdId.addAndGet(-100000000), 2L, 0L);
        assertEquals(getExistingTotalQty(), 4L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.orderAcked(clOrdId.addAndGet(200000000));
        assertEquals(getExistingTotalQty(), 7L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelRejected(clOrdId.addAndGet(-200000000), RejectReason.U);
        assertEquals(getExistingTotalQty(), 7L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
        orderBook.orderFilled(clOrdId.addAndGet(100000000), 3L, 0L);
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
    }

    /*-
     * NewOrder(A, 2@100) -> OrderAcked(A) -> NewOrder(B, 6@100) -> OrderAcked(B)
     *   -> NewOrder(C, 3@101) -> SendingNewOrder(C), CancelOrder(A), CancelOrder(B)
     *       -> NewOrder(D, 5@100, outward), O-Blocking
     *           -> OrderAcked(C), O-Blocking -> CancelAcked(A), O-Blocking -> CancelAcked(B), commit(D)
     *               -> CancelOrder(C) -> CancelAcked(C), commit(D)
     */
    // @Test
    public void testOrderEagerModeWithFollowingOutwardOrders() {
        // orderBook.updateOrderExecutor(OrderExecutionMode.EAGER);

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(3L, price + 1);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 9L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(-200000000));
        assertEquals(getExistingTotalQty(), 7L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 3L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);
    }

    /*-
     * NewOrder(A, 2@100) -> OrderAcked(A) -> NewOrder(B, 6@100) -> OrderAcked(B)
     *   -> NewOrder(C, 3@101) -> SendingNewOrder(C), CancelOrder(A), CancelOrder(B)
     *       -> NewOrder(D, 5@100, inward), O-Blocking
     *           -> OrderAcked(C), O-Blocking -> CancelAcked(A), O-Blocking -> CancelAcked(B), commit(D)
     *               -> SendingNewOrder(D), CancelOrder(C)
     *                   -> OrderAcked(D) -> CancelAcked(C)
     */
    // @Test
    public void testOrderEagerModeWithFollowingInwardOrders() {
        // orderBook.updateOrderExecutor(OrderExecutionMode.EAGER);

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(6L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 6L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.setOrder(3L, price + 1);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderHolder.setOrder(5L, price + 2);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 6L);
        assertEquals(getExpectedTotalQty(), 3L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 9L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(-200000000));
        assertEquals(getExistingTotalQty(), 7L);
        assertEquals(getExpectedTotalQty(), 3L);
        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 3L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.orderAcked(clOrdId.addAndGet(200000000));
        assertEquals(getExistingTotalQty(), 8L);
        assertEquals(getExpectedTotalQty(), 5L);
        orderBook.cancelAcked(clOrdId.addAndGet(-100000000));
        assertEquals(getExistingTotalQty(), getExpectedTotalQty());
    }

    // @Test
    public void testOrderEagerModeWithClearAll() {
        // orderBook.updateOrderExecutor(OrderExecutionMode.EAGER);

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());

        orderHolder.clearAllOrders();
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(), getExistingTotalQty());
    }

    @Test
    public void testGFDReset() {
        orderHolder.setOrder(5L, price);
        orderHolder.commit(updateId);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 5L);

        orderBook.reset();
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 0L);
        assertTrue(getClOrdIdToOrderMap().isEmpty());
        // assertTrue(getOrdersInFlightMap().isEmpty());

        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 0L);
        assertTrue(getClOrdIdToOrderMap().isEmpty());
        // assertTrue(getOrdersInFlightMap().isEmpty());

        orderHolder.setOrder(2L, price);
        orderHolder.commit(updateId + 1);
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 2L);
        orderBook.orderAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), getExpectedTotalQty());

        orderHolder.setOrder(2L, price + 1);
        orderHolder.commit(updateId + 2);
        assertEquals(getExistingTotalQty(), 2L);
        assertEquals(getExpectedTotalQty(), 0L);

        orderBook.reset();
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 0L);
        assertTrue(getClOrdIdToOrderMap().isEmpty());
        // assertTrue(getOrdersInFlightMap().isEmpty());

        orderBook.cancelAcked(clOrdId.addAndGet(100000000));
        assertEquals(getExistingTotalQty(), 0L);
        assertEquals(getExpectedTotalQty(), 0L);
        assertTrue(getClOrdIdToOrderMap().isEmpty());
        // assertTrue(getOrdersInFlightMap().isEmpty());
    }

}
