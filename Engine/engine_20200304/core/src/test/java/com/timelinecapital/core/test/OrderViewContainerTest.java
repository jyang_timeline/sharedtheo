package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.reflect.Whitebox;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.config.EngineMode;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.trade.TradeProxyListener;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.execution.IOCOrderBookBySide;
import com.timelinecapital.core.execution.NonStackingNoModOrderBookBySide;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.stats.ExecReportSink;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.types.PriceLevelPolicy;

@RunWith(MockitoJUnitRunner.class)
public class OrderViewContainerTest {

    @Mock
    private StrategyConfig config;
    @Mock
    private StrategyUpdater strategyUpdater;

    @Mock
    private OrderSender orderSender;
    @Mock
    private TradeProxyListener tradeProxyListener;
    @Mock
    private StrategyView view;
    @Mock
    private ExecReportSink statsCollector;
    @Mock
    private PositionTracker positionTracker;
    @Mock
    private TradeAppendix tradeAppendix;

    private final int strategyId = 1;
    private final String strategyName = "JUnitTest";
    private final long updateId = 256;
    private final double price = 100.0;
    private final long delta = 100000000;

    private final List<OrderHolder> allOrderViews = new ArrayList<>();
    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Contract contract;
    private final ClientOrderIdGenerator idGenerator;
    private final AtomicLong clOrdId;

    private final RiskManager riskManager;

    private OrderViewContainer orderViewContainer;

    private OrderHolder orderHolderBuyGFD;
    private OrderHolder orderHolderSellGFD;
    private OrderHolder orderHolderBuyIOC;
    private OrderHolder orderHolderSellIOC;

    private NonStackingNoModOrderBookBySide orderbookBuy;
    private NonStackingNoModOrderBookBySide orderbookSell;
    private IOCOrderBookBySide orderbookBuyIOC;
    private IOCOrderBookBySide orderBookSellIOC;

    private TradeEvent tradeEvent;

    public OrderViewContainerTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        riskManager = new RiskManager(RiskManagerConfig.getInstance());

        final CoreControllerBuilder builder = new CoreControllerBuilder();
        builder.withMappingIdToStatusView(idToStatusView).build();

        contract = ContractFactory.getDummyContract("IF1701", "CFFEX");
        idGenerator = new ClientOrderIdGenerator(strategyId);
        clOrdId = new AtomicLong(idGenerator.getNextId());
    }

    @Before
    public void setUp() throws ConditionNotFoundException {
        final PriceManager priceManager = PriceManagerBuilder.buildDummyPriceManager();

        Mockito.when(config.getInteger(StrategyConfigProtocol.Key.positionMaxOrderQty.toUntypedString())).thenReturn(1000);
        Mockito.when(config.getInteger(StrategyConfigProtocol.Key.positionMaxPositionPerSide.toUntypedString())).thenReturn(1000);
        Mockito.when(config.getInteger(StrategyConfigProtocol.Key.positionMaxOrderPerSide.toUntypedString())).thenReturn(10);
        final PositionManager positionManager = PositionManagerBuilder.buildPositionManager(contract, config);

        Mockito.when(config.getDouble("Strategy.Condition.maxLoss")).thenReturn(5000d);
        Mockito.when(config.getDouble("Strategy.Condition.maxDrawdown")).thenReturn(5000d);
        Mockito.when(config.getInteger("Strategy.Condition.maxRejectsPerInterval", riskManager.getRiskManagerConfig().getMaxRejectsPerInterval())).thenReturn(5);
        Mockito.when(config.getInteger("Strategy.Condition.maxCancelsPerInterval", riskManager.getRiskManagerConfig().getMaxCancelsPerInterval())).thenReturn(10);
        final ProtectiveManager riskControlManager = ProtectiveManagerBuilder.buildRiskControlManager(riskManager, config, strategyUpdater);

        Mockito.when(tradeAppendix.getRiskControlManager()).thenReturn(riskControlManager);
        Mockito.when(tradeAppendix.getPositionManager()).thenReturn(positionManager);
        Mockito.when(tradeAppendix.getPriceManager()).thenReturn(priceManager);
        Mockito.when(tradeAppendix.getPositionTracker()).thenReturn(positionTracker);
        Mockito.when(tradeAppendix.getPriceLevelPolicy()).thenReturn(PriceLevelPolicy.SinglePriceLevel);

        // orderHolderBuyGFD = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.BUY,
        // PriceLevelPolicy.SinglePriceLevel,
        // orderSender, positionManager, positionTracker, riskControlManager, strategyName);
        orderHolderBuyGFD = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.BUY, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolderBuyGFD.setTradeProxyListener(tradeProxyListener);
        orderHolderBuyGFD.setSendOrderPermission(true);
        // orderHolderSellGFD = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.SELL,
        // PriceLevelPolicy.SinglePriceLevel,
        // orderSender, positionManager, positionTracker, riskControlManager, strategyName);
        orderHolderSellGFD = new OrderHolder(idGenerator, contract, TimeCondition.GFD, Side.SELL, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolderSellGFD.setTradeProxyListener(tradeProxyListener);
        orderHolderSellGFD.setSendOrderPermission(true);
        // orderHolderBuyIOC = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.BUY,
        // PriceLevelPolicy.SinglePriceLevel,
        // orderSender, positionManager, positionTracker, riskControlManager, strategyName);
        orderHolderBuyIOC = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.BUY, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolderBuyIOC.setTradeProxyListener(tradeProxyListener);
        orderHolderBuyIOC.setSendOrderPermission(true);
        // orderHolderSellIOC = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.SELL,
        // PriceLevelPolicy.SinglePriceLevel,
        // orderSender, positionManager, positionTracker, riskControlManager, strategyName);
        orderHolderSellIOC = new OrderHolder(idGenerator, contract, TimeCondition.IOC, Side.SELL, tradeAppendix, orderSender, strategyName, statsCollector);
        orderHolderSellIOC.setTradeProxyListener(tradeProxyListener);
        orderHolderSellIOC.setSendOrderPermission(true);

        allOrderViews.add(orderHolderBuyGFD);
        allOrderViews.add(orderHolderSellGFD);
        allOrderViews.add(orderHolderBuyIOC);
        allOrderViews.add(orderHolderSellIOC);

        Whitebox.setInternalState(orderHolderBuyGFD, StrategyView.class, view);
        Whitebox.setInternalState(orderHolderSellGFD, StrategyView.class, view);
        Whitebox.setInternalState(orderHolderBuyIOC, StrategyView.class, view);
        Whitebox.setInternalState(orderHolderSellIOC, StrategyView.class, view);
        orderbookBuy = Whitebox.getInternalState(orderHolderBuyGFD, "orderBook");
        orderbookSell = Whitebox.getInternalState(orderHolderSellGFD, "orderBook");
        orderbookBuyIOC = Whitebox.getInternalState(orderHolderBuyIOC, "orderBook");
        orderBookSellIOC = Whitebox.getInternalState(orderHolderSellIOC, "orderBook");
    }

    private int getExpectedTotalQty(final NonStackingNoModOrderBookBySide orderBook) {
        return Whitebox.getInternalState(orderBook, "clientTotalQty");
    }

    private int getExistingTotalQty(final NonStackingNoModOrderBookBySide orderBook) {
        return Whitebox.getInternalState(orderBook, "exchangeTotalQty");
    }

    private long getInFlightOrderId(final IOCOrderBookBySide orderBook) {
        return Whitebox.getInternalState(orderBook, "inFlightOrderId");
    }

    @Test
    public void testIOCExecutions() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito.spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyIOCOrderView").set(orderViewContainer, orderHolderBuyIOC);
        MemberModifier.field(OrderViewContainer.class, "sellIOCOrderView").set(orderViewContainer, orderHolderSellIOC);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);

        tradeEvent = Whitebox.getInternalState(orderHolderBuyIOC, "execRptHandler");
        orderbookBuyIOC.setQty(5L, price);
        orderbookBuyIOC.commit(updateId, System.currentTimeMillis());
        assertEquals(getInFlightOrderId(orderbookBuyIOC), clOrdId.addAndGet(delta));
        tradeEvent.onOrderAck(clOrdId.get());
        tradeEvent.onFill(clOrdId.get(), 5L, 0L, price, 0.0d);
        assertEquals(getInFlightOrderId(orderbookBuyIOC), 0);

        orderbookBuyIOC.setQty(3L, price);
        orderbookBuyIOC.commit(updateId, System.currentTimeMillis());
        assertEquals(getInFlightOrderId(orderbookBuyIOC), clOrdId.addAndGet(delta));
        tradeEvent.onOrderAck(clOrdId.get());
        tradeEvent.onCancelAck(clOrdId.get());
        assertEquals(getInFlightOrderId(orderbookBuyIOC), 0);

        tradeEvent = Whitebox.getInternalState(orderHolderSellIOC, "execRptHandler");
        orderBookSellIOC.setQty(3L, price);
        orderBookSellIOC.commit(updateId, System.currentTimeMillis());
        assertEquals(getInFlightOrderId(orderBookSellIOC), clOrdId.addAndGet(delta));
        tradeEvent.onOrderAck(clOrdId.get());
        tradeEvent.onFill(clOrdId.get(), 2L, 1L, price, 0.0d);
        assertEquals(getInFlightOrderId(orderBookSellIOC), clOrdId.get());
        tradeEvent.onCancelAck(clOrdId.get());
        assertEquals(getInFlightOrderId(orderBookSellIOC), 0);
    }

    @Test
    public void testReplaceOrderExecutorWithCancelAndFill() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito
            .spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyDayOrderView").set(orderViewContainer, orderHolderBuyGFD);
        MemberModifier.field(OrderViewContainer.class, "sellDayOrderView").set(orderViewContainer, orderHolderSellGFD);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);

        orderHolderBuyGFD.setOrder(3L, price);
        orderHolderBuyGFD.commit(updateId);
        assertEquals(getExpectedTotalQty(orderbookBuy), 3L);
        assertEquals(getExistingTotalQty(orderbookBuy), 0L);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));
    }

    @Test
    public void testReplaceOrderExecutorWithRejectButFill() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito
            .spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyDayOrderView").set(orderViewContainer, orderHolderBuyGFD);
        MemberModifier.field(OrderViewContainer.class, "sellDayOrderView").set(orderViewContainer, orderHolderSellGFD);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);
        Mockito.when(positionTracker.getPosition()).thenReturn(5L);
        // Mockito.when(positionTracker.getOrderCount()).thenReturn(1);

        // orderbookBuy.updateOrderExecutor(OrderExecutionMode.DEFAULT);
        // orderbookSell.updateOrderExecutor(OrderExecutionMode.DEFAULT);
        orderHolderBuyGFD.setOrder(3L, price);
        orderHolderBuyGFD.commit(updateId);
        assertEquals(getExpectedTotalQty(orderbookBuy), 3L);
        assertEquals(getExistingTotalQty(orderbookBuy), 0L);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));
    }

    /*-
     * Default position is 5 -> NewOrderBuy(A) -> OrderAcked(A)
     *   -> ExitMode -> CancelOrder(A) -> FillAck(A) -> CancelReject(A)
     *     -> NewOrderSell(B) -> OrderAcked(B) -> FillAck(B)
     *       -> NewOrderBuy(C) -> BlockedInExitMode
     *         -> NewOrderSell(D) -> ReviseQtyInExitMode -> OrderAcked(D) -> FillAck(D)
     */
    @Test
    public void testTradingOutWithOutstandingOrdersAndFills() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito
            .spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyDayOrderView").set(orderViewContainer, orderHolderBuyGFD);
        MemberModifier.field(OrderViewContainer.class, "sellDayOrderView").set(orderViewContainer, orderHolderSellGFD);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);
        Mockito.when(positionTracker.getPosition()).thenReturn(5L);
        Mockito.when(positionTracker.getOrderCount()).thenReturn(1);

        orderHolderBuyGFD.setOrder(300L, price);
        orderHolderBuyGFD.commit(updateId);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        orderViewContainer.onExitMode();
        assertEquals(getExpectedTotalQty(orderbookBuy), 0L);
        assertEquals(getExistingTotalQty(orderbookBuy), 300L);

        orderbookBuy.orderFilled(clOrdId.get(), 300L, 0L);
        orderbookBuy.cancelRejected(clOrdId.get(), RejectReason.U);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(800L);
        orderHolderSellGFD.setOrder(500L, price);
        orderHolderSellGFD.commit(updateId + 1);
        orderbookSell.orderAcked(clOrdId.addAndGet(delta));
        orderbookSell.orderFilled(clOrdId.get(), 500L, 0L);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));

        Mockito.when(positionTracker.getPosition()).thenReturn(300L);
        orderHolderBuyGFD.setOrder(500L, price);
        orderHolderBuyGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(300L);
        orderHolderSellGFD.setOrder(500L, price);
        orderHolderSellGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookSell), 300L);
        assertEquals(getExistingTotalQty(orderbookSell), 0L);
        orderbookSell.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));
        orderbookSell.orderFilled(clOrdId.get(), 300L, 0L);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));
    }

    /*-
     * Default position is 5 -> NewOrderBuy(A) -> OrderAcked(A)
     *   -> ExitMode -> CancelOrder(A) -> CancelAck(A)
     *     -> NewOrderSell(B) -> OrderAcked(B) -> FillAck(B)
     *       -> NewOrderBuy(C) -> Revise Qty to 0 in ExitMode -> order permission set to false
     *         -> NewOrderSell(D) -> Revise Qty to 0 in ExitMode -> order permission set to false
     *           -> NewOrder -> skip order sending in ExitMode
     */
    @Test
    public void testTradingOutWithOutstandingOrdersAndCancels() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito
            .spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyDayOrderView").set(orderViewContainer, orderHolderBuyGFD);
        MemberModifier.field(OrderViewContainer.class, "sellDayOrderView").set(orderViewContainer, orderHolderSellGFD);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);
        Mockito.when(positionTracker.getPosition()).thenReturn(5L);
        Mockito.when(positionTracker.getOrderCount()).thenReturn(1);

        orderHolderBuyGFD.setOrder(3L, price);
        orderHolderBuyGFD.commit(updateId);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        orderViewContainer.onExitMode();
        assertEquals(getExpectedTotalQty(orderbookBuy), 0L);
        assertEquals(getExistingTotalQty(orderbookBuy), 3L);

        orderbookBuy.cancelAcked(clOrdId.get());
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(5L);
        orderHolderSellGFD.setOrder(5L, price);
        orderHolderSellGFD.commit(updateId + 1);
        orderbookSell.orderAcked(clOrdId.addAndGet(delta));
        orderbookSell.orderFilled(clOrdId.get(), 5L, 0L);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));

        Mockito.when(positionTracker.getPosition()).thenReturn(0L);
        orderHolderBuyGFD.setOrder(5L, price);
        orderHolderBuyGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        // Mockito.when(positionTracker.getPosition()).thenReturn(0L);
        orderHolderSellGFD.setOrder(5L, price);
        orderHolderSellGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));

        // Mockito.when(positionTracker.getPosition()).thenReturn(0L);
        orderHolderBuyGFD.setOrder(5L, price);
        orderHolderBuyGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));
    }

    @Test
    public void testTradingOutWithOutstandingOrders() throws IllegalArgumentException, IllegalAccessException {
        orderViewContainer = PowerMockito
            .spy(new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, strategyName));
        MemberModifier.field(OrderViewContainer.class, "positionTracker").set(orderViewContainer, positionTracker);
        MemberModifier.field(OrderViewContainer.class, "buyDayOrderView").set(orderViewContainer, orderHolderBuyGFD);
        MemberModifier.field(OrderViewContainer.class, "sellDayOrderView").set(orderViewContainer, orderHolderSellGFD);
        MemberModifier.field(OrderViewContainer.class, "allOrderViews").set(orderViewContainer, allOrderViews);
        Mockito.when(positionTracker.getPosition()).thenReturn(-500L);
        Mockito.when(positionTracker.getOrderCount()).thenReturn(1);

        orderHolderBuyGFD.setOrder(300L, price);
        orderHolderBuyGFD.commit(updateId);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        orderViewContainer.onExitMode();
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        orderbookBuy.orderFilled(clOrdId.get(), 300L, 0L);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        orderHolderBuyGFD.setOrder(300L, price);
        orderHolderBuyGFD.commit(updateId);
        orderbookBuy.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));
        orderbookBuy.orderFilled(clOrdId.get(), 300L, 0L);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(100L);

        orderHolderBuyGFD.setOrder(500L, price);
        orderHolderBuyGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(100L);
        orderHolderSellGFD.setOrder(300L, price);
        orderHolderSellGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookSell), 100L);
        assertEquals(getExistingTotalQty(orderbookSell), 0L);
        orderbookSell.orderAcked(clOrdId.addAndGet(delta));
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));
        orderbookSell.orderFilled(clOrdId.get(), 100L, 0L);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));

        Mockito.when(positionTracker.getPosition()).thenReturn(0L);
        orderHolderBuyGFD.setOrder(500L, price);
        orderHolderBuyGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookBuy), getExistingTotalQty(orderbookBuy));

        Mockito.when(positionTracker.getPosition()).thenReturn(0L);
        orderHolderSellGFD.setOrder(500L, price);
        orderHolderSellGFD.commit(updateId + 1);
        assertEquals(getExpectedTotalQty(orderbookSell), getExistingTotalQty(orderbookSell));
    }

}
