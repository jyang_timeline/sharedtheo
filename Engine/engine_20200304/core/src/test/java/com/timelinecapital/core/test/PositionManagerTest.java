package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;

@RunWith(MockitoJUnitRunner.class)
public class PositionManagerTest {

    @Mock
    private Contract contract;
    @Mock
    private Config config;
    @Mock
    private StrategyUpdater strategyUpdater;

    private final int maxQuantity = 500;
    private final int minQuantity = 200;
    private final int maxPosition = maxQuantity * 2;
    private final int maxOrders = 3;

    private PositionManager positionManager;

    public PositionManagerTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        new RiskManager(RiskManagerConfig.getInstance());
    }

    @Before
    public void setUp() throws Exception {
        Mockito.when(config.getInteger("Strategy.Condition.maxPositionPerSide")).thenReturn(maxPosition);
        Mockito.when(config.getInteger("Strategy.Condition.maxOrderQuantity")).thenReturn(maxQuantity);
        Mockito.when(config.getInteger("Strategy.Condition.minOrderQuantity")).thenReturn(minQuantity);
        Mockito.when(config.getInteger("Strategy.Condition.maxOrdersPerSide")).thenReturn(maxOrders);
        Mockito.when(config.getInteger("Strategy.Condition.subsequentOrderDelay")).thenReturn(null);

        positionManager = PositionManagerBuilder.buildPositionManager(contract, config);
    }

    @Test
    public void testOrders() throws ConditionNotFoundException {
        int existingOrders = 0;
        boolean canSend = true;
        for (int i = 0; i < maxOrders + 1; i++) {
            canSend = positionManager.canSendNewOrder(Side.BUY, 90L, 100L, existingOrders);
            if (canSend) {
                existingOrders++;
            }
            if (i != maxOrders) {
                assertEquals(canSend, true);
                assertThat(existingOrders, Matchers.lessThanOrEqualTo(maxOrders));
            } else {
                assertEquals(canSend, false);
                assertEquals(existingOrders, maxOrders);
            }
        }

        Mockito.when(config.getInteger("Strategy.Condition.maxOrdersPerSide")).thenReturn(0);
        positionManager.updateConfig(config);

        existingOrders = 0;
        canSend = positionManager.canSendNewOrder(Side.BUY, 90L, 100L, existingOrders);
        assertEquals(canSend, false);
    }

    @Test
    public void testQty() throws ConditionNotFoundException {
        int currentPosition = 0;
        long sendingQuantity = 0;

        // 1st. buy (sent: min, expected: min, position: less than maxPosition)
        sendingQuantity = positionManager.adjustQuantity(Side.BUY, minQuantity, currentPosition);
        currentPosition += sendingQuantity;
        assertEquals(sendingQuantity, minQuantity);
        assertThat(currentPosition, Matchers.lessThanOrEqualTo(maxPosition));

        // 2nd. buy (sent: max+N, expected: max, position: less than maxPosition)
        sendingQuantity = positionManager.adjustQuantity(Side.BUY, maxQuantity + 300L, currentPosition);
        currentPosition += sendingQuantity;
        assertEquals(sendingQuantity, maxQuantity);
        assertThat(currentPosition, Matchers.lessThanOrEqualTo(maxPosition));

        // 3rd. buy (sent: max, expected: less than max, position: at maxPosition)
        sendingQuantity = positionManager.adjustQuantity(Side.BUY, maxQuantity, currentPosition);
        currentPosition += sendingQuantity;
        assertThat((int) sendingQuantity, Matchers.lessThanOrEqualTo(maxQuantity));
        assertEquals(currentPosition, maxPosition);

        currentPosition = -200;

        // 1st. sell (sent: min, expected: min, position: greater than -maxPosition)
        sendingQuantity = positionManager.adjustQuantity(Side.SELL, minQuantity, currentPosition);
        currentPosition -= sendingQuantity;
        assertEquals(sendingQuantity, minQuantity);
        assertThat(currentPosition, Matchers.greaterThanOrEqualTo(-maxPosition));

        // 2nd. sell (sent: max+N, expected: max, position: greater than -maxPosition)
        sendingQuantity = positionManager.adjustQuantity(Side.SELL, maxQuantity, currentPosition);
        currentPosition -= sendingQuantity;
        assertEquals(sendingQuantity, maxQuantity);
        assertThat(currentPosition, Matchers.greaterThanOrEqualTo(-maxPosition));

        // 3rd. sell (sent: min, expected: 0, position: not being changed)
        sendingQuantity = positionManager.adjustQuantity(Side.SELL, minQuantity, currentPosition);
        currentPosition -= sendingQuantity;
        assertEquals(sendingQuantity, 0L);
        assertThat(currentPosition, Matchers.greaterThanOrEqualTo(-maxPosition));

        currentPosition = 0;

        // test onConfigChange of of quantity condition
        Mockito.when(config.getInteger("Strategy.Condition.maxOrderQuantity")).thenReturn(0);
        positionManager.updateConfig(config);
        // maxOrderQuantity.onConfigChange(config);
        sendingQuantity = positionManager.adjustQuantity(Side.BUY, minQuantity, currentPosition);
        currentPosition += sendingQuantity;
        assertEquals(sendingQuantity, 0L);
        assertThat(currentPosition, Matchers.lessThanOrEqualTo(maxPosition));
    }

    // @Test
    // public void testInvalidQuote() {
    // double sendingPrice = 0.0;
    //
    // // buying, price at limit up
    // Mockito.when(quoteSnapShot.getQuoteType()).thenReturn(QuoteType.LIMIT_UP);
    // double tradingPrice = 92.0;
    // sendingPrice = positionManager.adjustPrice(Side.BUY, tradingPrice);
    // assertThat(sendingPrice, Matchers.lessThan(tradingPrice));
    //
    // // selling, price at limit down
    // Mockito.when(quoteSnapShot.getQuoteType()).thenReturn(QuoteType.LIMIT_DOWN);
    // tradingPrice = 92.0;
    // sendingPrice = positionManager.adjustPrice(Side.SELL, tradingPrice);
    // assertThat(sendingPrice, Matchers.greaterThan(tradingPrice));
    // }

    @Test
    public void testManualFill() {
        final double incomingPrice = 100.0;
        long incomingQty = maxPosition + 1;
        boolean canImport;

        canImport = positionManager.isValidFill(Side.BUY, incomingPrice, incomingQty);
        assertFalse(canImport);
        canImport = positionManager.isValidFill(Side.SELL, incomingPrice, incomingQty);
        assertFalse(canImport);

        incomingQty = maxPosition;
        canImport = positionManager.isValidFill(Side.BUY, incomingPrice, incomingQty);
        assertTrue(canImport);
        canImport = positionManager.isValidFill(Side.SELL, incomingPrice, incomingQty);
        assertTrue(canImport);

        incomingQty = maxPosition - 1;
        canImport = positionManager.isValidFill(Side.BUY, incomingPrice, incomingQty);
        assertTrue(canImport);
        canImport = positionManager.isValidFill(Side.SELL, incomingPrice, incomingQty);
        assertTrue(canImport);
    }

}
