package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.position.PositionTracker;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickCalculator;

@RunWith(MockitoJUnitRunner.class)
public class PositionTrackerTest {

    @Mock
    private Contract contract;

    private final TickCalculator tickCalc = new FixedTickCalculator(1, 1000);

    @Before
    public void setUp() throws Exception {
        Mockito.when(contract.getTickCalculator()).thenReturn(tickCalc);
    }

    @Test
    public void testTrackFill() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        positionTracker.trackFill(Side.BUY, 2L, 1000.0, 4.5);
        positionTracker.trackFill(Side.SELL, 3L, 1002.0, 4.5);
        positionTracker.trackFill(Side.SELL, 1L, 1003.0, 4.5);
        positionTracker.trackFill(Side.BUY, 2L, 1001.0, 4.5);

        assertEquals(7000, positionTracker.getClosedPnl(), 0.0);
    }

    @Test
    public void testGetOrderCountBuy1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=2
        positionTracker.trackFill(Side.BUY, 2L, 1000.0, 4.5);

        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);

        assertEquals(1, positionTracker.getOrderCount());

        // 3. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);

        assertEquals(0, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountSell1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=2
        positionTracker.trackFill(Side.SELL, 2L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 3. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountBuy2() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=2
        positionTracker.trackFill(Side.BUY, 2L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=SELL, QTY=3
        positionTracker.trackFill(Side.SELL, 3L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountSell2() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=2
        positionTracker.trackFill(Side.SELL, 2L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=BUY, QTY=3
        positionTracker.trackFill(Side.BUY, 3L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountBuy3() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(2, positionTracker.getOrderCount());

        // 3. Fill with Side=SELL, QTY=2
        positionTracker.trackFill(Side.SELL, 2L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountSell3() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(2, positionTracker.getOrderCount());

        // 3. Fill with Side=BUY, QTY=2
        positionTracker.trackFill(Side.BUY, 2L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountBuy4() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(2, positionTracker.getOrderCount());

        // 3. Fill with Side=SELL, QTY=3
        positionTracker.trackFill(Side.SELL, 3L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());
    }

    @Test
    public void testGetOrderCountSell4() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());

        // 2. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(2, positionTracker.getOrderCount());

        // 3. Fill with Side=BUY, QTY=4
        positionTracker.trackFill(Side.BUY, 4L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getOrderCount());
    }

    @Test
    public void testGetPosition() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getPosition());

        // 2. Fill with Side=BUY, QTY=2
        positionTracker.trackFill(Side.BUY, 2L, 1000.0, 4.5);
        assertEquals(3, positionTracker.getPosition());

        // 3. Fill with Side=SELL, QTY=2
        positionTracker.trackFill(Side.SELL, 2L, 1000.0, 4.5);
        assertEquals(1, positionTracker.getPosition());

        // 4. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getPosition());

        // 5. Fill with Side=SELL, QTY=1
        positionTracker.trackFill(Side.SELL, 1L, 1000.0, 4.5);
        assertEquals(-1, positionTracker.getPosition());

        // 6. Fill with Side=SELL, QTY=2
        positionTracker.trackFill(Side.SELL, 2L, 1000.0, 4.5);
        assertEquals(-3, positionTracker.getPosition());

        // 7. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(-2, positionTracker.getPosition());

        // 8. Fill with Side=BUY, QTY=4
        positionTracker.trackFill(Side.BUY, 4L, 1000.0, 4.5);
        assertEquals(2, positionTracker.getPosition());

        // 9. Fill with Side=SELL, QTY=3
        positionTracker.trackFill(Side.SELL, 3L, 1000.0, 4.5);
        assertEquals(-1, positionTracker.getPosition());

        // 10. Fill with Side=BUY, QTY=1
        positionTracker.trackFill(Side.BUY, 1L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getPosition());
    }

    @Test
    public void testGetCurrentPnlBuyWithoutClosedPnl() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        positionTracker.trackFill(Side.BUY, 3L, 1000.0, 4.5);

        assertEquals(4.5 * 1, positionTracker.getTotalChargedFee(), 0.0);

        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);
        assertEquals(150000, positionTracker.getTotalPnl(1050), 0.0);
        assertEquals(-150000, positionTracker.getTotalPnl(950), 0.0);
        assertEquals(69000, positionTracker.getTotalPnl(1023), 0.0);
    }

    @Test
    public void testGetCurrentPnlSellWithoutClosedPnl() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        positionTracker.trackFill(Side.SELL, 3L, 1000.0, 4.5);

        assertEquals(4.5 * 1, positionTracker.getTotalChargedFee(), 0.0);

        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);
        assertEquals(-150000, positionTracker.getTotalPnl(1050), 0.0);
        assertEquals(150000, positionTracker.getTotalPnl(950), 0.0);
        assertEquals(-69000, positionTracker.getTotalPnl(1023), 0.0);
    }

    @Test
    public void testGetCurrentPnlBuy1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.BUY, 6L, 1000.0, 4.5);
        assertEquals(4.5 * 1, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);

        // 2. Fill with Side=SELL, QTY=1, PRICE=1050
        positionTracker.trackFill(Side.SELL, 1L, 1050.0, 4.5);
        assertEquals(4.5 * 2, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(50000, positionTracker.getTotalPnl(1000), 0.0);

        // 3. Fill with Side=SELL, QTY=1, PRICE=1030
        positionTracker.trackFill(Side.SELL, 1L, 1030.0, 4.5);
        assertEquals(4.5 * 3, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(80000, positionTracker.getTotalPnl(1000), 0.0);

        // 4. Fill with Side=SELL, QTY=1, PRICE=980
        positionTracker.trackFill(Side.SELL, 1L, 980.0, 4.5);
        assertEquals(4.5 * 4, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(60000, positionTracker.getTotalPnl(1000), 0.0);

        // 5. Fill with Side=SELL, QTY=1, PRICE=940
        positionTracker.trackFill(Side.SELL, 1L, 940.0, 4.5);
        assertEquals(4.5 * 5, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);

        // 6. Fill with Side=SELL, QTY=1, PRICE=1020
        positionTracker.trackFill(Side.SELL, 1L, 1020.0, 4.5);
        assertEquals(4.5 * 6, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(20000, positionTracker.getTotalPnl(1000), 0.0);

        // 7. Fill with Side=SELL, QTY=1, PRICE=932
        positionTracker.trackFill(Side.SELL, 1L, 932.0, 4.5);
        assertEquals(4.5 * 7, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(-48000, positionTracker.getTotalPnl(1000), 0.0);
    }

    @Test
    public void testGetCurrentPnlSell1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.SELL, 6L, 1000.0, 4.5);
        assertEquals(4.5 * 1, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);

        // 2. Fill with Side=BUY, QTY=1, PRICE=950
        positionTracker.trackFill(Side.BUY, 1L, 950.0, 4.5);
        assertEquals(4.5 * 2, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(50000, positionTracker.getTotalPnl(1000), 0.0);

        // 3. Fill with Side=BUY, QTY=1, PRICE=970
        positionTracker.trackFill(Side.BUY, 1L, 970.0, 4.5);
        assertEquals(4.5 * 3, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(80000, positionTracker.getTotalPnl(1000), 0.0);

        // 4. Fill with Side=BUY, QTY=1, PRICE=1020
        positionTracker.trackFill(Side.BUY, 1L, 1020.0, 4.5);
        assertEquals(4.5 * 4, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(60000, positionTracker.getTotalPnl(1000), 0.0);

        // 5. Fill with Side=BUY, QTY=1, PRICE=1060
        positionTracker.trackFill(Side.BUY, 1L, 1060.0, 4.5);
        assertEquals(4.5 * 5, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(0, positionTracker.getTotalPnl(1000), 0.0);

        // 6. Fill with Side=BUY, QTY=1, PRICE=980
        positionTracker.trackFill(Side.BUY, 1L, 980.0, 4.5);
        assertEquals(4.5 * 6, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(20000, positionTracker.getTotalPnl(1000), 0.0);

        // 7. Fill with Side=BUY, QTY=1, PRICE=1068
        positionTracker.trackFill(Side.BUY, 1L, 1068.0, 4.5);
        assertEquals(4.5 * 7, positionTracker.getTotalChargedFee(), 0.0);
        assertEquals(-48000, positionTracker.getTotalPnl(1000), 0.0);
    }

    @Test
    public void testGetClosedPnlBuy1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.BUY, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 2. Fill with Side=SELL, QTY=1, PRICE=1050
        positionTracker.trackFill(Side.SELL, 1L, 1050.0, 4.5);
        assertEquals(50000, positionTracker.getClosedPnl(), 0.0);

        // 3. Fill with Side=SELL, QTY=1, PRICE=1030
        positionTracker.trackFill(Side.SELL, 1L, 1030.0, 4.5);
        assertEquals(80000, positionTracker.getClosedPnl(), 0.0);
        // 4. Fill with Side=SELL, QTY=1, PRICE=980
        positionTracker.trackFill(Side.SELL, 1L, 980.0, 4.5);
        assertEquals(60000, positionTracker.getClosedPnl(), 0.0);

        // 5. Fill with Side=SELL, QTY=1, PRICE=940
        positionTracker.trackFill(Side.SELL, 1L, 940.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 6. Fill with Side=SELL, QTY=1, PRICE=1020
        positionTracker.trackFill(Side.SELL, 1L, 1020.0, 4.5);
        assertEquals(20000, positionTracker.getClosedPnl(), 0.0);

        // 7. Fill with Side=SELL, QTY=1, PRICE=932
        positionTracker.trackFill(Side.SELL, 1L, 932.0, 4.5);
        assertEquals(-48000, positionTracker.getClosedPnl(), 0.0);
    }

    @Test
    public void testGetClosedPnlSell1() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.SELL, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 2. Fill with Side=BUY, QTY=1, PRICE=950
        positionTracker.trackFill(Side.BUY, 1L, 950.0, 4.5);
        assertEquals(50000, positionTracker.getClosedPnl(), 0.0);

        // 3. Fill with Side=BUY, QTY=1, PRICE=970
        positionTracker.trackFill(Side.BUY, 1L, 970.0, 4.5);
        assertEquals(80000, positionTracker.getClosedPnl(), 0.0);
        // 4. Fill with Side=BUY, QTY=1, PRICE=1020
        positionTracker.trackFill(Side.BUY, 1L, 1020.0, 4.5);
        assertEquals(60000, positionTracker.getClosedPnl(), 0.0);

        // 5. Fill with Side=BUY, QTY=1, PRICE=1060
        positionTracker.trackFill(Side.BUY, 1L, 1060.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 6. Fill with Side=BUY, QTY=1, PRICE=980
        positionTracker.trackFill(Side.BUY, 1L, 980.0, 4.5);
        assertEquals(20000, positionTracker.getClosedPnl(), 0.0);

        // 7. Fill with Side=BUY, QTY=1, PRICE=1068
        positionTracker.trackFill(Side.BUY, 1L, 1068.0, 4.5);
        assertEquals(-48000, positionTracker.getClosedPnl(), 0.0);
    }

    @Test
    public void testGetClosedPnlBuy2() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=BUY, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.BUY, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 2. Fill with Side=SELL, QTY=1, PRICE=980
        positionTracker.trackFill(Side.SELL, 1L, 980.0, 4.5);
        assertEquals(-20000, positionTracker.getClosedPnl(), 0.0);

        // 3. Fill with Side=SELL, QTY=1, PRICE=900
        positionTracker.trackFill(Side.SELL, 1L, 900.0, 4.5);
        assertEquals(-120000, positionTracker.getClosedPnl(), 0.0);

        // 4. Fill with Side=SELL, QTY=1, PRICE=1050
        positionTracker.trackFill(Side.SELL, 1L, 1050.0, 4.5);
        assertEquals(-70000, positionTracker.getClosedPnl(), 0.0);

        // 5. Fill with Side=SELL, QTY=1, PRICE=1070
        positionTracker.trackFill(Side.SELL, 1L, 1070.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 6. Fill with Side=SELL, QTY=1, PRICE=960
        positionTracker.trackFill(Side.SELL, 1L, 960.0, 4.5);
        assertEquals(-40000, positionTracker.getClosedPnl(), 0.0);

        // 7. Fill with Side=SELL, QTY=1, PRICE=1099
        positionTracker.trackFill(Side.SELL, 1L, 1099.0, 4.5);
        assertEquals(59000, positionTracker.getClosedPnl(), 0.0);
    }

    @Test
    public void testGetClosedPnlSell2() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // 1. Fill with Side=SELL, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.SELL, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 2. Fill with Side=BUY, QTY=1, PRICE=1020
        positionTracker.trackFill(Side.BUY, 1L, 1020.0, 4.5);
        assertEquals(-20000, positionTracker.getClosedPnl(), 0.0);

        // 3. Fill with Side=BUY, QTY=1, PRICE=1100
        positionTracker.trackFill(Side.BUY, 1L, 1100.0, 4.5);
        assertEquals(-120000, positionTracker.getClosedPnl(), 0.0);

        // 4. Fill with Side=BUY, QTY=1, PRICE=950
        positionTracker.trackFill(Side.BUY, 1L, 950.0, 4.5);
        assertEquals(-70000, positionTracker.getClosedPnl(), 0.0);

        // 5. Fill with Side=BUY, QTY=1, PRICE=930
        positionTracker.trackFill(Side.BUY, 1L, 930.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        // 6. Fill with Side=BUY, QTY=1, PRICE=1040
        positionTracker.trackFill(Side.BUY, 1L, 1040.0, 4.5);
        assertEquals(-40000, positionTracker.getClosedPnl(), 0.0);

        // 7. Fill with Side=BUY, QTY=1, PRICE=901
        positionTracker.trackFill(Side.BUY, 1L, 901.0, 4.5);
        assertEquals(59000, positionTracker.getClosedPnl(), 0.0);
    }

    @Test
    public void testGetOpenPnlWithShort() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // Fill with Side=SELL, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.SELL, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        double marketPrice = PriceUtils.getMidPoint(QuoteType.QUOTE, 980.0, 1060.0);
        assertEquals(-120000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID_BID, 0.0, 1060.0);
        assertEquals(-360000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.LIMIT_DOWN, 0.0, 960.0);
        assertEquals(240000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID_ASK, 950.0, 0.0);
        assertEquals(300000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.LIMIT_UP, 1150.0, 0.0);
        assertEquals(-900000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID, 0.0, 0.0);
        assertEquals(0.0, positionTracker.getOpenPnl(marketPrice), 0.0);
    }

    @Test
    public void testGetOpenPnlWithLong() {
        final PositionTracker positionTracker = new PositionTracker(contract);

        // Fill with Side=BUY, QTY=6, PRICE=1000
        positionTracker.trackFill(Side.BUY, 6L, 1000.0, 4.5);
        assertEquals(0, positionTracker.getClosedPnl(), 0.0);

        double marketPrice = PriceUtils.getMidPoint(QuoteType.QUOTE, 980.0, 1060.0);
        assertEquals(120000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID_BID, 0.0, 1060.0);
        assertEquals(360000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.LIMIT_DOWN, 0.0, 960.0);
        assertEquals(-240000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID_ASK, 950.0, 0.0);
        assertEquals(-300000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.LIMIT_UP, 1150.0, 0.0);
        assertEquals(900000, positionTracker.getOpenPnl(marketPrice), 0.0);

        marketPrice = PriceUtils.getMidPoint(QuoteType.INVALID, 0.0, 0.0);
        assertEquals(0.0, positionTracker.getOpenPnl(marketPrice), 0.0);
    }

}
