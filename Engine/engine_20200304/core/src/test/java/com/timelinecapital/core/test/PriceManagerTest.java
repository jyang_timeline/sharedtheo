package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PriceManager;
import com.timelinecapital.core.riskmanagement.PriceManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;

@RunWith(MockitoJUnitRunner.class)
public class PriceManagerTest {

    @Mock
    private final Contract contract;
    @Mock
    private QuoteSnapshot quoteSnapShot;
    @Mock
    private BookView bookView;
    @Mock
    private Config config;

    private FixedTickCalculator fixedTickCalculator;
    private PriceManager priceManager;

    private final RiskManager riskManager;

    private final double tickSize = 1.0;
    private final double currentAsk = 100.0;
    private final double currentBid = 95.0;

    private final int maxCrossTicks = 5;
    private final double maxCrossPercentage = 1.0;

    public PriceManagerTest() throws ConfigurationException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        riskManager = new RiskManager(RiskManagerConfig.getInstance());

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
    }

    @Before
    public void setUp() throws Exception {
        fixedTickCalculator = new FixedTickCalculator(tickSize, 10.0);
        Mockito.when(quoteSnapShot.getContract()).thenReturn(contract);
        Mockito.when(quoteSnapShot.getContract().getTickCalculator()).thenReturn(fixedTickCalculator);
        Mockito.when(quoteSnapShot.getBookView()).thenReturn(bookView);
        Mockito.when(bookView.getAskPrice()).thenReturn(currentAsk);
        Mockito.when(bookView.getBidPrice()).thenReturn(currentBid);
        Mockito.when(quoteSnapShot.getQuoteType()).thenReturn(QuoteType.QUOTE);
    }

    @Test
    public void testPriceByPercentage() throws ConditionNotFoundException {
        Mockito.when(config.getInteger("Strategy.Condition.maxCrossTicks")).thenReturn(null);
        Mockito.when(config.getDouble("Strategy.Condition.maxCrossPercentage")).thenReturn(maxCrossPercentage);
        priceManager = PriceManagerBuilder.buildPriceManager(riskManager, quoteSnapShot, config);
        double sendingPrice = 0.0;

        // buying, cross price by percentage
        double tradingPrice = 102.0;
        sendingPrice = priceManager.adjustPrice(Side.BUY, tradingPrice);
        assertThat(sendingPrice, Matchers.lessThan(tradingPrice));

        // selling, cross price by percentage
        tradingPrice = 93.0;
        sendingPrice = priceManager.adjustPrice(Side.SELL, tradingPrice);
        assertThat(sendingPrice, Matchers.greaterThan(tradingPrice));

        // place buying, within percentage condition
        tradingPrice = 100.0;
        sendingPrice = priceManager.adjustPrice(Side.BUY, tradingPrice);
        assertEquals(sendingPrice, tradingPrice, 0.0);

        // place selling, within percentage condition
        tradingPrice = 95.0;
        sendingPrice = priceManager.adjustPrice(Side.SELL, tradingPrice);
        assertEquals(sendingPrice, tradingPrice, 0.0);
    }

    @Test
    public void testPriceByTicks() throws ConditionNotFoundException {
        Mockito.when(config.getInteger("Strategy.Condition.maxCrossTicks")).thenReturn(maxCrossTicks);
        Mockito.when(config.getDouble("Strategy.Condition.maxCrossPercentage")).thenReturn(null);
        priceManager = PriceManagerBuilder.buildPriceManager(riskManager, quoteSnapShot, config);
        double sendingPrice = 0.0;

        // buying, cross price by ticks
        double tradingPrice = 108.0;
        sendingPrice = priceManager.adjustPrice(Side.BUY, tradingPrice);
        assertThat(sendingPrice, Matchers.lessThan(tradingPrice));

        // selling, cross price by ticks
        tradingPrice = 88.0;
        sendingPrice = priceManager.adjustPrice(Side.SELL, tradingPrice);
        assertThat(sendingPrice, Matchers.greaterThan(tradingPrice));

        // place buying, within ticks condition
        tradingPrice = 104.0;
        sendingPrice = priceManager.adjustPrice(Side.BUY, tradingPrice);
        assertEquals(sendingPrice, tradingPrice, 0.0);

        // place selling, within ticks condition
        tradingPrice = 93.0;
        sendingPrice = priceManager.adjustPrice(Side.SELL, tradingPrice);
        assertEquals(sendingPrice, tradingPrice, 0.0);
    }

}
