package com.timelinecapital.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.reflect.Whitebox;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.config.EngineMode;
import com.nogle.core.position.PositionTracker;
import com.nogle.core.position.condition.MaxCancelsPerInterval;
import com.nogle.core.position.condition.MaxDrawdown;
import com.nogle.core.position.condition.MaxLoss;
import com.nogle.core.position.condition.MaxRejectsPerInterval;
import com.nogle.core.position.condition.types.BurstinessCondition;
import com.nogle.core.scheduler.StrategyScheduleService;
import com.nogle.core.strategy.OrderViewCache;
import com.nogle.core.strategy.OrderViewContainer;
import com.nogle.core.strategy.QuoteSnapshotCache;
import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.strategy.event.ProtectionHandler;
import com.nogle.core.strategy.logging.StrategyTradeLogger;
import com.nogle.core.trade.OrderSender;
import com.nogle.core.types.RejectReason;
import com.nogle.core.util.ClientOrderIdGenerator;
import com.nogle.core.util.TradeClock;
import com.nogle.strategy.api.StrategyBuilder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.TradeAppendix;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.Reject;
import com.timelinecapital.core.execution.report.factory.ExecReportFactory;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.ProtectiveManagerBuilder;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.strategy.StrategyConfig;
import com.timelinecapital.core.types.PriceLevelPolicy;
import com.timelinecapital.view.TradeStatistics;

@SuppressStaticInitializationFor({ "com.nogle.messaging.Messenger" })
@RunWith(MockitoJUnitRunner.class)
public class RiskManagerTest {

    @Mock
    private StrategyConfig config;
    @Mock
    private QuoteSnapshotCache quoteSnapshotCache;
    @Mock
    private TradeStatistics tradeStats;
    @Mock
    private StrategyScheduleService timeChecker;
    @Mock
    private StrategyBuilder strategyBuilder;
    @Mock
    private StrategyTradeLogger strategyTradeLogger;
    @Mock
    private OrderSender orderSender;
    @Mock
    private TradeAppendix tradeAppendix;

    private final int strategyId = 1;
    private final String strategyName = "Strategy";
    private final int loopCount = 100;
    private final int rejectsLimit;
    private final int cancelsLimit;
    private final double drawdownLimit = 5000;
    private final double lossLimit = 3000;
    private final long defaultQty = 1;
    private final double defaultCommission = 0;

    private final Map<String, Instrument> contractMap = new HashMap<>();
    private final Map<Contract, StrategyQuoteSnapshot> contractToQuoteSnapshot = new HashMap<>();
    private final Map<Contract, OrderViewContainer> contractToOrderview = new HashMap<>();

    private final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private final Instrument contract;
    private final ClientOrderIdGenerator idGenerator;
    private final AtomicLong clOrdId;

    private final OrderViewCache orderViewCache;
    private final PositionTracker positionTracker;
    private final RiskManager riskManager;

    private StrategyUpdater strategyUpdater;
    private StrategyView view;
    private ProtectionHandler protectionHandler;
    private ProtectiveManager riskControlManager;

    private MaxRejectsPerInterval maxRejectsPerInterval;
    private MaxCancelsPerInterval maxCancelsPerInterval;
    private MaxDrawdown maxDrawdown;
    private MaxLoss maxLoss;

    public RiskManagerTest() throws ConfigurationException {
        TradeClock.setMode(EngineMode.SIMULATION);

        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        RiskManagerConfig.forceBuildConfig();

        riskManager = new RiskManager(RiskManagerConfig.getInstance());
        rejectsLimit = riskManager.getRiskManagerConfig().getMaxRejectsPerInterval();
        cancelsLimit = riskManager.getRiskManagerConfig().getMaxCancelsPerInterval();

        final CoreControllerBuilder builder = new CoreControllerBuilder();
        builder.withMappingIdToStatusView(idToStatusView).build();

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
        idGenerator = new ClientOrderIdGenerator(strategyId);
        clOrdId = new AtomicLong(idGenerator.getNextId());

        contractMap.put("IF1601", contract);

        positionTracker = new PositionTracker(contract);
        orderViewCache = new OrderViewCache();
    }

    @Before
    public void setUp() throws Exception {
        view = Mockito.spy(new StrategyView(
            strategyId,
            strategyName,
            contractToQuoteSnapshot,
            contractToOrderview));
        idToStatusView.put(strategyId, view);

        Mockito.when(config.getName()).thenReturn(strategyName);
        Mockito.when(config.getDouble("Strategy.Condition.maxDrawdown")).thenReturn(drawdownLimit);
        Mockito.when(config.getDouble("Strategy.Condition.maxLoss")).thenReturn(lossLimit);
        Mockito.when(config.getInteger(StrategyConfigProtocol.Key.positionMaxRejectsPerInterval.toUntypedString(), riskManager.getRiskManagerConfig().getMaxRejectsPerInterval()))
            .thenReturn(rejectsLimit);
        Mockito.when(config.getInteger(StrategyConfigProtocol.Key.positionMaxCancelsPerInterval.toUntypedString(), riskManager.getRiskManagerConfig().getMaxCancelsPerInterval()))
            .thenReturn(cancelsLimit);

        final PositionManager positionManager = PositionManagerBuilder.buildDummyPositionManager();

        Mockito.when(tradeAppendix.getRiskControlManager()).thenReturn(riskControlManager);
        Mockito.when(tradeAppendix.getPositionManager()).thenReturn(positionManager);
        Mockito.when(tradeAppendix.getPositionTracker()).thenReturn(positionTracker);
        Mockito.when(tradeAppendix.getPriceLevelPolicy()).thenReturn(PriceLevelPolicy.SinglePriceLevel);

        final OrderViewContainer orderViewContainer = new OrderViewContainer(idGenerator, contract, tradeAppendix, orderSender, StringUtils.EMPTY);
        orderViewCache.addOrderView(contract, orderViewContainer);
        contractToOrderview.put(contract, orderViewContainer);

        strategyUpdater = new StrategyUpdater(
            strategyId,
            config,
            timeChecker,
            strategyBuilder,
            strategyTradeLogger,
            view,
            tradeStats,
            quoteSnapshotCache,
            orderViewCache,
            contractMap,
            new HashMap<>(),
            null);

        riskControlManager = ProtectiveManagerBuilder.buildRiskControlManager(riskManager, config, strategyUpdater);

        maxRejectsPerInterval = riskControlManager.getConditionsByBurstiness(MaxRejectsPerInterval.class).get();
        maxCancelsPerInterval = riskControlManager.getConditionsByBurstiness(MaxCancelsPerInterval.class).get();
        maxDrawdown = riskControlManager.getConditionsByBurstiness(MaxDrawdown.class).get();
        maxLoss = riskControlManager.getConditionsByBurstiness(MaxLoss.class).get();

        protectionHandler = new ProtectionHandler(riskControlManager);

        strategyUpdater.onTradingEnable();
    }

    private Fill generateFills(final long orderId, final Side side, final double price) {
        final OrderFill fill = ExecReportFactory.getFill();
        fill.setFill(contract, side, TimeCondition.GFD, orderId, 1L, price, 0.0d);
        fill.setRemainingQty(0);
        return fill;
    }

    private void generatePNL(final double cumPnl, final double closePnl) {
        final double basePrice = 2500;
        double buyPrice = basePrice;
        double sellPrice = basePrice;
        if (closePnl >= 0) {
            buyPrice = basePrice;
            sellPrice = basePrice + Math.abs(closePnl);
        } else {
            sellPrice = basePrice;
            buyPrice = basePrice + Math.abs(closePnl);
        }
        Mockito.when(view.getTotalPnl()).thenReturn(cumPnl + closePnl);

        Fill fill = generateFills(clOrdId.addAndGet(1), Side.BUY, buyPrice);
        positionTracker.trackFill(Side.BUY, defaultQty, buyPrice, defaultCommission);
        protectionHandler.add(fill);
        protectionHandler.handle();

        fill = generateFills(clOrdId.addAndGet(1), Side.SELL, sellPrice);
        positionTracker.trackFill(Side.SELL, defaultQty, sellPrice, defaultCommission);
        protectionHandler.add(fill);
        protectionHandler.handle();
    }

    @Test
    public void testDrawdownProtection() throws Exception {
        assertTrue(strategyUpdater.getStrategyStatusView().canStrategySendOrder());

        // Trigger onWatch (peak@2000, loss@-7000)
        generatePNL(0, 2000.0);
        generatePNL(2000.0, -4000.0);
        generatePNL(-2000.0, -3000.0);
        boolean isOnWatch = Whitebox.getInternalState(maxDrawdown, "isOnWatch");
        double peak = Whitebox.getInternalState(maxDrawdown, "peak");
        assertTrue(isOnWatch);
        assertEquals(2000, peak, 0);

        // Dismiss and examine peak value (peak@-5000)
        maxDrawdown.dismiss();
        isOnWatch = Whitebox.getInternalState(maxDrawdown, "isOnWatch");
        double drawdown = Whitebox.getInternalState(maxDrawdown, "maxDrawdown");
        peak = Whitebox.getInternalState(maxDrawdown, "peak");
        assertTrue(!isOnWatch);
        assertEquals(drawdownLimit, Math.abs(drawdown), 0);
        assertEquals(-5000, peak, 0);

        // Trigger onWatch (peak@-2500, loss@-5000)
        generatePNL(-5000.0, 2500);
        generatePNL(-2500, -5000);
        isOnWatch = Whitebox.getInternalState(maxDrawdown, "isOnWatch");
        drawdown = Whitebox.getInternalState(maxDrawdown, "maxDrawdown");
        peak = Whitebox.getInternalState(maxDrawdown, "peak");
        assertTrue(isOnWatch);
        assertEquals(drawdownLimit, Math.abs(drawdown), 0);
        assertEquals(-5000 + 2500, peak, 0);

        // Dismiss and examine peak value (peak@-7500)
        maxDrawdown.dismiss();
        isOnWatch = Whitebox.getInternalState(maxDrawdown, "isOnWatch");
        drawdown = Whitebox.getInternalState(maxDrawdown, "maxDrawdown");
        peak = Whitebox.getInternalState(maxDrawdown, "peak");
        assertTrue(!isOnWatch);
        assertEquals(drawdownLimit, Math.abs(drawdown), 0);
        assertEquals(-7500, peak, 0);
    }

    @Test
    public void testLossProtection() throws Exception {
        assertTrue(strategyUpdater.getStrategyStatusView().canStrategySendOrder());

        // Trigger onWatch (maxLoss@-3000, pnl@-3300)
        generatePNL(0, 500);
        generatePNL(500, -3800);
        boolean isOnWatch = Whitebox.getInternalState(maxLoss, "isOnWatch");
        assertTrue(isOnWatch);

        // Dismiss (nextLoss@-6300, pnl@-3300)
        maxLoss.dismiss();
        isOnWatch = Whitebox.getInternalState(maxLoss, "isOnWatch");
        double nextCheckpoint = Whitebox.getInternalState(maxLoss, "maxLoss");
        double resumePoint = Whitebox.getInternalState(maxLoss, "resumePoint");
        assertTrue(!isOnWatch);
        assertEquals(lossLimit + 3300, Math.abs(nextCheckpoint), 0);
        assertEquals(-3300, resumePoint, 0);

        // Trigger onWatch (nextLoss@-6300 pnl@-7000)
        generatePNL(-3300, -1900);
        generatePNL(-5200, -1800);
        isOnWatch = Whitebox.getInternalState(maxLoss, "isOnWatch");
        nextCheckpoint = Whitebox.getInternalState(maxLoss, "maxLoss");
        assertTrue(isOnWatch);
        assertTrue((resumePoint - 1900 - 1800) <= nextCheckpoint);

        // Dismiss (nextLoss@-10000 pnl@-7000)
        maxLoss.dismiss();
        isOnWatch = Whitebox.getInternalState(maxLoss, "isOnWatch");
        nextCheckpoint = Whitebox.getInternalState(maxLoss, "maxLoss");
        resumePoint = Whitebox.getInternalState(maxLoss, "resumePoint");
        assertTrue(!isOnWatch);
        assertEquals(lossLimit + 3300 + 1900 + 1800, Math.abs(nextCheckpoint), 0);
        assertEquals(-3300 - 1900 - 1800, resumePoint, 0);
    }

    @Test
    public void testRejectProtectionInvoked() throws Exception {
        TradeClock.setCurrentMillis(1476720000000L);
        maxRejectsPerInterval.enableRiskControl();
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i);
            final Reject reject = ExecReportFactory.getReject();
            reject.setReject(contract, Side.SELL, TimeCondition.GFD, ExecType.NEWORDER_ACK, 128L, RejectReason.X);
            maxRejectsPerInterval.onWatch(BurstinessCondition.EventType.Reject, reject);
        }
        assertEquals(view.canStrategySendOrder(), false);
    }

    @Test
    public void testRejectProtectionNotInvoked() throws Exception {
        TradeClock.setCurrentMillis(1476720000000L);
        maxRejectsPerInterval.enableRiskControl();
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i * 10 * 1000);
            final Reject reject = ExecReportFactory.getReject();
            reject.setReject(contract, Side.SELL, TimeCondition.GFD, ExecType.NEWORDER_ACK, 128L, RejectReason.U);
            maxRejectsPerInterval.onWatch(BurstinessCondition.EventType.Reject, reject);
        }
        assertEquals(view.canStrategySendOrder(), true);
    }

    @Test
    public void testRejectQueueMaintenance() throws Exception {
        TradeClock.setCurrentMillis(1476720000000L);
        maxRejectsPerInterval.enableRiskControl();
        final boolean isOnWatch = Whitebox.getInternalState(maxRejectsPerInterval, "isOnWatch");
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i * 60 * 1000);
            final Reject reject = ExecReportFactory.getReject();
            reject.setReject(contract, Side.SELL, TimeCondition.GFD, ExecType.NEWORDER_ACK, 128L, RejectReason.U);
            maxRejectsPerInterval.onWatch(BurstinessCondition.EventType.Reject, reject);
            assertTrue(!isOnWatch);
        }
        final CircularFifoQueue<Long> queue = Whitebox.getInternalState(maxRejectsPerInterval, "rejectQueue");
        assertEquals(queue.size(), rejectsLimit);
        assertEquals(view.canStrategySendOrder(), true);
    }

    @Test
    public void testCacnelProtectionInvoked() {
        TradeClock.setCurrentMillis(1476720000000L);
        maxCancelsPerInterval.enableRiskControl();
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i);
            final Ack cancelAck = ExecReportFactory.getAck();
            cancelAck.setAck(contract, Side.SELL, TimeCondition.GFD, ExecType.CANCEL_ACK, 256L);
            maxCancelsPerInterval.onWatch(BurstinessCondition.EventType.CancelAck, cancelAck);
        }
        assertEquals(view.canStrategySendOrder(), false);
    }

    @Test
    public void testCancelProtectionNotInvoked() throws Exception {
        TradeClock.setCurrentMillis(1476720000000L);
        maxCancelsPerInterval.enableRiskControl();
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i * 10 * 1000);
            final Ack cancelAck = ExecReportFactory.getAck();
            cancelAck.setAck(contract, Side.SELL, TimeCondition.GFD, ExecType.CANCEL_ACK, 256L);
            maxCancelsPerInterval.onWatch(BurstinessCondition.EventType.CancelAck, cancelAck);
        }
        assertEquals(view.canStrategySendOrder(), true);
    }

    @Test
    public void testCancelQueueMaintenance() throws Exception {
        TradeClock.setCurrentMillis(1476720000000L);
        maxCancelsPerInterval.enableRiskControl();
        final boolean isOnWatch = Whitebox.getInternalState(maxCancelsPerInterval, "isOnWatch");
        for (int i = 0; i < loopCount; i++) {
            TradeClock.setCurrentMillis(1476720000000L + i * 60 * 1000);
            final Ack cancelAck = ExecReportFactory.getAck();
            cancelAck.setAck(contract, Side.SELL, TimeCondition.GFD, ExecType.CANCEL_ACK, 256L);
            maxCancelsPerInterval.onWatch(BurstinessCondition.EventType.CancelAck, cancelAck);
            assertTrue(!isOnWatch);
        }
        final Map<Contract, CircularFifoQueue<Long>> queues = Whitebox.getInternalState(maxCancelsPerInterval, "cancelQueues");
        final CircularFifoQueue<Long> contractCancels = queues.get(contract);
        assertEquals(contractCancels.size(), cancelsLimit);
        assertEquals(view.canStrategySendOrder(), true);
    }

}
