package com.timelinecapital.core.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.regex.Matcher;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.config.EngineMode;
import com.nogle.core.exception.InvalidCodeNameException;
import com.nogle.core.exception.InvalidSymbolException;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.factory.FactoryBase;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.util.SymbolPatternUtil;

@RunWith(MockitoJUnitRunner.class)
public class SymbolInquiryPatternTest {

    private static String TRADINGDAY = "20191210";

    private static String SHFE_SYMBOL_NOEXG = "cu2001";
    private static String SHFE_SYMBOL = "cu2001@SHFE";
    private static String SHFE_PRODUCT = "cu@SHFE";

    private static String CZCE_SYMBOL_NOEXG = "AP001";
    private static String CZCE_SYMBOL = "AP001@CZCE";
    private static String CZCE_PRODUCT = "AP@CZCE";

    private static String DCE_SYMBOL_NOEXG_I = "i2001";
    private static String DCE_SYMBOL_NOEXG_P = "p2003";
    private static String DCE_SYMBOL_NOEXG_PP = "pp2003";
    private static String DCE_SYMBOL_I = "i2001@DCE";
    private static String DCE_SYMBOL_P = "p2003@DCE";
    private static String DCE_SYMBOL_PP = "pp2003@DCE";
    private static String DCE_PRODUCT_V = "v@DCE";
    private static String DCE_PRODUCT_P = "p@DCE";
    private static String DCE_PRODUCT_PP = "pp@DCE";

    private static String SHFE_PRODUCT_RANK = "cu&2@SHFE";
    private static String SHFE_PRODUCT_VOLUME = "cu?v@SHFE";
    private static String SHFE_PRODUCT_OPENINTEREST = "cu?o@SHFE";
    private static String SHFE_PRODUCT_RANK_VOLUME = "cu?v&1@SHFE";
    private static String SHFE_PRODUCT_RANK_OPENINTEREST = "cu?o&1@SHFE";

    private static String CZCE_PRODUCT_RANK = "MA@CZCE";
    private static String CZCE_PRODUCT_VOLUME = "MA?v@CZCE";
    private static String CZCE_PRODUCT_RANK_VOLUME = "MA&2?v@CZCE";

    private static String DCE_PRODUCT_RANK = "v@DCE";
    private static String DCE_PRODUCT_VOLUME = "v?v@DCE";
    private static String DCE_PRODUCT_RANK_VOLUME = "v?v&3@DCE";

    public SymbolInquiryPatternTest() throws ConfigurationException, FileNotFoundException {
        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.TESTING);
        FactoryBase.init(EngineMode.TESTING, new HashMap<>(), "", null, false, false, false);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFuturesWithSymbolCodeOnly() {
        final String[] targets = new String[] { SHFE_SYMBOL_NOEXG, CZCE_SYMBOL_NOEXG, DCE_SYMBOL_NOEXG_I, DCE_SYMBOL_NOEXG_P, DCE_SYMBOL_NOEXG_PP };

        for (int i = 0; i < targets.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(targets[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(targets[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(targets[i]);
            assertFalse(matcher.matches());

            Instrument instrument = null;
            try {
                instrument = ContractFactory.getByTradingDay(targets[i], TRADINGDAY);
            } catch (final InvalidSymbolException e) {
                assertNull(instrument);
            } catch (final InvalidCodeNameException e) {
                assertNull(instrument);
            }
        }
    }

    @Test
    public void testFuturesWithSymbolCode() throws InvalidCodeNameException {
        final String[] targets = new String[] { SHFE_SYMBOL, CZCE_SYMBOL, DCE_SYMBOL_I, DCE_SYMBOL_P, DCE_SYMBOL_PP };

        for (int i = 0; i < targets.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(targets[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(targets[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(targets[i]);
            assertFalse(matcher.matches());

            final Instrument instrument = ContractFactory.getByTradingDay(targets[i], TRADINGDAY);
            assertNotNull(instrument);
        }
    }

    @Test
    public void testFuturesWithProduct() throws InvalidCodeNameException {
        final String[] targets = new String[] { SHFE_PRODUCT, CZCE_PRODUCT, DCE_PRODUCT_V, DCE_PRODUCT_P, DCE_PRODUCT_PP };

        for (int i = 0; i < targets.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(targets[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(targets[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(targets[i]);
            assertFalse(matcher.matches());

            final Instrument instrument = ContractFactory.getByTradingDay(targets[i], TRADINGDAY);
            assertNotNull(instrument);
        }
    }

    @Test
    public void testFuturesWithProductRank() throws InvalidCodeNameException {
        final String[] targets = new String[] { SHFE_PRODUCT_RANK, CZCE_PRODUCT_RANK, DCE_PRODUCT_RANK };

        for (int i = 0; i < targets.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(targets[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(targets[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(targets[i]);
            assertFalse(matcher.matches());

            final Instrument instrument = ContractFactory.getByTradingDay(targets[i], TRADINGDAY);
            assertNotNull(instrument);
        }
    }

    @Test
    public void testFuturesWithRank() {
        final String[] targets = new String[] { SHFE_PRODUCT_VOLUME, SHFE_PRODUCT_OPENINTEREST, CZCE_PRODUCT_VOLUME, DCE_PRODUCT_VOLUME };

        for (int i = 0; i < targets.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(targets[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(targets[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(targets[i]);
            assertFalse(matcher.matches());

            Instrument instrument = null;
            try {
                instrument = ContractFactory.getByTradingDay(targets[i], TRADINGDAY);
            } catch (final InvalidCodeNameException e) {
                assertNull(instrument);
            }
        }
    }

    @Test
    public void testFuturesWithActivityRank() throws InvalidCodeNameException {
        final String[] volumeTarget = new String[] { SHFE_PRODUCT_RANK_VOLUME, CZCE_PRODUCT_RANK_VOLUME, DCE_PRODUCT_RANK_VOLUME };
        for (int i = 0; i < volumeTarget.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(volumeTarget[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(volumeTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(volumeTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(volumeTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(volumeTarget[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(volumeTarget[i]);
            assertFalse(matcher.matches());

            final Instrument instrument = ContractFactory.getByTradingDay(volumeTarget[i], TRADINGDAY);
            assertNotNull(instrument);
        }

        final String[] openInterestTarget = new String[] { SHFE_PRODUCT_RANK_OPENINTEREST };
        for (int i = 0; i < openInterestTarget.length; i++) {
            final Matcher exchangeMatcher = SymbolPatternUtil.CN_STOCK_EXCHANGE.matcher(openInterestTarget[i]);
            assertFalse(exchangeMatcher.matches());

            Matcher matcher = SymbolPatternUtil.CODENAME_FUTURES.matcher(openInterestTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES.matcher(openInterestTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_RNK.matcher(openInterestTarget[i]);
            assertFalse(matcher.matches());

            matcher = SymbolPatternUtil.SYMBOL_DESCRIPTOR_FUTURES_GRP_ACT_RNK.matcher(openInterestTarget[i]);
            assertTrue(matcher.matches());

            matcher = SymbolPatternUtil.PREFERENCE.matcher(openInterestTarget[i]);
            assertFalse(matcher.matches());

            final Instrument instrument = ContractFactory.getByTradingDay(openInterestTarget[i], TRADINGDAY);
            assertNotNull(instrument);
        }

    }

}
