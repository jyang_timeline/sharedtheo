package com.timelinecapital.deprecated;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.commons.utils.StrategyHeartbeat;
import com.nogle.core.EngineStatusView;
import com.nogle.core.EventTaskFactory;
import com.nogle.core.strategy.StrategyView;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.core.scheduler.NotificationService;
import com.timelinecapital.core.scheduler.ScheduledEventListener;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.watchdog.WatchdogEventHandler;

@Deprecated
public class NotificationEmitter {
    private static final Logger log = LogManager.getLogger(NotificationEmitter.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final List<StrategyHeartbeat> heartbeats = new ArrayList<>();
    private static final List<StrategyView> statusViewList = Collections.synchronizedList(new ArrayList<>());
    private static EngineStatusView engineStatusView;

    private static final TagValueEncoder scheduleMsgEncoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
    private static final TagValueEncoder stateChangeEncoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
    private static final MessageHeader heartbeatHeader = new MessageHeader("TE/Engine/Heartbeat");
    private static final MessageHeader notificationHeader = new MessageHeader(Messenger.getClientId() + "/Strategy/Heartbeat");

    private final BinaryMessage engineNotification = new BinaryMessage(heartbeatHeader, null);
    private final BinaryMessage strategyNotification = new BinaryMessage(notificationHeader, null);

    private static NotificationService notificationService;

    private static WatchdogEventHandler watchdogEventHandler;
    private final OneTimeTask task;

    private static NotificationEmitter INSTANCE;

    public static NotificationEmitter getInstance() throws ParseException {
        if (INSTANCE == null) {
            INSTANCE = new NotificationEmitter();
        }
        return INSTANCE;
    }

    private NotificationEmitter() throws ParseException {
        engineStatusView = EngineStatusView.getInstance();
        notificationService = new NotificationService("EngineStatus", new CronExpression(" * * * ? * * "));
        task = new OneTimeTask();
        watchdogEventHandler = new WatchdogEventHandler(OverviewFactory.getContractOverviews(), statusViewList);
    }

    public void start() {
    }

    // String onQueryEngineStatus() {
    // try {
    // final String engStatus = mapper.writeValueAsString(engineStatusView.toDetail());
    // task.setStatus(engStatus);
    // notificationService.setSingleJobListener(task, 3);
    // return engStatus;
    // } catch (final JsonProcessingException e) {
    // log.error(e.getMessage(), e);
    // }
    // return StringUtils.EMPTY;
    // }

    void notifyStrategyHeartBeat(final List<StrategyView> statusViewList) {
        heartbeats.clear();
        for (final StrategyView view : statusViewList) {
            view.onExitCheck();
            final StrategyHeartbeat tradeHeartbeat = view.onStatusRefresh();
            heartbeats.add(tradeHeartbeat);
        }

        try {
            scheduleMsgEncoder.clear();
            scheduleMsgEncoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeStrategyInformation);
            scheduleMsgEncoder.append(TradeEngineCommunication.engineModelsDetailTag, mapper.writeValueAsString(heartbeats));
            ofStrategy(scheduleMsgEncoder.compose());
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    void notifyStrategyHeartBeat(final StrategyView view) {
        try {
            stateChangeEncoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeStrategyInformation);
            stateChangeEncoder.append(TradeEngineCommunication.engineModelsDetailTag,
                mapper.writeValueAsString(Arrays.asList(view.onStatusRefresh())));
            ofStrategy(stateChangeEncoder.compose());
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    void notifyEngineHeartBeat(final EngineStatusView engineStatusView) {
        try {
            final String engineHeartbeat = mapper.writeValueAsString(engineStatusView.translateToHeartbeat());
            ofEngine(engineHeartbeat);
            log.info(engineHeartbeat);
        } catch (final JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    void notifyWatchdogs() {
        EventTaskFactory.getSystemTask().invoke(watchdogEventHandler);
    }

    private void ofEngine(final String message) {
        engineNotification.setPayLoad(message.getBytes(StandardCharsets.UTF_8));
        try {
            Messenger.send(engineNotification);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void ofStrategy(final String message) {
        strategyNotification.setPayLoad(message.getBytes(StandardCharsets.UTF_8));
        try {
            Messenger.send(strategyNotification);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    class OneTimeTask implements ScheduledEventListener {
        private String status;

        void setStatus(final String status) {
            this.status = status;
        }

        @Override
        public void onScheduledEvent() {
            ofEngine(status);
            log.info(status);
        }
    }

}
