package com.timelinecapital.deprecated;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.util.parser.CancelOrderParser;
import com.timelinecapital.core.execution.factory.RequestInstance;

public class OrderCancelWrapper extends CancelOrderParser {

    private long clOrdId;
    private long sourceTime;
    private long commitTime;
    private long handoverTime;
    private TradeEvent event;

    public final void initFrom(final RequestInstance request, final TradeEvent event) {
        this.clOrdId = request.getClOrderId();
        setClOrdId(clOrdId);

        this.sourceTime = request.getSourceEventTime();
        this.commitTime = request.getEventTime();
        this.event = event;
        this.handoverTime = TradeClock.getCurrentMicrosOnly();
    }

    @Override
    public final long getClOrdId() {
        return clOrdId;
    }

    public final long getSourceTime() {
        return sourceTime;
    }

    public final long getCommitTime() {
        return commitTime;
    }

    public final long getHandoverTime() {
        return handoverTime;
    }

    public final TradeEvent getTradeEvent() {
        return event;
    }

}
