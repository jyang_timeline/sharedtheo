package com.timelinecapital.deprecated;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.util.TradeClock;
import com.nogle.util.parser.NewOrderParser;
import com.timelinecapital.core.execution.factory.RequestInstance;

public class OrderEntryWrapper extends NewOrderParser {

    private long clOrdId;
    private long sourceTime;
    private long commitTime;
    private long handoverTime;
    private TradeEvent event;

    public final void initFrom(final RequestInstance request, final TradeEvent event) {
        this.clOrdId = request.getClOrderId();
        setClOrdId(clOrdId);
        // setSymbol(request.getContract().getSymbol());
        setSide(request.getSide().getCode());
        setTimeCondition(request.getTimeCondition().getCode());
        setQty(request.getQuantity());
        setPrice(request.getPrice());
        setPositionPolicy(request.getPositionPolicy().getPolicyCode());
        setPositionmax(request.getPositionMax());

        this.sourceTime = request.getSourceEventTime();
        this.commitTime = request.getEventTime();
        this.event = event;
        this.handoverTime = TradeClock.getCurrentMicrosOnly();
    }

    @Override
    public final long getClOrdId() {
        return this.clOrdId;
    }

    public final long getSourceTime() {
        return sourceTime;
    }

    public final long getCommitTime() {
        return commitTime;
    }

    public final long getHandoverTime() {
        return handoverTime;
    }

    public final TradeEvent getTradeEvent() {
        return event;
    }

}
