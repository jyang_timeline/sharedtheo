package com.timelinecapital.deprecated;

import com.nogle.strategy.types.QuoteType;

@Deprecated
public class QuoteTypeParser {

    public static final char QuoteTypeQuoteChar = 'Q';
    public static final char QuoteTypeNormalChar = 'N';
    public static final char QuoteTypeAskChar = 'A';
    public static final char QuoteTypeBidChar = 'B';
    public static final char QuoteTypeLimitUpChar = 'U';
    public static final char QuoteTypeLimitDownChar = 'D';
    public static final char QuoteTypeInvalidChar = 'I';

    public static QuoteType fromByte(final byte type) {
        return fromChar((char) type);
    }

    public static QuoteType fromChar(final char type) {
        switch (type) {
            case QuoteTypeQuoteChar:
            case QuoteTypeNormalChar:
                return QuoteType.QUOTE;
            case QuoteTypeAskChar:
                return QuoteType.INVALID_ASK;
            case QuoteTypeBidChar:
                return QuoteType.INVALID_BID;
            case QuoteTypeLimitUpChar:
                return QuoteType.LIMIT_UP;
            case QuoteTypeLimitDownChar:
                return QuoteType.LIMIT_DOWN;
            default:
                return QuoteType.INVALID;
        }
    }

    public static char toChar(final QuoteType quoteType) {
        switch (quoteType) {
            case QUOTE:
                return QuoteTypeNormalChar;
            case INVALID_ASK:
                return QuoteTypeAskChar;
            case INVALID_BID:
                return QuoteTypeBidChar;
            case LIMIT_UP:
                return QuoteTypeLimitUpChar;
            case LIMIT_DOWN:
                return QuoteTypeLimitDownChar;
            default:
                return QuoteTypeInvalidChar;
        }
    }

    public static boolean isValidForBid(final QuoteType quoteType) {
        return (quoteType != QuoteType.INVALID) && (quoteType != QuoteType.INVALID_BID) && (quoteType != QuoteType.LIMIT_DOWN);
    }

    public static boolean isValidForAsk(final QuoteType quoteType) {
        return (quoteType != QuoteType.INVALID) && (quoteType != QuoteType.INVALID_ASK) && (quoteType != QuoteType.LIMIT_UP);
    }

}
