package com.timelinecapital.deprecated;

import com.nogle.commons.protocol.TickTypeProtocol;
import com.nogle.strategy.types.TickType;

@Deprecated
public class TickTypeParser {

    public static TickType fromChar(final char input) {
        return fromByte((byte) input);
    }

    public static TickType fromByte(final byte type) {
        if (type == TickTypeProtocol.SELLER.toByte()) {
            return TickType.SELLER;
        } else if (type == TickTypeProtocol.BUYER.toByte()) {
            return TickType.BUYER;
        } else if (type == TickTypeProtocol.NO_AGGRESSOR.toByte()) {
            return TickType.NO_AGGRESSOR;
        } else {
            return TickType.UNKNOWN;
        }
    }

    public static char toChar(final TickType type) {
        switch (type) {
            case BUYER:
                return TickTypeProtocol.BUYER.toChar();
            case SELLER:
                return TickTypeProtocol.SELLER.toChar();
            case NO_AGGRESSOR:
                return TickTypeProtocol.NO_AGGRESSOR.toChar();
            default:
                return TickTypeProtocol.UNKNOWN.toChar();
        }
    }
}
