package com.timelinecapital.simulation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Map;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.core.config.EngineMode;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.marketdata.producer.metadata.MarketBookV3;
import com.nogle.core.marketdata.producer.metadata.QuoteHeaderEXGInfo;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.trade.simulation.ExchangeEventQueue;
import com.timelinecapital.core.trade.simulation.ExchangeMarketBook;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeBookMatchQueuingOrders {

    @Mock
    TradeEvent tradeEvent;
    @Mock
    FeeCalculator feeCalculator;
    @Mock
    ExchangeEventQueue delayedQueue;
    @Mock
    AckFactory ackFactory;
    @Mock
    FillFactory fillFactory;

    private final Contract contract;
    private final MarketBook marketbook;
    private final MarketBookParser parser;
    private final MarketBookEncoder encoder;
    private final OrderFill fill = new OrderFill();

    private final long fillPriceBanningDelayMicros = 200;

    private long exchangeTimeMicros;
    private double bidPrice = 95.0d;
    private long bidQty = 27;
    private double askPrice = 96.0d;
    private long askQty = 35;

    private Map<Long, SimulatedOrder> clOrdIdToOrder;

    private SimOrderExecutor simOrderExecutor;
    private SimulatedOrder simulatedOrder;
    private ExchangeMarketBook simMarketbook;

    public ExchangeBookMatchQueuingOrders() throws ConfigurationException {
        final String simDate = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));

        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.PRODUCTION);
        SimulationConfig.buildConfig(new File("simulation.properties"), simDate, simDate, null);

        contract = ContractFactory.getDummyContract("IF1601", "CFFEX");
        parser = ParserInstanceFactory.getBookParser(SbeVersion.PROT_DYNAMICLENGTH_BOOK, 0);
        encoder = EncoderInstanceFactory.getBookEncoder(SbeVersion.PROT_DYNAMICLENGTH_BOOK, 0);

        marketbook = new MarketBook(contract, parser, ContentType.Level2_Quote);
    }

    @Before
    public void setUp() {
        Mockito.when(fillFactory.get()).thenReturn(fill);

        exchangeTimeMicros = new Date().getTime() * 1000l;
        bidPrice = 95.0d;
        bidQty = 27;
        askPrice = 96.0d;
        askQty = 35;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        simOrderExecutor = new SimOrderExecutor(contract, feeCalculator, delayedQueue, ackFactory, fillFactory);
        simOrderExecutor.setFillPriceBanningTime(fillPriceBanningDelayMicros);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);

        simMarketbook = simOrderExecutor.getInternalBook();
        clOrdIdToOrder = simOrderExecutor.getInternalMap();
    }

    private void setMarketbook(final double bidPrice, final long bidQty, final double askPrice, final long askQty) {
        final String ascii = "101048000000,1579227048604506,1,1,IF1601,0,852,N,2,2,"
            + bidQty + "@" + bidPrice + ","
            + (bidQty * 2) + "@" + (bidPrice - 1) + ","
            + askQty + "@" + askPrice + ","
            + (askQty * 2) + "@" + (askPrice + 1);
        final String[] raw = ascii.split(",");

        encoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        encoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        encoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        encoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        encoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        encoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        encoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        encoder.setQuoteType(raw[MarketBookV3.QuoteType.getIndex()].getBytes()[0]);
        final int bidDepth = NumberUtils.toInt(raw[MarketBookV3.BidDepth.getIndex()]);
        encoder.setBidDepth(bidDepth);
        final int askDepth = NumberUtils.toInt(raw[MarketBookV3.AskDepth.getIndex()]);
        encoder.setAskDepth(askDepth);

        for (int i = 0; i < bidDepth; i++) {
            final String[] bid = raw[MarketBookV3.BookInfo.getIndex() + i].split("@");
            encoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
            encoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
        }
        for (int i = 0; i < askDepth; i++) {
            final String[] ask = raw[MarketBookV3.BookInfo.getIndex() + bidDepth + i].split("@");
            encoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
            encoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
        }
        final ByteBuffer data = encoder.getBuffer();

        marketbook.setData(data, exchangeTimeMicros);
    }

    @Test
    public void testOrderPriceEqualToBestPriceFullFill() {
        // place orders
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 95, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(bidQty, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));

        // order price = book best price, order qty < book best qty
        bidPrice = 92.0;
        bidQty = 17;
        askPrice = 95.0;
        askQty = 55;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        exchangeTimeMicros += fillPriceBanningDelayMicros / 4;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        simOrderExecutor.matchOrder(exchangeTimeMicros, marketbook);

        assertEquals(orderQty, fill.getFillQty());
        assertEquals(askQty - fill.getFillQty(), simMarketbook.getAvailableQty(simulatedOrder.getSide(), simulatedOrder.getPrice()));
        assertTrue(clOrdIdToOrder.isEmpty());
    }

    @Test
    public void testOrderPriceEqualToBestPricePartialFill() {
        // place orders
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 95, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(bidQty, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));

        // order price = book best price, order qty < book best qty
        bidPrice = 92.0;
        bidQty = 17;
        askPrice = 95.0;
        askQty = 45;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        exchangeTimeMicros += fillPriceBanningDelayMicros / 4;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        simOrderExecutor.matchOrder(exchangeTimeMicros, marketbook);

        assertEquals(askQty, fill.getFillQty());
        assertEquals(askQty - fill.getFillQty(), simMarketbook.getAvailableQty(simulatedOrder.getSide(), simulatedOrder.getPrice()));
        assertTrue(!clOrdIdToOrder.isEmpty());
    }

    @Test
    public void testConsequentialOrders() {
        // place orders - queue
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 95, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(bidQty, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));

        // order price > book best price, order qty > book best qty
        bidPrice = 92.0;
        bidQty = 17;
        askPrice = 94.0;
        askQty = 26;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        // full fill at order price, prevent another fill until marketbook getting reset after time
        exchangeTimeMicros += fillPriceBanningDelayMicros / 4;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        simOrderExecutor.matchOrder(exchangeTimeMicros, marketbook);

        assertEquals(orderQty, fill.getFillQty());
        assertEquals(95.0d, fill.getFillPrice(), 0.0);
        assertEquals(0, simMarketbook.getHittableQty(Side.BUY, 0));
        assertEquals(askQty * 2 - (orderQty - askQty), simMarketbook.getHittableQty(Side.BUY, 1));
        assertTrue(clOrdIdToOrder.isEmpty());

        // place another order at best offer
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 94, 20010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros + 10, simulatedOrder);
        assertEquals(0, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(20010011)));

        // order price = book best price, order qty > book best qty
        bidPrice = 92.0;
        bidQty = 17;
        askPrice = 94.0;
        askQty = 26;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        // won't match queued order
        exchangeTimeMicros += fillPriceBanningDelayMicros / 2;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        simOrderExecutor.matchOrder(exchangeTimeMicros, marketbook);

        assertEquals(orderQty, simulatedOrder.getQuantity());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(20010011)));

        // can match queued order
        exchangeTimeMicros += fillPriceBanningDelayMicros / 2;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        simOrderExecutor.matchOrder(exchangeTimeMicros, marketbook);

        assertEquals(askQty, fill.getFillQty());
        assertEquals(askPrice, fill.getFillPrice(), 0.0);
        assertEquals(0, simMarketbook.getHittableQty(Side.BUY, 0));
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(20010011)));
    }

}
