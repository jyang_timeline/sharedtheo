package com.timelinecapital.simulation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.Date;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.nogle.commons.utils.NogleTimeUnit;
import com.nogle.core.config.EngineMode;
import com.nogle.core.marketdata.producer.metadata.MarketBookV3;
import com.nogle.core.marketdata.producer.metadata.QuoteHeaderEXGInfo;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.timelinecapital.commons.channel.ContentType;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.ParserInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.marketdata.protocol.meta.SbeVersion;
import com.timelinecapital.commons.type.StockPriceCNDouble;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.trade.simulation.ExchangeEventQueue;
import com.timelinecapital.core.trade.simulation.ExchangeMarketBook;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeInternalMarketBook {

    @Mock
    Contract contract;
    @Mock
    FeeCalculator feeCalculator;
    @Mock
    ExchangeEventQueue delayedQueue;
    @Mock
    AckFactory ackFactory;
    @Mock
    FillFactory fillFactory;

    private final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(0.01d, 10.0d);
    private final MarketBookParser parser;
    private final MarketBookEncoder encoder;

    private MarketBook marketbook;
    private SimOrderExecutor simOrderExecutor;
    private ExchangeMarketBook simMarketbook;

    private final double bidP0 = 24.55d;
    private final long bidQ0 = 1000;
    private final double bidP1 = 24.53d;
    private final long bidQ1 = 8600;
    // private final double bidP2 = 24.52d;
    // private final long bidQ2 = 200;
    // private final double bidP3 = 24.51d;
    // private final long bidQ3 = 5800;
    // private final double bidP4 = 24.46d;
    // private final long bidQ4 = 2900;

    private final double askP0 = 24.58d;
    private final long askQ0 = 5700;
    private final double askP1 = 24.62d;
    private final long askQ1 = 100;
    // private final double askP2 = 24.69d;
    // private final long askQ2 = 500;
    // private final double askP3 = 24.70d;
    // private final long askQ3 = 1500;
    // private final double askP4 = 24.72d;
    // private final long askQ4 = 400;

    private final double[] bidPrice = new double[] { bidP0, bidP1, 24.52d, 24.51d, 24.46d };
    private final long[] bidQty = new long[] { bidQ0, bidQ1, 200, 5800, 2900 };

    private final double[] askPrice = new double[] { askP0, askP1, 24.69d, 24.70d, 24.72d };
    private final long[] askQty = new long[] { askQ0, askQ1, 500, 1500, 400 };

    private final double newBidP0 = 24.53d;
    private final long newBidQ0 = 7300;
    private final double newBidP1 = 24.52d;
    private final long newBidQ1 = 200;
    // private final double newBidP2 = 24.51d;
    // private final long newBidQ2 = 5800;
    // private final double newBidP3 = 24.46d;
    // private final long newBidQ3 = 2900;
    // private final double newBidP4 = 24.44d;
    // private final long newBidQ4 = 1000;

    private final double newAskP0 = 24.58d;
    private final long newAskQ0 = 1900;
    private final double newAskP1 = 24.69d;
    private final long newAskQ1 = 500;
    // private final double newAskP2 = 24.70d;
    // private final long newAskQ2 = 1500;
    // private final double newAskP3 = 24.72d;
    // private final long newAskQ3 = 400;
    // private final double newAskP4 = 24.73d;
    // private final long newAskQ4 = 1500;

    private final double nextBidPrice[] = new double[] { newBidP0, newBidP1, 24.51d, 24.46d, 24.44d };
    private final long nextBidQty[] = new long[] { newBidQ0, newBidQ1, 5800, 2900, 1000 };

    private final double nextAskPrice[] = new double[] { newAskP0, newAskP1, 24.70d, 24.72d, 24.73d };
    private final long nextAskQty[] = new long[] { newAskQ0, newAskQ1, 1500, 400, 1500 };

    private final long fillPriceBanningDelayMicros = 200;

    private long exchangeTimeMicros;

    public ExchangeInternalMarketBook() throws ConfigurationException {
        final String simDate = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));

        EngineConfig.buildConfig(0, new File("trading.properties"), EngineMode.PRODUCTION);
        SimulationConfig.buildConfig(new File("simulation.properties"), simDate, simDate, null);

        parser = ParserInstanceFactory.getBookParser(SbeVersion.PROT_DYNAMICLENGTH_BOOK, 0);
        encoder = EncoderInstanceFactory.getBookEncoder(SbeVersion.PROT_DYNAMICLENGTH_BOOK, 0);
    }

    @Before
    public void setUp() {
        Mockito.when(contract.getTickCalculator()).thenReturn(fixedTickCalculator);
        Mockito.when(contract.getExchange()).thenReturn("SSE");
        Mockito.when(contract.getSymbol()).thenReturn("600000");

        marketbook = new MarketBook(contract, parser, ContentType.Level2_Quote);

        exchangeTimeMicros = new Date().getTime() * 1000l;
        setMarketbook(bidPrice, bidQty, askPrice, askQty);

        simOrderExecutor = new SimOrderExecutor(contract, feeCalculator, delayedQueue, ackFactory, fillFactory);
        simOrderExecutor.setFillPriceBanningTime(fillPriceBanningDelayMicros);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);

        simMarketbook = simOrderExecutor.getInternalBook();
    }

    private void setMarketbook(final double[] bidPrice, final long[] bidQty, final double[] askPrice, final long[] askQty) {
        final StringBuilder sb = new StringBuilder().append("101048000000,1579227048604506,3,1,600000,0,852,N,5,5,");
        for (int i = 0; i < bidPrice.length; i++) {
            sb.append(bidQty[i]).append("@").append(bidPrice[i]).append(",");
        }
        for (int j = 0; j < askPrice.length; j++) {
            sb.append(askQty[j]).append("@").append(askPrice[j]);
            if (j < askPrice.length - 1) {
                sb.append(",");
            }
        }
        final String[] raw = sb.toString().split(",");

        encoder.setVersion(raw[QuoteHeaderEXGInfo.Version.getIndex()].getBytes()[0]);
        encoder.setType(raw[QuoteHeaderEXGInfo.Type.getIndex()].getBytes()[0]);
        encoder.setSymbol(raw[QuoteHeaderEXGInfo.Symbol.getIndex()]);
        encoder.setSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.SeqNo.getIndex()]));
        encoder.setExchangeSequenceNo(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_SeqNo.getIndex()]));
        encoder.setMicroTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Timestamp.getIndex()]));
        encoder.setExchangeTime(NumberUtils.toLong(raw[QuoteHeaderEXGInfo.Exchange_Timestamp.getIndex()]));

        encoder.setQuoteType(raw[MarketBookV3.QuoteType.getIndex()].getBytes()[0]);
        final int bidDepth = NumberUtils.toInt(raw[MarketBookV3.BidDepth.getIndex()]);
        encoder.setBidDepth(bidDepth);
        final int askDepth = NumberUtils.toInt(raw[MarketBookV3.AskDepth.getIndex()]);
        encoder.setAskDepth(askDepth);

        for (int i = 0; i < bidDepth; i++) {
            final String[] bid = raw[MarketBookV3.BookInfo.getIndex() + i].split("@");
            encoder.setBidQty(i, NumberUtils.toLong(bid[0], 0l));
            encoder.setBidPrice(i, NumberUtils.toDouble(bid[1], 0.0d));
        }
        for (int i = 0; i < askDepth; i++) {
            final String[] ask = raw[MarketBookV3.BookInfo.getIndex() + bidDepth + i].split("@");
            encoder.setAskQty(i, NumberUtils.toLong(ask[0], 0l));
            encoder.setAskPrice(i, NumberUtils.toDouble(ask[1], 0.0d));
        }
        final ByteBuffer data = encoder.getBuffer();

        marketbook.setData(data, exchangeTimeMicros);
    }

    @Test
    public void testBidBookMovingDown() {
        /*
         * MarketBook initialization
         */
        setMarketbook(bidPrice, bidQty, askPrice, askQty);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        exchangeTimeMicros += NogleTimeUnit.SECONDS.toMicros(3);

        /*
         * MarketBook moving down
         */
        setMarketbook(nextBidPrice, nextBidQty, nextAskPrice, nextAskQty);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);

        assertEquals(newBidP0, simMarketbook.getAtMarketPrice(Side.SELL, 0).asDouble(), 0);
        assertEquals(newBidQ0, simMarketbook.getHittableQty(Side.SELL, 0));
        assertEquals(newAskP1, simMarketbook.getAtMarketPrice(Side.BUY, 1).asDouble(), 0);
        assertEquals(newAskQ1, simMarketbook.getHittableQty(Side.BUY, 1));
    }

    @Test
    public void testBidBookMovingUp() {
        /*
         * MarketBook initialization
         */
        setMarketbook(nextBidPrice, nextBidQty, nextAskPrice, nextAskQty);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        exchangeTimeMicros += NogleTimeUnit.SECONDS.toMicros(3);

        /*
         * MarketBook moving up
         */
        setMarketbook(bidPrice, bidQty, askPrice, askQty);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);

        assertEquals(bidP0, simMarketbook.getAtMarketPrice(Side.SELL, 0).asDouble(), 0);
        assertEquals(bidQ0, simMarketbook.getHittableQty(Side.SELL, 0));
        assertEquals(askP1, simMarketbook.getAtMarketPrice(Side.BUY, 1).asDouble(), 0);
        assertEquals(askQ1, simMarketbook.getHittableQty(Side.BUY, 1));
    }

    @Test
    public void testBookWithFill() {
        /*
         * MarketBook initialization
         */
        setMarketbook(bidPrice, bidQty, askPrice, askQty);
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        exchangeTimeMicros += NogleTimeUnit.SECONDS.toMicros(3);

        final Price targetPrice = new StockPriceCNDouble();
        targetPrice.setDouble(24.58d);
        simMarketbook.onOrderMatching(exchangeTimeMicros, Side.SELL, targetPrice, 5000);

        System.out.println(simMarketbook.toString());
    }

}
