package com.timelinecapital.simulation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.trade.simulation.ExchangeEventQueue;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ExchangeOrderInsert.class })
public class ExchangeOrderInsert {
    @Mock
    Contract contract;
    @Mock
    FeeCalculator feeCalculator;
    @Mock
    MarketBook marketbook;
    @Mock
    MarketBookParser parser;
    @Mock
    TradeEvent tradeEvent;

    @Mock
    ExchangeEventQueue delayedQueue;
    @Mock
    AckFactory ackFactory;
    @Mock
    FillFactory fillFactory;

    private final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(1, 10.0);
    private final OrderFill fill = new OrderFill();

    private final long fillPriceBanningDelayMicros = 30;
    private long exchangeTimeMicros;

    private final double bidPrice = 95.0d;
    private final long bidQty = 27;
    private final double askPrice = 96.0d;
    private final long askQty = 35;

    private Map<Long, SimulatedOrder> clOrdIdToOrder;

    private SimOrderExecutor simOrderExecutor;
    private SimulatedOrder simulatedOrder;

    @Before
    public void setUp() {
        exchangeTimeMicros = new Date().getTime() * 1000l;

        Mockito.when(contract.getTickCalculator()).thenReturn(fixedTickCalculator);
        Mockito.when(fillFactory.get()).thenReturn(fill);

        Whitebox.setInternalState(marketbook, MarketBookParser.class, parser);
        Whitebox.setInternalState(marketbook, Contract.class, contract);
        Mockito.when(marketbook.getBidDepth()).thenReturn(2);
        Mockito.when(marketbook.getAskDepth()).thenReturn(2);

        Mockito.when(marketbook.getBidPrice()).thenReturn(bidPrice);
        Mockito.when(marketbook.getBidQty()).thenReturn(bidQty);
        Mockito.when(marketbook.getBidPrice(1)).thenReturn(bidPrice - 1);
        Mockito.when(marketbook.getBidQty(1)).thenReturn(bidQty * 2);

        Mockito.when(marketbook.getAskPrice()).thenReturn(askPrice);
        Mockito.when(marketbook.getAskQty()).thenReturn(askQty);
        Mockito.when(marketbook.getAskPrice(1)).thenReturn(askPrice + 1);
        Mockito.when(marketbook.getAskQty(1)).thenReturn(askQty * 2);

        simOrderExecutor = new SimOrderExecutor(contract, feeCalculator, delayedQueue, ackFactory, fillFactory);
        clOrdIdToOrder = Whitebox.getInternalState(simOrderExecutor, "clOrdIdToOrder");
    }

    @Test
    public void testOnNewOrder() {
        // same price: orderQty > bookQty
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 96, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(askQty, fill.getFillQty());
        assertEquals(orderQty - askQty, clOrdIdToOrder.get(Long.valueOf(10010011)).getQuantity());

        // same price: orderQty < bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        orderQty = 30;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 96, 20010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(orderQty, fill.getFillQty());
        assertTrue(!clOrdIdToOrder.containsKey(Long.valueOf(20010011)));

        // orderPrice > bookPrice, orderQty < bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        orderQty = 25;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 30010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(orderQty, fill.getFillQty());
        assertEquals(96.0d, fill.getFillPrice(), 0.0d);

        // orderPrice > bookPrice, orderQty > bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 40010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(0, simulatedOrder.getQuantity());

        // orderPrice > bookPrice, orderQty = bookQty(1) + bookQty(2)
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        orderQty = askQty * 3;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 50010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(0, simulatedOrder.getQuantity());

        // orderPrice < bookPrice, orderQty > bookQty(1) + bookQty(2)
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        orderQty = bidQty * 3 + 1;
        simulatedOrder = new SimulatedOrder(contract, Side.SELL, TimeCondition.GFD, orderQty, 94, 60010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(1, simulatedOrder.getQuantity());
    }

}
