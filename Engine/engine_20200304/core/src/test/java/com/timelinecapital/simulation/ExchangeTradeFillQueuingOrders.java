package com.timelinecapital.simulation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.types.FeeCalculator;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickType;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.marketdata.parser.TradeParser;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.execution.report.factory.AckFactory;
import com.timelinecapital.core.execution.report.factory.FillFactory;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.marketdata.type.Tick;
import com.timelinecapital.core.trade.simulation.ExchangeEventQueue;
import com.timelinecapital.core.trade.simulation.SimOrderExecutor;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ExchangeTradeFillQueuingOrders.class })
public class ExchangeTradeFillQueuingOrders {
    @Mock
    Contract contract;
    @Mock
    FeeCalculator feeCalculator;
    @Mock
    MarketBook marketbook;
    @Mock
    Tick trade;
    @Mock
    MarketBookParser bookParser;
    @Mock
    TradeParser tradeParser;
    @Mock
    TradeEvent tradeEvent;

    @Mock
    ExchangeEventQueue delayedQueue;
    @Mock
    AckFactory ackFactory;
    @Mock
    FillFactory fillFactory;

    private final FixedTickCalculator fixedTickCalculator = new FixedTickCalculator(1, 10.0);
    private final OrderFill fill = new OrderFill();

    // private final long fillPriceBanningDelayMicros = 100;
    private long exchangeTimeMicros;

    private final double bidPrice = 95.0d;
    private final long bidQty = 27;
    private final double askPrice = 96.0d;
    private final long askQty = 35;

    private final long tradeQty = 20;

    private Map<Long, SimulatedOrder> clOrdIdToOrder;

    private SimOrderExecutor simOrderExecutor;
    private SimulatedOrder simulatedOrder;

    @Before
    public void setUp() {
        exchangeTimeMicros = new Date().getTime() * 1000l;

        Mockito.when(contract.getTickCalculator()).thenReturn(fixedTickCalculator);
        Mockito.when(fillFactory.get()).thenReturn(fill);

        Whitebox.setInternalState(marketbook, MarketBookParser.class, bookParser);
        Whitebox.setInternalState(marketbook, Contract.class, contract);
        Whitebox.setInternalState(trade, TradeParser.class, tradeParser);
        Whitebox.setInternalState(trade, Contract.class, contract);
        Mockito.when(marketbook.getBidDepth()).thenReturn(2);
        Mockito.when(marketbook.getAskDepth()).thenReturn(2);
        setMarketbook();

        simOrderExecutor = new SimOrderExecutor(contract, feeCalculator, delayedQueue, ackFactory, fillFactory);
        clOrdIdToOrder = Whitebox.getInternalState(simOrderExecutor, "clOrdIdToOrder");
    }

    private void setMarketbook() {
        Mockito.when(marketbook.getBidPrice()).thenReturn(bidPrice);
        Mockito.when(marketbook.getBidQty()).thenReturn(bidQty);
        Mockito.when(marketbook.getBidPrice(1)).thenReturn(bidPrice - 1);
        Mockito.when(marketbook.getBidQty(1)).thenReturn(bidQty * 2);

        Mockito.when(marketbook.getAskPrice()).thenReturn(askPrice);
        Mockito.when(marketbook.getAskQty()).thenReturn(askQty);
        Mockito.when(marketbook.getAskPrice(1)).thenReturn(askPrice + 1);
        Mockito.when(marketbook.getAskQty(1)).thenReturn(askQty * 2);
    }

    private void setTrade(final TickType type, final double price, final long qty) {
        Mockito.when(tradeParser.getTradeType()).thenReturn(type.getCode());
        Mockito.when(trade.getPrice()).thenReturn(price);
        Mockito.when(trade.getQuantity()).thenReturn(qty);
    }

    @Test
    public void testTradeToUpdateQueue() {
        // place orders
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 95, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(bidQty, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));

        setTrade(TickType.SELLER, bidPrice, tradeQty);

        // trade not matchable
        exchangeTimeMicros += 50;
        simOrderExecutor.matchOnTrade(exchangeTimeMicros, trade);
        assertEquals(bidQty - tradeQty, simulatedOrder.getPriority());

        // trade is matchable - partial
        exchangeTimeMicros += 50;
        final long lastPriority = simulatedOrder.getPriority();
        simOrderExecutor.matchOnTrade(exchangeTimeMicros, trade);
        assertEquals(0, simulatedOrder.getPriority());
        assertEquals(tradeQty - lastPriority, fill.getFillQty());
    }

    @Test
    public void testTradeToFillOrder() {
        // place orders
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.SELL, TimeCondition.GFD, orderQty, 96, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, simulatedOrder);
        assertEquals(askQty, simulatedOrder.getPriority());
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));

        setTrade(TickType.BUYER, askPrice, tradeQty * 5);

        exchangeTimeMicros += 50;
        simOrderExecutor.matchOnTrade(exchangeTimeMicros, trade);
        assertEquals(orderQty, fill.getFillQty());
        assertTrue(!clOrdIdToOrder.containsKey(Long.valueOf(10010011)));
    }

    @Test
    public void testTradeFillMultipleOrders() {
        // place orders
        simOrderExecutor.onMarketBook(exchangeTimeMicros, marketbook);
        final long orderQty = 10;
        final long tradeQty = 55;

        final SimulatedOrder enqueueOrder = new SimulatedOrder(contract, Side.SELL, TimeCondition.GFD, orderQty, 97, 10010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros, enqueueOrder);
        enqueueOrder.setPosition(5, exchangeTimeMicros);
        final SimulatedOrder bestOrder = new SimulatedOrder(contract, Side.SELL, TimeCondition.GFD, orderQty, 96, 20010011, tradeEvent);
        simOrderExecutor.onOrderInsertAndTryMatching(exchangeTimeMicros + 10, bestOrder);

        assertNotNull(clOrdIdToOrder.get(Long.valueOf(10010011)));
        assertNotNull(clOrdIdToOrder.get(Long.valueOf(20010011)));

        setTrade(TickType.BUYER, askPrice + 1, tradeQty);
        exchangeTimeMicros += 50;
        simOrderExecutor.matchOnTrade(exchangeTimeMicros, trade);
        assertTrue(!clOrdIdToOrder.containsKey(Long.valueOf(20010011)));
        assertEquals(0, enqueueOrder.getPriority());
        assertEquals(tradeQty - askQty - orderQty - fill.getFillQty(), enqueueOrder.getQuantity());
    }

}
