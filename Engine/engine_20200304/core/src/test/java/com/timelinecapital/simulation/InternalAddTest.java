package com.timelinecapital.simulation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.types.FixedTickCalculator;
import com.nogle.core.types.SimulatedOrder;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.marketdata.parser.MarketBookParser;
import com.timelinecapital.commons.type.StockPriceCNDouble;
import com.timelinecapital.commons.type.Price;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.report.Ack;
import com.timelinecapital.core.execution.report.OrderFill;
import com.timelinecapital.core.marketdata.type.MarketBook;
import com.timelinecapital.core.trade.simulation.ExecutorMarketBookWithDoublePrice;
import com.timelinecapital.core.trade.simulation.SimMatchingUtils;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ InternalAddTest.class, EngineConfig.class })
public class InternalAddTest {
    private static final Logger log = LogManager.getLogger(InternalAddTest.class);

    @Mock
    Contract contract;
    @Mock
    MarketBook marketbook;
    @Mock
    MarketBook nextMarketbook;
    @Mock
    MarketBookParser parser;
    @Mock
    TradeEvent tradeEvent;

    private final Queue<SimulatedOrder> queuedSimOrders = new PriorityQueue<>();
    private final Map<Long, SimulatedOrder> clOrdIdToOrder = new HashMap<>();

    private long fillPriceResetMicros;
    private final long fillPriceBanningDelayMicros = 30000;

    private long exchangeTimeMicros;

    private OrderFill fill;
    private ExecutorMarketBookWithDoublePrice simMarketBook;

    private final double bidPrice = 95.0d;
    private final long bidQty = 27;
    private final double askPrice = 96.0d;
    private final long askQty = 35;

    private SimulatedOrder simulatedOrder;
    private FixedTickCalculator fixedTickCalculator;

    @Before
    public void setUp() {
        fixedTickCalculator = new FixedTickCalculator(1, 10.0);
        simMarketBook = new ExecutorMarketBookWithDoublePrice(contract);

        Mockito.when(contract.getTickCalculator()).thenReturn(fixedTickCalculator);

        Whitebox.setInternalState(marketbook, MarketBookParser.class, parser);
        Whitebox.setInternalState(marketbook, Contract.class, contract);
        Mockito.when(marketbook.getBidDepth()).thenReturn(2);
        Mockito.when(marketbook.getAskDepth()).thenReturn(2);

        Mockito.when(marketbook.getBidPrice()).thenReturn(bidPrice);
        Mockito.when(marketbook.getBidQty()).thenReturn(bidQty);
        Mockito.when(marketbook.getBidPrice(1)).thenReturn(bidPrice - 1);
        Mockito.when(marketbook.getBidQty(1)).thenReturn(bidQty * 2);

        Mockito.when(marketbook.getAskPrice()).thenReturn(askPrice);
        Mockito.when(marketbook.getAskQty()).thenReturn(askQty);
        Mockito.when(marketbook.getAskPrice(1)).thenReturn(askPrice + 1);
        Mockito.when(marketbook.getAskQty(1)).thenReturn(askQty * 2);

        exchangeTimeMicros = new Date().getTime() * 1000l;
    }

    @Test
    public void testOnNewOrder() {
        // same price: orderQty > bookQty
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.BUY);
        long orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 96, 10010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(askQty, fill.getFillQty());
        assertEquals(orderQty - askQty, clOrdIdToOrder.get(Long.valueOf(10010011)).getQuantity());

        // same price: orderQty < bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.BUY);
        orderQty = 30;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 96, 20010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(orderQty, fill.getFillQty());
        assertTrue(!clOrdIdToOrder.containsKey(Long.valueOf(20010011)));

        // orderPrice > bookPrice, orderQty < bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.BUY);
        orderQty = 25;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 30010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(orderQty, fill.getFillQty());
        assertEquals(96.0d, fill.getFillPrice(), 0.0d);

        // orderPrice > bookPrice, orderQty > bookQty
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.BUY);
        orderQty = 50;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 40010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(0, simulatedOrder.getQuantity());

        // orderPrice > bookPrice, orderQty = bookQty(1) + bookQty(2)
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.BUY);
        orderQty = askQty * 3;
        simulatedOrder = new SimulatedOrder(contract, Side.BUY, TimeCondition.GFD, orderQty, 97, 50010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(0, simulatedOrder.getQuantity());

        // orderPrice < bookPrice, orderQty > bookQty(1) + bookQty(2)
        exchangeTimeMicros += fillPriceBanningDelayMicros + 20;
        simMarketBook.updateFrom(exchangeTimeMicros, marketbook);
        // hittableQty = simMarketBook.getHittableQty(Side.SELL);
        orderQty = bidQty * 3 + 1;
        simulatedOrder = new SimulatedOrder(contract, Side.SELL, TimeCondition.GFD, orderQty, 94, 60010011, tradeEvent);
        internalAdd(simulatedOrder);
        assertEquals(1, simulatedOrder.getQuantity());
    }

    void testUpdateMarketBook() {
        Whitebox.setInternalState(nextMarketbook, MarketBookParser.class, parser);
        Whitebox.setInternalState(nextMarketbook, Contract.class, contract);
        Mockito.when(nextMarketbook.getBidDepth()).thenReturn(3);
        Mockito.when(nextMarketbook.getAskDepth()).thenReturn(3);

        Mockito.when(nextMarketbook.getBidPrice()).thenReturn(bidPrice + 1);
        Mockito.when(nextMarketbook.getBidQty()).thenReturn(bidQty);
        Mockito.when(nextMarketbook.getBidPrice(1)).thenReturn(bidPrice);
        Mockito.when(nextMarketbook.getBidQty(1)).thenReturn(bidQty + 2);
        Mockito.when(nextMarketbook.getBidPrice(2)).thenReturn(bidPrice - 1);
        Mockito.when(nextMarketbook.getBidQty(2)).thenReturn(bidQty + 12);

        Mockito.when(nextMarketbook.getAskPrice()).thenReturn(askPrice + 1);
        Mockito.when(nextMarketbook.getAskQty()).thenReturn(askQty);
        Mockito.when(nextMarketbook.getAskPrice(1)).thenReturn(askPrice + 2);
        Mockito.when(nextMarketbook.getAskQty(1)).thenReturn(askQty + 7);
        Mockito.when(nextMarketbook.getAskPrice(2)).thenReturn(askPrice + 3);
        Mockito.when(nextMarketbook.getAskQty(2)).thenReturn(askQty - 7);
        simMarketBook.updateFrom(exchangeTimeMicros, nextMarketbook);
    }

    void internalAdd(final SimulatedOrder simulatedOrder) {
        final Side side = simulatedOrder.getSide();
        final double restingPrice = simulatedOrder.getPrice().asDouble();
        final double hittablePrice = side.switchSide().getPrice(marketbook);

        if (PriceUtils.isInsideOfOrEqualTo(side, restingPrice, hittablePrice)) {
            onMatchablePrice(simulatedOrder);
        }
        if (simulatedOrder.getQuantity() > 0) {
            handleRemaining(simulatedOrder);
        }
    }

    private void toFill(final SimulatedOrder order, final double price, final long qty, final long remainingQty) {
        // final double commission = getSimulatedCommission(qty, price);
        final double commission = 0.0d;
        // final OrderFill fill = fillFactory.get();
        fill = new OrderFill();
        fill.setFill(order.getContract(), order.getSide(), order.getTimeCondition(), order.getClOrdId(), qty, price, commission);
        fill.setRemainingQty(remainingQty);
        switch (order.getTimeCondition()) {
            case GFS:
            case GFD:
                // delayedQueue.queueDelayedFill(arrivalTimeMicros, fill);
                break;
            case IOC:
                // delayedQueue.queueDelayedIOCFill(arrivalTimeMicros, fill);
            default:
                break;
        }
        log.debug("Fill: {} {} {} by internalAdd {} {}@{}/{}@{}", price, qty, order.getSide(), marketbook.getContract(),
            marketbook.getBidQty(), marketbook.getBidPrice(), marketbook.getAskQty(), marketbook.getAskPrice());
    }

    private void toQueue(final SimulatedOrder order, final long position) {
        order.setPosition(position, exchangeTimeMicros);
        clOrdIdToOrder.put(order.getClOrdId(), order);
        queuedSimOrders.add(order);
        log.debug("Queued: {} {} {} #{}", order.getContract(), order.getQuantity(), order.getPrice(), order.getPriority());
    }

    private void toMiss(final SimulatedOrder order) {
        // final Ack ack = ackFactory.get();
        final Ack ack = new Ack();
        ack.setAck(order.getContract(), order.getSide(), order.getTimeCondition(), ExecType.NEWORDER_ACK, order.getClOrdId());
        // delayedQueue.queueDelayedMissedAck(arrivalTimeMicros, ack);
        log.debug("Missed: {} {} {} by internalAdd {} {}@{}/{}@{}", order.getPrice(), order.getQuantity(), order.getSide(),
            marketbook.getContract(), marketbook.getBidQty(), marketbook.getBidPrice(), marketbook.getAskQty(), marketbook.getAskPrice());
    }

    void onMatchablePrice(final SimulatedOrder order) {
        final Side side = order.getSide();
        int bookLevel = 0;

        long bestHittableQty = simMarketBook.getHittableQty(side);
        final Price bestHittablePrice = new StockPriceCNDouble();
        bestHittablePrice.setDouble(order.getSide().switchSide().getPrice(marketbook)); // current book - price

        do {
            final Price fillPrice = PriceUtils.getOutsidePrice(order.getSide(), order.getPrice(), bestHittablePrice);
            final long fillQty = SimMatchingUtils.getFillableQty(order.getQuantity(), bestHittableQty);
            final long remainingQty = SimMatchingUtils.getRemainingQty(order.getQuantity(), bestHittableQty);

            order.fill(fillQty);
            toFill(order, fillPrice.asDouble(), fillQty, remainingQty);

            final long arrivalTimeMicros = exchangeTimeMicros + 10;
            fillPriceResetMicros = arrivalTimeMicros + fillPriceBanningDelayMicros;

            simMarketBook.onOrderMatching(fillPriceResetMicros, order.getSide().switchSide(), fillPrice, fillQty);
            if (simMarketBook.getHittableQty(side) <= 0) {
                // bestHittablePrice = order.getSide().switchSide().getPrice(marketbook, ++bookLevel);
                bestHittablePrice.setDouble(order.getSide().switchSide().getPrice(marketbook, ++bookLevel));
                bestHittableQty = simMarketBook.getHittableQty(order.getSide(), bookLevel);
            }
        } while (order.getQuantity() > 0 && bestHittableQty != 0 && PriceUtils.isInsideOfOrEqualTo(order.getSide(), order.getPrice(), bestHittablePrice));
    }

    private void handleRemaining(final SimulatedOrder order) {
        switch (order.getTimeCondition()) {
            case GFS:
            case GFD:
                toQueue(order, order.getSide().getQty(marketbook, order.getPrice().asDouble()));
                break;
            case IOC:
                toMiss(order);
                break;
            default:
                break;
        }
    }

}
