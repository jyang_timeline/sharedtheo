package com.timelinecapital.tool.test;

import java.util.UUID;

import com.nogle.commons.format.TagValueDecoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.Message;
import com.nogle.messaging.MessageConsumer;

public class CommandConsumer implements MessageConsumer {
    // private static final Logger log = LoggerFactory.getLogger(CommandConsumer.class);

    private static CommandConsumer instance;

    private final TagValueDecoder decoder = new TagValueDecoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);
    // private final Map<Long, CommandResponse> responses = new ConcurrentHashMap<>();

    // @SuppressWarnings("unused")
    // private StrategyMonitor strategyMonitor;
    private UUID uuid;

    public static CommandConsumer getInstance() {
        if (instance == null) {
            synchronized (CommandConsumer.class) {
                if (instance == null) {
                    instance = new CommandConsumer();
                }
            }
        }
        return instance;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void onMessage(final Message<?> message) {
        if (message instanceof BinaryMessage) {
            final String payload = new String(((BinaryMessage) message).getPayload());
            // log.debug("***[{}]*** {} << Response: {} ", uuid.getMostSignificantBits(), message.getHeader().getTopic(),
            // payload);

            decoder.decode(payload);
            System.out.println(decoder.get(TradeEngineCommunication.messageTag));
            // final CommandResponse commandResponse = new CommandResponse();
            // if (TradeEngineCommunication.msgTypeFailed.equals(decoder.get(TradeEngineCommunication.msgTypeTag))) {
            // commandResponse.setSuccess(false);
            // } else {
            // commandResponse.setSuccess(true);
            // }
            // commandResponse.setMsg(decoder.get(TradeEngineCommunication.messageTag));
            // responses.put(uuid.getMostSignificantBits(), commandResponse);

            synchronized (this.uuid) {
                uuid.notify();
            }
        } else {
            // log.error("Unknown message: {} ", message);
        }
    }

    @Override
    public void onError(final Message<?> message) {
        // log.error("***[{}]*** {} << ERROR: {}", uuid.getMostSignificantBits(), message.getHeader().getTopic(),
        // message.getPayload());
    }

    // public Map<Long, CommandResponse> getResponses() {
    // return responses;
    // }

    // public CommandResponse getAndRemoveResponses(final Long uuid) {
    // return responses.remove(uuid);
    // }
}
