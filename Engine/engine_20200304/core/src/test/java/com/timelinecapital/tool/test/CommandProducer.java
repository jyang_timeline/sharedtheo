package com.timelinecapital.tool.test;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nogle.commons.command.DumpSystemStatus;
import com.nogle.commons.command.QuerySymbolInformation;
import com.nogle.commons.format.TagValueEncoder;
import com.nogle.commons.protocol.TradeEngineCommunication;
import com.nogle.commons.utils.SymbolInfoRefreshForm;
import com.nogle.messaging.BinaryMessage;
import com.nogle.messaging.MessageHeader;
import com.nogle.messaging.Messenger;
import com.timelinecapital.commons.commands.FeedEventTailer;
import com.timelinecapital.commons.commands.FeedEventTailer.ActionLevel;
import com.timelinecapital.commons.commands.engine.RefreshContract;
import com.timelinecapital.commons.commands.engine.RefreshPosition;
import com.timelinecapital.commons.commands.engine.RefreshPosition.Action;

public class CommandProducer {
    private static final Logger log = LogManager.getLogger(CommandProducer.class);

    static final TagValueEncoder encoder = new TagValueEncoder(TradeEngineCommunication.tagValueDelimiter, TradeEngineCommunication.fieldDelimiter);

    static final String TARGET_ENGINE_ID = "TE6";
    static final String PS_REQUEST_HEADER = new StringBuilder().append("PS").append("/Batch/Request").toString();
    static final String RESPONSE_HEADER = new StringBuilder().append(TARGET_ENGINE_ID).append("/Command/Reply").toString();

    static UUID uuid;

    private static void receive() {
        final CommandConsumer commandConsumer = CommandConsumer.getInstance();
        commandConsumer.setUuid(uuid);
        Messenger.subscribe(RESPONSE_HEADER, commandConsumer);
    }

    private static void send(final BinaryMessage msg) {
        try {
            Messenger.send(msg);
            synchronized (uuid) {
                try {
                    uuid.wait(10000);
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    static String generateEngineCommand(final String command, final String parameters) {
        encoder.clear();
        encoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeRequest);
        encoder.append(TradeEngineCommunication.commandTag, command);
        if (parameters != null) {
            encoder.append(TradeEngineCommunication.parameterTag, parameters);
        }
        return encoder.compose();
    }

    static String prepareEngineRequest(final String command, final String parameters, final String uuid) {
        encoder.clear();
        encoder.append(TradeEngineCommunication.msgTypeTag, TradeEngineCommunication.msgTypeRequest);
        encoder.append(TradeEngineCommunication.commandTag, command);
        encoder.append(TradeEngineCommunication.parameterTag, parameters);
        encoder.append(TradeEngineCommunication.requestIDTag, uuid);
        return encoder.compose();
    }

    static final String TOPIC = "/monitor/engine/command/";
    static final short CREATE_CODE = 1;
    static final short START_CODE = 2;
    static final short STOP_CODE = 3;
    static final short RESTART_CODE = 6;
    static ByteBuffer byteBuffer = ByteBuffer.allocate(1024).order(ByteOrder.nativeOrder());

    static void testCom(final String hostname, final short code, final int engineId, final String targetAccount, final String arguments) throws IOException {
        final MessageHeader header = new MessageHeader(TOPIC + hostname);
        ((Buffer) byteBuffer).clear();
        byteBuffer.putShort(code);
        byteBuffer.putInt(engineId);
        byteBuffer.put(composeArgs(targetAccount, arguments).getBytes());
        ((Buffer) byteBuffer).flip();
        final byte[] content = new byte[byteBuffer.limit() - byteBuffer.position()];
        byteBuffer.get(content, byteBuffer.position(), byteBuffer.limit() - byteBuffer.position());
        final BinaryMessage msg = new BinaryMessage(header, content);
        Messenger.send(msg);
    }

    private static String composeArgs(final String... arguments) {
        final StringBuilder sb = new StringBuilder();
        for (final String argument : arguments) {
            sb.append(argument).append(" ");
        }
        return sb.toString();
    }

    public static void main(final String[] args) throws InterruptedException {
        uuid = UUID.randomUUID();
        receive();

        MessageHeader commandHeader;

        try {
            testCom("web-dev01", START_CODE, 102, "ycpb1_sz", null);
        } catch (final IOException e1) {
            e1.printStackTrace();
        }

        // MessageHeader header = new MessageHeader(TE_REQUEST_HEADER, uuid, new Date().getTime());
        // final BinaryMessage msg1 = new BinaryMessage(header, generateEngineCommand(RefreshContract.name, "").getBytes());
        // send(msg1);

        final MessageHeader header = new MessageHeader(PS_REQUEST_HEADER, uuid, new Date().getTime());

        try {
            final Collection<SymbolInfoRefreshForm> forms = new ArrayList<>();
            forms.add(new SymbolInfoRefreshForm("IF2001", "CFFEX", "20191226"));
            forms.add(new SymbolInfoRefreshForm("cu2001", "SHFE", "20191226"));
            forms.add(new SymbolInfoRefreshForm("jm2001", "DCE", "20191226"));
            forms.add(new SymbolInfoRefreshForm("j2001", "DCE", "20191226"));
            forms.add(new SymbolInfoRefreshForm("002831", "SZSE", "20191226"));
            log.info("[TEST] Try to execute command {}", QuerySymbolInformation.name);
            final String parameter = QuerySymbolInformation.composeParameter(forms);
            final BinaryMessage msg =
                new BinaryMessage(header, prepareEngineRequest(QuerySymbolInformation.name, parameter, uuid.toString()).getBytes());
            // send(msg);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
        }

        commandHeader = new MessageHeader(TARGET_ENGINE_ID + "/Command/Request");
        try {
            log.info("[TEST] Try to execute command {} {}", RefreshContract.name, RefreshContract.ActionLevel.INSTANCE_PRICELIMIT_ONLY.name());
            final String param = "{\"strategyId\":\"*\",\"Level\":\"" + RefreshContract.ActionLevel.INSTANCE_PRICELIMIT_ONLY.name() + "\"}";
            final BinaryMessage msg =
                new BinaryMessage(commandHeader, prepareEngineRequest(RefreshContract.name, param, uuid.toString()).getBytes());
            // send(msg);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        commandHeader = new MessageHeader(TARGET_ENGINE_ID + "/Command/Request");
        try {
            log.info("[TEST] Try to execute command {} {}", DumpSystemStatus.name, DumpSystemStatus.parameterStrategy);
            final BinaryMessage msgStgy =
                new BinaryMessage(commandHeader, prepareEngineRequest(DumpSystemStatus.name, DumpSystemStatus.parameterStrategy, uuid.toString()).getBytes());
            final BinaryMessage msgStgyStatus =
                new BinaryMessage(commandHeader, prepareEngineRequest(DumpSystemStatus.name, DumpSystemStatus.parameterStrategyStatus, uuid.toString()).getBytes());
            final BinaryMessage msgEngStatus =
                new BinaryMessage(commandHeader, prepareEngineRequest(DumpSystemStatus.name, DumpSystemStatus.parameterEngineStatus, uuid.toString()).getBytes());
            // send(msg);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        commandHeader = new MessageHeader(TARGET_ENGINE_ID + "/Command/Request");
        try {
            log.info("[TEST] Try to execute command {} {}", FeedEventTailer.name, ActionLevel.INSTANCE_THEO);
            final String param = FeedEventTailer.composeParameter(ActionLevel.INSTANCE_THEO, "1000001", 10);
            final BinaryMessage msg =
                new BinaryMessage(commandHeader, prepareEngineRequest(FeedEventTailer.name, param, uuid.toString()).getBytes());
            // send(msg);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        commandHeader = new MessageHeader(TARGET_ENGINE_ID + "/Command/Request");
        try {
            log.info("[TEST] Try to execute command {} {}", RefreshPosition.name, Action.SYNC);
            final String param = RefreshPosition.composeParameter(Action.SYNC, "*");
            final BinaryMessage msg =
                new BinaryMessage(commandHeader, prepareEngineRequest(RefreshPosition.name, param, uuid.toString()).getBytes());
            send(msg);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // final BinaryMessage msg = new BinaryMessage(header, parseEngineCommand("riskmanagementconfig",
        // "{\"maxRejectsPerInterval\": 7}").getBytes());

        // final BinaryMessage msg = new BinaryMessage(header, generateEngineCommand("riskmanagementconfig",
        // "{\"maxRejectsPerInterval\": 1}").getBytes());
        // Thread.sleep(2000);

        // log.debug("***[{}]*** To Engine#{} >> {} : {} ", uuid.getMostSignificantBits(), engineId, REQUEST_HEADER, command);

        // final BinaryMessage msg2 = new BinaryMessage(header, generateEngineCommand("tepause", "{\"engineId\":
        // 6}").getBytes());
        // Thread.sleep(2000);
        // send(msg2);
    }

}
