package com.timelinecapital.tool.test;

import java.util.Random;

import com.timelinecapital.commons.util.FastDivision;
import com.timelinecapital.commons.util.FastDivision.Magic;

public class DivisionTest {

    static void reduceArrayFastPre(final long[] data, final Magic denominator) {
        for (int i = 0; i < data.length; ++i) {
            data[i] = FastDivision.divideSignedFast(data[i], denominator);
        }
    }

    static void reduceArrayFast(final long[] data, final long denominator) {
        final FastDivision.Magic magic = FastDivision.magicSigned(denominator);
        for (int i = 0; i < data.length; ++i) {
            data[i] = FastDivision.divideSignedFast(data[i], magic);
        }
    }

    static void reduceArraySlow(final long[] data, final long denominator) {
        for (int i = 0; i < data.length; ++i) {
            data[i] = data[i] / denominator;
        }
    }

    static void test(final long[] data, final long add) {
        for (int i = 0; i < data.length; ++i) {
            data[i] = data[i] / add;
        }
    }

    public static void main(final String[] args) {
        final Random rnd = new Random(777);
        final long[] data = new long[1000];
        for (int i = 0; i < data.length; i++) {
            data[i] = rnd.nextLong();
        }

        final int nIterations = 1000000;
        final long denominator = 10000000000l;

        long consumed = 0l;
        for (int att = 0; att < nIterations; att++) {
            final long[] slow = data.clone();
            final long start2 = System.nanoTime();
            for (int i = 0; i < slow.length; ++i) {
                slow[i] = slow[i] / denominator;
            }
            final long slowTime = System.nanoTime() - start2;

            if (att > nIterations - 1000) {
                consumed += slowTime;
            }
        }
        System.out.println("average forloop: " + consumed / 100);

        System.gc();

        consumed = 0l;
        for (int att = 0; att < nIterations; att++) {
            final long[] fast = data.clone();
            final long start = System.nanoTime();
            test(fast, denominator);
            final long fastTime = System.nanoTime() - start;

            if (att > nIterations - 1000) {
                consumed += fastTime;
            }
        }
        System.out.println("average method: " + consumed / 100);

        System.gc();

        consumed = 0l;
        for (int att = 0; att < nIterations; att++) {

            final long[] slow = data.clone();
            final long start2 = System.nanoTime();
            reduceArraySlow(slow, denominator);
            final long slowTime = System.nanoTime() - start2;

            if (att > nIterations - 1000) {
                consumed += slowTime;
            }
        }
        System.out.println("average slow: " + consumed / 100);

        System.gc();

        consumed = 0l;
        for (int att = 0; att < nIterations; att++) {
            final long[] fast = data.clone();
            final long start = System.nanoTime();
            reduceArrayFast(fast, denominator);
            final long fastTime = System.nanoTime() - start;

            if (att > nIterations - 1000) {
                consumed += fastTime;
            }
        }
        System.out.println("average fast: " + consumed / 100);
    }

}
