package com.timelinecapital.tool.test;

import java.util.Queue;

import com.nogle.core.types.SimulatedOrder;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.trade.simulation.ExecutorMarketBookWithDoublePrice;

public class InheritanceTest {
    ExecutorMarketBookWithDoublePrice mb = new ExecutorMarketBookWithDoublePrice(ContractFactory.getDummyContract("AA", "SHFE"));

    private long v1;
    private demo instance;

    public InheritanceTest() {
        v1 = 0;
        instance = new Child1(null);

    }

    void set(final long value) {
        v1 = value;
    }

    long get() {
        return instance.check();
    }

    void change() {
        instance = new Child2(null);
    }

    interface demo {

        long check();

    }

    class Child2 implements demo {

        public Child2(final Queue<SimulatedOrder> queue) {

        }

        @Override
        public long check() {
            return v1;
        }

    }

    public class Child1 implements demo {

        public Child1(final Queue<SimulatedOrder> queue) {

        }

        @Override
        public long check() {
            return v1;
        }

    }

    public static void main(final String[] args) {
        final InheritanceTest t = new InheritanceTest();

        System.out.println(t.get());
        t.set(1000);
        System.out.println(t.get());
        t.change();
        System.out.println(t.get());
        t.set(500);
        System.out.println(t.get());
    }

}
