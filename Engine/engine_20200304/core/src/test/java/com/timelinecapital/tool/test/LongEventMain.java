package com.timelinecapital.tool.test;

import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ExceptionHandlerSetting;
import com.lmax.disruptor.dsl.ProducerType;
import com.nogle.core.event.IdleTradeEvent;
import com.nogle.core.event.TradeEvent;
import com.timelinecapital.core.util.TaskThreadBuilder;

public class LongEventMain {
    private static final Logger log = LogManager.getLogger(LongEventMain.class);

    public static class LongEvent {
        private long value;

        public final static EventFactory<LongEvent> EVENT_FACTORY = () -> new LongEvent();

        public void set(final long value) {
            this.value = value;
        }
    }

    public class LongEventFactory implements EventFactory<LongEvent> {
        @Override
        public LongEvent newInstance() {
            return new LongEvent();
        }
    }

    public class LongEventProducer {
        private final RingBuffer<LongEvent> ringBuffer;

        public LongEventProducer(final RingBuffer<LongEvent> ringBuffer) {
            this.ringBuffer = ringBuffer;
        }

        public void onData(final ByteBuffer bb) {
            final long sequence = ringBuffer.next(); // Grab the next sequence
            try {
                final LongEvent event = ringBuffer.get(sequence); // Get the entry in the Disruptor
                // for the sequence
                event.set(bb.getLong(0)); // Fill with data
            } finally {
                ringBuffer.publish(sequence);
            }
        }
    }

    public static class LongEventHandler implements EventHandler<LongEvent> {
        @Override
        public void onEvent(final LongEvent event, final long sequence, final boolean endOfBatch) throws Exception {
            log.info("{}", event);
            // System.out.println("Event: " + event);
            // throw new Exception("Test123");
        }
    }

    private static final EventTranslatorOneArg<LongEvent, ByteBuffer> TRANSLATOR =
        new EventTranslatorOneArg<LongEvent, ByteBuffer>() {

            @Override
            public void translateTo(final LongEvent event, final long sequence, final ByteBuffer bb) {
                event.set(bb.getLong(0));
                log.info("Translate {}", event);
            }
        };

    @SuppressWarnings("unchecked")
    public static void main(final String[] args) throws Exception {
        // Executor that will be used to construct new threads for consumers
        // final Executor executor = Executors.newCachedThreadPool();

        // Specify the size of the ring buffer, must be power of 2.
        final int bufferSize = 1024;

        final ConcurrentHashMap<Integer, TradeEvent> test = new ConcurrentHashMap<>();
        test.put(100, IdleTradeEvent.getInstance());

        final WaitStrategy waitStrategy = new BusySpinWaitStrategy();

        // final ThreadFactory threadFactory = DaemonThreadFactory.INSTANCE;
        // final ThreadFactory threadFactory = TaskThreadFactory.INSTANCE;
        final ThreadFactory threadFactoryFirst = new TaskThreadBuilder().setDaemon(false).setNamePrefix("Test", 1).setPriority(Thread.NORM_PRIORITY).build();
        final ThreadFactory threadFactorySecond = new TaskThreadBuilder().setDaemon(false).setNamePrefix("Test", 2).setPriority(Thread.NORM_PRIORITY).build();

        final Disruptor<LongEvent> disruptorFirst = new Disruptor<LongEvent>(
            LongEvent.EVENT_FACTORY,
            16,
            threadFactoryFirst,
            ProducerType.MULTI,
            waitStrategy);

        final Disruptor<LongEvent> disruptorSecond = new Disruptor<LongEvent>(
            LongEvent.EVENT_FACTORY,
            16,
            threadFactorySecond,
            ProducerType.MULTI,
            waitStrategy);

        // Construct the Disruptor
        // final Disruptor<LongEvent> disruptor = new Disruptor<>(LongEvent::new, bufferSize, executor);

        // Connect the handler using lambda
        // disruptor.handleEventsWith((event, sequence, endOfBatch) -> System.out.println("Event: " + event + " " + sequence + "
        // " + endOfBatch));

        // Connect the handler using class
        final LongEventHandler handler = new LongEventHandler();
        disruptorFirst.handleEventsWith(handler);
        disruptorSecond.handleEventsWith(handler);

        final ExceptionHandler<LongEvent> exceptionHandler = new ExceptionHandler<LongEventMain.LongEvent>() {

            @Override
            public void handleEventException(final Throwable ex, final long sequence, final LongEvent event) {
                System.out.println(ex + " " + sequence + " " + event);
            }

            @Override
            public void handleOnStartException(final Throwable ex) {
                System.out.println(ex);
            }

            @Override
            public void handleOnShutdownException(final Throwable ex) {
                System.out.println(ex);
            }
        };

        final ExceptionHandlerSetting<LongEvent> exceptionHandlerSetting = disruptorFirst.handleExceptionsFor(handler);
        exceptionHandlerSetting.with(exceptionHandler);

        // Start the Disruptor, starts all threads running
        disruptorFirst.start();
        disruptorSecond.start();

        // Get the ring buffer from the Disruptor to be used for publishing.
        final RingBuffer<LongEvent> ringBuffer01 = disruptorFirst.getRingBuffer();
        final RingBuffer<LongEvent> ringBuffer02 = disruptorFirst.getRingBuffer();

        // for (int eventCount = 0; eventCount < 32; eventCount++) {
        // final long sequenceId = ringBuffer.next();
        // final LongEvent valueEvent = ringBuffer.get(sequenceId);
        // valueEvent.set(eventCount);
        // ringBuffer.publish(sequenceId);
        // }
        final ByteBuffer bb = ByteBuffer.allocate(8);
        for (long l = 0; l < 10; l++) {
            bb.putLong(0, l);
            // System.out.println(ringBuffer.toString());
            // ringBuffer01.publishEvent((event, sequence, buffer) -> event.set(buffer.getLong(0)), bb);
            // ringBuffer02.publishEvent((event, sequence, buffer) -> event.set(buffer.getLong(0)), bb);
            ringBuffer01.publishEvent(TRANSLATOR, bb);
            ringBuffer02.publishEvent(TRANSLATOR, bb);
            Thread.sleep(1000);
        }

        disruptorFirst.halt();
        // System.out.println(disruptorFirst);
    }

}
