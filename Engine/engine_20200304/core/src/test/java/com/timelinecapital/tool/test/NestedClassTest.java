package com.timelinecapital.tool.test;

import java.util.ArrayList;
import java.util.List;

public class NestedClassTest {

    private final List<Long> longs = new ArrayList<>();

    private final Test T1;
    private final Test T2;

    public NestedClassTest() {
        T1 = new T1();
        T2 = new T2();
    }

    void set(final long value) {
        longs.add(value);
    }

    void doPublish() {
        T1.get();
        T2.get();
    }

    interface Test {
        void get();
    }

    class T1 implements Test {

        @Override
        public void get() {
            System.out.println(longs);
        }

    }

    class T2 implements Test {

        @Override
        public void get() {
            System.out.println(longs);
        }

    }

    public static void main(final String[] args) {
        final NestedClassTest test = new NestedClassTest();
        test.set(1);
        test.set(2);
        test.doPublish();
        test.set(3);
        test.doPublish();
    }

}
