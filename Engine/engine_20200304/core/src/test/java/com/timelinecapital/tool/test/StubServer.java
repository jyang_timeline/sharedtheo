package com.timelinecapital.tool.test;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

import org.agrona.concurrent.UnsafeBuffer;

import com.nogle.commons.protocol.TradeServerProtocol;
import com.timelinecapital.commons.binary.FillEncoder;
import com.timelinecapital.commons.binary.OrderAckEncoder;
import com.timelinecapital.commons.binary.PingResponseEncoder;
import com.timelinecapital.commons.binary.TimeInForce;

public class StubServer {

    public static void main(final String[] args) {
        if (args.length < 3) {
            System.err.println("Arguments should be: (Port) (DummyData Generator) (Ping responses) - ex: 6681 TRUE/FALSE/IDLE 50");
            System.exit(1);
        }

        final int port = Integer.parseInt(args[0]);

        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Server is listening on port " + port);

            // serverSocket.setOption(StandardSocketOptions.TCP_NODELAY, true);

            final byte[] buffer = new byte[2 * 1024];
            final ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
            final Socket socket = serverSocket.accept();

            System.out.println("New client connected");

            final InputStream in = socket.getInputStream();
            final DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

            for (int i = 0; i < Integer.parseInt(args[2]); i++) {
                final long startNano = System.nanoTime();
                generateResponse(byteBuffer, startNano);
                final byte[] result = Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
                outputStream.write(result);
                outputStream.flush();
                final long endNano = System.nanoTime();
                System.out.println(endNano - startNano);
                // Thread.sleep(5);
                byteBuffer.rewind();
            }
            Thread.sleep(1000);

            while (true) {
                if ("TRUE".contentEquals(args[1])) {
                    System.out.println("Preparing bytebuffer pos: " + byteBuffer.position() + " limit: " + byteBuffer.limit());
                    generateData(byteBuffer);
                    System.out.println("Generating bytebuffer pos: " + byteBuffer.position() + " limit: " + byteBuffer.limit());
                    final byte[] temp = Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
                    System.out.println("Generating data with length: " + temp.length + " position: " + byteBuffer.position());
                    outputStream.write(temp);
                    outputStream.flush();
                    Thread.sleep(5000);
                    byteBuffer.rewind();
                } else if ("IDLE".contentEquals(args[1])) {
                    final byte[] data = new byte[1024];
                    in.read(data);
                    System.out.println(Arrays.toString(data));
                } else {
                    generateDataWithUnknown(byteBuffer);
                    System.out.println("Generating single data with length: " + byteBuffer.array().length);
                    System.out.println("pos: " + byteBuffer.position());
                    System.out.println("limit: " + byteBuffer.limit());
                    System.out.println("capacity: " + byteBuffer.capacity());

                    final byte[] temp = Arrays.copyOfRange(byteBuffer.array(), 0, byteBuffer.position());
                    System.out.println("Writing data: " + temp.length);
                    outputStream.write(temp);
                    outputStream.flush();
                    byteBuffer.rewind();
                    // Thread.sleep(1000);
                    System.exit(0);
                }
            }

        } catch (final IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        } catch (final InterruptedException e) {
            System.out.println("Server exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    static void generateData(final ByteBuffer byteBuffer) {
        for (int i = 0; i < 10; i++) {
            byteBuffer.put(getAckBuffer(i));
        }
        for (int i = 0; i < 30; i++) {
            byteBuffer.put(getFillBuffer(i));
        }
    }

    static void generateDataWithUnknown(final ByteBuffer byteBuffer) {
        int i = 1;
        byteBuffer.put(getAckBuffer(i++, true));
        byteBuffer.put(getAckBuffer(i++, true));
        byteBuffer.put(getAckBuffer(i++, false));
        byteBuffer.put(getAckBuffer(i++, false));
        byteBuffer.put(getAckBuffer(i++, false));
        byteBuffer.put(getFillBuffer(i++, false));
        byteBuffer.put(getFillBuffer(i++, true));
    }

    static void generateResponse(final ByteBuffer byteBuffer, final long nanoTime) throws InterruptedException {
        byteBuffer.put(getPingResponse(nanoTime));
    }

    static ByteBuffer getPingResponse(final long nanoTime) {
        final UnsafeBuffer data = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.Ping_MsgLength));
        final PingResponseEncoder encoder = new PingResponseEncoder();
        encoder.wrap(data, 0);
        encoder.header((byte) TradeServerProtocol.Header_PingResponseChar);
        encoder.requestID(nanoTime);
        return encoder.buffer().byteBuffer();

    }

    static ByteBuffer getAckBuffer(final int seq) {
        final UnsafeBuffer data = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.Ack_MsgLength));
        final OrderAckEncoder encoder = new OrderAckEncoder();
        encoder.wrap(data, 0);
        encoder.header((byte) TradeServerProtocol.Header_OrderAckChar);
        encoder.clOrdId(seq + 1);
        encoder.side(com.timelinecapital.commons.binary.Side.BUY);
        encoder.timeInForce(TimeInForce.GOOD_TILL_DATE);
        return encoder.buffer().byteBuffer();
    }

    static ByteBuffer getFillBuffer(final int seq) {
        final UnsafeBuffer data = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.Fill_MsgLength));
        final FillEncoder encoder = new FillEncoder();
        encoder.wrap(data, 0);
        encoder.header((byte) TradeServerProtocol.Header_FillChar);
        encoder.clOrdId(seq + 1);
        encoder.side(com.timelinecapital.commons.binary.Side.BUY);
        encoder.fillQty(2);
        encoder.remainQty(1);
        encoder.fillPrice(55.0d);
        encoder.timeInForce(TimeInForce.GOOD_TILL_DATE);
        encoder.commission(2.75d);
        return encoder.buffer().byteBuffer();
    }

    static ByteBuffer getAckBuffer(final int seq, final boolean hasClientOrderId) {
        final UnsafeBuffer data = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.Ack_MsgLength));
        final OrderAckEncoder encoder = new OrderAckEncoder();
        encoder.wrap(data, 0);
        encoder.header((byte) TradeServerProtocol.Header_OrderAckChar);
        if (hasClientOrderId) {
            encoder.clOrdId(seq + 1);
        } else {
            encoder.clOrdId(0);
        }
        encoder.side(com.timelinecapital.commons.binary.Side.BUY);
        encoder.timeInForce(TimeInForce.GOOD_TILL_DATE);
        return encoder.buffer().byteBuffer();
    }

    static ByteBuffer getFillBuffer(final int seq, final boolean hasClientOrderId) {
        final UnsafeBuffer data = new UnsafeBuffer(ByteBuffer.allocateDirect(TradeServerProtocol.Fill_MsgLength));
        final FillEncoder encoder = new FillEncoder();
        encoder.wrap(data, 0);
        encoder.header((byte) TradeServerProtocol.Header_FillChar);
        if (hasClientOrderId) {
            encoder.clOrdId(seq + 1);
        } else {
            encoder.clOrdId(0);
        }
        encoder.side(com.timelinecapital.commons.binary.Side.BUY);
        encoder.fillQty(2);
        encoder.remainQty(1);
        encoder.fillPrice(55.0d);
        encoder.timeInForce(TimeInForce.GOOD_TILL_DATE);
        encoder.commission(2.75d);
        return encoder.buffer().byteBuffer();
    }

}
