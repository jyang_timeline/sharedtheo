package com.timelinecapital.tool.test;

public class ThreadNotifyTest {

    class Message {
        private String msg;

        public Message(final String str) {
            this.msg = str;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(final String str) {
            this.msg = str;
        }

    }

    class Waiter implements Runnable {

        private final Message msg;

        public Waiter(final Message m) {
            this.msg = m;
        }

        @Override
        public void run() {
            // final String name = Thread.currentThread().getName();
            while (true) {
                synchronized (msg) {
                    try {
                        // System.out.println(name + " waiting to get notified at time:" + System.currentTimeMillis());
                        msg.wait();
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                    // System.out.println(name + " waiter thread got notified at time:" + System.currentTimeMillis());
                    // process the message now
                    // System.out.println(name + " processed: " + msg.getMsg());
                }
            }
        }

    }

    class Notifier implements Runnable {

        private final Message msg;

        public Notifier(final Message msg) {
            this.msg = msg;
        }

        @Override
        public void run() {
            final String name = Thread.currentThread().getName();
            System.out.println(name + " started");
            // try {
            long spent = 0;
            for (int i = 0; i < 1000000; i++) {
                // Thread.sleep(1000);
                synchronized (msg) {
                    msg.setMsg(name + " Notifier work done");
                    final long start = System.nanoTime();
                    msg.notify();
                    final long end = System.nanoTime();
                    spent += end - start;
                    // System.out.println(end - start);
                    // msg.notifyAll();
                }
            }
            System.out.println(spent / 1000000);
            // } catch (final InterruptedException e) {
            // e.printStackTrace();
            // }

        }

    }

    public static void main(final String[] args) {
        // final double price = 24.58d;
        // final double tickSize = 0.01;
        // final double halfTickSize = tickSize / 2;
        // final double priceToTickRatio = 1 / tickSize;
        //
        // final double value = (int) ((price + halfTickSize) * priceToTickRatio) * tickSize;
        // System.out.println(value);
        // System.out.println(PriceUtils.toMantissa(value, 0.01));
        //
        // final double price2 = 0.1d + 0.7d;
        // final double value2 = (int) ((price2 + halfTickSize) * priceToTickRatio) * tickSize;
        // System.out.println(value2);
        // System.out.println(PriceUtils.toMantissa(price2, 0.01));

        final ThreadNotifyTest clazz = new ThreadNotifyTest();
        final Message msg = clazz.new Message("process it");

        final Waiter waiter0 = clazz.new Waiter(msg);
        new Thread(waiter0, "waiter0").start();
        final Waiter waiter1 = clazz.new Waiter(msg);
        new Thread(waiter1, "waiter1").start();

        final Notifier notifier = clazz.new Notifier(msg);
        new Thread(notifier, "notifier").start();

        System.out.println("All the threads are started");
    }

}
