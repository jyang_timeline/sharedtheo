package com.timelinecapital.warmup;

import java.util.Random;

import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.SnapshotEncoder;
import com.timelinecapital.core.config.EngineConfig;

public class BinaryQuoteWarmupUtil {

    private final SnapshotEncoder snapshotEncoder = EncoderInstanceFactory.getSnptEncoder(EngineConfig.getSchemaVersion());
    private final Random random = new Random();

    public BinaryQuoteWarmupUtil() {
    }

    public byte[] getQuoteSnapshot(final Contract contract) {
        snapshotEncoder.setSymbol(contract.getSymbol());
        snapshotEncoder.setTradeType(TickType.UNKNOWN.getCode());
        snapshotEncoder.setPrice(1 + random.nextDouble() * 100);
        snapshotEncoder.setQuantity(random.nextInt(100) + 1);
        snapshotEncoder.setOpenInterest(random.nextInt(1000) + 1);
        snapshotEncoder.setVolume(random.nextInt(1000) + 1);
        snapshotEncoder.setUpdateFlag(random.nextInt(2) + 1);
        snapshotEncoder.setQuoteType(QuoteType.values()[random.nextInt(QuoteType.values().length)].getCode());
        snapshotEncoder.setBidDepth(1);
        snapshotEncoder.setAskDepth(1);
        snapshotEncoder.setBidPrice(0, (1 + random.nextDouble()) * 100);
        snapshotEncoder.setBidQty(0, random.nextInt(100) + 1);
        snapshotEncoder.setAskPrice(0, (1 + random.nextDouble()) * 100);
        snapshotEncoder.setAskQty(0, random.nextInt(100) + 1);
        return snapshotEncoder.getData();
    }

}
