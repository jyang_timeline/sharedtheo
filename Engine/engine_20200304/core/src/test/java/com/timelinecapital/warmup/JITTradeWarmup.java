package com.timelinecapital.warmup;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.marketdata.QuoteViewFactory;
import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.types.RejectReason;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.ExecRequestType;
import com.nogle.strategy.types.OrderView;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.execution.OrderHolder;
import com.timelinecapital.core.execution.factory.RequestInstance;
import com.timelinecapital.core.stats.OverviewFactory;
import com.timelinecapital.core.trade.TradingAccount;

public class JITTradeWarmup {
    private static final Logger log = LogManager.getLogger(JITTradeWarmup.class);

    private final TimeCondition[] tifs = new TimeCondition[] { TimeCondition.GFD, TimeCondition.IOC };
    private final Side[] sides = new Side[] { Side.BUY, Side.SELL };

    private OrderHolder orderHolder;
    private TradeEvent event;

    private Random rnd;
    private int orderAcks = 0;
    private int orderRejects = 0;
    private int cancelAcks = 0;
    private int miss = 0;
    private int fill = 0;

    private static JITTradeWarmup jitTradeWarmup;

    public static JITTradeWarmup getInstance() {
        if (jitTradeWarmup == null) {
            jitTradeWarmup = new JITTradeWarmup();
        }
        return jitTradeWarmup;
    }

    private JITTradeWarmup() {
    }

    public synchronized void warmupTrade(final StrategyUpdater strategyUpdater, final TradingAccount account) {
        try {
            rnd = new Random(System.currentTimeMillis());
            orderAcks = 0;
            orderRejects = 0;
            cancelAcks = 0;
            miss = 0;
            fill = 0;

            final BinaryQuoteWarmupUtil util = new BinaryQuoteWarmupUtil();
            final int itrs = 1000;

            log.warn("Starting Trade warm-up...");
            WarmupUtils.onSystemLogSuppress();

            for (final Contract contract : strategyUpdater.getContracts()) {
                WarmupUtils.setUpQuoteView(contract);

                final StrategyQuoteSnapshot quoteSnapshot = (StrategyQuoteSnapshot) strategyUpdater.getTradeServices().getQuoteSnapshot(contract);
                final QuoteView quoteView = QuoteViewFactory.getQuoteView(contract, EngineConfig.getSchemaVersion());

                for (final Side eachSide : sides) {
                    for (final TimeCondition tif : tifs) {
                        final OrderView orderView = strategyUpdater.getTradeServices().getOrderView(contract, eachSide, tif);
                        orderHolder = (OrderHolder) strategyUpdater.getTradeServices().getOrderView(contract, eachSide, tif);

                        for (int i = 0; i < itrs; i++) {
                            if (!orderHolder.getSendOrderPermission()) {
                                orderHolder.setSendOrderPermission(true);
                            }
                            if (orderHolder.getBlockingCount() != 0) {
                                orderHolder.reset();
                            }
                            // TODO way to reset positions?
                            // orderHolder.resetPositions();

                            // quoteView.updateMarketDataSnapshot(TradeClock.getCurrentMicros(),
                            // util.getQuoteSnapshot(contract));
                            // quoteView.updateTick(TradeClock.getCurrentMicros(), WarmupUtils.getTick(contract));
                            // quoteView.updateBook(TradeClock.getCurrentMicros(), WarmupUtils.getBook(contract));
                            quoteSnapshot.setMarketBook(quoteView.getBook());

                            final double price = ((contract.getLimitUp() + contract.getLimitDown()) / 2) + contract.getTickCalculator().roundUpToNearestTick(rnd.nextInt(20) + 1);
                            final long qty = rnd.nextInt(20) + 1;

                            try {
                                if (orderView.getOutstandingOrders().size() >= 3) {
                                    orderView.clearAllOrders();
                                    orderView.commit(i);
                                } else {
                                    orderView.setOrder(qty, price, 0);
                                    orderView.commit(i);
                                }
                            } catch (final Exception e) {
                                log.info(e.getMessage());
                            }
                        }

                        orderHolder.rewind();
                    }
                }
                // EngineStatusView.getMarketEvent(contract).reset();
                OverviewFactory.getExecReportSBean(contract, account).reset();
            }
            log.warn("Strategy's order commit warmup is complete {} {} {} {} {}", orderAcks, orderRejects, cancelAcks, miss, fill);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void onIncomingEvent(final RequestInstance orderEntryObject) {
        WarmupUtils.onMessage();
        switch (orderEntryObject.getRequestType()) {
            case OrderCancel:
                event = orderHolder.getTradeEvent();
                event.onCancelAck(orderEntryObject.getClOrderId());
                cancelAcks++;
                break;
            case OrderEntry:
                final long clOrderId = orderEntryObject.getClOrderId();
                event = orderHolder.getTradeEvent();
                if (orderEntryObject.getTimeCondition() == TimeCondition.IOC) {
                    if (rnd.nextBoolean()) {
                        event.onOrderAck(clOrderId);
                        event.onCancelAck(clOrderId);
                        miss++;
                    } else {
                        event.onOrderAck(clOrderId);
                        event.onFill(clOrderId, orderEntryObject.getQuantity(), 0, orderEntryObject.getPrice(), 0.0);
                        fill++;
                    }
                } else if (orderEntryObject.getTimeCondition() == TimeCondition.GFD) {
                    if (orderEntryObject.getRequestType() == ExecRequestType.OrderCancel) {
                        event.onCancelAck(clOrderId);
                        cancelAcks++;
                    } else {
                        if (rnd.nextBoolean()) {
                            event.onOrderAck(clOrderId);
                            // event.onFill(clOrderId, orderEntryObject.getQuantity(), 0, orderEntryObject.getPrice(), 0.0);
                            orderAcks++;
                        } else {
                            event.onOrderReject(clOrderId, RejectReason.U);
                            orderRejects++;
                        }
                    }
                } else {
                    log.error("Unknown Object: {}", orderEntryObject);
                }
                break;
            default:
                log.error("No request type: {}", orderEntryObject);
                break;
        }
    }

}
