package com.timelinecapital.warmup;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.network.Constants;
import com.nogle.commons.socket.ConnectionProbe;
import com.nogle.commons.socket.MessageInfo;
import com.nogle.commons.socket.exception.UnsupportedNodeException;
import com.nogle.core.marketdata.LegacyPriceFeedProxy;
import com.nogle.core.marketdata.subscriber.MarketBookBinarySub;
import com.nogle.core.marketdata.subscriber.MarketDataBinarySub.MarketDataHandler;
import com.nogle.core.marketdata.subscriber.QuoteSnapshotBinarySub;
import com.nogle.core.marketdata.subscriber.TickBinarySub;
import com.nogle.strategy.types.Contract;
import com.timelinecapital.commons.channel.ConnectionType;
import com.timelinecapital.core.config.EngineConfig;

public class PriceFeedWarmup {
    private static final Logger log = LogManager.getLogger(PriceFeedWarmup.class);

    private final WarmupUtils utils;
    private final LegacyPriceFeedProxy priceFeedProxy;
    private final QuoteSnapshotBinarySub qsSubscriber;
    private final MarketBookBinarySub mbSubscriber;
    private final TickBinarySub tkSubscriber;

    public PriceFeedWarmup(final WarmupUtils utils) throws UnsupportedNodeException {
        this.utils = utils;
        priceFeedProxy = utils.getPriceFeedProxy();
        final ConnectionProbe probe = new ConnectionProbe(ConnectionType.QUOTE, StringUtils.EMPTY);
        qsSubscriber = new QuoteSnapshotBinarySub(Constants.MULTICAST_PREFIX + "dummy-snapshot:12345:lo", probe, EngineConfig.getSchemaVersion());
        mbSubscriber = new MarketBookBinarySub(Constants.MULTICAST_PREFIX + "dummy-marketbook:12345:lo", probe, EngineConfig.getSchemaVersion());
        tkSubscriber = new TickBinarySub(Constants.MULTICAST_PREFIX + "dummy-trade:12345:lo", probe, EngineConfig.getSchemaVersion());
    }

    void warmupByDefault() {
        // final MessageInfo info = new MessageInfo();
        // final MarketDataHandler marketbookHandler = mbSubscriber.new MarketDataHandler();
        // final MarketDataHandler tradeHandler = tkSubscriber.new MarketDataHandler();
        // // final MarketEvent marketEvent = EngineStatusView.getMarketEvent(utils.getContract());
        //
        // final QuoteEvent quoteEventHandler = priceFeedProxy.new QuoteEventHandler(utils.getQuoteView());
        // mbSubscriber.addHandler(utils.getContract().getSymbol(), quoteEventHandler);
        // tkSubscriber.addHandler(utils.getContract().getSymbol(), quoteEventHandler);
        //
        // int count = 0;
        // while (count < utils.getWarmupIteration()) {
        // // tradeHandler.onData(info, utils.getTick());
        // // marketbookHandler.onData(info, utils.getBook());
        // count++;
        // }
        // log.warn("PriceFeedProxy default warmup is complete with {} iteration", utils.getWarmupIteration());
    }

    void warmupBySnapshot() {
        // final MessageInfo info = new MessageInfo();
        // final MarketDataHandler snapshotHandler = qsSubscriber.new MarketDataHandler();
        // // final MarketEvent marketEvent = EngineStatusView.getMarketEvent(utils.getContract());
        //
        // final SnapshotFeedEvent quoteSnapshotEvent = priceFeedProxy.new SnapshotHandler(utils.getQuoteView());
        // qsSubscriber.addQuoteSnapshotHandler(utils.getContract().getSymbol(), quoteSnapshotEvent);
        //
        // int count = 0;
        // while (count < utils.getWarmupIteration()) {
        // // snapshotHandler.onData(info, utils.getQuoteSnapshot(utils.getContract()));
        // count++;
        // }
        // log.warn("PriceFeedProxy snapshot warmup is complete with {} iteration", utils.getWarmupIteration());
    }

    public void warmup(final Contract contract, final MarketDataHandler dataHandler) {
        final MessageInfo info = new MessageInfo();
        int count = 0;
        while (count < 5000) {
            // dataHandler.onData(info, utils.getQuoteSnapshot(contract));
            count++;
        }
    }

}
