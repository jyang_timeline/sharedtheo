package com.timelinecapital.warmup;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.event.TradeEvent;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TimeCondition;
import com.timelinecapital.commons.channel.Exchange;
import com.timelinecapital.core.DummyTrade;
import com.timelinecapital.core.execution.factory.RequestInstance;
import com.timelinecapital.core.stats.OverviewFactory;

public class TradeWarmup {
    private static final Logger log = LogManager.getLogger(TradeWarmup.class);

    private final TimeCondition[] tifs = new TimeCondition[] { TimeCondition.GFD, TimeCondition.IOC };
    private final Side[] sides = new Side[] { Side.BUY, Side.SELL };

    private Random rnd;

    private static TradeWarmup runner;
    private final TradeEvent tradeEvent;

    public static TradeWarmup getInstance() {
        if (runner == null) {
            runner = new TradeWarmup();
        }
        return runner;
    }

    private TradeWarmup() {
        tradeEvent = WarmupUtils.getDummyTradeEvent();
    }

    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    private static final String CME = Exchange.CME.name();

    public synchronized void onDummyTradeMessage(final StrategyUpdater strategyUpdater) {
        try {
            rnd = new Random(System.currentTimeMillis());
            final int itrs = 1000;

            log.warn("Starting Trade warm-up...");
            WarmupUtils.onSystemLogSuppress();

            for (final Contract contract : strategyUpdater.getContracts()) {
                if (contract.getExchange().contentEquals(CME)) {
                    continue;
                }

                final DummyTrade dummyTrade = strategyUpdater.getDummyTrade(contract);
                // final OrderViewContainer orderViewContainer = strategyUpdater.getOrderViewContainer(contract);
                for (final Side side : sides) {
                    for (final TimeCondition tif : tifs) {
                        for (int i = 0; i < itrs; i++) {
                            final double price = ((contract.getLimitUp() + contract.getLimitDown()) / 2) + contract.getTickCalculator().roundUpToNearestTick(rnd.nextInt(20) + 1);
                            final long qty = rnd.nextInt(8) + 1;

                            try {
                                lock.lock();
                                dummyTrade.onDummyMessage(side, tif, qty, price, itrs, tradeEvent);
                                condition.awaitNanos(1000);
                            } catch (final Exception e) {
                                log.error(e.getMessage());
                            } finally {
                                lock.unlock();
                            }
                        }
                    }
                }

                strategyUpdater.resetDummyTrade();
                OverviewFactory.getContractOverviews().forEach((k, v) -> v.reset());
            }

            log.warn("Strategy's Trade warmup is complete");
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void onIncomingMessage(final RequestInstance orderEntryObject) {
        WarmupUtils.onMessage();
    }

}
