package com.timelinecapital.warmup;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.joda.time.DateTime;

import com.nogle.commons.protocol.StrategyConfigProtocol;
import com.nogle.core.event.IdleTradeEvent;
import com.nogle.core.event.TradeEvent;
import com.nogle.core.exception.ConditionNotFoundException;
import com.nogle.core.marketdata.LegacyPriceFeedProxy;
import com.nogle.core.marketdata.QuoteView;
import com.nogle.core.marketdata.QuoteViewFactory;
import com.nogle.core.strategy.StrategyQuoteSnapshot;
import com.nogle.core.trade.LiveTradeProxy;
import com.nogle.core.util.PriceUtils;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.QuoteType;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickCalculator;
import com.nogle.strategy.types.TickType;
import com.timelinecapital.commons.marketdata.EncoderInstanceFactory;
import com.timelinecapital.commons.marketdata.encoder.MarketBookEncoder;
import com.timelinecapital.commons.marketdata.encoder.SnapshotEncoder;
import com.timelinecapital.commons.marketdata.encoder.TradeEncoder;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.factory.ContractFactory;
import com.timelinecapital.core.riskmanagement.PositionManager;
import com.timelinecapital.core.riskmanagement.PositionManagerBuilder;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;

public class WarmupUtils {
    private static final Logger log = LogManager.getLogger(WarmupUtils.class);

    private static ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[1024]);
    private static ServerSocketChannel serverSocket;
    private static ReadableByteChannel readSocketChannel;

    private final Random random = new Random();

    private final int iteration;

    private final MarketBookEncoder marketbookEncoder;
    private final TradeEncoder tradeEncoder;
    private final SnapshotEncoder snapshotEncoder;

    private final LegacyPriceFeedProxy priceFeedProxy;
    private final LiveTradeProxy tradeProxy;

    private final StrategyQuoteSnapshot quoteSnapshot;
    private final PositionManager positionManager;
    private final ProtectiveManager riskControlManager;
    private final QuoteView quoteView;
    // private final MarketEvent marketEvent;

    public WarmupUtils(final LegacyPriceFeedProxy priceFeedProxy, final LiveTradeProxy tradeProxy) throws ConditionNotFoundException {
        final Contract contract = ContractFactory.getSelfDefinedContract(EngineConfig.getWarmupSymbol());
        this.priceFeedProxy = priceFeedProxy;
        this.tradeProxy = tradeProxy;

        iteration = EngineConfig.getWarmupCycles();

        marketbookEncoder = EncoderInstanceFactory.getBookEncoder(EngineConfig.getSchemaVersion(), 0);
        marketbookEncoder.setSymbol(contract.getSymbol());
        tradeEncoder = EncoderInstanceFactory.getTickEncoder(EngineConfig.getSchemaVersion(), 0);
        tradeEncoder.setSymbol(contract.getSymbol());
        snapshotEncoder = EncoderInstanceFactory.getSnptEncoder(EngineConfig.getSchemaVersion());
        snapshotEncoder.setSymbol(contract.getSymbol());

        // marketEvent = new MarketEvent(contract);
        quoteView = QuoteViewFactory.getQuoteView(contract, EngineConfig.getSchemaVersion());
        quoteSnapshot = new StrategyQuoteSnapshot(contract);

        positionManager = genWarmupPositionManager(contract);
        riskControlManager = genWarmupRiskControlManager();
    }

    public static void initWarmupServer() throws IOException {
        final InetSocketAddress inetAddr = new InetSocketAddress("127.0.0.1", 19999 + EngineConfig.getEngineId());
        serverSocket = ServerSocketChannel.open();
        serverSocket.bind(inetAddr);
    }

    public static void shutdownWarmupServer() throws IOException {
        try {
            if (readSocketChannel != null) {
                readSocketChannel.close();
            }
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (final IOException e) {
            log.error("Fail to close internal warm up server: {}", e.getMessage());
        }
    }

    public static void onMessage() {
        try {
            byteBuffer.clear();
            readSocketChannel.read(byteBuffer);
        } catch (final SocketTimeoutException e) {
            log.info("Socket time out from waiting new warmup message");
        } catch (final IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void connectWarmupServer() throws IOException {
        final Socket connecetedSocket = serverSocket.socket().accept();
        connecetedSocket.setSoTimeout(1000);
        readSocketChannel = Channels.newChannel(connecetedSocket.getInputStream());
    }

    public static synchronized void onSystemLogSuppress() {
        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final LoggerConfig loggerConfig = ctx.getConfiguration().getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.removeAppender("EngineRollingFile");
        loggerConfig.addAppender(ctx.getConfiguration().getAppender("EngineRollingFile"), Level.WARN, null);
        ctx.updateLoggers();
    }

    public static synchronized void onSystemLogResume() {
        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final LoggerConfig loggerConfig = ctx.getConfiguration().getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.removeAppender("EngineRollingFile");
        loggerConfig.addAppender(ctx.getConfiguration().getAppender("EngineRollingFile"), Level.DEBUG, null);
        ctx.updateLoggers();
    }

    public static synchronized void onStrategyLogSuppress() {
        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final LoggerConfig loggerConfig = ctx.getConfiguration().getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.removeAppender("StrategyRoutingFile");
        loggerConfig.addAppender(ctx.getConfiguration().getAppender("StrategyRoutingFile"), Level.WARN, null);
        ctx.updateLoggers();
    }

    public static synchronized void onStrategyLogResume() {
        final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final LoggerConfig loggerConfig = ctx.getConfiguration().getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.removeAppender("StrategyRoutingFile");
        loggerConfig.addAppender(ctx.getConfiguration().getAppender("StrategyRoutingFile"), Level.DEBUG, null);
        ctx.updateLoggers();
    }

    public static TradeEvent getDummyTradeEvent() {
        return IdleTradeEvent.getInstance();
    }

    public LegacyPriceFeedProxy getPriceFeedProxy() {
        return priceFeedProxy;
    }

    public LiveTradeProxy getTradeProxy() {
        return tradeProxy;
    }

    public PositionManager getPositionManager() {
        return positionManager;
    }

    public ProtectiveManager getRiskControlManager() {
        return riskControlManager;
    }

    public int getWarmupStrategyId() {
        return new DateTime().getDayOfYear();
    }

    public int getWarmupIteration() {
        return iteration;
    }

    public QuoteView getQuoteView() {
        return quoteView;
    }

    public Contract getContract() {
        return quoteView.getContract();
    }

    private static final MarketBookEncoder mbEncoder = EncoderInstanceFactory.getBookEncoder(EngineConfig.getSchemaVersion(), 0);
    private static final TradeEncoder tkEncoder = EncoderInstanceFactory.getTickEncoder(EngineConfig.getSchemaVersion(), 0);
    private static final Random myRandom = new Random(System.currentTimeMillis());
    private static TickCalculator tickCalculator;
    private static double basePrice;

    public static void setUpQuoteView(final Contract contract) {
        basePrice = (contract.getLimitUp() + contract.getLimitDown()) / 2;
        tickCalculator = contract.getTickCalculator();
    }

    public static byte[] getBook(final Contract contract) {
        mbEncoder.setSymbol(contract.getSymbol());
        mbEncoder.setUpdateFlag(myRandom.nextInt(2) + 1);
        mbEncoder.setQuoteType(QuoteType.values()[myRandom.nextInt(QuoteType.values().length)].getCode());
        mbEncoder.setBidDepth(1);
        mbEncoder.setAskDepth(1);
        mbEncoder.setBidPrice(0, basePrice - tickCalculator.toPrice(myRandom.nextInt(10) + 1));
        mbEncoder.setBidQty(0, myRandom.nextInt(100) + 1);
        mbEncoder.setAskPrice(0, basePrice + tickCalculator.toPrice(myRandom.nextInt(10) + 1));
        mbEncoder.setAskQty(0, myRandom.nextInt(100) + 1);
        // return mbEncoder.getData();
        return mbEncoder.getBuffer().array();
    }

    public static byte[] getTick(final Contract contract) {
        tkEncoder.setSymbol(contract.getSymbol());
        tkEncoder.setTradeType(TickType.UNKNOWN.getCode());
        tkEncoder.setPrice(basePrice + tickCalculator.toPrice(myRandom.nextInt(10) + 1));
        tkEncoder.setQuantity(myRandom.nextInt(100) + 1);
        tkEncoder.setOpenInterest(myRandom.nextInt(1000) + 1);
        tkEncoder.setVolume(myRandom.nextInt(1000) + 1);
        // return tkEncoder.getData();
        return tkEncoder.getBuffer().array();
    }

    public byte[] getQuoteSnapshot(final Contract contract) {
        snapshotEncoder.setSnapshotUpdateFlag(random.nextInt(3) + 1);
        snapshotEncoder.setSymbol(contract.getSymbol());
        snapshotEncoder.setTradeType(TickType.UNKNOWN.getCode());
        snapshotEncoder.setPrice(1 + random.nextDouble() * 100);
        snapshotEncoder.setQuantity(random.nextInt(100) + 1);
        snapshotEncoder.setOpenInterest(random.nextInt(1000) + 1);
        snapshotEncoder.setVolume(random.nextInt(1000) + 1);
        snapshotEncoder.setUpdateFlag(random.nextInt(2) + 1);
        snapshotEncoder.setQuoteType(QuoteType.values()[random.nextInt(QuoteType.values().length)].getCode());
        snapshotEncoder.setBidDepth(1);
        snapshotEncoder.setAskDepth(1);
        snapshotEncoder.setBidPrice(0, (1 + random.nextDouble()) * 100);
        snapshotEncoder.setBidQty(0, random.nextInt(100) + 1);
        snapshotEncoder.setAskPrice(0, (1 + random.nextDouble()) * 100);
        snapshotEncoder.setAskQty(0, random.nextInt(100) + 1);
        return snapshotEncoder.getData();
    }

    public byte[] getBook() {
        marketbookEncoder.setQuoteType(QuoteType.values()[random.nextInt(QuoteType.values().length)].getCode());
        marketbookEncoder.setBidDepth(1);
        marketbookEncoder.setAskDepth(1);
        marketbookEncoder.setAskPrice(0, (1 + random.nextDouble()) * 100);
        marketbookEncoder.setAskQty(0, random.nextInt(100));
        marketbookEncoder.setBidPrice(0, (1 + random.nextDouble()) * 100);
        marketbookEncoder.setBidQty(0, random.nextInt(100));
        // return marketbookEncoder.getData();
        return marketbookEncoder.getBuffer().array();
    }

    public byte[] getTick() {
        tradeEncoder.setTradeType(TickType.UNKNOWN.getCode());
        tradeEncoder.setPrice(1 + random.nextDouble() * 100);
        tradeEncoder.setQuantity(1 + random.nextInt(100));
        // return tradeEncoder.getData();
        return tradeEncoder.getBuffer().array();
    }

    public double getTradePrice(final Side side) {
        switch (side) {
            case BUY:
                return quoteView.getBook().getAskPrice();
            case SELL:
                return quoteView.getBook().getBidPrice();
            default:
                return PriceUtils.getMidPoint(quoteView.getBook().getQuoteType(), quoteView.getBook().getBidPrice(), quoteView.getBook().getAskPrice());
        }
    }

    private PositionManager genWarmupPositionManager(final Contract contract) throws ConditionNotFoundException {
        return PositionManagerBuilder.buildPositionManager(contract, new WarmupConfig());
    }

    private ProtectiveManager genWarmupRiskControlManager() throws ConditionNotFoundException {
        // return new ProtectiveManager();
        return null;
    }

    private class WarmupConfig implements Config {
        private final Map<String, Integer> keyToInteger;

        public WarmupConfig() {
            keyToInteger = new HashMap<>();
            keyToInteger.put(StrategyConfigProtocol.Key.positionMaxPositionPerSide.toUntypedString(), Integer.MAX_VALUE);
            keyToInteger.put(StrategyConfigProtocol.Key.positionMaxOrderQty.toUntypedString(), Integer.MAX_VALUE);
            keyToInteger.put(StrategyConfigProtocol.Key.positionMaxOrderPerSide.toUntypedString(), Integer.MAX_VALUE);
            keyToInteger.put(StrategyConfigProtocol.Key.positionMinOrderQty.toUntypedString(), 1);
            keyToInteger.put(StrategyConfigProtocol.Key.positionMaxCrossTicks.toUntypedString(), Integer.MAX_VALUE);
            keyToInteger.put(StrategyConfigProtocol.Key.positionSequentOrderDelay.toUntypedString(), 0);
            keyToInteger.put(StrategyConfigProtocol.Key.positionCancelDelay.toUntypedString(), 0);
        }

        @Override
        public boolean containKeys(final String... arg0) {
            return false;
        }

        @Override
        public Boolean getBoolean(final String key) {
            return false;
        }

        @Override
        public Boolean getBoolean(final String key, final Boolean defaultValue) {
            return defaultValue;
        }

        @Override
        public Double getDouble(final String key) {
            return Double.NaN;
        }

        @Override
        public Double getDouble(final String key, final Double defaultValue) {
            return defaultValue;
        }

        @Override
        public Integer getInteger(final String key) {
            return keyToInteger.get(key);
        }

        @Override
        public Integer getInteger(final String key, final Integer defaultValue) {
            return defaultValue;
        }

        @Override
        public String getString(final String key) {
            return StringUtils.EMPTY;
        }

        @Override
        public String getString(final String key, final String defaultValue) {
            return defaultValue;
        }

        @Override
        public Set<String> getKeys() {
            return Collections.emptySet();
        }

    }
}
