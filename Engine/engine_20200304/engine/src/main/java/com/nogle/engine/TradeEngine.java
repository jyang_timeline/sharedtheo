package com.nogle.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.core.ClientParameters;
import com.nogle.core.config.EngineMode;
import com.nogle.core.contract.ContractInquiry;
import com.nogle.core.control.commands.LoadCommand;
import com.nogle.core.controller.Commander;
import com.nogle.core.exception.InvalidFeedEndpointException;
import com.nogle.core.market.LiveMarket;
import com.nogle.core.strategy.StrategyLoader;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.StrategyView;
import com.nogle.core.util.JobScheduler;
import com.nogle.engine.builtin.PrearrangementManager;
import com.timelinecapital.core.CoreController;
import com.timelinecapital.core.CoreController.CoreControllerBuilder;
import com.timelinecapital.core.commands.system.ContractManagement;
import com.timelinecapital.core.commands.system.CounterBasedPositionManagement;
import com.timelinecapital.core.commands.system.MonitorEventListener;
import com.timelinecapital.core.commands.system.RiskManagement;
import com.timelinecapital.core.config.EngineConfig;
import com.timelinecapital.core.config.RiskManagerConfig;
import com.timelinecapital.core.config.SimulationConfig;
import com.timelinecapital.core.factory.FactoryBase;
import com.timelinecapital.core.instrument.Instrument;
import com.timelinecapital.core.riskmanagement.ProtectiveManager;
import com.timelinecapital.core.riskmanagement.RiskManager;
import com.timelinecapital.core.strategy.StrategyEventManager;
import com.timelinecapital.engine.EngineArguments;
import com.timelinecapital.engine.MemoryUsageHelper;

import net.openhft.affinity.AffinityLock;

public class TradeEngine {

    /*-
     * version:
     *  #strategy-interface
     *  #network
     *  #utils
     *  #nogle-messaging
     *  #Date
     */
    private static final String ENGINE_VERSION = "1.153.403.111-2020.02.14_exchange_time";
    private static final String MESSAGE_HUB_FILE = "message.properties";

    private static final Map<Integer, String> idToName = new HashMap<>();
    private static final Map<Integer, StrategyUpdater> idToUpdater = new HashMap<>();
    private static final Map<Integer, StrategyEventManager> idToEventManager = new HashMap<>();
    private static final Map<Integer, StrategyView> idToStatusView = new HashMap<>();
    private static final Map<Integer, ProtectiveManager> idToRiskControl = new HashMap<>();
    private static final Map<String, Instrument> contracts = new HashMap<>();

    private static Logger log;

    private static byte[] defaultPropsByteArray;

    private static int engineId;
    private static String serverName;

    private static ContractInquiry contractInquiry;
    private static RiskManager riskManager;

    private static Commander commander;
    private static LoadCommand loadCommand;
    private static PrearrangementManager manager;

    static {
        if (!SystemUtils.IS_OS_WINDOWS && !SystemUtils.IS_OS_MAC) {
            System.loadLibrary("MicrosecondClock");
            // System.loadLibrary("OnloadExt");
        }
    }

    public static void main(final String[] args) {
        final EngineArguments arguments = EngineArguments.resolveArg(args);
        engineId = arguments.getEngineId();

        System.setProperty("engineId", "TE" + engineId);
        System.setProperty("logFilename", TradeEngine.class.getSimpleName() + "-" + engineId);
        System.setProperty("python.console.encoding", "UTF-8");

        try {
            EngineConfig.buildConfig(engineId, new File(arguments.getEngineProperties()), arguments.getEngineMode());
            initLogConfig();
            log = LogManager.getLogger();
            log.info("Engine {} start at version: [{}] and time: [{}]", engineId, ENGINE_VERSION, new Date());

            RiskManagerConfig.buildConfig();

            try {
                if (StringUtils.isEmpty(EngineConfig.getServerName())) {
                    serverName = InetAddress.getLocalHost().getHostName();
                } else {
                    serverName = EngineConfig.getServerName();
                }
            } catch (final UnknownHostException e) {
                serverName = EngineConfig.getServerName();
            }

            if (EngineMode.SIMULATION.equals(arguments.getEngineMode())) {
                SimulationConfig.buildConfig(
                    new File(arguments.getSimulationProperties()),
                    arguments.getSimStart(),
                    arguments.getSimEnd(),
                    arguments.getSimModels());

                final File propertiesFile = new File(TradeEngine.class.getClassLoader().getResource(MESSAGE_HUB_FILE).toURI());
                defaultPropsByteArray = FileUtils.readFileToByteArray(propertiesFile);

                final Properties props = new Properties();
                final FileInputStream is = new FileInputStream(propertiesFile);
                props.load(is);
                is.close();

                final FileOutputStream os = new FileOutputStream(propertiesFile);
                props.setProperty("message.client.id", "SIM-" + Inet4Address.getLocalHost().getHostName());
                props.store(os, "Only for simulation to avoid collision of using the same engineID");
                os.close();
            }
        } catch (final ConfigurationException | IOException | URISyntaxException e) {
            System.err.println(e.getMessage());
            Runtime.getRuntime().exit(1);
        }

        if (EngineMode.PRODUCTION.equals(arguments.getEngineMode())) {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    log.info("Shutdown hook triggered, closing all models and TradeEngine...");

                    final StrategyLoader strategyLoader = CoreController.getStrategyLoader();
                    for (final StrategyUpdater strategyUpdater : strategyLoader.getStrategyUpdaters()) {
                        try {
                            strategyLoader.shutdownStrategy(strategyUpdater);
                        } catch (final Exception e) {
                            log.warn("Unable to shutdown {} gracefully: {}", strategyUpdater.getStrategyName(), e.getMessage());
                        }
                    }
                    PrearrangementManager.close();
                    LogManager.shutdown();
                    try {
                        JobScheduler.getInstance().close();
                    } catch (final Exception e) {
                        log.error("Unable to shutdown JobScheduler gracefully: {}", e.getMessage());
                        System.exit(1);
                    }
                }
            });
        }

        try {
            new TradeEngine(arguments);
        } catch (final InvalidFeedEndpointException e) {
            log.error("TradeEngine terminated by {} ", e.getMessage());
            System.exit(1);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public TradeEngine(final EngineArguments arguments) throws Exception {
        dumpAffinityInfo(arguments.getEngineMode());

        initService(arguments.getTargetAccount());
        initFactory(
            arguments.getEngineMode(),
            arguments.getLocalFilePath(),
            arguments.isHasLocalContractInfo(),
            arguments.isUseCachedPersistForSimulation(),
            arguments.isSingleThreadOnly());
        initRiskControl();

        final CoreControllerBuilder controllerBuilder = new CoreControllerBuilder();

        if (EngineMode.SIMULATION.equals(arguments.getEngineMode())) {
            try {
                controllerBuilder.withMappingIdToName(idToName)
                    .withMappingIdToUpdater(idToUpdater)
                    .withMappingIdToEventManager(idToEventManager)
                    .withMappingIdToStatusView(idToStatusView)
                    .withMappingIdToRiskControl(idToRiskControl)
                    .withRiskManager(riskManager)
                    .withContractInquiry(contractInquiry)
                    .build();

                manager = new PrearrangementManager(SimulationConfig.getSimModels(), arguments.isSingleThreadOnly());
                manager.load(loadCommand);
            } catch (final Exception e) {
                log.error(e.getMessage(), e);
            } finally {
                PrearrangementManager.resotreMessengerProperties(MESSAGE_HUB_FILE, defaultPropsByteArray);
                PrearrangementManager.close();
                log.warn("Java memory usage: Total: {} Free: {} Used: {} Max: {}",
                    MemoryUsageHelper.getTotalMemoryInMiB(),
                    MemoryUsageHelper.getFreeMemoryInMiB(),
                    MemoryUsageHelper.getUsedMemoryInMiB(),
                    MemoryUsageHelper.getMaxMemoryInMiB());
                LogManager.shutdown();
                try {
                    JobScheduler.getInstance().getScheduler().clear();
                    JobScheduler.getInstance().getScheduler().shutdown();
                } catch (final Exception e) {
                    log.error("Unable to shutdown JobScheduler gracefully: {}", e.getMessage());
                    System.exit(1);
                }
            }
        } else {
            commander.addSystemCommand(loadCommand);

            final LiveMarket market = new LiveMarket(new ClientParameters(arguments.getTargetAccount(), arguments.getPassword()));
            market.startNotificationService();
            market.startConnectionMonitorService();
            market.startWatchdogService();
            market.startTradingHoursNotifyService();
            market.onPreOpening();
            commander.addSystemCommand(new MonitorEventListener(market));
            commander.addSystemCommand(new CounterBasedPositionManagement(market.getTradeConnections()));

            controllerBuilder.withMappingIdToName(idToName)
                .withMappingIdToUpdater(idToUpdater)
                .withMappingIdToEventManager(idToEventManager)
                .withMappingIdToStatusView(idToStatusView)
                .withMappingIdToRiskControl(idToRiskControl)
                .withMappingSymbolToContract(contracts)
                .withRiskManager(riskManager)
                .withContractInquiry(contractInquiry)
                .forMarket(market)
                .build();

            if (EngineConfig.enableWarmup()) {
                market.doWarmup();
                System.gc();
            }
            manager = new PrearrangementManager(arguments.getStrategyProperties(), false);
            manager.load(loadCommand);
            manager.enablePreloading(arguments.isHasOrderPrivileges(), idToUpdater);
        }

    }

    private static void initLogConfig() {
        final Boolean enableJMS = Boolean.valueOf(System.getProperty("enableJMS", "False"));
        if (enableJMS) {
            System.setProperty("log4j.configurationFile", "log4j2-jms.xml");
            System.setProperty("messageHubIP", EngineConfig.getMessageHubIP());
            System.setProperty("messageHubPort", EngineConfig.getMessageHubPort());
            System.out.format("Using logging config: %s, %s, %s", "log4j2-jms.xml", EngineConfig.getMessageHubIP(), EngineConfig.getMessageHubPort());
        } else {
            System.setProperty("log4j.configuration", "log4j2.xml");
            System.out.println("Using logging config: log4j2.xml");
        }
    }

    private void initRiskControl() {
        riskManager = new RiskManager(RiskManagerConfig.getInstance());
        commander.addSystemCommand(new RiskManagement(riskManager));
    }

    private void initService(final String account) {
        commander = new Commander(engineId, Collections.unmodifiableMap(idToUpdater));
        loadCommand = new LoadCommand(account);
    }

    private void initFactory(
        final EngineMode mode,
        final String path,
        final boolean hasLocalContractInfo,
        final boolean isUsingCache,
        final boolean isSingleThread) throws FileNotFoundException {

        contractInquiry = FactoryBase.init(mode, contracts, serverName, "persist.replacement", hasLocalContractInfo, isUsingCache, isSingleThread);
        commander.addSystemCommand(new ContractManagement(
            Collections.unmodifiableMap(idToUpdater),
            Collections.unmodifiableMap(idToEventManager),
            Collections.unmodifiableMap(contracts)));
    }

    private void dumpAffinityInfo(final EngineMode mode) {
        if (EngineMode.SIMULATION.equals(mode)) {
            return;
        }
        log.info("Affinity CPULayout: {} ; Process on start up: {}", AffinityLock.cpuLayout().cpus(), AffinityLock.BASE_AFFINITY);
        log.info("Available CPU for reservation: {}", AffinityLock.RESERVED_AFFINITY);
    }

}
