package com.nogle.engine.builtin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nogle.commons.command.Load;
import com.nogle.core.config.EngineMode;
import com.nogle.core.control.commands.LoadCommand;
import com.nogle.core.exception.StrategyException;
import com.nogle.core.strategy.StrategyUpdater;
import com.nogle.core.strategy.classloader.ClassLoaderFactory;
import com.nogle.engine.TradeEngine;
import com.nogle.messaging.Messenger;
import com.timelinecapital.core.config.EngineConfig;

public class PrearrangementManager {
    private static final Logger log = LogManager.getLogger(PrearrangementManager.class);

    private static final Pattern PreloadPattern = Pattern.compile("((\\S+).(jar)):((\\S+).(properties))");

    private static final HashMap<ByteArrayOutputStream, String> configsToJar = new HashMap<>();
    private static final HashMap<ByteArrayOutputStream, Integer> configToId = new HashMap<>();
    private static final HashMap<Integer, Thread> idToSimTask = new HashMap<>();

    private final boolean fixedId = true;
    private final boolean isSingleThreadyOnly;
    private int idSequence = 0;

    public PrearrangementManager(final String[] prearrangedStrategies, final boolean isSingleThreadyOnly) throws Exception {
        this.isSingleThreadyOnly = isSingleThreadyOnly;
        for (final String strategy : prearrangedStrategies) {
            log.warn("Preload models: {}", strategy);
            if (FileUtils.getFile(strategy).exists()) {
                loadFromPorpertyFile(strategy);
            } else {
                final InputStream propertiesStream = TradeEngine.class.getClassLoader().getResourceAsStream(strategy);
                if (propertiesStream != null) {
                    loadProperties(propertiesStream, StringUtils.EMPTY);
                } else {
                    loadFromExternalJar(strategy);
                }
            }
        }
    }

    private int getInternalStrategyId() {
        return fixedId ? ++idSequence : RandomUtils.nextInt(100, 1000);
    }

    /*
     * @Format ModelA.jar:Model.properties
     */
    private void loadFromExternalJar(final String propertyWithinJar) throws Exception {
        final Matcher matcher = PreloadPattern.matcher(propertyWithinJar);
        if (!matcher.matches()) {
            throw new StrategyException("Invalid format of prearranged model and properties: " + propertyWithinJar);
        }

        final String jarName = matcher.group(1);
        final String propertiesName = matcher.group(4);
        final ClassLoader classLoader = ClassLoaderFactory.getJARClassLoader(jarName);

        final InputStream propertiesStream = classLoader.getResourceAsStream(propertiesName);
        if (propertiesStream == null) {
            throw new StrategyException("Proerties file is missing: " + propertiesName);
        }
        loadProperties(propertiesStream, jarName);
        log.info("Found strategy settings {} from {}", propertiesName, jarName);
    }

    /*
     * @Format Path/To/Model.properties
     */
    private void loadFromPorpertyFile(final String propertyFile) throws IOException {
        final InputStream propertiesStream = new FileInputStream(propertyFile);
        loadProperties(propertiesStream, StringUtils.EMPTY);
        log.info("Found strategy settings {}", propertyFile);
    }

    /*
     * @Format Model.properties
     */
    private void loadProperties(final InputStream propertiesStream, final String jarName) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final Properties properties = new Properties();
        properties.load(propertiesStream);
        properties.store(outputStream, null);
        propertiesStream.close();
        configsToJar.put(outputStream, jarName);
        configToId.put(outputStream, getInternalStrategyId());
        outputStream.close();
    }

    public void load(final LoadCommand loadCommand) throws IOException {
        for (final Map.Entry<ByteArrayOutputStream, String> entry : configsToJar.entrySet()) {
            final int strategyId = (configToId.containsKey(entry.getKey())) ? configToId.get(entry.getKey()) : getInternalStrategyId();
            log.info("Loading prearranged strategy {}: {}", strategyId, entry.getValue());

            loadCommand.onCommand(Load.composeParameter(strategyId, true, entry.getValue(), entry.getKey().toString(StandardCharsets.UTF_8.toString())));

            if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
                if (isSingleThreadyOnly) {
                    loadCommand.getSimulationTask().run();
                } else {
                    final Thread simThread = new Thread(loadCommand.getSimulationTask());
                    idToSimTask.put(strategyId, simThread);
                }
            }
        }

        if (EngineMode.SIMULATION.equals(EngineConfig.getEngineMode())) {
            idToSimTask.forEach((k, v) -> v.start());
            idToSimTask.forEach((k, v) -> {
                try {
                    v.join();
                } catch (final Exception e) {
                    log.error(e.getMessage(), e);
                }
            });
            log.warn("All simulation tasks are done.");
        }
    }

    public void enablePreloading(final boolean forceEnable, final Map<Integer, StrategyUpdater> idToUpdater) {
        if (!forceEnable) {
            log.warn("Preload strategy does not have permission to trade!");
            return;
        }
        for (final Map.Entry<ByteArrayOutputStream, String> entry : configsToJar.entrySet()) {
            final int strategyId = (configToId.containsKey(entry.getKey())) ? configToId.get(entry.getKey()) : getInternalStrategyId();
            log.info("Enable prearranged strategy {}: {}", strategyId, entry.getValue());
            if (idToUpdater.containsKey(strategyId)) {
                idToUpdater.get(strategyId).onTradingEnable();
            }
        }
    }

    public static void close() {
        for (final Map.Entry<ByteArrayOutputStream, String> entry : configsToJar.entrySet()) {
            try {
                entry.getKey().close();
            } catch (final IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        configsToJar.clear();
        configToId.clear();
        Messenger.close();
    }

    public static void resotreMessengerProperties(final String fileName, final byte[] byteArray) throws URISyntaxException, IOException, InterruptedException {
        FileUtils.writeByteArrayToFile(new File(TradeEngine.class.getClassLoader().getResource(fileName).toURI()), byteArray);
    }

}
