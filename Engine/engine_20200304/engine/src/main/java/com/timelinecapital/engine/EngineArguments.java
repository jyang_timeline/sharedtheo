package com.timelinecapital.engine;

import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.nogle.core.config.EngineMode;
import com.nogle.core.strategy.logging.PrecisionPatternConverter;
import com.nogle.core.util.TradeClock;

@Parameters(separators = "=")
public class EngineArguments {

    static final String SIM_MODE = "sim";
    static final String TEST_MODE = "test";
    static final String PROD_MODE = "prod";

    private EngineMode engineMode;

    @Parameter(names = "--eid", description = "Engine Id", required = true)
    private int engineId;
    @Parameter(names = { "--user", "-u" }, description = "User")
    private String targetAccount;
    @Parameter(names = { "--password", "-p" }, description = "Password")
    private String password;
    @Parameter(names = { "--config", "-c" }, description = "Engine configuration file", required = true)
    private String engineProperties;
    @Parameter(names = "--strategy", description = "List of strategies getting loaded after engine starts")
    private List<String> strategyProperties;

    @Parameter(names = "--file-localinfo")
    private String localFilePath;
    @Parameter(names = "--hasContractInfo", description = "Read static contract information from file")
    private boolean hasLocalContractInfo;
    @Parameter(names = "--grantOrderPrivileges", description = "Grant trading privileges to all models from command line")
    private boolean hasOrderPrivileges;

    EngineMode getMode(final String mode) {
        switch (mode) {
            case SIM_MODE:
                TradeClock.setMode(EngineMode.SIMULATION);
                System.setProperty("log4j.Clock", TradeClock.class.getName());
                PrecisionPatternConverter.onSimulationMode();
                return EngineMode.SIMULATION;
            case TEST_MODE:
                return EngineMode.TESTING;
            case PROD_MODE:
                TradeClock.setMode(EngineMode.PRODUCTION);
                enableAsync();
                return EngineMode.PRODUCTION;
            default:
                TradeClock.setMode(EngineMode.PRODUCTION);
                enableAsync();
                return EngineMode.PRODUCTION;
        }
    }

    @Parameters(separators = "=")
    public static class ProductionMode {

    }

    @Parameters(separators = "=")
    public static class TestMode {

    }

    @Parameters(separators = "=")
    public static class SimulationMode {

        @Parameter(names = "--config", description = "Simulation configuration file")
        private static String simulationProperties;
        @Parameter(names = "--start")
        private static String simStart;
        @Parameter(names = "--end")
        private static String simEnd;
        @Parameter(names = "--simModels")
        private static List<String> simModels;
        @Parameter(names = "--isUseCachedPersist", description = "Use cached persist for simulation")
        private static boolean isUseCachedPersist;
        @Parameter(names = "--singleThread", description = "Force simulation to run in single thread")
        private static boolean singleThread;

    }

    private static void enableAsync() {
        System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
        // System.setProperty("AsyncLogger.WaitStrategy", "busyspin");
        System.setProperty("log4j2.enable.threadlocals", "TRUE");
        System.setProperty("log4j2.enable.direct.encoders", "TRUE");
        System.setProperty("log4j2.garbagefree.threadContextMap", "TRUE");
    }

    public String getSimulationProperties() {
        return SimulationMode.simulationProperties;
    }

    public String getSimStart() {
        return SimulationMode.simStart;
    }

    public String getSimEnd() {
        return SimulationMode.simEnd;
    }

    public String[] getSimModels() {
        if (SimulationMode.simModels == null) {
            return new String[0];
        }
        final String[] tempArray = new String[SimulationMode.simModels.size()];
        return SimulationMode.simModels.toArray(tempArray);
    }

    public boolean isUseCachedPersistForSimulation() {
        return SimulationMode.isUseCachedPersist;
    }

    public boolean isSingleThreadOnly() {
        return SimulationMode.singleThread;
    }

    public int getEngineId() {
        return engineId;
    }

    public String getEngineProperties() {
        return engineProperties;
    }

    public String[] getStrategyProperties() {
        if (strategyProperties == null) {
            return new String[0];
        }
        final String[] tempArray = new String[strategyProperties.size()];
        return strategyProperties.toArray(tempArray);
    }

    public String getTargetAccount() {
        return targetAccount;
    }

    public String getPassword() {
        return password;
    }

    public EngineMode getEngineMode() {
        return engineMode;
    }

    public String getLocalFilePath() {
        return localFilePath;
    }

    public boolean isHasLocalContractInfo() {
        return hasLocalContractInfo;
    }

    public boolean isHasOrderPrivileges() {
        return hasOrderPrivileges;
    }

    public static EngineArguments resolveArg(final String args[]) {
        final EngineArguments instance = new EngineArguments();
        final JCommander jCom = new JCommander(instance);

        jCom.addCommand(SIM_MODE, new SimulationMode());
        jCom.addCommand(TEST_MODE, new TestMode());
        jCom.addCommand(PROD_MODE, new ProductionMode());
        jCom.parse(args);
        instance.engineMode = instance.getMode(jCom.getParsedCommand());

        return instance;
    }

}
