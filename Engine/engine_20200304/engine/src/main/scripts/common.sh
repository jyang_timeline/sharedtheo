#!/bin/bash
ENGINE_HOME=$1
ENGINE_ID=$2
ENGINE_ACCOUNT=$3

ENGINE_MODE=prod
ENGINE_CLASS=com.nogle.engine.TradeEngine

ENGINE_CONFIG=trading.properties
#STRATEGY_CONFIG=SampleModel-Simulation.properties

#ENGINE_STRATEGYPATH=/path/to/place/strategy/implementation
#ENGINE_STRATEGY_PROPERTIES=/path/to/place/strategy/properties

ENGINE_ARGS_ID="-eid=$ENGINE_ID"

#ENGINE_ARGS_STRATEGYCONFIG="--stgy=$STRATEGY_CONFIG"
ENGINE_ARGS_ENGINECONFIG="--ep=$ENGINE_CONFIG"
ENGINE_ARGS_ACCOUNT="-user=$ENGINE_ACCOUNT"

ENGINE_ARGS="$ENGINE_ARGS_ID $ENGINE_ARGS_ENGINECONFIG $ENGINE_ARGS_ACCOUNT"

JAVA=/home/op/jdk-9.0.4/bin/java

function getPID()
{
    echo `ps x | grep "$ENGINE_CLASS" | grep "$ENGINE_ARGS_ID" | grep -v "grep" | awk '{ print $1 }'`
}

