#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/autostart-common.sh $1 $2

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ENGINE_HOME/share

JAVA_OPTS="-server -XX:CompileThreshold=1000 -XX:+BackgroundCompilation -XX:+UseFastAccessorMethods -XX:+UseFastJNIAccessors -XX:-UseBiasedLocking -Xmx4096m -Xms4096m -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -Xloggc:gclogs/gc.log -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.port=1098 -Dcom.sun.management.jmxremote.rmi.port=1097 -Djava.rmi.server.hostname=192.168.1.11"

CLASS_PATH="$ENGINE_HOME/trader/$ENGINE_ID/config:$ENGINE_HOME/share/config:$ENGINE_HOME/share/lib/*:$ENGINE_HOME/share/jar/*:$ENGINE_HOME/deploy/"

#echo $CLASS_PATH

(
    flock -x -w 10 200 || exit 1
    PIDS=$(getPID)
    if [ ! -z "$PIDS" ]; then
        echo "TradeEngine already running in $ENGINE_ID with $PIDS"
    else
         $JAVA $JAVA_OPTS -classpath $CLASS_PATH $ENGINE_CLASS $ENGINE_ARGS $ENGINE_MODE &
         # $JAVA $JAVA_OPTS -classpath $CLASS_PATH $ENGINE_CLASS $ENGINE_ARGS $ENGINE_MODE &
    fi
) 200>$ENGINE_HOME/trader/$ENGINE_ID/TradeEngine.runLock
