@echo off

REM Read all simulation settings from txt
for /f "eol=; tokens=1-4 delims=, " %%a in (%1) do (
    echo %%a %%b %%c %%d
	REM name of jar
    set MODEL_JAR=%%a
	REM name of model
    set MODEL_NAME=%%b
	REM simulation start day in yyyyMMdd
    set SIM_START=%%c
	REM simulation end day in yyyyMMdd
    set SIM_END=%%d
)

REM path\to\model\folder
set MODEL_HOME=C:\workspace\TradingModel\trade-model\target
REM path\to\engine\folder
set ENGINE_HOME=C:\workspace\TradeEngine\engine\target
set ENGINE_ID=101
set ENGINE_CONFIG=trading.properties
set SIM_CONFIG=simulation.properties
set ENGINE_CLASS=com.nogle.engine.TradeEngine
set ENGINE_MODE=sim

set STRATEGY_CONFIG=%MODEL_JAR%:%MODEL_NAME%

set CLASS_PATH=%MODEL_HOME%\;%ENGINE_HOME%\dependency\*;%ENGINE_HOME%\config;%ENGINE_HOME%\*;.

set PROGRAM_ARGS=-eid=%ENGINE_ID% --ep=%ENGINE_CONFIG% --stgy=%STRATEGY_CONFIG% --sim=%SIM_CONFIG%
set SIMCONFIG=-start=%SIM_START% -end=%SIM_END%

set JAVA_OPTS=-server -XX:CompileThreshold=2000 -Xmx1024m -Xms1024m
"%JAVA_HOME%\bin\java" %JAVA_OPTS% -classpath %CLASS_PATH% %ENGINE_CLASS% %PROGRAM_ARGS% %ENGINE_MODE% %SIMCONFIG%

REM pause
