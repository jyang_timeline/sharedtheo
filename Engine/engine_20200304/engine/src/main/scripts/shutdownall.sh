#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#source $DIR/common.sh

function getPID()
{
    echo `ps aux | grep "com.nogle.engine.TradeEngine" | grep -v "grep" | awk '{ print $2 }'`
}

function getConnectPID()
{
    echo `ps aux | grep "Responser" | grep -v grep | grep "trade" | awk ' {print $2}'`
}

function getQuotePID()
{
    echo `ps aux | grep "Responser" | grep -v grep | grep "quote" | awk ' {print $2}'`
}

QPIDS=$(getQuotePID)
if [ ! -z "$QPIDS" ]; then
    kill -SIGQUIT $QPIDS
    echo "Kill -SIGQUIT $QPIDS"
    sleep 5

    QPIDS=$(getQuotePID)
    if [ ! -z "$QPIDS" ]; then
        kill -SIGKILL $QPIDS
        echo "Kill -SIGKILL $QPIDS"
    fi
fi



CPIDS=$(getConnectPID)
if [ ! -z "$CPIDS" ]; then
    kill -SIGQUIT $CPIDS
    echo "Kill -SIGQUIT $CPIDS"
    sleep 5

    CPIDS=$(getConnectPID)
    if [ ! -z "$CPIDS" ]; then
        kill -SIGKILL $CPIDS
        echo "Kill -SIGKILL $CPIDS"
    fi
fi


PIDS=$(getPID)
if [ ! -z "$PIDS" ]; then
    kill -SIGTERM $PIDS
    echo "Kill -SIGTERM $PIDS"
    sleep 5

    PIDS=$(getPID)
    if [ ! -z "$PIDS" ]; then
        kill -SIGKILL $PIDS
        echo "Kill -SIGKILL $PIDS"
    fi
else
    echo "No process running"
fi

