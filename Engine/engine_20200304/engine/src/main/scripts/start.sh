#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HOME=$( getent passwd "op" | cut -d: -f6 )
RESERVED=$3

source $DIR/common.sh $HOME $1 $2

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ENGINE_HOME/share

JAVA_OPTS="-server -DenableJMS=false -XX:CompileThreshold=500 -XX:+UseCompressedOops -XX:+BackgroundCompilation -XX:-UseBiasedLocking -XX:+UseThreadPriorities -Xmx8G -Xms8G -Xmn4G"

CLASS_PATH="$ENGINE_HOME/trader/$ENGINE_ID/config:$ENGINE_HOME/share/config:$ENGINE_HOME/share/lib/*:$ENGINE_HOME/share/jar/*:$ENGINE_HOME/deploy"

(
    flock -x -w 10 200 || exit 1
    PIDS=$(getPID)
    if [ ! -z "$PIDS" ]; then
        echo "TradeEngine already running in $ENGINE_ID with $PIDS"
    else
        onload $JAVA $JAVA_OPTS -classpath $CLASS_PATH $ENGINE_CLASS $ENGINE_ARGS $ENGINE_MODE &
		
		# $JAVA $JAVA_OPTS -classpath $CLASS_PATH $ENGINE_CLASS $ENGINE_ARGS $ENGINE_MODE &
        # jstat -gc -h30 $PID $INTERVAL > test.txt
    fi
) 200>$ENGINE_HOME/trader/$ENGINE_ID/TradeEngine.runLock

