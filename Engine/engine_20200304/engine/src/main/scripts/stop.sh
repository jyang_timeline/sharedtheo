#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/common.sh

PIDS=$(getPID)
if [ ! -z "$PIDS" ]; then
    kill -SIGTERM $PIDS
    echo "Kill -SIGTERM $PIDS"
    sleep 5

    PIDS=$(getPID)
    if [ ! -z "$PIDS" ]; then
        kill -SIGKILL $PIDS
        echo "Kill -SIGKILL $PIDS"
    fi
else
    echo "No process running"
fi

