package com.nogle.engine;

import java.util.concurrent.TimeUnit;

import com.nogle.core.util.TradeClock;

public class TimerTest {
    void test_milli_speed(final int iters) {
        long sum = 0;
        final int N = iters;
        // final int N = 1000;
        final long t1 = System.currentTimeMillis();
        // final long t1 = System.nanoTime();
        for (int i = 0; i < N; i++)
            sum += System.currentTimeMillis();
        final long t2 = System.currentTimeMillis();
        // final long t2 = System.nanoTime();
        System.out.println("Sum = " + sum + "; time = " + (t2 - t1) +
            "; or " + (t2 - t1) * 1.0E6 / N + " ns / iter");
    }

    void test_jni_milli_speed(final int iters) {
        long sum = 0;
        final int N = iters;
        // final int N = 1000;
        // final long t1 = System.currentTimeMillis();

        final long t1_millis = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) * 1000000000;
        final long t1 = t1_millis + TradeClock.getCurrentMillis();
        for (int i = 0; i < N; i++)
            sum += TradeClock.getCurrentMillis();
        // sum += System.currentTimeMillis();
        // final long t2 = System.currentTimeMillis();
        final long t2_nano = TradeClock.getCurrentMillis();
        final long t2 = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) * 1000000000 + t2_nano;

        System.out.println("JNI Sum = " + sum + "; time = " + (t2 - t1) +
            "; or " + (t2 - t1) / N + " ns / iter");
    }
}
