import os
import sys

import conconfigparser

def group_properties(properties):
    '''group properties which have similar symbols so we can cache them simutaneously.'''
    p2s = {}
    for p in properties:
        config = conconfigparser.read(p)
        p2s[p] = {v for k, v in config.items() if 'Contract' in k}

    properties_groupized = set()
    symbols = set()
    for p in p2s:
        symbols |= p2s[p]
    print(len(p2s), len(symbols))

    group = {}

    group_index = 0
    for p in p2s:
        if p in properties_groupized:
            continue
        else:
            tmp_group_properties = set([p])
            tmp_group_symbols = set(p2s[p])
            properties_groupized.add(p)
            while True:
                change = False
                for p2 in p2s:
                    if p2 in properties_groupized:
                        continue
                    elif len(p2s[p2] & tmp_group_symbols) > 0:
                        tmp_group_symbols |= p2s[p2]
                        tmp_group_properties.add(p2)
                        properties_groupized.add(p2)
                        change = True

                print('group%d #properties: %d #symbols: %d' % (group_index, len(tmp_group_properties), len(tmp_group_symbols)))
                if not change:
                    break

            print('----final group%d #properties: %d #symbols: %d--------' % (group_index, len(tmp_group_properties), len(tmp_group_symbols)))
            print('symbol', sorted(tmp_group_symbols))
            group[group_index] = (tmp_group_properties, tmp_group_symbols)
            group_index += 1

    return group


if __name__ == '__main__':
    import glob
    properties = glob.glob(os.path.join(sys.argv[1], '*'))
    group = group_properties(properties)
    total_properties = sum(map(lambda x: len(x[0]), group.values()))
    total_symbols = sum(map(lambda x: len(x[1]), group.values()))
    print(len(properties), total_properties, total_symbols)
    import pdb; pdb.set_trace()
