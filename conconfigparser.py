from collections import OrderedDict


def read(fpath):
    space_count = 0
    config = OrderedDict()
    with open(fpath) as rfile:
         for line in rfile:
             if not line.startswith('#'):
                 if len(line.split('=')) == 2:
                     key, val = line.split('=')
                     config[key.strip()] = val.strip()
                 elif len(line.split('=')) == 1:
                     config['space%d' % space_count] = 0
                     space_count += 1
    return config


def write(fpath, config):
    with open(fpath, 'w') as wfile:
        for key, val in config.items():
            if key.startswith('space'):
                wfile.write('\n')
            else:
                wfile.write('%s = %s\n' % (key, val))



def update(config, key, val):
    if key in config:
        print('%s = %s -> %s = %s' % (key, config[key], key, val))    
    else:
        print('new field %s = %s' % (key, val))
    config[key] = val


if __name__ == '__main__':
   import sys
   config = read(sys.argv[1])
   config['dummykey'] = 'dummyvalue'
   write(sys.argv[2], config)
