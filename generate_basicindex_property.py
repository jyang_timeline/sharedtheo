import glob
import os
import pickle
import shutil
from group import Group
from collections import OrderedDict

from CorrIndexSharedTheo import CorrIndexSharedTheo

class CorrBasicIndexSharedTheo(CorrIndexSharedTheo):

    def generate_basicindex_properties(self, output_dir, basename=None, normalize=True, weights=None, suffix=''):
        if not basename:
            basename = self._get_default_basename(suffix)

        cluster_data = self.cluster_data
        property_dir = os.path.join(output_dir, 'properties')
        os.makedirs(property_dir, exist_ok=False)
        shutil.copytree(self.cluster_dir, os.path.join(output_dir, 'corr_cluster'))
        for corr in cluster_data:
            clusters = cluster_data[corr]['c']
            for i, cluster in enumerate(clusters):
                fname = basename + '_corr%d_cluster%d' % (int(float(corr) * 1000), i) + '.properties'
                fpath = os.path.join(property_dir, fname)
                if weights:
                    cluster = {s: weights[s] for s in cluster if s in weights}
                group = Group(cluster)
                params = OrderedDict()
                params['name'] = fname.split('.properties')[0]
                params['classPath'] = 'com.timelinecapital.sharedtheo.MHBasicIndexBuilder'
                params['normalize'] = 'true' if normalize else 'false'
                params['useWeight'] = 'false' if not weights else 'true'
                params.update(group.to_params())

                with open(fpath, 'w') as wfile:
                    for k, v in params.items():
                        wfile.write('%s = %s\n' % (k, v))

    # def generate_basicindex_turnover_weighted(self, output_dir, weights, basename=None, suffix=''):
    #     if not basename:
    #         basename = os.path.basename(self.cluster_dir)

    #     output_dir = output_dir[:-1] if output_dir[-1] == '/' else output_dir
    #     output_dir = output_dir + '_to'
    #     basename = basename + '_to'
    #     self.generate_basicindex_properties(output_dir, basename=basename, normalize=True, weights=weights)

    # def generate_basicindex_mktcpt_weighted(self, output_dir, weights, basename=None, suffix=''):
    #     if not basename:
    #         basename = os.path.basename(self.cluster_dir)

    #     output_dir = output_dir[:-1] if output_dir[-1] == '/' else output_dir
    #     output_dir = output_dir + '_mktcpt'
    #     basename = basename + '_mktcpt'
    #     self.generate_basicindex_properties(output_dir, basename=basename, normalize=False, weights=weights)

if __name__ == '__main__':
    import sys
    cluster_dir = sys.argv[1]
    corrsharedtheo = CorrBasicIndexSharedTheo(cluster_dir)
    import json
    outstandings = json.load(open('/home/mhyang/Data/experiment-stored/researchs/Examples/MH/stocks/additional_data/outstanding.json'))
    turnover = json.load(open('/home/mhyang/Data/experiment-stored/researchs/Examples/MH/stocks/additional_data/turnover_0601_0815.json'))
    # suffix = '_to'
    # output_dir = 'corrsharedtheo_%s%s' % (os.path.basename(cluster_dir), suffix)
    # corrsharedtheo.generate_basicindex_properties(output_dir, suffix=suffix, normalize=True, weights=turnover)
    suffix = 'mktcap'
    output_dir = 'corrsharedtheo_%s%s' % (os.path.basename(cluster_dir), suffix)
    corrsharedtheo.generate_basicindex_properties(output_dir, suffix=suffix, normalize=False, weights=outstandings)

