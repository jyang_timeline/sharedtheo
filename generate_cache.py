import sys
import os
import logging
import tempfile
import shutil
from distutils import dir_util
from random import randint
from collections import defaultdict
import multiprocessing

import conconfigparser
import combine


TRADE_MODEL_DIR = 'tradingmodel-live-stock'
SHAREDTHEO_MODEL = 'MHTestModelWithSharedTheo'

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_sharedfolder_cachedir(engine_config_dir):
    config = conconfigparser.read(os.path.join(engine_config_dir, 'simulation.properties'))
    return config['theoPath']


def get_cmd(model_dir, dates):

    modelname = SHAREDTHEO_MODEL
    sim_start, sim_end = dates.split(',')

    engine_id = randint(0, 2**31)
    print('eid:', engine_id)
    logger.info('Simulate Java Model on engine %d: %s' % (engine_id, modelname))


    MODEL_HOME = os.path.join(model_dir, 'trade-model', 'target')
    MODEL_JAR = "trade-model-1.0.jar"

    ENGINE_HOME = os.path.join(ENGINE_DIR, 'engine', 'target')

    temp_config_dir = tempfile.mkdtemp()
    dir_util.copy_tree(os.path.join(ENGINE_CONFIG_DIR), temp_config_dir)

    MODEL_PROPERTY_FILE = modelname + '.properties'

    ENGINE_ID= engine_id
    ENGINE_CONFIG="trading.properties"
    SIM_CONFIG="simulation.properties"
    ENGINE_CLASS="com.nogle.engine.TradeEngine"
    STRATEGY_CONFIG=MODEL_PROPERTY_FILE

    CLASS_PATH="{config_dir}:{model_home}:{engine_home}/jar/*:{engine_home}/lib/*:{model_home}:{model_jar}:.".format(
        model_home=MODEL_HOME,
        engine_home=ENGINE_HOME,
        model_jar=os.path.join(MODEL_HOME, MODEL_JAR),
        config_dir=temp_config_dir
    )
    PROGRAM_ARGS="--eid=%d --config=%s  sim --simModels=%s --config=%s --start=%s --end=%s --isUseCachedPersist " % (ENGINE_ID, ENGINE_CONFIG, STRATEGY_CONFIG, SIM_CONFIG, sim_start, sim_end)
    JAVA_OPTS="-server -XX:CompileThreshold=2000 -Xmx30000m -Xms1024m"

    cmd = "java {java_opts} -Djava.library.path={engine_home}/core/jni/ -classpath {class_path} {engine_class} {program_args} ".format(
        java_opts=JAVA_OPTS,
        class_path=CLASS_PATH,
        engine_class=ENGINE_CLASS,
        program_args=PROGRAM_ARGS,
        engine_home=os.path.dirname(os.path.dirname(ENGINE_HOME))
    )
    return cmd


def change_testmodel(model_dir, sharedtheo_properties_list):
    fpath = os.path.join(model_dir, 'trade-model', 'src', 'main', 'resources',  SHAREDTHEO_MODEL + '.properties')
    lines = open(fpath).readlines()
    new_lines = []
    for line in lines:
        if 'String.SharedTheo.CustomIndex1' in line:
            for i, p in enumerate(sharedtheo_properties_list):
               tmp_line = '%s = %s\n' % ('String.SharedTheo.CustomIndex%d' % (i + 1), p)
               # print(tmp_line)
               new_lines.append(tmp_line)
        else:
            new_lines.append(line)

    with open(fpath, 'w') as wfile:
        wfile.writelines(new_lines)



def dates_already_run(sharedtheo_properties_list):
    maxs = []
    mins = []
    for p in sharedtheo_properties_list:
        subfolder = os.path.join(SHAREDTHEO_CACHE_DIR, p.split('.properties')[0])
        if os.path.exists(subfolder) and len(os.listdir(subfolder)) > 0:
            tmplist = os.listdir(subfolder)
            maxs.append(max(tmplist))
            mins.append(min(tmplist))
        else:
            return None

    return max(mins), min(maxs)

def dates_to_cache(sharedtheo_properties_list, dates, overwrite=False):
    if overwrite:
        return [dates]

    from datetime import datetime, timedelta
    def shift(org, days):
        return (datetime.strptime(org, '%Y%m%d') + timedelta(days=days)).strftime('%Y%m%d')

    r = dates_already_run(sharedtheo_properties_list)
    if r is None:
        return [dates]
    else:
        startdate, enddate = dates.split(',')
        mindate, maxdate = r
        # No overlapped
        if enddate < mindate or startdate > maxdate:
            return [dates]
        # new interval is included by run dates
        elif mindate <= startdate and maxdate >= enddate:
            return []
        else:
            tmp = []
            if startdate < mindate:
                tmp.append(startdate + ',' + shift(mindate, -1))
            if enddate > maxdate:
                tmp.append(shift(maxdate, 1) + ',' + enddate)
            return tmp

def run(sharedtheo_properties_list):
    with tempfile.TemporaryDirectory() as tempdir:
        shutil.copytree(os.path.join(TRADE_MODEL_DIR, 'trade-model'), os.path.join(tempdir, 'trade-model'))
        change_testmodel(tempdir, sharedtheo_properties_list)
        for p in sharedtheo_properties_list:
            shutil.copy(os.path.join(PROPERTIES_FOLDER, p), os.path.join(tempdir, 'trade-model', 'src', 'main', 'resources'))
        dates_intervals = dates_to_cache(sharedtheo_properties_list, DATES)
        print(dates_intervals)
        cwd = os.getcwd()
        os.chdir(os.path.join(tempdir, 'trade-model'))
        os.system('mvn clean package > null 2> null2')
        os.system('mvn clean package')
        os.chdir(cwd)
        for d in dates_intervals:
            cmd = get_cmd(tempdir, d)
            print(cmd)
            os.system(cmd)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--properties_dir', help='sharedfolder properties dir', required=True)
    parser.add_argument('--engine_dir', help='engine dir', required=True)
    parser.add_argument('--engine_config_dir', help='engine config dir', required=True)
    parser.add_argument('--dates', help='default 2 weeks')
    parser.add_argument('--n_processes', type=int, default=3)
    args = parser.parse_args()
    N_PROCESSES = args.n_processes
    PROPERTIES_FOLDER = args.properties_dir
    ENGINE_DIR = args.engine_dir
    ENGINE_CONFIG_DIR = args.engine_config_dir
    if args.dates is not None:
        DATES = args.dates
    else:
        from datetime import datetime, timedelta
        today = datetime.now()
        DATES = today.strftime('%Y%m%d') + ',' + (today - timedelta(days=14)).strftime('%Y%m%d')
    SHAREDTHEO_CACHE_DIR = get_sharedfolder_cachedir(ENGINE_CONFIG_DIR)
    properties = os.listdir(PROPERTIES_FOLDER)
    group_properties = combine.group_properties([os.path.join(PROPERTIES_FOLDER, _) for _ in properties])
    list_list_properties = [[os.path.basename(__) for __ in _[0]] for _ in group_properties.values()]
    
    if N_PROCESSES > 1:
        pool = multiprocessing.Pool(N_PROCESSES)
        pool.map(run, list_list_properties)
    else:
        for elem in list_list_properties:
            run(elem)
