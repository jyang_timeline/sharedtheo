import glob
import os
import pickle
import shutil
from group import Group
from collections import OrderedDict

from CorrIndexSharedTheo import CorrIndexSharedTheo


class NameClusterSharedTheo:

    def __init__(self, fpath):
        self.fpath = fpath
        self.clusters = pickle.load(open(fpath, 'rb'))

    def generate_basicindex_properties(self, output_dir, basename=None, normalize=True, weights=None, suffix=''):
        property_dir = os.path.join(output_dir, 'properties')
        os.makedirs(property_dir, exist_ok=False)
        shutil.copyfile(self.fpath, os.path.join(output_dir, os.path.basename(self.fpath)))
        for name, cluster in self.clusters.items():
            fname = os.path.basename(self.fpath).split('.p')[0] + '_' + name + '_' + suffix + '.properties'
            fpath = os.path.join(property_dir, fname)
            if weights:
                cluster = {s: weights[s] for s in cluster if s in weights}
            group = Group(cluster)
            params = OrderedDict()
            params['name'] = fname.split('.properties')[0]
            params['classPath'] = 'com.timelinecapital.sharedtheo.MHRealTimeIndex3Builder'
            params['normalize'] = 'true' if normalize else 'false'
            params['useWeight'] = 'false' if not weights else 'true'
            params['insertBetterTradePrice'] = 'true'
            params['insertOneTickBookWhenConflict'] = 'true'
            params['protectedTradeDiffMicros'] = 800000
            params['isCheckTradeByCumSum'] = 'true'
            params.update(group.to_params())

            with open(fpath, 'w') as wfile:
                for k, v in params.items():
                    wfile.write('%s = %s\n' % (k, v))


if __name__ == '__main__':
    import sys
    cluster_file = sys.argv[1]
    corrsharedtheo = NameClusterSharedTheo(cluster_file)
    import json
    outstandings = json.load(open('additional_data/outstanding.json'))
    turnover = json.load(open('additional_data/turnover_0601_0815.json'))
    # suffix = '_to'
    # output_dir = 'corrsharedtheo_%s%s' % (os.path.basename(cluster_dir), suffix)
    # corrsharedtheo.generate_basicindex_properties(output_dir, suffix=suffix, normalize=True, weights=turnover)
    suffix = 'rl3_mktcap'
    output_dir = '%s_%s' % (os.path.basename(cluster_file).split('.')[0], suffix)
    corrsharedtheo.generate_basicindex_properties(output_dir, suffix=suffix, normalize=False, weights=outstandings)

