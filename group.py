import os
from enum import Enum
from collections import OrderedDict
from datetime import datetime, timedelta
from sqlalchemy import create_engine

import pandas as pd

ENGINE = create_engine("postgresql://postgres@192.168.1.11/hft_research")


def getSymbolExchange(symbol):
    return 'SSE' if symbol[0] == '6' else 'SZSE'


class Group:

    basic_info = None
    tu_info = None

    def __init__(self, candidates):
        self.candidates = candidates  # {candidates}

    def to_params(self):
        params = OrderedDict()
        symbols = sorted(self.candidates)
        for i, s in enumerate(symbols):
            params['Contract.Symbol%d' % (i + 1)] = s + '@' + getSymbolExchange(s)

        if isinstance(self.candidates, dict):
            for i, s in enumerate(symbols):
                params['Weight.Symbol%d' % (i + 1)] = self.candidates[s]

        return params


if __name__ == '__main__':
    import pickle
    clusters = pickle.load(open("/home/mhyang/Data/Experiment/ClusteringSymbols/dbscan_corr0.07_n5/clusters.p", 'rb'))
    group1 = Group(clusters[0])
    import pdb; pdb.set_trace()
