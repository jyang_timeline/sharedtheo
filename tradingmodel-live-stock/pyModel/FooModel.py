#
# Model implement: 
#    run with python in research/develop/test time(local),
#    with jython in test/run time(TradeEngine).
#

from com.nogle.strategy.api import Command, Strategy, StrategyBuilder;

from com.nogle.strategy.types import TradeServices, Side, TimeCondition, Config;

class FooCommand(Command):
    def onCommand(self, args):
        print('FooCommand.....')
        return 'FooReturn'

    def getName(self):
        print('~Name')
        return 'Foo'

    def getDescription(self):
        print('~Desciption')
        return 'FooDescription'

    def getUsage(self):
        print('~Usage')
        return 'FooUsage'

class BarCommand(Command):
    def onCommand(self, args):
        print('BarCommand.....')
        return 'BarReturn'

    def getName(self):
        print('+Name')
        return 'Bar'

    def getDescription(self):
        print('+Desciption')
        return 'BarDescription'

    def getUsage(self):
        print('+Usage')
        return 'BarUsage'

class FooModelBuilder(StrategyBuilder):
    def build(self, tradeServices):
        return FooModel(tradeServices)

class FooModel(Strategy):
    def __init__(self, tradeServices):
        self.tradeServices = tradeServices;
        tradeServices.addCommand(FooCommand())
        tradeServices.addCommand(BarCommand())
        self.logger = tradeServices.getLogger()
        self.logger.warn('FooModel!!!!')

    def onStart(self, updateId):
        self.logger.warn('Start!!!!!!!!!!!!!')

    def onStop(self, updateId):
        self.logger.warn('Stop!!!!!!!!!!!!!!!')

    def onShutdown(self, updateId):
        self.logger.warn('Shutdown!!!!!!!!!!!')

    def onTick(self, updateId, tick):
        self.logger.warn('onTick --> [%s] %d@%f' % (tick.getContract().getSymbol(), tick.getQuantity(), tick.getPrice()))

    def onMarketBook(self, updateId, book):
        self.logger.warn('onBook --> [%s] %d@%f %d@%f' % (book.getContract().getSymbol(), book.getBidQty(), book.getBidPrice(), book.getAskQty(), book.getAskPrice()))
        orderView = self.tradeServices.getOrderView(book.getContract(), Side.BUY, TimeCondition.GFD)
        orderView.setOrder(1, book.getAskPrice() + 1)
        orderView.commit(updateId)
 
        orderView = self.tradeServices.getOrderView(book.getContract(), Side.SELL, TimeCondition.GFD)
        orderView.setOrder(1, book.getBidPrice() - 1)
        orderView.commit(updateId)

    def onFill(self, updateId, fill):
        self.logger.warn('onFill --> [%s] %d@%f' %(fill.getContract().getSymbol(), fill.getFillQty(), fill.getFillPrice()))

    def onConfigChange(self, config):
        self.logger.warn('onCONFIGGGGGGGGGG!!')

