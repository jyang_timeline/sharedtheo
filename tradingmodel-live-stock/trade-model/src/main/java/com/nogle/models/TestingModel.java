package com.nogle.models;

import java.util.ArrayList;

import com.timelinecapital.util.EMA;
import com.timelinecapital.util.PriceUtils;
import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.types.BestOrderView;
import com.nogle.strategy.types.BookView;
import com.nogle.strategy.types.Config;
import com.nogle.strategy.types.Contract;
import com.nogle.strategy.types.Fill;
import com.nogle.strategy.types.Miss;
import com.nogle.strategy.types.OrderView;
import com.nogle.strategy.types.PositionView;
import com.nogle.strategy.types.QuoteOrderView;
import com.nogle.strategy.types.QuoteSnapshot;
import com.nogle.strategy.types.Side;
import com.nogle.strategy.types.TickView;
import com.nogle.strategy.types.TimeCondition;
import com.nogle.strategy.types.TradeLogger;
import com.nogle.strategy.types.TradeServices;
import com.timelinecapital.annotation.QuoteSubscription;
import com.timelinecapital.strategy.api.SharedTheo;
import com.timelinecapital.view.TradeStatistics;

public class TestingModel implements Strategy {
    private final TradeLogger log;

    private final static String KEY_SELF = "Strategy.Self";
    private final static String KEY_REF = "Strategy.Ref";
    private final static String KEY_NEXT = "Strategy.Next";

    private final ArrayList<Double> fillsList = new ArrayList<Double>();
    private final ArrayList<Double> pnlList = new ArrayList<Double>();

    private final TradeServices tradeServices;

    @QuoteSubscription
    private Contract self;
    @QuoteSubscription
    private Contract ref;
    private final double thres = 15;
    private SharedTheo customIndex1;

    public TestingModel(final TradeServices tradeServices) {
        this.tradeServices = tradeServices;
        log = tradeServices.getLogger();

    }

    public void updateMask(final boolean mask) {
        log.warn("Update variable mask to {}", mask);
    }

    @Override
    public void onStart(final long updateId) {
    }

    @Override
    public void onStop(final long updateId) {
        log.info("Closing. Going flat for the day.");
    }

    @Override
    public void onShutdown(final long updateId) { }

    @Override
    public void onTick(final long updateId, final TickView tick) {
        // log.info("[{}] onTick: {} {}ms {} {}, {} {} {} {}/{} ",
        // tick.getSequenceNo(),
        // tick.getExchangeUpdateTimeMicros(),
        // tick.getUpdateTimeMicros(),
        // tick.getContract(),
        // tick.getType(),
        // tick.getQuantity(),
        // tick.getPrice(),
        // tick.getVolume(),
        // tick.getOpenInterest());
    }

    @Override
    public void onMarketBook(final long updateId, final BookView book) { }

    @Override
    public void onBestOrderDetail(final long updateId, final BestOrderView bestOrderView) {
        log.info("onBestOrderDetail info {} {} {} / {}", bestOrderView.getContract(), bestOrderView.getUpdateTimeMillis(),
            bestOrderView.getBestBidPrice(), bestOrderView.getBestAskPrice());
    }

    @Override
    public void onQuoteOrderDetail(final long updateId, final QuoteOrderView quoteOrderView) {
        log.info("onQuoteOrderDetail info {} {} {} {}", quoteOrderView.getContract(), quoteOrderView.getUpdateTimeMillis(),
            quoteOrderView.getContract());
    }

    @Override
    public void onFill(final long updateId, final Fill fill) {
    }

    @Override
    public void onMiss(final long updateId, final Miss miss) {
        log.warn("IOC order missed: {} {} {}@{}", miss.getContract(), miss.getSide(), miss.getMissQty(), miss.getMissPrice());
    }

    @Override
    public void onConfigChange(final Config config) {
        try {
            if (config.getKeys().contains("String.SharedTheo.CustomIndex1"))
            {
                customIndex1 = tradeServices.getSharedTheo(config.getString("SharedTheo.CustomIndex1"));
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onCommandRollCall(final TradeServices tradeServices) {
    }

}
