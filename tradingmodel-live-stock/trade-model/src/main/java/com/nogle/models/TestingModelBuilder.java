package com.nogle.models;

import com.nogle.strategy.api.Strategy;
import com.nogle.strategy.api.StrategyBuilder;
import com.nogle.strategy.types.TradeServices;

public class TestingModelBuilder implements StrategyBuilder {

    @Override
    public Strategy build(final TradeServices tradeServices) {
        return new TestingModel(tradeServices);
    }

}
